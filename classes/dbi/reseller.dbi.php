<?php
require_once("classes/N2MY_DBI.class.php");

/*
 * Created on 2006/02/15
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
class ResellerTable extends N2MY_DB {

    var $table = 'reseller';
    protected $primary_key = "reseller_key";

    /**
     * DB接続
     */
    function ResellerTable($dsn)
    {
        $this->logger =& EZLogger::getInstance();
        $this->init($dsn, $this->table);
    }

    /**
     * ログイン認証を行い、存在すればリセラー情報を返す
     */
    function checkLogin($id, $pass, $admin_id="", $admin_pw="")
    {
        if ($admin_id == $id) {
            // 管理用ログインユーザ
            if ($admin_pw == $pass) {
                // チェックＯＫ
                $reseller_info = array();
                $reseller_info = array("reseller_key" => 0,
                                       "reseller_id"  => $admin_id,
                );
                return $reseller_info;
            }else{
                // チェックＮＧ
                $this->logger->info(__FUNCTION__."# Admin Login Error!", __FILE__, __LINE__);
                return null;
            }
        }else{
            // 通常ログインユーザ
            $sql = "select * from " . $this->table . " where".
                " reseller_id = '".addslashes($id)."'".
                " AND reseller_password = '".addslashes($pass)."'".
                " AND reseller_status = 1";
            $reseller_info = $this->getList($sql);
            if (DB::isError($reseller_info)) {
                return $reseller_info;
            }
            if ($reseller_info) {
                $_reseller_info = $reseller_info[0];
                if (($_reseller_info["reseller_id"] == $id) && ($_reseller_info["reseller_password"] == $pass)) {
                    // チェックＯＫ
                    return $_reseller_info;
                } else {
                    // チェックＮＧ
                    $this->logger->warn(__FUNCTION__."#大文字、小文字違い", __FILE__, __LINE__, array(
                        $id,
                        $_reseller_info["reseller_id"],
                        $pass,
                        $reseller_info["reseller_password"],
                    ));
                    return null;
                }
            } else {
                return null;
            }
        }
    }

    /**
     * ハッシュ化したパスワードで認証(MD5)
     */
    function checkLogin_MD5($id, $hash_pass, $admin_id="", $admin_pw="")
    {
$this->logger->info(__FUNCTION__."# Start!", __FILE__, __LINE__, array($this->config->get("RESELLER","admin_id"),$this->config->get("RESELLER","admin_pw")));

        if ($admin_id == $id) {
            // 管理用ログインユーザ
            if (md5($admin_pw) == $hash_pass) {
                // チェックＯＫ
                $reseller_info = array();
                $reseller_info = array("reseller_key" => 0,
                                       "reseller_id"  => $admin_id,
                );
                return $reseller_info;
            }else{
                // チェックＮＧ
                $this->logger->info(__FUNCTION__."# Admin Login Error!", __FILE__, __LINE__);
                return null;
            }
        }else{
            // 通常ログインユーザ
            $sql = "select * from " . $this->table . " where".
                " reseller_id = '".addslashes($id)."'".
                " AND reseller_password = '".addslashes($hash_pass)."'".
                " AND reseller_status = 1";
            $reseller_info = $this->getList($sql);
            if (DB::isError($reseller_info)) {
                return $reseller_info;
            }
            if ($reseller_info) {
                $_reseller_info = $reseller_info[0];
                if (($_reseller_info["reseller_id"] == $id) && (md5($_reseller_info["reseller_password"]) == $hash_pass)) {
                    return $_reseller_info;
                } else {
                    $this->logger->warn(__FUNCTION__."#大文字、小文字違い", __FILE__, __LINE__, array(
                        $id,
                        $_reseller_info["reseller_id"],
                        $hash_pass,
                        $_reseller_info["reseller_password"],
                    ));
                    null;
                }
            } else {
                return null;
            }
        }
    }

    /**
     * ハッシュ化したパスワードで認証(Sha1)
     */
    function checkLogin_Sha1($id, $hash_pass, $admin_id="", $admin_pw="")
    {
        if ($admin_id == $id) {
            // 管理用ログインユーザ
            if (sha1($admin_pw) == $hash_pass) {
                // チェックＯＫ
                $reseller_info = array();
                $reseller_info = array("reseller_key" => 0,
                                       "reseller_id"  => $admin_id,
                );
                return $reseller_info;
            }else{
                // チェックＮＧ
                $this->logger->info(__FUNCTION__."# Admin Login Error!", __FILE__, __LINE__);
                return null;
            }
        }else{
            // 通常ログインユーザ
            $sql = "select * from " . $this->table . " where".
                " reseller_id = '".addslashes($id)."'".
                " AND reseller_password = '".addslashes($hash_pass)."'".
                " AND reseller_status = 1";
            $reseller_info = $this->getList($sql);
            if (DB::isError($reseller_info)) {
                return $reseller_info;
            }
            if ($reseller_info) {
                $_reseller_info = $reseller_info[0];
                if (($_reseller_info["reseller_id"] == $id) && (sha1($_reseller_info["reseller_password"]) == $hash_pass)) {
                    return $_reseller_info;
                } else {
                    $this->logger->warn(__FUNCTION__."#大文字、小文字違い", __FILE__, __LINE__, array(
                        $id,
                        $_reseller_info["reseller_id"],
                        $hash_pass,
                        $_reseller_info["reseller_password"],
                    ));
                    null;
                }
            } else {
                return null;
            }
        }
    }

    function getLatestKey()
    {
            $sql = "select max(reseller_key) from " . $this->table;
            $reseller_info = $this->getList($sql);
            if (DB::isError($reseller_info)) {
                $this->logger->info(__FUNCTION__."# Get Max ResellerKey Error!", __FILE__, __LINE__);
                return $reseller_info;
            }
            return $reseller_info[0]['max(reseller_key)'];
    }

}
