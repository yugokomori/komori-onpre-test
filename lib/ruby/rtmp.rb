require File.dirname(__FILE__) + '/rtmp/net_connection'

class RTMPHack

  def initialize
    @connection_uri = ''
    @connection_args = []
    @method_name = ''
    @method_args = []
  end

  attr_accessor :connection_uri, :connection_args, :method_name, :method_args

  def execute
    nc = RTMP::NetConnection.new

    nc.on_connect do
      log "connected"
      log "try calling '" + @method_name + "' with args:" + @method_args.to_s
      nc.call(@method_name, @method_args) do |res|
        log "calling '" + @method_name + "' succeeded. (returned value:" + res.to_s + ")"
        puts res.to_s
        STDOUT.flush
        nc.disconnect
      end
    end
    nc.on_disconnect do
      log "disconnected"
      exit!
    end
    nc.on_error do |err|
      log "rejected"
      exit!
    end

    nc.connect(@connection_uri, @connection_args)
    sleep 10
    log "aborted"
  end

  private

  def log msg
    if $DEBUG
      t = Time.now
      puts sprintf("%04d-%02d-%02d %02d:%02d:%02d.%03d %s",
                   t.year, t.month, t.day, t.hour, t.min, t.sec, t.usec/1000, msg)
    end
  end
end
