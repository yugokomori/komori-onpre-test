<?php

require_once('AccessLog.class.php');
require_once('Main.php');
require_once('Room.php');
require_once('ex/ExSmarty.class.php');
require_once('ex/Session.class.php');
require_once('Rooms/MenuRoomFactory.class.php');
require_once('config/config.inc.php');
require_once("classes/N2MY_DBI.class.php");
require_once('dbi/room.dbi.php');

function showTop(){
    $logger =& EZLogger::getInstance();
    $logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
    $isContractShare = 0;

    $session = new Session();
    //アドミンログイン情報を消去
    $session->unsetSessionValue('admin_login');

    ////ページ送りの数をセッションとローカル変数に保存(リクエスト、現在閲覧中のページ、0ページ目　の優先順位)
    $current_page_number = $session->getSessionValue('mainmenu_page_number');
    $now_page_number = ($_REQUEST['page']||$_REQUEST['page']=="0")?$_REQUEST['page']:($current_page_number?$current_page_number:0);
    $session->setSessionValue('mainmenu_page_number', $current_page_number);
    $smarty = new ExSmarty($session->getSessionValue('lang'));
    $user_info = $session->getSessionValue('user_info');
    // 部屋一覧取得
    $room_table = new RoomTable($this->get_dsn());
    $rooms = $room_table->get_use_list($user_info['user_key']);
    $r = MenuRoomFactory::build($rooms);

    # --- for next page
    $total_room_count = count($r);
    if($total_room_count > MAINMENU_MAX_VIEW){

        $start_room_number = $now_page_number * MAINMENU_MAX_VIEW + 1;
        $end_room_number = $start_room_number + MAINMENU_MAX_VIEW - 1;




        $smarty->assign('page_header', true);
        $smarty->assign('total_room_count', $total_room_count);
        $smarty->assign('start_room_number', $start_room_number);
        $smarty->assign('end_room_number', $end_room_number);

        $next_page_number = $now_page_number + 1;
        $back_page_number = $now_page_number - 1;



        $smarty->assign('next_page_number', $next_page_number);
        $smarty->assign('back_page_number', $back_page_number);

        if($now_page_number == "0" || !$now_page_number){
            $smarty->assign('page_next', true);
            $smarty->assign('page_back', false);
        }else{
            $smarty->assign('page_back', true);
            if(($now_page_number + 1) * MAINMENU_MAX_VIEW < $total_room_count ){
                $smarty->assign('page_next', true);
            }else{
                $smarty->assign('page_next', false);
            }
        }

        $r = array_slice($r ,$start_room_number-1 , MAINMENU_MAX_VIEW );
    }

    // セミナー契約していない場合のjavascript表示
    $seminar_rooms = $ro->getSeminarRoomKeys();
    if(!$seminar_rooms){
        $no_seminar_plan = "<a href=\"#\" onClick=alert(\"".SEMINAR_ERROR_ALERT."\");>" ;
        $smarty->assign('no_seminar_plan', $no_seminar_plan);
    }else{
        $smarty->assign('no_seminar_plan', "<a href=\"seminar.php\">");
    }

    //会議予約のjavascript表示
    for($i = 0; $i < count($r); $i++){
        $room = new Room();
        $isReservation =  $room->isReservationRoomStatus( $r[$i]['room_key'] );
        $r[$i]['isReserve'] = $isReservation;
        if($r[$i]['is_share']) $isContractShare = 1;
    }

    $smarty->assign('name', $r);
    $smarty->assign('isContractShare', $isContractShare);
    $smarty->display('user/mainmenu.t.html');
    $logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);

}

function showAdminLogin($message = ""){

    $smarty = new ExSmarty($this->session->get("lang"));
    if($message){
        $smarty->assign('message', $message);
    }

    $smarty->display('admin/admin_login.t.html');

}


function showAdminTop(){
    $main = new Main();
    if($message = $main->checkAdminLoginInfo()){
        showAdminLogin($message);
        exit;
    }

    $accessLog = new AccessLog();
    $accessLog->access('admin_login');
    header("Location: /services/admin/");

}

