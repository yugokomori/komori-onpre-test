<?php
#-----------------------------------------------------------
$max_client     = 150;    // Apache max client
$percent_warn   = 10;      // Warning percent
$percent_alert  = 20;     // Alert percent
$interval       = 300;    // mail notify interval (sec)
$hostname       = "meeting1.gs1.vcube.co.jp";
$send_from      = "kiyomizu@vcube.co.jp";
$send_to_warn   = "kiyomizu@vcube.co.jp";
$send_to_alert  = "kiyomizu@vcube.co.jp";
#-----------------------------------------------------------

class ApacheStatus {
var $fp = null;
var $hostname;
var $send_from;

    function __construct($hostname, $from, $to) {
        $this->dir = dirname(__FILE__).'/';
        $this->fp = fopen($this->dir.date("Ymd"), "a+");
        $this->hostname = $hostname;
        $this->send_from = $from;
        $this->send_to_alert = $to;
    }

    function log($msg) {
        fwrite($this->fp, $msg."\n");
    }

    function sendmail($subject, $msg, $send_to) {
        $to      = $send_to;
        $message = $msg;
        $headers = 'From: ' . $this->send_from . "\r\n" .
            'Reply-To: ' . $this->send_from . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        mail($to, $subject, $message, $headers);
    }

    function error($msg) {
        $subject = '[Apache status ALERT] '.date("Y-m-d H:i:s").' '.$this->hostname;
//        $this->sendmail($subject, $msg, $this->send_to_alert);
        $this->log($this->netstat);
        exit();
    }

    function curl($url) {
        $ch = curl_init();
        $option = array(
            CURLOPT_URL => $url,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => 10,
            );
        curl_setopt_array($ch, $option);
        $contents = curl_exec($ch);
        return $contents;
    }

    function netstat() {
        return $netstat = `netstat -a -p`;
    }

    function get_status() {
        if (!$content = $this->curl("http://".$this->hostname."/server-status?auto")) {
            $this->error();
            return false;
        }
        return $content;
    }

    function get_full_status() {
        if (!$content = $this->curl("http://".$this->hostname."/server-status")) {
            $this->error();
            return false;
        }
        return strip_tags($content);
    }

    function __destruct() {
        if ($this->fp) {
            @fclose($this->fp);
        }
    }
}

$objApacheStatsu = new ApacheStatus($hostname, $sent_from, $send_to_alert);
$tcpdump_flg = $objApacheStatsu->dir.'tcpdump_flg';
$content = $objApacheStatsu->get_status();
// Get busy status number
preg_match('/^Scoreboard:.*$/m', $content, $matches);
$busyNum = substr_count($matches[0], "R");
$busyNum += substr_count($matches[0], "W");
// limit over
//$busyNum = 100;
if ($percent_warn < ($busyNum / $max_client * 100)) {
    // alert mail
    if ($percent_alert < ($busyNum / $max_client * 100)) {
        // send mail
        $subject = '[Apache status] '.$objApacheStatsu->hostname . ' - ALERT';
        $file = $objApacheStatsu->dir."alert_flg";
        $send_to = $send_to_alert;
        // pid
        if (!file_exists($tcpdump_flg)) {
            $pid = shell_exec('/usr/sbin/tcpdump -w /tmp/tcpdump > /dev/null & echo $!');
            $objApacheStatsu->log($pid);
            if (is_numeric(trim($pid))) {
                file_put_contents($tcpdump_flg, $pid);
            }
        }
    // warning mail
    } else {
        // send mail
        $subject = '[Apache status] '.$objApacheStatsu->hostname . ' - WATCH';
        $file = $objApacheStatsu->dir."warn_flg";
        $send_to = $send_to_warn;
    }
    if (!file_exists($file) || ($interval < (time() - filemtime($file)))) {
        touch($file);
        $objApacheStatsu->sendmail($subject, $content, $send_to);
    }
    // output log
    $msg = "=================================================================\n";
    $msg .= date("Y-m-d")." ".$objApacheStatsu->hostname."\n";
    $msg .= $objApacheStatsu->get_full_status($hostname);
    $msg .= "----------------------------------------------------------------\n";
    $msg .= $objApacheStatsu->netstat($hostname);
    $msg .= "=================================================================\n";
    $msg .= "\n\n";
    $objApacheStatsu->log($msg);
} else {
    // recovery
    if (file_exists($objApacheStatsu->dir."alert_flg")) {
        $recovery_file = $objApacheStatsu->dir."revovery_flg";
        // ファイルが出来てから5分経過したら復旧のメールを投げる
        if (file_exists($recovery_file)) {
            if (10 < (time() - filemtime($recovery_file))) {
                $objApacheStatsu->log('Recovery');
                $subject = '[Apache status] '.$objApacheStatsu->hostname . ' - RECOVERY';
                $objApacheStatsu->sendmail($subject, $content, $send_to_alert);
                if ($pid = file_get_contents($tcpdump_flg)) {
                    exec("kill $pid");
                }
                unlink($recovery_file);
                unlink($objApacheStatsu->dir."alert_flg");
                unlink($tcpdump_flg);
            } else {
                $objApacheStatsu->log(time() - filemtime($recovery_file));
            }
        } else {
            touch($recovery_file);
        }
    }
    $objApacheStatsu->log(date("Y-m-d H:i:s")." ".$busyNum." / " . $max_client);
}
?>
