# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/bin
export JAVA_HOME=/usr/local/jdk1.6.0_30
export PATH=$PATH:$JAVA_HOME/bin

export PATH
unset USERNAME
