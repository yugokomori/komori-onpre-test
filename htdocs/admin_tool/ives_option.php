<?php

require_once("classes/AppFrame.class.php");
require_once("classes/dbi/ives_setting.dbi.php");
require_once("classes/N2MY_IvesClient.class.php");
require_once("classes/mcu/config/McuConfigProxy.php");

class IvesOption extends AppFrame {

      function init() {
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $this->dsn = $this->get_dsn();
        define("WSDL_DOMAIN", "mcu01.nice2meet.us:8080");
        $this->configProxy 	= new McuConfigProxy();
      }

      /**
       * 状態変更
       */
    function default_view($message = "") {
        print $this->_header();

        $rooms = array (
            array ("room_name" => "hq1 ()", "room_key" => ""),
            array ("room_name" => "hq2 ()", "room_key" => ""),
        );

        if ($message['error']) {
            print $message['error'];
            print "<br />";
        }

        print "<table border='0' cellspacing='0' cellpadding='5'>";
        foreach ($rooms as $room) {
            $ivesSettingTable = new IvesSettingTable($this->dsn);
            $res = $ivesSettingTable->findByRoomKey($room["room_key"]);
            print "<tr><th>room_key</th>";
            print "<th>Confid</th>";
            print "<th>Did</th></tr>";
            print "<tr><td>".$res['room_key']."</td>";
            print "<td>".$res['ives_conference_id']."</td>";
            print "<td>".$res['ives_did']."</td>";
            print "</tr>";
        }
        print "</table>";

print <<<EOM
<form action="/admin_tool/ives_option.php" name="option" method="post">
<input type="hidden" name="action_chenge_conference" value="">
<table border="0" cellspacing="0" cellpadding="5">
EOM;
print "<tr><th>.$rooms[0]['room_name'].</th>";
print "<td><input value='Reset' onclick='change(".$rooms[0]['room_key'].");' type='button' class='icon i-go' /></td></tr>";
print "<tr><th>.$rooms[1]['room_name'].</th>";
print "<td><input value='Reset' onclick='change(".$rooms[1]['room_key'].");' type='button' class='icon i-go' /></td></tr>";
print <<<EOM
</table>
</form>
EOM;

        print $this->_footer();
    }

    function action_chenge_conference () {
        $room_key = $this->request->get("room_key");
        $result = $this->action_remove_conference($room_key);
        $result_add = $this->action_create_conference($room_key);
        $message = array("error" => $result["error"].$result_add["error"],
                         "data"  => $result_add["data"]);
        $this->default_view($message);
    }

    //{{{action_create_conferen ce
    private function action_create_conference($roomKey)
    {
        $result = array();
        try {
            $dsn = $this->dsn;
            $ivesSettingTable = new IvesSettingTable($dsn);

            $res = $ivesSettingTable->findByRoomKey($roomKey);

            if (PEAR::isError($res)) {
                throw new Exception("select ives_setting room_key failed PEAR said : ".$roomKey);
            }

            $ivesClient = new N2MY_IvesClient($dsn);
            $res        = $res[0];

            if ($res && $res["ives_did"] && $res["ives_conference_id"]) {
                $conf = $ivesClient->callGetConference($res["ives_conference_id"]);

                $this->logger2->info($conf);

                if ($conf->return) {
                    $result["error"] = "Already Created conference";
                    return $result;
                    exit;
                } else {
                    $where = "room_key = '".$roomKey."' AND ives_did = '".$res["ives_did"]."'";
                    $data  = array(
                        "is_deleted   " => 1
                    );
                    $res = $ivesSettingTable->update($data, $where);
                    if (PEAR::isError($res)) {
                        throw new Exception("update ives_setting room_key failed PEAR said : ".$roomKey);
                    }
                }
            }

            $did = $ivesClient->createDid($ivesSettingTable);
            $ivesClient->registerConferenceMngrListener();

            $conf = $ivesClient->callCreateConferenceExt($did);

            if ( isset($conf->return) ) {
                $conf         = $conf->return;
                $confId       = $conf->id;

                $data = array(
                    'room_key'           => $roomKey,
                    'ives_did'           => $did,
                    'ives_conference_id' => $confId,
//                    'client_id'          => 50863643,
//                    'client_pw'          => cubev2011,
                    'client_id'          => $this->configProxy->get("client_id"),
                    'client_pw'          => $this->configProxy->get("client_pw"),
                    'startdate'          => date("Y-m-d H:i:s"),
                    'registtime'         => date("Y-m-d H:i:s")
                );

                $res = $ivesSettingTable->add($data);

                if (PEAR::isError($res)) {
                    throw new Exception("update room failed PEAR said : ".$roomKey);
                }

                $params = array(
                        "confId"   => $confId,
                        "compType" => 0,
                        "size"     => 1 );
                $mosaic = $ivesClient->callCreateMosaic($params);
            } else {
                $this->logger2->info("could not create conference");
            }

            if ( isset($mosaic->return) ) {
                $this->logger2->info("<p> created mosaic: " . $mosaic->return . " </p>");
            } else {
                $this->logger2->info("<p> mosaic was not created <p>");
            }

            $mosaic = null;
            if ( isset($conf) ) {
                $mosaic = $ivesClient->callCreateMosaic($params);
            }

            if ( isset( $mosaic->return ) ) {
                $this->logger2->info("<p> created second mosaic " . $mosaic->return . " </p>");
            } else {
                $this->logger2->info("<p>Second mosaic was not created <p>");
            }
            $result["data"] = $data;
            return $result;
        } catch (SoapFault $e) {
            $this->logger->warn(__FUNCTION__."#soap fault",__FILE__,__LINE__);
            return "Create Error MCU Conference";
            exit;
        } catch (Exception $e) {
            $this->logger->warn(__FUNCTION__."#exception",__FILE__,__LINE__);
            return "Create Error MCU Conference";
            exit;
        }
    }
    //}}}
      //{{{action_remove_conferen ce
    private function action_remove_conference($roomKey)
    {
        try {
            $dsn = $this->dsn;
            $ivesSettingTable = new IvesSettingTable($dsn);

            $this->logger2->info($roomKey);
            $res = $ivesSettingTable->findByRoomKey($roomKey);
            $this->logger2->info($res);
            if (PEAR::isError($res) || empty($res[0]["ives_did"]) || empty($res[0]["ives_conference_id"])) {
                throw new Exception("select ives_setting room_key failed PEAR said : ".$roomKey);
            }

            $ivesSettingList = $res[0];
            $did          = $ivesSettingList["ives_did"];
            $confId       = $ivesSettingList["ives_conference_id"];

            $this->logger2->info($did);
            $this->logger2->info($confId);

            $where = "room_key = '".$roomKey."'";
            $data  = array(
                "is_deleted" => 1
            );
            $res = $ivesSettingTable->update($data, $where);

            if (PEAR::isError($res)) {
                throw new Exception("ives setting update room failed : ".$roomKey);
            }

            $ivesClient = new N2MY_IvesClient($dsn);

            // remove
            $this->logger2->info("-------soap api new--------");

            $conf = $ivesClient->callGetConference($confId);
            if (!$conf->return) {
                $result["error"] = "Not Found MCU conference";
                return $result;
                exit;
            }
            $ivesClient->callRemoveConference($confId);
        } catch (SoapFault $e) {
            $this->logger->warn(__FUNCTION__."#soap fault",__FILE__,__LINE__);
            $result["error"] = "MCU conference Delete Error";
            return $result;
            exit;
        } catch (Exception $e) {
            $this->logger->warn(__FUNCTION__."#exception",__FILE__,__LINE__);
            $result["error"] = "MCU conference Delete Error";
            return $result;
            exit;
        }
    }
    function _header() {
        return <<<EOM
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Reset Conference</title>
<script type="text/javascript">
function change(room_key) {
    location.href="?action_chenge_conference=&room_key="+room_key;
}
</script>
<style TYPE="text/css">
<!--
body {
text-align: center;
font-size: 12px;
font-family: "Arial, sans-serif, ＭＳ Ｐゴシック", Osaka, "ヒラギノ角ゴ Pro W3" ;
margin: 10px 0px 0px 0px;
}
.title {
font-size: 16px;
font-weight: bold;
margin: 20px auto 0px auto;
width: 760px
}
#content {
text-align: center;
}
#content table {
border-collapse: collapse;
border-spacing: 0;
empty-cells: show;
border: 1px solid #999999;
margin: 20px auto;
}
#content th {
border: 1px solid #999999;
padding: 4px 5px;
background-color: #EFEFEF;
text-align: left;
}
#content td {
border: 1px solid #999999;
padding: 4px 10px;
text-align: left;
}
.errorSection {
color: #FF0000;
}
-->
</style>
</head>
<body>
<div id="content">
<h1>Reset Conference</h1>
EOM;
    }

    function _footer() {
       print "</body></html>";
    }
}
$main = new IvesOption();
$main->execute();
?>