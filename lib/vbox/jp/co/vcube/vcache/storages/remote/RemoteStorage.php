<?php

/**
 * @author Yuki Asano <y-asano@vcube.co.jp>
 * @copyright Vcube, Inc. All Rights Reserved.
 * @version 0.1
 */

require_once( "jp/co/vcube/vcache/core/AbstractStorage.php" );

abstract class RemoteStorage extends AbstractStorage
{

  /**
   * $this filepath copy to $target filepath.
   * @param $target Object - Storage instance that inherited AbstractStorage.
   */
  public function copyTo( $target ) { }

  /**
  * $this filepath copy to $target filepath.
  * @param $target Object - Storage instance that inherited AbstractStorage.
  */
  public function uploadTo( $target ) { }

  /**
  * $this filepath copy to $target filepath.
  * @param $target Object - Storage instance that inherited AbstractStorage.
  */
  public function dowonloadTo( $target ) { }

  /**
   * $this filepath move to $target filepath.
   * @param $target Object - Storage instance that inherited AbstractStorage.
   */
  public function moveTo( $target ) { }

  /**
   * Create specified directory.
   */
  public function makeDir() { }

  /**
   * Return the exists filelist array without dot-files(`.` and `..`).
   */
  public function filelist() { }

  /**
   * Delete a file.
   */
  public function delete() { }

  /**
   * Verify a file exists.
   */
  public function exists() { }

}
