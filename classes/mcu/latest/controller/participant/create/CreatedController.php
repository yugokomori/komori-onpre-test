<?php
require_once("classes/mcu/config/McuConfigProxy.php");
require_once("classes/mcu/model/gateway/McuGatewayProxy.php");

require_once(dirname(__FILE__)."/../../../controller/participant/create/client/vcube/flash/FlashParticipant.php");
require_once(dirname(__FILE__)."/../../../controller/participant/create/client/vcube/flash/MobileParticipant.php");
require_once(dirname(__FILE__)."/../../../controller/participant/create/client/vcube/viewer/FlashViewer.php");
require_once(dirname(__FILE__)."/../../../controller/participant/create/client/vcube/viewer/MobileViewer.php");
require_once(dirname(__FILE__)."/../../../controller/participant/create/client/video/CalledVideoParticipant.php");
require_once(dirname(__FILE__)."/../../../controller/participant/create/client/video/VideoParticipantFromTemporaryDid.php");
require_once(dirname(__FILE__)."/../../../controller/participant/create/client/video/VideoParticipant.php");
require_once(dirname(__FILE__)."/../../../model/do/State.php");

require_once(dirname(__FILE__)."/../../../model/MCUControllerProxy.php");
require_once(dirname(__FILE__)."/../../../model/ConferenceProxy.php");

require_once(dirname(__FILE__)."/../../../params/GetConferenceParams.php");


class CreatedController {
	static function create($model) {
sleep(1);
		$configProxy 	= new McuConfigProxy();
		$dsn = $configProxy->getDsn();
		$logger = EZLogger2::getInstance();

		$conferenceProxy	= new ConferenceProxy();
		/* conferenceの詳細情報を取得 */
		$mcuControllerProxy	= new MCUControllerProxy();
		$obj = new GetConferenceParams(array("confId"=>$model->confId));
		$conferenceInfo = $mcuControllerProxy->getConference($obj->getParams());
		if (!$conferenceInfo &&
			($participant = $conferenceProxy->existCalleeParticipant($model->confId, $model->partName))) {
			$conferenceProxy->voidCalleeParticipant($participant);
			throw new Exception("conference is already destroyed. confId: ".$model->confId);
		}

		$model->setDid($conferenceInfo["did"]);
		$logger->info($model);
		
		/**/
		$gatewayProxy = new McuGatewayProxy();
		$mcuServerModel = $gatewayProxy->getMcuServerByServerIp($_SERVER["REMOTE_ADDR"]);

		$result = split("@", $model->partName);
		if ($model->partType == "SIP") {
			if (($mcuServerModel["guest_domain"] && $mcuServerModel["guest_domain"] == $result[1])
					|| ($configProxy->get("videoParticipantGuestDomain") == $result[1])) {
				return new VideoParticipantFromTemporaryDid($model);
			}
			else if ($mcuServerModel["server_address"] == $result[1]
					|| ($configProxy->get("videoParticipantDomain") == $result[1])) {
				return new VideoParticipant($model);
			}
			// call out.
			else
			if ($conferenceProxy->isCalleeParticipant($model->confId, $model->did, $model->partName)) {
				return new CalledVideoParticipant($model);
			}
			else {
				throw new Exception("unknown client type. ".$model);
			}
		}
		else if ($model->partType == "WEB") {
			// view用のPariticipant　判別
			if ($result[0] == $configProxy->get("defaultViewId")) {
				return new FlashViewer($model);
			}
			else if ($result[0] == $configProxy->get("smallId")) {	// mobile?
				return new MobileParticipant($model);
			}
			else {
				return new FlashParticipant($model);
			}
		}
		else {
			throw new Exception("unknown client type.".$model);
		}
	}
}
