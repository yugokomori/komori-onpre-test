<?php

require_once("classes/N2MY_DBI.class.php");

class DBI_MemberUsageDetails extends N2MY_DB
{
    public $table = "member_usage_details";
    //protected $primary_key = "datacenter_key";
    protected $columns;

    function __construct( $dsn ) {
        $this->init( $dsn, $this->table );
        $this->columns = $this->getTableInfo('data');
    }

    function getSummaryUseCount($user_key, $condition = array()) {
        $sql = 'SELECT SUM(use_count) AS use_count' .
            " FROM ".$this->table;
        if (!$user_key) {
            $this->logger2->warn($user_key, 'user_key not found!!');
            return 0;
        } else {
            $condition['user_key'] = $user_key;
        }
        $where = $this->getWhere($this->columns, $condition);
        if ($where) {
            $sql .= ' WHERE '.$where;
        }
        $count = $this->_conn->getOne($sql);
        if ($count) {
            return $count;
        } else {
            return 0;
        }
    }
}
