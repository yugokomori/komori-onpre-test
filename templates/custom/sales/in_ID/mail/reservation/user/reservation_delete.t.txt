Ini hanya email transmisi, mohon untuk tidak membalasnya

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
           Pemberitahuan Pembatalan dari V-CUBE Sales & Support
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
    {$info.sender}  telah membatalkan sebuah pertemuan menggunakan "V-CUBE Sales & Support".
Pertemuan berikut telah dibatalkan:
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

　■ Topik
　　 {$info.reservation_name}

　■ Tanggal dan Waktu
　　 {$guest.starttime}
　　 〜 {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

　■ Pesan dari {$info.sender}
　　 {$info.mail_body}


■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　  About V-CUBE Sales&Support
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■

V-CUBE Sales & Support adalah sebuah sistem konferensi web video berbasis udara. Dengan mengakses alamat tertentu, para pengguna dapat melakukan komunikasi tatap muka menggunakan video dan audio.
There are many tools for communication such as whiteboard, recording, and PC desktop sharing feature.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Sales & Support
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■Dikembangkan dan Dikelola oleh:
V-cube, Inc. {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Pusat Layanan Pelanggan (Khusus di Jepang)
Tel: 0570-002192 (Open 24/7)
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if}
■Pusat Layanan Pelanggan (Bahasa Inggris & Lainnya)
Tel:
　Tokyo　+81-3-4560-1287
　Malaysia　+60-3-7724-9693
　Singapore　+65-3158-2832
　China　 +86-4006-618-2360
E-mail：{if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if}
Waktu Layanan：Tiap hari kerja 9:00-18:00(GMT+8)
Layanan tidak tersedia selama hari kerja dan hari libur di Malaysia
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
