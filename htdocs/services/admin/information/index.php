<?php

require_once ('classes/AppFrame.class.php');
require_once("classes/N2MY_Account.class.php");
require_once ('classes/dbi/news.dbi.php');
require_once ('classes/dbi/news_cabinet.dbi.php');
require_once ('config/config.inc.php');
require_once( "lib/EZLib/EZCore/EZLogger2.class.php" );
require_once "lib/EZLib/EZUtil/EZLanguage.class.php";

class AppAdminInformation extends AppFrame
{
    var $logger = null;
    var $_name_space = null;
    var $news_obj = null;
    var $newsCabinet_obj = null;

    function init() {
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        //$this->objUser          = new UserTable($this->get_dsn());
        $this->news_obj   = new NewsTable(N2MY_MDB_DSN);
        $this->newsCabinet_obj   = new NewsCabinetTable(N2MY_MDB_DSN);
        $this->_name_space      = md5(__FILE__);
    }

    /**
     * ログイン認証
     */
    function auth() {
        $this->checkAuth();
        $this->checkAdminAuth('admin/session_error.t.html');
    }

    /**
     * デフォルト処理
     */
    function default_view() {
        $this->action_top();
    }

    /**
     * top画面
     */
    function action_top() {
        $this->display('admin/member/index.t.html');
    }

    function action_information_edit() {
    	if($this->request->get("err")) {
    		$err_message = "アップロード可能なファイルサイズは100MBまでになります。";
    		$this->template->assign('message', $err_message );
    	}
        if($this->request->get("new")) {
            $this->session->set("news", "" );
        }
        if(!$news = $this->session->get("news")) {
            $news_lang = $this->request->get("lang")? $this->request->get("lang") : "";
            if(!$news_lang) {
            	$news_lang = $this->session->get("news_lang")? $this->session->get("news_lang") : "ja";
            } else {
                $this->session->set("news_lang", $news_lang );
            }
            //インフォメーション内容を取る
            $news_lang = EZLanguage::getLangCd($news_lang);
            $this->logger2->info($news_lang);
            $where = "news_lang = '".($news_lang?$news_lang:"ja_JP")."' AND news_delete = 0";
            $news =$this->news_obj->getRow($where, null, array("news_key" => "desc"));
            if(!$news) {
            $news["news_lang"] = $news_lang?$news_lang:"ja_JP";
            for($i = 0; $i < 5; $i++)
                $news["news_cabinet"][$i] = array();
            } else {
                //ファイルリス
                $news["news_cabinet"] = $this->newsCabinet_obj->get_news_cabinet_list($news["news_key"]);
            }
        }
        
        $this->template->assign('news', $news );
        $this->template->assign('news_cabinet', $news["news_cabinet"] );
        $this->display('admin/information/edit.t.html');
    }

    function action_information_confirm() {
    	if($_SERVER["CONTENT_LENGTH"]/(1024*1024) >= 501) {
    		$url = "?action_information_edit&err=1";
    		header("Location: ".$url);  
    		return ;
    	}
    	if (!$this->request->get("post_flag")) {
    		$url = "?action_information_edit";
    		header("Location: ".$url);
    		return ;
    	}
    	//$this->logger2->info($_SERVER["CONTENT_LENGTH"]);
        $news = array(
                "news_key" => $this->request->get("news_key"),
                "news_lang" => $this->request->get("news_lang"),
                "news_date" => date('Y-m-d'),
                "news_title" => $this->request->get("news_title"),
                "news_contents" => $this->request->get("news_contents"),
            ); 
        $fileId = $this->request->get("fileId");
        $fileTmpName = $this->request->get("fileTmpName");
        $fileDelete = $this->request->get("fileDelete");
        $fileType = $this->request->get("fileType");
        $filePath = $this->request->get("filePath");
        $fileUpdate = $this->request->get("fileUpdate");
        $fileDelete = $this->request->get("fileDelete");
        $fileName = $this->request->get("fileName");
        $file = $this->request->get("file");
        //$this->logger2->info($_FILES['file']);
        for($i = 0; $i < 5; $i++) {
            $news["news_cabinet"][$i]["news_cabinet_key"] = $fileId[$i];
            $news["news_cabinet"][$i]["tmp_name"] = $fileTmpName[$i];
            $news["news_cabinet"][$i]["file_name"] = $fileName[$i];
            $news["news_cabinet"][$i]["type"] = $fileType[$i];
            $news["news_cabinet"][$i]["size"] = $fileSize[$i];
            $news["news_cabinet"][$i]["file_path"] = $filePath[$i];
            $news["news_cabinet"][$i]["update_flg"] = $fileUpdate[$i];
            $news["news_cabinet"][$i]["delete_flg"] = $fileDelete[$i];
            if(!$_FILES['file']["error"][$i]) {
                $news["news_cabinet"][$i]["type"] = $_FILES['file']["type"][$i];
                $news["news_cabinet"][$i]["size"] = $_FILES['file']["size"][$i];
                //一応tempディレクトリに保存
                $news["news_cabinet"][$i]["file_path"] = "files/tmp/".uniqid().".".pathinfo($_FILES['file']["name"][$i], PATHINFO_EXTENSION);
                if(!is_dir(N2MY_DOCUMENT_ROOT."files/tmp/")) {
                    mkdir(N2MY_DOCUMENT_ROOT."files/tmp/", 0777);  
                }
                move_uploaded_file($_FILES['file']["tmp_name"][$i],N2MY_DOCUMENT_ROOT.$news["news_cabinet"][$i]["file_path"]);
                //$this->logger2->info($news["news_cabinet"][$i]);
                $news["news_cabinet"][$i]["update_flg"] = 1;
            } else if ($_FILES['file']["error"][$i] == 1) {
            	$err_message = "アップロード可能なファイルサイズは100MBまでになります。";
            	$news["news_cabinet"][$i]["tmp_name"] = "";
            	if(!$news["news_cabinet"][$i]["news_cabinet_key"]) {
            		$news["news_cabinet"][$i]["file_name"] = "";
            	}
            }
        }
        //$this->logger2->info($news);
        $this->session->set("news", $news );
        $this->template->assign('news', $news );
        $this->template->assign('news_cabinet', $news["news_cabinet"] );
        if($err_message) {
        	$this->template->assign('message', $err_message );
        	$this->display('admin/information/edit.t.html');
        	return ;
        }
        $this->display('admin/information/confirm.t.html');
    }

    function action_information_complete() {
        if(!$news = $this->session->get("news")) {
            $message = "更新失敗です。もう一度編集ください。";
        }
        $news_key = $news["news_key"];
        if($news_key) {
            //更新
            $where = "news_key = '".$news_key."'";
            $data = array(
                "news_date" => $news["news_date"],
                "news_title" => $news["news_title"],
                "news_contents" => $news["news_contents"],
                );
            $ret = $this->news_obj->update($data,$where);
            if (DB::isError($ret)) {
                $this->logger2->error($ret->getUserInfo());
                $message = "更新失敗でした。もう一度編集ください。";
            }
        }else {
            //追加
            $data = array(
                "news_lang" => $news["news_lang"],
                "news_date" => $news["news_date"],
                "news_title" => $news["news_title"],
                "news_contents" => $news["news_contents"],
                "news_show" => 1,
                );
            $ret = $this->news_obj->add($data);
            if (DB::isError($ret)) {
                $this->logger2->error($ret->getUserInfo());
                $message = "更新失敗でした。もう一度編集ください。";
            } else {
                $where = "news_lang = '".$news["news_lang"]."' AND news_delete = 0";
                $news =$this->news_obj->getRow($where, null, array("news_key" => "desc"));
                //$this->logger2->info($news);
                $news_key = $news["news_key"];
            }
        }
        //ファイル更新
        for($i = 0; $i < 5; $i++) {
            if($news["news_cabinet"][$i]["news_cabinet_key"]) {
                $this->newsCabinet_obj->update_news_cabinet($news["news_cabinet"][$i]);
            } else if ($news["news_cabinet"][$i]["file_name"]) {
                 $this->newsCabinet_obj->add_news_cabinet($news["news_cabinet"][$i],$news_key);
            }
        }
        //tmpファイルを削除
        foreach(glob(N2MY_DOCUMENT_ROOT."files/tmp/".'*.*') as $v){
            unlink($v);
        }
        $this->template->assign('message', $message );
        $this->display('admin/information/complete.t.html');
    }

    function action_download_file() {
        $session_id = $this->request->get("id");
        $this->newsCabinet_obj->download_news_cabinet($session_id);
    }
}

$main = new AppAdminInformation();
$main->execute();
