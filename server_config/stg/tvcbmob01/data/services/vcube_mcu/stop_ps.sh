#! /bin/sh

HOSTNAME="`hostname`"
LOGFILE="/var/log/${HOSTNAME}.log"
SCRIPT_NAME="`basename $0`"
PS_CONTROL_SCRIPT_DIR="/etc/init.d"
PS_NAME=${1}
PS_FILE="${PS_CONTROL_SCRIPT_DIR}/${PS_NAME}"
PS_CONTROL_SCRIPT_SLEEP_SUFFIX=".sleep"
PS_CONTROL_SCRIPT_BACKUP_SUFFIX=".stop_ps_backup"
FLG1=${2}
RC=0

LOG()
{
    STATUS=$1
    MSG=$2
    echo [`date '+%Y/%m/%d %T'`] [${STATUS}] "${HOSTNAME}" "${SCRIPT_NAME}" "${MSG}" | tee -a ${LOGFILE}
}

case ${PS_NAME} in
    mcuWeb|mediaserver|opensips|dnsmasq)
        case ${FLG1} in
            on)
                if [ -e ${PS_FILE}${PS_CONTROL_SCRIPT_BACKUP_SUFFIX} ];then 
                    LOG "warn" "\"It has been already stopped.\""
                    RC=1
                else
                    mv ${PS_FILE} ${PS_FILE}${PS_CONTROL_SCRIPT_BACKUP_SUFFIX}
                    mv ${PS_FILE}${PS_CONTROL_SCRIPT_SLEEP_SUFFIX} ${PS_FILE}
                    ${PS_FILE}${PS_CONTROL_SCRIPT_BACKUP_SUFFIX} stop
                fi
                ;;
            off)
                if [ -e ${PS_FILE}${PS_CONTROL_SCRIPT_SLEEP_SUFFIX} ];then 
                    LOG "warn" "\"It has been already started.\""
                    RC=1
                else
                    mv ${PS_FILE} ${PS_FILE}${PS_CONTROL_SCRIPT_SLEEP_SUFFIX}
                    mv ${PS_FILE}${PS_CONTROL_SCRIPT_BACKUP_SUFFIX} ${PS_FILE}
                fi
                ;;
            *)
                echo $"Usage: $0 {mcuWeb|mediaserver|opensips|dnsmasq} {on|off}"
                RC=2
                ;;
        esac
        ;;
    *)
        echo $"Usage: $0 {mcuWeb|mediaserver|opensips|dnsmasq} {on|off}"
        RC=2
        ;;
esac

exit ${RC}
