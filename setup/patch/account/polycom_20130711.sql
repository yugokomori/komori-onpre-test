ALTER TABLE  `mcu_server` DROP  `prefix_number`;

ALTER TABLE  `mcu_server` ADD  `number_domain` VARCHAR( 255 ) NOT NULL AFTER  `server_address` ,
ADD  `guest_domain` VARCHAR( 255 ) NOT NULL AFTER  `number_domain` ,
ADD  `guest_number_domain` VARCHAR( 255 ) NOT NULL AFTER  `guest_domain` ,
ADD  `guest_h323_domain` VARCHAR( 255 ) NOT NULL AFTER  `guest_number_domain` ,
ADD  `guest_h323_number_domain` VARCHAR( 255 ) NOT NULL AFTER  `guest_h323_domain` ,
ADD  `ip` VARCHAR( 255 ) NOT NULL AFTER  `guest_h323_number_domain` ,
ADD INDEX ( ip ),
ADD  `controller_port` INT( 10 ) NOT NULL DEFAULT  '8080' AFTER  `ip` ,
ADD  `socket_port` INT( 10 ) NOT NULL DEFAULT  '1500' AFTER  `controller_port` ,
ADD  `sip_proxy_port` INT( 10 ) NOT NULL DEFAULT  '5060' AFTER  `socket_port` ,
ADD INDEX ( ip );

CREATE TABLE IF NOT EXISTS `media_mixer` (
  `media_mixer_key` int(11) NOT NULL,
  `mcu_server_key` int(10) NOT NULL,
  `media_name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `media_ip` varchar(255) NOT NULL,
  `local_net` varchar(255) NOT NULL,
  `public_ip` varchar(255) NOT NULL,
  `mobile_mix` int(1) NOT NULL,
  `is_available` int(1) NOT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  PRIMARY KEY (`media_mixer_key`),
  KEY `mcu_server_key` (`mcu_server_key`,`is_available`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;