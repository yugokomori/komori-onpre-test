<?php
require_once "classes/AppFrame.class.php";
require_once "classes/dbi/user.dbi.php";
require_once "classes/dbi/meeting.dbi.php";
require_once "lib/EZLib/EZHTML/EZValidator.class.php";

class AppMeetingLayout extends AppFrame {

    function default_view() {
        $request = $this->request->getAll();
        $this->logger2->debug($request);
        $validator = new EZValidator($request);
        $rules = array(
            "seat" => array(
                "allow" => array('20'),
                ),
            "type" => array(
                "allow" => array('normal', 'audience', 'multicamera', 'multicamera_audience', 'whiteboard', 'whiteboard_audience'),
                ),
            "display_size" => array(
                "allow" => array('4to3', '16to9'),
                ),
        );
        foreach($rules as $field => $rule) {
            $validator->check($field, $rule);
        }
        // とりあえず警告
        if (EZValidator::isError($validator)) {
            $this->logger2->error($this->get_error_info($validator));
            return false;
        }
        $obj_User       = new UserTable($this->get_dsn());
        $obj_Meeting    = new MeetingTable($this->get_dsn());
        $meeting_key    = $this->session->get('meeting_key');
        $meeting_info   = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND is_deleted = 0", $meeting_key ) );
        $room_info      = $this->get_room_info($meeting_info["room_key"]);
        $user_info      = $obj_User->getRow("user_key = '".$meeting_info["user_key"]."'");
        $this->logger2->debug(array($user_info, $room_info, $meeting_info));
        $this->template->assign('user_info', $user_info);
        $this->template->assign('room_info', $room_info);
        $this->template->assign('meeting_info', $meeting_info);
        $this->display("core/meeting/layout/".$request["type"]."/layout".$request["seat"]."_".$request["display_size"].".t.xml");
    }
}
$main = new AppMeetingLayout();
$main->execute();
?>
