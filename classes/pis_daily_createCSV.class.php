#!/usr/local/bin/php
<?php
set_time_limit ( 0 );
ini_set ( "memory_limit", "2048M" );
require_once ("lib/EZLib/EZUtil/EZCsv.class.php");

class AppDailyCsvClass {

	// 基本情報の配列情報
	var $index_member_id = 0;
	var $index_type = 1;
	var $index_name = 2;
	var $index_name_furi = 3;
	var $index_mail = 6;

	// 元籍情報の配列情報
	var $index_company_code = 1;
	var $index_company_name = 2;
	var $index_classification = 9;
	var $index_accounting_unit_code = 10;
	var $index_expense_department_code = 12;

	// ID変換情報ファイルの配列情報
	var $index_henkan_id = 0;
	var $index_henkan_global_id = 2;

	// ID基本情報ファイルの配列情報
	var $index_gid_kihon_global_id = 0;
	var $index_gid_kihon_name = 2;
	var $index_gid_kihon_name_furi = 3;
	var $index_gid_kihon_mail = 6;

	// 所属情報ファイルの配列情報
	var $index_gid_syozoku_company_code = 2;
	var $index_gid_syozoku_company_name = 3;
	var $index_gid_syozoku_kessan_jigyo_code = 13;
	var $index_gid_syozoku_yosan_tanni_code = 15;

	// TID_LISTファイルの配列情報
	var $index_tid = 0;
	var $index_t_name = 1;
	var $index_t_kana = 2;
	var $index_t_mail = 3;
	var $index_t_company_code = 4;
	var $index_t_company_name = 5;
	var $index_t_classification = 6;
	var $index_t_accounting_unit_code = 7;
	var $index_t_expense_department_code = 8;
	var $index_gid = 9;

	// GID_LISTファイルの配列情報
	var $index_gid_gid = 0;
	var $index_g_name = 1;
	var $index_g_kana = 2;
	var $index_g_mail = 3;
	var $index_g_company_code = 4;
	var $index_g_company_name = 5;
	var $index_g_accounting_unit_code = 6;
	var $index_g_expense_department_code = 7;

	/**
	 * コンストラクタ
	 */
	function AppDailyCsvClass($dsn) {
		$this->logger2 = EZLogger::getInstance ();

		// ログ出力前提作業
		$config = parse_ini_file ( sprintf ( "%sconfig/config.ini", N2MY_APP_DIR ), true );
		$log_dir = $config ["LOGGER"] ["log_dir"];

	}

	function multipleCeheck() {
		$_status = "";
		$_pid = 0;
		//プロセスkill
		$killflg  = false;
		$pidFile = dirname ( __FILE__ ) . "/daily_createCSV.pid";
		if (file_exists ( $pidFile )) {
			$_pid = trim ( file_get_contents ( $pidFile ) );
			system ( "ps {$_pid} 2>&1 >/dev/null", $_status );
			if ($_status) {
				unlink ( $pidFile );
			} else {
				$file_time = filemtime ( $pidFile );
				$time = time () - $file_time;
				if ($time < 600) {
					// 多重起動エラー
					return ERROR_MUITIPLE_START_UP;
				} else {
					// ハングと見なし
					// ログを残し、古いプロセスをkill
					system ( "kill -9 {$_pid}", $_status );
					$killflg  =true;
				}
			}
		}
		// pidファイル作成
		system ( 'echo ' . getmypid () . ' > ' . $pidFile, $_status );

		if($killflg){
			return ERROR_PROCESS_KILL;
		}else{
			return true;
		}
	}

	function create_tid_list() {
		//ID無し判定用
		$IdNullflg  =false;

		// 念のためファイル確認
		if (! file_exists ( WIDS_KIHON_PATH )) {
			return ERROR_KIHON_FILE_NOT_FOUND;
		}

		// 基本データ読み込み
		$csv = new EZCsv ( false, "UTF-8", "UTF-8", "\t", true );
		$csv->open ( WIDS_KIHON_PATH, "r" );
		$basic_list = array ();
		while ( $row = $csv->getNext ( true ) ) {
			$basic_list [$row [0]] = $row;
			// IDが無いデータが存在した場合はログ出力
 			if ($basic_list [$row [0]] [$this->index_member_id] == null) {
				$IdNullflg = true;
			}
		}
		$csv->close ();

		// ファイル確認
		if (! file_exists ( WIDS_ZAISEKI_PATH )) {
			return ERROR_ZAISEKI_FILE_NOT_FOUND;
		}

		// 元籍データ読み込み
		$csv = new EZCsv ( false, "UTF-8", "UTF-8", "\t", true );
		$csv->open ( WIDS_ZAISEKI_PATH, "r" );
		$list = array ();
		while ( $row = $csv->getNext ( true ) ) {
			$list [$row [0]] = $row;
			// IDが無いデータが存在した場合はログ出力
			if ($list [$row [0]] [$this->index_member_id] == null) {
				$IdNullflg = true;
			}
		}
		$csv->close ();

		// ファイル確認
		if (! file_exists ( WIDS_HENKAN_PATH )) {
			return ERROR_HENKAN_FILE_NOT_FOUND;
		}

		// ID変換情報ファイル読み込み
		$csv = new EZCsv ( false, "UTF-8", "UTF-8", "\t", true );
		$csv->open ( WIDS_HENKAN_PATH, "r" );
		$henkan_list = array ();
		while ( $row = $csv->getNext ( true ) ) {
			$henkan_list [$row [0]] = $row;
			// IDが無いデータが存在した場合はログ出力
			if ($henkan_list [$row [0]] [$this->index_member_id] == null) {
				$IdNullflg = true;
			}
		}
		$csv->close ();
		foreach ( $basic_list as $basic ) {
			$member_id = $basic [$this->index_member_id];

			// 統合IDが一致する場合は、グローバルIDを入れる
			if ($henkan_list [$member_id] [$this->index_henkan_id] != null) {
				$hikaku_global_id = $henkan_list [$member_id] [$this->index_henkan_global_id];
			} else {
				$hikaku_global_id = "";
			}
			$TID_LIST [$member_id] = array (
					"member_id" => $member_id, // 統合ID
					"member_name" => $basic [$this->index_name], // 氏名
					"member_name_kana" => $basic [$this->index_name_furi], // カナ氏名
					"member_email" => $basic [$this->index_mail], // 主メールアドレス
					"member_company_id" => $list [$member_id] [$this->index_company_code], // 会社コード
					"member_company_name" => $list [$member_id] [$this->index_company_name], // 会社名
					"member_classification" => $list [$member_id] [$this->index_classification], // 相手先区分
					"member_accounting_unit_code" => $list [$member_id] [$this->index_accounting_unit_code], // 経理単位コード
					"member_expense_department_code" => $list [$member_id] [$this->index_expense_department_code], // 費用部課コード
					"member_global_id" => $hikaku_global_id// グローバルID
			);
		}

		// TID_LIST CSV出力
		try {

			$csvFileName = TID_LIST_PATH;
			$res = fopen ( $csvFileName, 'w' );
			if ($res === FALSE) {
				return ERROR_CREATE_TID_LIST;
			}

			foreach ( $TID_LIST as $dataInfo ) {

				fputcsv ( $res, $dataInfo, "\t" );
			}

			fclose ( $res );
		} catch ( Exception $e ) {
			return ERROR_CREATE_TID_LIST;
		}

		// メモリ解放
		unset ( $basic_list );
		unset ( $list );
		unset ( $henkan_list );
		unset ( $TID_LIST );

		//ID無し判定
		if($IdNullflg){
			return ERROR_IDE_NOT_FOUND;
		}else{
			return true;
		}
	}

	function create_gid_list() {
		//ID無し判定用
		$IdNullflg  =false;

		// ファイル確認
		if (! file_exists ( WIDS_GID_KIHON_PATH )) {
			return ERROR_GID_KIHON_FILE_NOT_FOUND;
		}

		// GID_KIHON、GID_SYOZOKUからGID_LIST作成
		// ID基本情報ファイル読み込み
		$csv = new EZCsv ( false, "UTF-8", "UTF-8", "\t", true );
		$csv->open ( WIDS_GID_KIHON_PATH, "r" );
		$GID_LIST = array ();

		while ( $row = $csv->getNext ( true ) ) {
			$GID_LIST [$row [0]] = array (
					"global_id" => $row [$this->index_gid_kihon_global_id], // グローバルID
					"gid_member_name" => $row [$this->index_gid_kihon_name], // 氏名
					"gid_member_name_kana" => $row [$this->index_name_furi], // カナ氏名
					"gid_member_email" => $row [$this->index_gid_kihon_mail]// 主メールアドレス
			) ;

			// IDが無いデータが存在した場合はログ出力
			if ($row [$this->index_gid_kihon_global_id] == null) {
				$IdNullflg = true;
			}
		}

		$csv->close ();

		// ファイル確認
		if (! file_exists ( WIDS_GID_SYOZOKU_PATH )) {
			return ERROR_GID_SYOZOKU_FILE_NOT_FOUND;
		}

		// 所属情報ファイル読み込み
		$csv = new EZCsv ( false, "UTF-8", "UTF-8", "\t", true );
		$csv->open ( WIDS_GID_SYOZOKU_PATH, "r" );
		while ( $row = $csv->getNext ( true ) ) {
			$global_id = $row [0];
			if ($GID_LIST[$global_id]) {
				$GID_LIST [$global_id] = array (
						"global_id" => $GID_LIST [$global_id] ["global_id"], // グローバルID
						"gid_member_name" => $GID_LIST [$global_id] ["gid_member_name"], // 氏名
						"gid_member_name_kana" => $GID_LIST [$global_id] ["gid_member_name_kana"], // カナ氏名
						"gid_member_email" => $GID_LIST [$global_id] ["gid_member_email"], // 主メールアドレス
						"gid_member_company_id" => $row [$this->index_gid_syozoku_company_code], // 会社組織ID
						"gid_member_company_name" => $row [$this->index_gid_syozoku_company_name], // 会社名
						"gid_member_kessan_jigyo_code" => $row [$this->index_gid_syozoku_kessan_jigyo_code], // 決算事業場コード
						"gid_member_yosan_tanni_code" => $row [$this->index_gid_syozoku_yosan_tanni_code]// 予算単位コード
					);
			} else {
				// IDが無いデータが存在した場合はログ出力
				$IdNullflg = true;
			}
		}
		$csv->close ();

		// GID_LIST CSV出力
		try {

			$csvFileName = GID_LIST_PATH;
			$res = fopen ( $csvFileName, 'w' );
			if ($res === FALSE) {
				return ERROR_CREATE_GID_LIST;
			}

			foreach ( $GID_LIST as $dataInfo ) {
				fputcsv ( $res, $dataInfo, "\t" );
			}

			fclose ( $res );
		} catch ( Exception $e ) {
			return ERROR_CREATE_GID_LIST;
		}

		unset ( $GID_LIST );

		//ID無し判定
		if($IdNullflg){
			return ERROR_IDE_NOT_FOUND;
		}else{
			return true;
		}
	}

	function create_user_list() {
		//ID無し判定用
		$IdNullflg  =false;

		// 念のためファイル確認
		if (! file_exists ( TID_LIST_PATH )) {
			return ERROR_TID_LIST_DOSE_NOT_EXIT;
		}

		// TID_LIST読み込み
		$csv = new EZCsv ( false, "UTF-8", "UTF-8", "\t", true );
		$csv->open ( TID_LIST_PATH, "r" );
		$TID_LIST = array ();
		while ( $row = $csv->getNext ( true ) ) {
			$TID_LIST [$row [0]] = $row;
			// IDが無いデータが存在した場合はログ出力
			if ($TID_LIST [$row [0]] [$this->index_member_id] == null) {
				$IdNullflg = true;
			}
		}
		$csv->close ();

		foreach ( $TID_LIST as $tid ) {
			$member_id = $tid [$this->index_tid];
			$global_id = $tid [$this->index_gid];
			// 統合側にのみIDがある
			if ($global_id == null) {
				$USER_LIST [$member_id] = array (
						"member_id" => $tid [$this->index_tid], // 統合ID
						"member_name" => $tid [$this->index_t_name], // 氏名
						"member_name_kana" => $tid [$this->index_t_kana], // カナ氏名
						"member_email" => $tid [$this->index_t_mail], // 主メールアドレス
						"member_company_id" => $tid [$this->index_t_company_code], // 会社コード
						"member_company_name" => $tid [$this->index_t_company_name], // 会社名
						"member_classification" => $tid [$this->index_t_classification], // 相手先区分
						"member_accounting_unit_code" => $tid [$this->index_t_accounting_unit_code], // 経理単位コード
						"member_expense_department_code" => $tid [$this->index_t_expense_department_code], // 費用部課コード
						"member_global_id" => $tid [$this->index_gid], // グローバルID
						"gid_member_name" => "", // 氏名
						"gid_member_name_kana" => "", // カナ氏名
						"gid_member_email" => "", // 主メールアドレス
						"gid_member_company_id" => "", // 会社組織ID
						"gid_member_company_name" => "", // 会社名
						"gid_member_kessan_jigyo_code" => "", // 決算事業場コード
						"gid_member_yosan_tanni_code" => ""// 予算単位コード
				);

				// 統合側とグローバル側にIDがあるのTID情報追加
			} else {
				$USER_LIST [$global_id] = array (
						"member_id" => $tid [$this->index_tid], // 統合ID
						"member_name" => $tid [$this->index_t_name], // 氏名
						"member_name_kana" => $tid [$this->index_t_kana], // カナ氏名
						"member_email" => $tid [$this->index_t_mail], // 主メールアドレス
						"member_company_id" => $tid [$this->index_t_company_code], // 会社コード
						"member_company_name" => $tid [$this->index_t_company_name], // 会社名
						"member_classification" => $tid [$this->index_t_classification], // 相手先区分
						"member_accounting_unit_code" => $tid [$this->index_t_accounting_unit_code], // 経理単位コード
						"member_expense_department_code" => $tid [$this->index_t_expense_department_code], // 費用部課コード
						"member_global_id" => $tid [$this->index_gid]// グローバルID
				) ;

			}
		}

		// メモリ解放
		unset ( $TID_LIST );

		// 念のためファイル確認
		if (! file_exists ( GID_LIST_PATH )) {
			return ERROR_GID_LIST_DOSE_NOT_EXIT;
		}

		// GID_LIST読み込み
		$csv = new EZCsv ( false, "UTF-8", "UTF-8", "\t", true );
		$csv->open ( GID_LIST_PATH, "r" );
		$GID_LIST = array ();
		while ( $row = $csv->getNext ( true ) ) {
			$GID_LIST [$row [0]] = $row;
			// IDが無いデータが存在した場合はログ出力
			if ($GID_LIST [$row [0]] [$this->index_member_id] == null) {
				$IdNullflg = true;
			}
		}
		$csv->close ();

		foreach ( $GID_LIST as $gid ) {
			$global_id = $gid [$this->index_gid_gid];
			$user_list_id = $USER_LIST [$global_id] ["member_global_id"];
			//GID_LIST情報を追加
			// グローバル側にのみIDがある
			if ($user_list_id == null) {
				$USER_LIST [$global_id] = array (
						"member_id" => "", // 統合ID
						"member_name" => "", // 氏名
						"member_name_kana" => "", // カナ氏名
						"member_email" => "", // 主メールアドレス
						"member_company_id" => "", // 会社コード
						"member_company_name" => "", // 会社名
						"member_classification" => "1", // 相手先区分
						"member_accounting_unit_code" => "", // 経理単位コード
						"member_expense_department_code" => "", // 費用部課コード
						"member_global_id" => $gid [$this->index_gid_gid], // グローバルID
						"gid_member_name" => $gid [$this->index_g_name], // 氏名
						"gid_member_name_kana" => $gid [$this->index_g_kana], // カナ氏名
						"gid_member_email" => $gid [$this->index_g_mail], // 主メールアドレス
						"gid_member_company_id" => $gid [$this->index_g_company_code], // 会社組織ID
						"gid_member_company_name" => $gid [$this->index_g_company_name], // 会社名
						"gid_member_kessan_jigyo_code" => $gid [$this->index_g_accounting_unit_code], // 決算事業場コード
						"gid_member_yosan_tanni_code" => $gid [$this->index_g_expense_department_code] // 予算単位コード
				);

				// 統合側とグローバル側にIDがある
			} else {
				$USER_LIST [$global_id] = array (
						"member_id" => $USER_LIST [$global_id] ["member_id"], // 統合ID
						"member_name" => $USER_LIST [$global_id] ["member_name"], // 氏名
						"member_name_kana" => $USER_LIST [$global_id] ["member_name_kana"], // カナ氏名
						"member_email" => $USER_LIST [$global_id] ["member_email"], // 主メールアドレス
						"member_company_id" => $USER_LIST [$global_id] ["member_company_id"], // 会社コード
						"member_company_name" => $USER_LIST [$global_id] ["member_company_name"], // 会社名
						"member_classification" => $USER_LIST [$global_id] ["member_classification"], // 相手先区分
						"member_accounting_unit_code" => $USER_LIST [$global_id] ["member_accounting_unit_code"], // 経理単位コード
						"member_expense_department_code" => $USER_LIST [$global_id] ["member_expense_department_code"], // 費用部課コード
						"member_global_id" => $gid [$this->index_gid_gid], // グローバルID
						"gid_member_name" => $gid [$this->index_g_name], // 氏名
						"gid_member_name_kana" => $gid [$this->index_g_kana], // カナ氏名
						"gid_member_email" => $gid [$this->index_g_mail], // 主メールアドレス
						"gid_member_company_id" => $gid [$this->index_g_company_code], // 会社組織ID
						"gid_member_company_name" => $gid [$this->index_g_company_name], // 会社名
						"gid_member_kessan_jigyo_code" => $gid [$this->index_g_accounting_unit_code], // 決算事業場コード
						"gid_member_yosan_tanni_code" => $gid [$this->index_g_expense_department_code]// 予算単位コード
				);
			}
		}

		unset ( $GID_LIST );

		// CSV作成 タブ区切り
		try {

			$csvFileName = USER_LIST_PATH;
			$res = fopen ( $csvFileName, 'w' );
			if ($res === FALSE) {
				return ERROR_CREATE_USER_LIST;
			}

			foreach ( $USER_LIST as $dataInfo ) {
				fputcsv ( $res, $dataInfo, "\t" );
			}

			fclose ( $res );
		} catch ( Exception $e ) {
			return ERROR_CREATE_USER_LIST;
		}

		unset ( $USER_LIST );

		//ID無し判定
		if($IdNullflg){
			return ERROR_IDE_NOT_FOUND;
		}else{
			return true;
		}
	}

	function result_write() {
		//結果ファイルが存在しなければ作成
		if (!file_exists ( USER_LIST_FLG_PATH )) {
			$res = fopen ( USER_LIST_FLG_PATH, 'w' );
			fclose ( $res );
		}
		// 処理日付をファイル書き込む
		$fp = fopen(USER_LIST_FLG_PATH, 'w');
		fputs($fp, date("Ymd"));
		fclose($fp);
	}

	private function _log($string, $result_log_dir, $error_flg = null) {
		if ($error_flg === "error") {
			$log_prefix = "daily_id_error_list";
		} else {
			$log_prefix = "daily_id_success_list";
		}

		$log_file = sprintf ( "%s/%s_%s.log", $result_log_dir, $log_prefix, date ( 'Ymd', time () ) );
		$fp = fopen ( $log_file, "a" );

		if ($fp) {
			fwrite ( $fp, $string . "\n" );
		}
		fclose ( $fp );
	}
}
