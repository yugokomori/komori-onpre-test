<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_Account.class.php");
require_once("classes/N2MY_Reservation.class.php");

class AppMainMenu extends AppFrame {

    var $_reservation_obj = null;
    var $_name_space = null;
    var $session = null;

    private $obj_N2MY_Account = null;
    var $obj_Meeting = null;
    var $obj_Reservation = null;

    function init() {
        // 予約専用ページらかログインした場合は予約作成ページに飛ばす
        if($this->session->get('login_type') == 'reservation'){
            $url = $this->get_redirect_url("services/reservation/?action_create=&new=1");
            header("Location: ".$url);
        }
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->obj_Reservation = new ReservationTable($this->get_dsn());
        $this->_name_space = md5(__FILE__);
    }

    /**
     * ログイン認証
     */
    function auth() {
        $this->user_auth = "invite";
        $this->checkAuth();
    }

    function action_set_env() {
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_showTop();
        }
        $request    = $this->request->getAll();
        $rules = array(
            'user_email' => array(
                'required' => true,
                'email' => true
                )
        );
        if (!$request['eco_report']) {
            unset($rules['user_email']);
        }
        $check_obj = new EZValidator($request);
        foreach($rules as $field => $rules) {
            $check_obj->check($field, $rules);
        }
        if (EZValidator::isError($check_obj)) {
            $err_msg = $this->get_error_info($check_obj);
            $this->template->assign('err', $err_msg);
            return $this->display_env($request);
        }


        // 有効期限
        $limit_time = time() + 365 * 24 * 3600;
        // 地域コード
        $_country_key = $this->request->get("country_key");
        $result_country_key = $this->_get_country_key($_country_key);
        if (!$result_country_key) {
            // 地域コードの不正な指定を防ぐ
            $country_list = $this->get_country_list();
            if (!array_key_exists($_country_key, $country_list)) {
                $country_keys = array_keys($country_list);
                $_country_key = $country_keys[0];
                $this->logger->warn(__FUNCTION__, __FILE__, __LINE__,$_country_key);
            }
            foreach ($country_list as $country_key => $country_row) {
                if ($_country_key == $country_key) {
                    $this->session->set('country_key', $country_row["country_key"]);
                    $result_country_key = $_country_key;
                }
            }
        } else {
            $this->session->set('country_key', $result_country_key);
        }
        setcookie("country", $result_country_key,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        $this->session->set("country_key", $result_country_key);
        $this->session->set('selected_country_key', $_country_key);
        setcookie("selected_country_key", $_country_key,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);

        setcookie("personal_name", $request["user_name"],  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        $request["user_station"] && $request["station_select"] ?
            setcookie("personal_station", $request["user_station"],  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY):
            setcookie("personal_station", "",  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        if( $request["eco_report"]){
            setcookie("eco_report", $request["eco_report"],  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
            setcookie("personal_email_secure", $request["user_email"], N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        } else {
            setcookie( "eco_report", "",  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY );
            setcookie("personal_email_secure", "",  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        }
        // 言語コード
        $lang = $this->user_lang_allow_confirm($this->request->get("lang"));
        if ($lang) {
            setcookie("lang", $lang,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
            $this->session->set("lang", $lang);
        }
        // タイムゾーン
        $time_zone = $this->request->get("time_zone");
        if (is_numeric($time_zone)) {
            setcookie("time_zone", $time_zone,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
            $this->session->set("time_zone", $time_zone);
        }
        header("Location: index.php");
    }

    function action_env() {
        $env_data = array(
            'lang'           => $this->request->get("lang", $_COOKIE["lang"]),
            'country'        => $this->request->get("country", $_COOKIE["country"]),
            'time_zone'      => $this->request->get("time_zone", $_COOKIE["time_zone"]),
            'user_email'     => $this->request->get("user_email", $_COOKIE["personal_email_secure"]),
            'user_name'      => $this->request->get("user_name", $_COOKIE["personal_name"]),
            'user_station'   => $this->request->get("user_station", $_COOKIE["personal_station"]),
            'eco_report'     => $this->request->get("eco_report", $_COOKIE["eco_report"]),
        );
        return $this->display_env($env_data);
    }

    function action_change_user_mode() {
      $request = $this->request->getAll();
      $user_info = $this->session->get("user_info");
      if($user_info["use_sales"] == 1) {
          $this->session->set("view_room_key", null);
          if ($request["service_mode"] == "meeting") {
            $this->session->set("service_mode","sales");
            if ("sales" == $this->config->get('SMARTY_DIR','custom')) {
              $user_info = $this->session->get("user_info");
              $user_info["custom"] = "sales";
              $this->session->set("user_info",$user_info);
            }

          } elseif($request["service_mode"] == "sales") {
            $this->session->set("service_mode","meeting");
            if ("sales" == $this->config->get('SMARTY_DIR','custom')) {
              $user_info = $this->session->get("user_info");
              $user_info["custom"] = "";
              $this->session->set("user_info",$user_info);
            }
          }
      } else {
        //prevent url send request
        $this->session->set("service_mode",null);
      }
      header("Location: index.php");
    }

    function display_env($env_data) {
        $this->set_submit_key($this->_name_space);
        $this->template->assign("form_data", $env_data);
        $this->display('user/personal.t.html');
    }
    function action_set_language() {
      // 有効期限
      $limit_time = time() + 365 * 24 * 3600;
      // 言語コード
      $lang = $this->request->get("lang");
      if ($lang) {
        setcookie("lang", $lang,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        $this->session->set("lang", $lang);
      }
      // 地域コード
      $_country_key = $this->request->get("country");
      $result_country_key = $this->_get_country_key($_country_key);
      if (!$result_country_key) {
        // 地域コードの不正な指定を防ぐ
        $country_list = $this->get_country_list();
        if (!array_key_exists($_country_key, $country_list)) {
          $country_keys = array_keys($country_list);
          $_country_key = $country_keys[0];
          $this->logger->warn(__FUNCTION__, __FILE__, __LINE__,$_country_key);
        }
        foreach ($country_list as $country_key => $country_row) {
          if ($_country_key == $country_key) {
            $this->session->set('country_key', $country_row["country_key"]);
            $result_country_key = $_country_key;
          }
        }
      } else {
        $this->session->set('country_key', $result_country_key);
      }
      setcookie("country", $result_country_key,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
      $this->session->set("country_key", $result_country_key);
      $this->session->set('selected_country_key', $_country_key);
      setcookie("selected_country_key", $_country_key,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);

    header("Location: index.php");
    }

    /**
     * 会議開始
     */
    function action_meeting_start() {

        $this->session->remove("meeting_auth_flg");
        require_once("classes/N2MY_Meeting.class.php");
        $obj_N2MY_Meeting = new N2MY_Meeting($this->get_dsn());
        // 現在利用可能な会議情報取得
        $room_key = $this->get_room_key();
        // ログインタイプ取得
        $login_type = $this->session->get("login_type");

        /** typeを増やした場合の影響範囲がはかり知れないのでミーティングデータを取得するまでinviteから変更します。。
            下のほうでparticipantに登録する際inviteに戻しています。**/
        if ($this->session->get("invitedGuest")) $login_type = "invitedGuest";

        // ユーザ情報
        $user_info = $this->session->get("user_info");
        $member_info = $this->session->get( "member_info" );
        //member課金を利用していてメンバー以外が会議を利用しようとした場合
        if ( $user_info["account_model"] == "member" && !$member_info && !$user_info["external_user_invitation_flg"]) {
            return $this->render_valid();
        }

        //地域情報
        $country_key = $this->request->get("country");
        if ($country_key == "auto") {
          $result_country_key = $this->_get_country_key($country_key);
          $country_key = $result_country_key;
        }

        $this->logger2->info(array($user_info["user_id"], $room_key, $country_key, $result_country_key));
        if (!$country_key) {
          $country_key = $this->session->get("country_key");
        }

        // 地域が特定できない場合は利用可能な地域をデフォルトとする
        $country_list = $this->get_country_list();

        if (!array_key_exists($country_key, $country_list)) {
          $_country_data = array_shift($country_list);
          $country_key = $_country_data["country_key"];
        }
        $this->session->set("country_key", $country_key);
        // 会議キー
        $meeting_key = $this->request->get("meeting_key");
        // 会議情報取得
        $meeting_info = $obj_N2MY_Meeting->getMeeting($room_key, $login_type, $meeting_key);

        if ($meeting_info["reservation_session"] || !$meeting_info) {
            $reservationInfo = $this->obj_Reservation->getRow(sprintf("reservation_session='%s'", $meeting_info["reservation_session"]));
        }
        //ポート課金の場合は人数チェック
        if ($user_info["max_connect_participant"] > 0 && $user_info["use_port_plan"]) {
            require_once('classes/core/dbi/Participant.dbi.php');
            $obj_Participant     = new DBI_Participant( $this->get_dsn() );
            $nowParticipantNumber = $obj_Participant->numRows('user_key = '.$user_info["user_key"].' AND is_active = 1');
            if ($nowParticipantNumber >= $user_info["max_connect_participant"]) {
                $message = array(
                    "title" => $this->get_message("MEETING_START", "max_participant_error_title"),
                    "text" => $this->get_message("MEETING_START", "max_participant_error_body"),
                    "back_url" => "javascript:window.close();",
                    "back_url_label" => $this->get_message("MEETING_START", "cancel_back_url_label")
                );
                require_once( "classes/dbi/meeting.dbi.php" );
                $obj_Meeting = new MeetingTable($this->get_dsn());
                $where = "meeting_ticket = '".$meeting_info["meeting_key"]."'".
                    " AND is_active = 1";
                $rowNowMeeting = $obj_Meeting->getRow($where);
                require_once('classes/dbi/max_connect_log.dbi.php');
                $obj_MaxConnectLog     = new MaxConnectLog( $this->get_dsn() );
                $data = array(
                            "user_key"      => $user_info["user_key"],
                            "meeting_key"   => $rowNowMeeting["meeting_key"],
                            "room_key"      => $room_key,
                            "type"          => "port_plan",
                            "connect_count" => $nowParticipantNumber,
                            );
                $obj_MaxConnectLog->add($data);
                $this->template->assign("message", $message);
                return $this->display('user/message.t.html');
            }
        }
        if (!$meeting_info) {
            /**
             * 予約会議
             */
            if ("invite" == $login_type) {
                $objN2MYReservation = new N2MY_Reservation($this->get_dsn());
                $reservationInfo = $this->obj_Reservation->getRow(sprintf("meeting_key='%s'", $meeting_key));

                $time_zone = $reservationInfo["reservation_place"];
                $login_start = strtotime($reservationInfo['reservation_starttime']);
                $login_end = strtotime($reservationInfo['reservation_extend_endtime']);
                $now_time = time();
                // 現地時間
                $reservationInfo['reservation_starttime'] = EZDate::getLocateTime($reservationInfo['reservation_starttime'], $time_zone, N2MY_SERVER_TIMEZONE);
                $reservationInfo['reservation_endtime'] = EZDate::getLocateTime($reservationInfo['reservation_endtime'], $time_zone, N2MY_SERVER_TIMEZONE);

                // 削除？
                if (0 == $reservationInfo["reservation_status"]) {
                    $message = array(
                    "title" => $this->get_message("MEETING_START", "cancel_title"),
                    "text" => $this->get_message("MEETING_START", "cancel_text"),
                    "back_url" => "javascript:window.close();",
                    "back_url_label" => $this->get_message("MEETING_START", "cancel_back_url_label")
                    );
                    $this->template->assign("message", $message);
                    return $this->display('user/message.t.html');
                }

                //予約時間終了
                if ( $login_end <= $now_time ) {
                    $this->template->assign("message", RESERVATION_ERROR_FINISHED);
                    $local_datetime = EZDate::getLocateTime(time(), $reservationInfo["reservation_place"], N2MY_SERVER_TIMEZONE);
                    $this->template->assign("now_date", $local_datetime);
                    $this->template->assign("start", $reservationInfo["reservation_starttime"]);
                    $this->template->assign("end", $reservationInfo["reservation_extend_endtime"]);
                    $this->template->assign("reservation_place", $reservationInfo["reservation_place"]);
                    $this->template->assign("reservation_name", $reservationInfo["reservation_name"]);
                    return $this->display('user/reservation/error.t.html');
                }
            }
            /**
             * 会議内から招待されたが既に終了していた場合
             */
            else if($login_type == "invitedGuest") {
                $this->logger->error(__FUNCTION__, __FILE__, __LINE__, $meeting_info);
                $message["title"] = $this->get_message("INVITATION", "finish_error_body");
                $message["body"] = $this->get_message("INVITATION", "finish_error_body");
                $this->template->assign('message', $message);
                return $this->display('common.t.html');
            }
            $message = array(
                "title" => $this->get_message("MEETING_START", "using_title"),
                "text" => $this->get_message("MEETING_START", "using_text"),
                "back_url" => "javascript:window.close();",
                "back_url_label" => $this->get_message("MEETING_START", "using_back_url_label")
                );
            $this->logger2->debug($message);
            $this->template->assign("message", $message);
            return $this->display('user/message.t.html');
        }
        $meeting_key = $meeting_info["meeting_key"];

        // オプション指定
        $options = array(
            "meeting_name" => $meeting_info["meeting_name"],
            "user_key"     => $user_info["user_key"],
            "start_time"   => $meeting_info["start_time"],
            "end_time"     => $meeting_info["end_time"],
            "country_id"   => $country_key,
            "password"     => $meeting_info["meeting_password"]
        );
        // 現在の部屋のオプションで会議更新
        $core_session = $obj_N2MY_Meeting->createMeeting($room_key, $meeting_key, $options);
        // ユーザごとのオプションを取得
        $type        = $this->request->get("type");
        // マルチカメラが無効の場合はtypeをnormalに変更
        if ("multicamera" == $type && $meeting_info["reservation_session"] && $reservationInfo["is_multicamera"] == "0") {
            $type = "normal";
        }
        //ユーザーがトライアルである場合
        $mode = ( $user_info["user_status"] == 2 ) ? "trial" : "";

        // ホワイトボードアップロードメールアドレス
        $mail_upload_address = ($this->config->get("N2MY", "mail_wb_host")) ? $room_key."@".$this->config->get("N2MY", "mail_wb_host") : "";
        $name = $_COOKIE["personal_name"] ? $_COOKIE["personal_name"] : "";
        /* ブラウザ以外のデバイス */
        //モバイル判別
        if (strpos($_SERVER['HTTP_USER_AGENT'], "iPad;") || strpos($_SERVER['HTTP_USER_AGENT'], "Android") || strpos($_SERVER['HTTP_USER_AGENT'],"iPhone") || strpos($_SERVER['HTTP_USER_AGENT'],"iPod")) {
            require_once( "classes/dbi/meeting.dbi.php" );
            $obj_Meeting = new MeetingTable($this->get_dsn());
            $where = "meeting_session_id = '".addslashes($core_session)."'" .
                " AND is_deleted = 0";
            $meeting_info = $obj_Meeting->getRow($where);
            if (!$meeting_info) {
                $message["title"] = MEETING_START_ERROR_TITLE;
                $message["body"] = MEETING_START_ERROR_BODY;
                return $this->render_valid($message);
            }
            $url    = $this->config->get("N2MY", "mobile_protcol", 'vcube-meeting')."://join?session=".session_id()."&room_id=".$meeting_info["room_key"]."&entrypoint=".N2MY_BASE_URL."&pin_code=".$meeting_info['pin_cd'];
            $this->logger2->info($url);
            header("Location: ".$url);
            exit();
        }
        /*typeを増やした場合の影響範囲がはかり知れないので元にもどします。。*/
        if ($this->session->get("invitedGuest")) {
            $login_type = "invite";
            $name = $this->session->get("contact_name") ? $this->session->get("contact_name") : $name;
        }
        //スタッフの場合はinviteではないように変更
        if ( $login_type == "invite" && $type == "staff") $login_type = "";
        $user_options = array(
            "user_key"  => $user_info["user_key"],
            "name"      => $name,
            "participant_email"         => $_COOKIE["personal_email_secure"],
            "participant_station"       => $_COOKIE["personal_station"],
            "participant_country"       => $country_key,
            "member_key"=> $member_info["member_key"],
            "narrow"    => $this->request->get("is_narrow"),
            "lang"      => $this->session->get("lang"),
            "skin_type" => $this->request->get("skin_type", ""),
            "mode"      => $mode,
            "role"      => $login_type,
            "mail_upload_address"       => $mail_upload_address,
            "account_model"       => $user_info["account_model"],
            );
        // 会議入室
        $meetingDetail = $obj_N2MY_Meeting->startMeeting($core_session, $type, $user_options);
        if ( !$meetingDetail ) {
            $message["title"] = MEETING_START_ERROR_TITLE;
            $message["body"] = MEETING_START_ERROR_BODY;
            return $this->render_valid($message);
        }

        // 先に入室しているpolycom clientが存在する場合 participant record にmeeting_keyを追加更新
        /*
        if ($meetingDetail["ives_conference_id"] && $meetingDetail["ives_did"]) {
          require_once ('classes/polycom/Polycom.class.php');
          $polycom = new PolycomClass($this->get_dsn());
          $polycom->addMeetingKey2PolycomParticipant($meetingDetail["meeting_key"], $meetingDetail["ives_conference_id"], $meetingDetail["ives_did"]);
        }
        */

        $limit = time() + 365 * 24 * 3600;
        $display_size = $this->request->get("display_size", '4to3');
        $room_info = $this->get_room_info($room_key);
        if ($room_info["room_info"]["use_sales_option"]) {
            $display_size = "16to9";
        }
        //招待ユーザー入室する際に16対9に固定
        if($login_type == "invite" || $login_type == "invitedGuest") {
            $display_size = "16to9";
        }
        setcookie("displaySize", $display_size,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        $selectDisplaySize = $this->session->get("selectedDisplaySize");
        $selectDisplaySize[$room_key] = $display_size;
        $this->session->set("selectedDisplaySize",$selectDisplaySize);
        //リダイレクト先の core/htdocs/sercie/*　の処理を直接書いています。
        $this->session->set( "room_key", $room_key );
        $this->session->set( "type", $meetingDetail["participant_type_name"] );
        $this->session->set( "meeting_key", $meetingDetail["meeting_key"] );
        $this->session->set( "meeting_type", $type );
        $this->session->set( "participant_key", $meetingDetail["participant_key"] );
        $this->session->set( "participant_name", $name );
        $this->session->set( "mail_upload_address", $mail_upload_address );
        $this->session->set( "fl_ver", $this->request->get("fl_ver") );
        $this->session->set( "display_size", $display_size );
        $this->session->set( "meeting_version", $user_info["meeting_version"] );
        $this->session->set( "reload_type", 'normal');
        // 参加者情報
        $this->session->set( "user_agent_ticket", uniqid());
        // 入室時のログ
        $this->logger2->debug(array(
            "room_key" => $room_key,
            "type" => $meetingDetail["participant_type_name"],
            "meeting_key" => $meetingDetail["meeting_key"],
            "meeting_type" => $type,
            "participant_key" => $meetingDetail["participant_key"],
            "participant_name" => $name,
            ));
        // 会議入室
        $base_url = N2MY_BASE_URL;
        if ($this->session->get("invitedGuest") || $this->session->get("is_invite")) {
            $fep_directory = $this->config->get("SMARTY_DIR", "fep_guest_directory");
            $fep_directory = str_replace("/","",$fep_directory);
            $base_url = $base_url.$fep_directory."/";
        }
        $redirect_url = $base_url."services/?action_meeting_display";
        header( "Location: ".$redirect_url);
        // if (!$meeting_info["meeting_password"] || $reservationInfo["reservation_pw_type"] != "1") {
        //     header( "Location: ".$redirect_url);
        // } else {
        //     header( "Location: ".$redirect_url);
        //     $this->session->set("redirect_url", $redirect_url, $this->_name_space);
        //     // パスワード確認
        //     $params = array(
        //         "action_login"    => "",
        //         "reservation_session" => $meeting_info["reservation_session"],
        //         "ns"              => $this->_name_space
        //         );
        //     $url = $this->get_redirect_url("services/", $params);
        //     $this->logger->info(__FUNCTION__."#end", __FILE__, __LINE__,$url);
        //     header("Location: ".$url);
        // }
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array($room_key, $meeting_info));
        return true;
    }

    function action_meeting_display()
    {
        $this->render_meeting_view();
    }

    /**
     * 会議開始画面を表示
     */
    function action_start_view() {
        $this->render_start_view();
    }

    /**
     * ノッカー
     */
    public function action_knocker()
    {
        $request = $this->request->getAll();
        if( mb_strlen( $request["msg"] ) <= 100 ){
        require_once( "classes/dbi/meeting.dbi.php" );
        require_once( "classes/mgm/dbi/FmsServer.dbi.php" );
        // 直前の会議取得
        $obj_Room = new RoomTable($this->get_dsn());
        $where = "room_key = '".addslashes($request["room_key"])."'";
        $meeting_ticket = $obj_Room->getOne($where, "meeting_key");
        // 会議のステータス取得
        $obj_Meetng = new MeetingTable($this->get_dsn());
        $where = "meeting_ticket = '".addslashes($meeting_ticket)."'";
        $meeting_info = $obj_Meetng->getRow($where);
        // サーバ情報
        if ($meeting_info["intra_fms"]) {
            require_once("classes/dbi/user_fms_server.dbi.php");
            $obj_UserFmsServer = new UserFmsServerTable( N2MY_MDB_DSN );
            $server_info = $obj_UserFmsServer->getRow(sprintf("fms_key=%d", $meeting_info["server_key"]));
        } else {
            $obj_FmsServer = new DBI_FmsServer( N2MY_MDB_DSN );
            $server_info = $obj_FmsServer->getRow(sprintf("server_key=%d", $meeting_info["server_key"]));
        }
        $this->logger2->info(array($request["room_key"], $meeting_ticket, $request["msg"]));
        require_once("lib/EZLib/EZUtil/EZRtmp.class.php");
        $rtmp = new EZRtmp();
        $msg = $request["msg"];
        $msg = addslashes($msg);
        $msg = "'".$msg."'";
        $meeting_id = $obj_Meetng->getMeetingID($meeting_info["meeting_key"]);
        $uri = "rtmp://".$server_info["server_address"]."/".$this->config->get( "CORE", "app_name" )."/".$meeting_info["fms_path"].$meeting_id;
        $cmd .= "\"h = RTMPHack.new; " .
                "h.connection_uri = '" . $uri . "';" .
                "h.connection_args = [0x310, {:target => 'ALL', :message => ".$msg. "}];" .
                "h.method_name = '';" .
                "h.execute\"";
        $this->logger2->info($cmd);
        $rtmp->run($cmd);
            print "1";
        } else {
            print "0";
        }
    }

    /**
     * 予約キャンセルのパスワード確認
     */
    function action_reservation_cancel_view() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        $reservation_session = $this->request->get("reservation_session");
        $return_url = $this->request->get("return_url");
        $params = array(
            "action_reservation_cancel" => $reservation_session,
            "return_url" => $return_url
            );
        // 戻り先
        $redirect_url = $this->get_redirect_url("services/", $params);
        $this->logger->info(__FUNCTION__."#redirect_url", __FILE__, __LINE__,$redirect_url);
        $this->session->set("redirect_url", $redirect_url, $this->_name_space);
        // パスワード確認
        $params = array(
            "action_login"        => "",
            "reservation_session" => $reservation_session,
            "ns"                  => $this->_name_space
            );
        $url = $this->get_redirect_url("services/", $params);
        $this->logger->info(__FUNCTION__."#end", __FILE__, __LINE__,$url);
        header("Location: ".$url);
        exit();
    }

    /**
     * 予約キャンセルを実行
     */
    function action_reservation_cancel() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        $server_info = $this->session->get("server_info");
        $obj_N2MY_Reservation = new N2MY_Reservation($this->get_dsn(), $server_info["host_name"]);
        // パラメタ取得
        $reservation_session = $this->request->get("action_reservation_cancel");
        $obj_N2MY_Reservation->cancel($reservation_session);
        // リダイレクト先を指定
        $return_url = $this->request->get("return_url");
        if ($return_url != "") {
            header("Location: ".$return_url);
        } else {
           $this->action_showTop();
        }
    }

    function action_getRooms(){
      $rooms = $this->get_room_list();
      $roomList = array();
      $val = array();
      foreach($rooms as $room){
        $val["room_key"] = $room["room_info"]["room_key"];
        $val["room_name"] = $room["room_info"]["room_name"];
        $roomList[] = $val;
      }
      // エンティティ処理
      array_walk_recursive($roomList, create_function('&$val, $key', '$val = htmlspecialchars($val);'));
      $json_value = json_encode($roomList);
    header("Content-Type: text/javascript; charset=UTF-8");
    print $json_value;
    }


    function action_getRoom(){
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        // 認証済みの予約PWに再度認証をかけるため
        $this->session->remove('reservation_auth', md5(dirname(__FILE__).DIRECTORY_SEPARATOR.'reservation'.DIRECTORY_SEPARATOR.'index.php'));
        $this->session->remove('admin_login');
        $user_info = $this->session->get('user_info');
        $member_info = $this->session->get('member_info');
        $avairable_intra_fms = $this->session->get("avairable_intra_fms");    // 1: 利用可能, 0: 利用不可能（非表示）

        $rooms = $this->get_room_list();
        if (!$selectDisplaySize = $this->session->get("selectedDisplaySize")) {
            $displaySize = $_COOKIE["displaySize"];
            $selectedDisplaySize = array();
            foreach ($rooms as $room) {
                  $selectDisplaySize[$room["room_info"]["room_key"]] = $displaySize;
            }
            $this->session->set("selectedDisplaySize",$selectDisplaySize);
        }
        //表示する部屋を選択
        $room_key = $this->request->get("room_key") ? $this->request->get("room_key") : $this->session->get("view_room_key");
        $this->logger2->debug($room_key);
        if ($room_key) {
            $room = $rooms[$room_key];
            $this->session->set("view_room_key", $room_key);
        } else {
            //初期表示時は一番最初の部屋を表示
            $room_keys = array_keys($rooms);
            $room = $rooms[$room_keys[0]];
            $room_key = $room_keys[0];
            $this->session->set("view_room_key", $room_keys[0]);
        }
        $room["displaySize"] = $selectDisplaySize[$room_key];
        // 部屋一覧取得
        $page = $this->request->get('page', 1);
        $page_cnt = MAINMENU_MAX_VIEW;
        $offset = ($page - 1) * MAINMENU_MAX_VIEW;
        // メンバー課金
        if ( $member_info && ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre" || $user_info["account_model"] == "free"
              || ($member_info["use_sales"] && $this->session->get("service_mode") == "sales")) ) {
            $total_room_count = $this->obj_N2MY_Account->getOwnRoomCount($member_info["member_key"], $user_info['user_key'], $this->session->get("service_mode"));
        } else {
//            $rooms = array_slice($rooms, $offset, $page_cnt, true);
            $total_room_count = $this->obj_N2MY_Account->getRoomCount($user_info['user_key'],$this->session->get("service_mode"));
            $room_list = array();
            foreach($rooms as $_room_key => $_room){
                $room_list[$_room_key] = $_room["room_info"]["room_name"];
            }
        }
        $pager = $this->setPager($page_cnt, $page, $total_room_count);
        $this->template->assign("pager", $pager);
        $this->template->assign("total_room_cont", $total_room_count);

        $isContractShare = 0;
        // 部屋状態取得
        // デスクトップシェアリング
        if ($room["options"]['desktop_share']) {
            $isContractShare = 1;
        }
        $room["room_info"]['participant_num'] = 0;
        $room["room_info"]['audience_num'] = 0;
        $room["room_info"]['whiteboard_num'] = 0;
        $room["room_info"]['reserve_count'] = 0;
        $room["room_info"]['reserves'] = array();

        $filetype = array();
        if ($room["room_info"]["whiteboard_filetype"]) {
            $whiteboard_filetype = unserialize($room["room_info"]["whiteboard_filetype"]);
            $filetype = array();
            if ($whiteboard_filetype["document"]) {
                foreach($whiteboard_filetype["document"] as $val) {
                    $filetype[] = $val;
                }
            }
            if ($whiteboard_filetype["image"]) {
                foreach($whiteboard_filetype["image"] as $val) {
                    $filetype[] = $val;
                }
            }
        }
        $room["room_info"]["whiteboard_filetype"] = join(",", $filetype);
        //オプション契約数チェック
        require_once("classes/dbi/ordered_service_option.dbi.php");
        $this->obj_OrderedOption = new OrderedServiceOptionTable($this->get_dsn());
        $where_option = "room_key = '".$room_key."'".
                        " AND ordered_service_option_status = 1";
        $room["option_count"] = $this->obj_OrderedOption->numRows($where_option);

        //メインページに表示されるオプション数を計算
        $display_option_count = 0;
        $display_option_list = $this->obj_N2MY_Account->getDisplayOptionList($room_key);
        foreach ($display_option_list as $option)
        {
          if($option)
            $display_option_count++;
        }

        $room["display_option_count"] = $display_option_count;

        $json_value = json_encode($room);
        header("Content-Type: text/javascript; charset=UTF-8");
        print $json_value;
    }

    /**
     * ユーザメインページ
     */
    function action_showTop($message = ""){
        $this->template->assign('redirect_url' , N2MY_RESERVATION_WEB_URL."home" );
        $guest_url = $this->config->get("SMARTY_DIR", "fep_directory").$this->config->get("SMARTY_DIR", "fep_guest_directory");
        if (!strstr($_SERVER["REQUEST_URI"], $guest_url)) {
              $this->display('user/redirect.t.html');
        }
exit;
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        // 認証済みの予約PWに再度認証をかけるため
        $this->session->remove('reservation_auth', md5(dirname(__FILE__).DIRECTORY_SEPARATOR.'reservation'.DIRECTORY_SEPARATOR.'index.php'));
        $this->session->remove('admin_login');
        $user_info = $this->session->get('user_info');
        $member_info = $this->session->get('member_info');
        $avairable_intra_fms = $this->session->get("avairable_intra_fms");    // 1: 利用可能, 0: 利用不可能（非表示）

        $rooms = $this->get_room_list();
        if(($user_info["account_model"] == "member" || $user_info["use_sales"]) && !$member_info) {
          $room_count = 1;
          foreach($rooms as $key => $room_info) {
            $rooms[$key]["room_info"]["room_sort"] = $room_count;
            $room_count++;
          }
        }
        $this->logger2->debug($rooms,"room_list");
        if (!$selectDisplaySize = $this->session->get("selectedDisplaySize")) {
            $displaySize = $_COOKIE["displaySize"];
            $selectedDisplaySize = array();
            foreach ($rooms as $room) {
                  $selectDisplaySize[$room["room_info"]["room_key"]] = $displaySize;
            }
            $this->session->set("selectedDisplaySize",$selectDisplaySize);
        }
        //表示する部屋を選択
        $room_key = $this->request->get("room_key") ? $this->request->get("room_key") : $this->session->get("view_room_key");
        $this->logger2->debug($room_key);
        if ($room_key) {
            $room = $rooms[$room_key];
            if (!$room) {
                $room_keys = array_keys($rooms);
                $room = $rooms[$room_keys[0]];
                $room_key = $room_keys[0];
            }
            $this->session->set("view_room_key", $room_key);
        } else {
            //初期表示時は一番最初の部屋を表示
            $room_keys = array_keys($rooms);
            $room = $rooms[$room_keys[0]];
            $room_key = $room_keys[0];
            $this->session->set("view_room_key", $room_keys[0]);
        }
        $room["displaySize"] = $selectDisplaySize[$room_key];
        // 部屋一覧取得
        $page = $this->request->get('page', 1);
        $page_cnt = MAINMENU_MAX_VIEW;
        $offset = ($page - 1) * MAINMENU_MAX_VIEW;
        // メンバー課金
        if ( $member_info && ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre" || $user_info["account_model"] == "free"
             || ($member_info["use_sales"]) && $this->session->get("service_mode") == "sales")) {
            $total_room_count = $this->obj_N2MY_Account->getOwnRoomCount($member_info["member_key"], $user_info['user_key'], $this->session->get("service_mode"));
        } else {
            $total_room_count = $this->obj_N2MY_Account->getRoomCount($user_info['user_key'],$this->session->get("service_mode"));
        }
        // 1部屋の場合はOFF
        if ($total_room_count == 1) {
            $pcount_flag = 0;
        } else {
            $room_list = array();
            $pcount_flag = 1;
            foreach($rooms as $_room_key => $_room){
                $room_list[$_room_key] = $_room["room_info"]["room_name"];
            }
        }
        $this->logger2->debug($room_list);
        $pager = $this->setPager($page_cnt, $page, $total_room_count);
        $this->template->assign("pcount_flag", $pcount_flag);
        $this->template->assign("pager", $pager);
        $this->template->assign("total_room_cont", $total_room_count);
        $isContractShare = 0;
        // 部屋状態取得
        // デスクトップシェアリング
        if ($room["options"]['desktop_share']) {
            $isContractShare = 1;
        }
        $room["room_info"]['participant_num'] = 0;
        $room["room_info"]['audience_num'] = 0;
        $room["room_info"]['whiteboard_num'] = 0;
        $room["room_info"]['reserve_count'] = 0;
        $room["room_info"]['reserves'] = array();

        $filetype = array();
        if ($room["room_info"]["whiteboard_filetype"]) {
            $whiteboard_filetype = unserialize($room["room_info"]["whiteboard_filetype"]);
            $filetype = array();
            if ($whiteboard_filetype["document"]) {
                foreach($whiteboard_filetype["document"] as $val) {
                    $filetype[] = $val;
                }
            }
            if ($whiteboard_filetype["image"]) {
                foreach($whiteboard_filetype["image"] as $val) {
                    $filetype[] = $val;
                }
            }
        }
        $room["room_info"]["whiteboard_filetype"] = join(",", $filetype);
        //オプション契約数チェック
        require_once("classes/dbi/ordered_service_option.dbi.php");
        $this->obj_OrderedOption = new OrderedServiceOptionTable($this->get_dsn());
        $where_option = "room_key = '".$room_key."'".
                        " AND ordered_service_option_status = 1";
        $room["option_count"] = $this->obj_OrderedOption->numRows($where_option);

        //メインページに表示されるオプション数を計算
        $display_option_count = 0;
        $display_option_list = $this->obj_N2MY_Account->getDisplayOptionList($room_key);
        foreach ($display_option_list as $option)
        {
          if($option)
            $display_option_count++;
        }

        $room["display_option_count"] = $display_option_count;

        $this->template->assign("room", $room);
        $this->template->assign('rooms', $room_list);
        $this->template->assign("message", $message);
        $this->template->assign('room_str', $room_key);
        if ($avairable_intra_fms) {
            require_once("classes/dbi/user_fms_server.dbi.php");
            $obj_UserFmsServer = new UserFmsServerTable($this->get_dsn());
            $serverInfo = $obj_UserFmsServer->getRow(sprintf("user_key=%d", $user_info["user_key"]));
            $server = $serverInfo["server_address"];
        } else {
            $server = $this->config->get("N2MY","fms_server");
        }
        $this->template->assign('server', $server);
        $this->template->assign('user_id', $user_info["user_id"]);
        $this->template->assign('user_key', $user_info["user_key"]);
        $this->template->assign('serverDsnKey', $this->get_dsn_key() );
        // Win + IE + 共有
        if (strpos($_SERVER["HTTP_USER_AGENT"], "Windows") !== false) {
             if (strpos($_SERVER["HTTP_USER_AGENT"], "MSIE") !== false) {
                $this->template->assign('isContractShare', $isContractShare);
             }
        }

        //eco
        require_once( "classes/N2MY_Eco.class.php" );
        $thisMonth = date( "Y-m" );
        $lastYear = date( "Y-m", strtotime( "last year +1 month" ) );
        $objEco = new N2MY_Eco( $this->get_dsn(), N2MY_MDB_DSN );
        $ecoInfo = $objEco->getYearlyLog( $user_info["user_id"], $thisMonth, $lastYear );
        $ecoInfo["thisMonth"] = $thisMonth;
        $ecoInfo["lastYear"] = $lastYear;
        $this->template->assign( 'ecoInfo', $ecoInfo );

        // メンテナンス情報
        require_once("classes/dbi/news.dbi.php");
        $obj_NewsTable = new NewsTable(
                $this->get_auth_dsn(),
                DB_NEWS_TYPE_MAINTENANCE,
                $this->_lang
            );
        $news_rs = $obj_NewsTable->getReleaseList($this->_lang, 3, 0);
        $news_list = array();
        while ($row = $news_rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            $news_list[] = $row;
        }
        $this->template->assign("use_clip_share" , $user_info['use_clip_share']);
        $this->template->assign("news"           , $news_list);
        $this->template->assign('displaySize'    , $selectDisplaySize );
        $this->template->assign('meeting_version' , $user_info["meeting_version"] );
        $this->template->assign('force_stop_meeting_flg' , $user_info["force_stop_meeting_flg"] );

        $this->display('user/mainmenu.t.html');
    }

    function action_force_stop_confirm(){
        $room_key = $this->request->get("room_key");
        $this->template->assign("room_key" , $room_key);
        $this->display('user/force_stop_confirm.t.html');
    }


    /**
     * 管理者メインログインページ
     */
    function action_show_adminlogin(){
        /*
        if ( 1 == $this->session->get("admin_login")) {
            $url = $this->get_redirect_url("services/admin/");
            header("Location: ".$url);
            exit();
        } else
        */
        if( 1 == $this->request->get( "action_show_adminlogin" )) {
            $this->session->remove('admin_login');
        } else {
            $user_info = $this->session->get('user_info');
            $password = $this->request->get( "password" );

            require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
            require_once 'classes/dbi/user.dbi.php';
            $user_db = new UserTable($this->get_dsn());
            $admin_password = $user_db->getOne('user_key = ' . $user_info["user_key"] , "user_admin_password");
            $db_admin_password = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $admin_password);

            if( ! $password ){
                $message .= "<li>".LOGIN_ADMIN_ERROR_PASSWORD . "</li>";
            }else{
                if( 0 != strcmp( $db_admin_password, $password ) ){
                    $message .= "<li>".LOGIN_ADMIN_ERROR_INVALIDPASSWORD . "</li>";
                }else{
                    $this->session->set('admin_login', 1);
                    $url = $this->get_redirect_url("services/admin/");
                    $this->logger2->info($user_info['user_id'], "admin_login");
                    // 操作ログ
                    $this->add_operation_log('admin_login');
                    header("Location: ".$url);
                    exit();
                }
            }
        }

        if ($message){
            $this->template->assign('message', $message);
        }
        $this->display('admin/login/index.t.html');
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    /**
     * 管理者メインメニューページ
     */
//    function action_show_admintop(){
//        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
////        $obj_Main = new Main();
//
////        $message = $obj_Main->checkAdminLoginInfo();
//        if($message){
//            $this->action_show_adminlogin($message);
//        } else {
//            $url = $this->get_redirect_url("services/admin/");
//            header("Location: ".$url);
//            exit();
//        }
//        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
//    }

    /**
     * 管理者ログアウト
     */
    function action_show_adminlogout(){
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        $user_info = $this->session->get('user_info');
        $this->logger2->info($user_info['user_id'], "admin_logout");
        $this->session->remove('admin_login');
        // 操作ログ
        $this->add_operation_log('admin_logout');
        $this->action_showTop();
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    /**
     * 参加人数を取得
     */
    function _participant_num($meeting_key) {
        $list = $this->_participant_list($meeting_key);
        return count($list);
    }

    /**
     * 参加者一覧を出力
     */
    function action_participant_list() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        $room_key = $this->get_room_key();
        // パラメタ取得
        $obj_Meeting = new Meeting();
        $meeting_info = $obj_Meeting->serviceLogin($room_key);
        $meeting_key = $meeting_info["meeting_key"];
        $this->logger->trace(__FUNCTION__."#meeting_key", __FILE__, __LINE__, array($room_key, $meeting_key));
        $list = $this->_participant_list($meeting_key);
        $output = "";
        foreach ($list as $_id => $_name) {
            $output .= "<dd>".$_name."</dd>";
        }
        header("Content-Type: text/html; charset=UTF-8");
        $output = mb_convert_encoding($output, "UTF-8", "EUC-JP");
        print $output;
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__, $output);
    }

    /**
     * 参加者一覧を取得
     */
    function _participant_list($meeting_key) {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        // ２回目以降は結果のみ返す
        static $participant_list;
        if ($participant_list) {
            return $participant_list;
        }
        // 会議参加者一覧を取得
        $query = array(
            "meeting_ticket" => $meeting_key,
        );
        $url = $this->get_core_url("api/meeting/MeetingParticipants.php", $query);
        $contents = $this->wget($url);
        // パースして一覧を取得
        require_once("lib/EZLib/EZXML/EZXML.class.php");
        $obj_XML = new EZXML();
        $list = $obj_XML->openXML($contents);
        $_list = $list["Logs"]["Meeting"][0]["Users"][0]["User"];
        if (is_array($_list)) {
            foreach($_list as $_key => $_data) {
                if ($_data["ROLE"][0]["_data"] == "normal") {
                    $participant_list[$_data["_attr"]["key"]] = $_data["NAME"][0]["_data"];
                }
            }
        }
        $this->logger->trace(__FUNCTION__."#participant_list", __FILE__, __LINE__, $participant_list);
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
        return $participant_list;
    }
    /**
     * 部屋状態表示
     *
     * 出力　0：空室、1：会議中
     */
    function action_meeting_status() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        $room_key = $this->get_room_key();
        // パラメタ取得
        $obj_Meeting = new Meeting();
        $meeting_info = $obj_Meeting->serviceLogin($room_key);
        $meeting_key = $meeting_info["meeting_key"];
        $this->logger->debug(__FUNCTION__."#meeting_key", __FILE__, __LINE__, array($room_key, $meeting_key));
        $status = $this->api_room_status($meeting_key);
        if ($status == true) {
            $status = 1;
        } else {
            $status = 0;
        }
        header("Content-Type: text/html; charset=UTF-8");
        $status = mb_convert_encoding($status, "UTF-8", "EUC-JP");
        print $status;
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__, $status);
    }

    /**
     * パスワードの
     */
    function render_login($message = "")
    {
        $this->logger->trace(__FUNCTION__."#start",__FILE__,__LINE__);
        $user_info = $this->session->get('user_info');
        $member_info = $this->session->get('member_info');
        $api_login_flg = $this->session->get('api_login_flg');
        $this->template->assign('ns', $this->request->get("ns"));
        $this->template->assign('message', $message);
        $reservation_session = $this->request->get("reservation_session");
        $this->template->assign('reservation_session', $reservation_session);
        $this->template->assign('invite', $this->session->get('invite'));
        if ($user_info['account_model'] == 'centre' && $member_info["member_type"] == 'centre' && $api_login_flg == 1) {
            $this->display('user/reservation/centre.t.html');
        } else {
            $this->display('user/reservation/login.t.html');
        }
        $this->logger->trace(__FUNCTION__."#end",__FILE__,__LINE__);
    }

    /**
     *
     */
    function action_login() {
        $this->logger->trace(__FUNCTION__."#start",__FILE__,__LINE__);
        // パスワードチェック
        $reservation_session = $this->request->get("reservation_session");
        $hasPassword = $this->obj_Reservation->hasPassword($reservation_session);
        // パスワードなし
        if (!$hasPassword) {
            // 指定された名前空間のリダイレクト先取得
            $name_space = $this->request->get("ns");
            $redirect_url = $this->session->get("redirect_url", $name_space);
            // セッションから消去
            $this->session->remove("redirect_url", $name_space);
            $this->logger->trace(__FUNCTION__."#url",__FILE__,__LINE__,$redirect_url);
            header("Location: ".$redirect_url);
            return;
        }
        $this->logger->trace(__FUNCTION__."#end",__FILE__,__LINE__);
        return $this->render_login();
    }

    function action_pw_check() {
        $this->logger->trace(__FUNCTION__."#start",__FILE__,__LINE__);
        $reservation_session = $this->request->get("reservation_session");
        $reservation_pw = $this->request->get("reservation_pw");
        $check_flg = $this->obj_Reservation->checkPassword($reservation_session, $reservation_pw);
        if ($check_flg == true) {
            // 指定された名前空間のリダイレクト先取得
            $name_space = $this->request->get("ns");
            $redirect_url = $this->session->get("redirect_url", $name_space);
            // セッションから消去
            $this->session->remove("redirect_url", $name_space);
            $this->logger->debug(__FUNCTION__."#url",__FILE__,__LINE__,$redirect_url);
            $this->session->set("meeting_auth_flg", "1");
            header("Location: ".$redirect_url);
            return;
        } else {
            $err_msg = RESERVATION_ERROR_NOTMUCTH_AUTH_PW;
        }
        return $this->render_login($err_msg);
    }


    function action_invite() {
        return $this->render_invite();
    }

    /**
     * デフォルトページ
     */
    function default_view() {
        $this->logger->trace(__FUNCTION__."#START", __FILE__, __LINE__);
        $login_type = $this->session->get("login_type");
        // 有効期限
        $limit_time = time() + 365 * 24 * 3600;
        // クッキーの保存し直し
        if(isset($_COOKIE["personal_email"])){
            // セキュア属性を付けクッキーに保存しなおす
            setcookie("personal_email_secure", $_COOKIE["personal_email"],  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
            // セキュアじゃないcookie削除
            setcookie("personal_email", "", (time()-1800), N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        }
        if(isset($_COOKIE["sender_name"])){
            // セキュア属性を付けクッキーに保存しなおす
            setcookie("sender_name_secure", $_COOKIE["sender_name"],  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
            // セキュアじゃないcookie削除
            setcookie("sender_name", "", (time()-1800), N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        }
        if(isset($_COOKIE["sender_email"])){
            // セキュア属性を付けクッキーに保存しなおす
            setcookie("sender_email_secure",  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
            // セキュアじゃないcookie削除
            setcookie("sender_email", "", (time()-1800), N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        }
        // 通常ログイン以外は対応メインメニューを表示
        switch ($login_type) {
        case "invite":
        case "invitedGuest":
            $url = $this->get_redirect_url("services/invite.php");
            header("Location: ".$url);
            exit();
        case "trance" :
            $url = $this->get_redirect_url("services/translator.php");
            header("Location: ".$url);
            exit();
        case "presence":
          $url = $this->get_redirect_url("services/api.php?action_presence2_start_meeting=a");
          header("Location: ".$url);
          exit();
        }
        $mode = $this->request->get('mode');
        switch($mode){
            case 'admin':
                $this->action_show_adminlogin();
                break;
            case 'admin_login':
                $this->action_show_admintop();
                break;
            case 'guest':
                $this->action_showGuestTop();
                break;
            default:
                $this->action_showTop();
        }
        $this->logger->trace(__FUNCTION__."#END", __FILE__, __LINE__);
    }

    /**
     * 招待ユーザのメインメニューを表示
     */
    function render_invite() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        // デフォルトの言語指定
        $lang = $this->get_language();
        $this->session->set("lang", $lang);
        $this->_set_template_dir($lang);
        // セッションにセット
        $r_user_session = $this->request->get("r_user_session");
        $this->session->set("r_user_session", $r_user_session);
        // ユーザごとに割り振られたキーから、予約IDを取得
        $obj_Reservation = new Reservation();
        if(!$obj_Reservation->check()){
            $this->display('user/reservation/error.t.html');
            exit;
        }
        // 予約IDから、部屋IDを取得
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $_SESSION);
        $user_info = $this->session->set("r_user_session", $r_user_session);
        $this->display("user/invite.t.html");
        $this->logger->trace(__FUNCTION__."#END", __FILE__, __LINE__);
    }

    /**
     * 会議開始画面を表示
     */
    function render_start_view() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
         $this->set_submit_key($this->_name_space);
        if (!$this->config->get( "IGNORE_MENU", "questionnaire" )) {
            require_once("classes/N2MY_Questionnaire.class.php");
            $obj_Questionnaire = new N2MY_Questionnaire($this->get_dsn());
            $lang = $this->session->get("lang");
            if($lang == null){
              $lang = $this->get_language();
            }
            $question_list = $obj_Questionnaire->getQuestionnaire("meeting_end", $lang);
            $this->logger->debug("question",__FILE__,__LINE__,$question_list);
            $this->template->assign("question_list", $question_list);
        }
        $this->display('user/start.t.html');
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    public function action_detect_location() {
      $country = null;
      $limit_time = time() + 365 * 24 * 3600;
      $country = $this->_get_country_key("auto");
      setcookie("country", $country,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
      $this->session->set("country_key", $country);
      $this->session->set('selected_country_key', "auto");
      setcookie("selected_country_key", "auto",  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
      echo json_encode($country);
    }

    private function render_meeting_view()
    {
        $type = $this->session->get( "type" );
        $this->template->assign('charset', "UTF-8" );

        // 会議情報取得
        $user_info = $this->session->get("user_info");
        $meeting_key = $this->session->get("meeting_key");
        $obj_Meetng = new MeetingTable($this->get_dsn());
        $where = "meeting_key = '".addslashes($meeting_key)."'" .
                " AND user_key = '".$user_info["user_key"]."'" .
                " AND is_active = 1" .
                " AND is_deleted = 0";
        $meeting_info = $obj_Meetng->getRow($where);
        $this->template->assign('meeting_info' , $meeting_info );
        $obj_Room = new RoomTable($this->get_dsn());
        $room_info = $obj_Room->getRow("room_key = '".addslashes($meeting_info['room_key'])."'");
        $this->template->assign("room_info" , $room_info);

        //Skinsの選択を追加
        $skinName = $user_info["user_logo"] ? "SkinsFuji" : "Skins";
        $this->template->assign('skinName' , $skinName );
        $this->template->assign('locale' , $this->get_language() );

        $meeting_version = $this->session->get("meeting_version");
        if ($meeting_version >= "4.7.0.0") {
           $version = "4.7.0.0";
        } else {
           $version = "4.6.5.0";
        }
        $this->template->assign('meeting_version' , $version );

        $content_delivery_base_url = $this->get_content_delivery_base_url();
        $this->template->assign("content_delivery_base_url", $content_delivery_base_url);

        // eco canceler
        $certificate = substr(md5(uniqid(rand(), true)), 1, 16);
        $oneTime = array(
                    "certificate"    =>    $certificate,
                    "createtime"    =>    time());
        $this->session->set("oneTime", $oneTime);
        $this->template->assign('certificateId', $certificate );
        $this->template->assign('apiUrl', '/services/api.php?action_issue_certificate');

        // UserAgent更新
        $this->template->assign('user_agent_ticket' , $this->session->get("user_agent_ticket") );
        // ディスプレイサイズ指定
        $this->template->assign('display_size', htmlspecialchars($this->session->get("display_size"), ENT_QUOTES));
        if ($this->session->get("fl_ver") == "as3") {
            $this->template->assign('meeting_type' , htmlspecialchars($this->session->get("meeting_type"),ENT_QUOTES) );
            $this->template->assign('participant_name' , $this->session->get("participant_name") );
            $this->template->assign('ignore_staff_reenter_alert' , $room_info["ignore_staff_reenter_alert"] );
            // V-Cube Meetingのバージョンが違う場合、キャッシュしたデータを使わせない。
            $this->template->assign('cachebuster' , $this->get_meeting_version());
            $this->display( sprintf( 'core/%s/meeting_as3.t.html', $type ) );
        } else {
            $this->display( sprintf( 'core/%s/meeting_base.t.html', $type ) );
        }
    }

    private function get_content_delivery_base_url() {
        $protocol = '';
        $content_delivery_base_url = '';
        $country_key = $this->session->get("country_key");

        if($this->config->get("CONTENT_DELIVERY", "is_enabled", false)) {
            $country_key = $this->session->get("country_key");
            // 中国からの入室の場合、データの参照元をCDNからではなくBravからに設定する。
            $content_delivery_host = $this->config->get("CONTENT_DELIVERY_HOST", $country_key, "");
            if($content_delivery_host) {
                if(strlen($content_delivery_host)) {
                    $protocol = isset($_SERVER["HTTPS"]) ? 'https://' : 'http://';
                }
                $content_delivery_base_url = $protocol . $content_delivery_host;
                $meeting_file_dir = $this->config->get("CONTENT_DELIVERY", "meeting_file_dir", "");
                if(strlen($meeting_file_dir)) {
                    $content_delivery_base_url .= '/' . $meeting_file_dir;
                }
            }
        }
        return $content_delivery_base_url;
    }

    private function render_valid($message = null)
    {
        if ($message) {
            $this->template->assign('message', $message);
            return $this->display('common.t.html');
        } else {
            return $this->display('error.t.html');
        }
    }

    public function action_before_using() {
        $this->display( 'user/before_using.t.html' );
    }

  public function action_room_option() {
        $user_info = $this->session->get("user_info");
        $room_key = $this->session->get("view_room_key");
        $room_info = $this->obj_N2MY_Account->getOwnRoomByRoomKey($room_key, $user_info["user_key"] );
        $room_info = $room_info[$room_key];
        $room_info["options"] = $this->obj_N2MY_Account->getDisplayOptionList($room_key);
        require_once("classes/mcu/resolve/Resolver.php");
        if (($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($room_key))) {
          $ivesSettingInfo = $videoConferenceClass->getIvesInfoByRoomKey($room_key);
          if ($ivesSettingInfo) {
            $this->template->assign("ignore_video_conference_address", $ivesSettingInfo["ignore_video_conference_address"]);
            $video_conference_number_address = $videoConferenceClass->getNumberAddress($ivesSettingInfo["ives_did"]);
            if ($video_conference_number_address)
              $this->template->assign("video_conference_number_address", $video_conference_number_address);
            $video_conference_address = $videoConferenceClass->getRegularAddress($ivesSettingInfo["ives_did"]);
            if ($video_conference_address)
              $this->template->assign("video_conference_address", $video_conference_address);
            $this->template->assign("use_active_speaker", $ivesSettingInfo["use_active_speaker"]);

            require_once( "classes/dbi/meeting.dbi.php" );
            $obj_Meeting = new MeetingTable($this->get_dsn());
            $where = "meeting_ticket = '".$room_info["room_info"]["meeting_key"]."'".
                " AND is_reserved = 1".
                " AND is_active = 1";
            $rowNowMeeting = $obj_Meeting->getRow($where);
            if ($rowNowMeeting && $rowNowMeeting["temporary_did"]) {
              $where = "meeting_key = '".$rowNowMeeting["meeting_ticket"]."'";
              $now_reservation     = $this->obj_Reservation->getRow($where);

              $guestH323NumberAddress = $videoConferenceClass->getH323TemporaryNumberAddress($rowNowMeeting["temporary_did"]);
              if ($guestH323NumberAddress)
        $this->template->assign("h323TemporaryNumberAddress", $guestH323NumberAddress);
              $guestH323Address = $videoConferenceClass->getH323TemporaryAddress($rowNowMeeting["temporary_did"]);
              if ($guestH323Address)
                $this->template->assign("h323TemporaryAddress", $guestH323Address);
              $guestSipNumberAddress = $videoConferenceClass->getSipTemporaryNumberAddress($rowNowMeeting["temporary_did"]);
              if ($guestSipNumberAddress)
                $this->template->assign("sipTemporaryNumberAddress", $guestSipNumberAddress);
              $guestSipAddress = $videoConferenceClass->getSipTemporaryAddress($rowNowMeeting["temporary_did"]);
              if ($guestSipAddress)
                $this->template->assign("sipTemporaryAddress", $guestSipAddress);

              if ($now_reservation["reservation_pw"] && $now_reservation["reservation_pw_type"] != "2") {
                $this->template->assign("is_password", 1);
              }
            }
          }
        }
        $this->template->assign('room_info', $room_info);
        $this->display( 'user/room_option.t.html' );
    }

    public function action_send_questionnaire() {
        if (!$this->check_submit_key($this->_name_space) || !$this->request->get("__submit_key")) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_showTop();
        }
        require_once("classes/N2MY_Questionnaire.class.php");
        $obj_Questionnaire = new N2MY_Questionnaire($this->get_dsn());
        $questionnaire_id = $obj_Questionnaire->getQuestionnaireId("meeting_end");
        $answers = $this->request->get("answer");
        $participant_key = $this->session->get("participant_key");
        $participant_name = $this->session->get("participant_name");

        if ($questionnaire_id && $participant_key) {
            require_once("classes/dbi/answersheet.dbi.php");
            $obj_Answersheet     = new AnswersheetTable($this->get_dsn());
            $answersheet = array(
                "questionnaire_id" => $questionnaire_id,
                "participant_key"  => $participant_key,
                "participant_name" => $participant_name,
            );
            try {
                $answersheet_id = $obj_Answersheet->add($answersheet);
            } catch (Exception $e) {
                $this->logger2->error($e->getMessage());
                $this->errorToExit();
            }

            if ($answers) {
                foreach ($answers as $question_id => $answer_data) {
                    //設問情報、コメント登録
                    require_once("classes/dbi/answer.dbi.php");
                    $obj_Answer = new AnswerTable($this->get_dsn());
                    $comment = "";
                    if (is_array($answer_data)) {
                        $comment = $answer_data["comment"] ? $answer_data["comment"] : "";
                    }
                    if (is_numeric($answer_data) || $comment || $answer_data["select"]) {
                        $answer = array(
                            "answersheet_id" => $answersheet_id,
                            "question_id"    => $question_id,
                            "comment"        => $comment,
                        );
                        try {
                            $answer_id = $obj_Answer->add($answer);
                        } catch (Exception $e) {
                            $this->logger2->error($e->getMessage());
                            $this->errorToExit();
                        }
                        require_once("classes/dbi/answer_branch.dbi.php");
                        $obj_AnswerBranch = new AnswerBranchTable($this->get_dsn());
                        if (!is_array($answer_data) && $answer_id) {
                            $answer_branch = array(
                                "answer_id"          => $answer_id,
                                "question_id"        => $question_id,
                                "question_branch_id" => $answer_data,
                            );
                            try {
                                $obj_AnswerBranch->add($answer_branch);
                            } catch (Exception $e) {
                                $this->logger2->error($e->getMessage());
                                $this->errorToExit();
                            }
                        } elseif ($answer_data["select"] && $answer_id) {
                            foreach ($answer_data["select"] as $_key => $value) {
                                $answer_branch = array(
                                    "answer_id"          => $answer_id,
                                    "question_id"        => $question_id,
                                    "question_branch_id" => $value,
                                );
                                try {
                                    $obj_AnswerBranch->add($answer_branch);
                                } catch (Exception $e) {
                                    $this->logger2->error($e->getMessage());
                                    $this->errorToExit();
                                }
                            }
                        } else {
                            $this->logger->info("parameter error",__FILE__,__LINE__,array($answer_id, $question_id, $answer_data));
                        }
                    }
                }
            }
        } else {
            $this->logger->info("parameter error",__FILE__,__LINE__,array($questionnaire_id, $participant_key, $participant_name));
            $this->errorToExit();
        }
        $result = array(
                "status" => true,
                "data"   => null,
                );
        // 結果をJSONで出力
        print json_encode($result);
    }
    private function errorToExit($message)
    {
        $result = array(
                "status" => false,
                "data"   => null,
                );
        print json_encode($result);
        exit;
    }

    public function action_enquete() {
        require_once 'classes/N2MY_Enquete.class.php';
        $objEnquete = new N2MY_Enquete($this->config->get('GLOBAL', 'enquete_dsn'));
        $objEnquete->init("trial");
        // 入力チェック
        $request  = $this->request->getAll();
        $objCheck = $objEnquete->check($request);
        // エラーメッセージの生成
        if (EZValidator::isError($objCheck)) {
            $err_msg = array();
            $fields = $objCheck->error_fields();
            foreach($fields as $field) {
                $type = $objCheck->get_error_type($field);
                $err_msg["errors"][] = array(
                    "field"   => $field,
                    "err_cd"  => $type,
                    "err_msg" => sprintf($this->get_message("ERROR", $type), $objCheck->get_error_rule($field, $type)),
                    );
            }
            $result = array(
                "status" => false,
                "data"   => $err_msg,
                );
        // データを保存
        } else {
            $user_info = $this->session->get('user_info');
            $data = array(
                "session"   => session_id(),
                "user_id"   => $user_info["user_id"],
                "q1"        => $request["q1"],
                "q2"        => $request["q2"],
                "q3"        => $request["q3"]
            );
            $ret = $objEnquete->save($data);
            $result = array(
                "status" => true,
                "data"   => null,
                );
        }
        // 結果をJSONで出力
        print json_encode($result);
    }

    public function action_start_notification() {
      $this->display("user/start_notification.t.html");
    }

    private function get_meeting_version() {
        $version_file = N2MY_APP_DIR."version.dat";
        if ($fp = fopen($version_file, "r")) {
            $version = trim(fgets($fp));
        }
        return $version;
    }
}

$main =& new AppMainMenu();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
