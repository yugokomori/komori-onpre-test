<?php
/*
 * Created on 2009/08/24
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once("classes/N2MY_DBI.class.php");

class N2MY_BATCH_4_5_1_1 {
    var $frame = null;

    /**
     * コンストラクタ
     */
    function __construct($frame) {
        $this->frame = $frame;
    }

    function getInfo() {
        $batch_info = <<<EOM
プレゼンスアプリ等で入室したときに発生していた、ユーザーKEYが欠落している会議データを復元する。
EOM;
        return $batch_info;
    }

    /**
     * 前処理
     */
    function prepare() {
    }

    /**
     * バックアップ
     */
    function backup() {
    }

    /**
     * ロールバック
     */
    function roleback() {
    }

    /**
     * 実行
     */
    function execute() {
        $server_list = parse_ini_file(sprintf("%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        print "<table border=1>";
        print "<tr>";
        print "<th>meeting_key</th>";
        print "<th>room_key</th>";
        print "<th>user_key</th>";
        print "</tr>";
        foreach( $server_list["SERVER_LIST"] as $key => $dsn ){
            $meeting_db = new N2MY_DB($dsn,"meeting");
            $room_db = new N2MY_DB($dsn,"room");
            $where = "user_key = 0";
            $meeting_list = $meeting_db->getRowsAssoc($where, null, null, null, "user_key, meeting_key, room_key");
            foreach($meeting_list as $meeting) {
                $where = "room_key = '".addslashes($meeting["room_key"])."'";
                $room_info = $room_db->getRow($where);
                $where = "meeting_key = ".$meeting["meeting_key"];
                $data = array("user_key" => $room_info["user_key"]);
                $this->frame->logger2->info(array($meeting["room_key"], $room_info["room_key"], $where, $data));
                print "<tr>";
                print "<td>".htmlspecialchars($meeting["meeting_key"])."</td>";
                print "<td>".htmlspecialchars($meeting["room_key"])."</td>";
                print "<td>".htmlspecialchars($room_info["user_key"])."</td>";
                print "</tr>";
                $meeting_db->update($data, $where);
            }
        }
        print "<table>";
        exit();
    }

    /**
     * 後処理
     */
    function destroy() {
    }
}
?>