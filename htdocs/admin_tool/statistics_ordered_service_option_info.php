<?php

// 環境設定
require_once('classes/AppFrame.class.php');
// 部屋情報
require_once('classes/dbi/ordered_service_option.dbi.php');

/* ***********************************************
 * 契約済オプション情報を請求管理DBへ反映するための処理
 *
 * ***********************************************/
class AppStatisticsOrderedServiceOptionInfo extends AppFrame {

    var $_dsn = null;
    var $account_dsn = null;

    function init()
    {
        // DBサーバ情報取得
        if (file_exists(sprintf( "%sconfig/slave_list.ini", N2MY_APP_DIR ))) {
            $server_list = parse_ini_file( sprintf( "%sconfig/slave_list.ini", N2MY_APP_DIR ), true);
            $this->_dsn = $server_list["SLAVE_LIST"][N2MY_DEFAULT_DB];
            $this->account_dsn = $server_list["SLAVE_LIST"]["auth_dsn"];
        } else {
            $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);
            $this->_dsn = $server_list["SERVER_LIST"][N2MY_DEFAULT_DB];
            $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        }
        //$this->logger->info(__FUNCTION__.' # Array Of Dsn Lists! ', __FILE__, __LINE__, $this->_dsn);
    }


    /**
     * default view routine
     *
     */
    function default_view()
    {

        $this->logger->info(__FUNCTION__.' # GetMeetingOrderedServiceOptions Info Operation Start ... ', __FILE__, __LINE__);

        // 初期化
        $mtg_ordered_service_options = array();

        // パラメタ取得
        $target_day = $this->request->get('target_day');
        //$target_day = date('Y-m-d', time() - (3600*24)) . ' 00:00:00'; // 常に前日

        // パラメータ検証 ( boolearn : true / false )
        $param_is_safe = $this->_check_param($target_day);

        // パラメータ判定
        if ($param_is_safe){
            // MTG利用統計用契約済みオプションデータ（差分データ）作成
            $this->mtg_ordered_service_options = array();
            //foreach ($this->_dsn as $server_key => $server_dsn) {
                $_tmp_data = null;
                //$_tmp_data = $this->_get_ordered_service_option_routine($server_dsn, $target_day);
                $_tmp_data = $this->_get_ordered_service_option_routine($this->_dsn, $target_day);
                if (is_array($_tmp_data) === true) {
                    //$this->logger->info(__FUNCTION__.' # OrderedServiceOptionInfo ... ', __FILE__, __LINE__, $_tmp_data);
                    $this->logger->info(__FUNCTION__.' # OrderedServiceOptionInfo ... ', __FILE__, __LINE__, "### Merge OrderedServiceOption Info!!");
                    $mtg_ordered_service_options = array_merge($mtg_ordered_service_options, $_tmp_data);
                    //$this->logger->info(__FUNCTION__.' # OrderedServiceOptionInfo ... ', __FILE__, __LINE__, $mtg_ordered_service_options);
                }
            //}
        } else {
            $mtg_ordered_service_options = array();
        }
        $this->logger->info(__FUNCTION__.' # OrderedServiceOptionInfo Count ... ', __FILE__, __LINE__, count($mtg_ordered_service_options));
        //$this->logger->debug(__FUNCTION__.' # OrderedServiceOptionInfo ... ', __FILE__, __LINE__, $mtg_ordered_service_options);

        // 戻り値編集
        //print_r($mtg_ordered_service_options);
        print serialize($mtg_ordered_service_options);

        $this->logger->info(__FUNCTION__.' # GetMeetingOrderedServiceOptions Info Operation End ... ', __FILE__, __LINE__);
    }


    /**
     * メイン取得
     *
     */
    private function _get_ordered_service_option_routine($data_db_dsn, $target_day)
    {
        $ordered_service_option_obj = new OrderedServiceOptionTable($data_db_dsn);
        $rows = null;
        $_rows = null;

        $where = ' `ordered_service_option_registtime` LIKE \'' . addslashes($target_day) . '%\' ' .
                 ' OR `ordered_service_option_deletetime` LIKE \'' . addslashes($target_day) . '%\' ';
        $coulumns = ' `ordered_service_option_key`, `room_key`, `user_service_option_key`, `service_option_key`, ' .
                    ' `ordered_service_option_status`, `ordered_service_option_starttime`,  ' .
                    ' `ordered_service_option_registtime`, `ordered_service_option_deletetime` ';
        $rows = $ordered_service_option_obj->getRowsAssoc($where, null, null, null, $coulumns);
        foreach ($rows as $key => $val) {
            $_rows[] = $val;
        }
        return $_rows;
    }


    /**
     * パラメータ検証
     *
     */
    private function _check_param($_param = null)
    {
        $ret = false;
        //未設定チェック
        if (!$_param) {
            return $ret;
        }
        //文字数チェック
        if (strlen($_param) < 7) {
            return $ret;
        }
        //指定不可文字存在チェック
        if (preg_match("/[_%*?]/", $_param)) {
            return $ret;
        }
        //チェック OK!
        $ret = true;

        return $ret;

    }


}

$main = new AppStatisticsOrderedServiceOptionInfo();
$main->execute();
?>