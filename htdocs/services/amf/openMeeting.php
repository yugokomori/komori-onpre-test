<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
$sessiondata = split("=", $_GET["url"]);

if( $sessiondata[1] ){
    session_id( $sessiondata[1] );
}
require_once("classes/AppFrame.class.php");

class AppOpenMeeting extends AppFrame
{

    function default_view()
    {
        $action = $this->action_default();
        if (PEAR::isError($action)) {
            return $action;
        }
    }

    function action_default()
    {
        $sessiondata = split("=", $_GET["url"]);
        $this->template->assign( "url", $sessiondata[1] );
        $this->session->set( "user_agent_ticket", uniqid());
        $this->display('amf/start_meeting.t.html');
    }

    function action_start_meeting()
    {
        $this->render_start_meeting();
    }

    private function render_start_meeting()
    {
        // 参加者情報
        $this->template->assign('user_agent_ticket' , $this->session->get("user_agent_ticket") );
        $this->template->assign('charset', "UTF-8" );
        $this->template->assign('locale' , $this->get_language() );
        $this->display('core/meeting/meeting_base.t.html');
    }
}

$main =& new AppOpenMeeting();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>