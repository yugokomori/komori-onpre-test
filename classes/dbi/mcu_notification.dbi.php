<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/N2MY_DBI.class.php");

class McuNotificationTable extends N2MY_DB {

    var $maintenance = "";
    var $lang = "";

    /**
     * コンストラクタ
     *
     */
    function McuNotificationTable($dsn) {
        $this->rules = array(
            "mcu_notification_key"  => array(
                "required" => true),
            "server_key"          => array(
                "required" => true),
            "server_address"      => array(
                "required" => true),
            "down_datetime"       => array(
                "required" => true),
            "update_datetime"     => array(
                "required" => true),
        );
        $this->init($dsn, "mcu_notification");
    }

}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
