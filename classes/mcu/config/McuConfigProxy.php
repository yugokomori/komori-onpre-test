<?php
require_once("config/config.inc.php");
require_once('lib/pear/PEAR.php');
require_once("lib/EZLib/EZCore/EZLogger.class.php");
require_once("lib/EZLib/EZCore/EZLogger2.class.php");

require_once("classes/mcu/config/McuConfig.php");
require_once("classes/mcu/config/McuConstant.php");
require_once("classes/mcu/config/McuXmlParser.php");

require_once("classes/mcu/lib/ConferenceLogger.php");


$config = McuConfig::getInstance();
$logger = EZLogger::getInstance($config);
$logger->open();
$logger2 = EZLogger2::getInstance($config);
$logger2->open();

define("N2MY_MDB_DSN", $config->get("GLOBAL", "auth_dsn"));

class McuConfigProxy {
	private $config;
	public $parser;
	const PREFIX = "MCU";
	public function __construct() {
		$this->config = McuConfig::getInstance();
		$this->logger = $logger2 = EZLogger2::getInstance();
		
		$this->parser = new MCUXmlParser();		
	}
	
	public function get($param) {
		return $this->config->get(self::PREFIX, $param);
	}
	
	public function getWithGroupName($groupName, $param) {
		return $this->config->get($groupName, $param);
	}
	
	public function getAuthDsn() {
		return $this->config->get("GLOBAL", "auth_dsn");
	}
	
	public function getDsn() {
		$server_list = parse_ini_file("config/server_list.ini");
		return $server_list[N2MY_DEFAULT_DB];
	}
}
