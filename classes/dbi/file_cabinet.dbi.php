<?php
require_once("classes/N2MY_DBI.class.php");

class FileCabinetTable extends N2MY_DB
{

    var $table = 'file_cabinet';
    protected $primary_key = "cabinet_id";

    function __construct( $dsn )
    {
        $this->init($dsn, $this->table );
    }
}

