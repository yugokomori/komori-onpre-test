<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml2/DTD/xhtml1-strict.dtd">
<?php
require_once 'dBug.php';
require_once 'Net/LDAP.php';

ini_set("display_errors", 1);
ini_set("error_reporting", 2039);

$ldap_info = $_REQUEST["ldap_info"];

$master_ldap_server = isset($ldap_info["master_ldap_server"])  ? $ldap_info["master_ldap_server"]  : "mail.vcube.co.jp"; // "hi-iaidf.shiga-u.ac.jp";
$slave_ldap_server  = isset($ldap_info["slave_ldap_server"])   ? $ldap_info["slave_ldap_server"]   : "is-ialdf.shiga-u.ac.jp";

$base_dn            = isset($ldap_info["base_dn"])             ? $ldap_info["base_dn"]             : "ou=People,dc=vcube,dc=co,dc=jp"; // ou=users,dc=shiga-u,dc=ac,dc=jp
$user_id            = isset($ldap_info["user_id"])             ? $ldap_info["user_id"]             : "user_id";
$user_pw            = isset($ldap_info["user_pw"])             ? $ldap_info["user_pw"]             : "password";
?>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="base.css">
</head>
<body>

<h1>LDAP認証</h1>

<h2>LDAP接続情報</h2>
<div class="section">
    <div class="subsection">
        <form action="<?php print $_SERVER['SCRIPT_NAME']; ?>" method="post">
        <table class="data">
        <thead>
            <tr>
                <th>項目</th>
                <th>値</th>
                <th>例</th>
                <th>備考</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>マスターLDAPサーバー</td>
                <td><input type="text" size="60" name="ldap_info[master_ldap_server]" value="<?php print $master_ldap_server;?>" /></td>
                <td>hi-iaidf.shiga-u.ac.jp</td>
                <td>LDAPフロントサーバ（彦根）</td>
            </tr>
            <tr>
                <td>スレーブLDAPサーバー</td>
                <td><input type="text" size="60" name="ldap_info[slave_ldap_server]" value="<?php print $slave_ldap_server;?>" /></td>
                <td>is-ialdf.shiga-u.ac.jp</td>
                <td>LDAPフロントサーバ（石山）</td>
            </tr>
            <tr>
                <td>識別子</td>
                <td><input type="text" size="60" name="ldap_info[base_dn]" value="<?php print $base_dn;?>" /></td>
                <td>ou=users,dc=shiga-u,dc=ac,dc=jp</td>
                <td>ベースディレクトリ</td>
            </tr>
            <tr>
                <td>ユーザーID</td>
                <td><input type="text" size="60" name="ldap_info[user_id]" value="<?php print $user_id;?>" /></td>
                <td>x1234567</td>
                <td>ユーザーID</td>
            </tr>
            <tr>
                <td>パスワード</td>
                <td><input type="password" size="60" name="ldap_info[user_pw]" value="" /></td>
                <td>password</td>
                <td>パスワード</td>
            </tr>
        </tbody>
        </table>
        <br />
        <input type="submit" value="接続検証">
        </form>
    </div>
</div>
<?php

if ($ldap_info) {
    print '<h2>実行結果</h2>' .
            '<div class="section">';
    if (!$user_id || !$user_pw) {
        print '<font color="red">ユーザーID、パスワードの入力は必須です。</font>';
    } else {
        $my_ldap = new MyLDAP();
        $server_list = array($master_ldap_server, $slave_ldap_server);
        $ldap = null;
        foreach($server_list as $ldap_server) {
            if ($ldap = $my_ldap->connect($ldap_server, $base_dn, $user_id, $user_pw)) {
                break;
            }
        }
        if ($ldap) {
            $base = null;
            $filter = Net_LDAP_Filter::create('uid', 'equals', $user_id);
            // $filter = Net_LDAP_Filter::create('uid', 'begins', $user_id);
            $params = array(
                'scope' => 'one',
                'sizelimit' => 1,
                'attributes' => array()
                );
            $search = $ldap->search($base, $filter, $params);
            // エラーが出ていないかどうかを確かめます。
            if (PEAR::isError($search)) {
                die($search->getMessage() . "\n");
            }
            print '<h3>取得情報</h3><dl>';
            $entry = $search->shiftEntry();
            $user_info = $entry->getValues();
            $uid = $user_info["uid"];
            print "<ul>";
            print "<li>ユーザーID: ".$uid."<br />";
            print "<li>グループ: ".$my_ldap->getGroup($uid)."<br />";
            print "</ul>";
            print '<h3>詳細</h3><dl>';
            new dBug($user_info);
        }
    }
}
?>
</div>
</body>
</html>
<?php

class MyLDAP
{
    function connect($server, $base_dn, $user_id, $user_pw) {
        $bind_dn = "uid=".$user_id.",".$base_dn;
        // 設定配列
        $config = array (
            'binddn'    => $bind_dn,
            'bindpw'    => $user_pw,
            'basedn'    => $base_dn,
            'host'      => $server,
            'options'   => array(
//                "LDAP_OPT_SIZELIMIT" => 1,
//                "LDAP_OPT_TIMELIMIT" => 2,
//                "LDAP_OPT_NETWORK_TIMEOUT" => 2,
            )
        );
        // 設定を用いた接続
        $ldap = Net_LDAP::connect($config);
        // 接続時のエラーの確認
        if (PEAR::isError($ldap)) {
            print '[ '.$server. ' ] <br />' .
                    '<font color="red">LDAP サーバに接続できませんでした。<br />' .
                    'Error Info : '.$ldap->getMessage()."</font><br />";
            return false;
        } else {
            return $ldap;
        }
    }

    function getGroup($uid) {
        $cd = substr($uid, 0, 1);
        switch ($cd) {
            case "s" :
                $group = "正規の学生・院生";
                break;
            case "r" :
                $group = "非正規の学生・院生";
                break;
            case "a" :
                $group = "常勤教職員";
                break;
            case "d" :
                $group = "非常勤教員";
                break;
            case "c" :
                $group = "非常勤職員";
                break;
            case "e" :
                $group = "TA,RA,SA";
                break;
            case "t" :
                $group = "一時利用者";
                break;
            default:
                $group = "該当無し";
                break;
        }
        return $group;
    }
}
?>