<?php
set_time_limit(0);

require_once("classes/core/dbi/Meeting.dbi.php");
require_once('classes/AppFrame.class.php');
require_once("classes/N2MY_Document.class.php");
require_once("classes/mcu/config/McuConfigProxy.php");
require_once("classes/mcu/resolve/Resolver.php");
require_once("lib/EZLib/EZUtil/EZRtmp.class.php");

class API_DocumentStatus extends AppFrame
{
    var $obj_Document = null;
    var $document = null;

    /**
     * 必ず、document_idをパラメタで投げること!!
     */
    function auth() {
        $obj_ConvertFile = new DBI_ConvertFile(N2MY_MDB_DSN);
        $document_id = $this->request->get("document_id");
        // パラメタチェック
        if (!$document_id) {
            $this->logger2->error("DOCUMENT_ID_ERROR");
            return;
        } else {
            $where = "document_id = '".addslashes($document_id)."'";
            $this->document = $obj_ConvertFile->getRow($where);
            if (!$this->document) {
                $this->logger2->error("DB_SELECT_ERROR");
                return;
            }
        }
        $dsn = $this->get_dsn_value($this->document["db_host"]);
        $this->logger2->debug(array($this->document, $dsn));
        $doc_conf = $this->config->getAll("DOCUMENT");
        $this->obj_Document = new N2MY_Document($dsn);
    }

    /**
     * デフォルト処理
     *
     * @access public
     * @param  object &$this クラスファクトリーオブジェクト（参照渡し）
     * @return
     */
    function default_view() {
        $this->logger2->debug($this->document);
        $document = $this->document;
        $document['document_extension'] = $this->document['extension'];
        $document['document_status']    = $this->document['status'];
        $document['document_category'] = "document";
        $document['document_index']     = 1;
        $document['document_result'] = "pending";
        $document['document_index'] = "1";
        $config = $this->config->getAll("DOCUMENT");
        $url_param = parse_url(N2MY_LOCAL_URL);
        $config["document_original_path"] = sprintf($config["scp"], $url_param["host"], $config["doc_dir"]."/".$document["document_path"]);
        $document_path_list[] = $config["document_original_path"];
        $host = null;
        //
        $rtmp = new EZRtmp();
        $fms_list = split(",", $config['fms']);
        foreach($fms_list as $fms) {
            $document_app = sprintf($config["rtmp"], $fms);
            // メール資料貼り込み
            $cmd = "\"".
                "h = RTMPHack.new; " .
                " h.connection_uri = '".$document_app."';" .
                " h.connection_args = [0x320];" .
                " h.method_name = 'getAvailable';" .
                " h.execute;".
                "\"";
            $result = $rtmp->run($cmd);
            $this->logger2->debug(array($cmd, $result));
            list($active, $max_connection) = split("/", $result);
            if ($active > 0) {
                break;
            // 接続可能な台数がない
            } else {
                $this->logger2->warn($fms, "DocumentGateway");
            }
        }
        $urls[] = sprintf($document_app);
        $this->template->assign('c', $config);
        $this->template->assign('d', $document);
        $this->template->assign('document_path_list', $document_path_list);
        $this->template->assign('urls', $urls);
        $this->display("api/document/status.t.xml");
    }

    /**
     * 正常終了
     */
    function action_dispatch() {
        $document_index = $this->request->get("document_index", 1);
        $document_status = $this->request->get("document_status");
        if ($document_status == 1) {
            // ユーザ指定のフォーマット
            $option = unserialize($this->document["addition"]);
            // DOCクライアントが変換した画像フォーマット
            $ext = "jpg";
            // 画像のリサイズ
            $document_id = $this->document["document_id"];
            $base = N2MY_DOC_DIR.$this->document["document_path"].$document_id."/".$document_id;
            if (file_exists($base."_1.".$ext)){
                $obj_Image = new EZImage(N2MY_IMGMGC_DIR);
                $options = null;
                if ($ext != $option["ext"]) {
                    $options["format"] = $option["ext"];
                }
                $image_info = $obj_Image->info($base."_1.".$ext);
                $x = $image_info[0]["x"];
                $y = $image_info[0]["y"];
                $x_max = $option["x"];
                $y_max = $option["y"];
                // 縦横比を保持してリサイズ
                if($x > $x_max || $y > $y_max){
                    $x_rate = $x_max / $x;
                    $y_rate = $y_max / $y;
                    if( $x_rate > $y_rate ){
                        $new_x = intval( $x * $y_rate );
                        $new_y = intval( $y * $y_rate );
                    } else {
                        $new_x = intval( $x * $x_rate );
                        $new_y = intval( $y * $x_rate );
                    }
                    $options["resize"] = $new_x."x".$new_y;
                }
                // 変換の必要があれば
                if ($options) {
                    $obj_Image->mogrify($input_file = $base."_*.".$ext, $options);
                }
                
                $dsn = $this->get_dsn_value($this->document["db_host"]);
                $documentRecord = $this->obj_Document->getDetail($this->document["document_id"]);
                $obj_Meeting         = new DBI_Meeting($dsn);
                $meetingInfo = $obj_Meeting->getRow(sprintf("meeting_key='%s'", $documentRecord["meeting_key"]));
                //
                if (!$meetingInfo || (!$controller = Resolver::getConferenceOptionClassByRoomKey($meetingInfo["room_key"]))) {
                	// 拡張子が異なれば処理しない
                	if ($ext != $option["ext"]) {
                		for($i = 1; $i <= $document_index; $i++) {
                			unlink($base."_".$i.".".$ext);
                		}
                	}
                }
            }
            // 変換完了後にステータス変更
            $this->obj_Document->successful($this->document["document_id"], $document_index);
        } else {
            $this->logger2->info(array($this->document, $document_status));
            $this->obj_Document->error($this->document["document_id"], $document_status);
        }
    }
}

$main = new API_DocumentStatus();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
