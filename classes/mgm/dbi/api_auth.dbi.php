<?php
/**
 * Created on 2006/09/19
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

require_once("classes/N2MY_DBI.class.php");

class ApiAuthTable extends N2MY_DB {

    var $rules = array(
        "api_key"  => array("required" => true),
        "secret"   => array("required" => true),
        "status"   => array("required" => true, "allow" => array(0, 1)),
    );

    function __construct( $dsn ) {
        $this->init( $dsn, "api_auth" );
    }

    public function add( $data )
    {
        $data["create_datetime"] = date( "Y-m-d H:i:s" );
        $data["update_datetime"] = date( "Y-m-d H:i:s" );
        return parent::add( $data );
    }

    public function update( $data, $where )
    {
        $data["update_datetime"] = date( "Y-m-d H:i:s" );
        parent::update( $data, $where );
    }
}