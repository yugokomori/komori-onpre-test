<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_Account.class.php");
require_once("classes/N2MY_DBI.class.php");
require_once("classes/N2MY_MeetingLog.class.php");
require_once("classes/core/Core_Meeting.class.php");
require_once("classes/core/dbi/MeetingSequence.dbi.php");
require_once("classes/core/dbi/Participant.dbi.php");
require_once("classes/dbi/file_cabinet.dbi.php");
require_once("classes/dbi/meeting.dbi.php");
require_once("classes/mgm/dbi/meeting_date_log.dbi.php");
require_once("lib/EZLib/EZUtil/EZDate.class.php");
require_once 'lib/EZLib/EZUtil/EZMath.class.php';
require_once('Pager.php');
require_once ("classes/N2MY_Storage.class.php");

class AppMeetingLog extends AppFrame
{
    /**
     * ログクラス
     * @access public
     */
    var $logger = null;
    var $meetingLog = null;

    function init() {
        $this->obj_N2MY_Account   = new N2MY_Account($this->get_dsn());
        $this->obj_MeetingLog     = new N2MY_MeetingLog($this->get_dsn());
        $this->obj_Meeting        = new MeetingTable($this->get_dsn());
        $this->objMeetingSequence = new DBI_MeetingSequence($this->get_dsn());
        $this->objParticipant     = new DBI_Participant($this->get_dsn());
        $this->objFileCabinet     = new FileCabinetTable($this->get_dsn());
        $this->_name_space        = md5(__FILE__);
    }

    /**
     * ログイン認証
     */
    function auth() {
        $this->checkAuth();
        $this->checkAdminAuth('admin/session_error.t.html');
    }

    /**
     * 会議記録タイトル変更
     */
    function action_edit_title(){
        $request = $this->request->getAll();
        $this->logger->info(__FUNCTION__, __FILE__, __LINE__, $request);
        $this->obj_MeetingLog->editTitle($request["room_key"], $request["meeting_session_id"], $request["title"]);
        $this->add_operation_log('meetinglog_name_edit', array(
            'room_key'     => $request["room_key"],
            'meeting_name' => $request["title"])
            );
        $this->action_showDetail();
    }

    public function action_control_password()
    {
        $request = $this->request->getAll();
        $this->logger->info(__FUNCTION__, __FILE__, __LINE__, $request);
        if ($request["meeting_log_password"]) {
            $this->action_unset_password();
        } else {
            $this->action_set_password();
        }
    }

    /**
     * 会議記録パスワード削除
     */
    function action_set_password()
    {
        $request = $this->request->getAll();
        $this->obj_MeetingLog->setPassword($request["room_key"], $request["meeting_session_id"], $request["password"]);
        // 操作ログ
        $meeting_name = $this->obj_Meeting->getOne("meeting_session_id = '".addslashes( $request["meeting_session_id"] )."'", 'meeting_name');
        $this->add_operation_log('meetinglog_set_password', array(
            'room_key'     => $request["room_key"],
            'meeting_name' => $meeting_name)
            );
        $this->action_showDetail();
    }

    /**
     * 会議記録パスワード設定
     */
    function action_unset_password(){
        $request = $this->request->getAll();
        $this->obj_MeetingLog->unsetPassword($request["room_key"], $request["meeting_session_id"]);
        $request["meeting_session_id"];
        // 操作ログ
        $meeting_name = $this->obj_Meeting->getOne("meeting_session_id = '".addslashes( $request["meeting_session_id"] )."'", 'meeting_name');
        $this->add_operation_log('meetinglog_unset_password', array(
            'room_key'     => $request["room_key"],
            'meeting_name' => $meeting_name)
            );
        $this->action_showDetail();
    }

    public function action_control_protect()
    {
        $request = $this->request->getAll();
        $this->logger->info(__FUNCTION__, __FILE__, __LINE__, $request);
        $request["is_locked"] ? $this->action_unset_protect() : $this->action_set_protect();
    }

    /**
     * 会議記録保護設定
     */
    function action_set_protect(){
        $request = $this->request->getAll();
        $this->obj_MeetingLog->setProtect($request["room_key"], $request["meeting_session_id"]);
        // 操作ログ
        $meeting_name = $this->obj_Meeting->getOne("meeting_session_id = '".addslashes( $request["meeting_session_id"] )."'", 'meeting_name');
        $this->add_operation_log('meetinglog_lock', array(
            'room_key'      => $request["room_key"],
            'meeting_name'  => $meeting_name
            ));
        $this->action_showDetail();
    }

    /**
     * 会議記録保護解除
     */
    function action_unset_protect(){
        $request = $this->request->getAll();
        $this->obj_MeetingLog->unsetProtect($request["room_key"], $request["meeting_session_id"]);
        // 操作ログ
        $meeting_name = $this->obj_Meeting->getOne("meeting_session_id = '".addslashes( $request["meeting_session_id"] )."'", 'meeting_name');
        $this->add_operation_log('meetinglog_unlock', array(
            'room_key'      => $request["room_key"],
            'meeting_name'  => $meeting_name
            ));
        $this->action_showDetail();
    }

    public function action_control_publish()
    {
        $request = $this->request->getAll();
        $this->logger->info(__FUNCTION__, __FILE__, __LINE__, $request);
        $this->obj_MeetingLog->controlPublish($request["room_key"], $request["meeting_session_id"], $request["is_publish"]);
        // 操作ログ
        $meeting_name = $this->obj_Meeting->getOne("meeting_session_id = '".addslashes( $request["meeting_session_id"] )."'", 'meeting_name');
        if ($request["is_publish"]) {
            // 公開
            $this->add_operation_log('meetinglog_unpublic', array(
                'room_key'      => $request["room_key"],
                'meeting_name'  => $meeting_name
                ));
        } else {
            // 非公開
            $this->add_operation_log('meetinglog_public', array(
                'room_key'      => $request["room_key"],
                'meeting_name'  => $meeting_name
                ));
        }
        $this->action_showDetail();
    }

    /**
     * 会議削除
     */
    function action_delete(){
        $request = $this->request->getAll();
        $this->logger2->info($request);
        // 動画生成中は削除不可
        if ($this->isRecorded($request["meeting_session_id"]) == false) {
            $this->obj_MeetingLog->delete($request["room_key"], $request["meeting_session_id"]);
            // 操作ログ
            $meeting_name = $this->obj_Meeting->getOne("meeting_session_id = '".addslashes( $request["meeting_session_id"] )."'", 'meeting_name');
            $this->add_operation_log('meetinglog_delete', array(
                'room_key'      => $request["room_key"],
                'meeting_name'  => $meeting_name
                ));
            $this->action_show_top();
        } else {
            $this->logger2->info($request["meeting_session_id"], "動画生成中のため削除不可");
            $this->action_showDetail();
        }
    }

    /**
     * 複数ログ削除
     */
    public function action_delete_multiple()
    {
        $request = $this->request->getAll();
        if ($request["meetingKeyList"]) {
            $this->add_operation_log('meetinglog_multi_delete', array(
                'type' => $request["status"]
                ));
        }
        switch( $request["status"] ){
            case 1:        //映像の削除
                $this->deleteVideos( $request );
                break;

            case 2:        //議事録の削除
                $this->deleteMinutes( $request );
                break;

            case 3:        //映像と議事録の削除
                $this->deleteVideos( $request );
                $this->deleteMinutes( $request );
                break;

            case 4:        //会議ログの削除
                $this->deleteLogs( $request );
                break;

            default:
                break;
        }
        header("Location: index.php?action_show_top=1");
    }

    /**
     * 映像の削除
     */
    private function deleteVideos( $data )
    {
        require_once( "classes/N2MY_MeetingLog.class.php");
        $objMeetingLog = new N2MY_MeetingLog( $this->get_dsn() );
        for( $i = 0; $i < count( $data["meetingKeyList"] ); $i++ ){
            // 動画生成中は削除不可
            if ($this->isRecorded($data["meetingKeyList"][$i]) == false) {
                $this->obj_MeetingLog->deleteVideo( $data["meetingKeyList"][$i] );
                $this->obj_MeetingLog->getMeetingSize($data["meetingKeyList"][$i]);
            } else {
                $this->logger2->info($data["meetingKeyList"][$i], "動画生成中のため削除不可");
            }
        }
    }

    /**
     * 議事録の削除
     */
    private function deleteMinutes( $data )
    {
        require_once( "classes/N2MY_MeetingLog.class.php");
        $objMeetingLog = new N2MY_MeetingLog( $this->get_dsn() );
        for( $i = 0; $i < count( $data["meetingKeyList"] ); $i++ ){
            // 動画生成中は削除不可
            if ($this->isRecorded($data["meetingKeyList"][$i]) == false) {
                $this->obj_MeetingLog->deleteMinutes( $data["meetingKeyList"][$i] );
                $this->obj_MeetingLog->getMeetingSize($data["meetingKeyList"][$i]);
            } else {
                $this->logger2->info($data["meetingKeyList"][$i], "動画生成中のため削除不可");
            }
        }
    }

    /**
     * 会議ログの削除
     */
    private function deleteLogs( $data )
    {
        for( $i = 0; $i < count($data["meetingKeyList"]); $i++ ){
            // 動画生成中は削除不可
            if ($this->isRecorded($data["meetingKeyList"][$i]) == false) {
                $this->logger2->info($data["meetingKeyList"][$i]);
                if (!$data["room_key"]) {
                    $where = "meeting_session_id = '".addslashes($data["meetingKeyList"][$i])."'";
                    $room_key = $this->obj_Meeting->getOne($where, "room_key");
                } else {
                    $room_key = $data["room_key"];
                }
                $this->obj_MeetingLog->delete($room_key, $data["meetingKeyList"][$i] );
            } else {
                $this->logger2->info($data["meetingKeyList"][$i], "動画生成中のため削除不可");
            }
        }
    }

    /**
     * CSVファイルのダウンロード
     */
    function action_use_log()
    {
        $date_log = new MeetingDateLogTable($this->get_dsn(), $this->base_url, $this->provider_id, $this->provider_pw);
        // 昨日の分
        $date = date("Y-m-d", time() - (3600 * 24));
        $ret = $date_log->day_update($date);
        if (PEAR::isError($ret)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret);
        }
        $date = date("Y-m-d");
        $ret = $date_log->day_update($date);
        if (PEAR::isError($ret)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret);
        }
        // CSV出力
        $csv = new EZCsv();
        $dir = $this->get_work_dir();
        $tmpfile = tempnam($dir, "csv_");
        $csv->open($tmpfile, "w");
        // HTTPヘッダ
        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="JICAmeeting_total_'.date("Ymd").'.csv"');
        // ヘッダ
        $header = array("");
        $month_list = array();
        $month_length = $this->request->get("month", 6);
        $time  = time();
        $end_time = time();
        // 指定月分
        for($i = 0; $i < $month_length; $i++) {
            array_push($header, date("Y年m月", $time));
            $month_list[] = date("Y-m", $time);
            $time = mktime(0,0,0,date("m", $time) -1, 1, date("Y", $time));
        }
        $time = mktime(0,0,0,date("m", $time) +1, 1, date("Y", $time));
        $start_time = $time;
        $csv->write($header);
        // 指定月から取得
        $user_info  = $this->session->get('user_info');
        $room_table = new RoomTable($this->get_dsn());
        $_rooms     = $room_table->get_use_list($user_info['user_key']);
        $summary    = $date_log->month_summary($start_time, $end_time, $_rooms);
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $summary);
        if (PEAR::isError($summary)) {
            $this->logger->error(__FUNCTION__, __FILE__, __LINE__, $summary);
            return false;
        }
        foreach ($summary as $room_key => $room_data) {
            // 初期化
            $meeting_cnt     = array();
            $use_time        = array();
            $participant_cnt = array();
            foreach($month_list as $_key => $month) {
                $meeting_cnt[$month]     = "-";
                $use_time[$month]        = "-";
                $participant_cnt[$month] = "-";
            }
            // データセット
            foreach($room_data as $month => $row) {
                $meeting_cnt[$month]     = $row["sum_meeting_cnt"];
                $use_time[$month]        = $row["sum_use_time"];
                $participant_cnt[$month] = $row["sum_participant_cnt"];
            }
            $line = null;
            $line["room_name"] = "【".$this->_get_room_name($room_key)."】";
            $csv->write($line);
            $line = null;
            // 会議回数
            $line[] = "ログイン総数";
            foreach($participant_cnt as $_key => $val){
                $line[] = $val;
            }
            $this->logger->trace(__FUNCTION__, __FILE__, __LINE__, $line);
            $csv->write($line);
            // 会議回数
            $line = null;
            $line[] = "会議開催総数";
            foreach($meeting_cnt as $_key => $val){
                $line[] = $val;
            }
            $this->logger->trace(__FUNCTION__, __FILE__, __LINE__, $line);
            $csv->write($line);
            // 参加者人数
            $line = null;
            $line[] = "会議開催時間総数";
            foreach($use_time as $_key => $val){
                if ($val == "-") {
                    $use_time_str = "-";
                } else {
                    $hour = floor($val / 3600);
                    $min  = floor(($val % 3600) / 60);
                    $sec  = $val % 60;
                    $use_time_str = "";
                    if ($hour > 0) {
                        $use_time_str = $hour."時間";
                    }
                    if ($min > 0) {
                        $use_time_str .= $min."分";
                    }
                    if ($sec > 0) {
                        $use_time_str .= $sec."秒";
                    }
                }
                $line[] = $use_time_str;
            }
            $this->logger->trace(__FUNCTION__, __FILE__, __LINE__, $line);
            $csv->write($line);
            // 一行あける
            $line = null;
            $csv->write($line);
        }
        $csv->close();
        $fp = fopen($tmpfile, "r");
        $contents = fread($fp, filesize($tmpfile));
        fclose($fp);
        print $contents;
        //unlink($tmpfile);
        exit();
    }

    /**
     * 部屋のログ出力
     */
    function action_room_log()
    {
        // CSV出力
        $csv = new EZCsv();
        $dir = $this->get_work_dir();
        $tmpfile = tempnam($dir, "csv_");
        $csv->open($tmpfile, "w");
        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="'.date("Ymd").'.csv"');
        $room_key      = $this->get_room_key();
        $default_end   = date("Y-m-d H:i:s");
        $default_start = date("Y-m-d H:i:s", mktime(0,0,0,date("m") - 3,date("d"), date("Y")));

        $end        = $this->request->get("end_datetime", $default_end);
        $start      = $this->request->get("start_datetime", $default_start);
        $end_time   = strtotime($end);
        $start_time = strtotime($start);
        $header[]   = $this->_get_room_name($room_key);
        $header[]   = date("Y年m月", $start_time). " - ". date("Y年m月", $end_time);
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $header);
        $csv->write($header);
        // ヘッダ２
        $header = array("タイトル", "開催日時", "参加者", "容量", "利用時間");
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $header);
        $csv->write($header);
        $query = array(
            "room_key"       => $room_key,
            "limit"          => null,
            "offset"         => null,
            "start_datetime" => date("Y-m-d H:i:s", $start_time),
            "end_datetime"   => date("Y-m-d H:i:s", $end_time),
        );
        $log_list_xml = $this->obj_MeetingLog->getList($query, true);
        $log_list = array();
        if (isset($log_list_xml["Logs"]["Meeting"])) {
            foreach($log_list_xml["Logs"]["Meeting"] as $_key1 => $meeting) {
                $line["room_name"]      = $meeting["Title"][0]["_data"];
                $line["date"]            = substr($meeting["date"][0]["_data"], 0, 10);
                // 参加者一覧
                $_users = isset($meeting["Users"][0]["User"]) ? $meeting["Users"][0]["User"] : array();
                $user_list = array();
                foreach($_users as $_key2 => $user) {
                    if ($user["TYPE"][0]["_data"] != "audience" && $user["NAME"][0]["_data"] != "") {
                        $user_list[] = array(
                            "member_key" => $user["MID"][0]["_data"],
                            "type"       => $user["TYPE"][0]["_data"],
                            "name"       => $user["NAME"][0]["_data"]
                            );
                    }
                }
                $log_data["user_list"] = $user_list;
                $line["size"] = number_format(($meeting["Filesize"][0]["_data"] / 1024), 2) . " Kbyte";
                $use_time     = $meeting["useTime"][0]["_data"];
                $hour         = floor($use_time / 3600);
                $min          = floor(($use_time % 3600) / 60);
                $sec          = $use_time % 60;
                $use_time_str = "";
                if ($hour > 0) {
                    $use_time_str = $hour."時間";
                }
                if ($min > 0) {
                    $use_time_str .= $min."分";
                }
                if ($sec > 0) {
                    $use_time_str .= $sec."秒";
                }
                $line["use_time"] = $use_time_str;
                $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $line);
                $csv->write($line);
            }
        }
        // CSV出力
        $csv->close();
        $fp = fopen($tmpfile, "r");
        $contents = fread($fp, filesize($tmpfile));
        fclose($fp);
        print $contents;
        //unlink($tmpfile);
        exit();

    }

    public function action_showDetail() {
        $userInfo = $this->session->get("user_info");
        $request  = $this->request->getAll();
        $where    = sprintf("meeting_key='%s' AND user_key='%s'", $request["meeting_key"], $userInfo["user_key"] );
        $meetingInfo = $this->obj_Meeting->getRow( $where );
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__,$meetingInfo);
        if (! $meetingInfo) {
            $this->logger->info(__FUNCTION__, __FILE__, __LINE__,$meetingInfo);
            return $this->default_view();
        }
        $meetingInfo["meeting_size_used"] = EZMath::number_format($meetingInfo["meeting_size_used"], 1024);
        $meetingInfo["participantList"]   = $this->objParticipant->getList( $meetingInfo ["meeting_key"], false, true );

        $where = sprintf("meeting_key='%s'", $request["meeting_key"]);
        $sequeceList = $this->objMeetingSequence->getRowsAssoc( $where, array( "meeting_sequence_key"=>"asc" ) );
        foreach($meetingInfo["participantList"] as $participant) {
            if ($participant["participant_type_name"] != "audience" && $participant["participant_type_name"] != "multicamera_audience" && $participant["participant_type_name"] != "whiteboard_audience" && $participant["participant_type_name"] != "invisible_wb_audience") {
                $participants_list[] = $participant;
            } else if ($participant["participant_type_name"] == "audience" || $participant["participant_type_name"] == "multicamera_audience" || $participant["participant_type_name"] == "whiteboard_audience" || $participant["participant_type_name"] == "invisible_wb_audience") {
                $audiences[] = $participant;
            }
        }
        $meetingInfo["participants"] = $participants_list;
        $meetingInfo["audiences"]    = $audiences;
        if($meetingInfo["has_shared_memo"] == 1){
            require_once("classes/dbi/shared_file.dbi.php");
            $objSharedFile = new sharedFileTable($this->get_dsn());
            $shared_where = sprintf("status = 1 AND meeting_key='%s'", $request["meeting_key"]);
            $sharedFileList = $objSharedFile->getRowsAssoc($shared_where);
            $this->template->assign("sharedFileList"     , $sharedFileList);
        }
        require_once("lib/EZLib/EZUtil/EZEncrypt.class.php");
        $key = N2MY_ENCRYPT_KEY;
        $iv = N2MY_ENCRYPT_IV;
        $list["count"] = 0;
        for( $i = 0; $i < count( $sequeceList ); $i++ ){
            if ($sequeceList[$i]["has_recorded_video"] || $sequeceList[$i]["record_status"] == "complete") {
                $data = sprintf("%s,log_video,%s,%s", $request["meeting_key"], $this->session->get("lang"), $sequeceList[$i]["meeting_sequence_key"]);
                $minute_data = sprintf("%s,log_minute,%s,%s", $meetingInfo["meeting_key"], $this->session->get("lang"), $sequeceList[$i]["meeting_sequence_key"]);
                if (file_exists(N2MY_MOVIES_DIR.$sequeceList[$i]["record_filepath"])) {
                    $record_filesize = EZMath::number_format(filesize(N2MY_MOVIES_DIR.$sequeceList[$i]["record_filepath"]), 1024);
                } else {
                    $record_filesize = 0;
                }
                $encrypted_data = EZEncrypt::encrypt($key, $iv, $data);
                $encrypted_data = str_replace("/", "-", $encrypted_data);
                $encrypted_data = str_replace("+", "_", $encrypted_data);

                $encrypted_minute_data = EZEncrypt::encrypt($key, $iv, $minute_data);
                $encrypted_minute_data = str_replace("/", "-", $encrypted_minute_data);
                $encrypted_minute_data = str_replace("+", "_", $encrypted_minute_data);
                $videoData = array(
                                "key"                   => $sequeceList[$i]["meeting_sequence_key"],
                                "ondemandUrl"           => sprintf( "%sondemand/%s", N2MY_BASE_URL, $encrypted_data),
                                "minuteUrl"             => sprintf( "%sondemand/%s", N2MY_BASE_URL, $encrypted_minute_data),
                                "has_recorded_video"    => $sequeceList[$i]["has_recorded_video"],
                                "record_id"             => $sequeceList[$i]["record_id"],
                                "record_endtime"        => $sequeceList[$i]["record_endtime"] ? EZDate::getLocateTime( $sequeceList[$i]["record_endtime"], N2MY_USER_TIMEZONE, N2MY_SERVER_TIMEZONE) : "",
                                "record_second"         => $sequeceList[$i]["record_second"],
                                "record_status"         => $sequeceList[$i]["record_status"],
                                "record_result"         => $sequeceList[$i]["record_result"],
                                "record_filepath"       => $sequeceList[$i]["record_filepath"],
                                "record_file_extension" => end(explode('.', $sequeceList[$i]["record_filepath"])),
                                "record_filesize"       => $record_filesize,
                                );
                $list["video"][] = $videoData;
                $list["count"]++;
                if ($sequeceList[$i]["has_recorded_video"]) {
                    $list["video_flg"] = 1;
                }
            }
            if ($sequeceList[$i]["has_recorded_minutes"] ) $list["minute"][] = $sequeceList[$i]["meeting_sequence_key"];
        }
        $meetingInfo["sequeceList"] = $list;
        $meetingInfo["actual_start_datetime"] = EZDate::getLocateTime( $meetingInfo["actual_start_datetime"], N2MY_USER_TIMEZONE, N2MY_SERVER_TIMEZONE);
        //cabinet
        $where = sprintf( "meeting_key='%s' AND status = 1", $request["meeting_key"] );
        $meetingInfo["cabinetList"] = $this->objFileCabinet->getRowsAssoc( $where, array( "cabinet_id"=>"asc" ) );
        $room_info = $this->get_room_info($meetingInfo["room_key"]);

        require_once('classes/N2MY_Clip.class.php');
        $n2my_clip = new N2MY_Clip($this->get_dsn());
        try {
            $clips = $n2my_clip->findIncDeletedByUsersMeetingKey($meetingInfo['meeting_key'], $userInfo['user_key']);
        } catch (Exception $e) {
            $this->logger2->error(__FILE__.':'.__LINE__.' => '.$e); // fix me error template
            die(__LINE__);
        }
        $this->template->assign("clips" , $clips);

        $page = $request["page"];
        $page_cnt = $request["page_cnt"];
        // XSS 対策
        if (!$page_cnt || !is_int((int)$page_cnt) || $page_cnt < 1) {
            $page_cnt = 10;
        }
        if (!$page || !is_int((int)$page) || $page < 1) {
            $page = 1;
        }
        //meetng suggest
        require_once("classes/dbi/meeting_suggest_log.dbi.php");
        $obj_MeetingSuggestLog = new MeetingSuggestTable($this->get_dsn());
        $has_meeting_suggest = $obj_MeetingSuggestLog->numRows(sprintf( "meeting_key='%s'", $meetingInfo['meeting_key'] ));
        $this->logger2->info($has_meeting_suggest);
        $this->template->assign("has_meeting_suggest"      , $has_meeting_suggest);
        $this->template->assign("info"      , $meetingInfo);
        $this->template->assign("room_info" , $room_info);
        $this->template->assign("log"       , $meetingInfo);
        $this->template->assign("page"      , $page);
        $this->template->assign("page_cnt"  , $page_cnt);
        $this->set_submit_key();
        $this->display('admin/meetinglog/detail.t.html');
    }

    /**
     * 共有メモの削除
     */
    function action_memo_delete(){
        require_once("classes/dbi/shared_file.dbi.php");
        $objSharedFile = new sharedFileTable($this->get_dsn());

        $where_meeting       = sprintf("meeting_key='%s'", $this->request->get("meeting_key"));
        $meetingInfo = $this->obj_Meeting->getRow($where_meeting);

        $meeting_key =  $this->request->get("meeting_key");
        $meeting_sequence_key = $this->request->get("meeting_sequence_key");

        $where = sprintf("shared_file_key='%s' AND meeting_key='%s' AND meeting_sequence_key='%s'",  $this->request->get("shared_file_key"), $meeting_key , $meeting_sequence_key);
        $shard_file_info = $objSharedFile->getRow($where);
        $user_info = $this->session->get('user_info');

        // エラー処理
        if($shard_file_info["status"] == 0 || !$shard_file_info || $user_info["user_key"] != $meetingInfo["user_key"]){
            return $this->action_showDetail();
        }
        $this->obj_MeetingLog->deleteSharedFile($meeting_key, $meeting_sequence_key,  $this->request->get("shared_file_key"));

        // ファイルを物理削除
        if(is_file($shard_file_info["file_path"])){
            unlink($shard_file_info["file_path"]);
        }
        $this->logger2->info($shard_file_info["file_path"] . "を削除しました");
        // メモサイズ分減らす
        $this->obj_MeetingLog->shorten_meeting_size_used($meeting_sequence_key , $shard_file_info["file_size"]);

        $this->logger2->info("meeting_key:".$meeting_key."shorten_meeting_size_used:".$shard_file_info["file_size"]);
        $this->obj_MeetingLog->getMeetingSize($meetingInfo["meeting_session_id"]);
        return $this->action_showDetail();
    }

    /**
     * 共有メモデータDL
     */
    function action_get_memo(){
        require_once("classes/dbi/shared_file.dbi.php");
        $objSharedFile = new sharedFileTable($this->get_dsn());
        $shared_where = sprintf("status = 1 AND shared_file_key='%s' AND meeting_key='%s'", $this->request->get("shared_file_key"), $this->request->get("meeting_key"));
        $sharedFile = $objSharedFile->getRow($shared_where);

        $where       = sprintf("meeting_key='%s'", $this->request->get("meeting_key"));
        $meetingInfo = $this->obj_Meeting->getRow($where);
        $user_info = $this->session->get('user_info');
        // エラー処理
        if(!$meetingInfo || $user_info["user_key"] != $meetingInfo["user_key"] || !$sharedFile){
            $this->logger2->warn("meeting_key:".$this->request->get("meeting_key")." NOT MEETING DATA");
            header("Location: index.php");
            return ;
        }
        if(!is_file($sharedFile["file_path"])){
            $this->logger2->warn("shared_file_key:".$this->request->get("shared_file_key")."path:".is_file($sharedFile["file_path"]). " NOT FOUND FILE");
            header("Location: index.php");
            return ;
        }
        // セッションのタイムゾーンを元に時間を形成
        $time_zone = $this->session->get("time_zone");
        if(!$time_zone){
            $time_zone = 9;
        }
        $time = (EZDate::getLocateTime($sharedFile["create_datetime"], $time_zone, N2MY_SERVER_TIMEZONE));

        $file_name = sprintf( "%s%s%s", "vcmemo_", date("YmdHi" , $time), ".txt" );
        // ファイルダウンロード
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=" . $file_name);
        // 対象ファイルを出力する。
        readfile($sharedFile["file_path"]);
    }

    /**
     * 部屋名を取得
     */
    function _get_room_name($room_key) {
        static $rooms;
        if (!isset($rooms)) {
            $user_info  = $this->session->get('user_info');
            $room_table = new RoomTable($this->get_dsn());
            $_rooms     = $room_table->use_room($user_info['user_key']);
            foreach($_rooms as $_key => $val) {
                $rooms[$val["room_key"]] = $val["room_name"];
            }
            $this->logger->info(__FUNCTION__,__FILE__,__LINE__, $rooms);
        }
        $this->logger->info(__FUNCTION__,__FILE__,__LINE__, $rooms[$room_key]);
        return $rooms[$room_key];
    }

    /**
     * デフォルト処理
     */
    function default_view() {
        $this->action_show_top();
    }

    /**
     * 会議ログ一覧表示
     */
    function action_show_top() {
        if (1 == $this->request->get("reset")) {
            // 検索クエリーをクリア
            $this->session->remove("condition", $this->_name_space);
        } else if ($this->request->get("action_show_top") != 1) {
            // 検索指定
            $search              = $this->request->get("search");
            $search["page_cnt"]  = $this->request->get("page_cnt");
            $search["page"]      = $this->request->get("page", 1);
            $search["sort_key"]  = $this->request->get("sort_key")  ? htmlspecialchars($this->request->get("sort_key"),ENT_QUOTES)   : "actual_start_datetime";
            $search["sort_type"] = $this->request->get("sort_type") ? htmlspecialchars($this->request->get("sort_type"),ENT_QUOTES) : "desc";
            $room_key            = $this->get_room_key();
            $search["room_key"]  = $room_key;
            $this->session->set("condition", $search, $this->_name_space);
        }
        $this->display_list();
    }

    public function action_sort_list() {
        $search = $this->session->get("condition", $this->_name_space);
        $search["page"]         = 1;
        $search["sort_key"]     = $this->request->get("sort_key")  ? htmlspecialchars($this->request->get("sort_key"),ENT_QUOTES)   : "actual_start_datetime";
        $search["sort_type"]    = $this->request->get("sort_type") ? htmlspecialchars($this->request->get("sort_type"),ENT_QUOTES) : "desc";
        $sort_key_rules = array("actual_start_datetime","meeting_name","meeting_name","meeting_use_minute","meeting_size_used");
        if (!EZValidator::valid_allow($search["sort_key"], $sort_key_rules)) {
            $search["sort_key"] = "actual_start_datetime";
        }
        $sort_type_rules = array("asc","desc");
        if (!EZValidator::valid_allow($search["sort_type"], $sort_type_rules)) {
            $search["sort_type"] = "desc";
        }
        $this->session->set("condition", $search, $this->_name_space);
        $this->display_list();
    }

    public function display_list() {
        $search       = $this->session->get("condition", $this->_name_space);
        if (!$search["room_key"]) {
            $room_list = $this->session->get("room_info");
            if (count($room_list) == 1) {
                $room_info = array_shift($room_list);
                $search["room_key"] = $room_info["room_info"]["room_key"];
            }
        }

        $page_cnt     = $search["page_cnt"];
        $current_page = $search["page"];
        $user_info    = $this->session->get('user_info');

        //終了した予約会議の確認処理
        require_once ("classes/dbi/room.dbi.php");
        $obj_Room = new RoomTable( $this->get_dsn() );
        $where = "user_key = ".mysql_real_escape_string($user_info["user_key"]);
        $rooms = $obj_Room->getRowsAssoc($where, null, null, null, "room_key, meeting_key");
        foreach( $rooms as $key => $val ){
            $query[$val["room_key"]] = $val["meeting_key"];
        }
        $obj_CoreMeeting = new Core_Meeting($this->get_dsn());
        $obj_CoreMeeting->checkReservationMeetingStatus($query);

        $room_list    = $this->session->get("room_info");
        // 会議の検索ヒット件数を取得
        $param = array(
            "user_key"              => $user_info["user_key"],
            "room_key"              => $search["room_key"],
            "title"                 => $search["title"],
            "delete"                => 0,
            "participant"           => $search["participant"],
            "meeting_size_used"     => $search["size"],
//            "meeting_use_minute"    => $search["minute"],
            "start_datetime"        => ($search["start"]) ? EZDate::getLocateTime($search["start"]." 00:00:00",
                                                                      N2MY_SERVER_TIMEZONE, N2MY_USER_TIMEZONE, "Y-m-d H:i:s") : "",
            "end_datetime"          => ($search["end"])   ? EZDate::getLocateTime($search["end"]." 23:59:59",
                                                                      N2MY_SERVER_TIMEZONE, N2MY_USER_TIMEZONE, "Y-m-d H:i:s") : "",
            "has_recorded_video"    => $search["has_recorded_video"],
            "has_recorded_minutes"  => $search["has_recorded_minutes"],
            "is_locked"             => $search["is_locked"],
            "is_reserved"           => $search["is_reserved"],
            "is_publish"            => $search["is_publish"],
            );
        $this->logger2->info($param);
        $obj_CoreMeeting = new Core_Meeting($this->get_dsn());
        $summary         = $obj_CoreMeeting->getSummary($param);
        $allnum          = $summary["meeting_count"];
        $total_log_size  = $summary["total_size_used"];
        // ページ情報取得
        if (!$page_cnt) $page_cnt = 10;
        $pager  = $this->setPager($page_cnt, $current_page, $allnum);
        $offset = ($pager["start"] > 0) ? $pager["start"] - 1 : 0;
        if (!$search["sort_key"])  $search["sort_key"] = "actual_start_datetime";
        if (!$search["sort_type"]) $search["sort_type"] = "desc";
        $current_page = ($current_page > 1) ? $current_page :1;
        $param["limit"]     = $page_cnt;
        $param["offset"]    = $offset;
        $param["sort_key"]  = $search["sort_key"];
        $param["sort_type"] = $search["sort_type"];
        $list = $obj_CoreMeeting->getMeetingInfoList( $param );
        // データ整形
        $log_list = array();
        $meeting_keys = array();
        foreach( $list as $key => $val ){
            $val["meeting_size_used"]     = EZMath::number_format($val["meeting_size_used"], 1024);
            $val["actual_start_datetime"] = EZDate::getLocateTime($val["actual_start_datetime"],
                                                                  N2MY_USER_TIMEZONE, N2MY_SERVER_TIMEZONE);
            require_once("classes/dbi/fms_watch_status.dbi.php");
            $objFmsWatch  = new FmsWatchTable($this->get_dsn());
            $where        = sprintf("meeting_key=%d", $val["meeting_key"]);
            $fmsWatchInfo = $objFmsWatch->getRow($where);
            $val["fms_watch_status"] = $fmsWatchInfo["status"];
            require_once("classes/dbi/meeting_suggest_log.dbi.php");
            $obj_MeetingSuggestLog = new MeetingSuggestTable($this->get_dsn());
            $val["has_meeting_suggest"] = $obj_MeetingSuggestLog->numRows(sprintf( "meeting_key='%s'", $val['meeting_key'] ));
            $log_list[] = $val;
            $meeting_keys[] = $val['meeting_key'];
        }
        $max_rec_size = $user_info["max_storage_size"];
        if ($meeting_keys) {
            try {
                require_once('classes/dbi/meeting_clip.dbi.php');
                $meeting_clip_table  = new MeetingClipTable($this->get_dsn());
                $count_meeting_clips = $meeting_clip_table->countByMeetingKeys($meeting_keys);
            } catch (Exception $e) {
                $this->logger2->error(__FILE__.':'.__LINE__.' => '.$e->getMessage()); // fix me error template
                die(__LINE__);
            }
        }
        $obj_Storage  = new N2MY_Storage($this->get_dsn(), $serverInfo["host_name"]);
        $used_storage_size = $obj_Storage->get_total_size($user_info["user_key"]);
        $this->template->assign("total_log_size", round( $used_storage_size / (1024 * 1024) ) );
        $this->template->assign("total_use_time", $summary["total_use_time"] );
        $this->template->assign('page_cnt'      , $page_cnt);
        $this->template->assign('pager'         , $pager);
        $this->template->assign("log_size_unit" , "MB");
        $this->template->assign('room_list'     , $room_list);
        $this->template->assign('room_info'     , $room_info["room_info"]);
        $this->template->assign('max_rec_size'  , $max_rec_size);
        $this->template->assign('log_list'      , $log_list);
        $this->template->assign('search'        , $search);
        $this->template->assign('count_meeting_clips' , $count_meeting_clips);
        $this->display('admin/meetinglog/index.t.html');
    }


    /**
     * フォームの内容を再現
     */
    function set_form($default_sort_key, $default_sort_type) {
        //
        // デフォルトのソート順指定
        $sort_key  = $this->request->get("sort_key");
        $sort_type = $this->request->get("sort_type");
        $page      = $this->request->get("page");
        $page_cnt  = $this->request->get("page_cnt");
        $reset     = $this->request->get("reset");
        // デフォルト
        if (!isset($_SESSION[$this->_name_space]) || $reset == 1 || $reset == 2) {
            $_SESSION[$this->_name_space]['sort_key']  = $default_sort_key;
            $_SESSION[$this->_name_space]['sort_type'] = $default_sort_type;
            $_SESSION[$this->_name_space]['page'] = 1;
            $_SESSION[$this->_name_space]['page_cnt'] = 20;
        }
        if ($reset == 1) {
        $this->logger->debug("session",__FILE__,__LINE__,$_SESSION);
            unset($_SESSION[$this->_name_space]["request"]);
        } else {
            // ソート
            if ($sort_key) {
                $_SESSION[$this->_name_space]['sort_key']  = $sort_key;
                $_SESSION[$this->_name_space]['sort_type'] = $sort_type;
            }
            // ページ
            if ($page) {
                $_SESSION[$this->_name_space]['page'] = $page;
            }
            // ページ
            if ($page_cnt) {
                $_SESSION[$this->_name_space]['page_cnt'] = $page_cnt;
            }
            $request = $this->request->get("form");
            if ($request) {
                $_SESSION[$this->_name_space]['page'] = 1;
                $_SESSION[$this->_name_space]["request"] = $request;
            }
        }
        return $_SESSION[$this->_name_space];
    }

    function action_create_movie() {
        if ($this->check_submit_key()) {
            $this->_create_movie();
        }
        return $this->action_showDetail();
    }

    private function _create_movie() {
        $meeting_session_id   = $this->request->get("meeting_session_id");
        $meeting_sequence_key = $this->request->get("meeting_sequence_key");
        $_delete_meeting_data = $this->request->get("delete_meeting_data");
        $user_info            = $this->session->get("user_info");
        $room_key             = $this->get_room_key();
        // 会議情報
        $where = "meeting_session_id = '".$meeting_session_id."'" .
            " AND room_key = '".addslashes($room_key)."'" .
            " AND (meeting_log_password = '' OR meeting_log_password IS NULL)" .
            " AND is_deleted = 0";
        $meeting_info = $this->obj_Meeting->getRow($where, "meeting_key, meeting_name, user_key, room_key");
        $this->logger2->info($meeting_info);
        if (!$meeting_info) {
            $this->logger2->warn($where);
            return false;
        }
        // シーケンス情報
        $where = "meeting_key = '".$meeting_info["meeting_key"]."'" .
            " AND (record_status = '' OR record_status IS NULL OR record_status = 'error')".
            " AND meeting_sequence_key = ".$meeting_sequence_key.
            " AND has_recorded_video = 1";
        $meeting_sequence_info = $this->objMeetingSequence->getRow($where);
        $this->logger2->info($meeting_sequence_info);
        if (!$meeting_sequence_info) {
            $this->logger2->warn($where);
            return false;
        }
        // 容量取得
        $obj_OrderedServiceOption = new OrderedServiceOptionTable($this->get_dsn());
        $where = "room_key = '".addslashes($room_key)."'".
            " AND service_option_key = 4" .
            " AND ordered_service_option_status = 1";
        $count = $obj_OrderedServiceOption->numRows($where);
        /*if ($user_info["account_model"] == 'member'){
            $max_rec_size = ($count * 500 * 1024 * 1024);
        } else {
            $max_rec_size = ($count * 1024 * 1024 * 1024) + (RECORD_SIZE * 1024 * 1024);
        }*/
        $max_rec_size = $user_info["max_storage_size"]  * 1024 * 1024;
        $obj_CoreMeeting = new Core_Meeting( $this->get_dsn() );
        require_once("classes/N2MY_Storage.class.php");
        $obj_Storage = new N2MY_Storage($this->get_dsn());
        $total_used_size = $obj_Storage->get_total_size($user_info["user_key"]);
        if ($max_rec_size < $total_used_size) {
            // シーケンス情報変更
            $meeting_sequence_data = array(
                "record_status"     => "error",
                "record_result"     => "E1000",
                "record_end_proc"   => "",
                "record_filepath"   => "",
                );
            $this->logger2->warn("部屋の容量が足りない");
            $where = "meeting_sequence_key = ".$meeting_sequence_key;
            $result = $this->objMeetingSequence->update($meeting_sequence_data, $where);
            return false;
        }
        // メンテナンス時間を取得
        $fms_server_key = $meeting_sequence_info["server_key"];
        require_once 'classes/mgm/dbi/FmsServer.dbi.php';
        $objFmsServer = new DBI_FmsServer(N2MY_MDB_DSN);
        $where = "server_key = '".$fms_server_key."'";
        $fms_server_info = $objFmsServer->getRow($where);
        if ($fms_server_info["maintenance_time"]) {
            $disable_starttime = $fms_server_info["maintenance_time"];
            $disable_duration = 60 * 10;
        } else {
            $disable_starttime = "";
            $disable_duration = "";
        }
        $data = $meeting_info["meeting_key"].
            ",log_video" .
            "," .$this->_lang.
            ",".$meeting_sequence_info["meeting_sequence_key"];
        $room_info = $this->get_room_info($room_key);
        if ("wmv" == $room_info["room_info"]["rec_gw_convert_type"]) {
            $output_type = "0";
        } else {
            $output_type = "2";
        }
        require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
        $encrypted_data = EZEncrypt::encrypt(N2MY_ENCRYPT_KEY, N2MY_ENCRYPT_IV, $data);
        $encrypted_data = str_replace("/", "-", $encrypted_data);
        $encrypted_data = str_replace("+", "_", $encrypted_data);
        $ondemand_url = N2MY_LOCAL_URL."rec_gw/".$encrypted_data;
        $callback_url = N2MY_LOCAL_URL."services/api.php?action_record_callback";
        $url = $this->config->get("RECORD_GW", "url")."api/receive.php";
        $user_id = $user_info["user_id"];
        $path = $user_id."/".$meeting_info["room_key"]."/";
        $file = $meeting_session_id."_".$meeting_sequence_key.".".$room_info["room_info"]["rec_gw_convert_type"];
        // umaskを設定し元に戻す
        $old = umask(0);
        @mkdir(N2MY_MOVIES_DIR.$path, 0777, true);
        umask($old);
        $ch = curl_init();
        $post_data = array(
            "key"               => $meeting_session_id."_".$meeting_sequence_info["meeting_sequence_key"],
            "play_url"          => $ondemand_url,
            "output_type"       => $output_type,       // movファイル
            "duration"          => $meeting_sequence_info["record_second"],
            "callback_url"      => $callback_url,
            "output_fqdn"       => $_SERVER["SERVER_NAME"],
            "scp_user"          => $this->config->get("RECORD_GW", "user"),
            "output_file"       => N2MY_MOVIES_DIR.$path.$file,
            "disable_starttime" => $disable_starttime,
            "disable_duration"  => $disable_duration,
            );
        $this->logger->info("post_data",__FILE__,__LINE__,$post_data);
        $option = array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => 1,
            CURLOPT_TIMEOUT        => 10,
            CURLOPT_POST           => 10,
            CURLOPT_POSTFIELDS     => $post_data,
            );
        $this->logger2->info($option);
        curl_setopt_array($ch, $option);
        $ret = curl_exec($ch);
        $xml = new EZXML();
        $data = $xml->openXML($ret);
        if (!$data) {
            $this->logger2->info($ret, "XML DATA parse error!!");
            return false;
        }
        if ($data["response"]["status"][0]["_data"]) {
            $where                  = "meeting_sequence_key = ".$meeting_sequence_key;
            $record_id              = $data["response"]["queue_key"][0]["_data"];
            $record_endtime         = $data["response"]["record_end"][0]["_data"];
            $record_end_proc        = ($_delete_meeting_data[$meeting_sequence_key] == "1") ? "delete" : "";
            $meeting_sequence_data  = array("record_id"       => $record_id,
                                            "record_status"   => "convert",
                                            "record_endtime"  => $record_endtime,
                                            "record_end_proc" => $record_end_proc,
                                            "record_filepath" => $path.$file,);
            $this->logger2->info(array($meeting_sequence_data, $where));
            $result = $this->objMeetingSequence->update($meeting_sequence_data, $where);
            $this->add_operation_log('meetinglog_create_movie', array(
                'room_key'      => $meeting_info['room_key'],
                'meeting_name'  => $meeting_info['meeting_name']
                ));
            if (DB::isError($result)) {
                $this->logger2->error($result->getUserInfo());
                return false;
            }
            require_once 'classes/mgm/MGM_Auth.class.php';
            $objMgmRelationTable = new MGM_AuthClass(N2MY_MDB_DSN);
            $this->logger2->info(array($user_id, $record_id, "record_gw"));
            if (!$result = $objMgmRelationTable->addRelationData($user_id, $record_id, "record_gw")) {
                return false;
            } else {
                $this->logger2->info(array($option, $meeting_sequence_data));
                file_get_contents($this->config->get("RECORD_GW", "url")."api/dispach.php");
                return true;
            }
        } else {
            $err_cd = $data["response"]["error"][0]["code"][0]["_data"];
            $err_msg = $data["response"]["error"][0]["message"][0]["_data"];
            // シーケンス情報変更
            $meeting_sequence_data = array(
                "record_status"   => "error",
                "record_result"   => $err_cd,
                "record_end_proc" => "",
                "record_filepath" => "",
                );
            $this->logger2->warn(array($err_cd, $err_msg));
            $where  = "meeting_sequence_key = ".$meeting_sequence_key;
            $result = $this->objMeetingSequence->update($meeting_sequence_data, $where);
            if (DB::isError($result)) {
                $this->logger2->error($result->getUserInfo());
                return false;
            }
            return false;
        }
    }

    /**
     * キャンセル処理
     */
    function action_cancel_movie() {
        if ($this->check_submit_key()) {
            $this->_cancel_movie();
        }
        return $this->action_showDetail();
    }

    function _cancel_movie() {
        $meeting_session_id   = $this->request->get("meeting_session_id");
        $meeting_sequence_key = $this->request->get("meeting_sequence_key");
        // 会議情報
        $where = "meeting_session_id = '".$meeting_session_id."'" .
            " AND room_key = '".addslashes($this->get_room_key())."'" .
            " AND is_deleted = 0";
        $meeting_info = $this->obj_Meeting->getRow($where, "meeting_key, user_key, room_key");
        $this->logger2->info($meeting_info);
        if (!$meeting_info) {
            $this->logger2->warn($where);
            return false;
        }
        // シーケンス情報
        $where = "meeting_key = '".$meeting_info["meeting_key"]."'" .
            " AND meeting_sequence_key = ".$meeting_sequence_key.
            " AND record_status = 'convert'".
            " AND has_recorded_video = 1";
        $meeting_sequence_info = $this->objMeetingSequence->getRow($where);
        $this->logger2->info($meeting_sequence_info);
        if (!$meeting_sequence_info) {
            $this->logger2->warn($where);
            return false;
        }
        // キャンセル処理
        file_get_contents($this->config->get("RECORD_GW", "url")."api/cancel.php?queue_key=".$meeting_sequence_info["record_id"]);
        $where = "meeting_sequence_key = ".$meeting_sequence_key;
        $meeting_sequence_data = array(
            "record_id"       => "",
            "record_endtime"  => "",
            "record_status"   => "",
            "record_result"   => "",
            "record_end_proc" => "",
            "record_filepath" => "",
            );
        $this->logger2->info(array($meeting_sequence_data, $where));
        $result = $this->objMeetingSequence->update($meeting_sequence_data, $where);
        if (DB::isError($result)) {
            $this->logger2->error($result->getUserInfo());
            return false;
        }
        // ワンタイムURL破棄
        require_once 'classes/mgm/MGM_Auth.class.php';
        $objMgmRelationTable = new MGM_AuthClass(N2MY_MDB_DSN);
        $objMgmRelationTable->deleteRelationData($meeting_sequence_info["record_id"], "record_gw");
        return true;
    }

    /**
     * 削除
     */
    function action_delete_movie() {
        if ($this->check_submit_key()) {
            $this->_delete_movie();
        }
        return $this->action_showDetail();
    }

    function _delete_movie() {
        $meeting_session_id   = $this->request->get("meeting_session_id");
        $meeting_sequence_key = $this->request->get("meeting_sequence_key");
        // 会議情報
        $where = "meeting_session_id = '".$meeting_session_id."'" .
            " AND room_key = '".addslashes($this->get_room_key())."'" .
            " AND is_deleted = 0";
        $meeting_info = $this->obj_Meeting->getRow($where, "meeting_key, user_key, room_key");
        $this->logger2->info($meeting_info);
        if (!$meeting_info) {
            $this->logger2->warn($where);
            return false;
        }
        // シーケンス情報
        $where = "meeting_key = '".$meeting_info["meeting_key"]."'" .
            " AND meeting_sequence_key = ".$meeting_sequence_key.
            " AND record_status = 'complete'";
        $meeting_sequence_info = $this->objMeetingSequence->getRow($where);
        $this->logger2->info($meeting_sequence_info);
        if (!$meeting_sequence_info) {
            $this->logger2->warn($where);
            return false;
        }
        $where = "meeting_sequence_key = ".$meeting_sequence_key;
        $meeting_sequence_data = array(
            "record_id"         => "",
            "record_endtime"    => "",
            "record_status"     => "",
            "record_result"     => "",
            "record_end_proc"   => "",
            "record_filepath"   => "",
            );
        $this->logger2->info(array($meeting_sequence_data, $where));
        $result = $this->objMeetingSequence->update($meeting_sequence_data, $where);
        if (DB::isError($result)) {
            $this->logger2->error($result->getUserInfo());
            return false;
        }
        $this->logger2->info(N2MY_MOVIES_DIR.$meeting_sequence_info["record_filepath"], "Delete movie file");
        unlink(N2MY_MOVIES_DIR.$meeting_sequence_info["record_filepath"]);
        $meetingLog = new N2MY_MeetingLog($this->get_dsn());
        $meetingLog->getMeetingSize($meeting_session_id);
        return true;
    }

    /**
     * 変換されたファイルをダウンロード
     */
    function action_download_movie() {
        $meeting_session_id    = $this->request->get("meeting_session_id");
        $meeting_sequence_key  = $this->request->get("meeting_sequence_key");
        $room_key              = $this->request->get("room_key");
        $sequence_no           = $this->request->get("no");
        $useragent = getenv("HTTP_USER_AGENT");
        // 会議情報
        $where = "meeting_session_id = '".$meeting_session_id."'" .
            " AND room_key = '".addslashes($this->get_room_key())."'" .
            " AND is_deleted = 0";
        $meeting_info = $this->obj_Meeting->getRow($where, "meeting_key, meeting_name, user_key, room_key");
        $this->logger2->info($meeting_info);
        if (!$meeting_info) {
            $this->logger2->warn($where);
            $this->display("user/404_error.t.html");
            return false;
        }
        // シーケンス情報
        $where = "meeting_key = '".$meeting_info["meeting_key"]."'" .
            " AND meeting_sequence_key = ".$meeting_sequence_key.
            " AND record_status = 'complete'";
        $meeting_sequence_info = $this->objMeetingSequence->getRow($where);
        $this->logger2->info($meeting_sequence_info);
        if (!$meeting_sequence_info) {
            $this->logger2->warn($where);
            $this->display("user/404_error.t.html");
            return false;
        }
        $file = N2MY_MOVIES_DIR.$meeting_sequence_info["record_filepath"];
        if (!file_exists($file)) {
            $this->logger2->warn($file, "File not exists");
            $this->display("user/404_error.t.html");
            return false;
        }
        $extension = end(explode('.', $meeting_sequence_info["record_filepath"]));
        if ($meeting_info["meeting_name"]) {
            $file_name = $meeting_info["meeting_name"]."_".$sequence_no.".".$extension;
        } else {
            $file_name = "no_title_".$sequence_no.".".$extension;
        }
        if (ereg("MSIE", $useragent)) {
            $file_name = mb_convert_encoding( $file_name , "SJIS-win" );
            Header('Pragma: private');
            Header('Cache-Control: private');
/*
        } elseif (ereg( "Safari", $useragent)) {
            //Safariの場合は全角文字が全て化けるので、何かしら固定のファイル名にして回避
            $file_name = "";
*/
        }
        $this->logger2->info($meeting_sequence_info);
        header("Content-Disposition: attachment; filename=" . $file_name );
        header("Content-Type: application/octet-stream; name=" . $file_name );
        header("Content-Length: " . filesize($file) );
        ob_end_flush();
        $fp = fopen($file, "r");
        while($str = fread($fp, 4096)) {
            print $str;
        }
        fclose($fp);
    }

    function action_meeting_suggest_log() {
    	$meeting_key = $this->request->get("id");
    	require_once("classes/dbi/meeting_suggest_log.dbi.php");
    	$obj_MeetingSuggestLog = new MeetingSuggestTable($this->get_dsn());
    	$meeting_suggest = $obj_MeetingSuggestLog->getRowsAssoc(sprintf( "meeting_key='%s'", $meeting_key));//getSuggestByMeetingKey($meeting_key);
        foreach($meeting_suggest as $key => $data) {
        	$meeting_suggest[$key]["suggest_time"] = date("Y-m-d H:i:s", EZDate::getLocateTime( $data["suggest_time"], N2MY_USER_TIMEZONE, N2MY_SERVER_TIMEZONE));
        	switch($data["status_type"]){
        		case "RTMPT80":
        			$meeting_suggest[$key]["suggest_text"] = STATE_ALERT_HISTORY_MESSAGE_LABEL_PROTOCOL_01;
        			break;
        		case "RTMPT8080":
        			$meeting_suggest[$key]["suggest_text"] = STATE_ALERT_HISTORY_MESSAGE_LABEL_PROTOCOL_02;
        			break;
        		case "LowSpeedOfCircuit":
        			$meeting_suggest[$key]["suggest_text"] = STATE_ALERT_HISTORY_MESSAGE_LABEL_SPEED_OF_CIRCUIT;
        			break;
        		case "UseCpuReduction":
        			$meeting_suggest[$key]["suggest_text"] = STATE_ALERT_HISTORY_MESSAGE_LABEL_PING_FAILED;
        			break;
        		case "NetConnection.PingFailed":
        			$meeting_suggest[$key]["suggest_text"] = STATE_ALERT_HISTORY_MESSAGE_LABEL_PING_OUT;
        			break;
        		case "NetConnection.PingOut":
        			$meeting_suggest[$key]["suggest_text"] = STATE_ALERT_HISTORY_MESSAGE_LABEL_CPU_REDUCTION;
        			break;
        	}
        }
        $this->logger2->info($meeting_suggest);
         $this->template->assign("meeting_suggest"  , $meeting_suggest);
        $this->display('admin/meetinglog/suggest.t.html');
    }

    function action_download_vote_csv() {
    	require_once("classes/dbi/quick_vote_log.dbi.php");
    	$obj_quickVoteLog = new QuickVoteLogTable($this->get_dsn($server_dsn_key));
    	$where = "meeting_key = ".$this->request->get("meeting_key");
    	$actual_start_datetime = $this->obj_Meeting->getOne($where, "actual_start_datetime");
    	$results = $obj_quickVoteLog->getRowsAssoc($where);

    	// CSV出力
    	require_once ('lib/EZLib/EZUtil/EZCsv.class.php');
    	$output_encoding = $this->get_output_encoding();
    	$csv = new EZCsv(true,$output_encoding,"UTF-8");
    	$dir = $this->get_work_dir();
    	$tmpfile = tempnam($dir, "csv_");
    	$csv->open($tmpfile, "w");

    	header('Content-Type: application/octet-stream;');
    	header('Content-Disposition: attachment; filename="vote_result_'.$actual_start_datetime.'.csv"');

    	// ヘッダ
    	$header = array(
    			"num"   => "num",
    			"name"  => "name",
    			"answer"=> "answer",
    	);
    	$csv->setHeader($header);
    	$csv->write($header);

    	foreach($results as $num => $result){
    		$line["num"]    = $result["number"];
    		$line["name"]   = $result["participant_name"];
    		$line["answer"] = $result["result"];
    		$this->logger2->info($line);
    		$csv->write($line);
    	}
    	// CSV出力
    	$csv->close();
    	$fp = fopen($tmpfile, "r");
    	$contents = fread($fp, filesize($tmpfile));
    	fclose($fp);

    	print $contents;
    	unlink($tmpfile);
    }

    private function isRecorded($meeting_session_id) {
        $obj_Meeting           = new DBI_Meeting( $this->get_dsn() );
        $obj_MeetingSequence   = new DBI_MeetingSequence( $this->get_dsn() );
        $where                 = "meeting_session_id = '".addslashes($meeting_session_id)."'";
        $meeting_key           = $obj_Meeting->getOne($where, "meeting_key");
        $where                 = "meeting_key = ".$meeting_key;
        $meeting_sequence_list = $obj_MeetingSequence->getRowsAssoc($where);
        $this->logger2->info($meeting_sequence_list);
        foreach ($meeting_sequence_list as $meeting_sequence_info) {
            if ($meeting_sequence_info["record_status"] == "convert") {
                return true;
            }
        }
        return false;
    }
}

$main = new AppMeetingLog();
$main->execute();
