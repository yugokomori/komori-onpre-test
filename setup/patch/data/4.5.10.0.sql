-- メンバーの権限
ALTER TABLE `member` ADD `member_type` VARCHAR( 20 ) NOT NULL COMMENT  'メンバータイプ' AFTER  `member_status`;
-- NOT NULL制約削除
ALTER TABLE `ping_failed` CHANGE `reload_type` `reload_type` VARCHAR( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'リロードタイプ'