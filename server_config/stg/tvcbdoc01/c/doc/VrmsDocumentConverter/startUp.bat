@echo off
rem 多重起動しない

set apppath=C:\doc\VrmsDocumentConverter\
set appname=VrmsDocumentConverter.exe

:main
tasklist /fo list | find "%appname%"
if errorlevel 1 (goto run) else goto msg

:run
start %apppath%%appname%
exit

:msg
echo すでに %appname% プロセスが起動しています。
set /p end="プロセスの自動起動を行いません(Enterで終了)" 
exit
