<?php
require_once("classes/N2MY_DBI.class.php");

class IpWhiteListTable extends N2MY_DB {

    var $table = 'ip_whitelist';
    protected $primary_key = "";

    function __construct($dsn)
    {
        $this->logger =& EZLogger::getInstance();
        $this->init($dsn, $this->table);
    }
}
