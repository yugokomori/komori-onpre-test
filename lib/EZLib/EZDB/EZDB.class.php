<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
// +----------------------------------------------------------------------+
// |                                                                      |
// | [            ]                                                       |
// |                                                                      |
// +----------------------------------------------------------------------+
// | PHP version 4                                                        |
// +----------------------------------------------------------------------+
// | Authors:kiyomizu                                                     |
// |                                                                      |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006               all rights reserved.                |
// +----------------------------------------------------------------------+
//

require_once('lib/pear/DB.php');
require_once('lib/EZLib/EZDB/EZDBR.class.php');
require_once('lib/EZLib/EZCore/EZLogger.class.php');
require_once('lib/EZLib/EZCore/EZLogger2.class.php');
require_once('lib/EZLib/EZHTML/EZValidator.class.php');

define("EZDB_TYPE_NUMBER", "number");
define("EZDB_TYPE_STRING", "string");
define("EZDB_TYPE_TEXT",   "text");
define("EZDB_TYPE_ENUM",   "enum");
define("EZDB_TYPE_SET",    "set");
define("EZDB_TYPE_DATE",   "date");
define("EZDB_TYPE_FILE",   "file");

class EZDB extends DB {

    var $logger = null;
    var $logger2 = null;
    var $rules = array();
    var $table = "";
    var $table_info = "";
    var $table_dir = "";
    var $cache_dir = "";
    protected $primary_key = null;

    /**
     * 初期処理
     *
     * @param string $dsn データソース
     * @param string $table テーブル名
     */
    function init($dsn, $table = null) {
        // ログ
        $this->logger = EZLogger::getInstance();
        $this->logger2 = EZLogger2::getInstance();
        $this->_dsn = $dsn;
        // テーブル
        if (isset($table)) {
            $this->table = $table;
        }
        // ディレクトリ
        $this->table_dir = N2MY_APP_DIR."setup/tables/";
        $this->cache_dir = N2MY_APP_DIR."tmp/tables/";
        if (defined("N2MY_SELECT_DB") && ($dsn != N2MY_MGM_DB)) {
            $_dsn = array("master" => $dsn, "slave" => N2MY_SELECT_DB);
        } else {
            $_dsn = $dsn;
        }
        //create connection
        $conn =& EZDBR::connect($_dsn);
        //set connection
        if (DB::isError($conn)) {
            $this->logger2->error(array($dsn, $conn->getUserInfo()));
        } else {
            $this->_conn =& $conn;
            // キャラセット指定
            $dsn_elem = parse_url($dsn);
            if (isset($dsn_elem["query"])) {
                $query_opt = array();
                parse_str($dsn_elem["query"], $query_opt);
                if (isset($query_opt["charset"])) {
                    $this->_conn->query("SET NAMES ".$query_opt["charset"]);
                }
            }
        }
    }

    /**
     * テーブル情報取得
     * ・読み込み済のキャッシュファイルが有れば、キャッシュから情報を取得
     * ・テーブル定義XMLファイルが有れば、XMLをパースして情報を取得、キャッシュを作成
     * ・定義情報がなければ、DBから直接取得してキャッシュを作成
     */
    function getTableInfo($db_type ="") {
        $dsn = md5($this->_dsn);
        $dsn_param = parse_url($this->_dsn);
        static $table_info;
        // テーブル情報取得
        if (!isset($table_info[$dsn][$this->table])) {
            // キャッシュファイル
            if (!file_exists($this->cache_dir)) {
                mkdir ($this->cache_dir);
                chmod ($this->cache_dir, 0777);
            }
            $cache_dir = $this->cache_dir."/".$db_type."/";
            if (!file_exists($cache_dir)) {
                mkdir ($cache_dir);
                chmod ($cache_dir, 0777);
            }
            $cache_file = $cache_dir.$this->table.".cache";
            $cache_time = file_exists($cache_file) ? filemtime($cache_file) : 0;
            // テーブル情報ファイル
            $file = $this->table_dir."/".$db_type."/".$this->table.".xml";
            $this->logger->debug("file",__FILE__,__LINE__,$file);
            $file_time = file_exists($file) ? filemtime($file) : 1;
            // テーブル定義XMLファイルが存在し、キャッシュファイルより新しい場合は更新する。
            if (file_exists($file) && ($file_time > $cache_time)) {
                require_once("lib/EZLib/EZXML/EZXML.class.php");
                $contents = file_get_contents($file);
                $xml = new EZXML();
                $table_xml = $xml->openXML($contents);
                $fields = array();
                foreach($table_xml["table"]["columns"][0]["column"] as $key => $column) {
                    $item = array();
                    if (isset($column["select"])) {
                        if (isset($column["select"][0]["option"])) {
                            foreach($column["select"][0]["option"] as $option) {
                                $item["select"][] = array(
                                    "value" => $option["_attr"]["value"],
                                    "data" => $option["_data"],
                                    "selected" => isset($column["_attr"]["selected"]) ? $column["_attr"]["selected"] : $option["_attr"]["selected"],
                                    );
                            }
                        }
                    } elseif (isset($column["relation"])) {
                        $item["relation"] = array(
                            "table"       => $column["relation"][0]["_attr"]["table"],
                            "key"         => $column["relation"][0]["_attr"]["key"],
                            "value"       => $column["relation"][0]["_attr"]["value"],
                            "status_name" => $column["relation"][0]["_attr"]["status_name"],
                            "status"      => $column["relation"][0]["_attr"]["status"],
                            "db_type"     => $column["relation"][0]["_attr"]["db_type"],
                            "default_key"     => $column["relation"][0]["_attr"]["default_key"],
                            "default_value"     => $column["relation"][0]["_attr"]["default_value"],
                        );
                    }
                    $item["search"] = $column["_attr"]["search"];
                    $flags = array(
                        "not_null"        => isset($column["_attr"]["not_null"]) ? $column["_attr"]["not_null"] : "",
                        "primary_key"     => isset($column["_attr"]["primary_key"]) ? $column["_attr"]["primary_key"] : "",
                        "blob"            => isset($column["_attr"]["blob"]) ? $column["_attr"]["blob"] : "",
                        "auto_increment"  => isset($column["_attr"]["auto_increment"]) ? $column["_attr"]["auto_increment"] : "",
                    );
                    $fields[$column["_attr"]["id"]] = array(
                        "id"        => $column["_attr"]["id"],
                        "label"     => $column["_attr"]["label"],
                        "desc"      => $column["_attr"]["desc"],
                        "type"      => $column["_attr"]["type"],
                        "sub_type"  => isset($column["_attr"]["sub_type"]) ? $column["_attr"]["sub_type"] : "",
                        "mode"      => isset($column["_attr"]["mode"]) ? $column["_attr"]["mode"] : $column["_attr"]["type"],
                        "size"      => $column["_attr"]["size"],
                        "default"   => $column["_attr"]["default"],
                    	"format"   => $column["_attr"]["format"],
                        "item"      => $item,
                        "flags"      => $flags,
                        "searchform_status"    => $column["_attr"]["searchform_status"],
                        "modifyform_status"    => $column["_attr"]["modifyform_status"],
                        "addform_status"    => $column["_attr"]["addform_status"],
                        "listform_status"    => $column["_attr"]["listform_status"],
                        "except_modify"    => $column["_attr"]["except_modify"],
                        "search"    => ($column["_attr"]["search"]) ? $column["_attr"]["search"] : 1,
                        "sort"  => true,
                        );
                }
                $this->logger->trace(__FUNCTION__,__FILE__,__LINE__,$fields);
                // キャッシュファイル作成
                if ($fp = fopen($cache_file, "w")) {
                    fwrite($fp, serialize($fields));
                    fclose($fp);
                } else {
                    $this->logger->warn(__FUNCTION__."#It failed in making cash.",__FILE__,__LINE__,$this->table);
                }
            // キャッシュファイルからパース
            } else if (file_exists($cache_file)) {
                $this->logger->debug(__FUNCTION__."#cache",__FILE__,__LINE__,$this->table);
                $cash_data = file_get_contents($cache_file);
                $fields = unserialize($cash_data);
                $this->logger->debug(__FUNCTION__."#It reads from cash.",__FILE__,__LINE__,$this->table);
            // テーブル定義情報があれば取得
            } else {
                $_table_info = $this->_conn->tableInfo($this->table);
                if (DB::isError($_table_info)) {
                    $this->logger2->info($_table_info->getUserInfo());
                    return false;
                }
                $rs1 = $this->_conn->query("SHOW FULL FIELDS FROM ".$this->table);
                while($row1 = $rs1->fetchRow(DB_FETCHMODE_ASSOC)) {
                    $type = $row1["Type"];
                    $len_start = strpos($type, "(");
                    if ($len_start) {
                        $len_end = strpos($type, ")") - 1;
                        $row1["Type"] = substr($type, 0, $len_start);
                        $row1["Size"] = substr($type, $len_start + 1, ($len_end - $len_start));
                    }
                    $_table_info2[$row1["Field"]] = $row1;
                }
                $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$_table_info2);
                foreach($_table_info as $key => $field) {
                    $flags = array();
                    if ($field["flags"]) {
                        $_flags = split(" ",$field["flags"]);
                        foreach ($_flags as $flag) {
                            $flags[$flag] = true;
                        }
                    }
                    $type = strtolower(($_table_info2[$field["name"]]["Type"]) ? $_table_info2[$field["name"]]["Type"] : $field["type"]);
                    $mode = $type;
                    $this->logger->trace(__FUNCTION__."#db",__FILE__,__LINE__, array($_table_info2, $type, $mode));
                    switch ($type) {
                    // 数値
                    case "tinyint" :
                    case "smallint" :
                    case "mediumint" :
                    case "int" :
                    case "bigint" :
                    case "decimal" :
                    case "float" :
                    case "double" :
                        $type = EZDB_TYPE_NUMBER;
                        break;
                    // 文字
                    case "char" :
                    case "varchar" :
                        $type = EZDB_TYPE_STRING;
                        break;
                    // テキストエリア
                    case "tinytext" :
                    case "text" :
                    case "mediumtext" :
                    case "longtext" :
                    case "tinyblob":
                    case "blob":
                    case "mediumblob" :
                    case "longblob" :
                        $type = EZDB_TYPE_TEXT;
                        break;
                    // 単一選択
                    case "enum" :
                        $type = EZDB_TYPE_ENUM;
                        break;
                    // 複数選択
                    case "set" :
                        $type = EZDB_TYPE_SET;
                        break;
                    // 日付
                    case "datetime" :
                    case "date" :
                    case "timestamp" :
                    case "time" :
                    case "year" :
                        $type = EZDB_TYPE_DATE;
                        break;
                    }
                    $fields[$field["name"]] = array(
                        "id"        => $field["name"],
                        "label"     => ($_table_info2[$field["name"]]["Comment"]) ? $_table_info2[$field["name"]]["Comment"] : $field["name"],
                        "desc"     => ($_table_info2[$field["name"]]["Comment"]) ? $_table_info2[$field["name"]]["Comment"] : "",
                        "type"      => $type,
                        "mode"      => $mode,
                        "size"      => isset($_table_info2[$field["name"]]["Size"]) ? $_table_info2[$field["name"]]["Size"] : $field["len"],
                        "default"   => $_table_info2[$field["name"]]["Default"],
                        "sort"      => true,
                        "flags"     => $flags,
                        "search"    => 1,
                        );
                }
                $this->logger->debug(__FUNCTION__."#db",__FILE__,__LINE__, array($this->table, $_table_info, $fields));
                // キャッシュファイル作成
                if ($fp = fopen($cache_file, "w")) {
                    fwrite($fp, serialize($fields));
                    fclose($fp);
                } else {
                    $this->logger->warn(__FUNCTION__."#It failed in making cash.",__FILE__,__LINE__,$this->table);
                }
            }
            $table_info[$dsn][$this->table] = $fields;
        }
        return $table_info[$dsn][$this->table];
    }

    /**
     * テーブル情報のXMLデータを生成する
     */
    function tableInfoBuilder($db_type) {
        // テーブル情報取得
        if (!$table_info = $this->getTableInfo($db_type)) {
            return false;
        }
        //$this->logger->info("table",__FILE__,__LINE__,$table_info);
        $dsn_param = parse_url($this->_dsn);
        // テーブル情報XMLデータ
        $xml = '<table id="'.$this->table.'" name="'.$this->table.'">'."\n";
        $xml .= '<columns>'."\n";
        foreach($table_info as $key => $field) {
            $xml .= '<column' .
                    ' id ="'.$field["id"].'"' .
                    ' label="'.$field["label"].'"' .
                    ' type="'.$field["type"].'"' .
                    ' mode="'.$field["mode"].'"' .
                    ' size="'.$field["size"].'"'.
                    ' default="'.$field["default"].'"'.
                    ' searchform_status="1"'.
                    ' addform_status="1"'.
                    ' listform_status="1"'.
                    ' except_modify=""';
            if ($field["flags"]) {
                foreach($field["flags"] as $_key => $_field) {
                    $xml .= ' '.$_key.'="'.$_field.'"';
                }
            }
            $xml .= '>';
            $xml .= '</column>'."\n";
        }
        $xml .= '</columns>'."\n";
        $xml .= '</table>';
        // テーブル情報ファイル
        $xml_dir = $this->table_dir."/".$db_type."/";
        if (!file_exists($xml_dir)) {
            mkdir ($xml_dir);
            chmod ($xml_dir, 0777);
        }
        $file = $xml_dir.$this->table.".xml";
        // テーブル情報XMLファイル作成
        $xml = mb_convert_encoding($xml, "UTF-8", "AUTO");
        if ($fp = fopen($file, "w")) {
            fwrite($fp, $xml);
            fclose($fp);
        } else {
            $this->logger->error(__FUNCTION__."#It failed in making table information.",__FILE__,__LINE__,$this->table);
        }
        return $xml;
    }

    /**
     * 追加
     */
    function add($data) {
        $sth = $this->_conn->autoExecute($this->table, $data,
                                DB_AUTOQUERY_INSERT);
        if (PEAR::isError($sth)) {
            $this->logger2->error($sth->getUserInfo());
            return $sth;
        }
        if ($this->primary_key) {
            $result = mysql_insert_id();
            /**
            $sql = "SELECT MAX(".$this->primary_key.") FROM ".$this->table;
            $result = $this->_conn->getOne($sql);
            if (PEAR::isError($result)) {
                $this->logger2->error($result->getUserInfo());
                return false;
            }
            */
            return $result;
        }
    }

    /**
     * 置換
     */
    function replace($data) {
        if (is_array($data) && $data) {
            foreach($data as $column => $value) {
                $columns[] = $column;
                $values[] = "'".mysql_real_escape_string($value)."'";
            }
            $sql = "REPLACE INTO ".
                $this->table.
                " (".join(",", $columns).")" .
                " VALUES " .
                "(".join(",", $values).")";
            $result = $this->_conn->query($sql);
            if (PEAR::isError($result)) {
                $this->logger2->error($result->getUserInfo());
                return false;
            }
            return $result;
        }
    }

    /**
     * 更新処理
     */
    function update($data, $where) {
        // 無条件にすべて削除はさせない。
        // 必ず別のメソッドで実装してください！！！
        if (!$where) {
            $this->logger2->error(array($data, $where), "where句の指定がありません");
            return false;
        } else {
            if (is_numeric($where)) {
                $this->logger2->error(array($data, $where), "where句の指定がありません");
                return false;
            }
        }
        $sth = $this->_conn->autoExecute($this->table, $data,
                                DB_AUTOQUERY_UPDATE, $where);
        if (PEAR::isError($sth)) {
            $this->logger2->error($sth->getUserInfo());
            return $sth;
        }
    }

    /**
     * 削除処理
     */
    function remove($where) {
        // 無条件にすべて削除はさせない。
        // 必ず別のメソッドで実装してください！！！
        if (!$where) {
            $this->logger2->error($where, "where句の指定がありません");
            return false;
        } else {
            if (is_numeric(trim($where))) {
                $this->logger2->error($where, "where句の指定がありません");
                return false;
            }
        }
        $sql = "delete from ".$this->table." WHERE ".$where;
        $this->logger->trace(__FUNCTION__."sql", __FILE__, __LINE__, $sql);
        $this->_conn->query($sql);
    }

    /**
     * クエリーを自動で作成
     */
    public function getWhere($columns, $request, $and_or = "AND") {
        $where = "";
        $and_or = " ".$and_or." ";
        $condition = array();
        $fields = "";
        foreach($columns as $key => $item){
            $fields .= ",".$key;
            if (!isset($request[$key])) {
                continue;
            }
            switch ($item["mode"]) {
            // 数値
            case "tinyint" :
            case "smallint" :
            case "mediumint" :
            case "int" :
            case "bigint" :
            case "decimal" :
            case "float" :
            case "double" :
                if($item["item"]["search"] == "range") {
                    if ($request[$key]["min"] && !is_numeric($request[$key]["min"])) {
                        break;
                    }
                    if ($request[$key]["max"] && !is_numeric($request[$key]["max"])) {
                        break;
                    }
                } else {
                    if (!is_numeric($request[$key])) {
                        break;
                    }
                }
            // 日付/数値表記
            case "timestamp" :
            case "time" :
            case "year" :
                switch($item["item"]["search"]) {
                // 同一
                case "equal":
                    $condition[] = $key." = ".addslashes($request[$key]);
                    break;
                // 範囲指定
                case "range":
                    if ($request[$key]["min"]) {
                        $condition[] = $key." >= ".addslashes($request[$key]["min"]);
                    }
                    if ($request[$key]["max"]) {
                        $condition[] = $key." <= ".addslashes($request[$key]["max"]);
                    }
                    break;
                case "like":
                    $condition[] = $key." like '".addslashes($request[$key])."%'";
                    break;
                // 同一
                default :
                    if ($request[$key] || $request[$key] == "0") {
                        $condition[] = $key." = ".addslashes($request[$key]);
                    }
                    break;
                }
                break;
            // 文字列
            case "char" :
            case "varchar" :
            case "tinytext" :
            case "text" :
            case "mediumtext" :
            case "string" :
            case "longtext" :
            case "tinyblob":
            case "blob":
            case "mediumblob" :
            case "longblob" :
            case "email" :
                if ($request[$key]) {
                    switch($item["item"]["search"]) {
                    // 同一
                    case "equal":
                        $condition[] = $key." = '".addslashes($request[$key])."'";
                        break;
                    case "prefix":
                        $condition[] = $key." like '".addslashes($request[$key])."%'";
                        break;
                    case "suffix":
                        $condition[] = $key." like '%".addslashes($request[$key])."'";
                        break;
                    case "like":
                        $condition[] = $key." like '%".addslashes($request[$key])."%'";
                        break;
                    default :
                        $condition[] = $key." = '".addslashes($request[$key])."'";
                        break;
                    }
                }
                break;
            case "datetime" :
            case "date" :
                if ($request[$key]) {
                    switch($item["item"]["search"]) {
                    case "equal":
                        $condition[] = $key." = '".addslashes($request[$key]["min"])."'";
                        break;
                    case "range":
                        if ($request[$key]["min"]) {
                            $condition[] = $key." >= '".addslashes($request[$key]["min"])."'";
                        }
                        if ($request[$key]["max"]) {
                            $condition[] = $key." <= '".addslashes($request[$key]["max"])."'";
                        }
                        break;
                    case "like":
                        $condition[] = $key." like '".addslashes($request[$key])."%'";
                        break;
                    default :
                        if ($request[$key]["min"]) {
                            $condition[] = $key." >= '".addslashes($request[$key]["min"])."'";
                        }
                        if ($request[$key]["max"]) {
                            $condition[] = $key." <= '".addslashes($request[$key]["max"])."'";
                        }
                        break;
                    }
                }
                break;
            case "enum":
                if (is_array($request[$key])) {
                    $_and_or = ($request[$key]["__AND_OR"]) ? " ".$request[$key]["__AND_OR"]." " : " or ";
                    unset($request[$key]["__AND_OR"]);
                    $_cond = array();
                    foreach($request[$key] as $_key => $_val) {
                        $_cond[] = $key." = '".addslashes($_val)."'";
                    }
                    $condition[] = "(".implode($_and_or, $_cond).")";
                } else {
                    $condition[] = $key." = '".addslashes($request[$key])."'";
                }
                break;
            case "set":
                if (is_array($request[$key])) {
                    foreach($request[$key] as $_key => $_val) {
                        $condition[] = $key." = '".addslashes($_val)."'";
                    }
                } else {
                    $condition[] = $key." = '".addslashes($request[$key])."'";
                }
                break;
            }
        }
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$condition);
        if ($condition) {
            $where = implode($and_or, $condition);
        }
        return $where;
    }

    /**
     * 件数取得
     *
     * @param string $where 条件式
     * @return integer 件数
     */
    function numRows($where = "") {
        $sql = "SELECT count(*) FROM ". $this->table;
        if ($where) {
            $sql .= " WHERE ".$where;
        }
        $count = $this->_conn->getOne($sql);
        if (DB::isError($count)) {
            $this->logger2->error($count->getUserInfo());
            return false;
        }
        return $count;
    }
    /**
     * 件数取得
     *
     * @param string $where 条件式
     * @return integer 件数
     */
    function numRowsInnerJoin($table, $relate, $where = "") {
    	$sql = "SELECT count(*) FROM ". $this->table;
    	$sql .= " JOIN ".$table;
    	$sql .= " ON ".$relate;
    	if ($where) {
    		$sql .= " WHERE ".$where;
    	}
    	$count = $this->_conn->getOne($sql);
    	if (DB::isError($count)) {
    		$this->logger2->error($count->getUserInfo());
    		return false;
    	}
    	return $count;
    }

    /**
     * データ取得
     *
     * @param string $where 条件式
     * @param string $sort_key ソートフィールド
     * @param string $sort_type desc:昇順、asc:降順（デフォルト）
     * @param integer $limit 取得件数
     * @param integer $offset 開始行
     * @param string columns フィールドをカンマ区切りで（SQL92にあわせてくれれば何でもOK）
     * @return object DB_resultオブジェクト
     *
     */
    function select($where = "", $sort = array(), $limit = null, $offset = 0, $columns = "*", $force = ""){
        if (!$columns) {
            $columns = "*";
        }
        $sql = "SELECT ". $columns . " FROM ". $this->table;
        if ($force) {
            $sql .= " force index (".$force.")";
        }
        if ($where) {
            $sql .= " WHERE ".$where;
        }
        if ($sort) {
            $sql .= " ORDER BY ";
            $i = 0;
            foreach($sort as $field => $type) {
                if ($i > 0) {
                    $sql .= ",";
                }
                // ランダム
                if ($field == "RAND()") {
                    $sql .= " RAND()";
                } else {
                    if (strtolower($type) == "desc") {
                        $sql .= $field." DESC";
                    } else {
                        $sql .= $field." ASC";
                    }
                }
                $i++;
            }
        }
        if ($limit !== null) {
            if (!is_int((int)$limit) || $limit < 1) {
                $limit = 10;
            }
            if (!$offset || !is_int((int)$offset) || $offset < 1) {
                $offset = 0;
            }
            $ret = $this->_conn->limitQuery($sql, $offset, $limit);
        } else {
            $ret = $this->_conn->query($sql);
        }
        if (DB::isError($ret)) {
            $this->logger2->error($ret->getUserInfo());
            return false;
        }
        return $ret;
    }

    /**
     * 連想配列で取得
     *
     * @param string where 条件
     * @param array
     */
    function getRowsAssoc($where = "", $sort = array(), $limit = null, $offset = 0, $columns = "*", $key = null) {
        $rs = $this->select($where, $sort, $limit, $offset, $columns);
        if (DB::isError($rs)) {
            $this->logger2->error($rs->getUserInfo());
            return array();
        }
        $ret = array();
        while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            if ($key) {
                $ret[$row[$key]] = $row;
            } else {
                $ret[] = $row;
            }
        }
        return $ret;
    }

    /**
     * 1行目のみ連想配列で返す
     */
    function getRow($where, $columns = "*", $sort = array(), $limit = null, $offset = 0, $force = "") {
        $rs = $this->select($where, $sort, $limit, $offset, $columns, $force);
        if (DB::isError($rs)) {
            $this->logger2->error($rs->getUserInfo());
            return $rs;
        }
        $row = $rs->fetchRow(DB_FETCHMODE_ASSOC);
        return $row;
    }

    /**
     * 1列のみ返す(連想配列は使わない)
     */
    function getCol($where, $column, $sort = array(), $limit = null, $offset = 0) {
        if (!$column) {
            $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,$where, $column);
            return false;
        }
        $rs = $this->select($where, $sort, $limit, $offset, $column);
        if (DB::isError($rs)) {
            $this->logger2->error($rs->getUserInfo());
            return $rs;
        }
        $cols = array();
        while($row = $rs->fetchRow(DB_FETCHMODE_ORDERED)) {
            $cols[] = $row[0];
        }
        return $cols;
    }

    /**
     * 1行目の1カラムだけ返す
     */
    function getOne($where, $column, $sort = array(), $limit = null, $offset = 0) {
        $rs = $this->select($where, $sort, $limit, $offset, $column);
        if (DB::isError($rs)) {
            return false;
        }
        $row = $rs->fetchRow(DB_FETCHMODE_ORDERED);
        return $row[0];
    }
    
    /**
     * テーブルがジョインで検索データ取得
     *
     * @param string $where 条件式
     * @param string $sort_key ソートフィールド
     * @param string $sort_type desc:昇順、asc:降順（デフォルト）
     * @param integer $limit 取得件数
     * @param integer $offset 開始行
     * @param string columns フィールドをカンマ区切りで（SQL92にあわせてくれれば何でもOK）
     * @return object DB_resultオブジェクト
     *
     */
    function innerJoin($table, $relate, $where = "", $sort = array(), $limit = null, $offset = 0, $columns = "*") {
    	$sql = "SELECT ". $columns . " FROM ". $this->table;
    	$sql .= " JOIN ".$table;
    	$sql .= " ON ".$relate;
    	if ($where) {
    		$sql .= " WHERE ".$where;
    	}
    	if ($sort) {
    		$sql .= " ORDER BY ";
    		$i = 0;
    		foreach($sort as $field => $type) {
    			if ($i > 0) {
    				$sql .= ",";
    			}
    			// ランダム
    			if ($field == "RAND()") {
    				$sql .= " RAND()";
    			} else {
    				if (strtolower($type) == "desc") {
    					$sql .= $field." DESC";
    				} else {
    					$sql .= $field." ASC";
    				}
    			}
    			$i++;
    		}
    	}
    	if ($limit !== null) {
    		if (!is_int((int)$limit) || $limit < 1) {
    			$limit = 10;
    		}
    		if (!$offset || !is_int((int)$offset) || $offset < 1) {
    			$offset = 0;
    		}
    		$this->logger->debug("sql",__FILE__,__LINE__,$sql);
    		$rs = $this->_conn->limitQuery($sql, $offset, $limit);
    	} else {
    		$rs = $this->_conn->query($sql);
    	}
    	if (DB::isError($rs)) {
    		$this->logger2->error($rs->getUserInfo());
    		$this->logger->debug("sql",__FILE__,__LINE__,"s");
    		return false;
    	}
    	$ret = array();
        while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            if ($key) {
                $ret[$row[$key]] = $row;
            } else {
                $ret[] = $row;
            }
        }
        return $ret;
    }

    /**
     * 入力チェック
     *
     * @author kiyomizu
     * @param array $data フィールド名を連想配列形式で渡す
     * @return object EZValidatorのインスタンス
     *
     * EZValidator::isError()などでエラーの存在チェックを行ってください
     */
    function check($data, $rules = null) {
        if ($rules !== null) {
            $_rules = $rules;
        } else {
            $_rules = $this->rules;
        }
        $check_obj = new EZValidator($data);
        foreach($_rules as $field => $rules) {
            $check_obj->check($field, $rules);
        }
        return $check_obj;
    }

    /**
     *
     */
    function parse_query($data) {
        // set default values
        $timeout_passed = FALSE;
        $error = FALSE;
        $read_multiply = 1;
        $finished = FALSE;
        $offset = 0;
        $max_sql_len = 0;
        $file_to_unlink = '';
        $sql_query = '';
        $sql_query_disabled = FALSE;
        $go_sql = FALSE;
        $executed_queries = 0;
        $run_query = TRUE;
        $charset_conversion = FALSE;
        $reset_charset = FALSE;
        $bookmark_created = FALSE;
        // init values
        $sql_delimiter = ';';
        $sql = '';
        $start_pos = 0;
        $i = 0;
        // Append new data to buffer
        $buffer = $data;
        // Current length of our buffer
        $len = strlen($buffer);
        // Grab some SQL queries out of it
        while ($i < $len) {
            $found_delimiter = false;
            // Find first interesting character, several strpos seem to be faster than simple loop in php:
            //while (($i < $len) && (strpos('\'";#-/', $buffer[$i]) === FALSE)) $i++;
            //if ($i == $len) break;
            $oi = $i;
            $p1 = strpos($buffer, '\'', $i);
            if ($p1 === FALSE) {
                $p1 = 2147483647;
            }
            $p2 = strpos($buffer, '"', $i);
            if ($p2 === FALSE) {
                $p2 = 2147483647;
            }
            $p3 = strpos($buffer, $sql_delimiter, $i);
            if ($p3 === FALSE) {
                $p3 = 2147483647;
            } else {
                $found_delimiter = true;
            }
            $p4 = strpos($buffer, '#', $i);
            if ($p4 === FALSE) {
                $p4 = 2147483647;
            }
            $p5 = strpos($buffer, '--', $i);
            if ($p5 === FALSE || $p5 >= ($len -2) || $buffer[$p5 +2] > ' ') {
                $p5 = 2147483647;
            }
            $p6 = strpos($buffer, '/*', $i);
            if ($p6 === FALSE) {
                $p6 = 2147483647;
            }
            $p7 = strpos($buffer, '`', $i);
            if ($p7 === FALSE) {
                $p7 = 2147483647;
            }
            $i = min($p1, $p2, $p3, $p4, $p5, $p6, $p7);
            unset ($p1, $p2, $p3, $p4, $p5, $p6, $p7);
            if ($i == 2147483647) {
                $i = $oi;
                if (!$finished) {
                    break;
                }
                // at the end there might be some whitespace...
                if (trim($buffer) == '') {
                    $buffer = '';
                    $len = 0;
                    break;
                }
                // We hit end of query, go there!
                $i = strlen($buffer) - 1;
            }

            // Grab current character
            $ch = $buffer[$i];

            // Quotes
            if (!(strpos('\'"`', $ch) === FALSE)) {
                $quote = $ch;
                $endq = FALSE;
                while (!$endq) {
                    // Find next quote
                    $pos = strpos($buffer, $quote, $i +1);
                    // No quote? Too short string
                    if ($pos === FALSE) {
                        // We hit end of string => unclosed quote, but we handle it as end of query
                        if ($finished) {
                            $endq = TRUE;
                            $i = $len -1;
                        }
                        break;
                    }
                    // Was not the quote escaped?
                    $j = $pos -1;
                    while ($buffer[$j] == '\\')
                        $j--;
                    // Even count means it was not escaped
                    $endq = (((($pos -1) - $j) % 2) == 0);
                    // Skip the string
                    $i = $pos;
                }
                if (!$endq) {
                    break;
                }
                $i++;
                // Aren't we at the end?
                if ($finished && $i == $len) {
                    $i--;
                } else {
                    continue;
                }
            }

            // Not enough data to decide
            if ((($i == ($len -1) && ($ch == '-' || $ch == '/')) || ($i == ($len -2) && (($ch == '-' && $buffer[$i +1] == '-') || ($ch == '/' && $buffer[$i +1] == '*')))) && !$finished) {
                break;
            }

            // Comments
            if ($ch == '#' || ($i < ($len -1) && $ch == '-' && $buffer[$i +1] == '-' && (($i < ($len -2) && $buffer[$i +2] <= ' ') || ($i == ($len -1) && $finished))) || ($i < ($len -1) && $ch == '/' && $buffer[$i +1] == '*')) {
                // Copy current string to SQL
                if ($start_pos != $i) {
                    $sql .= substr($buffer, $start_pos, $i - $start_pos);
                }
                // Skip the rest
                $j = $i;
                $i = strpos($buffer, $ch == '/' ? '*/' : "\n", $i);
                // didn't we hit end of string?
                if ($i === FALSE) {
                    if ($finished) {
                        $i = $len -1;
                    } else {
                        break;
                    }
                }
                // Skip *
                if ($ch == '/') {
                    // Check for MySQL conditional comments and include them as-is
                    if ($buffer[$j +2] == '!') {
                        $comment = substr($buffer, $j +3, $i - $j -3);
                        if (preg_match('/^[0-9]{5}/', $comment, $version)) {
                            if ($version[0] <= PMA_MYSQL_INT_VERSION) {
                                $sql .= substr($comment, 5);
                            }
                        } else {
                            $sql .= $comment;
                        }
                    }
                    $i++;
                }
                // Skip last char
                $i++;
                // Next query part will start here
                $start_pos = $i;
                // Aren't we at the end?
                if ($i == $len) {
                    $i--;
                } else {
                    continue;
                }
            }

            // End of SQL
            if ($found_delimiter || ($finished && ($i == $len -1))) {
                $tmp_sql = $sql;
                if ($start_pos < $len) {
                    $length_to_grab = $i - $start_pos;
                    if (!$found_delimiter) {
                        $length_to_grab++;
                    }
                    $tmp_sql .= substr($buffer, $start_pos, $length_to_grab);
                    unset ($length_to_grab);
                }
                // Do not try to execute empty SQL
                if (!preg_match('/^([\s]*;)*$/', trim($tmp_sql))) {
                    $sql = $tmp_sql;
                    $querys[] = $tmp_sql;
                    //                    $this->PMA_importRunQuery($sql, substr($buffer, 0, $i + strlen($sql_delimiter)));
                    $buffer = substr($buffer, $i +strlen($sql_delimiter));
                    // Reset parser:
                    $len = strlen($buffer);
                    $sql = '';
                    $i = 0;
                    $start_pos = 0;
                    // Any chance we will get a complete query?
                    //if ((strpos($buffer, ';') === FALSE) && !$finished) {
                    if ((strpos($buffer, $sql_delimiter) === FALSE) && !$finished) {
                        break;
                    }
                } else {
                    $i++;
                    $start_pos = $i;
                }
            }
        } // End of parser loop
        return $querys;
    }

}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
