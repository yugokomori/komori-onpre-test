<?php
require_once("classes/N2MY_DBI.class.php");

class MgmServerTable extends N2MY_DB {

    function __construct( $dsn ) {
        $this->init( $dsn, "db_server" );
    }

    public function get_server_info( $server_key ){
        $server_info = $this->getRow( sprintf( "server_key='%d' AND server_status=1", $server_key ) );
        $dsn_list = split( ";", $server_info["dsn"]);
        $db = new DB();
        foreach( $dsn_list as $key => $dsn ){
            $db_conn = DB::connect( $dsn );
            if (DB::isError($db_conn)) {
                $this->logger2->error(array(
                    '標準のメッセージ' => $db_conn->getMessage(),
                    '標準のコード ' => $db_conn->getCode(),
                    'DBMS 固有のユーザメッセージ: ' => $db_conn->getUserInfo(),
                    'DBMS 固有のデバッグメッセージ: ' => $db_conn->getDebugInfo()
                    ));
            } else {
                return array(
                        "server_key"    => $server_key,
                        "host_name"        => $server_info["host_name"],
//                        "host_name"        => sprintf( "%s_%s", $server_info["host_name"], ( 0 == $key ) ? "primary" : "secondary" ),
                        "dsn"            => $dsn );
            }
        }
        return false;
    }
}