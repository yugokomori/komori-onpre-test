ALTER TABLE  `ip_whitelist` ADD `comment` varchar(50)  NULL COMMENT 'IP制限用コメント' DEFAULT  null AFTER  `ip`;

CREATE TABLE `pgi_session` (
  `pgi_session_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'プライマリキー',
  `system_key` varchar(32) DEFAULT NULL COMMENT 'pgiのシステムキー',
  `company_id` varchar(64) DEFAULT NULL COMMENT 'CompanyID',
  `pgi_session_token` varchar(255) DEFAULT NULL COMMENT 'Token',
  `update_datetime` datetime NOT NULL COMMENT 'Login Time',
  PRIMARY KEY (`pgi_session_key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

INSERT INTO `participant_type` VALUES(22, 'teleconference', NOW(), NULL);

/* room制でのストレージ容量を */
ALTER TABLE  `service` ADD `add_storage_size` integer  NULL COMMENT 'room制用、1部屋毎ストレージ容量' DEFAULT  1000 AFTER  `member_storage_size`;
ALTER TABLE  `service` ADD `lang_allow` varchar(100)  NULL COMMENT '言語許可リスト(「,」区切りで指定nullの場合は全許可)' DEFAULT  null AFTER  `add_storage_size`;

INSERT INTO `service` (`service_key`, `country_key`, `service_name`, `max_seat`, `max_audience_seat`, `max_whiteboard_seat`, `max_guest_seat`, `max_guest_seat_flg`, `extend_max_seat`, `extend_max_audience_seat`, `extend_seat_flg`, `meeting_limit_time`, `max_room_bandwidth`, `max_user_bandwidth`, `min_user_bandwidth`, `default_camera_size`, `disable_rec_flg`, `whiteboard_video`, `meeting_ssl`, `desktop_share`, `hdd_extention`, `high_quality`, `mobile_phone`, `h323_client`, `whiteboard`, `multicamera`, `telephone`, `smartphone`, `record_gw`, `h264`, `global_link`, `hd_flg`, `sales_flg`, `use_room_plan`, `use_member_plan`, `use_sales_plan`, `whiteboard_page`, `whiteboard_size`, `document_filetype`, `image_filetype`, `cabinet_filetype`, `service_initial_charge`, `service_charge`, `service_additional_charge`, `service_freetime`, `service_registtime`, `service_status`, `service_expire_day`, `member_storage_size`, `add_storage_size`, `lang_allow`) VALUES
(108, 1, 'BIZ＋', 5, 0, 0, 0, 0, 0, 0, 0, 0, 3072, 384, 80, '320x240', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, '0000-00-00 00:00:00', 1, 0, 50, 1000, 'ja,en'),
(109, 1, 'プロスパート(プロスパID)', 10, 0, 10, 0, 0, 0, 0, 0, 0, 3072, 384, 80, '320x240', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, '0000-00-00 00:00:00', 1, 0, 200, 1000, 'ja,en'),
(110, 1, 'プロスパート(プロスパルーム2)', 5, 0, 5, 5, 1, 0, 0, 0, 0, 3072, 384, 80, '320x240', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, '0000-00-00 00:00:00', 1, 0, 0, 500, NULL);

/* 専用のストレージ容量に変更 */
UPDATE  `service` SET  `add_storage_size` =  '200' WHERE  `service`.`service_key` =90 LIMIT 1 ;
UPDATE  `service` SET  `add_storage_size` =  '0' WHERE  `service`.`service_key` =95 LIMIT 1 ;
UPDATE  `service` SET  `add_storage_size` =  '200' WHERE  `service`.`service_key` =97 LIMIT 1 ;
UPDATE  `service` SET  `add_storage_size` =  '200' WHERE  `service`.`service_key` =98 LIMIT 1 ;
UPDATE  `service` SET  `add_storage_size` =  '0' WHERE  `service`.`service_key` =100 LIMIT 1 ;

INSERT INTO `service_option` VALUES (32 , 1, 'user_max_storage_size_extention_500M',0,0,0,1,1);

/* PGI用 */
ALTER TABLE  `service` ADD `teleconference` integer  NOT NULL COMMENT '電話会議オプション' DEFAULT 0 AFTER `global_link`;
UPDATE  `service` SET  `teleconference` =  '1' WHERE  `service`.`service_key` =73 LIMIT 1 ;
UPDATE  `service` SET  `teleconference` =  '1' WHERE  `service`.`service_key` =74 LIMIT 1 ;
UPDATE  `service` SET  `teleconference` =  '1' WHERE  `service`.`service_key` =75 LIMIT 1 ;
UPDATE  `service` SET  `teleconference` =  '1' WHERE  `service`.`service_key` =80 LIMIT 1 ;
UPDATE  `service` SET  `teleconference` =  '1' WHERE  `service`.`service_key` =81 LIMIT 1 ;
UPDATE  `service` SET  `teleconference` =  '1' WHERE  `service`.`service_key` =82 LIMIT 1 ;
UPDATE  `service` SET  `teleconference` =  '1' WHERE  `service`.`service_key` =83 LIMIT 1 ;
UPDATE  `service` SET  `teleconference` =  '1' WHERE  `service`.`service_key` =84 LIMIT 1 ;
UPDATE  `service` SET  `teleconference` =  '1' WHERE  `service`.`service_key` =88 LIMIT 1 ;
UPDATE  `service` SET  `teleconference` =  '1' WHERE  `service`.`service_key` =90 LIMIT 1 ;
UPDATE  `service` SET  `teleconference` =  '1' WHERE  `service`.`service_key` =93 LIMIT 1 ;
UPDATE  `service` SET  `teleconference` =  '1' WHERE  `service`.`service_key` =96 LIMIT 1 ;
UPDATE  `service` SET  `teleconference` =  '1' WHERE  `service`.`service_key` =97 LIMIT 1 ;
UPDATE  `service` SET  `teleconference` =  '1' WHERE  `service`.`service_key` =98 LIMIT 1 ;





