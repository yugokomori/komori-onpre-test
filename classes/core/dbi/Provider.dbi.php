<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/N2MY_DBI.class.php");

class DBI_Provider extends N2MY_DB
{
    public $table = "provider";
    public $logger = null;
    public $rules = array();
    protected $primary_key = "provider_key";

    public function __construct( $dsn ) {
        $this->init( $dsn, $this->table );
    }

    public function getKeyByAuth( $provider_id, $provider_password )
    {
        $where = sprintf( "provider_id='%s' AND provider_password='%s' AND is_active = 1",
                    $provider_id, $provider_password );
        $_ = $this->getRow( $where, "provider_key" );
        return $_['provider_key'];
    }
}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
