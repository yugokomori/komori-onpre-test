-- スマートフォン連携
ALTER TABLE `service` ADD `smartphone` INT NOT NULL COMMENT 'スマートフォン連携' AFTER `telephone`;

INSERT INTO `service_option` (`service_option_key` ,`country_key` ,`service_option_name` ,`service_option_price` ,`service_option_init_price` ,`service_option_running_price`) VALUES
('21', '1', 'smartphone', '0', '0', '0');

INSERT INTO `options` (`option_key` ,`option_name` ,`create_datetime` ,`update_datetime`) VALUES
(19 , 'smartphone', '0000-00-00 00:00:00', NULL);
