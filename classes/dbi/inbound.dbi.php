<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/N2MY_DBI.class.php");
require_once ("lib/EZLib/EZXML/EZXML.class.php");

class InboundTable extends N2MY_DB {

    var $table = 'inbound';
    protected $primary_key = "inbound_key";

    function InboundTable($dsn) {
        $this->init($dsn, $this->table);
    }

    function get_max_sort($user_key) {

        //インバウンドキーからソート番号取得
        $where = " user_key = '".addslashes($user_key)."'";
        $inbound_count = $this->numRows($where);
        if($inbound_count > 0){
            $query = "SELECT max(inbound_sort) FROM inbound WHERE user_key = ". $user_key;
            $ret = $this->_conn->getOne($query);
            $this->logger->info("max_sort",__FILE__,__LINE__,$ret);
            if (DB::isError($ret)) {
                $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, $query);
                return $ret;
            }
            return $ret;
        }else{
            return 0;
        }
    }
    function get_iframe_size($design_id) {
    	$design_size = array();
    	$design_size["design_key"] = $design_id;
    	$design_file = N2MY_APP_DIR."/config/sales/inbound_iframe_size.xml";
    	if (file_exists($design_file)) {
    		$contents = file_get_contents($design_file);
    		$xml = new EZXML();
    		$table_xml = $xml->openXML($contents);
    		foreach($table_xml["inboundIframeSize"]["iframeSize"] as $design_data) {
    			//$this->logger2->info($design_data,"xml");
    			if($design_data["_attr"]["id"] === $design_id) {
    				$design_size["width"] = $design_data["_attr"]["width"];
    				$design_size["height"] =  $design_data["_attr"]["height"];
    				$design_size["layout"] =  $design_data["_attr"]["layout"];
    				$design_size["statusButton"]["width"] = $design_data["statusButton"][0]["_attr"]["width"];
    				$design_size["statusButton"]["height"] = $design_data["statusButton"][0]["_attr"]["height"];
    				$design_size["statusButton"]["design_key"] = $design_data["statusButton"][0]["_attr"]["designKey"];
    				$design_size["entryButton"]["width"] = $design_data["entryButton"][0]["_attr"]["width"];
    				$design_size["entryButton"]["height"] = $design_data["entryButton"][0]["_attr"]["height"];
    				$design_size["entryButton"]["design_key"] = $design_data["entryButton"][0]["_attr"]["designKey"];
    				$this->logger2->debug($design_size,"design_size");
    			}
    		}
    	} else {
    		$this->logger2->info("no file","design info");
    		$design_size["width"] = 0;
    		$design_size["height"] = 0;
    	}
    	return $design_size;
    }
}