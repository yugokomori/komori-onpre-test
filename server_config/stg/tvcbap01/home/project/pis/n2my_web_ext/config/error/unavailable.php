<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php 
  $context = sfContext::getInstance();
  $context->getConfiguration()->loadHelpers('I18N', $context->getModuleName());
?>

<?php $path = sfConfig::get('sf_relative_url_root', preg_replace('#/[^/]+\.php5?$#', '', isset($_SERVER['SCRIPT_NAME']) ? $_SERVER['SCRIPT_NAME'] : (isset($_SERVER['ORIG_SCRIPT_NAME']) ? $_SERVER['ORIG_SCRIPT_NAME'] : ''))) ?>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" media="all" href="<?php echo $path ?>/shared/css/import.css?4.9.5.3" />
    <link rel="stylesheet" type="text/css" media="all" href="<?php echo $path ?>/shared/js/calendar/skins/tiger/theme.css" title="Aqua" />
    <script type="text/javascript" src="<?php echo $path ?>/shared/js/lib/prototype.js?4.9.5.3"></script>
    <script type="text/javascript" src="<?php echo $path ?>/shared/js/lib/jquery.js?4.9.5.3"></script>
    <script type="text/javascript" src="<?php echo $path ?>/shared/js/common.js?4.9.5.3"></script>
    <script type="text/javascript" src="<?php echo $path ?>/shared/js/interaction.js?4.9.5.3"></script>
    <script type="text/javascript" src="<?php echo $path ?>/shared/js/lib/smoothscroll.js?4.9.5.3"></script>
    <script type="text/javascript" src="<?php echo $path ?>/shared/js/lib/dropdown.js?4.9.5.3"></script>
    <script type="text/javascript" src="<?php echo $path ?>/shared/js/lib/jquery.bgiframe.js"></script>
    <script type="text/javascript" src="<?php echo $path ?>/shared/js/lib/jquery.ezpz_tooltip.min.js?4.9.5.3"></script>
    <script type="text/javascript" src="<?php echo $path ?>/shared/js/lib/jquery.qtip-1.0.0-rc3.js"></script>
    <script type="text/javascript" src="<?php echo $path ?>/shared/js/lib/swfobject.js"></script>
    <script type="text/javascript" src="<?php echo $path ?>/shared/js/calendar/calendar_stripped.js"></script>
    <script type="text/javascript" src="<?php echo $path ?>/shared/js/calendar/lang/calendar-en_US.js"></script>
    <script type="text/javascript" src="<?php echo $path ?>/shared/js/calendar/lang/calendar-ja_JP.js"></script>
    <script type="text/javascript" src="<?php echo $path ?>/shared/js/calendar/calendar-setup_stripped.js"></script>
    <script type="text/javascript" src="<?php echo $path ?>/shared/js/extend.js" charset="UTF-8"></script>
    <script type="text/javascript" src="<?php echo $path ?>/shared/js/lib/table_cell.js" charset="UTF-8"></script>
    <title>Error:</title>
  </head>
  <body id="prompt">
    <div id="header">
      <h1>
        <?php /*
        <img src="<?php echo $path ?>/shared/images/logo.gif" alt="<?php echo __('ＶＡ 会議システム') ?>" />
         */ ?>
        <img src="<?php echo $path ?>/lang/<?php echo $context->getUser()->getCulture() ?>/images/logo.gif" alt="<?php echo __('ＶＡ 会議システム') ?>" />
      </h1>
    </div>
    <div class="gbar">
      <div class="gnav-inner">
        <h2><?php echo __('エラーが発生しました') ?></h2>
      </div>
    </div>
    <div id="wrapper">
      <div id="inner-col" class="clearfix">
      <!-- コンテンツここから -->
        <div id="content">
          <div class="msg-col attention">
            <p class="bold"><?php echo $code ?> <?php echo $text ?></p><br />
            <p class="bold"><?php echo __('エラーが発生しました。    <br />下記のボタンからメインページへお戻りください。') ?></p>
          </div>
            <p class="toHome center">
            <span class="button">
              <span class="button-inner">
                <input type="button" value="<?php echo __('ホームに戻る') ?>" class="icon i-go" onClick="location.href='<?php echo $path.'/home/index' ?>';" />
              </span>
            </span>
            </p>
        </div>
        <!-- コンテンツここまで -->
      </div>
      <!--/inner-col -->
    </div>
  <!--/wrapper -->
  <p class="page-top"><a href="#wrapper"><?php echo __('ページトップへ') ?></a></p>

  <div id="footer">
    <div id="footer-inner" class="clearfix">
    </div>
  </div>
  </body>
</html>