<?php

require_once('classes/AppFrame.class.php');
require_once("classes/N2MY_Account.class.php");

/*新型roomクラスに移行中*/
require_once('classes/N2MY_DBI.class.php');
require_once('classes/dbi/room.dbi.php');
require_once('classes/dbi/user.dbi.php');
require_once("classes/dbi/ordered_service_option.dbi.php");

class AppAdmin extends AppFrame
{
    var $err_obj = null;

    function init() {
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->_name_space = md5(__FILE__);
    }

    /**
     * ログイン認証
     */
    function auth() {
        $this->checkAuth();
        $this->checkAdminAuth('admin/session_error.t.html');
    }

    function action_env() {
        $this->set_submit_key($this->_name_space);
        $lang       = $this->request->get("lang", $_COOKIE["lang"]);
        $country    = $this->request->get("country", $_COOKIE["country"]);
        $time_zone  = $this->request->get("time_zone", $_COOKIE["time_zone"]);
        $this->template->assign("lang", $lang);
        $this->template->assign("country", $country);
        $this->template->assign("time_zone", $time_zone);
        $this->display('admin/personal.t.html');
    }

    function action_set_env() {
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_showTop();
        }
        // 有効期限
        $limit_time = time() + 365 * 24 * 3600;
        // 地域コード
        $_country_key = $this->request->get("country_key");
        if ($_country_key !== "") {
            $country_list = $this->get_country_list();
            // 改ざんされた場合の対応
            if (!array_key_exists($_country_key, $country_list)) {
                $country_keys = array_keys($country_list);
                $_country_key = $country_keys[0];
                $this->logger->warn(__FUNCTION__, __FILE__, __LINE__,$_country_key);
            }
            foreach ($country_list as $country_key => $country_row) {
                if ($_country_key == $country_key) {
                    setcookie("country", $_country_key,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
                    $this->session->set("country_key", $country_row["country_key"]);
                }
            }
        }
        // 言語コード
        $lang = $this->request->get("lang");
        if ($lang !== "") {
            setcookie("lang", $lang,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
            $this->session->set("lang", $lang);
        }
        // タイムゾーン
        $time_zone = $this->request->get("time_zone");
        if (is_numeric($time_zone)) {
            setcookie("time_zone", $time_zone,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
            $this->session->set("time_zone", $time_zone);
        }
        header("Location: index.php");
    }

    function action_change_user_mode() {
      $request = $this->request->getAll();
      $user_info = $this->session->get("user_info");
        $this->logger2->info($request["service_mode"]);
      if($user_info["use_sales"] == 1) {
        if ($request["service_mode"] == "meeting") { $this->session->set("service_mode","sales"); }
        elseif($request["service_mode"] == "sales") { $this->session->set("service_mode","meeting"); }
      } else {
        //avoid url send request
        $this->session->set("service_mode",null);
      }
      header("Location: index.php");
    }

    function action_showtop(){
      $flag = ($this->obj_N2MY_Account->checkChangeVersion($this->session->get('user_info')));
      $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
      $this->template->assign("flag",$flag);
        $this->display('admin/mainmenu.t.html');
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    function action_version_change(){
      $this->display('admin/versionup.t.html');
    }

    function action_version_change_complete(){

      // バージョンアップ
      if($this->obj_N2MY_Account->changeVersion($this->session->get('user_info'))){
        // セッションのバージョン情報も書き換え
        $user_info = $this->session->get('user_info');
        $user_info["meeting_version"] = "4.7.0.0";
        $this->session->set('user_info',$user_info);
        $this->display('admin/versionup_complete.t.html');
      }else{
         header("Location: index.php");
      }
    }

    //　「利用料金の照会」
    function action_charge(){
        $this->display('admin/charge/index.t.html');
    }

    # 現在の契約状況を表示
    function action_room_plan($message = ""){
        $country_key = $this->session->get('country_key');
        $user_info   = $this->session->get('user_info');
        $rooms       = $this->obj_N2MY_Account->getRoomList( $user_info["user_key"], $this->session->get("service_mode") );

        $this->session->set("room_info", $rooms);

        $document_ext = $this->config->getAll('DOCUMENT_FILES');
        $image_ext    = $this->config->getAll('DOCUMENT_IMAGES');

        foreach ($rooms as $key => $room) {
            $filetype = array();
            // ホワイトボードのファイルタイプ
            if ($room["room_info"]["whiteboard_filetype"]) {
                $whiteboard_filetype = unserialize($room["room_info"]["whiteboard_filetype"]);
                if ($whiteboard_filetype["document"]) {
                    foreach($whiteboard_filetype["document"] as $val) {
                        foreach($document_ext as $type => $extensions) {
                            $_extensions = split(",", $extensions);
                            // 登録形式を、拡張子 -> タイプ に変更し、既存データとの整合性確保のため
                            if (in_array($val, $_extensions) || $val == $type) {
                                $filetype[] = $type;
                            }
                        }
                    }
                }
                if ($whiteboard_filetype["image"]) {
                    foreach($whiteboard_filetype["image"] as $val) {
                        foreach($image_ext as $type => $extensions) {
                            $_extensions = split(",", $extensions);
                            // 登録形式を、拡張子 -> タイプ に変更し、既存データとの整合性確保のため
                            if (in_array($val, $_extensions) || $val == $type) {
                                $filetype[] = $type;
                            }
                        }
                    }
                }
            // 初期化
            } else {
                $filetype = array_keys($document_ext + $image_ext);
            }
            // 資料のタイプ
            if ($rooms[$key]["room_info"]["cabinet_filetype"]) {
                $cabinet_filetype = @unserialize($rooms[$key]["room_info"]["cabinet_filetype"]);
                if (is_array($cabinet_filetype)) {
                    $rooms[$key]["room_info"]["cabinet_filetype"] = join(", ", $cabinet_filetype);
                } else {
                    $rooms[$key]["room_info"]["cabinet_filetype"] = "";;
                }
            }
            $rooms[$key]["room_info"]["whiteboard_filetype"] = join(", ", $filetype);
            // プロトコルのデフォルト
            if (!$rooms[$key]["room_info"]["rtmp_protocol"]) {
                if ($rooms[$key]["options"]["meeting_ssl"]) {
                    $protocol_list = array(
                        "rtmpe:1935",
                        "rtmpe:80",
                        "rtmpe:8080",
                        "rtmps-tls:443",
                        "rtmps:443",
                        "rtmpte:80",
                        "rtmpte:8080",
                        );
                } else {
                    $protocol_list = array(
                        "rtmp:1935",
                        "rtmp:80",
                        "rtmp:8080",
                        "rtmps-tls:443",
                        "rtmps:443",
                        "rtmpt:80",
                        "rtmpt:8080",
                        );
                }
                $rooms[$key]["room_info"]["rtmp_protocol"] = join(", ", $protocol_list);
            } else {
                $rooms[$key]["room_info"]["rtmp_protocol"] = join(", ", split(",", $rooms[$key]["room_info"]["rtmp_protocol"]));
            }
            if (!$rooms[$key]["room_info"]["transceiver_number"]) $rooms[$key]["room_info"]["transceiver_number"] = 11;
            //利用帯域追加
            if($room["room_info"]["use_extend_bandwidth"] === null) {
            	require_once("classes/dbi/room_plan.dbi.php");
            	$obj_RoomPlan = new RoomPlanTable($this->get_dsn());
            	$room_plan_key = $obj_RoomPlan->getOne("room_plan_status=1 and room_key='".$room["room_info"]["room_key"]."'", "service_key");
            	$this->logger2->debug($room_plan_key);
            	if($room_plan_key) {
            		require_once("classes/dbi/service.dbi.php");
            		$obj_Service = new ServiceTable($this->get_auth_dsn());
            		$room_plan = $obj_Service->getRow("service_key=".$room_plan_key);
            		if($room_plan["bandwidth_up_flg"]) {
            			if($room_plan["max_user_bandwidth"] == $room["room_info"]["max_user_bandwidth"])
            				$rooms[$key]["room_info"]["use_extend_bandwidth"] = 1;
            			else
            				$rooms[$key]["room_info"]["use_extend_bandwidth"] = 0;
            		} else {
            			//非表示部屋
            			$rooms[$key]["room_info"]["use_extend_bandwidth"] = -1;
            		}
            	} else {
            		//プランがない部屋
            		$rooms[$key]["room_info"]["use_extend_bandwidth"] = -1;
            	}
            }
            if ($rooms[$key]["options"]["video_conference_number"] > 0) {
                require_once("classes/dbi/ives_setting.dbi.php");
                require_once("classes/mgm/dbi/mcu_server.dbi.php");
                $obj_ivesSetting = new IvesSettingTable($this->get_dsn());
                $obj_mcuServer = new McuServerTable(N2MY_MDB_DSN);
                $where = "room_key = '" . addslashes($rooms[$key]["room_info"]["room_key"]) . "' AND is_deleted = 0";
                $ives_setting = $obj_ivesSetting->getRow($where);
                $mcuServer = $obj_mcuServer->getRow(sprintf("server_key = '%s'", $ives_setting["mcu_server_key"]));
                $address = sprintf("%s@%s", $ives_setting["ives_did"], $mcuServer["server_address"]);
                $ives_setting["video_conference_address"] = $address;
                $rooms[$key]["ives_setting"] = $ives_setting;
            }
        }
        // 回線速度一覧
        $net_speed_list = $this->get_message("NET_SPEED_LIST");
        foreach($net_speed_list as $key => $val) {
            $net_speed_list[$key] = substr($val, strpos($val, ":") + 1);
        }
        $has_bandwidth_extend = 0;
        foreach ($rooms as $room) {
        	if($room["room_info"]["use_extend_bandwidth"] >= 0) 
        		$has_bandwidth_extend = 1;
        }
        $this->template->assign(array("net_speed_list" => $net_speed_list,
                                      'data'           => $rooms,
                                      'message'        => $message,
                                      'country_key'    => $country_key,
                                      'is_trial'       => $user_info['user_status'] == 2,
                                      'has_bandwidth_extend' => $has_bandwidth_extend));
        $this->display('admin/room/index.t.html');
    }

    //部屋のソートを変更
    function action_room_up(){
        $obj_Room = new RoomTable($this->get_dsn());
        $user_info = $this->session->get('user_info');
        $obj_Room->up_sort($user_info["user_key"],$_REQUEST["room_key"]);
        $this->action_room_plan();
    }

    //部屋のソートを変更
    function action_room_down(){
        $obj_Room = new RoomTable($this->get_dsn());
        $user_info = $this->session->get('user_info');
        $obj_Room->down_sort($user_info["user_key"],$_REQUEST["room_key"]);
        $this->action_room_plan();
    }


    function action_edit_room() {
        $room_key  = $this->get_room_key();
        $obj_Room  = new RoomTable($this->get_dsn());
        $where     = "room_key = '".addslashes($room_key)."'";
        $room_info = $obj_Room->getRow($where);
        if (!$room_info["whiteboard_filetype"]) {
            $document_ext = $this->config->getAll('DOCUMENT_FILES');
            $image_ext    = $this->config->getAll('DOCUMENT_IMAGES');
            $room_info["whiteboard_filetype"] = serialize(array(
                "document" => array_keys($document_ext),
                "image"    => array_keys($image_ext)));
        }
        if (!$room_info["transceiver_number"]) {
            $room_info["transceiver_number"] = 11;
        }
        $this->session->set("room_info", $room_info, $this->_name_space);
        $this->render_edit_room();
    }

    function render_edit_room() {
        // 二重登録回避
        $this->set_submit_key($this->_name_space);
        $room_info = $this->session->get("room_info", $this->_name_space);
        if (EZValidator::isError($this->err_obj)) {
            $msg_format = $this->get_message("error");
            $msg        = $this->get_message("ERROR");
            $err_fields = $this->err_obj->error_fields();
            foreach ($err_fields as $key) {
                $type = $this->err_obj->get_error_type($key);
                $_msg = sprintf($msg[$type], $this->err_obj->get_error_rule($key, $type));
                $error[$key] = sprintf($msg_format, $_msg);
            }
            $this->logger->info(__FUNCTION__."#error", __FILE__, __LINE__, $error);
            $this->template->assign("error_info", $error);
        }
        $room_info_detail = $this->get_room_info($room_info["room_key"]);
       if ($room_info_detail["options"]["meeting_ssl"]) {
            $protocol_list = array(
                "rtmpe:1935"  => "rtmpe:1935",
                "rtmpe:80"    => "rtmpe:80",
                "rtmpe:8080"  => "rtmpe:8080",
                "rtmps-tls:443"   => "rtmps-tls:443",
                "rtmps:443"   => "rtmps:443",
                "rtmpte:80"   => "rtmpte:80",
                "rtmpte:8080" => "rtmpte:8080",
                );
        } else {
            $protocol_list = array(
                "rtmp:1935"   => "rtmp:1935",
                "rtmp:80"     => "rtmp:80",
                "rtmp:8080"   => "rtmp:8080",
                "rtmps-tls:443"   => "rtmps-tls:443",
                "rtmps:443"   => "rtmps:443",
                "rtmpt:80"    => "rtmpt:80",
                "rtmpt:8080"  => "rtmpt:8080",
                );
        }
        $protocol_deny_list  = array();
        $protocol_allow_list = array();
        $list                = array();

        if (!$room_info["rtmp_protocol"]) {
            $protocol_allow_list = $protocol_list;
        } else {
            $list = split(",", $room_info["rtmp_protocol"]);
            foreach($list as $protocol) {
                if (array_key_exists($protocol, $protocol_list)) {
                    $protocol_allow_list[$protocol] = $protocol_list[$protocol];
                }
            }
        }
        foreach($protocol_list as $key => $value) {
            if (!array_key_exists($key, $protocol_allow_list)) {
                $protocol_deny_list[$key] = $value;
            }
        }
        if ($room_info["cabinet_filetype"]) {
            $room_info["cabinet_filetype"] = unserialize($room_info["cabinet_filetype"]);
        }
        $document_ext = $this->config->getAll('DOCUMENT_FILES');
        $image_ext    = $this->config->getAll('DOCUMENT_IMAGES');
        if ($room_info["whiteboard_filetype"]) {
            $filetype = unserialize($room_info["whiteboard_filetype"]);
            $this->logger2->debug($filetype);
            $room_info["whiteboard_filetype"] = array();
            if ($filetype["document"]) {
                foreach($filetype["document"] as $val) {
                    foreach($document_ext as $type => $extensions) {
                        $_extensions = split(",", $extensions);
                        // 登録形式を、拡張子 -> タイプ に変更し、既存データとの整合性確保のため
                        if (in_array($val, $_extensions) || $val == $type) {
                            $room_info["whiteboard_filetype"][$type] = 1;
                        }
                    }
                }
            }
            if ($filetype["image"]) {
                foreach($filetype["image"] as $val) {
                    foreach($image_ext as $type => $extensions) {
                        $_extensions = split(",", $extensions);
                        // 登録形式を、拡張子 -> タイプ に変更し、既存データとの整合性確保のため
                        if (in_array($val, $_extensions) || $val == $type) {
                            $room_info["whiteboard_filetype"][$type] = 1;
                        }
                    }
                }
            }
        }
        $net_speed_list = $this->get_message("NET_SPEED_LIST");
        foreach ($net_speed_list as $key => $val) {
            $net_speed_list[$key] = substr($val, strpos($val, ":") + 1);
        }

        if (!$this->config->get("IGNORE_MENU", "teleconference")) {
            try {
                $service_option  = new OrderedServiceOptionTable($this->get_dsn());
                $enable_teleconf = $service_option->enableOnRoom(23, $room_info["room_key"]); // 23 = teleconf
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                exit;
            }
            $user_info = $this->session->get("user_info");
            $this->template->assign(array("enable_teleconf" => $enable_teleconf,
                                          "is_trial"        => $user_info['user_status'] == 2,
                                          "service_name"    => $user_info['service_name']));
        }

        $room_info["options"] = $room_info_detail["options"];
        $this->template->assign(array("image_files"         => $image_ext,
                                      "document_files"      => $document_ext,
                                      "protocol_list"       => $protocol_list,
                                      "net_speed_list"      => $net_speed_list,
                                      "protocol_deny_list"  => $protocol_deny_list,
                                      "protocol_allow_list" => $protocol_allow_list,
                                      "room_info"           => $room_info));
    // 部屋のプラン確認
        require_once('classes/dbi/room_plan.dbi.php');
    // 資料共有プランだったら録画機能欄を表示させない(今後増えるかも)
    $rec_disable_flag = 0;
    if($room_info["options"]["whiteboard"]){
      $rec_disable_flag = 1;
    }
    $this->template->assign("rec_disable_flag",$rec_disable_flag);
        $this->display('admin/room/edit.t.html');
    }

    function action_edit(){
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_room_plan();
        }
        $user_info = $this->session->get( "user_info" );
        $request = $this->request->getAll();
        $this->logger2->info($request);
        $obj_Room = new RoomTable( $this->get_dsn() );
        $room_key = $this->get_room_key();
        if ($request["cabinet_filetype"]) {
            foreach ($request["cabinet_filetype"] as $extension) {
                $extensions[] = strtolower($extension);
            }
            $request["cabinet_filetype"] = $extensions;
        }
        $data = array(
            "room_key"                    => $request["room_key"],
            "room_name"                   => $request["room_name"],
            "cabinet"                     => $request["cabinet"],
            "cabinet_size"                => $request["cabinet_size"],
            "cabinet_filetype"            => serialize($request["cabinet_filetype"]),
            "mfp"                         => $request["mfp"],
            "default_layout_4to3"         => $request["default_layout_4to3"],
            "default_layout_16to9"        => $request["default_layout_16to9"],
            "rtmp_protocol"               => $request["rtmp_protocol"],
            "whiteboard_page"             => $request["whiteboard_page"],
            "whiteboard_size"             => $request["whiteboard_size"],
            "whiteboard_filetype"         => serialize($request["whiteboard_filetype"]),
            "default_microphone_mute"     => $request["default_microphone_mute"],
            "default_camera_mute"         => $request["default_camera_mute"],
            "is_auto_transceiver"         => $request["is_auto_transceiver"],
            "default_h264_use_flg"        => $request["default_h264_use_flg"],
            "default_agc_use_flg"         => $request["default_agc_use_flg"],
            "transceiver_number"          => $request["transceiver_number"],
            "is_auto_voice_priority"      => $request["is_auto_voice_priority"],
            "is_netspeed_check"           => $request["is_netspeed_check"],
            "is_wb_no"                    => $request["is_wb_no"],
            "is_device_skip"              => $request["is_device_skip"],
            "is_participant_notifier"     => $request["is_participant_notifier"],
        	"audience_chat_flg"           => $request["audience_chat_flg"],
            "disable_rec_flg"             => $request["disable_rec_flg"],
            "is_personal_wb"              => $request["is_personal_wb"],
            "is_convert_wb_to_pdf"        => $request["is_convert_wb_to_pdf"],
            "rec_gw_convert_type"         => $request["rec_gw_convert_type"] ? $request["rec_gw_convert_type"] : "mov",
            "rec_gw_userpage_setting_flg" => isset($request["rec_gw_userpage_setting_flg"]) ? $request["rec_gw_userpage_setting_flg"] : "1",
            "customer_camera_use_flg"     => isset($request["customer_camera_use_flg"]) ? $request["customer_camera_use_flg"] : "1",
            "customer_camera_status"      => isset($request["customer_camera_status"]) ? $request["customer_camera_status"] : "1",
            "customer_sharing_button_hide_status" => isset($request["customer_sharing_button_hide_status"]) ? $request["customer_sharing_button_hide_status"] : "1",
            "wb_allow_flg"                => isset($request["wb_allow_flg"]) ? $request["wb_allow_flg"] : "0",
            "chat_allow_flg"              => isset($request["chat_allow_flg"]) ? $request["chat_allow_flg"] : "0",
            "print_allow_flg"             => isset($request["print_allow_flg"]) ? $request["print_allow_flg"] : "0",
            "prohibit_extend_meeting_flg" => isset($request["prohibit_extend_meeting_flg"]) ? $request["prohibit_extend_meeting_flg"] : "0",
            );
        $this->logger2->info($request);
        $service_option  = new OrderedServiceOptionTable($this->get_dsn());
        $enable_teleconf = $service_option->enableOnRoom(23, $room_key); // 23 = teleconf

        if (!$this->config->get("IGNORE_MENU", "teleconference") && $enable_teleconf) {
            if ($request['use_teleconf']) {
                $data['use_teleconf']        = 1;
                $data['use_pgi_dialin']      = $request['use_pgi_dialin']      ? 1: 0;
                $data['use_pgi_dialin_free'] = $request['use_pgi_dialin_free'] ? 1: 0;
                $data['use_pgi_dialout']     = $request['use_pgi_dialout']     ? 1: 0;
            } else {
                $data['use_teleconf']        = 0;
                $data['use_pgi_dialin']      = 0;
                $data['use_pgi_dialin_free'] = 0;
                $data['use_pgi_dialout']     = 0;
            }
        }


        $rules = array("rtmp_protocol" => array("required" => true,),
                       "room_name"     => array("required" => true,
                                                "maxlen"   => 20),
                       "cabinet"       => array("required" => true,
                                                "allow"    => array("0", "1")),
                       "mfp"           => array("required" => true,
                                                "allow"    => array("all", "select", "deny")),);
        $this->err_obj = $obj_Room->check($data, $rules);
        if ($request["cabinet_filetype"]) {
            foreach ($request["cabinet_filetype"] as $ext) {
                if (ereg("[^0-9a-z]", $ext)) {
                    $this->err_obj->set_error("cabinet_filetype", "alnum", $ext);
                    break;
                }
            }
        }
        if (!$this->config->get("IGNORE_MENU", "teleconference")) {
            if ($data['use_teleconf'] && !$data['use_pgi_dialin']
             && !$data['use_pgi_dialin_free'] && !$data['use_pgi_dialout']) {
                $this->err_obj->set_error("teleconf", "minitem", 1);
            }
        }
        // エラー
        if (EZValidator::isError($this->err_obj)) {
            $this->session->set("room_info", $data, $this->_name_space);
            return $this->render_edit_room();
        }

        if ($room_key && $user_info["user_key"]) {
            $this->logger2->debug($data);
            $obj_Room->update($data, sprintf("room_key='%s' AND user_key='%s'",
                                             $request["room_key"], $user_info["user_key"] ) );
        }

        $this->session->remove("room_info", $this->_name_space);
        $user_info   = $this->session->get("user_info");
        $member_info = $this->session->get("member_info");
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        if ( $member_info && ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") ) {
            $rooms = $obj_N2MY_Account->getOwnRoom( $member_info["member_key"], $user_info["user_key"], $this->session->get("service_mode") );
        } else {
            $rooms = $obj_N2MY_Account->getRoomList( $user_info["user_key"], $this->session->get("service_mode") );
        }
        $this->session->set('room_info', $rooms);
        $this->add_operation_log('setup_room', array( 'room_key' => $request["room_key"]));

        return $this->action_room_plan();
    }

    function action_reset() {

        $user_info = $this->session->get( "user_info" );
        $request   = $this->request->getAll();
        $obj_Room  = new RoomTable( $this->get_dsn() );
        $data = array(
            "cabinet"                     => "1",
            "cabinet_size"                => 20,
            "cabinet_filetype"            => "",
            "mfp"                         => "all",
            "default_layout_4to3"         => "normal",
            "default_layout_16to9"        => "small",
            "rtmp_protocol"               => "",
            "whiteboard_page"             => 100,
            "whiteboard_size"             => 20,
            "whiteboard_filetype"         => "",
            "default_microphone_mute"     => "off",
            "default_camera_mute"         => "off",
            "default_h264_use_flg"        => 0,
            "default_agc_use_flg"         => 1,
            "is_auto_transceiver"         => 0,
            "transceiver_number"          => 11,
            "is_auto_voice_priority"      => 0,
            "is_netspeed_check"           => "",
            "is_wb_no"                    => 1,
            "is_device_skip"              => 0,
            "is_participant_notifier"     => 1,
        	"audience_chat_flg"           => 0,
            "disable_rec_flg"             => 0,
            "is_personal_wb"              => 1,
            "is_convert_wb_to_pdf"        => 1,
            "rec_gw_convert_type"         => "mov",
            "customer_camera_use_flg"     => 1,
            "customer_camera_status"      => 1,
            "customer_sharing_button_hide_status" => 1,
            "wb_allow_flg"                => 0,
            "chat_allow_flg"              => 0,
            "print_allow_flg"             => 0,
            "prohibit_extend_meeting_flg" => 0,
            );
    if($this->session->get("service_mode") == "sales" || $user_info["meeting_version"] == null) {
      $data["default_layout_16to9"] = "normal";
    }
        $service_option  = new OrderedServiceOptionTable($this->get_dsn());
        $enable_teleconf = $service_option->enableOnRoom(23, $request["room_key"]); // 23 = teleconf

        if ($enable_teleconf) {
            $data += array(
                "use_teleconf"              => 1,
                "use_pgi_dialin"            => 1,
                "use_pgi_dialin_free"       => 1,
                "use_pgi_dialout"           => 1
             );
        }

        $obj_Room->update( $data, sprintf( "room_key='%s' AND user_key='%s'", $request["room_key"], $user_info["user_key"] ) );
        return $this->action_room_plan();
    }

    function action_contract(){
        $admin_info = $this->session->get('user_info');
        foreach ($admin_info as $key => $value) {
            $this->template->assign($key, $value);
        }
        if ($admin_edit_info = $this->session->get('admin_edit_info')) {
            foreach($admin_edit_info as $key => $value){
                $this->template->assign($key, $value);
            }
        }
        $this->display('admin/account/index.t.html');
    }

    function action_option_list(){
        $admin_info = $this->session->get('user_info');
        foreach ($admin_info as $key => $value) {
            $this->template->assign($key, $value);
        }
        if ($admin_edit_info = $this->session->get('admin_edit_info')) {
            foreach($admin_edit_info as $key => $value){
                $this->template->assign($key, $value);
            }
        }
        $this->display('admin/account/option.t.html');
    }

    function action_admin_info(){
        $admin_info = $this->session->get('user_info');
        foreach ($admin_info as $key => $value) {
            $this->template->assign($key, $value);
        }
        if ($admin_edit_info = $this->session->get('admin_edit_info')) {
            foreach($admin_edit_info as $key => $value){
                $this->template->assign($key, $value);
            }
        }
        $this->display('admin/account/account.t.html');
    }

    /**
     * パスワード変更画面
     */
    function action_login_password_form()
    {
        $this->render_login_password_form();
    }

    /**
     * パスワード変更画面表示
     */
    function render_login_password_form($msg = null)
    {
        $this->set_submit_key($this->_name_space);
        $request = $this->request->getAll();
        $this->template->assign("message", $msg);
        $this->template->assign("request", $request);
        $this->display("admin/account/login_password.t.html");
    }

    /**
     * パスワード設定完了画面
     */
    function action_login_password_complete()
    {
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_showTop();
        }
        $old_password     = $this->request->get("old_password");
        $new_password     = $this->request->get("new_password");
        $confirm_password = $this->request->get("confirm_password");
        $user_info        = $this->session->get("user_info");
        $user_key         = $user_info["user_key"];
        $msg              = "";

        require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
        $db_password = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $user_info["user_password"]);
        if ($old_password != $db_password) {
            $msg = ADMIN_PASSWORD_ERROR1;
        } elseif (($new_password != $confirm_password) || (trim($new_password) == "")) {
            $msg .= ADMIN_PASSWORD_ERROR2;
        //ユーザーパスワードをチェック
        } elseif (!preg_match('/^[!-\[\]-~]{8,128}$/', $new_password)) {
            $msg .= USER_ERROR_PASS_INVALID_01;
        } elseif (!preg_match('/[[:alpha:]]+/',$new_password) || preg_match('/^[[:alpha:]]+$/',$new_password)) {
            $msg .= USER_ERROR_PASS_INVALID_02;
        }
        if ($msg) {
            return $this->render_login_password_form($msg);
        }
        $user = new UserTable($this->get_dsn());
        $data = array(
            "user_password" => EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $new_password),
            "user_updatetime" => date("Y-m-d H:i:s")
            );
        $where = "user_key = ".$user_key;
        $ret = $user->update($data, $where);
        if (DB::isError($ret)) {
            return $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
        }
        $users = $user->getRowsAssoc($where);
        $new_user_info = $users[0];
        $this->session->set("user_info", $new_user_info);
        $this->logger->info(__FUNCTION__,__FILE__,__LINE__,array("old" => $user_info, "new" => $new_user_info));
        // 操作ログ
        $this->add_operation_log('change_login_password');
        return $this->render_login_password_complete();
    }

    /**
     * パスワード設定完了画面表示
     */
    function render_login_password_complete()
    {
        $this->display("admin/account/login_password_complete.t.html");
    }

    /**
     * 管理者パスワード変更画面
     */
    function action_admin_password_form()
    {
        $this->render_admin_password_form();
    }

    /**
     * 管理者パスワード変更画面表示
     */
    function render_admin_password_form($msg = null)
    {
        $this->set_submit_key($this->_name_space);
        $request = $this->request->getAll();
        $this->template->assign("message", $msg);
        $this->template->assign("request", $request);
        $this->display("admin/account/admin_password.t.html");
    }

    /**
     * 管理者パスワード設定完了画面
     */
    function action_admin_password_complete()
    {
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_showTop();
        }
        $old_password = $this->request->get("old_password");
        $new_password = $this->request->get("new_password");
        $confirm_password = $this->request->get("confirm_password");
        $user_info = $this->session->get("user_info");
        $user_key = $user_info["user_key"];
        $msg = "";
        require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
        $db_admin_password = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $user_info["user_admin_password"]);
        if ($old_password != $db_admin_password) {
            $msg = ADMIN_PASSWORD_ERROR1;
        } elseif (($new_password != $confirm_password) || (trim($new_password) == "")) {
            $msg .= ADMIN_PASSWORD_ERROR2;
        //ユーザーパスワードをチェック
        } elseif (!preg_match('/^[!-\[\]-~]{8,128}$/', $new_password)) {
            $msg .= USER_ERROR_PASS_INVALID_01;
        } elseif (!preg_match('/[[:alpha:]]+/',$new_password) || preg_match('/^[[:alpha:]]+$/',$new_password)) {
            $msg .= USER_ERROR_PASS_INVALID_02;
        }
        if ($msg) {
            return $this->render_admin_password_form($msg);
        }
        $user = new UserTable($this->get_dsn());
        $data = array(
            "user_admin_password" => EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $new_password),
            "user_updatetime" => date("Y-m-d H:i:s")
            );
        $where = "user_key = ".$user_key;
        $ret = $user->update($data, $where);
        if (DB::isError($ret)) {
            return $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
        }
        $users = $user->getRowsAssoc($where);
        $new_user_info = $users[0];
        $this->session->set("user_info", $new_user_info);
        $this->logger->info(__FUNCTION__,__FILE__,__LINE__,array("old" => $user_info, "new" => $new_user_info));
        // 操作ログ
        $this->add_operation_log('change_admin_password');
        $this->render_admin_password_complete();
    }

    /**
     * 管理者パスワード設定完了画面表示
     */
    function render_admin_password_complete()
    {
        $this->display("admin/account/admin_password_complete.t.html");
    }

    /**
     * 管理者パスワード入力チェック
     */
    function _admin_password_check()
    {

    }

    function action_admin_info_confirm(){

        $admin = new Admin();
        if($message = $admin->checkAdminInfo()){
            $this->action_admin_info($message);
            exit;
        }

        $admin_edit_info = $this->session->get('admin_edit_info');
        $len = strlen($admin_edit_info["user_password"]);
        $this->template->assign("user_password", str_pad("",$len, "*"));
        $len = strlen($admin_edit_info["user_admin_password"]);
        $this->template->assign("user_admin_password", str_pad("",$len, "*"));
        $this->display('admin/account/confirm.t.html');
    }

    function action_admin_info_complete(){

        $admin = new Admin();
        $admin->updateAdminInfo();

        $this->display('admin/account/done.t.html');
    }


    function action_inquiry($message = ""){

        if($message){
            $this->template->assign('message', $message);
        }
        if($inquiry_info = $this->session->get('inquiry_info')){
            foreach($inquiry_info as $key => $value){
                $this->template->assign($key, $value);
            }
        }
        $this->display('admin/ask/index.t.html');
    }

    function action_inquiry_confirm(){

        $admin = new Admin();
        if($message = $admin->checkInquiryInfo()){

            $this->action_inquiry($message);
            exit;
        }

        $inquiry_info = $this->session->get('inquiry_info');
        foreach($inquiry_info as $key => $value){
            $value = htmlspecialchars($value);
            //$value = nl2br($value);
            $this->template->assign($key, $value);
        }

        $this->display('admin/ask/confirm.t.html');
    }

    function action_inquiry_complete(){

        $admin = new Admin();
        $admin->sendInquiry();

        $this->display('admin/ask/done.t.html');
    }

    function action_logout(){
        //アドミンログイン情報を消去
        $this->session->remove('admin_login');
        header("Location: ".'/services/');
    }

    function showMeetingLogContorol(){}

    /**
     * デフォルト処理
     */
    function default_view() {
        $this->logger->trace("test", __FILE__, __LINE__, __FUNCTION__);
        $this->action_showtop();
    }

    function action_questionnairelog_player(){

        if($meeting_key = $_REQUEST['meeting_key']){

            $this->session->set('log_meeting_key', $meeting_key);

            $lang = $this->session->get('lang');
            if($lang == "en"){
                $lang = 2;
            }else{
                $lang = 1;
            }

            $accessLog = new AccessLog();
            $accessLog->access('questionnaireLogplayer');

            $this->template->assign('lang', $lang);
            $this->display('user/questionnaire_log_player.t.html');
        }
    }

    function action_log_player(){

        if($meeting_key = $_REQUEST['meeting_key']){

            $this->session->set('log_meeting_key', $meeting_key);

            $lang = $this->session->get('lang');
            if($lang == "en"){
                $lang = 2;
            }else{
                $lang = 1;
            }

            $accessLog = new AccessLog();
            $accessLog->access('logplayer');

            $this->template->assign('lang', $lang);
            $this->template->assign('isPresen',$_REQUEST['isPresen']);
            $this->display('admin/meetinglog/player.t.html');
        }
    }

    function action_lastlog_player(){
        if($meeting_key = $_REQUEST['meeting_key']){

            $this->session->set('log_meeting_key', $meeting_key);

            $lang = $this->session->get('lang');
            if($lang == "en"){
                $lang = 2;
            }else{
                $lang = 1;
            }

            $accessLog = new AccessLog();
            $accessLog->access('lastlogplayer');

            $this->template->assign('lang', $lang);
            $this->template->assign('isPresen',$_REQUEST['isPresen']);
            $this->display('admin/meetinglog/log_player.t.html');
        }
    }



    /**
     * 会議記録パスワード確認フォーム表示（レコード削除時）
     */
    function showPwdDialog(){

        $this->template->assign('log_key', $this->log_key);
        $this->display('admin/admin_meetinglog_delete_pwd.t.html');
        exit;
    }

    function action_admin_log(){
        $admin_info = $this->session->get('user_info');
        foreach ($admin_info as $key => $value) {
            $this->template->assign($key, $value);
        }
        if ($admin_edit_info = $this->session->get('admin_edit_info')) {
            foreach($admin_edit_info as $key => $value){
                $this->template->assign($key, $value);
            }
        }
        $this->display('admin/log.t.html');
    }
}

$main =& new AppAdmin();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
