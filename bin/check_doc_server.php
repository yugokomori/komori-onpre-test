#!/usr/local/bin/php
<?php
require_once("set_env.php");
require_once('classes/AppFrame.class.php');
require_once("lib/EZLib/EZUtil/EZRtmp.class.php");

class API_DocumentServerStatus extends AppFrame
{
    function init() {
    }

    function default_view() {
    	$config = $this->config->getAll("DOCUMENT");
    	$rtmp = new EZRtmp();
        $fms_list = split(",", $config['fms']);
        $doc_default_count = $config["doc_default_count"] ? $config["doc_default_count"] : '2';
        foreach($fms_list as $fms) {
        	$document_app = sprintf($config["rtmp"], $fms, $config["appli"]);
            $this->logger2->info(array($document_app,$config["rtmp"], $fms, $config["appli"]));
            // メール資料貼り込み
            $cmd = "\"".
                "h = RTMPHack.new; " .
                " h.connection_uri = '".$document_app."';" .
                " h.connection_args = [0x320];" .
                " h.method_name = 'getAvailable';" .
                " h.execute;".
                "\"";
            $result = $rtmp->run($cmd);
            $this->logger2->debug(array($cmd, $result));
            list($active, $max_connection) = split("/", $result);
            if ($max_connection < $doc_default_count) {
                $this->logger2->error("DocサーバがFMSに接続できてない可能性があります。");
                require_once ("lib/EZLib/EZMail/EZSmtp.class.php");
				$mail = new EZSmtp(null, "ja", "UTF-8");
				$mail->setSubject("ＶＡ 会議システム　DOCサーバダウンアラート");
				$mail->setFrom(N2MY_DOC_ALERT_FROM);
				$mail->setReturnPath(NOREPLY_ADDRESS);
				$mail->setTo(N2MY_DOC_ALERT_TO);
				$template = new EZTemplate($this->config->getAll('SMARTY_DIR'));
				$template->assign('send_date', date('Y-m-d H:i:s'));
				$template->assign('fms_address', $fms);
				$body = $template->fetch('common/mail/doc_server_alert.t.txt');
				$mail->setBody($body);
				$mail->send();
            } else {
                $this->logger2->info($fms, "DocumentGateway");
            }
        }
    }
}
$main = new API_DocumentServerStatus();
$main->execute();
