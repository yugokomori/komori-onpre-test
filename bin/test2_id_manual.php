<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
ini_set("memory_limit","2000M");
set_time_limit(0);
require_once ("set_env.php");
require_once ('classes/AppFrame.class.php');
require_once ('classes/mgm/dbi/user.dbi.php');
require_once("classes/N2MY_Account.class.php");
require_once ('classes/dbi/member.dbi.php');
require_once ('classes/dbi/member_group.dbi.php');
require_once ('classes/dbi/member_status.dbi.php');
require_once ('classes/dbi/user.dbi.php');
require_once ('config/config.inc.php');
require_once ('lib/EZLib/EZUtil/EZEncrypt.class.php');
require_once( "lib/EZLib/EZCore/EZLogger2.class.php" );
require_once ('lib/EZLib/EZUtil/EZCsv.class.php');
require_once ('classes/dbi/blacklist.dbi.php');
require_once ('classes/dbi/whitelist.dbi.php');


class Test2IdManual extends AppFrame
{
    var $logger = null;
    var $member = null;
    var $blacklist = null;
    var $whitelist = null;
    var $account_dsn = null;

    function init() {
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $this->member      = new MemberTable($this->get_dsn());
        $this->blacklist   = new blacklistTable($this->get_dsn());
        $this->whitelist   = new whitelistTable($this->get_dsn());
    }

    var $table_name;

    /* -------------------------------
    * 手動実行用追加
    * bin/id_list 配下にファイルを配置して名前を指定して実行
    * ファイル名は固定で、before.csvとafter.csv と表記
    *
    * 引数1 ： Table 名の指定 → whitelist or blacklist
    */
    function __construct($argv) {
        parent::__construct();
        $this->table_name = $argv;
    }

    function default_view() {
        $table_name = $this->table_name;
        $user_info = $this->session->get("user_info");
        $user_key = $user_info["user_key"];


        // 対象となるTABLEチェック
        switch($table_name) {
            case "whitelist":
                $col_name  = "member_id";
                $table = "whitelist";
                break;
            case "blacklist":
                $col_name  = "member_id";
                $table = "blacklist";
                break;
            default:
                echo "例外発生、Table 名の指定 → whitelist or blacklist のどちらかを指定 \n";
                exit;
        }


        // before ファイル読み込み
        $data = N2MY_APP_DIR."bin/id_list/before.csv";

        //CSVファイルチェック
        if (is_readable($data)) {
            echo " 変更前のCSVファイル = OK \n";
        } else{
            echo " 変更前のCSVファイルに不備があります、形式を確認してください。";
            exit;
        }

        //CSVファイル格納
        $csv = new EZCsv(true,$output_encoding,"UTF-8");
        $csv->open(N2MY_APP_DIR.'/bin/id_list/before.csv', "r");
        $rows_b = array();
        while($row = $csv->getNext(true)) {
            //            $rows_b[] = $row[$col_name];
            $rows_b[] = $row["member_id"];
        }
        $csv->close();

        // after ファイル読みこみ
        $data = N2MY_APP_DIR."bin/id_list/after.csv";

        //CSVファイルチェック
            if (is_readable($data)) {
            echo " 変更後のCSVファイル = OK \n";
        } else{
            echo " 変更後のCSVファイルに不備があります、形式を確認してください。";
            exit;
        }
        
        //CSVファイル格納
        $csv = new EZCsv(true,$output_encoding,"UTF-8");
        $csv->open(N2MY_APP_DIR.'/bin/id_list/after.csv', "r");
        $rows_a = array();
        while($row = $csv->getNext(true)) {
            //            $rows_a[] = $row[$col_name];
            $rows_a[] = $row["member_id"];
        }
        $csv->close();

        // 変更前後の行数チェック
        $count_a = count($rows_a);
        $count_b = count($rows_b);
        
        echo "before 件数 = ". $count_b ."\n";
        echo "After  件数 = ". $count_a ."\n";
        echo " ------------------ \n";
        
        
        if($count_a !== $count_b){
            echo "CSVファイルの行数に差異があります。ファイルの確認してください";
            exit;
        }

        // 結果出力用
        $count = 0 ;
        $ok_count = 0 ;
        $ng_count = 0 ;
        foreach($rows_b as $member_row) {

            // data スキーマの処理
            ///////////////
            $update_data = array("member_id"  => $rows_a[$count]);

            $sql = "SELECT count(*) FROM " . $table . " WHERE " . "member_id" . "='". $rows_b[$count]."'";
            $preCheck_data = $this->$table->_conn->getOne($sql);

            if ($preCheck_data){
                $result =  $this->$table->update($update_data,  sprintf("member_id='%s'", $rows_b[$count] ));
                // check
                if (DB::isError($result)) {
                    $check =($check.  " <font color='red'> ★ : data result :");
                    $check =($check." " .$rows_b[$count]. " => " . $rows_a[$count] . "\n");
                    $check_log =($check_log.  " ★ : data result :");
                    $check_log =($check_log." " .$rows_b[$count]. " => " . $rows_a[$count] . "\n");
                } else {
                    $check =($check.  "<font color='black'> ok : data result :");
                    $check =($check." " .$rows_b[$count]. " => " . $rows_a[$count] . "\n");
                    $check_log =($check_log.  " ok : data result :");
                    $check_log =($check_log." " .$rows_b[$count]. " => " . $rows_a[$count] . "\n");
                }
            }else{
                $check =($check.  " <font color='red'> SKIP : data result :");
                $check =($check." " .$rows_b[$count]. " :  not data \n");
                $check_log =($check_log.  " SKIP : data result :");
                $check_log =($check_log." " .$rows_b[$count]. " :  not data \n");
            }
            $count = $count + 1;
        }

        // logファイル出力
        $config = parse_ini_file( sprintf( "%sconfig/config.ini", N2MY_APP_DIR ), true);
        $log_dir = $config["LOGGER"]["log_dir"];
        $this->_log ( $check_log , $log_dir , $table);
        
    }

    private function _log($string , $result_log_dir , $table){
        $log_prefix = $table."Table_log";
        $log_file = sprintf("%s/%s_%s.log", $result_log_dir, $log_prefix, date('Ymd', time()));
        $fp = fopen($log_file, "a");

        if($fp){
            fwrite($fp, $string."\n");
        }
        fclose($fp);

        echo "作成されたログファイルを確認ください。\n".$log_file. "\n";
        
    }

}

$main =& new Test2IdManual($argv[1]);
$main->execute();

/*
 * Local variables:
* tab-width: 4
* c-basic-offset: 4
* c-hanging-comment-ender-p: nil
* End:
*/
?>

