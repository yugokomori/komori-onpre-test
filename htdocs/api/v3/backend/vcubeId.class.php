<?php

require_once("classes/N2MY_DBI.class.php");
require_once "classes/dbi/room.dbi.php";
require_once('classes/mgm/MGM_Auth.class.php');
require_once('lib/EZLib/EZCore/EZLogger2.class.php');
require_once('lib/EZLib/EZHTML/EZValidator.class.php');
require_once('lib/EZLib/EZUtil/EZEncrypt.class.php');

class vcubeId {
   var $auth_dsn    = null;
   var $dsn         = null;
   var $server_info = null;
   var $user_info   = null;
   var $dummyUserId = null;
   
   var $services = null;
   var $sOptions = null;
   
  //private $purgeLimit = Constants::PURGE_LIMIT;
  public function __construct($auth_dsn, $userId){


    $this->logger2 = EZLogger2::getInstance($this->config);

    $this->auth_dsn = $auth_dsn;
    $this->dummyUserId = $userId;
    
    $this->logger2->info($this->dsn);
    
  }

  //初期設定
  private function init($user_id = null)
  {
    try{
      $obj_MGMClass = new MGM_AuthClass( $this->auth_dsn);

      $this->user_info   = $obj_MGMClass->getUserInfoById($user_id);
      $this->server_info = $obj_MGMClass->getServerInfo( $this->user_info["server_key"]);
       //dsn
      $this->dsn    = $this->server_info["dsn"];

      if(!$this->dsn){
        return false;
      }

      return true;
    }catch(Exception $e){
      $this->logger2->error($e->getMessage());
      return false;
    }

  }

  public function testFunc($user_id, $consumerKey, $redirectUrl)
  {
    $this->logger2->info("testFunc API start");
    try{
      if(!$this->init($user_id)){
        $this->logger2->error("user not found");
        throw new Exception("user not Found");
      }

      $this->logger2->info($user_id);
      $this->logger2->info($consumerKey);
      $this->logger2->info($redirectUrl);


      $ret["result"] = true;
      $ret["user_id"]     = $user_id;
      $ret["consumerKey"] = $consumerKey;
      $ret["redirectUrl"] = $redirectUrl;

    }catch(Exception $e){
      $ret["result"]      = false;
      $ret["user_id"]     = $user_id;
      $ret["consumerKey"] = $consumerKey;
      $ret["message"]     = $e->getMessage();
    }
    return $ret;
  }

  public function checkUserId($userId = null, $userPw = null){
  
    try{
      if(!$this->init($this->dummyUserId)){
        $this->logger2->error("init error");
        throw new Exception("init error");
      }
      
      $user_db = new N2MY_DB($this->dsn, "user");
      
      if ( $user_db->numRows( sprintf("user_id='%s' AND user_delete_status < 2 ", $userId ) ) > 0 ){
        $ret["result"] = true;
      } else {
        $ret["result"] = false;
      }
      
    }catch(Exception $e){
      $ret["result"]  = false;
      $ret["message"] = $e->getMessage();
    }
    
    return $ret;
  }
  
  //有効なメンバーのリスト取得
  public function getMemberList(){
    try{
      if(!$this->init($this->dummyUserId)){
        $this->logger2->error("init error");
        throw new Exception("init error");
      }
      
      $member_db = new N2MY_DB($this->dsn, "member");
      $sql  = "SELECT ";
      $sql .= "user.user_id, ";
      $sql .= "member.member_id ";
      $sql .= "FROM ";
      $sql .= "user, member ";
      $sql .= "WHERE ";
      $sql .= "user.user_key = member.user_key ";
      $sql .= "AND member.member_status > -1 ";
      
      $this->logger2->info($sql);
      
      $rs = $member_db->_conn->query($sql);
      $member_list = array();
      while( $member = $rs->fetchRow( DB_FETCHMODE_ASSOC ) ){
        $member_list[] = $member;
      }
      
      $ret["result"]     = true;
      $ret["memberList"] = $member_list;
      
    }catch(Exception $e){
      $ret["result"]  = false;
      $ret["message"] = $e->getMessage();
    }
    
    return $ret;
  }
  
  //メンバーIDの有無確認
  public function existMemberId($memberId = null){
    try{
      if(!$this->init($this->dummyUserId)){
        $this->logger2->error("init error");
        throw new Exception("init error");
      }
      
      $member_db = new N2MY_DB($this->dsn, "member");
      $sql  = "SELECT ";
      $sql .= " member_id ";
      $sql .= "FROM ";
      $sql .= "member ";
      $sql .= "WHERE ";
      $sql .= "member_id = '".mysql_real_escape_string($memberId)."' ";
      
      $this->logger2->info($sql);
      
      $rs = $member_db->_conn->query($sql);
      $rtn = false; 
      if($cnt = $rs->numRows( DB_FETCHMODE_ASSOC ) > 0){
        $rtn = true;
      }
      
      $ret["result"]     = true;
      $ret["return"]     = $rtn;
      
    }catch(Exception $e){
      $ret["result"]  = false;
      $ret["message"] = $e->getMessage("");
    }
    
    return $ret;
  }
  
  public function getMember($memberId = null){
    try{
      if(!$this->init($this->dummyUserId)){
        $this->logger2->error("init error");
        throw new Exception("init error");
      }
      
      
      //メンバー情報
      $member_db = new N2MY_DB($this->dsn, "member");
      $mSql  = "SELECT ";
      $mSql .= "user.user_id, ";
      $mSql .= "member.* ";
      $mSql .= "FROM ";
      $mSql .= "user, member ";
      $mSql .= "WHERE ";
      $mSql .= "member.member_id = '".mysql_real_escape_string($memberId)."' ";
      $mSql .= "AND user.user_key = member.user_key ";
      $mSql .= "AND member.member_status > -1 ";
      $mSql .= "LIMIT 1 ";
      
      $this->logger2->info($mSql);
      
      $rs = $member_db->_conn->query($mSql);
      
      $memberInfo = array();
      while( $member = $rs->fetchRow( DB_FETCHMODE_ASSOC ) ){
      
        $memberInfo[] = $member;
      }
      
      if(count($memberInfo) > 0){
        //メンバーの部屋情報
        $room_db = new N2MY_DB($this->dsn, "room");
        $rSql  = "SELECT ";
        $rSql .= "member.member_id, ";
        $rSql .= "room.*, ";
        $rSql .= "room_plan.* ";
        $rSql .= "FROM ";
        $rSql .= "member, member_room_relation, room LEFT JOIN room_plan on ( room.room_key = room_plan.room_key ) ";
        $rSql .= "WHERE ";
        $rSql .= "member.member_id = '".mysql_real_escape_string($memberId)."' ";
        $rSql .= "AND member.member_key = member_room_relation.member_key ";
        $rSql .= "AND member_room_relation.room_key = room.room_key ";
        
        $this->logger2->info($rSql);
        
        $rs = $room_db->_conn->query($rSql);
        
        $osOption_db = new N2MY_DB($this->dsn, "ordered_service_option");
        
        $rooms = array();
        while( $room = $rs->fetchRow( DB_FETCHMODE_ASSOC ) ){
           //サービス詳細取得
           $this->initServices();
           foreach($this->services as $service){
            if($room["service_key"] == $service["service_key"]){
              $room["service"] = $service;
              break;
            }
           }
           
           //order_service_option
           $osOptions = $osOption_db->getRowsAssoc(sprintf("room_key = '%s' ", $room["room_key"]));
           
           $tmpOsOptions = array();
           foreach($osOptions as $osOption){
            foreach($this->sOptions as $sOption){
             if($osOption["service_option_key"] == $sOption["service_option_key"]){
               $osOption["service_option"] = $sOption;
               break;
             }
            }
            
            $tmpOsOptions[] = $osOption;
           }
           
           $room["ordered_serivce_option"] = $tmpOsOptions;
           
           $rooms[] = $room;
        }
        
        $ret["result"]  = true;
        $ret["member"]  = $memberInfo[0];
        $ret["rooms"]    = $rooms;
        $ret["message"] = "success";
        
      }else{
        $ret["result"]  = true;
        $ret["message"] = "member not found";
      }
      
      
      
    }catch(Exception $e){
      $ret["result"]  = false;
      $ret["message"] = $e->getMessage();
    }
    
    return $ret;
  }
  
  public function getUser($userId = null){
    try{
      if(!$this->init($this->dummyUserId)){
        $this->logger2->error("init error");
        throw new Exception("init error");
      }
      
      
      //メンバー情報
      $user_db = new N2MY_DB($this->dsn, "user");
      $user = $user_db->getRow( sprintf( "user_id ='%s' ", $userId ) );
      if($user){
        //部屋情報
        $room_db = new N2MY_DB($this->dsn, "room");
        $rSql  = "SELECT ";
        $rSql .= "user.user_id, ";
        $rSql .= "room.* ";
        $rSql .= "FROM ";
        $rSql .= "user, room LEFT JOIN room_plan ON ( room.room_key = room_plan.room_key ) ";
        $rSql .= "WHERE ";
        $rSql .= "user.user_id = '".mysql_real_escape_string($userId)."' ";
        $rSql .= "AND user.user_key = room.user_key ";
        
        $this->logger2->info($rSql);
        
        $rs = $room_db->_conn->query($rSql);
        
        $rooms = array();
        
        $osOption_db = new N2MY_DB($this->dsn, "ordered_service_option");
        while( $room = $rs->fetchRow( DB_FETCHMODE_ASSOC ) ){
           //サービス詳細取得
           $this->initServices();
           foreach($this->services as $service){
            if($room["service_key"] == $service["service_key"]){
              $room["service"] = $service;
              break;
            }
           }
           
           //order_service_option
           $osOptions = $osOption_db->getRowsAssoc(sprintf("room_key = '%s' ", $room["room_key"]));
           
           $tmpOsOptions = array();
           foreach($osOptions as $osOption){
            foreach($this->sOptions as $sOption){
             if($osOption["service_option_key"] == $sOption["service_option_key"]){
               $osOption["service_option"] = $sOption;
               break;
             }
            }
            
            $tmpOsOptions[] = $osOption;
           }
           
           $room["ordered_serivce_option"] = $tmpOsOptions;
           
           $rooms[] = $room;
        }
        
        $ret["result"]  = true;
        $ret["user"]    = $user;
        $ret["rooms"]   = $rooms;
        $ret["message"] = "success";
        
      }else{
        $ret["result"]  = true;
        $ret["message"] = "user not found";
      }
      
      
      
    }catch(Exception $e){
      $ret["result"]  = false;
      $ret["message"] = $e->getMessage();
    }
    
    return $ret;
  }
  
  public function initServices(){
    $service_db = new N2MY_DB($this->auth_dsn, "service");
    $this->services = $service_db->getRowsAssoc();
    
    $sOption_db = new N2MY_DB($this->auth_dsn, "service_option");
    $this->sOptions = $sOption_db->getRowsAssoc();
    
  }
  
}
