<?php

interface ICreateParticipant {
	function createParticipantRecord();
	function removeParticipantRecord();
	function setParticipantRecord();
}
