<?php
/**
 * Created on 2009/09/01
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 *
 * @todo メールテンプレート取得
 * @todo htmlテンプレートの関数埋め込み
 * @todo jsに関数埋め込み
 * @todo 件数
 * @todo 言語ごとの編集機能
 * @todo
 */
require_once("classes/AppFrame.class.php");
require_once("classes/multilang/language.dbi.php");
require_once("classes/multilang/define.dbi.php");
require_once("classes/multilang/word.dbi.php");
require_once("lib/pear/Pager.php");

class AppMultiLang extends AppFrame {

    var $lang_dsn = "";
    var $obj_lang = null;
    var $obj_define = null;
    var $obj_word = null;
    var $flg_row = array(0 => "未確認", 1 => "保留", 2 => "確認済", 3 => "翻訳不要", 4 => "削除");
    var $now = null;
    var $_name_space = null;

    function init() {
        ini_set("memory_limit", "128M");
        $this->now = date("Y-m-d H:i:s");
        $this->lang_dsn = $this->config->get("GLOBAL", "lang_dsn");
        $this->obj_lang = new LanguageTable($this->lang_dsn);
        $this->obj_define = new DefineTable($this->lang_dsn);
        $this->obj_word = new WordTable($this->lang_dsn);
        $this->_name_space = $this->getActionName();
//        $this->obj_lang->tableInfoBuilder("multlang");
//        $this->obj_define->tableInfoBuilder("multlang");
//        $this->obj_word->tableInfoBuilder("multlang");
    }

    /**
     * 一覧表示
     */
    function default_view() {
        $this->html_header();
        $search_form = $this->session->get("search_form",__FUNCTION__);
        $whrer = "flg = 1";
        $lang_list = $this->obj_lang->getRowsAssoc($whrer);
        $type_row = $this->obj_define->_conn->getCol("SELECT type FROM define GROUP BY type");
        $define_columns = $this->obj_define->getTableInfo();
        $define_columns["path"]["item"]["search"] = "like";
        $define_columns["define_id"]["item"]["search"] = "like";
        $define_columns["name"]["item"]["search"] = "like";
        $where = $this->obj_define->getWhere($define_columns, $search_form, "AND");
        $limit = 20;
        $page = $this->request->get("page", $this->session->get("page", __FUNCTION__, 1));
        $this->session->set("page", $page, __FUNCTION__);
        $offset = $limit * ($page - 1);
        $total_count = $this->obj_define->numRows($where);
        if (!$sort = $this->session->get("sort", __FUNCTION__)) {
            $sort = array("type" => "asc", "path" => "asc");
        }
        $define_list = $this->obj_define->getRowsAssoc($where, $sort, $limit, $offset);
        $pager = $this->setPager($limit, $page, $total_count);
        $this->_pager($pager);
        print '<table class="data" width="100%">';
        print '<tr>';
        print '<th>check</th>';
        print '<th><a href="?action_sort=define_no&act='.htmlspecialchars(__FUNCTION__).'">NO</a></th>';
        print '<th><a href="?action_sort=type&act='.htmlspecialchars(__FUNCTION__).'">タイプ</a></th>';
        print '<th><a href="?action_sort=path&act='.htmlspecialchars(__FUNCTION__).'">パス</a></th>';
        print '<th><a href="?action_sort=define_id&act='.htmlspecialchars(__FUNCTION__).'">ID</a></th>';
        print '<th><a href="?action_sort=name&act='.htmlspecialchars(__FUNCTION__).'">文言</a></th>';
        print '<th><a href="?action_sort=flg&act='.htmlspecialchars(__FUNCTION__).'">check</a></th>';
//        print '<th>作成日</th>';
        print '<th><a href="?action_sort=update_datetime&act='.htmlspecialchars(__FUNCTION__).'">更新日</a></th>';
        print '<th>action</th>';
        print '</tr>';
        print '<form action="'.$_SERVER["SCRIPT_NAME"].'" method="post">';
        print '<input type="hidden" name="act" value="'.htmlspecialchars(__FUNCTION__).'">';
        print '<tr>';
        print '<th><input type="checkbox" id="checkall"/></th>';
        print '<th><input type="text" name="define_no" value="'.htmlspecialchars($search_form["define_no"]).'" size="5" /></th>';
        print '<th>';
        print '<select name="type">';
        print '<option value="">all</option>';
        foreach($type_row as $type) {
            print '<option value="'.$type.'"';
            if ($search_form["type"] == $type) {
                print ' selected="selected"';
            }
            print '>'.$type.'</option>';
        }
        print '</select>';
        print '</th>';
        print '<th><input type="text" name="path" value="'.htmlspecialchars($search_form["path"]).'" size="20" /></th>';
        print '<th><input type="text" name="define_id" value="'.htmlspecialchars($search_form["define_id"]).'" size="20" /></th>';
        print '<th><input type="text" name="name" value="'.htmlspecialchars($search_form["name"]).'" size="40" /></th>';
        print '<th>';
        print '<select name="flg">';
        print '<option value="">all</option>';
        foreach($this->flg_row as $key => $val) {
            print '<option value="'.$key.'"';
            if ($search_form["flg"] !== "") {
                if ($search_form["flg"] == $key) {
                    print ' selected="selected"';
                }
            }
            print '>'.$val.'</option>';
        }
        print '</select>';
        print '</th>';
//        print '<th>create_datetime</th>';
        print '<th>-</th>';
        print '<th><input type="submit" name="action_search" value="検索" /></th>';
        print '</tr>';
        print '</form>';
        print '<form action="'.$_SERVER["SCRIPT_NAME"].'" id="form_list" method="post">';
        print '<input type="hidden" name="_action" value="">';
        print '<input type="hidden" name="flg" value="">';
        foreach ($define_list as $row) {
            print '<tr';
            switch ($row["flg"]) {
                case "1";
                    print ' bgcolor="#dddddd"';
                    break;
                case "2";
                    print ' bgcolor="#ffdddd"';
                    break;
            }
            print '>';
            print '<td align="center"><input type="checkbox" class="row_select" name="check_flg['.$row["define_no"].']" value="'.$row["define_no"].'" /></td>';
            print '<td>'.$row["define_no"].'</td>';
            print '<td>'.$row["type"].'</td>';
            print '<td>'.$row["path"].'</td>';
            print '<td>'.$row["define_id"].'</td>';
            print '<td>'.htmlspecialchars($row["name"]).'</td>';
            print '<td>'.$this->flg_row[$row["flg"]].'</td>';
//            print '<td>'.$row["create_datetime"].'</td>';
            print '<td>'.$row["update_datetime"].'</td>';
            print '<td>';
            print '  <input type="button" name="action_edit" value="追加" onclick="add_form('.$row["define_no"].');">';
            print '  <input type="button" name="action_edit" value="編集" onclick="edit_form('.$row["define_no"].');">';
            print '</td>';
            print '</tr>';
        }
        print '</table>';
        $this->_pager($pager);
        print '<br />';
        foreach($this->flg_row as $key => $val) {
            print '<input type="button" value="'.$val.'" onclick="check_edit(this.form, '.$key.');"/>';
        }
        print '</form>';
    }

    function action_config_from() {
        $this->html_header();
        $whrer = "support = 1";
        $lang_list = $this->obj_lang->getRowsAssoc($whrer);
        print '<form action="./" method="post">';
        print '<input type="hidden" name="action_config" />';
        print '<table class="data">';
        print '<tr>';
        print '<th>適用</th>';
        print '<th>ID</th>';
        print '<th>言語</th>';
        print '<th>ロケールID</th>';
        print '<tr>';
        foreach ($lang_list as $lang_row) {
            print '<tr>';
            print '<td><input type="checkbox" name="set_lang['.$lang_row["lang_cd"].']" value="'.$lang_row["lang_cd"].'" id="'.$lang_row["lang_cd"].'"';
            if ($lang_row["flg"] == 1) {
                print ' checked="checked"';
            }
            print '></td>';
            print '<td>'.$lang_row["lang_cd"].'</td>';
            print '<td><label for="'.$lang_row["lang_cd"].'">'.$lang_row["lang_name"].'</label></td>';
            print '<td><input type="text" name="locale_id['.$lang_row["lang_cd"].']" value="'.$lang_row["locale_id"].'" /></td>';
            print '</tr>';
        }
        print '</table>';
        print '<input type="submit" name="action_config" value="設定">';
        print '</form>';
    }

    function action_config() {
        $set_lang = $this->request->get("set_lang");
        $where = "support = '1'";
        $data = array("flg" => 0);
        $this->obj_lang->update($data, $where);
        foreach($set_lang as $lang_cd) {
            $where = "lang_cd = '".$lang_cd."'";
            $data = array("flg" => 1);
            $this->obj_lang->update($data, $where);
        }
        $locale_id = $this->request->get("locale_id");
        foreach($locale_id as $lang_cd => $locale) {
            $where = "lang_cd = '".$lang_cd."'";
            $data = array("locale_id" => $locale);
            $this->obj_lang->update($data, $where);
        }
        $this->action_config_from();
    }

    /**
     * エクスポートフォーム
     */
    function action_export_form() {
        $this->html_header();
        $whrer = "flg = 1";
        $lang_list = $this->obj_lang->getRowsAssoc($whrer, null , null, null, null, "lang_cd");
        $set_lang = $this->request->get("set_lang");
        $lang_from = $set_lang["from"] ? $set_lang["from"] : "ja";
        $lang_to = $set_lang["to"] ? $set_lang["to"] : "en";

        print '<form action="./" method="post">';
        print '<select name="set_lang[from]">';
        foreach ($lang_list as $lang_row) {
            if ($lang_from == $lang_row["lang_cd"]) {
                print '<option value="'.$lang_row["lang_cd"].'" selected="selected">'.$lang_row["lang_name"].'</option>';
                $lang_from_name = $lang_row["lang_name"];
            } else {
                print '<option value="'.$lang_row["lang_cd"].'">'.$lang_row["lang_name"].'</option>';
            }
        }
        print '</select>';

        print ' -> ';
        print '<select name="set_lang[to]">';
        foreach ($lang_list as $lang_row) {
            if ($lang_to == $lang_row["lang_cd"]) {
                print '<option value="'.$lang_row["lang_cd"].'" selected="selected">'.$lang_row["lang_name"].'</option>';
                $lang_to_name = $lang_row["lang_name"];
            } else {
                print '<option value="'.$lang_row["lang_cd"].'">'.$lang_row["lang_name"].'</option>';
            }
        }
        print '</select> ';
        print '<input type="submit" name="action_export" value="エクスポート"/>';
        print '</form>';
    }

    /**
     * エクスポート
     */
    function action_export() {
        require_once("lib/EZLib/EZUtil/EZCsv.class.php");
        $csv = new EZCsv(true, "utf-8", "utf-8", "\t");
        $dir = $this->get_work_dir();
        $tmpfile = tempnam($dir, "csv_");
        $csv->open($tmpfile, "w");
        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="'.date("Ymd").'.csv"');
        $set_lang = $this->request->get("set_lang");
        $lang_from = $set_lang["from"] ? $set_lang["from"] : "ja";
        $lang_to = $set_lang["to"] ? $set_lang["to"] : "en";
        $where = " flg = 2";
//                "type = 'template_html'";
        $sort = array(
            "define_no" => "asc",
            );
        $define_list = $this->obj_define->getRowsAssoc($where, $sort, null, null, "define_no, type, path, name");
        $list = array();
        $_path = "";
        $header = array(
            "define_no" => "define_no",
            "type" => "type",
            "path" => "path",
            "original" => "original",
            $lang_from => $lang_from,
            $lang_to => $lang_to
            );
        $csv->setHeader($header);
        $csv->write($header);
        foreach($define_list as $row) {
//            if (!in_array($row["name"], $list)) {
                if ($_path != $row["path"]) {
                    $path = $row["path"];
                } else {
                    $path = "";
                }
                $_path = $row["path"];
                //header
                $csv->write(array(
                    "define_no" => $row["define_no"],
                    "type" => $row["type"],
                    "path" => $row["path"],
                    "original" => $row["name"],
                    $lang_from => $this->_get_message($lang_from, $row["define_no"]),
                    $lang_to => $this->_get_message($lang_to, $row["define_no"]),
                    ));
                $list[] = $row["name"];
//            }
        }
        // CSV出力
        $csv->close();
        $fp = fopen($tmpfile, "r");
        $contents = fread($fp, filesize($tmpfile));
        fclose($fp);
        print $contents;
        unlink($tmpfile);
    }

    function action_backup_form() {
        $this->html_header();
?>
<form action="index.php" method="post">
  <table border="0">
    <tr>
      <td>テーブル削除: </td>
      <td><input type="checkbox" name="sql_drop_table" <?php if(isset($_REQUEST['action']) && ! isset($_REQUEST['sql_drop_table'])) ; else echo 'checked' ?> /></td>
    </tr>
    <tr>
      <td>テーブル作成: </td>
      <td><input type="checkbox" name="sql_create_table" <?php if(isset($_REQUEST['action']) && ! isset($_REQUEST['sql_create_table'])) ; else echo 'checked' ?> /></td>
    </tr>
    <tr>
      <td>データ: </td>
      <td><input type="checkbox" name="sql_table_data"  <?php if(isset($_REQUEST['action']) && ! isset($_REQUEST['sql_table_data'])) ; else echo 'checked' ?>/></td>
    </tr>
    <tr>
      <td>スナップショット作成のみ: </td>
      <td><input type="checkbox" name="snapshot_only"  <?php if(isset($_REQUEST['action']) && ! isset($_REQUEST['snapshot_only'])) ; else echo 'checked' ?>/></td>
    </tr>
  </table>
<input type="submit" name="action_backup" value="バックアップ"><br />
</form>
<hr size="1" />
Snapshot<br />
    <?php
        $list = glob(N2MY_APP_DIR.'var/mysqldump/*.sql');
        foreach($list as $dumpfile)
        {
            $filename = basename($dumpfile);
            echo '<a href="?action_snapshot='.$filename.'">'.$filename.'</a> [ '.number_format(filesize($dumpfile)).' byte ]<br />';
        }
    }

    function action_snapshot() {
        $file = $this->request->get(__FUNCTION__);
        $fullpath = N2MY_APP_DIR.'var/mysqldump/'.$file;
        header('Content-type: text/plain');
        header('Content-Disposition: attachment; filename="'.$file);
        echo file_get_contents($fullpath);
    }

    function action_backup() {
        $parm = parse_url($this->lang_dsn);
        $mysql_host = $parm["host"];
        $mysql_username = $parm["user"];
        $mysql_password = $parm["pass"];
        $mysql_database = substr($parm["path"], 1);
        ob_start();
        echo "/*mysqldump.php version $mysqldump_version */\n";
        $link = mysql_connect($mysql_host, $mysql_username, $mysql_password);
        if (!$link) {
            $this->html_header();
            die(mysql_error());
        }
        $db_selected = mysql_select_db($mysql_database, $link);
        if (!$db_selected) {
            $this->html_header();
            die(mysql_error());
        }
        $this->_mysqldump($mysql_database);
        $contents = ob_get_contents();
        mkdir(N2MY_APP_DIR.'var/mysqldump/', 0777, true);
        file_put_contents(N2MY_APP_DIR.'var/mysqldump/'.$mysql_database.'_'.date('Ymd-His').'.sql', $contents);
        if ($this->request->get("snapshot_only")) {
            ob_end_clean();
            header("Location: index.php?action_backup_form");
            exit;
        }
        header('Content-type: text/plain');
        header('Content-Disposition: attachment; filename="'.$mysql_database."_".date('Ymd').'.sql"');
        ob_end_flush();
    }

    /**
     * MySQLdump
     * この機能、EZFrameに欲しいな
     */
    function _mysqldump($mysql_database)
    {
        $sql="show tables;";
        $result= mysql_query($sql);
        if( $result)
        {
            while( $row= mysql_fetch_row($result))
            {
                $this->_mysqldump_table_structure($row[0]);
                if( isset($_REQUEST['sql_table_data']))
                {
                    $this->_mysqldump_table_data($row[0]);
                }
            }
        }
        else
        {
            echo "/* no tables in $mysql_database */\n";
        }
        mysql_free_result($result);
    }

    function _mysqldump_table_structure($table)
    {
        echo "/* Table structure for table `$table` */\n";
        if( isset($_REQUEST['sql_drop_table']))
        {
            echo "DROP TABLE IF EXISTS `$table`;\n\n";
        }
        if( isset($_REQUEST['sql_create_table']))
        {
            $sql="show create table `$table`; ";
            $result=mysql_query($sql);
            if( $result)
            {
                if($row= mysql_fetch_assoc($result))
                {
                    echo $row['Create Table'].";\n\n";
                }
            }
            mysql_free_result($result);
        }
    }

    function _mysqldump_table_data($table)
    {
        $sql="select * from `$table`;";
        $result = mysql_query($sql);
        if ( $result)
        {
            $num_rows= mysql_num_rows($result);
            $num_fields= mysql_num_fields($result);

            if ( $num_rows > 0)
            {
                echo "/* dumping data for table `$table` */\n";

                $field_type=array();
                $i=0;
                while( $i < $num_fields)
                {
                    $meta= mysql_fetch_field($result, $i);
                    array_push($field_type, $meta->type);
                    $i++;
                }

                //print_r( $field_type);
                echo "insert into `$table` values\n";
                $index=0;
                while( $row= mysql_fetch_row($result))
                {
                    echo "(";
                    for( $i=0; $i < $num_fields; $i++)
                    {
                        if( is_null( $row[$i]))
                            echo "null";
                        else
                        {
                            switch( $field_type[$i])
                            {
                                case 'int':
                                    echo $row[$i];
                                    break;
                                case 'string':
                                case 'blob' :
                                default:
                                    echo "'".mysql_real_escape_string($row[$i])."'";

                            }
                        }
                        if( $i < $num_fields-1)
                            echo ",";
                    }
                    echo ")";

                    if( $index < $num_rows-1)
                        echo ",";
                    else
                        echo ";";
                    echo "\n";

                    $index++;
                }
            }
        }
        mysql_free_result($result);
        echo "\n";
    }

    /**
     * データの反映対象を選択するフォーム
     */
    function action_fix_form() {
        $this->html_header();
        print '<form action="index.php" method="post" id="fix_form">';
        print '<input type="hidden" name="action_fix" />';
        $type_row = $this->obj_define->_conn->getCol("SELECT type FROM define GROUP BY type");
        foreach ($type_row as $type) {
            print '<input type="checkbox" name="type['.$type.']" value="1" id="'.$type.'" /><label for="'.$type.'">'.$type.'</label><br />';
        }
        print '<input type="button" value="反映" id="fix_button"/>';
        print '</form>';
    }

    /**
     * 翻訳データを各種言語ファイルで反映
     * なんか、po, moファイルつくるの面倒になってきた・・・。yamlにすっかなぁ・・・。
     */
    function action_fix() {
        $this->html_header();
        $lang_list   = $this->obj_lang->getRowsAssoc("flg = 1 AND support = 1", null , null, null, null, "lang_cd");
        print "<pre>";
        $type = $this->request->get("type");
        foreach ($lang_list as $lang_cd => $lang) {
            @mkdir(N2MY_APP_DIR."/config/lang/".$lang["locale_id"],          0777, true);
            @mkdir(N2MY_APP_DIR."/var/flash/lang/".$lang["locale_id"],       0777, true);
            @mkdir(N2MY_DOCUMENT_ROOT."/lang/".$lang["locale_id"]."/appli",  0777, true);
            @mkdir(N2MY_DOCUMENT_ROOT."/lang/".$lang["locale_id"]."/css",    0777, true);
            @mkdir(N2MY_DOCUMENT_ROOT."/lang/".$lang["locale_id"]."/images", 0777, true);
            @mkdir(N2MY_DOCUMENT_ROOT."/lang/".$lang["locale_id"]."/flash/tools",  0777, true);
            @mkdir(N2MY_DOCUMENT_ROOT."/lang/".$lang["locale_id"]."/js",     0777, true);
        }

         /**
         * flash AS3プロパティファイル
         */
        if (array_key_exists("flash", $type)) {
            // n2my_meeting.properties
            $file_name = "n2my_meeting.properties";
            $original_file = N2MY_APP_DIR."/setup/flash/".$file_name;
            $contents = file_get_contents($original_file);
            $define_list = $this->obj_define->getRowsAssoc("type = 'flash' AND path = 'n2my_meeting.properties' AND flg = 2", array("define_id" => "asc"), null, null, "define_no,define_id,name", "define_no");
            foreach ($lang_list as $lang_cd => $lang) {
                $word_list = $this->obj_word->getRowsAssoc("lang_cd = '".$lang_cd."'", null, null, null, "define_no,value", "define_no");
                $_contents = $contents;
                foreach($define_list as $key => $row) {
                    $_contents = str_replace("{".$row["define_id"]."}", $word_list[$key]["value"], $_contents);
                }
                $file = N2MY_APP_DIR."var/flash/lang/".$lang["locale_id"]."/".$file_name;
                print $file."<br />";
                if(!file_put_contents($file, $_contents)) {
                    print '<font color="red">Output Error!!</font></br>';
                }
            }
            // vrms.properties
            $file_name = "vrms.properties";
            $original_file = N2MY_APP_DIR."/setup/flash/".$file_name;
            $contents = file_get_contents($original_file);
            $define_list = $this->obj_define->getRowsAssoc("type = 'flash' AND path = 'vrms.properties' AND flg = 2", array("define_id" => "asc"), null, null, "define_no,define_id,name", "define_no");
            foreach ($lang_list as $lang_cd => $lang) {
                $word_list = $this->obj_word->getRowsAssoc("lang_cd = '".$lang_cd."'", null, null, null, "define_no,value", "define_no");
                $_contents = $contents;
                foreach($define_list as $key => $row) {
                    $_contents = str_replace("{".$row["define_id"]."}", $word_list[$key]["value"], $_contents);
                }
                $file = N2MY_APP_DIR."var/flash/lang/".$lang["locale_id"]."/".$file_name;
                print $file."<br />";
                if(!file_put_contents($file, $_contents)) {
                    print '<font color="red">Output Error!!</font></br>';
                }
            }
            // whiteboard.properties
            $file_name = "whiteboard.properties";
            $original_file = N2MY_APP_DIR."/setup/flash/".$file_name;
            $contents = file_get_contents($original_file);
            $define_list = $this->obj_define->getRowsAssoc("type = 'flash' AND path = 'whiteboard.properties' AND flg = 2", array("define_id" => "asc"), null, null, "define_no,define_id,name", "define_no");
            foreach ($lang_list as $lang_cd => $lang) {
                $word_list = $this->obj_word->getRowsAssoc("lang_cd = '".$lang_cd."'", null, null, null, "define_no,value", "define_no");
                $_contents = $contents;
                foreach($define_list as $key => $row) {
                    $_contents = str_replace("{".$row["define_id"]."}", $word_list[$key]["value"], $_contents);
                }
                $file = N2MY_APP_DIR."var/flash/lang/".$lang["locale_id"]."/".$file_name;
                print $file."<br />";
                if(!file_put_contents($file, $_contents)) {
                    print '<font color="red">Output Error!!</font></br>';
                }
            }
            // language.xml
            $file_name = "language.xml";
            $define_list = $this->obj_define->getRowsAssoc("type = 'flash' AND path = 'language.xml' AND flg = 2", array("define_id" => "asc"), null, null, "define_no,define_id,name", "define_no");
            // 全言語入った文言の作成
            $all_lang_file = $file = N2MY_DOCUMENT_ROOT."/lang/language_as3.xml";
            $all_lang_contents = '<?xml version="1.0" encoding="UTF-8"?>
<languages>' .
"\n";
            foreach ($lang_list as $lang_cd => $lang) {
                // 言語毎に個別に作成
                $file = N2MY_DOCUMENT_ROOT."/lang/".$lang["locale_id"]."/flash/language_as3.xml";
                $word_list = $this->obj_word->getRowsAssoc("lang_cd = '".$lang_cd."'", null, null, null, "define_no,value", "define_no");
$contents = '<?xml version="1.0" encoding="UTF-8"?>
<languages>
  <language id="'.$lang["locale_id"].'">' .
"\n";
$all_lang_contents .= '<language id="'.$lang["locale_id"].'">' .
"\n";
                foreach ($define_list as $define_no => $define) {
                    $value = $word_list[$define_no]["value"] ? $word_list[$define_no]["value"] : $define["name"];
                    $contents .= '    <string key="'.$define["define_id"].'" value="'.htmlspecialchars($value).'"/>'."\n";
                    $all_lang_contents.='    <string key="'.$define["define_id"].'" value="'.htmlspecialchars($value).'"/>'."\n";
                }
$contents .= '  </language>
</languages>';
$all_lang_contents .= '  </language>'."\n";
                print $file."<br />";
                if(!file_put_contents($file, $contents)) {
                    print '<font color="red">Output Error!!</font></br>';
                }

            }
            $all_lang_contents .= '</languages>';
            file_put_contents($all_lang_file, $all_lang_contents);
        }

         /**
         * flash_config
         */
        if (array_key_exists("flash_config", $type)) {
            // 言語別のファイル
            $define_list = $this->obj_define->getRowsAssoc("type = 'flash_config' AND flg = 2", array("define_id" => "asc"), null, null, "define_no,define_id,name", "define_no");
            foreach ($lang_list as $lang_cd => $lang) {
                $file = N2MY_DOCUMENT_ROOT."/lang/".$lang["locale_id"]."/flash/language.xml";
                $word_list = $this->obj_word->getRowsAssoc("lang_cd = '".$lang_cd."'", null, null, null, "define_no,value", "define_no");
$contents = '<?xml version="1.0" encoding="UTF-8"?>
<languages>
  <language id="'.$lang["locale_id"].'">' .
"\n";
                foreach ($define_list as $define_no => $define) {
                    $value = $word_list[$define_no]["value"] ? $word_list[$define_no]["value"] : $define["name"];
                    $contents .= '    <string key="'.$define["define_id"].'" value="'.htmlspecialchars($value).'"/>'."\n";
                }
$contents .= '  </language>
</languages>';
                print $file."<br />";
                if(!file_put_contents($file, $contents)) {
                    print '<font color="red">Output Error!!</font></br>';
                }
            }
        }
        /**
         * javascript 言語ファイル
         */
        if (array_key_exists("js", $type)) {
            // 言語別のファイル
            $define_list = $this->obj_define->getRowsAssoc("type = 'js' AND flg = 2", null, null, null, "define_no,name", "define_no");
            foreach ($lang_list as $lang_cd => $lang) {
                $file = N2MY_DOCUMENT_ROOT."/lang/".$lang["locale_id"]."/js/html.js";
                $word_list = $this->obj_word->getRowsAssoc("lang_cd = '".$lang_cd."'", null, null, null, "define_no,value", "define_no");
                $data = array();
                foreach ($define_list as $define_no => $define) {
                    $data[$define_no] = $word_list[$define_no]["value"] ? $word_list[$define_no]["value"] : $define["name"];
                }
                $message = 'var gLangList = '.json_encode($data);
                print $file."<br />";
                if(!file_put_contents($file, $message)) {
                    print '<font color="red">Output Error!!</font></br>';
                }
            }
            // オリジナル（用意されていない言語が指定された場合を想定しているがあまり意味が無いかも）
            foreach ($define_list as $define_no => $define) {
                $data[$define_no] = $define["name"];
            }
            $message = 'var gOriginalLangList = '.json_encode($data);
            $file = N2MY_DOCUMENT_ROOT."/shared/js/original.js";
            print $file."<br />";
            if(!file_put_contents($file, $message)) {
                print '<font color="red">Output Error!!</font></br>';
            }
        }
        /**
         * template html 言語ファイル
         */
        if (array_key_exists("template_html", $type)) {
            // 言語別のファイル
            $define_list = $this->obj_define->getRowsAssoc("type = 'template_html' AND flg = 2", null, null, null, "define_no,name", "define_no");
            foreach ($lang_list as $lang_cd => $lang) {
                $file = N2MY_APP_DIR."config/lang/".$lang["locale_id"]."/html.php";
                $word_list = $this->obj_word->getRowsAssoc("lang_cd = '".$lang_cd."'", null, null, null, "define_no,value", "define_no");
                $data = array();
                foreach ($define_list as $define_no => $define) {
                    $data[$define_no] = $word_list[$define_no]["value"] ? str_replace("'", '&#039;', $word_list[$define_no]["value"]) : str_replace("'", '&#039;', $define["name"]);
                }
                print $file."<br />";
                if(!file_put_contents($file, var_export($data, true))) {
                    print '<font color="red">Output Error!!</font></br>';
                }
            }
        }
        /**
         * message.ini
         */
        if (array_key_exists("n2my_web", $type)) {
            // 言語別のファイル
            $file = N2MY_APP_DIR."setup/message.ini";
            if (file_exists($file)) {
                $original_ini = parse_ini_file($file, true);
            }
            $define_list = $this->obj_define->getRowsAssoc("type = 'n2my_web' AND flg = 2", null, null, null, "define_no,define_id,name", "define_no");
            foreach ($lang_list as $lang_cd => $lang) {
                $word_list = $this->obj_word->getRowsAssoc("lang_cd = '".$lang_cd."'", null, null, null, "define_no,value", "define_no");
                $ini = array();
                foreach ($define_list as $define_no => $define) {
                    list($sections, $key) = split("/",$define["define_id"]);
                    if ($key == null) {
                        $ini[$sections] = $word_list[$define_no]["value"] ? $word_list[$define_no]["value"] : $define["name"];
                    } else {
                        $ini[$sections][$key] = $word_list[$define_no]["value"] ? $word_list[$define_no]["value"] : $define["name"];
                    }
                }
                require_once("lib/EZLib/EZUtil/EZArray.class.php");
                $objArray = new EZArray();
                $assoc_array = $objArray->mergeRecursiveReplace($original_ini, $ini);
                $content = '';
                $sections = '';
                foreach ($assoc_array as $key => $item) {
                    if (is_array($item)) {
                        $sections .= "\n[{$key}]\n";
                        foreach ($item as $key2 => $item2) {
                            if (is_numeric($item2) || is_bool($item2))
                                $sections .= "{$key2} = {$item2}\n";
                            else
                                $sections .= "{$key2} = \"{$item2}\"\n";
                        }
                    } else {
                        if(is_numeric($item) || is_bool($item))
                            $content .= "{$key} = {$item}\n";
                        else
                            $content .= "{$key} = \"{$item}\"\n";
                    }
                }
                $content .= $sections;
                $file = N2MY_APP_DIR."config/lang/".$lang["locale_id"]."/message.ini";
                print $file."<br />";
                if(!file_put_contents($file, $content)) {
                    print '<font color="red">Output Error!!</font></br>';
                }
            }
        }
        /**
         * FAQ
         */
        $faq_list = new N2MY_DB(N2MY_MDB_DSN, "faq_list");
        $faq_data = new N2MY_DB(N2MY_MDB_DSN, "faq_data");
        if (array_key_exists("faq_answer", $type)) {
            // faq_answer
            $define_list = $this->obj_define->getRowsAssoc("type = 'faq_answer' AND flg = 2", null, null, null, "define_no,define_id,name", "define_no");
            foreach ($lang_list as $lang_cd => $lang) {
                $word_list = $this->obj_word->getRowsAssoc("lang_cd = '".$lang_cd."'", null, null, null, "define_no,value", "define_no");
                foreach ($define_list as $define_no => $define) {
                    $where = "faq_key = '".$define["define_id"]."'" .
                            " AND lang = '".$lang["locale_id"]."'";
                    $data = array(
                        "faq_key" => $define["define_id"],
                        "lang" => $lang["locale_id"],
                        "answer" => $word_list[$define_no]['value'],
                        "create_datetime" => $this->now,
                        "update_datetime" => $this->now,
                    );
                    if ($faq_data->numRows($where)) {
                        //$faq_data->update($data, $where);
                    } else {
                        //$faq_data->add($data);
                    }
                }
            }
            // csvに吐き出す
            $this->action_faq_answer_out_csv();
        }
        if (array_key_exists("faq_question", $type)) {
            // faq_question
            $define_list = $this->obj_define->getRowsAssoc("type = 'faq_question' AND flg = 2", null, null, null, "define_no,define_id,name", "define_no");
            foreach ($lang_list as $lang_cd => $lang) {
                $word_list = $this->obj_word->getRowsAssoc("lang_cd = '".$lang_cd."'", null, null, null, "define_no,value", "define_no");
                foreach ($define_list as $define_no => $define) {
                    $where = "faq_key = '".$define["define_id"]."'" .
                            " AND lang = '".$lang["locale_id"]."'";
                    $data = array(
                        "faq_key" => $define["define_id"],
                        "lang" => $lang["locale_id"],
                        "question" => $word_list[$define_no]['value'],
                        "create_datetime" => $this->now,
                        "update_datetime" => $this->now,
                    );
                    if ($faq_data->numRows($where)) {
                        //$faq_data->update($data, $where);
                    } else {
                        //$faq_data->add($data);
                    }
                }
            }
            // csvに吐き出す
            $this->action_faq_question_out_csv();
        }
        /**
         * xml
         */
        if (array_key_exists("xml", $type)) {
            // faq_question
            $define_list = $this->obj_define->getRowsAssoc("type = 'xml' AND flg = 2", null, null, null, "define_no,define_id,name", "define_no");
            foreach ($lang_list as $lang_cd => $lang) {
                $file = N2MY_APP_DIR."config/lang/".$lang["locale_id"]."/xml.php";
                $word_list = $this->obj_word->getRowsAssoc("lang_cd = '".$lang_cd."'", null, null, null, "define_no,value", "define_no");
                $data = array();
                foreach ($define_list as $define_no => $define) {
                    $data[$define_no] = $word_list[$define_no]["value"] ? $word_list[$define_no]["value"] : $define["name"];
                }
                print $file."<br />";
                if (!file_put_contents($file, var_export($data, true))) {
                    print '<font color="red">Output Error!!</font></br>';
                }
            }
        }
        /**
         * appli
         */
        if (array_key_exists("appli", $type)) {
            // 言語別のファイル
            $define_list = $this->obj_define->getRowsAssoc("type = 'appli' AND (path = 'checker' OR path = 'easy_checker') AND flg = 2", array("define_id" => "asc"), null, null, "define_no,define_id,name", "define_no");
            foreach ($lang_list as $lang_cd => $lang) {
                $file = N2MY_DOCUMENT_ROOT."/lang/".$lang["locale_id"]."/flash/tools/easy_checker.xml";
                $word_list = $this->obj_word->getRowsAssoc("lang_cd = '".$lang_cd."'", null, null, null, "define_no,value", "define_no");
                $contents = '<language>'."\n";
                foreach ($define_list as $define_no => $define) {
                    $value = $word_list[$define_no]["value"] ? $word_list[$define_no]["value"] : $define["name"];
                    $contents .= '    <string key="'.$define["define_id"].'" value="'.htmlspecialchars($value).'" />'."\n";
                }
                $contents .= '</language>';
                print $file."<br />";
                if(!file_put_contents($file, $contents)) {
                    print '<font color="red">Output Error!!</font></br>';
                }
            }
        }
        print "適用しました。";
    }

    /**
     * FAQ質問CSVファイルの作成
    */
    function action_faq_question_out_csv(){
        $sort = array("define_id" => "asc");
        // define_idをキーにして取得
        $define_list = $this->obj_define->getRowsAssoc("type LIKE 'faq_question' AND flg = 2", $sort, null, null, null, "define_id");
        // DB定義でdefine_idは文字列order_byでソート出来ないためPHP側でソート(faqの場合define_idは必ず数字)
        ksort($define_list);
        $faq_list = array();
        $lang_list   = $this->obj_lang->getRowsAssoc("flg = 1 AND support = 1", null , null, null, null, "lang_cd");
        require_once("lib/EZLib/EZUtil/EZCsv.class.php");
        // csv作成
        $csv = new EZCsv(true, "utf-8", "utf-8", "\t");
        foreach ($lang_list as $lang_cd => $lang) {
            $word_list = $this->obj_word->getRowsAssoc("lang_cd = '".$lang_cd."'", null, null, null, "define_no,value", "define_no");
            $file =  N2MY_APP_DIR."config/lang/".$lang["locale_id"]."/faq_question.csv";
            // 元ファイル削除(EZCsvクラスの仕様上、上書きが出来ないため)
            unlink($file);
            $csv->open($file, "w");
            $header = array(
                    "faq_key" => "faq_key",
                    "faq_category" => "faq_category",
                    "faq_subcategory" => "faq_subcategory",
                    "faq_question" => "faq_question",
            );
            $csv->setHeader($header);
            $csv->write($header);
            foreach($define_list as $define){
                if(isset($word_list[$define['define_no']['value']])){
                    $category = split("/",$define['path']);
                    $data = array(
                            "faq_key" => $define["define_id"],
                            "faq_category" => $category[0],
                            "faq_subcategory" => $category[1],
                            "faq_question" =>  $word_list[$define['define_no']]['value'],
                    );
                    $csv->write($data);
                }
            }

            $csv->close();
            chmod($file,0777);
        }
    }

    /**
     * FAQ回答CSVファイルの作成
     */
    function action_faq_answer_out_csv(){
        $sort = array("define_id" => "asc");
        $define_list = $this->obj_define->getRowsAssoc("type LIKE 'faq_answer' AND flg = 2", $sort, null, null, null, "define_id");
        ksort($define_list);
        $faq_list = array();
        $lang_list   = $this->obj_lang->getRowsAssoc("flg = 1 AND support = 1", null , null, null, null, "lang_cd");
        require_once("lib/EZLib/EZUtil/EZCsv.class.php");
        $csv = new EZCsv(true, "utf-8", "utf-8", "\t");
         foreach ($lang_list as $lang_cd => $lang) {
            $word_list = $this->obj_word->getRowsAssoc("lang_cd = '".$lang_cd."'", null, null, null, "define_no,value", "define_no");
            $file =  N2MY_APP_DIR."config/lang/".$lang["locale_id"]."/faq_answer.csv";
            // 元ファイル削除(クラスの仕様上上書きが出来ないため)
            unlink($file);
            $csv->open($file, "w");
            $header = array(
                    "faq_key" => "faq_key",
                    "faq_category" => "faq_category",
                    "faq_subcategory" => "faq_subcategory",
                    "faq_answer" => "faq_answer",
            );
            $csv->setHeader($header);
            $csv->write($header);
            foreach($define_list as $define){
                if(isset($word_list[$define['define_no']['value']])){
                    $category = split("/",$define['path']);
                    $data = array(
                            "faq_key" => $define["define_id"],
                            "faq_category" => $category[0],
                            "faq_subcategory" => $category[1],
                            "faq_answer" =>  $word_list[$define['define_no']]['value'],
                    );
                    $csv->write($data);
                }
            }

            $csv->close();
            chmod($file,0777);
        }
    }

    function action_get_flash_csv() {
        $this->html_header();
        require_once("lib/EZLib/EZUtil/EZCsv.class.php");
        print "<pre>";
        $file = N2MY_DOCUMENT_ROOT."multilang/presenceappli.csv";
        $csv = new EZCsv(true, "UTF-8", "UTF-8");
        $csv->open($file, "r");
        while($row = $csv->getNext(true)) {
            print_r($row);
            $data = array(
                "type" => $row["type"],
                "path" => $row["path"],
                "define_id" => $row["define_id"],
                "name" => $row["ja"],
                "flg" => 2,
                "create_datetime" => $this->now,
                "update_datetime" => $this->now
                );
            $define_no = $this->obj_define->add($data);
            $data = array(
                "define_no" => $define_no,
                "lang_cd" => "ja",
                "value" => $row["ja"],
                "flg" => 2,
                "create_datetime" => $this->now,
                "update_datetime" => $this->now
                );
            $word_no = $this->obj_word->add($data);
            $data = array(
                "define_no" => $define_no,
                "lang_cd" => "en",
                "value" => $row["en"],
                "flg" => 2,
                "create_datetime" => $this->now,
                "update_datetime" => $this->now
                );
            $word_no = $this->obj_word->add($data);
        }
    }

    /**
     * Flash config
     */
    function action_get_flash_config() {
        $this->html_header();
        require_once("lib/EZLib/EZUtil/EZCsv.class.php");
        $this->obj_define->remove("type = 'flash_config'");
        print "<pre>";
        $file = N2MY_DOCUMENT_ROOT."multilang/flash_config.csv";
        $csv = new EZCsv(true, "UTF-8", "UTF-8");
        $csv->open($file, "r");
        $type = "flash_config";
        while($row = $csv->getNext(true)) {
            print_r($row);
            $data = array(
                "type" => $type,
                "path" => "core/api/meeting_config/meeting/resources/languages.t.xml",
                "define_id" => $row["id"],
                "name" => $row["ja"],
                "flg" => 2,
                "create_datetime" => $this->now,
                "update_datetime" => $this->now
                );
            $define_no = $this->obj_define->add($data);
            $data = array(
                "define_no" => $define_no,
                "lang_cd" => "ja",
                "value" => $row["ja"],
                "flg" => 2,
                "create_datetime" => $this->now,
                "update_datetime" => $this->now
                );
            $word_no = $this->obj_word->add($data);
            $data = array(
                "define_no" => $define_no,
                "lang_cd" => "en",
                "value" => $row["en"],
                "flg" => 2,
                "create_datetime" => $this->now,
                "update_datetime" => $this->now
                );
            $word_no = $this->obj_word->add($data);
        }
    }

    function action_get_n2my_config() {
//        $this->obj_define->remove("type = 'n2my_web'");
        print "<xmp>";
        $file = N2MY_APP_DIR."/config/ja/message.ini";
        $ja_list = parse_ini_file($file, true);
//        print_r($ja_list);
        $file = N2MY_APP_DIR."/config/en/message.ini";
        $en_list = parse_ini_file($file, true);
        //print_r($en_list);
        foreach($ja_list as $group_id => $group) {
            if (is_array($group)) {
                foreach($group as $key => $val) {
                    $where = "type = 'n2my_web'" .
                            " AND path = 'message.ini'" .
                            " AND define_id = '".substr($group_id."/".$key, 0, 30)."'";
                    $row = $this->obj_define->getRow($where);
                    if ($row) {
                        $where = "define_no = ".$row["define_no"];
                        $data = array(
                            "define_id" => $group_id."/".$key,
//                            "update_datetime" => $this->now
                            );
//                        $this->obj_define->update($data, $where);
                    } else {
                        $data = array(
                            "type" => "n2my_web",
                            "path" => "message.ini",
                            "define_id" => $group_id."/".$key,
                            "name" => $val,
                            "flg" => 2,
                            "create_datetime" => $this->now,
                            "update_datetime" => $this->now
                            );
                        $define_no = $this->obj_define->add($data);
                        $data = array(
                            "define_no" => $define_no,
                            "lang_cd" => "ja",
                            "value" => $val,
                            "flg" => 2,
                            "create_datetime" => $this->now,
                            "update_datetime" => $this->now
                            );
                        $word_no = $this->obj_word->add($data);
                        if (isset($en_list[$group_id][$key])) {
                            $data = array(
                                "define_no" => $define_no,
                                "lang_cd" => "en",
                                "value" => $en_list[$group_id][$key],
                                "flg" => 2,
                                "create_datetime" => $this->now,
                                "update_datetime" => $this->now
                                );
                            $word_no = $this->obj_word->add($data);
                        }
                    }
                    /*
                    $data = array(
                        "define_no" => $define_no,
                        "lang_cd" => "ja",
                        "value" => $val,
                        "flg" => 2,
                        "create_datetime" => $this->now,
                        "update_datetime" => $this->now
                        );
                    $word_no = $this->obj_word->add($data);
                    if (isset($en_list[$group_id][$key])) {
                        $data = array(
                            "define_no" => $define_no,
                            "lang_cd" => "en",
                            "value" => $en_list[$group_id][$key],
                            "flg" => 2,
                            "create_datetime" => $this->now,
                            "update_datetime" => $this->now
                            );
                        $word_no = $this->obj_word->add($data);
                    }
                    */
                }
            } else {
                /*
                $data = array(
                    "type" => "n2my_web",
                    "path" => "message.ini",
                    "define_id" => $group_id,
                    "name" => $group,
                    "flg" => 2,
                    "create_datetime" => $this->now,
                    "update_datetime" => $this->now
                    );
                $define_no = $this->obj_define->add($data);
                $data = array(
                    "define_no" => $define_no,
                    "lang_cd" => "ja",
                    "value" => $group,
                    "flg" => 2,
                    "create_datetime" => $this->now,
                    "update_datetime" => $this->now
                    );
                $word_no = $this->obj_word->add($data);
                if (isset($en_list[$group_id])) {
                    $data = array(
                        "define_no" => $define_no,
                        "lang_cd" => "en",
                        "value" => $en_list[$group_id],
                        "flg" => 2,
                        "create_datetime" => $this->now,
                        "update_datetime" => $this->now
                        );
                    $word_no = $this->obj_word->add($data);
                }
                */
            }
        }
    }

    function action_define_template() {
        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="define.csv"');
        readfile(N2MY_APP_DIR."setup/multilang/define.csv");
    }

    function action_add_define_form() {
        $this->html_header();
        print <<<EOM
<a href="?action_define_template=">CSVテンプレート</a>
<form action="./" method="post" enctype="multipart/form-data">
CSVファイル：<input type="file" name="file" />
<input type="submit" name="action_add_define" value="インポート確認" />
</form>
EOM;
    }

    function action_add_define() {
        $this->html_header();
        require_once("lib/EZLib/EZUtil/EZCsv.class.php");
        $is_commit = $this->request->get("commit");
        if ($is_commit == '1') {
            $filename = N2MY_APP_DIR.'var/multilang/'.$this->request->get("path");
            if (!file_exists($filename)) {
                die("CSVファイルをアップロードしてください");
            }
        } else {
            // ファイルアップロード
            $file = $_FILES["file"];
            $data = pathinfo($file["name"]);
            if ($data['extension'] != "csv") {
                die("CSVファイルをアップロードしてください");
            }
            if (!is_dir(N2MY_APP_DIR.'var/multilang/')) {
                mkdir(N2MY_APP_DIR.'var/multilang/', 0777, true);
            }
            $filename = N2MY_APP_DIR.'var/multilang/'.basename($file["tmp_name"]);
            move_uploaded_file($file["tmp_name"], $filename);
        }
        $whrer = "flg = 1";
        $lang_list = $this->obj_lang->getRowsAssoc($whrer, null , null, null, null, "lang_cd");
        $msg_format = $this->get_message("error");
        $msg = $this->get_message("ERROR");
        $csv = new EZCsv(true, "UTF-8", "UTF-8", ",");
        $csv->open($filename, "r");
        $header = $csv->getHeader();
        $list = array();
        $rows = array();
        $err_flg = 0;
        print '<table class="data">';
        print "<tr>";
        print "<th>NO</th>";
        print "<th>TYPE</th>";
        print "<th>PATH</th>";
        print "<th>ID</th>";
        print "<th>オリジナル</th>";
        foreach($header as $_key => $val) {
            if (array_key_exists($val, $lang_list)) {
                print '<th>'.$lang_list[$val]["lang_name"].'</th>';
            }
        }
        print "</tr>";
        $ok = 0;
        while($row = $csv->getNext()) {
            $bgcolor = "";
            $define_no = "";
            if (!$row["type"] || !$row["path"]) {
                $bgcolor = "gray";
            } else {
                $where = "type = '".addslashes($row["type"])."'".
                    " AND path = '".addslashes($row["path"])."'" .
                    " AND define_id = '".addslashes($row["define_id"])."'" .
                    " AND name = '".addslashes($row["original"])."'";
                if ($this->obj_define->numRows($where) > 0) {
                    $bgcolor = "pink";
                } elseif ($is_commit == "1") {
                    $data = array(
                        "type"              => $row["type"],
                        "path"              => $row["path"],
                        "define_id"         => $row["define_id"],
                        "name"              => $row["original"],
                        "flg"               => 2,
                        "create_datetime"   => $this->now,
                        "update_datetime"   => $this->now
                        );
                    $define_no = $this->obj_define->add($data);
                    if ($define_no) {
                        foreach($lang_list as $lang_cd => $lang) {
                            if ($row[$lang_cd]) {
                                $data = array(
                                    "define_no"         => $define_no,
                                    "lang_cd"           => $lang_cd,
                                    "value"             => $row[$lang_cd],
                                    "flg"               => 2,
                                    "create_datetime"   => $this->now,
                                    "update_datetime"   => $this->now
                                    );
                                $word_no = $this->obj_word->add($data);
                            }
                        }
                    }
                    $ok++;
                }
                // */
            }
            print '<tr bgcolor="'.$bgcolor.'">';
            print "<td>".htmlspecialchars($define_no)."&nbsp;</td>";
            print "<td>".htmlspecialchars($row["type"])."&nbsp;</td>";
            print "<td>".htmlspecialchars($row["path"])."&nbsp;</td>";
            print "<td>".htmlspecialchars($row["define_id"])."&nbsp;</td>";
            print "<td>".htmlspecialchars($row["original"])."&nbsp;</td>";
            foreach($header as $_key => $val) {
                if (array_key_exists($val, $lang_list)) {
                    print "<td>".$row[$val]."</td>";
                }
            }
            print "</tr>";
        }
        print "<table>";
        print "<br />";
        if (!$is_commit) {
            print '<form action="./" method="post">';
            print '<input type="hidden" name="commit" value="1" />';
            print '<input type="hidden" name="path" value="'.basename($filename).'" />';
            print '<input type="submit" name="action_add_define" value="インポート" />';
            print '</form>';
        } else {
            unlink($filename);
            print "$ok 件、登録完了";
        }
    }

    function action_get_faq() {
        $where = "type = 'faq_answer'";
        $answer_list = $this->obj_define->getRowsAssoc($where);
        print_r($answer_list);
        foreach($answer_list as $answer) {
            // 日本語
            $data = array(
                "define_no"         => $answer["define_no"],
                "lang_cd"           => "ja",
                "value"             => $answer["name"],
                "flg"               => 2,
                "create_datetime"   => $this->now,
                "update_datetime"   => $this->now
                );
            $word_no = $this->obj_word->add($data);
        }

        $where = "type = 'faq_question'";
        $answer_list = $this->obj_define->getRowsAssoc($where);
        print_r($answer_list);
        foreach($answer_list as $answer) {
            // 日本語
            $data = array(
                "define_no"         => $answer["define_no"],
                "lang_cd"           => "ja",
                "value"             => $answer["name"],
                "flg"               => 2,
                "create_datetime"   => $this->now,
                "update_datetime"   => $this->now
                );
            $word_no = $this->obj_word->add($data);
        }
/*
        $faq = new N2MY_DB(N2MY_MDB_DSN, "faq_list");
        $where = "faq_status = 1" .
                " AND faq_lang = 0";
        $list = $faq->getRowsAssoc($where);
        foreach ($list as $row) {
            // FAQ質問
            $data = array(
                "type" => "faq_answer",
                "path" => $row["faq_category"]."/".$row["faq_subcategory"],
                "define_id" => $row["faq_key"],
                "name" => $row['faq_answer'],
                "create_datetime" => $this->now,
                "update_datetime" => $this->now,
                );
            $define_no = $this->obj_define->add($data);
            if (DB::isError($define_no)) {
                $this->logger2->info($define_no->getUserInfo());
            }
            // FAQ解答
            $data = array(
                "type" => "faq_question",
                "path" => $row["faq_category"]."/".$row["faq_subcategory"],
                "define_id" => $row["faq_key"],
                "name" => $row['faq_question'],
                "create_datetime" => $this->now,
                "update_datetime" => $this->now,
                );
            $define_no = $this->obj_define->add($data);
            if (DB::isError($define_no)) {
                $this->logger2->info($define_no->getUserInfo());
            }
        }
        return true;
        */
    }

    /**
     * インポートフォーム
     */
    function action_import_form() {
        $this->html_header();
        print <<<EOM
<form action="./" method="post" enctype="multipart/form-data">
CSVファイル：<input type="file" name="file" />
<input type="submit" name="action_import" value="インポート" />
</form>
EOM;
    }

    /**
     * インポート
     */
    function action_import() {
        $this->html_header();
        require_once("lib/EZLib/EZUtil/EZCsv.class.php");
        // ファイルアップロード
        $file = $_FILES["file"];
        $data = pathinfo($file["name"]);
        if ($data['extension'] != "csv") {
            die("CSVファイルをアップロードしてください");
        }
        $whrer = "flg = 1";
        $lang_list = $this->obj_lang->getRowsAssoc($whrer, null , null, null, null, "lang_cd");

        $filename = $file["tmp_name"];
        $msg_format = $this->get_message("error");
        $msg = $this->get_message("ERROR");
        $csv = new EZCsv(true, "UTF-8", "UTF-8", "\t", true);
        $csv->open($filename, "r");
        $header = $csv->getHeader();
        $list = array();
        $rows = array();
        $err_flg = 0;
        print '<table class="data">';
        print "<tr>";
        print "<th>NO</th>";
        print "<th>PATH</th>";
        print "<th>オリジナル</th>";
        foreach($header as $_key => $val) {
            if (array_key_exists($val, $lang_list)) {
                print '<th>'.$lang_list[$val]["lang_name"].'</th>';
            }
        }
//        print "<th>備考</th>";
        print "</tr>";
        while($row = $csv->getNext(true)) {
            print "<tr>";
            print "<td>".htmlspecialchars($row["define_no"])."&nbsp;</td>";
            print "<td>".htmlspecialchars($row["path"])."&nbsp;</td>";
            $original = $this->_get_message("ja", $row["define_no"]);
            print "<td>".htmlspecialchars($original)."&nbsp;</td>";
            foreach ($row as $_key => $val) {
                if (array_key_exists($_key, $lang_list)) {
                    $before = $this->_get_message($_key, $row["define_no"]);
                    if ($before != $row[$_key]) {
                        print '<td bgcolor="#ffaabb">';
                        $this->_set_message($_key, $row["define_no"], $row[$_key]);
                    } else {
                        print '<td>';
                    }
                    print htmlspecialchars($row[$_key]).'</td>';
                }
            }
//            print "<td>".htmlspecialchars($row["info"])."&nbsp;</td>";
            print "</tr>";
        }
        print "<table>";
    }

    /**
     *
     */
    function _get_message($lang, $define_no) {
        $where = "define_no = '".$define_no."'".
            " AND lang_cd = '".$lang."'";
        return $this->obj_word->getOne($where, "value");
    }

    /**
     * 作成時間取得
     * @param $lang
     * @param $define_no
     */
    function _get_create_datetime($lang, $define_no){
        $where = "define_no = '".$define_no."'".
                " AND lang_cd = '".$lang."'";
        return $this->obj_word->getOne($where, "create_datetime");
    }

    /**
     * 更新時間取得
     * @param $lang
     * @param $define_no
     */
    function _get_update_datetime($lang, $define_no){
        $where = "define_no = '".$define_no."'".
                " AND lang_cd = '".$lang."'";
        return $this->obj_word->getOne($where, "update_datetime");
    }

    /**
     *
     */
    function _set_message($lang, $define_no, $message, $flg = null) {
        $where = "lang_cd = '".$lang."'";
        if (is_array($define_no)) {
            $where .= " AND define_no IN (".join(",", $define_no).")";
        } else {
            $where .= " AND define_no = '".trim($define_no)."'";
        }
        $data["value"] = $message;
        if ($flg) {
            $data["flg"] = $flg;
        }
        $count = $this->obj_word->numRows($where);
        if ($count == 1) {
            $ret = $this->obj_word->update($data, $where);
        } else {
            $data["lang_cd"] = $lang;
            $data["define_no"] = $define_no;
            $data["create_datetime"] = $this->now;
            $data["update_datetime"] = $this->now;
            $ret = $this->obj_word->add($data);
        }
        if (DB::isError($ret)) {
            die($ret->getUserInfo());
        }
    }

    /**
     * 翻訳作業
     */
    function action_translator() {
        $this->html_header();
        $whrer = "flg = 1";
        $lang_list = $this->obj_lang->getRowsAssoc($whrer, null , null, null, null, "lang_cd");
        $set_lang = $this->request->get("set_lang");
        $lang_from = $set_lang["from"] ? $set_lang["from"] : ($this->session->get("from", __FUNCTION__) ? $this->session->get("from", __FUNCTION__) : "ja");
        $this->session->set("from", $lang_from, __FUNCTION__);
        $lang_to = $set_lang["to"] ? $set_lang["to"] : ($this->session->get("to", __FUNCTION__) ? $this->session->get("to", __FUNCTION__) : "en");
        $this->session->set("to", $lang_to, __FUNCTION__);
        $type_row = $this->obj_define->_conn->getCol("SELECT type FROM define GROUP BY type");
        $search_form = $this->session->get("search_form", __FUNCTION__);
        if ($search_form) {
            $where = "";
            $condition = array();
            if ($search_form["define_no"]) {
                $condition[] = $this->obj_define->table.".define_no = '".addslashes($search_form["define_no"])."'";
            }
            if ($search_form["type"]) {
                $condition[] = $this->obj_define->table.".type = '".addslashes($search_form["type"])."'";
            }
            if ($search_form["path"]) {
                $condition[] = $this->obj_define->table.".path like '%".addslashes($search_form["path"])."%'";
            }
            if ($search_form["define_id"]) {
                $condition[] = $this->obj_define->table.".define_id like '%".addslashes($search_form["define_id"])."%'";
            }
            if ($search_form["name"]) {
                $condition[] = $this->obj_define->table.".name like '%".addslashes($search_form["name"])."%'";
            }
            if ($search_form["lang_from"]) {
                $condition[] = "word_from.value like '%".addslashes($search_form["lang_from"])."%'";
            }
            if ($search_form["lang_to"]) {
                $condition[] = "word_to.value like '%".addslashes($search_form["lang_to"])."%'";
            }
            $condition[] = $this->obj_define->table.".flg = 2";
            if ($condition) {
                $where = implode(" AND ", $condition);
            }
        }
        $limit = 20;
        $page = $this->request->get("page", 1);
        $offset = $limit * ($page - 1);
        // 件数取得
        $count_sql = "SELECT count(define.define_no)" .
            " FROM define" .
            " LEFT JOIN word AS word_from ON define.define_no = word_from.define_no AND word_from.lang_cd = '".$lang_from."'" .
            " LEFT JOIN word AS word_to ON define.define_no = word_to.define_no AND word_to.lang_cd = '".$lang_to."'";
        if ($where) {
            $count_sql .= " WHERE " .$where;
        }
        $total_count = $this->obj_define->_conn->getOne($count_sql);
        if (DB::isError($total_count)) {
            $this->logger2->info($total_count->getUserInfo());
            $total_count = 0;
        }
        // ソート
        $sql = "SELECT define.*" .
            ", word_from.value AS value_from" .
            ", word_to.value AS value_to" .
            " FROM define" .
            " LEFT JOIN word AS word_from ON define.define_no = word_from.define_no AND word_from.lang_cd = '".$lang_from."'" .
            " LEFT JOIN word AS word_to ON define.define_no = word_to.define_no AND word_to.lang_cd = '".$lang_to."'";
        if ($where) {
            $sql .= " WHERE " .$where;
        }
        if (!$sort = $this->session->get("sort", __FUNCTION__)) {
            $sort = array("define.type" => "asc", "define.path" => "asc");
        }
        $sql .= " ORDER BY ";
        $i = 0;
        foreach($sort as $field => $type) {
            if ($i > 0) {
                $sql .= ",";
            }
            // ランダム
            if ($field == "RAND()") {
                $sql .= " RAND()";
            } else {
                if (strtolower($type) == "desc") {
                    $sql .= $field." DESC";
                } else {
                    $sql .= $field." ASC";
                }
            }
            $i++;
        }
        $define_list = array();
        $define_rs = $this->obj_define->_conn->limitQuery($sql, $offset, $limit);
        if (DB::isError($define_rs)) {
            $this->logger2->info($define_rs->getUserInfo());
        } else {
            while ($define_row = $define_rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                $define_list[] = $define_row;
            }
        }
        $pager = $this->setPager($limit, $page, $total_count);
        print '<form action="./" method="post">';
        print '<select name="set_lang[from]">';
        foreach ($lang_list as $lang_row) {
            if ($lang_from == $lang_row["lang_cd"]) {
                print '<option value="'.$lang_row["lang_cd"].'" selected="selected">'.$lang_row["lang_name"].'</option>';
                $lang_from_name = $lang_row["lang_name"];
            } else {
                print '<option value="'.$lang_row["lang_cd"].'">'.$lang_row["lang_name"].'</option>';
            }
        }
        print '</select>';
        print ' -&gt; ';
        print '<select name="set_lang[to]">';
        foreach ($lang_list as $lang_row) {
            if ($lang_to == $lang_row["lang_cd"]) {
                print '<option value="'.$lang_row["lang_cd"].'" selected="selected">'.$lang_row["lang_name"].'</option>';
                $lang_to_name = $lang_row["lang_name"];
            } else {
                print '<option value="'.$lang_row["lang_cd"].'">'.$lang_row["lang_name"].'</option>';
            }
        }
        print '</select> ';
        print '<input type="submit" name="action_translator" value="翻訳"/> ';
        //print '<input type="submit" name="action_auto_translator" value="自動翻訳"/>';
        print '</form>';
        $this->_pager($pager, "action_translator");
        print '<table class="data">';
        print '<tr>';
        print '<th rowspan="2"><input type="checkbox" id="checkall"></th>';
        print '<th><a href="?action_sort=define_no&act='.htmlspecialchars(__FUNCTION__).'">NO</a></th>';
        print '<th><a href="?action_sort=type&act='.htmlspecialchars(__FUNCTION__).'">タイプ</a></th>';
        print '<th><a href="?action_sort=path&act='.htmlspecialchars(__FUNCTION__).'">パス</a></th>';
        print '<th><a href="?action_sort=define_id&act='.htmlspecialchars(__FUNCTION__).'">ID</a></th>';
        print '<th><a href="?action_sort=name&act='.htmlspecialchars(__FUNCTION__).'">オリジナル</a></th>';
        print '<th><a href="?action_sort=value_from&act='.htmlspecialchars(__FUNCTION__).'">'.$lang_from_name.'</th>';
        print '<th><a href="?action_sort=value_to&act='.htmlspecialchars(__FUNCTION__).'">'.$lang_to_name.'</th>';
        print '<th>アクション</th>';
        print '</tr>';
        print '<form action="'.$_SERVER["SCRIPT_NAME"].'" method="post">';
        print '<input type="hidden" name="act" value="'.htmlspecialchars(__FUNCTION__).'">';
        print '<tr>';
        print '<th><input type="text" name="define_no" value="'.htmlspecialchars($search_form["define_no"]).'" size="5" /></th>';
        print '<th>';
        print '<select name="type">';
        print '<option value="">all</option>';
        foreach($type_row as $type) {
            print '<option value="'.$type.'"';
            if ($search_form["type"] == $type) {
                print ' selected="selected"';
            }
            print '>'.$type.'</option>';
        }
        print '</select>';
        print '</th>';
        print '<th><input type="text" name="path" value="'.htmlspecialchars($search_form["path"]).'" size="5" /></th>';
        print '<th><input type="text" name="define_id" value="'.htmlspecialchars($search_form["define_id"]).'" size="3" /></th>';
        print '<th><input type="text" name="name" value="'.htmlspecialchars($search_form["name"]).'" size="20" /></th>';
        print '<th><input type="text" name="lang_from" value="'.htmlspecialchars($search_form["lang_from".$lang_from]).'" size="20" /></th>';
        print '<th><input type="text" name="lang_to" value="'.htmlspecialchars($search_form["lang_to"]).'" size="20" /></th>';
        print '<th><input type="submit" name="action_search" value="検索" /></th>';
        print '</tr>';
        print '</form>';
        print '<form action="'.$_SERVER["SCRIPT_NAME"].'" id="form_list" method="post">';
        print '<input type="hidden" name="_action" value="">';
        print '<input type="hidden" name="flg" value="">';
        foreach ($define_list as $row) {
            print '<tr>';
            print '<td align="center"><input type="checkbox" class="row_select" name="check_flg['.$row["define_no"].']" value="'.$row["define_no"].'" /></td>';
            print '<td>'.$row["define_no"].'</td>';
            print '<td>'.$row["type"].'</td>';
            if ($row["type"] == "template_html" || $row["type"] == "xml") {
                print '<td><a target="template" href="?action_get_template=&define_no='.$row["define_no"].'">'.$row["path"].'</a></td>';
            } elseif ($row["type"] == "flash_config") {
                print '<td><a target="template" href="?action_get_template=&define_no='.$row["define_no"].'&lang=common">'.$row["path"].'</a></td>';
            } else {
                print '<td>'.$row["path"].'</td>';
            }
            print '<td width="35">'.htmlspecialchars($row["define_id"]).'</td>';
            print '<td width="35">'.htmlspecialchars($row["name"]).'</td>';
            print '<td width="50%" height="100%"><textarea id="from_'.$row["define_no"].'" readonly="readonly" class="from_text">'.htmlspecialchars($row["value_from"]).'</textarea></td>';
            print '<td width="50%" height="100%"><textarea id="to_'.$row["define_no"].'_'.$lang_to.'" class="to_text" onchange="change_text('.$row["define_no"].', \''.$lang_to.'\');" ondblclick="translate('.$row["define_no"].', \''.$lang_from.'\', \''.$lang_to.'\');">'.htmlspecialchars($row["value_to"]).'</textarea></td>';
            print '<td>';
            print '  <input type="button" name="action_edit" value="編集" onclick="edit_lang('.$row["define_no"].', \''.$lang_to.'\');">';
            print '</td>';
            print '</tr>';
        }
        print '</table>';
        $this->_pager($pager, "action_translator");
        print '<br />';
        foreach($this->flg_row as $key => $val) {
            print '<input type="button" value="'.$val.'" onclick="check_edit(this.form, '.$key.');"/>';
        }
        print '</form>';
    }

    function action_edit_translator() {
        $request = $this->request->getAll();
        $define_log = new N2MY_DB($this->lang_dsn, "define_log");
        $where = "define_no = ".$request["id"].
            " AND lang_cd = '".$request["lang"]."'";
        $no = $this->obj_word->getOne($where, "word_no");
        $data = array(
            "define_no"       => $request["id"],
            "lang_cd"         => $request["lang"],
            "value"           => $request["word"],
            "flg"             => 0,
            "create_datetime" => $this->now,
            "update_datetime" => $this->now,
            );
        // replace
        if ($no) {
            // 更新時は作成時間は更新しない
            unset($data["create_datetime"]);
            $where = "word_no = ".$no;
            $this->obj_word->update($data, $where);
        } else {
            $no = $this->obj_word->add($data);
        }
        $data = array(
            "word_no"         => $no,
            "define_no"       => $request["id"],
            "lang_cd"         => $request["lang"],
            "value"           => $request["word"],
            "flg"             => 0,
            "create_datetime" => $this->now,
            "update_datetime" => $this->now,
            );
        $word_log = new N2MY_DB($this->lang_dsn, "word_log");
        $ret = $word_log->add($data);
        if (DB::isError($ret)) {
            print $ret->getUserInfo();
        }
        print "ok";
    }

    /**
     * 機械翻訳
     */
    function action_auto_translator() {
        $this->html_header();
        print "<pre>";
        $set_lang = $this->request->get("set_lang");
        $from = $set_lang["from"];
        $to = $set_lang["to"];
        // 上書きする
        $override = 0;
        $define_list = $this->obj_define->getRowsAssoc("flg = 2", null, null, null, "define_no,name", "define_no");
        $from_list   = $this->obj_word->getRowsAssoc("lang_cd = '".$from."'", null, null, null, "define_no,value", "define_no");
        $to_list   = $this->obj_word->getRowsAssoc("lang_cd = '".$to."'", null, null, null, "define_no,value", "define_no");
        foreach ($define_list as $define_no => $define) {
            if ($from_list[$define_no]) {
                if ($from_list[$define_no]["value"]) {
                    if (!$to_list[$define_no]["value"] || $override) {
                        // 翻訳成功時は上書き
                        if ($text = $this->google_translate($define["name"], $from, $to)) {
                            $where = "define_no = ".$define_no.
                                " AND lang_cd = '".$to."'";
                            $no = $this->obj_word->getOne($where, "word_no");
                            $data = array(
                                "define_no"       => $define_no,
                                "lang_cd"         => $to,
                                "value"           => $text,
                                "flg"             => 0,
                                "create_datetime" => $this->now,
                                "update_datetime" => $this->now,
                                );
                            $this->logger->info($data);
                            // replace
                            if ($no) {
                                $where = "word_no = ".$no;
//                                $this->obj_word->update($data, $where);
                            } else {
//                                $no = $this->obj_word->add($data);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Google翻訳
     */
    function google_translate($message, $lang_from, $lang_to) {
        // API URL
        $url = "http://ajax.googleapis.com/ajax/services/language/translate?";
        // 引数整形
        $params = "v=1.0" . "&q=" . rawurlencode($message) . "&langpair=" . $lang_from . "%7c" . $lang_to;
        // APIを叩く
        $result = file_get_contents($url.$params);
        // 戻り値を扱いやすくする
        $result_obj = json_decode($result);
        // ちゃんと返事が来てたら翻訳文を出力
        if ( $result_obj->responseStatus == 200 ) {
            return $result_obj->responseData->translatedText;
        } else {
            $this->logger->warn($result_obj);
            return false;
        }
    }

    /**
     * テンプレート比較フレーム
     */
    function action_get_template_frame() {
        $define_no = $this->request->get("define_no");
print <<<EOM
<html>
<head>
<title>N2MY Meeting 運用管理ツール Ver.1.0.0</title>
<script language="javascript1.2">

<!--

window.onload=init

ie=false
nn=false
if(document.all){ie=true}
if(navigator.appName=="Netscape"||navigator.userAgent.indexOf("Opera")!=-1){nn=true}

function init(){
if(ie){
frames["ja"].document.body.onscroll=scrollie
frames["en"].document.body.onscroll=scrollie
}
if(nn){
scroll=new Array(0,0)
scrollnn()
}
}

function scrollie(){
if(frames["en"].event){
frames["en"].scrollTo(frames["ja"].document.body.scrollLeft, frames[1].document.body.scrollTop)
}
if(frames["en"].event){
frames["ja"].scrollTo(frames["en"].document.body.scrollLeft, frames[1].document.body.scrollTop)
}
}

function scrollnn(){
    /* 横スクロールはいらんだろう・・・
var scr0=frames["ja"].pageXOffset
var scr1=frames["en"].pageXOffset

if(scr0!=scroll[0]){
//左がスクロール
frames["en"].scrollTo(scr0,0)
scroll[0]=scr0
scroll[1]=scr0
}else{
if(scr1!=scroll[1]){
//右がスクロール
frames["ja"].scrollTo(scr1,0)
scroll[0]=scr1
scroll[1]=scr1
}}*/

var scr0=frames["ja"].pageYOffset
var scr1=frames["en"].pageYOffset
if(scr0!=scroll[0]){
//左がスクロール
frames["en"].scrollTo(0,scr0)
scroll[0]=scr0
scroll[1]=scr0
}else{
if(scr1!=scroll[1]){
//右がスクロール
frames["ja"].scrollTo(0,scr1)
scroll[0]=scr1
scroll[1]=scr1
}}

setTimeout("scrollnn()",50)
}
//-->
</script>
</head>
<frameset rows="50%,*">
  <frame src="?action_get_template&define_no=$define_no&lang=zh#$define_no" name="multilang">
  <frameset cols="50%,*">
    <frame src="?action_get_template&define_no=$define_no&lang=ja" name="ja">
    <frame src="?action_get_template&define_no=$define_no&lang=en" name="en">
  </frameset>
</frameset>
</html>
EOM;
    }

    /**
     * 置換対象を表示
     */
    function action_get_template() {
        $define_no = $this->request->get("define_no");
        $lang = $this->request->get("lang");
        $whrer = "support = 1" .
                " AND flg = 1";
        $lang_list = $this->obj_lang->getRowsAssoc($whrer);

print <<<EOM
<html>
<head>
    <script type="text/javascript" src="/shared/js/lib/jquery.js"></script>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript" src="js/multilang.js"></script>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" href="css/api.css" type="text/css">
</head>
EOM;
        print '<body>';
        $where = "define_no = ".$define_no;
        $row = $this->obj_define->getRow($where);
        print "<xmp>";
        $contents = file_get_contents(N2MY_APP_DIR."templates/common/".$row["path"]);
        $pattern = '/{t+.*?{\/t}/';
        if (preg_match_all($pattern, $contents, $aMatches)) {
            $matches = $aMatches[0];
            $matches = array_unique($matches);
            if ($matches) {
                foreach($matches as $val) {
                    //print $val;
                    $aaa = preg_match('/}[0-9]+,_\(*/',$val, $m);
                    $id = substr($m[0], 1, -3);
                    $str = substr($val, strpos($val,"_('")+3, -6);
                    $form = '<span class="form_'.$id.'" style="display: none;">';
                    foreach ($lang_list as $lang) {
                        $form .= '<form><textarea id="to_'.$id.'_'.$lang["lang_cd"].'" cols="100" onchange="change_text('.$id.', \''.$lang["lang_cd"].'\' );">'.htmlspecialchars($this->_get_message($lang["lang_cd"],$id)).'</textarea>' .
                                '<input type="button" onclick="edit_lang('.$id.', \''.$lang["lang_cd"].'\');" value="編集" name="action_edit"/>' .
                                '<input type="reset" value="リセット" onclick="resetb('.$id.', \''.$lang["lang_cd"].'\');"/>'.$lang["lang_name"].'</form>';
                    }
                    $form .= '</span>';
                    $replace = '</xmp><a name="'.$id.'"></a><font color="#CC3333" size="+1"><a href="javascript:togglea('.$id.');"><xmp>'.$val.'</xmp></a></font>'.$form.'<xmp>';
                    $contents = substr_replace($contents, $replace, strpos($contents, $val), strlen($val));
                }
            }
        }
        print $contents;
        print "</xmp>";
    }

    /**
     * DBからデータを取得
     */
    function action_edit_form() {
        $define_no = $this->request->get("define_no");
        $where = "define_no = ".$define_no;
        $define_info = $this->obj_define->getRow($where);
        $this->session->set("form", $define_info);
        $this->render_edit_form();
    }

    /**
     * 編集画面
     */
    function render_edit_form() {
        $define_info = $this->session->get("form");
        $this->html_header();
        print '<form action="'.$_SERVER["SCRIPT_NAME"].'">';
        print '<input type="hidden" name="_action" value="">';
        print '<input type="hidden" name="define_no" value="'.$define_info["define_no"].'">';
        print '<table class="data">';
        print '<tr>';
        print '  <th>タイプ</th>';
        print '  <td>'.$define_info["type"].'</td>';
        print '</tr>';
        print '<tr>';
        print '  <th>パス</th>';
        print '  <td>'.$define_info["path"].'</td>';
        print '</tr>';
        print '<tr>';
        print '  <th>ID</th>';
        print '  <td><input type="text" name="define_id" size="65" maxlength="50" value="'.$define_info["define_id"].'" /></td>';
        print '</tr>';
        print '<tr>';
        print '  <th>文言</th>';
        print '  <td><textarea name="name" cols="100" rows="5">'.htmlspecialchars($define_info["name"]).'</textarea></td>';
        print '</tr>';
        print '<tr>';
        print '  <th>フラグ</th>';
        print '  <td><select name="flg">';
        print '      <option value="0"'.($define_info["flg"] == 0 ? ' selected="selected"' : '').'>未確認</option>';
        print '      <option value="1"'.($define_info["flg"] == 1 ? ' selected="selected"' : '').'>保留</option>';
        print '      <option value="2"'.($define_info["flg"] == 2 ? ' selected="selected"' : '').'>確認済み</option>';
        print '      <option value="3"'.($define_info["flg"] == 3 ? ' selected="selected"' : '').'>翻訳不要</option>';
        print '      <option value="4"'.($define_info["flg"] == 4 ? ' selected="selected"' : '').'>削除</option>';
        print '  </select></td>';
        print '</tr>';
        print '<tr>';
        print '  <th>作成日</th>';
        print '  <td>'.$define_info["create_datetime"].'</td>';
        print '</tr>';
        print '<tr>';
        print '  <th>更新日</th>';
        print '  <td>'.$define_info["update_datetime"].'</td>';
        print '</tr>';
        print '<tr>';
        print '  <th>編集</th>';
        print '  <td>';
        print '    <input type="button" value="編集" onclick="edit(this.form);" />';
//        print '    <input type="button" value="分割" onclick="data_split();" />';
        print '  </td>';
        print '</tr>';
        print '<tr>';
        print '  <th>分割</th>';
        print '  <td>';
        print '<div id="split_form">';
        print '<table class="data">';
        print '  <tr>';
        print '    <th>ID</th>';
        print '    <th>文言</th>';
        print '  </tr>';
        for ($i = 1; $i <= 5; $i++) {
            print '  <tr>';
            print '    <td><input type="text" name="split['.$i.'][define_id]" value="'.$define_info["define_id"].'_'.$i.'" /></td>';
            print '    <td><textarea name="split['.$i.'][name]" cols="70" rows="2"></textarea></td>';
            print '  </tr>';
        }
        print '</table>';
        print '</div>';
        print '  </td>';
        print '</tr>';
        print '</table>';
        print '</form>';
        // 編集履歴表示
        $whrer = "define_no = ".$define_info["define_no"];
        $define_log = new N2MY_DB($this->lang_dsn, "define_log");
        $define_log_list = $define_log->getRowsAssoc($whrer, array("define_log_no" => "desc"));
        if ($define_log_list) {
            print '<table class="data">';
            print '<tr>';
            print '<th>NO</a></th>';
            print '<th>タイプ</a></th>';
            print '<th>パス</a></th>';
            print '<th>ID</a></th>';
            print '<th>文言</a></th>';
            print '<th>check</th>';
            print '<th>作成日</th>';
            print '<th>更新日</th>';
            print '</tr>';
            foreach ($define_log_list as $row) {
                print '<tr';
                switch ($row["flg"]) {
                    case "1";
                        print ' bgcolor="#cccccc"';
                        break;
                    case "2";
                        print ' bgcolor="#ffcccc"';
                        break;
                }
                print '>';
                print '<td>'.$row["define_log_no"].'</td>';
                print '<td>'.$row["type"].'</td>';
                print '<td>'.$row["path"].'</td>';
                print '<td>'.$row["define_id"].'</td>';
                print '<td>'.htmlspecialchars($row["name"]).'</td>';
                print '<td>'.$this->flg_row[$row["flg"]].'</td>';
                print '<td>'.$row["create_datetime"].'</td>';
                print '<td>'.$row["update_datetime"].'</td>';
                print '</tr>';
            }
            print "</table>";
        }

        $whrer = "support = 1" .
                " AND flg = 1";
        $lang_list = $this->obj_lang->getRowsAssoc($whrer);

print <<<EOM
    <script type="text/javascript" src="/shared/js/lib/jquery.js"></script>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript" src="js/multilang.js"></script>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" href="css/api.css" type="text/css">
    <br />
EOM;
        $id = $define_info["define_no"];
        $where = "define_no = ".$id;
        $row = $this->obj_define->getRow($where);
        $form = '<span class="form_'.$id.'">';
        foreach ($lang_list as $lang) {
            $form .= '<form>'.$lang["lang_name"].'<br /><textarea id="to_'.$id.'_'.$lang["lang_cd"].'" cols="100" onchange="change_text('.$id.', \''.$lang["lang_cd"].'\' );">'.htmlspecialchars($this->_get_message($lang["lang_cd"],$id)).'</textarea>' .
                    '<input type="button" onclick="edit_lang('.$id.', \''.$lang["lang_cd"].'\');" value="編集" name="action_edit"/>' .
                    '<input type="reset" value="リセット" onclick="resetb('.$id.', \''.$lang["lang_cd"].'\');"/><br />作成日：'.$this->_get_create_datetime($lang["lang_cd"],$id).'　更新日：'.$this->_get_update_datetime($lang["lang_cd"],$id).'</form>';
        }
        $form .= '</span>';
        print $form;
    }

    /**
     * 内容編集
     */
    function action_edit() {
        $define_no = $this->request->get("define_no");
        $where = "define_no = ".$define_no;
        $define_info = $this->obj_define->getRow($where);
        $request = $this->request->getAll();
        $this->logger2->info($request);
        $rules = array(
            "define_no" => array(
                "required" => true,
                ),
//            "define_id" => array(
//                "required" => true,
//                ),
            "name" => array(
                "required" => true,
                ),
            );
        $this->err_obj = $this->obj_define->check($request, $rules);
        if (EZValidator::isError($this->err_obj)) {
            return $this->render_edit_form();
        }
        $rules = array(
            "define_id" => array(
                "required" => true,
                ),
            "name" => array(
                "required" => true,
                ),
            );
        $new_data = array();
        foreach($request["split"] as $split_data) {
            if ($split_data["define_id"] && $split_data["name"]) {
                $data = array(
                    "type"              => $define_info["type"],
                    "path"              => $define_info["path"],
                    "define_id"         => $split_data["define_id"],
                    "name"              => $split_data["name"],
                    "create_datetime"   => $this->now,
                    "update_datetime"   => $this->now,
                    );
                $this->err_obj = $this->obj_define->check($data, $rules);
                if (EZValidator::isError($this->err_obj)) {
                    return $this->render_edit_form();
                }
                $new_data[] = $data;
            }
        }
        // 編集前に履歴に記録
        $this->define_log($define_no);
        $data = array(
            "name"          => $request["name"],
            "flg"           => $request["flg"],
            "define_id"     => $request["define_id"],
            "update_datetime" => $this->now
            );
        $where = "define_no = ".$define_no;
        $this->obj_define->update($data, $where);
        foreach($new_data as $data) {
            $new_define_no = $this->obj_define->add($data);
        }
        header("Location: ".$_SERVER["SCRIPT_NAME"]);
    }

    /**
     * DBからデータを取得
     */
    function action_add_form() {
        $define_no = $this->request->get("define_no");
        $where = "define_no = ".$define_no;
        $define_info = $this->obj_define->getRow($where);
        $define_info["name"] = "";
        $this->session->set("form", $define_info);
        $this->render_add_form();
    }

    /**
     * 編集画面
     */
    function render_add_form() {
        $define_info = $this->session->get("form");
        $this->html_header();
        print '<form action="'.$_SERVER["SCRIPT_NAME"].'">';
        print '<input type="hidden" name="_action" value="">';
        print '<input type="hidden" name="define_no" value="'.$define_info["define_no"].'">';
        print '<table class="data">';
        print '<tr>';
        print '  <th>タイプ</th>';
        print '  <td><input type="text" name="type" value="'.$define_info["type"].'" /></td>';
        print '</tr>';
        print '<tr>';
        print '  <th>パス</th>';
        print '  <td><input type="text" name="path" value="'.$define_info["path"].'" /></td>';
        print '</tr>';
        print '<tr>';
        print '  <th>ID</th>';
        print '  <td><input type="text" name="define_id" value="'.$define_info["define_id"].'_1" /></td>';
        print '</tr>';
        print '<tr>';
        print '  <th>文言</th>';
        print '  <td><textarea name="name" cols="100" rows="5">'.htmlspecialchars($define_info["name"]).'</textarea></td>';
        print '</tr>';
        print '<tr>';
        print '  <th>フラグ</th>';
        print '  <td><select name="flg">' .
                '<option value="0">未確認</option>' .
                '<option value="1">保留</option>' .
                '<option value="2">確認済み</option>' .
                '<option value="3">翻訳不要</option>' .
                '<option value="4">削除</option>' .
                '</select></td>';
        print '</tr>';
        print '<tr>';
        print '  <th>作成日</th>';
        print '  <td>'.$define_info["create_datetime"].'</td>';
        print '</tr>';
        print '<tr>';
        print '  <th>更新日</th>';
        print '  <td>'.$define_info["update_datetime"].'</td>';
        print '</tr>';
        print '<tr>';
        print '  <th>追加</th>';
        print '  <td>';
        print '    <input type="button" value="実行" onclick="add(this.form);" />';
        print '  </td>';
        print '</tr>';
        print '</table>';
        print '</form>';
    }

    /**
     * 内容編集
     */
    function action_add() {
        $request = $this->request->getAll();
        $this->logger2->info($request);
        $rules = array(
            "type" => array(
                "required" => true,
                ),
//            "define_id" => array(
//                "required" => true,
//                ),
            "name" => array(
                "required" => true,
                ),
            );
        $this->err_obj = $this->obj_define->check($request, $rules);
        if (EZValidator::isError($this->err_obj)) {
            return $this->render_add_form();
        }
        $data = array(
            "type"              => $request['type'],
            "path"              => $request['path'],
            "define_id"         => $request['define_id'],
            "flg"               => $request['flg'],
            "name"              => $request['name'],
            "create_datetime"   => $this->now,
            "update_datetime"   => $this->now,
            );
        $this->obj_define->add($data);
        $this->logger2->info($this->obj_define->_conn->last_query);
        header("Location: ".$_SERVER["SCRIPT_NAME"]);
    }

    /**
     * 編集履歴作成
     */
    function define_log($define_no) {
        $where = "define_no = $define_no";
        $row = $this->obj_define->getRow($where);
        $this->logger2->info($row);
        if ($row) {
            $define_log = new N2MY_DB($this->lang_dsn, "define_log");
            $define_log->add($row);
        }
    }

    /**
     * 一括編集
     */
    function action_select_edit() {
        $request = $this->request->getAll();
        if ($request["flg"] && $request["check_flg"]) {
            $now = date("Y-m-d H:i:s");
            $data = array(
                "flg" => $request["flg"],
                "update_datetime" => $this->now,
            );
            $where = "define_no IN (".join($request["check_flg"], ",").")";
        }
        $this->obj_define->update($data, $where);
        $this->default_view();
    }

    function action_sort() {
        $field = $this->request->get("action_sort", "define_no");
        $action = $this->request->get("act");
        $this->session->set("page", 1, $action);
        if ($sort = $this->session->get("sort", $action)) {
            list($sort_key, $sort_type) = each($sort);
        }
        if ($sort_key == $field) {
            if ($sort_type == "desc") {
                $type = "asc";
            } else {
                $type = "desc";
            }
        } else {
            $type = "asc";
        }
        $sort = array(
            $field => $type,
        );
        $this->session->set("sort", $sort, $action);
        header("Location: index.php?$action");
    }

    function action_search() {
        $request = $this->request->getAll();
        $action = $this->request->get("act");
        $this->session->set("page", 1, $action);
        $this->session->set("search_form", $request, $action);
        header("Location: index.php?$action");
    }

    function action_create_lang() {
        $lang = $this->request->get("lang");
        $where = "type = 'template_html'" .
                " AND flg = 2";
        $rs = $this->obj_define->select($where);
        if (DB::isError($rs)) {
            $this->logger2->error($rs->getUserInfo());
        }
        while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            if ($this->_get_message($lang, $row["define_no"]) === false) {
                $data = array(
                    "define_no"       => $row["define_no"],
                    "lang_cd"         => $lang,
                    "value"           => "",
                    "flg"             => 0,
                    "create_datetime" => $this->now,
                    "update_datetime" => $this->now,
                    );
                $this->logger2->info($data);
                $ret = $this->obj_word->add($data);
            }
        }
        header("Location: ".$_SERVER["SCRIPT_NAME"]);
    }

    function db_gettext() {
        $faq = new N2MY_DB(N2MY_MDB_DSN, "faq_list");
        $where = "faq_status = 1" .
                " AND faq_lang = 0";
        $list = $faq->getRowsAssoc($where);
        foreach ($list as $row) {
            // FAQ質問
            $data = array(
                "type" => "faq_answer",
                "path" => $row["faq_category"]."/".$row["faq_subcategory"],
                "define_id" => $row["faq_key"],
                "name" => $row['faq_answer'],
                "create_datetime" => $this->now,
                "update_datetime" => $this->now,
                );
            $define_no = $this->obj_define->add($data);
            if (DB::isError($define_no)) {
                $this->logger2->info($define_no->getUserInfo());
            }
            // FAQ解答
            $data = array(
                "type" => "faq_question",
                "path" => $row["faq_category"]."/".$row["faq_subcategory"],
                "define_id" => $row["faq_key"],
                "name" => $row['faq_question'],
                "create_datetime" => $this->now,
                "update_datetime" => $this->now,
                );
            $define_no = $this->obj_define->add($data);
            if (DB::isError($define_no)) {
                $this->logger2->info($define_no->getUserInfo());
            }
        }
        return true;
    }

    function action_gettext() {
        // セキュリティ設定
        print "<pre>";
        $is_test = false;
        $this->gettext($this->template->template_dir."common/", "admin/security/*.t.html", "template_html", $is_test);
        $this->gettext($this->template->template_dir."common/", "admin/room/all*.t.html", "template_html", $is_test);
        $this->gettext($this->template->template_dir."common/", "admin/room/edit*.t.html", "template_html", $is_test);
        $this->gettext($this->template->template_dir."common/", "admin/room/form*.t.html", "template_html", $is_test);
        $this->gettext($this->template->template_dir."common/", "admin/room/index*.t.html", "template_html", $is_test);
        /*
        set_time_limit(0);
        $this->obj_define->_conn->query("TRUNCATE define");
        $this->obj_define->_conn->query("TRUNCATE word");
        // HTMLテンプレート
        $this->gettext($this->template->template_dir."ja/", "*.t.html", "template_html");
        // 設定ファイル
        $this->gettext($this->template->template_dir."ja/", "*.t.xml", "xml");
        // メールテンプレート
        $this->gettext($this->template->template_dir."ja/", "*.t.txt", "mail");
        // 共通
        $this->gettext($this->template->template_dir."common/", "*.t.xml", "flash_config");
        // html
        $this->gettext(N2MY_DOCUMENT_ROOT, "*.html", "html");
        // javascript
        $this->gettext(N2MY_DOCUMENT_ROOT, "*.js", "js");
        // メッセージファイル
        $this->gettext(N2MY_APP_DIR."config/ja/", "message.ini", "n2my_web");
        // db
        $this->db_gettext();
        */
//        header("Location: ".$_SERVER["SCRIPT_NAME"]);
    }

    /**
     * テキストファイルを取得
     */
    function gettext($template_dir, $pattern, $type, $check = false) {
        $list = $this->rglob($pattern, 0, $template_dir);
        $ignore_files = array(
            "htdocs/admin_tool/selenium",
            "htdocs/shared/js/lib/",
            "htdocs/shared/js/calendar/calendar.js",
            );
            $this->logger2->info($list);
        foreach ($list as $file) {
//            $this->logger2->info(array($file, memory_get_peak_usage()));
            foreach($ignore_files as $ignore_file) {
                if (strpos($file, $ignore_file) !== false) {
                    continue 2;
                }
            }
            $pattern = '/("([^"\\\\]|\\\\.)*")|' .
                    '(\'([^\'\\\\]|\\\\.)*\')|' .
                    '(>(.*?)<)/u';

            /* 適当に試した結果がこれだよ
            $pattern = '/("[^\!-\/\:-\@\s\[-\`-]([^"\\\\]|\\\\.)*")|(\'[^\!-\/\:-\@\s\[-\`-]([^\'\\\\]|\\\\.)*\')|(>(.*?)<)/u';
            '/("[^\!-\/0-9a-zA-Z\:-\@\s\[-\`\{-\}]([^"\\\\]|\\\\.)*")|(\'[^\!-\/0-9a-zA-Z\:-\@\s\[-\`\{-\}]([^\'\\\\]|\\\\.)*\')/u';
            '/("[^\!-\/0-9a-zA-Z\:-\@\s\[-\`\{-\}]([^"\\\\]|\\\\.)*")|(\'[^\!-\/0-9a-zA-Z\:-\@\s\[-\`\{-\}]([^\'\\\\]|\\\\.)*\')/u';
            '/(\'[^\!-\/0-9a-zA-Z\:-\@\s\[-\`\{-\}]([^\'\\\\]|\\\\.)*\')/u';
            '/("[^\!-\/0-9a-zA-Z\:-\@\s\[-\`\{-\}]([^"\\\\]|\\\\.)*")/u';
            '/\'([^\'\\\\]|\\\\.)*\'/s';
            '/"([^"\\\\]|\\\\.)*"/s';
            '/("[^\!-\/0-9a-zA-Z\:-\@\s\[-\`\{-\}]([^"\\\\]|\\\\.)*")|([^\!-\/0-9a-zA-Z\:-\@\s\[-\`\{-\}]+[0-9a-zA-Z\s.\[-\]][^\!-\/0-9a-zA-Z\:-\@\s\[-\`\{-\}]+)/u';
            '/("[^\!-\/0-9a-zA-Z\:-\@\s\[-\`\{-\}]([^"\\\\]|\\\\.)*")|([^\!-\/0-9a-zA-Z\:-\@\s\[-\`\{-\}]+[0-9a-zA-Z\s.\[-\]]+)/u';
            '/("[^\!-\/0-9a-zA-Z\:-\@\s\[-\`\{-\}]([^"\\\\]|\\\\.)*")/u';
            '/(([^\!-\/0-9a-zA-Z\:-\@\s\[-\`\{-\}])+[0-9a-zA-Z\s.\[-\]]*[^\!-\/0-9a-zA-Z\:-\@\s\[-\`\{-\}]*)/u';
            '/([^\!-\/0-9a-zA-Z\:-\@\s\[-\`\{-\}]+[0-9a-zA-Z\s.\[-\]])+/u';

            '/[^\!-\/0-9a-zA-Z\:-\@\s\[-\`\{-\}]+/u';
            '/[a-z]{4}|([^\s-\~]+|[s-\~])+/u';
            // これだと日本語しか引っかからない。記号とか色々
            '/([一-龠ぁ-んァ-ヴーａ-ｚＡ-Ｚ０-９]|[a-zA-Z0-9\s])+/u';
            '/([一-龠ぁ-んァ-ヴーａ-ｚＡ-Ｚ０-９]|[a-zA-Z0-9])+/u';
            '/[一-龠ぁ-んァ-ヴーａ-ｚＡ-Ｚ０-９]+[a-zA-Z0-9]/u';
            '/[一-龠ぁ-んァ-ヴーａ-ｚＡ-Ｚ０-９]+/u';
            '/[一-龠ぁ-んァ-ヴーａ-ｚＡ-Ｚ０-９]+/u';
            '/[一-龠ぁ-んァ-ヴーａ-ｚＡ-Ｚ０-９a-zA-Z0-9 、。！”｜「」\s,\(\)]+/u';
            '/[一-龠ぁ-んァ-ヴーａ-ｚＡ-Ｚ０-９a-zA-Z0-9\s]+/u';
            '/[一-龠ぁ-んァ-ヴーａ-ｚＡ-Ｚ０-９a-zA-Z0-9]+/u';
            '/[一-龠ぁ-んァ-ヴーａ-ｚＡ-Ｚ０-９]+/u';
            */
            if (!file_exists($file)) {
                return false;
            }
            $contesnts = file_get_contents($file);
            $matches = array();
            if (preg_match_all($pattern, $contesnts, $aMatches)) {
                $matches = $aMatches[0];
            }
      //      print "抽出パターン: ".$pattern."<br />";
    //        print $path."<br />";
    //        print '<textarea cols=150 rows=10>';
    //        print $contesnts."\n";
            $i = 0;

            $data = array();
            foreach($matches as $val) {
                $val = trim($val);
                if (preg_match('/[^\!-\/0-9a-zA-Z\:-\@\s\[-\`\{-\}]+/u', $val) && substr($val, 1, 2) != '{t') {
                    $this->logger2->info($val);
                    $num = $i++;
                    // 先頭、最後尾不要
                    $data[] = substr($val,1,-1);
                }
            }
            // 重複不要
            $data = array_unique($data);
            /*
             * テレビ会議、テレビ会議システムの順で置換すると、
             * テレビ会議システムが変換できなくなってしまうので、
             * 文字列が長い方から順にソートする。
             */
            $word = array();
            $volume = array();
            $edition = array();
            foreach($data as $val) {
//                print $val."<br>";
                $word[] = array("length" => strlen($val), "name" => $val);
            }
            if ($word) {
                $script_path = str_replace($template_dir, "", $file);
                $replace_list = array();
                foreach ($word as $key => $row) {
                    $volume[$key]  = $row['length'];
                    $edition[$key] = $row['name'];
                    $id = "id_".str_pad($key, 3, "0", STR_PAD_LEFT);
                    $data = array(
                        "type" => $type,
                        "path" => $script_path,
                        "define_id" => $id,
                        "flg" => 2,
                        "name" => $row["name"],
                        "create_datetime" => $this->now,
                        "update_datetime" => $this->now,
                        );
                    $this->logger2->info($data);
                    if ($check) {
                        $define_no = uniqid();
                    } else {
                        $define_no = $this->obj_define->add($data);
                        if (DB::isError($define_no)) {
                            $this->logger2->info($define_no->getUserInfo());
                        } else {
                            // 日本語をデフォルトとして登録
                            $data = array(
                                "define_no"       => $define_no,
                                "lang_cd"         => "ja",
                                "value"           => $row["name"],
                                "flg"             => 0,
                                "create_datetime" => $this->now,
                                "update_datetime" => $this->now,
                                );
                            $this->logger2->info($data);
                            $ret = $this->obj_word->add($data);
                        }
                    }
                    $word[$key]["define_no"] = $define_no;
                }
                array_multisort($volume, SORT_DESC, $edition, SORT_ASC, $word);
                print $file;
                print_r($word);
                $result = $this->replace($file, $word);
                $missmatch = $result["missmatch"];
                $contents = $result["contents"];
                if (!$check) {
                    file_put_contents($file, $contents);
                }
//                print_r($word);
            }
    //        print "</textarea>";
    //        print "<hr>";
        }
        return true;
    }

    function action_settext() {
        // HTMLテンプレート
//        $this->settext("template_html");
//        // 設定ファイル
        $this->settext("xml");
//        // メールテンプレート
//        $this->settext("mail");
        // 共通
//        $this->settext("flash_config");
        // javascript
//        $this->gettext(N2MY_DOCUMENT_ROOT, "*.js", "js");
//        header("Location: ".$_SERVER["SCRIPT_NAME"]);
    }

    function settext($type) {
        $this->html_header();
        print "<pre>";
        $where = "type = '".$type."'" .
                " AND flg IN (2,3)";
        $sort = array(
            "path" => "asc",
            "LENGTH(name)" => "desc"
        );
        $base_dir = "";
        switch ($type) {
            case "template_html":
            case "xml":
                $base_dir = $this->template->template_dir."common/";
                break;
            default:
                return false;
        }
        $list = $this->obj_define->getRowsAssoc($where, $sort);
        $_path = "";
        $replace_list = array();
        $end_item = end($list);
        list($end,$val) = each($end_item);
        foreach ($list as $row) {
            if ($_path != $row["path"] || $row["define_no"] == $val) {
                if ($_path && $replace_list) {
                    $file = $base_dir.$replace_list[0]["path"];
                    $result = $this->replace($file, $replace_list);
                    $missmatch = $result["missmatch"];
                    $contents = $result["contents"];
                    if ($missmatch) {
                        foreach($missmatch as $key => $str) {
                            /*
                            $id = "auto_".str_pad($key, 3, "0", STR_PAD_LEFT);
                            $script_path = str_replace($base_dir, "", $file);
                            $data = array(
                                "type" => $type,
                                "path" => $script_path,
                                "define_id" => $id,
                                "name" => $str,
                                "flg" => "2",
                                "create_datetime" => $now,
                                "update_datetime" => $now,
                                );
                            $define_no = $this->obj_define->add($data);
                            if (DB::isError($define_no)) {
                                $this->logger2->info($define_no->getUserInfo());
                            }
                            */
                        }
                    }
                    //file_put_contents($file, $contents);
                }
                $replace_list = array();
            }
            $replace_list[] = $row;
            $_path = $row["path"];
        }
    }

    function replace($file, $replace_list) {
        print $file;
        $contents = file_get_contents($file);
        if ($contents) {
            $list = array();
            // 重複を排除
            foreach ($replace_list as $key => $row) {
                if (!in_array($row["name"], $list)) {
                    $list[$row["define_no"]] = $row["name"];
                }
            }
            foreach ($list as $key => $val) {
                $contents = str_replace($val, '{t}'.$key."{/t}", $contents);
            }
            foreach ($list as $key => $val) {
                $contents = str_replace('{t}'.$key."{/t}", '{t}'.$key.",_('".$val."'){/t}", $contents);
            }
            $pattern = '/[^\!-\/0-9a-zA-Z\:-\@\s\[-\`\{-\}]+/u';
            $matches = array();
            if (preg_match_all($pattern, $contents, $aMatches)) {
                $matches = $aMatches[0];
            }
            $matches = array_unique($matches);
            if ($matches) {
//                print "<br/>$file<ul>";
//                print "<textarea cols=120 rows=15>".htmlspecialchars($contents)."</textarea><br />";
//                print "<textarea cols=120 rows=10>";
//                print "[NG]\n";
//                print_r($matches);
//                print "[変換候補]\n";
//                print_r($list);
//                print "</textarea>";
//                print "</ul>";
            }
            print "<xmp>";
            print $contents;
            print "</xmp>";
            return array(
                "missmatch" => $matches,
                "contents" => $contents
                );
        }
    }

    function action_check() {
        $template_dir = $work_file = $work = N2MY_APP_DIR."multi_lang/template_html/";
        $pattern = "*.t.html";
        $list = $this->rglob($pattern, 0, $template_dir);
        $pattern = '/[^\!-\/0-9a-zA-Z\:-\@\s\[-\`\{-\}]+/u';
//        print "<pre>";
        foreach($list as $file) {
            // ECOメーターと、管理者ツール以外
            if (!ereg('/admin_tool|eco|charge/', $file)) {
//               print "$file<br />";
                $contents = file_get_contents($file);
                if (preg_match_all($pattern, $contents, $aMatches)) {
                    $matches = $aMatches[0];
//                    print_r($matches);
                }
            }
        }
    }

    function action_check2() {
        $this->html_header();
        $template_dir = N2MY_APP_DIR."templates/";
        $pattern = "*.t.html";
        $list = $this->rglob($pattern, 0, $template_dir);
        $this->gettext_check($list, 'template_html');
        $pattern = "*.t.xml";
        $list = $this->rglob($pattern, 0, $template_dir);
        $this->gettext_check($list, 'xml');
        $pattern = "*.t.csv";
        $list = $this->rglob($pattern, 0, $template_dir);
        $this->gettext_check($list, 'template_html');
    }

    function action_diff() {
        require_once 'lib/EZLib/EZUtil/EZImage.class.php';
        $lang_list   = $this->obj_lang->getRowsAssoc("flg = 1 AND support = 1", null , null, null, null, "lang_cd");
        $ext_list = array("gif", "jpg", "png", "pdf", "exe");
        $this->html_header();
        print <<<EOM
<link type="text/css" href="css/basic.css" rel="stylesheet" media="screen" />
EOM;
        $base_dir = N2MY_DOCUMENT_ROOT."lang/";
        $template_dir = $base_dir."ja_JP/";
        $pattern = "*";
        $list = $this->rglob($pattern, 0, $template_dir);
        print '<table class="data">'."\n";
        print '<tr>'."\n";
        print '<th>ファイル</td>'."\n";
        foreach ($lang_list as $lang_cd => $lang) {
            print '<th width="25%">'.$lang["lang_name"].'</th>'."\n";
        }
        print '</tr>'."\n";
        foreach ($list as $file) {
            if (is_file($file)) {
                $path_info = pathinfo($file);
                if (in_array($path_info["extension"], $ext_list)) {
                    $path = substr($file, strlen($template_dir));
                    print '<tr>'."\n";
                    print '<td>'.$path.'</td>'."\n";
                    foreach ($lang_list as $lang_cd => $lang) {
                        print '<td align="center">';
                        $lang_file = $base_dir.$lang["locale_id"].'/'.$path;
                        if (file_exists($lang_file)) {
                            switch ($path_info["extension"]) {
                                case "gif":
                                case "jpg":
                                case "png":
                                    print '<a href="/lang/'.$lang["locale_id"].'/'.$path.'" title="" class="lightbox" rel="superbox[500x200]">';
                                    print '<img border=1 src="/lang/'.$lang["locale_id"].'/'.$path.'" alt="" />';
                                    print '</a>';
                                    print '<a href="#" class="basic-modal" id="/lang/'.$lang["locale_id"].'/'.$path.'">';
                                    list($width, $height, $type, $attr) = getimagesize($lang_file);
                                    print '<br />';
                                    print $width." x ". $height;
                                    break;
                                default:
                                    print '<a href="#" class="basic-modal" id="/lang/'.$lang["locale_id"].'/'.$path.'">';
                            }
                            $stat = stat($lang_file);
                            print '<br />';
                            print $stat["size"]."Byte";
                        } else {
                            print '<a href="#" class="basic-modal" id="/lang/'.$lang["locale_id"].'/'.$path.'">';
                            print 'no data';
                        }
                        print '</a>';
                        print '</td>'."\n";
                    }
                    print '</tr>'."\n";
                }
            }
        }
        print '</table>';
        print <<<EOM
<div id="basic-modal-content">
    <form action="index.php" method="post" enctype="multipart/form-data">
    <fieldset>
    <legend>ファイルの更新</legend>
    <table class="data">
    <tr>
        <th>path</th>
        <td><input type="text" name="path" id="upload_path" size="40" value="" readonly="readonly" /></td>
    </tr>
    <tr>
        <th>file</th>
        <td><input type="file" name="file" size="60" value="" /></td>
    </tr>
    <tr>
        <th>&nbsp;</th>
        <td><input type="submit" name="action_upload_file" value="変更" /></td>
    </tr>
    </table>
    </fieldset>
    </form>
</div>
<script type='text/javascript' src='/shared/js/lib/jquery.js'></script>
<script type='text/javascript' src='js/jquery.simplemodal.js'></script>
<script type='text/javascript' src='js/basic.js'></script>
<script type="text/javascript" src="js/jquery.lightbox-0.5.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.lightbox-0.5.css" media="screen" />
<script type="text/javascript">
$(function() {
    $('.data .lightbox').lightBox();
});
</script>
</body>
</html>
EOM;
    }

    function action_upload_file() {
        $path = $this->request->get("path");
        $upload_file_info = pathinfo($_FILES["file"]["name"]);
        $move_file_info = pathinfo(N2MY_DOCUMENT_ROOT.$path);
        if (is_uploaded_file($_FILES["file"]["tmp_name"]) && $_FILES["file"]["error"] == 0 && $upload_file_info["extension"] == $move_file_info["extension"]) {
            move_uploaded_file($_FILES["file"]["tmp_name"], N2MY_DOCUMENT_ROOT.$path);
        }
        $this->action_diff();
    }

    function gettext_check($list, $type) {
        $pattern = '/{t+.*?{\/t}/';
        $template_dir = N2MY_APP_DIR."templates/common/";
        print "<pre>";
        foreach($list as $file) {
            // ECOメーターと、管理者ツール以外
            $contents = file_get_contents($file);
            $path = str_replace($template_dir, '', $file);
            if (preg_match_all($pattern, $contents, $aMatches)) {
                $matches = $aMatches[0];
//                print_r($matches);
                if ($matches) {
                    foreach($matches as $val) {
                        $aaa = preg_match('/}[0-9]+,_\(*/',$val, $m);
                        $id = substr($m[0], 1, -3);
                        $str = substr($val, strpos($val,"_('")+3, -6);
                        if (isset($result[$file][$id])) {
                            // 同一IDで文言が異なる
                            if ($result[$file][$id] != $str) {
                                print "$file:&lt;<a href='?action_add=&define_no=$id' target='translator'>$id</a>&gt;:<font color=red>同一IDで文言が異なる</font>\n";
                                print_r(array(htmlspecialchars($result[$file][$id]), htmlspecialchars($str)));
                            }
                        } else {
                            // 登録済みのDBと異なる
                            $where = "define_no = '".$id."'";
                            $before = $this->obj_define->getOne($where, "name");
                            if ($before != $str) {
                                if (!$before) {
                                    print "$file:&lt;<a href='?action_add=&type=$type&path=".urlencode($path)."&define_id=&name=".urlencode($str)."&flg=2' target='translator'>$id</a>&gt;:<font color=blue>追加</font>\n";
                                } else {
                                    print "$file:&lt;<a href='?action_edit_form=&define_no=$id' target='translator'>$id</a>&gt;:<font color=blue>DBと違う</font>\n";
                                }
                                $where = "type = '$type' AND name = '".addslashes($str)."' AND flg = 2";
                                $define_ids = $this->obj_define->getCol($where, "define_no");
                                if ($define_ids) {
                                    print '<font color="green">既に存在する文言ID一覧</font>( '.join(", ", $define_ids)." )\n";
                                }
                                print_r(array(htmlspecialchars($before), htmlspecialchars($str)));
                            }
                            $result[$file][$id] = $str;
                        }
                        /*
                        if (ereg('/admin_tool|eco|charge/', $file)) {
                            $data = array(
                                "flg" => 3,
                                "update_datetime" => $this->now
                                );
                            $where = "define_no = ".$id;
                            $this->obj_define->update($data, $where);
                        } else {
                            $data = array(
                                "flg" => 2,
                                "update_datetime" => $this->now
                                );
                            $where = "define_no = ".$id;
                            $this->obj_define->update($data, $where);
                        }
                        */
                    }
                }
            }
        }
//        ksort($result);
        print_r($result);
    }

    /**
     * 対象ファイル一覧を取得
     */
    function rglob($pattern, $flags = 0, $path = '') {
        if (!$path && ($dir = dirname($pattern)) != '.') {
            if ($dir == '\\' || $dir == '/') $dir = '';
            return $this->rglob(basename($pattern), $flags, $dir . '/');
        }
        $paths = glob($path . '*', GLOB_ONLYDIR | GLOB_NOSORT);
        $files = glob($path . $pattern, $flags);
        foreach ($paths as $p) $files = array_merge($files, $this->rglob($pattern, $flags, $p . '/'));
        return $files;
    }

    /**
     * ヘッダ
     */
    function html_header() {
print <<<EOM
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <script type="text/javascript" src="/shared/js/lib/jquery.js"></script>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript" src="js/multilang.js"></script>
    <link rel="stylesheet" type="text/css" href="css/api.css">
</head>
EOM;
        $_param = parse_url($this->lang_dsn);
        if (substr($_param["path"], 1) == "n2my_try_lang_wk") {
            print '<body bgcolor="#ddddff"><h3>開発中</h3>';
        } else {
            print '<body>';
        }
        print '| <a href="?">文言表</a> | ';
        print '<a href="?action_check2">テンプレートチェック</a> | ';
        print '<a href="?action_diff">静的コンテンツのチェック</a> | ';
        print '<a href="?action_add_define_form">追加</a> | ';
        print '<a href="?action_translator">翻訳</a> | ';
        print '<a href="?action_export_form&type=csv">CSVエクスポート</a> | ';
        print '<a href="?action_import_form&type=csv">CSVインポート</a> | ';
        print '<a href="?action_fix_form">適用</a> | ';
        print '<a href="?action_config_from">設定</a> | ';
        print '<a href="?action_backup_form">バックアップ</a> | ';
//        print '<a href="?action_settext">テンプレート書き換え</a> | ';
        print '<hr size=1>';
//        print '<a href="?action_export&type=utf8">XMLエクスポート</a> | ';
    }

    function _pager($pager, $action = "") {
        print '<p>全 <strong class="total strong">'.$pager["total"].'</strong> 件中 <span class="begin">'.$pager["start"].'</span>～<span class="end">'.$pager["end"].'</span>件を表示しています。';
        print '<span class="pager"> (';
        if ($pager["prev"]) {
            print '&nbsp;<a href="javascript:void(0);" onclick="jump(1, \''.$action.'\');return false;">[1]</a>';
            print '&nbsp;<a href="javascript:void(0);" onclick="jump('.$pager["prev"].', \''.$action.'\');return false;">前へ</a>';
        }
        foreach ($pager["range"] as $key => $is_current) {
            if ($is_current) {
                print '&nbsp;<b>'.$key.'</b>';
            } else {
                print '&nbsp;<a href="javascript:void(0);" onclick="jump('.$key.', \''.$action.'\');return false;">'.$key.'</a>';
            }
        }
        if ($pager["next"]) {
            print '&nbsp;<a href="javascript:void(0);" onclick="jump('.$pager["next"].', \''.$action.'\');return false;">次へ</a>';
            print '&nbsp;<a href="javascript:void(0);" onclick="jump('.$pager["last"].', \''.$action.'\');return false;">['.$pager["last"].']</a>';
        }
        print '&nbsp;) </span><br />';
    }

}

$main = new AppMultiLang();
$main->execute();
?>