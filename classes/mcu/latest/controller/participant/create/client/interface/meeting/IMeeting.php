<?php

interface IMeeting {
	function checkBlocked();
	function createMeetingRecord();
	function setMeetingRecord();
}
