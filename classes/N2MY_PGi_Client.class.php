<?php
/*
 * Created on 2009/07/29
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once("lib/EZLib/EZCore/EZLogger2.class.php");

class N2MY_PGi_Client
{
    var $url = "";
    var $web_id;
    var $web_pw;
    var $_encodings = array('ISO-8859-1', 'US-ASCII', 'UTF-8');

    function __construct($url, $web_id, $web_pw) {
        $this->logger = EZLogger2::getInstance();
        $this->url    = $url;
        $this->web_id = $web_id;
        $this->web_pw = $web_pw;
    }

    /**
     * WSDL なにそれうまいの？
     * 何故かいつも失敗するのでメッセージは自分で生成
     * Smartyで生成することにした
     */
    private function _creaet_message($template_name, $params) {
        $config = EZConfig::getInstance();
        $template = new EZTemplate($config->getAll('SMARTY_DIR'));
        $template->assign("web_id", $this->web_id);
        $template->assign("web_pw", $this->web_pw);
        $template->assign("params", $params);
        $template_file = "common/pgi/".$template_name.".t.xml";
        $body = $template->fetch($template_file);
        return $body;
    }

    /**
     *
     */
    function call($template_name, $params, $headers = array(), $options = array()) {
        $msg = $this->_creaet_message($template_name, $params);
        if ($msg == "") {
            $this->logger->error('pgi call failed te  template :'.$template_name.' params:'. var_export($params));
            return false;
        }
        $this->logger->debug(array($this->url, $msg, $headers));
        $urlparts = parse_url($this->url);
        if (strcasecmp($urlparts['scheme'], 'HTTP') == 0) {
            if (!isset($urlparts['port'])) {
                $urlparts['port'] = 80;
            }
            $result = $this->http($urlparts, $msg, $headers, $options);
        } elseif (strcasecmp($urlparts['scheme'], 'HTTPS') == 0) {
            if (!isset($urlparts['port'])) {
                $urlparts['port'] = 443;
            }
            $result = $this->https($urlparts, $msg, $headers, $options);
        } else {
            $this->logger->error('pgi error bad url_schema : '.$urlparts['scheme']);
            return false;
        }
        $this->logger->debug('pgi said : '.$result);
        if (!$result = $this->_parseResponse($result)) {
            $this->logger->error('pgi error parse failed result :'.$result);
            return false;
        }
        $result = $result['s:Envelope']['s:Body'][0];
        return $result;
    }

    private function http($urlparts, $msg, $headers = array(), $options = array()) {
        $fullpath = $urlparts['path'];
        if (isset($urlparts['query'])) {
            $fullpath .= '?' . $urlparts['query'];
        }
        if (isset($urlparts['fragment'])) {
            $fullpath .= '#' . $urlparts['fragment'];
        }
        $headers['Host'] = $urlparts['host'];
        $headers['Content-Length'] = strlen($msg);
        if (isset($options['headers'])) {
            $headers = array_merge($headers, $options['headers']);
        }
        $_headers = '';
        foreach ($headers as $k => $v) {
            $_headers .= "$k: $v\r\n";
        }
        $outgoing_payload = "POST $fullpath HTTP/1.0\r\n" . $_headers .
            "\r\n" . $msg;
        $this->logger->debug($outgoing_payload);
        $incoming_payload = '';
        $host = $urlparts['host'];
        $port = $urlparts['port'];
        // タイムアウト
        $timeout = isset($options["timeout"]) ? $options["timeout"] : 0;
        if ($timeout > 0) {
            $fp = @fsockopen($host, $port, $errno, $errmsg, $timeout);
        } else {
            $fp = @fsockopen($host, $port, $errno, $errmsg);
        }
        if (!$fp) {
            $this->logger->error("pgi Connect Error to $host:$port");
            return false;
        }
        if ($timeout > 0) {
            @socket_set_timeout($fp, $timeout);
        }
        if (!fwrite($fp, $outgoing_payload)) {
            $this->logger->error("pgi Error POSTing Data to $host");
            fclose($fp);
            return false;
        }
        do {
            $data = fread($fp, 4096);
            $_tmp_status = socket_get_status($fp);
            if ($_tmp_status['timed_out']) {
                $this->logger->error("pgi Timed out read from $host");
                fclose($fp);
                return false;
            } else {
                $incoming_payload .= $data;
            }
        } while (!$_tmp_status['eof']);
        fclose($fp);
        return $incoming_payload;
    }

    /**
     * Sends the outgoing HTTPS request and reads and parses the response.
     *
     * @access private
     *
     * @param string $msg     Outgoing SOAP package.
     * @param array $options  Options.
     *
     * @return string  Response data without HTTP headers.
     */
    private function https($urlparts, $msg, $headers = array(), $options = array()) {
        /* Check if the required curl extension is installed. */
        if (!extension_loaded('curl')) {
            $this->logger->error('pgi CURL Extension is required for HTTPS');
            return false;
        }

        $ch = curl_init();
        if (isset($options['proxy_host'])) {
            $port = isset($options['proxy_port']) ? $options['proxy_port'] : 8080;
            curl_setopt($ch, CURLOPT_PROXY,
                        $options['proxy_host'] . ':' . $port);
        }
        if (isset($options['proxy_user'])) {
            curl_setopt($ch, CURLOPT_PROXYUSERPWD,
                        $options['proxy_user'] . ':' . $options['proxy_pass']);
        }

        if (isset($options['user'])) {
            curl_setopt($ch, CURLOPT_USERPWD,
                        $options['user'] . ':' . $options['pass']);
        }
        $headers['Content-Length'] = strlen($msg);
        foreach ($headers as $header => $value) {
            $headers[$header] = $header . ': ' . $value;
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // タイムアウト
        $timeout = isset($options["timeout"]) ? $options["timeout"] : 0;
        if ($timeout) {
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        }
        $data = array("url" => $this->url,
                      "msg" => $msg,
                      "headers" => $headers);
        $this->logger->debug($data);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $msg);
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        // SSL
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        if (defined('CURLOPT_HTTP_VERSION')) {
            curl_setopt($ch, CURLOPT_HTTP_VERSION, 3);
        }
        if (!ini_get('safe_mode') && !ini_get('open_basedir')) {
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        }
        if (isset($options['curl'])) {
            foreach ($options['curl'] as $key => $val) {
                curl_setopt($ch, $key, $val);
            }
        }
        // Save the outgoing XML. This doesn't quite match _sendHTTP as CURL
        // generates the headers, but having the XML is usually the most
        // important part for tracing/debugging.
        $incoming_payload = curl_exec($ch);
        if (!$incoming_payload) {
            $this->logger->error('pgi curl_exec error ' . curl_errno($ch) . ' ' . curl_error($ch));
            curl_close($ch);
            return false;
        }
        curl_close($ch);
        return $incoming_payload;
    }


    /**
     * Removes HTTP headers from response.
     *
     * @return boolean
     * @access private
     */
    function _parseResponse($incoming_payload)
    {
        if (!preg_match("/^(.*?)\r?\n\r?\n(.*)/s", $incoming_payload, $match)) {
            $this->logger->error('pgi Invalid HTTP Response');
            return false;
        }
        $this->response = $match[2];
        // Find the response error, some servers response with 500 for
        // SOAP faults.
        $this->_parseHeaders($match[1]);
        list(, $code, $msg) = sscanf($this->result_headers[0], '%s %s %s');
        unset($this->result_headers[0]);
        switch($code) {
            case 100: // Continue
                $incoming_payload = $match[2];
                return $this->_parseResponse($incoming_payload);
            case 200:
            case 202:
                $incoming_payload = trim($match[2]);
                if (!strlen($incoming_payload)) {
                    ///* Valid one-way message response.
                    return true;
                }
                break;
            case 400:
                $this->logger->error("HTTP Response $code Bad Request");
                return false;
            case 401:
                $this->logger->error("HTTP Response $code Authentication Failed");
                return false;
            case 403:
                $this->logger->error("HTTP Response $code Forbidden");
                return false;
            case 404:
                $this->logger->error("HTTP Response $code Not Found");
                return false;
            case 407:
                $this->logger->error("HTTP Response $code Proxy Authentication Required");
                return false;
            case 408:
                $this->logger->error("HTTP Response $code Request Timeout");
                return false;
            case 410:
                $this->logger->error("HTTP Response $code Gone");
                return false;
            default:
                if ($code >= 400 && $code < 500) {
                    $this->logger->info("HTTP Response $code Not Found, Server message: $msg");
                    return false;
                }
                break;
        }
        $this->_parseEncoding($match[1]);
        require_once "lib/EZLib/EZXML/EZXML.class.php";
        $xml_parser = new EZXML();
        $data = $xml_parser->openXML($this->response);
        return $data;
    }

    /**
     * Parses the headers.
     *
     * @param array $headers  The headers.
     */
    function _parseHeaders($headers)
    {
        /* Largely borrowed from HTTP_Request. */
        $this->result_headers = array();
        $headers = split("\r?\n", $headers);
        foreach ($headers as $value) {
            if (strpos($value,':') === false) {
                $this->result_headers[0] = $value;
                continue;
            }
            list($name, $value) = split(':', $value);
            $headername = strtolower($name);
            $headervalue = trim($value);
            $this->result_headers[$headername] = $headervalue;
        }
    }

    /**
     * Finds out what the encoding is.
     * Sets the object property accordingly.
     *
     * @access private
     * @param array $headers  Headers.
     */
    function _parseEncoding($headers)
    {
        $h = stristr($headers, 'Content-Type');
        preg_match_all('/^Content-Type:\s*(.*)$/im', $h, $ct, PREG_SET_ORDER);
        $n = count($ct);
        $ct = $ct[$n - 1];

        // Strip the string of \r.
        $this->result_content_type = str_replace("\r", '', $ct[1]);

        if (preg_match('/(.*?)(?:;\s?charset=)(.*)/i',
                       $this->result_content_type,
                       $m)) {
            $this->result_content_type = $m[1];
            if (count($m) > 2) {
                $enc = strtoupper(str_replace('"', '', $m[2]));
                if (in_array($enc, $this->_encodings)) {
                    $this->result_encoding = $enc;
                }
            }
        }

        // Deal with broken servers that don't set content type on faults.
        if (!$this->result_content_type) {
            $this->result_content_type = 'text/xml';
        }
    }
}
?>
