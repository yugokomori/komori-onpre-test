<?php 
require_once("classes/mcu/config/McuConfigProxy.php");

require_once(dirname(__FILE__)."/../../logic/FMSNetConnectionController.php");
require_once(dirname(__FILE__)."/../../logic/MosaicController.php");

require_once(dirname(__FILE__)."/../../model/ConferenceProxy.php");
require_once(dirname(__FILE__)."/../../model/MeetingProxy.php");
require_once(dirname(__FILE__)."/../../model/MCUControllerProxy.php");
require_once(dirname(__FILE__)."/../../model/do/BfcpDocumentSharingStatus.php");

class BfcpDocumentSharingStatusController {
	private $logger;
	private $configProxy;
	private $requestProxy;
	
	// model
	private $conferenceProxy;
	private $mcuControllerProxy;
	private $meetingProxy;
	
	function __construct($model){
		$this->configProxy	= new McuConfigProxy();
		
		$this->logger = EZLogger2::getInstance();	
		$this->requestProxy			= $model;

		$this->conferenceProxy		= ConferenceProxy::getInstance();
		$this->mcuControllerProxy	= MCUControllerProxy::getInstance();
		$this->meetingProxy			= MeetingProxy::getInstance();
		
		$this->mosaicController = new MosaicController();
		
		$this->initializeRecord();
	}
	
	private function initializeRecord() {
		// records
		// conference record.
		$this->conferenceProxy->setActiveConferenceByConfIdThrowable($this->requestProxy->confId);
			
		// meeting record.
		$this->meetingProxy->setActiveMeetingRecordByMeetingKeyThrowable($this->conferenceProxy->activeConferenceRecord["meeting_key"]);
		
		// room record
		$this->meetingProxy->setRoomRecordByRoomKeyThrowable($this->meetingProxy->activeMeetingRecord["room_key"]);
		
		// ives_setting record
		$this->meetingProxy->setSettingRecordByRoomKey($this->meetingProxy->activeMeetingRecord["room_key"]);
	}

	public function execute() {
		try {
			switch ($this->requestProxy->status) {
				case BfcpDocumentSharingStatus::$WAITING_ACCEPT:
					$this->receiveBFCPStatus();
					break;
					
				case BfcpDocumentSharingStatus::$ACTIVE:
					$this->startBfcpDocumentSharing();
					break;
					
				case BfcpDocumentSharingStatus::$NONE:
				case BfcpDocumentSharingStatus::$FAILED:
				default:
					$this->stopBfcpDocumentSharing();
					break;
			}
		}
		catch (Exception $e) {
			
		}
	}
	
	/**
	 * <p>
	 * 
	 * </p>
	 */
	private function receiveBFCPStatus() {
		$this->fmsController = new FMSNetConnectionController($this->meetingProxy->activeMeetingRecord);
		$this->fmsController->connect();
		$this->fmsController->noticeBFCPStatus(true, $this->requestProxy->partId);
		
		$this->mcuControllerProxy->acceptDocumentSharing($this->requestProxy->confId, $this->requestProxy->partId);
	}
	
	/**
	 * 
	 */
	private function startBfcpDocumentSharing() {
		$this->mcuControllerProxy->startRenderingInSlot($this->requestProxy->confId);

		$this->mcuControllerProxy->setDocSharingInMosaict($this->requestProxy->confId);
		
		$obj = CompositionCreater::createBFCPComposition($this->requestProxy->confId);
		$obj->run();
	}
	
	/**
	 * 
	 */
	private function stopBfcpDocumentSharing() {
		$this->mcuControllerProxy->stopRenderingInSlot($this->requestProxy->confId);
		
		$this->fmsController = new FMSNetConnectionController($this->meetingProxy->activeMeetingRecord);
		$this->fmsController->connect();
		$this->fmsController->noticeBFCPStatus(false);
		
		$obj = CompositionCreater::createByConfId($this->requestProxy->confId);
		$obj->run();
		
		// control vad slot
		/*   stopDocSharingメソッドの後に下記を実行するため、ここでは必要ない。
		if ($this->mosaicController->isActivationForDefaultMosaic($this->requestProxy->confId))
			$this->mcuControllerProxy->setMosaicSlot($this->mosaicController->getDefaultMosaicSlotParamsToActivateVadSlot($this->requestProxy->confId));
		
		if ($this->mosaicController->isActivationForFlashMosaic($this->requestProxy->confId))
			$this->mcuControllerProxy->setMosaicSlot($this->mosaicController->getFlashMosaicSlotParams($this->requestProxy->confId));
		
		if ($this->mosaicController->isActivationForMobileMosaic($this->requestProxy->confId))
			$this->mcuControllerProxy->setMosaicSlot($this->mosaicController->getMobileMosaicSlotParamsToActivateVadSlot($this->requestProxy->confId));
			*/
	}
}