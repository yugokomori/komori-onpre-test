<?php
/*
 * Created on 2008/02/14 To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once ("classes/N2MY_Api.class.php");
require_once ("classes/N2MY_Reservation.class.php");
require_once("classes/dbi/reservation_user.dbi.php");
require_once("classes/dbi/reservation_extend.dbi.php");
require_once ("config/config.inc.php");

class N2MY_Meeting_Resevation_API extends N2MY_Api
{

    var $_err = null;

    var $_ssl_mode = null;

    /**
     * 初期化
     * @param
     * @return
     */
    function init ()
    {}

    /**
     * 認証処理
     * @param
     * @return
     */
    function auth ()
    {
        $this->checkAuthorization();
        $this->objReservation = new N2MY_Reservation($this->get_dsn(), $this->session->get("dsn_key"));
        $this->objReservationUser = new ReservationUserTable($this->get_dsn());
    }

  /**
  * 予約延長
     * @param string  reservation_id ルームキー
     * @param string member_id 会議名 (デフォルト null)
     * @param datetime extend_minute 検索開始日 (デフォルト 0)
     * @param datetime extend_seat 	 検索終了日 (デフォルト 0)
     * @return string $output 会議キー、予約キー、会議名、開始時間、終了時間をxmlで出力
  */
  function action_extend (){
        $request = $this->request->getAll();
        $this->logger2->info($request);
        $reservation_id = $this->request->get("reservation_id", "");
        $member_id = $this->request->get("member_id", "");
        $extend_minute = $this->request->get("extend_minute", "");
        $extend_seat = $this->request->get("extend_seat", "");
        $extend_videoconference = $this->request->get("extend_videoconference", "0");
        $room_infos = $this->session->get("room_info");
        // バリデーション
        $rules = array(
                "extend_minute" => array(
                        "integer" => true,
                        "maxval"  => 121,
                ),
                "extend_seat" => array(
                        "integer" => true
                ),
                "extend_videoconference" => array(
                        "allow" => array("0","1")
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR",$this->get_error_info($err_obj));
        }
        $reservation_data = $this->objReservation->getDetail($reservation_id);

        if($extend_seat == ""){
            $extend_seat = 0;
        }
        if($extend_minute == ""){
            $extend_minute = 0;
        }

        if (!$reservation_data) {
            //エラー処理
             $err_obj->set_error(API_RESERVATION_ID,"RESERVATION_SESSION_INVALID");
             return $this->display_error(0, "PARAMETER_ERROR",$this->get_error_info($err_obj));
        }
        $room_info = $room_infos[$reservation_data["info"]["room_key"]];
        $obj_reservation_extend = new ReservationExtendTable($this->get_dsn());
        $where = "reservation_session_id = '" .mysql_real_escape_string($reservation_id) . "'";
        $sort = array(
            "extend_endtime" => "DESC"
        );
        $extend_infos = $obj_reservation_extend->getRowsAssoc($where , $sort );

        // 開催中かどうか判断
        $now = strtotime(date("Y-m-d H:i:s"));
        $meeting_start_time = strtotime($reservation_data["info"]["reservation_starttime_time_zone_9"]);
        $meeting_end_time = strtotime($reservation_data["info"]["reservation_endtime_time_zone_9"]);
        // 拡張データがあれば拡張データの終了日時を取得
        if($extend_infos){
            $meeting_end_time = strtotime($extend_infos[0]["extend_endtime"]);
            // 拡張合計人数も確認
            $extend_seat_all = 0;
            foreach($extend_infos as $extend_info){
                $extend_seat_all = $extend_seat_all + $extend_info["extend_seat"];
            }
            if(($extend_seat) >= 3){
                // 拡張人数は二人まで
                $err_obj->set_error("extend_seat","EXTEND_SEAT_OVER");
                return $this->display_error(0, "PARAMETER_ERROR",$this->get_error_info($err_obj));
            }

        }
        // 開催中じゃなかったらエラー
        if($now < $meeting_start_time || $now > $meeting_end_time){
            $err_obj->set_error(API_RESERVATION_ID,"RESERVATION_SESSION_INVALID");
            return $this->display_error(0, "PARAMETER_ERROR",$this->get_error_info($err_obj));
        }
        // member確認
        require_once 'classes/dbi/member.dbi.php';
        $obj_Member = new MemberTable($this->get_dsn());
        $where = "member_status = 0 AND member_id LIKE '" . mysql_real_escape_string($member_id) ."'";

        $member_info = $obj_Member->getRow($where);
        if(!$member_info){
            $err_obj->set_error("member_id","MEMBER_ID_INVALID");
            return $this->display_error(0, "PARAMETER_ERROR",$this->get_error_info($err_obj));
        }
        $member_key = $member_info["member_key"];

        // 延長データがあるとき
        if($extend_infos){
            if($reservation_data["info"]["reservation_endtime_time_zone_9"] > $extend_infos[0]["extend_endtime"]){
                $extend_endtime = $reservation_data["info"]["reservation_endtime_time_zone_9"];
            }else{
                $extend_endtime = $extend_infos[0]["extend_endtime"];
            }
            if($extend_seat > 0 || $extend_videoconference == 1){
                //終了時間を確認　人数拡張の場合は即時人数拡張
                $extend_starttime = $this->_step_detatime(15);
            }else{
               // 延長開始時間を確認
                if($reservation_data["info"]["reservation_endtime_time_zone_9"] > $extend_infos[0]["extend_endtime"]){
                    $extend_starttime = $reservation_data["info"]["reservation_endtime_time_zone_9"];
                }else{
                    $extend_starttime = $extend_infos[0]["extend_endtime"];
                }

            }
            $extend_endtime = date("Y-m-d H:i:s" , strtotime($extend_endtime) + ($extend_minute * 60));

        }else{
            // 人数拡張の場合は即時人数拡張
            if($extend_seat > 0 || $extend_videoconference == 1){
                $extend_starttime = $this->_step_detatime(15);
            }else{
                $extend_starttime = $reservation_data["info"]["reservation_endtime_time_zone_9"];
            }
            $extend_endtime = date("Y-m-d H:i:s" , strtotime($reservation_data["info"]["reservation_endtime_time_zone_9"]) + ($extend_minute * 60));
            // 初拡張の場合は延長フラグを変更する
            $reservation_updata_data["reservation_extend_flg"] = 1;
        }
        // 新規延長追加
        $data = array(
            'reservation_session_id' => mysql_real_escape_string($reservation_id),
            'member_key' => $member_key ,
            'extend_minute' => $extend_minute,
            'extend_seat' => $extend_seat,
            'extend_videoconference' => $extend_videoconference,
            'extend_starttime' => $extend_starttime,
            'extend_endtime' => $extend_endtime,
        );

        //延長する時間に後ろの会議の確認
        require_once ("classes/dbi/reservation.dbi.php");
        $obj_Reservation     = new ReservationTable($this->get_dsn());
        $after_reservation_where = "room_key = '".$reservation_data["info"]["room_key"]."' AND '". $extend_starttime . "' <= reservation_starttime AND reservation_starttime < '". $extend_endtime . "'";
        $after_reservations = $obj_Reservation->getRowsAssoc($after_reservation_where);
        $this->logger2->info($after_reservations);

        // 人数拡張と時間拡張が同時にくることはないので人数拡張時は移動処理を行わない
        if($after_reservations && $extend_seat == 0 && $extend_videoconference == 0){

            // media_mixer_key 取得
            require_once("classes/dbi/ives_setting.dbi.php");
            $ivesSettingTable = new IvesSettingTable($this->get_dsn());
            $where = "room_key = '" . addslashes($room_info["room_info"]['room_key']) . "' AND is_deleted = 0";
            $ives_setting = $ivesSettingTable->getRow($where);
            $media_mixer_key = $ives_setting["media_mixer_key"];

            // 会議移動処理
            require_once 'classes/dbi/room.dbi.php';
            $obj_Room = new RoomTable($this->get_dsn());
            $room_where = "max_seat = " . $room_info["room_info"]["max_seat"] . " AND user_key = " . $room_info["room_info"]["user_key"] . " AND is_extend_room = 1 AND room_status = 1 ";
            $room_sort = array(
                    "room_sort" => "asc"
                    );
            //$extend_rooms = $obj_Room->getRowsAssoc($room_where,$room_sort);
            $extend_rooms = $obj_Room->get_extend_room($room_info["room_info"]["max_seat"], $room_info["room_info"]["user_key"], $media_mixer_key);

            require_once 'classes/dbi/meeting.dbi.php';
            $obj_Meeting = new MeetingTable($this->get_dsn());
            foreach ($after_reservations as $after_reservation){
                // 明示的にループを初期化
                $i = 0;
                $count = count($extend_rooms);
                $reservation_move = 0;
                for($i = 0; $i < $count; $i++){
                    $extexd_res_where = "room_key = '".addslashes($extend_rooms[$i]["room_key"])."'" .
                    " AND reservation_status = 1" .
                    " AND reservation_extend_endtime != '".$after_reservation["reservation_starttime"]."'" .
                    " AND reservation_starttime != '".$after_reservation["reservation_extend_endtime"]."'" .
                    " AND (" .
                    "( reservation_starttime between '".$after_reservation["reservation_starttime"]."' AND '".$after_reservation["reservation_extend_endtime"]."' )" .
                    " OR ( reservation_extend_endtime between '".$after_reservation["reservation_starttime"]."' AND '".$after_reservation["reservation_extend_endtime"]."' )" .
                    " OR ( '".$after_reservation["reservation_starttime"]."' BETWEEN reservation_starttime AND reservation_extend_endtime )" .
                    " OR ( '".$after_reservation["reservation_extend_endtime"]."' BETWEEN reservation_starttime AND reservation_extend_endtime ) )";

                    // 退避部屋に同時間帯に会議があるかチェック
                    $extexd_res = $obj_Reservation->getRowsAssoc($extexd_res_where);
                    if(!$extexd_res){
                        // この部屋に予約を退避させる
                        $this->logger2->info($extend_rooms[$i]);
                        $this->logger2->info($after_reservation);
                        // 予約情報の変更
                        $meeting_ticket = $after_reservation["meeting_key"];
                        $new_meeting_ticket = str_replace($after_reservation["room_key"],$extend_rooms[$i]["room_key"],$after_reservation["meeting_key"]);
                        $this->logger2->info($new_meeting_ticket);
                        $new_reservation = array(
                                "room_key"    => $extend_rooms[$i]["room_key"],
                                "meeting_key" => $new_meeting_ticket
                                );
                        $obj_Reservation->update($new_reservation , "reservation_key = " . $after_reservation["reservation_key"]);
                        //meeting情報の変更
                        $new_meeting = array(
                                "room_key"         => $extend_rooms[$i]["room_key"],
                                "meeting_ticket"   => $new_meeting_ticket,
                                "meeting_tag"      => $extend_rooms[$i]["room_key"],
                                "meeting_room_name"=> $extend_rooms[$i]["room_name"]
                                );
                        $obj_Meeting->update($new_meeting, "meeting_ticket = '" .$meeting_ticket."'");
                        $reservation_move = 1;
                        break;
                    }
                }
                if(!$reservation_move){
                    //移動エラー
                    $err_obj->set_error("move error","MOVE FAILD");
                    return $this->display_error(0, "PARAMETER_ERROR",$this->get_error_info($err_obj));
                }
            }

        }

        // reservationの延長時間も変更
        $where = "reservation_session = '" .mysql_real_escape_string($reservation_id) . "'";
        $reservation_updata_data["reservation_extend_endtime"] = $extend_endtime;
        $obj_Reservation->update($reservation_updata_data, $where);

        $return_data = array(
            "extend_seat" => $extend_seat,
            "total_seat"  => $room_info["room_info"]["max_seat"] + $extend_seat + $extend_seat_all,
            "extend_videoconference" => $extend_videoconference,
            "reservation_extend_start_date"  => strtotime($extend_starttime),
            "reservation_extend_end_date"  => strtotime($extend_endtime),
        );
        // 延長追加
        $obj_reservation_extend->add($data);
        return $this->output(($return_data));
    }

    function _step_detatime($stepTime = 15){
        $now_hour = date("Y-m-d H:00");
        for ($i = 0; $i < (24*4); $i++) {
            $stamp = strtotime($now_hour) + ($i * $stepTime * 60);
            $dispTime = date("Y-m-d H:i:00",$stamp);
            $test[$i] = $dispTime;
            $now = date("Y-m-d H:i:00");
             if ($now >= $dispTime and (strtotime($dispTime) + $stepTime*60) > strtotime($now)) {
                return $dispTime;

            }
        }
    }

    /**
     * 予約一覧（検索）
     * @param string room_key ルームキー
     * @param string meeting_name 会議名 (デフォルト null)
     * @param datetime start_limit 検索開始日 (デフォルト null)
     * @param datetime end_limit 検索終了日 (デフォルト null)
     * @param string reservation_name
     * @param string member_id
     * @param string sort_key ソート基準 (デフォルト 予約会議開始時間)
     * @param string sort_type 降順、昇順 (デフォルト 昇順)
     * @param int limit 表示件数 (デフォルト null)
     * @param int offset 開始件数 (デフォルト null)
     * @return string $output 会議キー、予約キー、会議名、開始時間、終了時間をxmlで出力
     */
    function action_get_list ()
    {
        $user_info = $this->session->get("user_info");
        $room_info = $this->session->get("room_info");
        $request = $this->request->getAll();
        $room_key = $this->request->get(API_ROOM_ID, "");
        $reservation_name = $this->request->get("reservation_name", "");
        $member_id = $this->request->get("member_id");
        $member_name = $this->request->get("member_name");
        $_wk_start_limit = $this->request->get("start_limit");
        $_wk_end_limit = $this->request->get("end_limit");
        $_wk_updatetime = $this->request->get("updatetime");
        $limit = $this->request->get("limit", N2MY_API_GET_LIST_LIMIT);
        $page = $this->request->get("page", 1);
        $sort_key = $this->request->get("sort_key", "reservation_starttime");
        $sort_type = $this->request->get("sort_type", "asc");
//         if($sort_key == "member_name") {
//             $sort_key = "reservation_starttime";
//             if($sort_type == "desc") {
//                 $sort_by_member_name = SORT_DESC;
//             } else {
//                 $sort_by_member_name = SORT_ASC;
//             }
//         }
        // タイムスタンプ変換
        $server_time_zone = $this->config->get("GLOBAL", "time_zone",
                N2MY_SERVER_TIMEZONE); // このサーバーのタイムゾーン
        $user_time_zone = $this->session->get("time_zone"); // セッションの値から取得
        if ($_wk_start_limit) {
            if (is_numeric($_wk_start_limit)) {
                $_ts_local = $_wk_start_limit;
            } else {
                $_ts_local = EZDate::getLocateTime($_wk_start_limit, $server_time_zone,
                        $user_time_zone);
            }
            $start_limit = EZDate::getZoneDate("Y-m-d H:i:s", $server_time_zone,
                    $_ts_local);
        }
        $this->logger2->debug(
                array(
                        $_wk_start_limit,
                        $user_time_zone,
                        $server_time_zone,
                        $start_limit,
                        date("Y-m-d H:i:s", $_ts_local)
                ));
        if ($_wk_end_limit) {
            if (is_numeric($_wk_end_limit)) {
                $_ts_local = $_wk_end_limit;
            } else {
                $_ts_local = EZDate::getLocateTime($_wk_end_limit, $server_time_zone,
                        $user_time_zone);
            }
            $end_limit = EZDate::getZoneDate("Y-m-d H:i:s", $server_time_zone,
                    $_ts_local);
        }
        $this->logger2->debug(
                array(
                        $_wk_end_limit,
                        $user_time_zone,
                        $server_time_zone,
                        $end_limit,
                        date("Y-m-d H:i:s", $_ts_local)
                ));
        if ($_wk_updatetime) {
            if (is_numeric($_wk_updatetime)) {
                $_ts_local = $_wk_updatetime;
            } else {
                $_ts_local = EZDate::getLocateTime($_wk_updatetime,
                        $user_time_zone, $server_time_zone);
            }
            $updatetime = EZDate::getZoneDate("Y-m-d H:i:s", $user_time_zone,
                    $_ts_local);
        }
        $this->logger2->debug(
                array(
                        $_wk_updatetime,
                        $user_time_zone,
                        $server_time_zone,
                        $updatetime,
                        date("Y-m-d H:i:s", $_ts_local)
                ));
        $request["start_limit"] = $start_limit;
        $request["end_limit"] = $end_limit;
        $request["updatetime"] = $updatetime;

        $options = array(
                "user_key" => $user_info["user_key"],
                "start_time" => $start_limit,
                "room_key" => $room_key,
                "end_time" => $end_limit,
                "member_id" => $member_id,
                "member_name" => $member_name,
                "reservation_name" => $reservation_name,
                "updatetime" => $updatetime,
                "limit" => $limit,
                "page" => $page,
                "sort_key" => $sort_key,
                "sort_type" => $sort_type
        );
//         $where = "member_status = 0 AND member_id LIKE '%" .
//                  addslashes($member_id) . "%' AND user_key = '" .
//                  addslashes($user_info['user_key']) . "'";
//         $members = $obj_Member->getRowsAssoc($where);
//         $member_id_list = array();
//         $member_key_list = array();
//         foreach ($members as $member) {
//             $member_id_list[] = $member['member_id'];
//             $member_key_list[$member['member_key']] = $member['member_id'];
//         }
        $rules = array(
                API_ROOM_ID => array(
                        "allow" => array_keys($room_info)
                ),
                "start_limit" => array(
                        "datetime" => true
                ),
                "end_limit" => array(
                        "datetime" => true
                ),
                "updatetime" => array(
                        "datetime" => true
                ),
                "limit" => array(
                        "integer" => true
                ),
                "page" => array(
                        "integer" => true
                ),
                "reservation_name" => array(
                        "string" => true
                ),
                "sort_key" => array(
                        "allow" => array(
                                "reservation_name",
                                "reservation_starttime",
                                "member_name"
                        )
                ),
                "sort_type" => array(
                        "allow" => array(
                                "asc",
                                "desc"
                        )
                ),
                "output_type" => array(
                        "allow" => $this->get_output_type_list()
                )
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR",
                    $this->get_error_info($err_obj));
        } else {
            $offset = ($limit * ($page - 1));
            $options["offset"] = $offset;
            unset($options["page"]);
            $reservations = $this->get_reservation_list($options,
                    $server_time_zone);
            $reservationsList = array();
            if ($reservations) {
            	require_once 'classes/dbi/member.dbi.php';
            	$obj_Member = new MemberTable($this->get_dsn());
            	require_once("classes/dbi/ives_setting.dbi.php");
            	$ivesSettingTable = new IvesSettingTable($this->get_dsn ());
            	$member_list = array();
            	$ivesSetting_list = array();
                // 一覧表示
                foreach ($reservations as $_key => $_val) {
                    if(!$member_list[$reservations[$_key]["member_key"]]) {
                        $where = "member_status = 0 AND member_key = '".addslashes($reservations[$_key]["member_key"])."'";
                        $member_info = $obj_Member->getRow($where);
                        $member_list[$reservations[$_key]["member_key"]] = $member_info;
                    }
                    $member_info = $member_list[$reservations[$_key]["member_key"]];
                    if(!$ivesSetting_list[$reservations [$_key] ["room_key"]]) {
                    	$ives_setting = $ivesSettingTable->findLatestByRoomKey($reservations [$_key] ["room_key"]);
                    	$ivesSetting_list[$reservations [$_key] ["room_key"]] = $ives_setting;
                    }
                    $ives_setting = $ivesSetting_list[$reservations [$_key] ["room_key"]];
                    $reservations_list = array(
                            'reservation_name' => $reservations[$_key]["reservation_name"],
                            'reservation_pw' =>  ($reservations[$_key]["reservation_pw"]) ? 1 : 0,
                            'max_seat' => $reservations[$_key]["max_seat"],
                            'status' => $reservations[$_key]["status"],
                            'room_id' => $reservations[$_key]["room_key"],
                            'member_id' => $member_info["member_id"],
                            'member_name' => $member_info["member_name"],
                            API_RESERVATION_ID => $reservations[$_key]["reservation_session"],
                            API_MEETING_ID => $this->ticket_to_session(
                                    $reservations[$_key]["meeting_key"]),
                            'media_server_key' => $ives_setting["media_mixer_key"] ? $ives_setting["media_mixer_key"] : "",
                            API_RESERVATION_START => $reservations[$_key]["reservation_starttime"],
                            API_RESERVATION_END => $reservations[$_key]["reservation_endtime"],
                            "reservation_registtime" => $reservations[$_key]["reservation_registtime"],
                            "reservation_updatetime" => $reservations[$_key]["reservation_updatetime"],
                            "payment_type" => $reservations[$_key]["payment_type"],
                            "is_publish" => $reservations[$_key]["is_publish"],
                    );
                    if($reservations[$_key]["reservation_pw"] === "0"){
                        $reservations_list['reservation_pw'] = 1;
                    }
                    if($reservations[$_key]["reservation_extend_flg"]){
                        // 予約拡張取得
                        $obj_reservation_extend = new ReservationExtendTable($this->get_dsn());
                        $where = "reservation_session_id = '" .mysql_real_escape_string($reservations[$_key]["reservation_session"]) . "'";
                        $sort = array(
                            "extend_endtime" => "DESC"
                        );
                        $extend_infos = $obj_reservation_extend->getRowsAssoc($where , $sort );
                        $reservations_list[API_RESERVATION_END] = strtotime($extend_infos[0]["extend_endtime"]);
                        $extend_seat_all = 0;
                        foreach($extend_infos as $extend_info){
                            $extend_seat_all = $extend_seat_all + $extend_info["extend_seat"];
                        }
                        $reservations_list['max_seat'] = $reservations_list['max_seat'] + $extend_seat_all;
                    }
                    $reservationsList[] = $reservations_list;
                }
            }
//             if($sort_by_member_name) {
//                 $member_name_sort = array();
//                 foreach ($reservationsList as $key => $row)
//                 {
//                     $member_name_sort[$key] = $row['member_name'];
//                 }
//                 array_multisort($member_name_sort, $sort_by_member_name , $reservationsList);
//             }
            $data = array(
                    "count" => count($reservationsList),
                    "reservations" => array(
                            "reservation" => $reservationsList
                    )
            );
            $this->logger2->debug($data);
            return $this->output($data);
        }
    }

    function action_get_list_mini ()
    {
    	$user_info = $this->session->get("user_info");
        $room_info = $this->session->get("room_info");
        $request = $this->request->getAll();
        $room_key = $this->request->get(API_ROOM_ID, "");
        $reservation_name = $this->request->get("reservation_name", "");
        $member_id = $this->request->get("member_id");
        $member_name = $this->request->get("member_name");
        $_wk_start_limit = $this->request->get("start_limit");
        $_wk_end_limit = $this->request->get("end_limit");
        $_wk_updatetime = $this->request->get("updatetime");
        $limit = $this->request->get("limit", N2MY_API_GET_LIST_LIMIT);
        $page = $this->request->get("page", 1);
        $sort_key = $this->request->get("sort_key", "reservation_starttime");
        $sort_type = $this->request->get("sort_type", "asc");

        $server_time_zone = $this->config->get("GLOBAL", "time_zone",
                N2MY_SERVER_TIMEZONE); // このサーバーのタイムゾーン
        $user_time_zone = $this->session->get("time_zone"); // セッションの値から取得
        if ($_wk_start_limit) {
            if (is_numeric($_wk_start_limit)) {
                $_ts_local = $_wk_start_limit;
            } else {
                $_ts_local = EZDate::getLocateTime($_wk_start_limit, $server_time_zone,
                        $user_time_zone);
            }
            $start_limit = EZDate::getZoneDate("Y-m-d H:i:s", $server_time_zone,
                    $_ts_local);
        }
        $this->logger2->debug(
                array(
                        $_wk_start_limit,
                        $user_time_zone,
                        $server_time_zone,
                        $start_limit,
                        date("Y-m-d H:i:s", $_ts_local)
                ));
        if ($_wk_end_limit) {
            if (is_numeric($_wk_end_limit)) {
                $_ts_local = $_wk_end_limit;
            } else {
                $_ts_local = EZDate::getLocateTime($_wk_end_limit, $server_time_zone,
                        $user_time_zone);
            }
            $end_limit = EZDate::getZoneDate("Y-m-d H:i:s", $server_time_zone,
                    $_ts_local);
        }
        $this->logger2->debug(
                array(
                        $_wk_end_limit,
                        $user_time_zone,
                        $server_time_zone,
                        $end_limit,
                        date("Y-m-d H:i:s", $_ts_local)
                ));
        if ($_wk_updatetime) {
            if (is_numeric($_wk_updatetime)) {
                $_ts_local = $_wk_updatetime;
            } else {
                $_ts_local = EZDate::getLocateTime($_wk_updatetime,
                        $user_time_zone, $server_time_zone);
            }
            $updatetime = EZDate::getZoneDate("Y-m-d H:i:s", $user_time_zone,
                    $_ts_local);
        }
        $this->logger2->debug(
                array(
                        $_wk_updatetime,
                        $user_time_zone,
                        $server_time_zone,
                        $updatetime,
                        date("Y-m-d H:i:s", $_ts_local)
                ));
        $request["start_limit"] = $start_limit;
        $request["end_limit"] = $end_limit;
        $request["updatetime"] = $updatetime;

        $options = array(
                "user_key" => $user_info["user_key"],
                "start_time" => $start_limit,
                "room_key" => $room_key,
                "end_time" => $end_limit,
                "member_id" => $member_id,
                "member_name" => $member_name,
                "reservation_name" => $reservation_name,
                "updatetime" => $updatetime,
                "limit" => $limit,
                "page" => $page,
                "sort_key" => $sort_key,
                "sort_type" => $sort_type
        );

        $rules = array(
                API_ROOM_ID => array(
                        "allow" => array_keys($room_info)
                ),
                "start_limit" => array(
                        "datetime" => true
                ),
                "end_limit" => array(
                        "datetime" => true
                ),
                "updatetime" => array(
                        "datetime" => true
                ),
                "limit" => array(
                        "integer" => true
                ),
                "page" => array(
                        "integer" => true
                ),
                "reservation_name" => array(
                        "string" => true
                ),
                "sort_key" => array(
                        "allow" => array(
                                "reservation_name",
                                "reservation_starttime",
                                "member_name"
                        )
                ),
                "sort_type" => array(
                        "allow" => array(
                                "asc",
                                "desc"
                        )
                ),
                "output_type" => array(
                        "allow" => $this->get_output_type_list()
                )
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR",
                    $this->get_error_info($err_obj));
        } else {
            $offset = ($limit * ($page - 1));
            $options["offset"] = $offset;
            unset($options["page"]);
            $reservations = $this->get_reservation_list($options,
                    $server_time_zone);
            $reservationsList = array();
            if ($reservations) {
                require_once 'classes/dbi/member.dbi.php';
                $obj_Member = new MemberTable($this->get_dsn());
                require_once("classes/dbi/ives_setting.dbi.php");
                $ivesSettingTable = new IvesSettingTable($this->get_dsn ());
                $member_list = array();
                $ivesSetting_list = array();
                // 一覧表示
                foreach ($reservations as $_key => $_val) {
                    $reservations_list = array(
                            'reservation_name' => $reservations[$_key]["reservation_name"],
                            'status' => $reservations[$_key]["status"],
                            'room_id' => $reservations[$_key]["room_key"],
                            API_RESERVATION_ID => $reservations[$_key]["reservation_session"],
                            //API_MEETING_ID => $this->ticket_to_session($reservations[$_key]["meeting_key"]),
                            API_RESERVATION_START => $reservations[$_key]["reservation_starttime"],
                            API_RESERVATION_END => $reservations[$_key]["reservation_endtime"],
                            "payment_type" => $reservations[$_key]["payment_type"],
                            "is_publish" => $reservations[$_key]["is_publish"],
                    );
                    // if($reservations[$_key]["reservation_extend_flg"]){
                    //     // 予約拡張取得
                    //     $obj_reservation_extend = new ReservationExtendTable($this->get_dsn());
                    //     $where = "reservation_session_id = '" .mysql_real_escape_string($reservations[$_key]["reservation_session"]) . "'";
                    //     $sort = array(
                    //         "extend_endtime" => "DESC"
                    //     );
                    //     $extend_infos = $obj_reservation_extend->getRowsAssoc($where , $sort );
                    //     $reservations_list[API_RESERVATION_END] = strtotime($extend_infos[0]["extend_endtime"]);
                    //     $extend_seat_all = 0;
                    //     foreach($extend_infos as $extend_info){
                    //         $extend_seat_all = $extend_seat_all + $extend_info["extend_seat"];
                    //     }
                    //     $reservations_list['max_seat'] = $reservations_list['max_seat'] + $extend_seat_all;
                    // }
                    $reservationsList[] = $reservations_list;
                }
            }
            $data = array(
                    "count" => count($reservationsList),
                    "reservations" => array(
                            "reservation" => $reservationsList
                    )
            );
            $this->logger2->debug($data);
            return $this->output($data);
        }
    }

    /**
     * 予約詳細一覧（検索）
     * @param string room_key ルームキー
     * @param string meeting_name 会議名 (デフォルト null)
     * @param datetime start_limit 検索開始日 (デフォルト null)
     * @param datetime end_limit 検索終了日 (デフォルト null)
     * @param string reservation_name
     * @param string member_id
     * @param string sort_key ソート基準 (デフォルト 予約会議開始時間)
     * @param string sort_type 降順、昇順 (デフォルト 昇順)
     * @param int limit 表示件数 (デフォルト null)
     * @param int offset 開始件数 (デフォルト null)
     * @return string $output 会議キー、予約キー、会議名、開始時間、終了時間をxmlで出力
     */
    function action_get_detail_list ()
    {
        $user_info = $this->session->get("user_info");
        $room_info = $this->session->get("room_info");
        $request = $this->request->getAll();
        $room_key = $this->request->get(API_ROOM_ID, "");
        $reservation_name = $this->request->get("reservation_name", "");
        $member_id = $this->request->get("member_id");
        $member_name = $this->request->get("member_name");
        $_wk_start_limit = $this->request->get("start_limit");
        $_wk_end_limit = $this->request->get("end_limit");
        $_wk_updatetime = $this->request->get("updatetime");
        $limit = $this->request->get("limit", N2MY_API_GET_LIST_LIMIT);
        $page = $this->request->get("page", 1);
        $sort_key = $this->request->get("sort_key", "reservation_starttime");
        $sort_type = $this->request->get("sort_type", "asc");
        // タイムスタンプ変換
        $server_time_zone = $this->config->get("GLOBAL", "time_zone",
                N2MY_SERVER_TIMEZONE); // このサーバーのタイムゾーン
        $user_time_zone = $this->session->get("time_zone"); // セッションの値から取得
        if ($_wk_start_limit) {
            if (is_numeric($_wk_start_limit)) {
                $_ts_local = $_wk_start_limit;
            } else {
                $_ts_local = EZDate::getLocateTime($_wk_start_limit, $server_time_zone,
                        $user_time_zone);
            }
            $start_limit = EZDate::getZoneDate("Y-m-d H:i:s", $server_time_zone,
                    $_ts_local);
        }
        $this->logger2->debug(
                array(
                        $_wk_start_limit,
                        $user_time_zone,
                        $server_time_zone,
                        $start_limit,
                        date("Y-m-d H:i:s", $_ts_local)
                ));
        if ($_wk_end_limit) {
            if (is_numeric($_wk_end_limit)) {
                $_ts_local = $_wk_end_limit;
            } else {
                $_ts_local = EZDate::getLocateTime($_wk_end_limit, $server_time_zone,
                        $user_time_zone);
            }
            $end_limit = EZDate::getZoneDate("Y-m-d H:i:s", $server_time_zone,
                    $_ts_local);
        }
        $this->logger2->debug(
                array(
                        $_wk_end_limit,
                        $user_time_zone,
                        $server_time_zone,
                        $end_limit,
                        date("Y-m-d H:i:s", $_ts_local)
                ));
        if ($_wk_updatetime) {
            if (is_numeric($_wk_updatetime)) {
                $_ts_local = $_wk_updatetime;
            } else {
                $_ts_local = EZDate::getLocateTime($_wk_updatetime,
                        $user_time_zone, $server_time_zone);
            }
            $updatetime = EZDate::getZoneDate("Y-m-d H:i:s", $user_time_zone,
                    $_ts_local);
        }
        $this->logger2->debug(
                array(
                        $_wk_updatetime,
                        $user_time_zone,
                        $server_time_zone,
                        $updatetime,
                        date("Y-m-d H:i:s", $_ts_local)
                ));
        $request["start_limit"] = $start_limit;
        $request["end_limit"] = $end_limit;
        $request["updatetime"] = $updatetime;

        $options = array(
                "user_key" => $user_info["user_key"],
                "start_time" => $start_limit,
                "room_key" => $room_key,
                "end_time" => $end_limit,
                "member_id" => $member_id,
                "member_name" => $member_name,
                "reservation_name" => $reservation_name,
                "updatetime" => $updatetime,
                "limit" => $limit,
                "page" => $page,
                "sort_key" => $sort_key,
                "sort_type" => $sort_type
        );
        require_once 'classes/dbi/member.dbi.php';
        $obj_Member = new MemberTable($this->get_dsn());
        $where = "member_status = 0 AND member_id LIKE '%" .
                 addslashes($member_id) . "%' AND user_key = '" .
                 addslashes($user_info['user_key']) . "'";
        $members = $obj_Member->getRowsAssoc($where);
        $member_key_list = array();
        foreach ($members as $member) {
            $member_id_list[] = $member['member_id'];
            $member_key_list[$member['member_key']] = $member['member_id'];
        }
        $rules = array(
                API_ROOM_ID => array(
                        "allow" => array_keys($room_info)
                ),
                "start_limit" => array(
                        "datetime" => true
                ),
                "end_limit" => array(
                        "datetime" => true
                ),
                "updatetime" => array(
                        "datetime" => true
                ),
                "limit" => array(
                        "integer" => true
                ),
                "page" => array(
                        "integer" => true
                ),
                "reservation_name" => array(
                        "string" => true
                ),
                "sort_key" => array(
                        "allow" => array(
                                "reservation_name",
                                "reservation_starttime"
                        )
                ),
                "sort_type" => array(
                        "allow" => array(
                                "asc",
                                "desc"
                        )
                ),
                "output_type" => array(
                        "allow" => $this->get_output_type_list()
                )
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR",
                    $this->get_error_info($err_obj));
        } else {
            $offset = ($limit * ($page - 1));
            $options["offset"] = $offset;
            unset($options["page"]);
            $reservations = $this->get_reservation_detail_list($options,
                    $server_time_zone);
            $reservationsList = array();
            if ($reservations) {
                require_once("classes/dbi/ives_setting.dbi.php");
                // 一覧表示
                $country_key = $this->session->get("country_key");
                foreach ($reservations as $_key => $_val) {
                    if($reservations[$_key]["member_key"] != $member_info["member_key"]) {
                        $where = "member_status = 0 AND member_key = '".addslashes($reservations[$_key]["member_key"])."'";
                        $member_info = $obj_Member->getRow($where);

                    }
                    $ivesSettingTable = new IvesSettingTable($this->get_dsn ());
                    $ives_setting = $ivesSettingTable->findLatestByRoomKey($reservations [$_key] ["room_key"]);
                    $reservations_list = array(
                            'reservation_name' => $reservations[$_key]["reservation_name"],
                            'reservation_pw' => ($reservations[$_key]["reservation_pw"]) ? 1 : 0,
                            'max_seat' => $reservations[$_key]["max_seat"],
                            'sender' => $reservations[$_key]["sender"],
                            'sender_mail' => $reservations[$_key]["sender_mail"],
                            'status' => $reservations[$_key]["status"],
                            'room_id' => $reservations[$_key]["room_key"],
                            'member_id' => $member_key_list[$reservations[$_key]["member_key"]],
                            'member_name' => $member_info["member_name"],
                            API_RESERVATION_ID => $reservations[$_key]["reservation_session"],
                            API_MEETING_ID => $this->ticket_to_session(
                                    $reservations[$_key]["meeting_key"]),
                            'media_server_key' => $ives_setting["media_mixer_key"] ? $ives_setting["media_mixer_key"] : "",
                            API_RESERVATION_START => $reservations[$_key]["reservation_starttime"],
                            API_RESERVATION_END => $reservations[$_key]["reservation_endtime"],
                            "reservation_registtime" => $reservations[$_key]["reservation_registtime"],
                            "reservation_updatetime" => $reservations[$_key]["reservation_updatetime"],
                            "payment_type" => $reservations[$_key]["payment_type"],
                            "is_publish" => $reservations[$_key]["is_publish"],
                    );
                    if($reservations[$_key]["reservation_pw"] === "0"){
                        $reservations_list["reservation_pw"] = 1;
                    }
                    foreach ($reservations[$_key]["guests"] as $_key => $guest) {
                        $guest_type = ($guest["type"] == 1) ? "audience" : "";
                        $guests[] = array(
                                'name' => $guest["name"],
                                'email' => $guest["email"],
                                'timezone' => $guest["timezone"],
                                'lang' => $guest["lang"],
                                API_GUEST_ID => $guest["r_user_session"],
                                'type' => $guest_type,
                                'invite_url' => N2MY_BASE_URL . "r/" .
                                         $guest["r_user_session"] . '&c=' . $country_key .
                                         '&lang=' . $guest["lang"]
                        );
                    }
                    $reservations_list["guests"]["guest"] = $guests;
                    $reservationsList[] = $reservations_list;
                }
            }
            $data = array(
                    "count" => count($reservationsList),
                    "reservations" => array(
                            "reservation" => $reservationsList
                    )
            );
            $this->logger2->debug($data);
            return $this->output($data);
        }
    }

    /**
     * 予約内容詳細（予約、招待者、事前資料）
     *
     * @param
     *            string room_key ルームキー
     * @param
     *            string reservation_session 予約セッション
     * @return string $output 予約キー、会議室名、会議名、開始時間、終了時間、パスワード、招待者、資料名をxmlで出力
     */
    function action_get_detail ()
    {
        $request = $this->request->getAll();
        $reservation_session = $this->request->get(API_RESERVATION_ID);
        $reservation_pw = $this->request->get("password");
        $time_zone = $this->session->get("timezone");
        $user_info = $this->session->get("user_info");
        $rules = array(
                API_RESERVATION_ID => array(
                        "required" => true
                ),
                "output_type" => array(
                        "allow" => $this->get_output_type_list()
                )
        );
        $err_obj = $this->error_check($request, $rules);
        if (! $reservation_data = $this->objReservation->getDetail(
                $reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID,
                    "RESERVATION_SESSION_INVALID");
        } else {
            $info = $reservation_data["info"];
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID,
                        "RESERVATION_SESSION_INVALID");
            }
        }
        require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
        if ( ($reservation_pw || $reservation_pw === "0") && ($info["reservation_pw"] || $info["reservation_pw"] === "0")  ) {
            if (($reservation_pw !== $info["reservation_pw"]) &&
                     ($reservation_pw !==
                     EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV,
                            $user_info["user_admin_password"]))) {
                $err_obj->set_error("password", "pw_equal");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR",
                    $this->get_error_info($err_obj));
        } else {
          $where = "reservation_key = ".$reservation_data["info"]["reservation_key"]. " AND r_organizer_flg = 1";
          $organizer_info           = $this->objReservationUser->getRow($where);
          $organizer                = array(
                            'organizer_name'         => "",
                            'organizer_email'        => "",
                            'organizer_timezone'     => "",
                            'organizer_lang'     	 => "");
          if($reservation_data["info"]["is_organizer_flg"]){
              $organizer        = array(
                                'organizer_name'         => $reservation_data["info"]["organizer_name"],
                                'organizer_email'        => $reservation_data["info"]["organizer_email"],
                                'organizer_timezone'     => $organizer_info["r_user_place"] ? $organizer_info["r_user_place"] : $reservation_data["info"]["reservation_place"],
                                'organizer_lang'     	 => $organizer_info["r_user_lang"] ? $organizer_info["r_user_lang"] : $reservation_data["info"]["sender_lang"]);
          }
          $reservation_data["organizer"] = $organizer;
            require_once ("classes/dbi/meeting.dbi.php");
            $objMeeting = new MeetingTable($this->get_dsn());
            $pin_cd = $objMeeting->getOne(
                    "meeting_ticket = '" .
                             addslashes(
                                    $reservation_data["info"]["meeting_key"]) .
                             "'", "pin_cd");
            $room_info = $this->session->get("room_info");
            require_once 'classes/dbi/member.dbi.php';
            $obj_Member = new MemberTable($this->get_dsn());
            $where = "member_status = 0 AND member_key = '".addslashes($reservation_data["info"]["member_key"])."'";
            $member_info = $obj_Member->getRow($where);
            require_once("classes/dbi/ives_setting.dbi.php");
            $ivesSettingTable = new IvesSettingTable($this->get_dsn ());
            $ives_setting = $ivesSettingTable->findLatestByRoomKey($reservation_data["info"]["room_key"]);
            $_room_info = $room_info[$reservation_data["info"]["room_key"]];
            $info = array(
                    'reservation_name' => $reservation_data["info"]["reservation_name"],
                    'reservation_pw' => ($reservation_data["info"]["reservation_pw"]) ? 1 : 0,
                    'max_seat' => $reservation_data["info"]["max_seat"],
                    'status' => $reservation_data["info"]["status"],
                    'payment_type' => $reservation_data["info"]["payment_type"],
                    "is_publish" => $reservation_data["info"]["is_publish"],
                    'member_id' =>$member_info['member_id'],
                    'member_name' =>$member_info['member_name'],
                    'sender' => $reservation_data["info"]["sender"],
                    'sender_mail' => $reservation_data["info"]["sender_mail"],
                    'sender_lang' => $reservation_data["info"]["sender_lang"],
                    'reservation_place' => $reservation_data["info"]["reservation_place"],
                    'mail_body' => $reservation_data["info"]["mail_body"],
                    'is_reminder'           => $reservation_data["info"]["is_reminder_flg"],
                    'mail_type'				=> $reservation_data["info"]["mail_type"],
                    "is_convert_wb_to_pdf"   => $reservation_data["info"]["is_convert_wb_to_pdf"],
                    "is_rec_flg"             => $reservation_data["info"]["is_rec_flg"],
                    "is_cabinet_flg"         => $reservation_data["info"]["is_cabinet_flg"],
                    "is_desktop_share"       => $reservation_data["info"]["is_desktop_share"],
                    "is_invite_flg"          => $reservation_data["info"]["is_invite_flg"],
                    API_ROOM_ID => $reservation_data["info"]["room_key"],
                    API_MEETING_ID => $this->ticket_to_session(
                            $reservation_data["info"]["meeting_key"]),
                    'media_server_key' => $ives_setting["media_mixer_key"] ? $ives_setting["media_mixer_key"] : "",
                    API_RESERVATION_ID => $reservation_data["info"]["reservation_session"],
                    API_RESERVATION_START => $reservation_data["info"]["reservation_starttime_unix_time"],
                    API_RESERVATION_END => $reservation_data["info"]["reservation_endtime_unix_time"],
                    'pin_cd' => $pin_cd,
                    'use_videoconference' =>$reservation_data["info"]["use_videoconference"],
                    'videoconference_room_num' =>$reservation_data["info"]["videoconference_room_num"],
                    'videoconference_layout' =>$reservation_data["info"]["videoconference_layout"],
            );
            if($reservation_data["info"]["reservation_pw"] === "0"){
                $info['reservation_pw'] = 1;
            }

            if (strtotime($reservation_data["info"]['reservation_starttime']) < strtotime("now") && strtotime($reservation_data["info"]['reservation_extend_endtime']) > strtotime("now")) {
                $info["status"] = "now"; // 開催中
            } elseif (strtotime($reservation_data["info"]['reservation_extend_endtime']) < strtotime("now")) {
                $info["status"] = "end"; // 終了
            } else {
                $info["status"] = "wait"; // 開催待
            }

            $reservation_data["info"] = $info;
            if (! $_room_info["use_sales_option"]) {
                $info["url"] = $reservation_data["info"]["url"];
            }

            unset($reservation_data["guest_flg"]);
            $country_key = $this->session->get("country_key");

            foreach ($reservation_data["guests"] as $_key => $guest) {
                if ($_room_info["use_sales_option"] && $guest["member_key"]) {
                    $guest_type = "staff";
                } else {
                    if ($_room_info["use_sales_option"] &&
                             ! $guest["member_key"]) {
                        $guest_type = "customer";
                    } else {
                        $guest_type = ($guest["type"] == 1) ? "audience" : "";
                    }
                }
                $guests[] = array(
                        'name' => $guest["name"],
                        'email' => $guest["email"],
                        'timezone' => $guest["timezone"],
                        'lang' => $guest["lang"],
                        API_GUEST_ID => $guest["r_user_session"],
                        'type' => $guest_type,
                        'invite_url' => N2MY_BASE_URL . "r/" .
                                 $guest["r_user_session"] . '&c=' . $country_key .
                                 '&lang=' . $guest["lang"]
                );
            }

            unset($reservation_data["guests"]);
            $reservation_data["guests"]["guest"] = $guests;

            foreach ($reservation_data["documents"] as $_key => $_val) {
                $documents[] = array(
                        'document_id' => $_val["document_id"],
                        'name' => $_val["name"],
                        'type' => $_val["type"],
                        'category' => $_val["category"],
                        'status' => $_val["status"],
                        'index' => $_val["index"],
                        'sort' => $_val["sort"]
                );
            }

            // 予約拡張取得
            $obj_reservation_extend = new ReservationExtendTable($this->get_dsn());
            $where = "reservation_session_id = '" .mysql_real_escape_string($reservation_session) . "'";
            $sort = array(
                "extend_endtime" => "DESC"
            );

            $extend_infos = $obj_reservation_extend->getRowsAssoc($where , $sort );
            $extend_infos_count = count($extend_infos);
            for($i = 0; $i < $extend_infos_count; $i++){
                $extend_infos[$i]["extend_starttime"] = strtotime($extend_infos[$i]["extend_starttime"]);
                $extend_infos[$i]["extend_endtime"] = strtotime($extend_infos[$i]["extend_endtime"]);
                $extend_infos[$i]["create_datetime"] = strtotime($extend_infos[$i]["create_datetime"]);
                unset($extend_infos[$i]["videoconference_room_num"]);
                unset($extend_infos[$i]["videoconference_layout"]);
            }

            unset($reservation_data["documents"]);
            require_once ("lib/EZLib/EZUtil/EZEncrypt.class.php");
            $reservation_data["documents"]["document"] = $documents;
            $encrypted_data = EZEncrypt::encrypt(N2MY_ENCRYPT_KEY,
                    N2MY_ENCRYPT_IV, $reservation_session);
            $encrypted_data = str_replace("/", "-", $encrypted_data);
            $encrypted_data = str_replace("+", "_", $encrypted_data);
            $reservation_data["info"]["url"] = N2MY_BASE_URL . "y/" .
                     $encrypted_data;
            $data = array(
                    "reservation_info" => $reservation_data,
                    "extend"           => $extend_infos
            );
            return $this->output($data);
        }
    }

    function check_reservation_password($enter_pw , $original_pw, $admin_pw) {
        $flag = true;
        require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
        if (($enter_pw != $original_pw) &&
                 ($enter_pw !=
                 EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV,
                        $admin_pw))) {
            $flag = false;
        }
        return $flag;
    }

    /**
     * 予約追加
     * @param string room_key ルームキー
     * @param string meeting_name 会議名 (デフォルト null)
     * @param datetime start_time 開始日時
     * @param datetime end_time 終了日時
     * @param string r_user_name 参加者名 (デフォルト null)
     * @param string r_user_email 参加者メールアドレス (デフォルト null)
     * @param st string r_user_timezone 参加者タイムゾーン (デフォルト null)
     * @return boolean
     */
    function action_add ()
    {
        $user_info = $this->session->get("user_info");
        $member_info = $this->session->get("member_info");
        $user_key = $user_info["user_key"];
        $room_key = $this->request->get(API_ROOM_ID);
        $reservation_name = $this->request->get("name");
        $reservation_place = $this->session->get("time_zone"); // セッションの値から取得
        $_starttime = $this->request->get("start");
        $_endtime = $this->request->get("end");
        $reservation_pw = $this->request->get("password");
        $reservation_pw_type     = $this->request->get("password_type");
        $payment_type = $this->request->get("payment_type")?$this->request->get("payment_type"):1;
        $is_publish = $this->request->get("is_publish") != null?$this->request->get("is_publish"):1;
        $sender_name = $this->request->get("sender_name");
        $sender_email = $this->request->get("sender_email");
        $reservation_info = $this->request->get("info");
        $_guests = $this->request->get("guests");
        $send_mail = $this->request->get("send_mail", 1);
        $_organizer              = $this->request->get("organizer");
        $use_videoconference = $this->request->get("use_videoconference");
        $videoconference_room_num = $this->request->get("videoconference_room_num");
        $videoconference_layout = $this->request->get("videoconference_layout");

        /*2014-01-14 追加
      予約追加で「リマインダー」と「メールHTML化」追加
    */
        $mail_type 		         = $this->request->get("mail_type");
        $is_reminder_flg         = $this->request->get("is_reminder");

        $is_convert_wb_to_pdf    = $this->request->get("is_convert_wb_to_pdf");
        $is_rec_flg              = $this->request->get("is_rec");
        $is_cabinet_flg          = $this->request->get("is_cabinet");
        $is_desktop_share        = $this->request->get("is_desktop_share");
        $is_invite_flg           = $this->request->get("is_invite");

        if($is_convert_wb_to_pdf == 0 || $is_rec_flg == 0 || $is_cabinet_flg == 0 || $is_desktop_share == 0 || $is_invite_flg == 0){
          $is_limited_function = 1;
        }else{
          $is_limited_function = 0;
        }

        // 時差変換
        if ($_starttime) {
            if (is_numeric($_starttime)) {
                $reservation_starttime = EZDate::getZoneDate("Y-m-d H:i:s",
                        $reservation_place, $_starttime);
            } else {
                $reservation_starttime = $_starttime;
            }
        }
        if ($_endtime) {
            if (is_numeric($_endtime)) {
                $reservation_endtime = EZDate::getZoneDate("Y-m-d H:i:s",
                        $reservation_place, $_endtime);
            } else {
                $reservation_endtime = $_endtime;
            }
        }
        $room_info = $this->session->get("room_info");
        $room_keys = array_keys($room_info);
        $_room_info = $room_info[$room_key];
        $reservation_data["info"] = array(
                "user_key" => $user_key,
                "member_key" => $member_info["member_key"],
                "room_key" => $room_key,
                "reservation_name" => $reservation_name,
                "reservation_place" => $reservation_place,
                "reservation_starttime" => $reservation_starttime,
                "reservation_endtime" => $reservation_endtime,
                "sender" => $sender_name,
                "sender_mail" => $sender_email,
                "sender_lang" => $this->session->get("lang"),
                "mail_body" => $reservation_info,
                "mail_type"				 => $mail_type,
                "is_reminder_flg"        => $is_reminder_flg,
                "is_convert_wb_to_pdf"   => $is_convert_wb_to_pdf,
                "is_rec_flg"             => $is_rec_flg,
                "is_cabinet_flg"         => $is_cabinet_flg,
                "is_desktop_share"       => $is_desktop_share,
                "is_invite_flg"          => $is_invite_flg,
                "is_limited_function"    => $is_limited_function,
                "payment_type"           => $payment_type,
                "is_publish"             => $is_publish,
                "use_videoconference"    => $use_videoconference,
                "videoconference_room_num"=> $videoconference_room_num,
                "videoconference_layout" => $videoconference_layout,
        );
        if($_organizer["name"] != null && $_organizer["email"] != null){
          $reservation_data["info"]["is_organizer_flg"] = 1;
          $_organizer["r_organizer_flg"] = 1;

          if(empty($_organizer["timezone"])){
            $_organizer["timezone"] = 100;
          }
          if(empty($_organizer["lang"])){
            $_organizer["lang"] = "ja";
          }
        }
        // 予約会議登録
        if ($reservation_pw || $reservation_pw === "0") {
            $reservation_data["pw_flg"] = 1;
            $reservation_data["info"]["reservation_pw"] = $reservation_pw;
            if($reservation_pw_type == 1){
              $reservation_data["info"]["reservation_pw_type"] = 1;
            }else{
              $reservation_data["info"]["reservation_pw_type"] = 2;
            }
        }
        if ($_room_info['options']['multicamera'] > 0) {
            $reservation_data["info"]["is_multicamera"] = 1;
        }
        $this->logger2->info($reservation_data);
        $err_obj = $this->action_check($reservation_data);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(100, "PARAMETER_ERROR",
                    $this->get_error_info($err_obj));
        }
        // 参加者更新
        if ($_guests) {
            // 本当は直接招待者のみ取得したいが、N2MY_Reservation::getParticipantListが、reservation_keyで取得している。こちらを変更する必要有り
            $error_info = array();
            $timezone_list = array_keys($this->get_timezone_list());
            $timezone_list[] = 100;
            $guest_rules = array(
                    "name" => array(
                            "required" => true,
                            "maxlen" => 50
                    ),
                    "email" => array(
                            "required" => true,
                            "email" => true
                    ),
                    "timezone" => array(
                            "allow" => $timezone_list
                    ),
                    "lang" => array(
                            "allow" => array_keys($this->get_language_list())
                    )
            );
            if ($_room_info["room_info"]["use_sales_option"]) {
                $guest_rules["type"] = array(
                        "allow" => array(
                                "staff,customer"
                        )
                );
                require_once ("classes/dbi/member_room_relation.dbi.php");
                $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
                $where = "room_key = '" . addslashes($room_key) . "'";
                $room_relation = $objRoomRelation->getRow($where);
                $staff_count = 0;
                $customer_count = 0;
            } else {
                $guest_rules["type"] = array(
                        "allow" => array(
                                "audience"
                        )
                );
            }

            // 一括登録対応
            $guests = array();
            if ($_room_info["room_info"]["use_sales_option"]) {
                if ($_guests["staff"]) {
                    // 古いAPI利用者用対応(zhの場合はzh-cnに変更)
                    if ($_guests["staff"]["lang"] == "zh") {
                        $_guests["staff"]["lang"] = "zh-cn";
                    }
                    $guests["staff"] = array(
                            "name" => $_guests["staff"]["name"],
                            "email" => $_guests["staff"]["email"],
                            "timezone" => (@is_numeric(
                                    $_guests["staff"]["timezone"])) ? $_guests["staff"]["timezone"] : 100,
                            "lang" => ($_guests["staff"]["lang"]) ? $_guests["staff"]["lang"] : $this->session->get("lang"),
                            "type" => "",
                            "r_organizer_flg" => 0,
                            "member_key" => $room_relation["member_key"]
                    );
                } else
                    if (! $_guests["staff"]) {
                        $err_obj->set_error(API_GUEST_ID,
                                "RESERVATION_GUEST_STAFF_INVALID");
                    }
                if ($_guests["customer"]) {
                    // 古いAPI利用者用対応(zhの場合はzh-cnに変更)
                    if ($_guests["customer"]["lang"] == "zh") {
                        $_guests["customer"]["lang"] = "zh-cn";
                    }
                    $guests["customer"] = array(
                            "name" => $_guests["customer"]["name"],
                            "email" => $_guests["customer"]["email"],
                            "timezone" => (@is_numeric(
                                    $_guests["customer"]["timezone"])) ? $_guests["customer"]["timezone"] : 100,
                            "lang" => ($_guests["customer"]["lang"]) ? $_guests["customer"]["lang"] : $this->session->get(
                                    "lang"),
                            "type" => "",
                            "r_organizer_flg" => 0
                    );
                } else
                    if (! $_guests["customer"]) {
                        $err_obj->set_error(API_GUEST_ID,
                                "RESERVATION_GUEST_CUSTOMER_INVALID");
                    }
            } else {
                foreach ($_guests as $key => $guest) {
                    $type = ($type == "audience") ? 1 : "";
                    // メールアドレスがないユーザーが招待者と見なさない
                    if ($guest["email"]) {
                        // 古いAPI利用者用対応(zhの場合はzh-cnに変更)
                        if ($guest["lang"] == "zh") {
                            $guest["lang"] = "zh-cn";
                        }
                        $guests[$key] = array(
                                "name" => $guest["name"],
                                "email" => $guest["email"],
                                "timezone" => (@is_numeric($guest["timezone"])) ? $guest["timezone"] : 100,
                                "lang" => ($guest["lang"]) ? $guest["lang"] : $this->session->get(
                                        "lang"),
                                "type" => $guest["type"],
                                "r_organizer_flg" => 0
                        );
                    }
                }
            }
            // 入力チェック
            foreach ($guests as $key => $guest) {
                $guest_err_obj = $this->error_check($guest, $guest_rules);
                if (EZValidator::isError($guest_err_obj)) {
                    $error_info[] = $this->get_error_info($guest_err_obj);
                } else {
                    $guests[$key]["type"] = ($guest["type"] == "audience") ? 1 : "";
                }
            }
            if ($error_info) {
                $err_obj->set_error("guests", "regex");
            }
            $reservation_data["guest_flg"] = 1;
            $reservation_data["send_mail"] = $send_mail;
            $reservation_data["guests"] = $guests;
            $reservation_data['organizer'] = $_organizer;
        } else
            if (! $_guests && $_room_info["room_info"]["use_sales_option"]) {
                $err_obj->set_error(API_GUEST_ID, "RESERVATION_GUEST_INVALID");
            }

        // エラー確認
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(100, "PARAMETER_ERROR",
                    $this->get_error_info($err_obj));
        }
        if ($_room_info["room_info"]["use_sales_option"]) {
            $reservation_data["staff"] = $reservation_data["guests"]["staff"];
            $reservation_data["guest"] = $reservation_data["guests"]["customer"];
            unset($reservation_data["guests"]);
        }

        // テレビ会議系の設定
        if($use_videoconference == 0){
            $reservation_data["info"]["videoconference_room_num"] = null;
            $reservation_data["info"]["videoconference_layout"] = null;
        }

        if($use_videoconference == 1){
            $rules = array(
                    'videoconference_room_num' => array(
                            "required" => true,
                    ),
                    "videoconference_layout"  => array(
                            "required" => true,
                    ),
            );
            // チェック
            $err_obj = $this->error_check($reservation_data["info"], $rules);
            // チェック判定
            if (EZValidator::isError($err_obj)) {
                return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
            }
        }

        $result = $this->objReservation->add($reservation_data);

        $guests = $this->_format_guests($result["guests"],
                $_room_info["room_info"]["use_sales_option"]);
        require_once ("lib/EZLib/EZUtil/EZEncrypt.class.php");
        $encrypted_data = EZEncrypt::encrypt(N2MY_ENCRYPT_KEY, N2MY_ENCRYPT_IV,
                $result["reservation_session"]);
        $encrypted_data = str_replace("/", "-", $encrypted_data);
        $encrypted_data = str_replace("+", "_", $encrypted_data);
        $data = array(
                API_RESERVATION_ID => $result["reservation_session"],
                "url" => N2MY_BASE_URL . "y/" . $encrypted_data,
                "pin_cd" => $result["pin_cd"],
                "guests" => $guests
        );
        return $this->output($data);
    }

    function action_add_sales ()
    {
        $this->action_add();
    }

    /**
     * 予約変更
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @param string meeting_name 会議名
     * @param datetime start_time 変更後開始時間
     * @param datetime end_time 変更後終了時間
     * @return boolean
     */
    function action_update ()
    {
        $user_info = $this->session->get("user_info");
        $user_key = $user_info["user_key"];
        $room_key = $this->request->get(API_ROOM_ID);
        $reservation_session = $this->request->get(API_RESERVATION_ID);
        $reservation_name = $this->request->get("name");
        $reservation_place = $this->session->get("time_zone"); // セッションの値から取得
        $_starttime = $this->request->get("start");
        $_endtime = $this->request->get("end");
        $reservation_pw = $this->request->get("password");
        $new_reservation_pw = $this->request->get("new_password");
        $reservation_pw_type     = $this->request->get("password_type");
        $is_password_update = $this->request->get("is_password_update");
        $payment_type = $this->request->get("payment_type")?$this->request->get("payment_type"):1;
        $is_publish = $this->request->get("is_publish") != null?$this->request->get("is_publish"):1;
        $sender_name = $this->request->get("sender_name");
        $sender_email = $this->request->get("sender_email");
        $reservation_info = $this->request->get("info");
        $_guests = $this->request->get("guests");
        $guest_flg = $this->request->get("guest_flg", 1);
        $send_mail = $this->request->get("send_mail", 1);
        $mail_type 		         = $this->request->get("mail_type");
        $is_reminder_flg         = $this->request->get("is_reminder");
        $mail_send_type          = $this->request->get("mail_send_type");
        $is_convert_wb_to_pdf    = $this->request->get("is_convert_wb_to_pdf");
        $is_rec_flg              = $this->request->get("is_rec");
        $is_cabinet_flg          = $this->request->get("is_cabinet");
        $is_desktop_share        = $this->request->get("is_desktop_share");
        $is_invite_flg           = $this->request->get("is_invite");
        $_organizer              = $this->request->get("organizer");
        $use_videoconference = $this->request->get("use_videoconference");
        $videoconference_room_num = $this->request->get("videoconference_room_num");
        $videoconference_layout = $this->request->get("videoconference_layout");

        // 時差変換
        if ($_starttime) {
            if (is_numeric($_starttime)) {
                $reservation_starttime = EZDate::getZoneDate("Y-m-d H:i:s",
                        $reservation_place, $_starttime);
            } else {
                $reservation_starttime = $_starttime;
            }
        }
        if ($_endtime) {
            if (is_numeric($_endtime)) {
                $reservation_endtime = EZDate::getZoneDate("Y-m-d H:i:s",
                        $reservation_place, $_endtime);
            } else {
                $reservation_endtime = $_endtime;
            }
        }

        if($is_convert_wb_to_pdf == 0 || $is_rec_flg == 0 || $is_cabinet_flg == 0 || $is_desktop_share == 0 || $is_invite_flg == 0){
          $is_limited_function = 1;
        }else{
          $is_limited_function = 0;
        }

        $reservation_data["info"] = array(
                "room_key" => $room_key,
                "reservation_session" => $reservation_session,
                "reservation_name" => $reservation_name,
                "reservation_place" => $reservation_place,
                "reservation_starttime" => $reservation_starttime,
                "reservation_endtime" => $reservation_endtime,
                "sender" => $sender_name,
                "sender_mail" => $sender_email,
                "mail_body" => $reservation_info,
                "user_key" => $user_key,
          "mail_type"             => $mail_type,
          "is_reminder_flg"       => $is_reminder_flg,
          "is_convert_wb_to_pdf"   => $is_convert_wb_to_pdf,
          "is_rec_flg"             => $is_rec_flg,
          "is_cabinet_flg"         => $is_cabinet_flg,
          "is_desktop_share"       => $is_desktop_share,
          "is_invite_flg"          => $is_invite_flg,
          "is_limited_function"    => $is_limited_function,
          "payment_type"           => $payment_type,
          "is_publish"           => $is_publish,
          "use_videoconference"    => $use_videoconference,
          "videoconference_room_num"=> $videoconference_room_num,
          "videoconference_layout" => $videoconference_layout,
        );

        $_reservation_data = $this->objReservation->getDetail($reservation_session);
        // 予約会議登録
        if ($is_password_update && ($reservation_pw || $reservation_pw === "0") ) {
            $reservation_data["pw_flg"] = 1;
            $reservation_data["info"]["reservation_pw"] = $reservation_pw;
        } else if ($is_password_update && !$reservation_pw) {
            $reservation_data["pw_flg"] = 0;
            $reservation_data["info"]["reservation_pw"] = "";
        } else if ($_reservation_data["info"]["reservation_pw"] || $_reservation_data["info"]["reservation_pw"] === "0") {
            $reservation_data["pw_flg"] = 1;
            $reservation_data["info"]["reservation_pw"] = $_reservation_data["info"]["reservation_pw"];
        }
        $err_obj = $this->action_check($reservation_data);
        if (! $_reservation_data) {
            $err_obj->set_error(API_RESERVATION_ID,
                    "RESERVATION_SESSION_INVALID");
        } else {
            // ログインユーザ以外の予約
            if ($_reservation_data["info"]["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID,
                        "RESERVATION_SESSION_INVALID");
            }
            $reservation_data["info"]["meeting_key"] = $_reservation_data["info"]["meeting_key"];
        }
        $room_modify_flg = 0;
        if ($_reservation_data["info"]["room_key"] != $reservation_data["info"]["room_key"]) {
            //$err_obj->set_error("room_key", "ROOMKEY_INVALID");
            require_once ("classes/N2MY_Meeting.class.php");
            $obj_N2MY_Meeting    = new N2MY_Meeting($this->dsn);
            $reservation_data["info"]["meeting_key"] = $obj_N2MY_Meeting->createMeetingKey($room_key);
            $room_modify_flg = 1;
        }
        if (@$_reservation_data["info"]["is_multicamera"] > 0) {
            $reservation_data["info"]["is_multicamera"] = 1;
        }
        $reservation_data["info"]["reservation_key"] = $_reservation_data["info"]["reservation_key"];
        // 参加者更新
        $room_info = $this->session->get("room_info");
        $room_keys = array_keys($room_info);
        $_room_info = $room_info[$room_key];
        $reservation_data["guest_flg"] = $guest_flg;
        $guests = array();
        if ($_guests) {
            $error_info = array();
            $timezone_list = array_keys($this->get_timezone_list());
            $timezone_list[] = 100;
            $guest_rules = array(
                    "name" => array(
                            "required" => true,
                            "maxlen" => 50
                    ),
                    "email" => array(
                            "required" => true,
                            "email" => true
                    ),
                    "timezone" => array(
                            "allow" => $timezone_list
                    ),
                    "lang" => array(
                            "allow" => array_keys($this->get_language_list())
                    )
            );
            if ($_room_info["room_info"]["use_sales_option"]) {
                $guest_rules["type"] = array(
                        "allow" => array(
                                "staff,customer"
                        )
                );
                require_once ("classes/dbi/member_room_relation.dbi.php");
                $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
                $where = "room_key = '" . addslashes($room_key) . "'";
                $room_relation = $objRoomRelation->getRow($where);
            } else {
                $guest_rules["type"] = array(
                        "allow" => array(
                                "audience"
                        )
                );
            }
            if ($_room_info["room_info"]["use_sales_option"]) {
                if ($_guests["staff"]) {
                    // 古いAPI利用者用対応(zhの場合はzh-cnに変更)
                    if ($_guests["staff"]["lang"] == "zh") {
                        $_guests["staff"]["lang"] = "zh-cn";
                    }
                    $guests["staff"] = array(
                            "r_user_session" => $_guests["staff"]["guest_id"],
                            "name" => $_guests["staff"]["name"],
                            "email" => $_guests["staff"]["email"],
                            "timezone" => (@is_numeric(
                                    $_guests["staff"]["timezone"])) ? $_guests["staff"]["timezone"] : 100,
                            "lang" => ($_guests["staff"]["lang"]) ? $_guests["staff"]["lang"] : $this->session->get(
                                    "lang"),
                            "type" => "",
                            "r_organizer_flg" => 0,
                            "member_key" => $room_relation["member_key"]
                    );
                } else
                    if (! $_guests["staff"]) {
                        $err_obj->set_error(API_GUEST_ID,
                                "RESERVATION_GUEST_STAFF_INVALID");
                    }
                if ($_guests["customer"]) {
                    // 古いAPI利用者用対応(zhの場合はzh-cnに変更)
                    if ($_guests["customer"]["lang"] == "zh") {
                        $_guests["customer"]["lang"] = "zh-cn";
                    }
                    $guests["customer"] = array(
                            "r_user_session" => $_guests["customer"]["guest_id"],
                            "name" => $_guests["customer"]["name"],
                            "email" => $_guests["customer"]["email"],
                            "timezone" => (@is_numeric(
                                    $_guests["customer"]["timezone"])) ? $_guests["customer"]["timezone"] : 100,
                            "lang" => ($_guests["customer"]["lang"]) ? $_guests["customer"]["lang"] : $this->session->get(
                                    "lang"),
                            "type" => "",
                            "r_organizer_flg" => 0
                    );
                } else
                    if (! $_guests["customer"]) {
                        $err_obj->set_error(API_GUEST_ID,
                                "RESERVATION_GUEST_CUSTOMER_INVALID");
                    }
            } else {
                // 一括登録対応
                foreach ($_guests as $key => $guest) {
                    // メールアドレスがないユーザーが招待者と見なさない
                    if ($guest["email"]) {
                        $type = ($type == "audience") ? 1 : "";
                        // 古いAPI利用者用対応(zhの場合はzh-cnに変更)
                        if ($guest["lang"] == "zh") {
                            $guest["lang"] = "zh-cn";
                        }
                        $guests[$key] = array(
                                "reservation_key" => $_reservation_data["info"]["reservation_key"],
                                "name" => $guest["name"],
                                "email" => $guest["email"],
                                "timezone" => (is_numeric($guest["timezone"])) ? $guest["timezone"] : 100,
                                "lang" => ($guest["lang"]) ? $guest["lang"] : $this->session->get(
                                        "lang"),
                                "type" => $guest["type"],
                                "r_user_session" => $guest["guest_id"],
                                "r_organizer_flg" => 0
                        );
                    }
                }
            }
            $this->logger2->warn($guests);
            // 入力チェック
            foreach ($guests as $key => $guest) {
                $guest_err_obj = $this->error_check($guest, $guest_rules);
                if (EZValidator::isError($guest_err_obj)) {
                    $error_info[] = $this->get_error_info($guest_err_obj);
                } else {
                    $guests[$key]["type"] = ($guest["type"] == "audience") ? 1 : "";
                }
            }
            if ($error_info) {
                $err_obj->set_error("guests", "regex");
            }
        }
        $reservation_data["guests"] = $guests;
        $reservation_data['organizer'] = $_organizer;
        $reservation_data["send_mail"] = $send_mail;
        if($send_mail){
          if($mail_send_type == 2){
            $reservation_data["mail_send_type"] = 2;
          }else{
            $reservation_data["mail_send_type"] = 1;
          }
        }else{
          $reservation_data["mail_send_type"] = 0;
        }

        // テレビ会議系の設定
        if($use_videoconference == 0){
            $reservation_data["info"]["videoconference_room_num"] = null;
            $reservation_data["info"]["videoconference_layout"] = null;
        }

        if($use_videoconference == 1){
            $rules = array(
                    'videoconference_room_num' => array(
                            "required" => true,
                    ),
                    "videoconference_layout"  => array(
                            "required" => true,
                    ),
            );
            // チェック
            $err_obj = $this->error_check($reservation_data["info"], $rules);
            // チェック判定
            if (EZValidator::isError($err_obj)) {
                return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
            }
        }

        require_once ("classes/dbi/meeting.dbi.php");
        $objMeeting = new MeetingTable($this->get_dsn());
        $pin_cd = $objMeeting->getOne(
                "meeting_ticket = '" .
                         addslashes($reservation_data["info"]["meeting_key"]) .
                         "'", "pin_cd");
        // エラー確認
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(100, "PARAMETER_ERROR",
                    $this->get_error_info($err_obj));
        } else {
            if ($_room_info["room_info"]["use_sales_option"]) {
                $reservation_data["staff"] = $reservation_data["guests"]["staff"];
                $reservation_data["guest"] = $reservation_data["guests"]["customer"];
                unset($reservation_data["guests"]);
            }
            $result = $this->objReservation->update($reservation_data);
            $guests = $this->_format_guests($result["guests"],
                    $_room_info["room_info"]["use_sales_option"]);
        if($room_modify_flg){
        $m_data = array(
          "is_deleted"    => 1,
          "is_active"     => 0,
          "pin_cd"        => "",
                );
        $objMeeting->update($m_data , "meeting_ticket = '".addslashes($_reservation_data["info"]["meeting_key"])."'");
      }
            $guests["pin_cd"] = $objMeeting->getOne("meeting_ticket = '".addslashes($reservation_data["info"]["meeting_key"])."'", "pin_cd");
            return $this->output($guests);
        }
    }

    function action_update_room_key ()
    {
      $user_info = $this->session->get("user_info");
      $user_key = $user_info["user_key"];
      $room_key = $this->request->get(API_ROOM_ID);
      $reservation_session = $this->request->get(API_RESERVATION_ID);
      //$this->logger2->info($reservation_session);
      $room_info = $this->session->get("room_info");
      $rules = array(
          "room_key" => array(
              "required" => true,
              "allow" => array_keys($room_info)
          ),
      );
      $param["room_key"] = $room_key;
      $err_obj = $this->error_check($param, $rules);
      $_reservation_data = $this->objReservation->getDetail($reservation_session);
      $this->logger2->debug($_reservation_data);
      if (! $_reservation_data) {
        $err_obj->set_error(API_RESERVATION_ID,
            "RESERVATION_SESSION_INVALID");
      }
      if ($_reservation_data["info"]["user_key"] != $user_info["user_key"]) {
          $err_obj->set_error(API_RESERVATION_ID,
              "RESERVATION_SESSION_INVALID");
      }
      //部屋のMedia Mixer状態チェック
      require_once("classes/dbi/ives_setting.dbi.php");
      $ivesSettingTable = new IvesSettingTable($this->get_dsn());
      $where = "room_key = '" . addslashes($room_key) . "' AND is_deleted = 0";
      $ives_setting = $ivesSettingTable->getRow($where);
      //$this->logger2->info($ives_setting);
      if (EZValidator::isError($err_obj)) {
        require_once("classes/mgm/dbi/media_mixer.dbi.php");
        $objMediaMixer = new MediaMixerTable(N2MY_MDB_DSN);
        $where = "media_mixer_key = '" . addslashes($ives_setting["media_mixer_key"]) . "' AND is_available = 1";
        $mideaMixer = $objMediaMixer->getRow($where);
        //$this->logger2->info($mideaMixer);
        if(!$mideaMixer) {
          $err_obj->set_error(API_ROOM_ID,
              "MIDEA_MIXER_OF_ROOM_IS_DOWN");
        }
      }
      //$this->logger2->info($err_obj);
      if (EZValidator::isError($err_obj)) {
        return $this->display_error(100, "PARAMETER_ERROR",
            $this->get_error_info($err_obj));
      } else {
        //$this->logger2->info($_reservation_data);
        $data["room_key"] = $room_key;
        $data["mcu_down_flg"] = 0;
        //Media Mixer変更
        $data["media_mixer_key"] = $ives_setting["media_mixer_key"];
        $data["reservation_updatetime"] = date('Y-m-d H:i:s');
        $obj_Reservation     = new ReservationTable($this->get_dsn());
        $obj_Reservation->update($data, "reservation_key = '".addslashes($_reservation_data["info"]["reservation_key"])."'");
        $room_info = $this->session->get("room_info");
        //$this->logger2->info($room_info);
        $objMeeting = new MeetingTable($this->get_dsn());
        $meeting_data["room_key"] = $room_key;
        $meeting_data["meeting_room_name"] = $room_info[$room_key]["room_info"]["room_name"];
        $meeting_data["meeting_tag"] = $room_key;
        $meeting_data["update_datetime"] = date('Y-m-d H:i:s');
        $objMeeting->update($meeting_data , "meeting_ticket = '".addslashes($_reservation_data["info"]["meeting_key"])."'");
        return $this->output();
      }
    }

    function action_update_sales ()
    {
        $this->action_update();
    }

    /**
     * 予約入力チェック
     */
    function action_check ($data)
    {
        $user_info = $this->session->get("user_info");
        $room_info = $this->session->get("room_info");
        // 入力チェック
        $rules = array(
                "room_key" => array(
                        "required" => true,
                        "allow" => array_keys($room_info)
                ),
                "reservation_name" => array(
                        "required" => true,
                        "maxlen" => 100
                ),
                "reservation_starttime" => array(
                        "required" => true,
                        "datetime" => true
                ),
                "reservation_endtime" => array(
                        "required" => true,
                        "datetime" => true
                ),
                "reservation_place" => array(
                        "allow" => array_keys($this->get_timezone_list())
                ),
                "payment_type" => array(
                        "allow" => array("1","2")
                ),
                "is_publish" => array(
                        "allow" => array("0","1")
                ),
                "use_videoconference" => array(
                        "allow" => array("0","1")
                ),
                "videoconference_layout" => array(
                        "allow" => array("1x1","2x2","3x3","1+1","1+5","1+7")
                ),
                "sender" => array(
                        "maxlen" => 50
                ),
                "sender_email" => array(
                        "email" => true
                ),
                "info" => array(
                        "maxlen" => 10000
                ),
                "reservation_pw" => array(
                        "alnum"    => true,
                ),
                "output_type" => array(
                        "allow" => $this->get_output_type_list()
                )
        );
        $param = $data["info"];
        $err_obj = $this->error_check($param, $rules);

        // 開始、終了時間をチェック
        $limit_start_time = time() - (2 * 24 * 3600);
        $limit_end_time = time() + (367 * 24 * 3600);
        if (strtotime($param["reservation_starttime"]) <= $limit_start_time) {
            $err_obj->set_error("reservation_starttime",
                    "RESERVATION_STARTTIME_INVALID");
        }
        if (strtotime($param["reservation_endtime"]) >= $limit_end_time) {
            $err_obj->set_error("reservation_endtime",
                    "RESERVATION_ENDTIME_INVALID");
        }
        if (strtotime($param["reservation_starttime"]) >=
                 strtotime($param["reservation_endtime"])) {
            $err_obj->set_error("reservation_endtime",
                    "RESERVATION_ERROR_INVALIDTIME");
        }

        // 終了時間が現時刻より前だった場合のエラー処理
        $svr_timezone = $this->config->get("GLOBAL", "time_zone",
                N2MY_SERVER_TIMEZONE); // このサーバーのタイムゾーン
        $_starttime = EZDate::getLocateTime($param['reservation_starttime'],
                $svr_timezone, $param['reservation_place']);
        $_endtime = EZDate::getLocateTime($param['reservation_endtime'],
                $svr_timezone, $param['reservation_place']);
        if ($_endtime < time()) {
            $err_obj->set_error("reservation_endtime",
                    "RESERVATION_ENDTIME_INVALID2");
        }
        $endtime = date('Y-m-d H:i:s', $_endtime);
        $starttime = date('Y-m-d H:i:s', $_starttime);

        // 重複一覧
        require_once ("classes/dbi/reservation.dbi.php");
        $obj_Reservation = new ReservationTable($this->get_dsn());

        $where = "room_key = '" . addslashes($param["room_key"]) . "'" .
                 " AND reservation_status = 1" . " AND reservation_endtime != '" .
                 $starttime . "'" . " AND reservation_starttime != '" . $endtime .
                 "'" . " AND (" . "( reservation_starttime between '" .
                 $starttime . "' AND '" . $endtime . "' )" .
                 " OR ( reservation_endtime between '" . $starttime . "' AND '" .
                 $endtime . "' )" . " OR ( '" . $starttime .
                 "' BETWEEN reservation_starttime AND reservation_endtime )" .
                 " OR ( '" . $endtime .
                 "' BETWEEN reservation_starttime AND reservation_endtime ) )";

        if ($param["reservation_session"]) {
            $where .= " AND reservation_session != '" .
                     $param["reservation_session"] . "'";
        }

        $duplicate_list = $obj_Reservation->getList($where,
                array(
                        "reservation_starttime" => "asc"
                ));
        if ($duplicate_list) {
            $this->logger2->info($duplicate_list);
            $err_obj->set_error("reservation_starttime",
                    "RESERVATION_ERROR_TIME");
        }
        return $err_obj;
    }

    /**
     * 予約削除
     * @param  string room_key ルームキー
     * @param  string reservation_session 予約セッション
     * @return boolean
     */
    function action_delete ()
    {
        $request = $this->request->getAll();
        $reservation_session = $this->request->get(API_RESERVATION_ID);
        $user_info = $this->session->get("user_info");
        $rules = array(
                API_RESERVATION_ID => array(
                        "required" => true
                ),
                "output_type" => array(
                        "allow" => $this->get_output_type_list()
                )
        );
        $err_obj = $this->error_check($request, $rules);
        if (! $reservation_data = $this->objReservation->getDetail(
                $reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID,
                    "RESERVATION_SESSION_INVALID");
        } else {
            $info = $reservation_data["info"];
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID,
                        "RESERVATION_SESSION_INVALID");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR",
                    $this->get_error_info($err_obj));
        } else {
            $result = $this->objReservation->cancel($reservation_session);
            return $this->output();
        }
    }

    /**
     * 招待者一覧
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @return string $output 名前、メールアドレス、言語、タイムゾーンをxmlで出力
     */
    function action_get_invite ()
    {
        $request = $this->request->getAll();
        $reservation_session = $this->request->get(API_RESERVATION_ID);
        $user_info = $this->session->get("user_info");
        $rules = array(
                API_RESERVATION_ID => array(
                        "required" => true
                ),
                "output_type" => array(
                        "allow" => $this->get_output_type_list()
                )
        );
        $err_obj = $this->error_check($request, $rules);
        if (! $reservation_data = $this->objReservation->getDetail(
                $reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID,
                    "RESERVATION_SESSION_INVALID");
        } else {
            $info = $reservation_data["info"];
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID,
                        "RESERVATION_SESSION_INVALID");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR",
                    $this->get_error_info($err_obj));
        } else {
            // 本当は直接招待者のみ取得したいが、N2MY_Reservation::getParticipantListが、reservation_keyで取得している。こちらを変更する必要有り
            $country_key = $this->session->get("country_key");
            foreach ($reservation_data["guests"] as $key => $guest) {
                $guest["invite_url"] = N2MY_BASE_URL . "r/" .
                         $guest["r_user_session"] . '&c=' . $country_key .
                         '&lang=' . $guest["lang"];
                $guest[API_GUEST_ID] = $guest["r_user_session"];
                unset($guest["r_user_session"]);
                $guests[] = $guest;
            }
            $data["guests"]["guest"] = $guests;
            return $this->output($data);
        }
    }

    /**
     * 招待者追加
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @param string r_user_name 参加者名
     * @param string r_user_email 参加者メールアドレス
     * @param string r_user_lang 参加者言語
     * @param string r_user_timezone 参加者タイムゾーン
     * @return boolean
     */
    function action_add_invite ()
    {
        $request = $this->request->getAll();
        // 古いAPI利用者用対応(zhの場合はzh-cnに変更)
        if ($request["lang"] == "zh") {
            $request["lang"] = "zh-cn";
        }
        $reservation_session = $this->request->get(API_RESERVATION_ID);
        $send_mail = $this->request->get("send_mail", 1);
        $name = $this->request->get("name");
        $email = $this->request->get("email");
        $type = $this->request->get("type");
        $timezone = $this->request->get("timezone");
        $lang = $this->request->get("lang");
        $user_info = $this->session->get("user_info");
        // 本当は直接招待者のみ取得したいが、N2MY_Reservation::getParticipantListが、reservation_keyで取得している。こちらを変更する必要有り
        $error_info = array();
        $rules = array(
                API_RESERVATION_ID => array(
                        "required" => true
                ),
                "output_type" => array(
                        "allow" => $this->get_output_type_list()
                )
        );
        $timezone_list = array_keys($this->get_timezone_list());
        $timezone_list[] = 100;
        $guest_rules = array(
                "name" => array(
                        "required" => true,
                        "maxlen" => 50
                ),
                "email" => array(
                        "required" => true,
                        "email" => true
                ),
                "timezone" => array(
                        "allow" => $timezone_list
                ),
                "lang" => array(
                        "allow" => array_keys($this->get_language_list())
                ),
                "type" => array(
                        "allow" => array(
                                "audience"
                        )
                )
        );
        $err_obj = $this->error_check($request, $rules);
        if (! $reservation_data = $this->objReservation->getDetail(
                $reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID,
                    "RESERVATION_SESSION_INVALID");
        } else {
            $info = $reservation_data["info"];
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID,
                        "RESERVATION_SESSION_INVALID");
            }
        }
        if (EZValidator::isError($err_obj)) {
            $error_info[] = $this->get_error_info($err_obj);
        }
        // 一括登録対応
        $guests = array();
        if (is_array($name)) {
            foreach ($name as $_key => $_val) {
                $type = ($type == "audience") ? $type : "";
                // メールアドレスがないユーザーが招待者と見なさない
                if ($email[$_key]) {
                    $guests[$_key] = array(
                            "reservation_key" => $info["reservation_key"],
                            "name" => $name[$_key],
                            "email" => $email[$_key],
                            "timezone" => ($timezone[$_key]) ? $timezone[$_key] : 100,
                            "lang" => ($lang[$_key]) ? $lang[$_key] : $this->session->get(
                                    "lang"),
                            "type" => $type[$_key],
                            "r_user_session" => $this->objReservation->getInviteId(),
                            "r_organizer_flg" => 0
                    );
                }
            }
        } else {
            $type = ($type == "audience") ? $type : "";
            $guests[] = array(
                    "reservation_key" => $info["reservation_key"],
                    "name" => $name,
                    "email" => $email,
                    "timezone" => ($timezone) ? $timezone : 100,
                    "lang" => ($lang) ? $lang : $this->session->get("lang"),
                    "type" => $type,
                    "r_user_session" => $this->objReservation->getInviteId(),
                    "r_organizer_flg" => 0
            );
        }
        // 入力チェック
        foreach ($guests as $key => $guest) {
            $guest_err_obj = $this->error_check($guest, $guest_rules);
            if (EZValidator::isError($guest_err_obj)) {
                $error_info[] = $this->get_error_info($guest_err_obj);
            }
        }
        if ($error_info) {
            return $this->display_error(100, "PARAMETER_ERROR", $error_info);
        }
        $country_key = $this->session->get("country_key");
        foreach ($guests as $key => $guest) {
            if ($guest['type'] == 'audience') {
                $guest['type'] = 1;
            }
            $guest = $this->objReservation->participant_mail($guest, $info,
                    "create", $send_mail);
            $invited[] = array(
                    API_GUEST_ID => $guest["r_user_session"],
                    "invite_url" => N2MY_BASE_URL . "r/" .
                             $guest["r_user_session"] . '&c=' . $country_key .
                             '&lang=' . $guest["lang"]
            );
        }
        require_once ("classes/dbi/meeting.dbi.php");
        $objMeeting = new MeetingTable($this->get_dsn());
        $pin_cd = $objMeeting->getOne(
                "meeting_ticket = '" .
                         addslashes($reservation_data["info"]["meeting_key"]) .
                         "'", "pin_cd");
        $data["pin_cd"] = $pin_cd;
        $data["guests"]["guest"] = $invited;
        return $this->output($data);
    }

    /**
     * 招待者削除
     *
     * @param string room_key ルームキー
     * @param string reservation_session
     * @param int r_user_key 参加者キー
     * @return boolean
     */
    function action_delete_invite ()
    {
        $request = $this->request->getAll();
        $reservation_session = $this->request->get(API_RESERVATION_ID);
        $invite_id = $this->request->get(API_GUEST_ID);
        $user_info = $this->session->get("user_info");
        $rules = array(
                API_RESERVATION_ID => array(
                        "required" => true
                ),
                API_GUEST_ID => array(
                        "required" => true
                ),
                "output_type" => array(
                        "allow" => $this->get_output_type_list()
                )
        );
        $err_obj = $this->error_check($request, $rules);
        // 予約が存在しない
        if (! $reservation_data = $this->objReservation->getDetail(
                $reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID,
                    "RESERVATION_SESSION_INVALID");
        } else {
            $info = $reservation_data["info"];
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID,
                        "RESERVATION_SESSION_INVALID");
            } else {
                $where = "reservation_key = " . $info["reservation_key"] .
                         " AND r_user_session = '" . addslashes($invite_id) . "'";
                // 招待者IDが存在しない
                if (! $guest = $this->objReservation->obj_ReservationUser->getRow(
                        $where)) {
                    $err_obj->set_error(API_RESERVATION_ID,
                            "INVITE_SESSION_INVALID");
                }
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR",
                    $this->get_error_info($err_obj));
        } else {
            $_guest["reservation_key"] = $guest["reservation_key"];
            $_guest["name"] = $guest["r_user_name"];
            $_guest["email"] = $guest["r_user_email"];
            $_guest["timezone"] = $guest["r_user_place"];
            $_guest["lang"] = $this->request->get("lang", $this->session->get("lang"));
            $_guest["type"] = $guest["r_user_audience"];
            $_guest["r_user_session"] = $guest["r_user_session"];
            $_guest["r_organizer_flg"] = $guest["r_organizer_flg"];
            $this->logger2->info(array($info, $_guest));
            $send_mail = true;
            $this->objReservation->participant_mail($_guest, $info, "deletelist", $send_mail);
            return $this->output();
        }
    }

    /**
     * 指定された招待者一覧で更新
     */
    private function _update_invite ()
    {}

    /**
     * 予約の資料追加
     */
    function action_add_document ()
    {
        $request = $this->request->getAll();
        $reservation_session = $this->request->get(API_RESERVATION_ID);
        $name = $this->request->get("name");
        $file = $_FILES["file"];
        $user_info = $this->session->get("user_info");
        $rules = array(
                API_RESERVATION_ID => array(
                        "required" => true
                ),
                "output_type" => array(
                        "allow" => $this->get_output_type_list()
                )
        );
        $err_obj = $this->error_check($request, $rules);
        if (! $file) {
            $err_obj->set_error("file", "required");
        } else {
            if (! EZValidator::valid_file_ext($file["name"],
                    split(",", $this->config->get("N2MY", "convert_format")))) {
                $err_obj->set_error("file", "file_ext",
                        split(",",
                                $this->config->get("N2MY", "convert_format")));
            }
        }
        if (! $reservation_data = $this->objReservation->getDetail(
                $reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID,
                    "RESERVATION_SESSION_INVALID");
        } else {
            $info = $reservation_data["info"];
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID,
                        "RESERVATION_SESSION_INVALID");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR",
                    $this->get_error_info($err_obj));
        } else {
            $dir = $this->get_work_dir();
            $string = new EZString();
            $document_id = $string->create_id();
            $fileinfo = pathinfo($file["name"]);
            $tmp_name = $dir . "/" . $document_id . "." . $fileinfo["extension"];
            move_uploaded_file($file["tmp_name"], $tmp_name);
            $name = ($name) ? $name : $fileinfo["basename"];
            $reservation_data = $this->objReservation->getDetail(
                    $reservation_session);
            $meeting_ticket = $reservation_data["info"]["meeting_key"];
            $document_id = $this->objReservation->addDocument($meeting_ticket,
                    $tmp_name, $name);
            $data = array(
                    "document_id" => $document_id
            );
            unlink($tmp_name);
            return $this->output($data);
        }
    }

    /**
     * 予約の資料追加(ストレージ)
     */
    function action_add_document_storage() {
      $reservation_session = $this->request->get(API_RESERVATION_ID);
      $name = $this->request->get("name");
      //パラメータをチェック
      $request = $this->request->getAll();
      $user_info = $this->session->get("user_info");
      $rules = array(
          API_RESERVATION_ID => array(
              "required" => true
          ),
          "output_type" => array(
              "allow" => $this->get_output_type_list(),
          ),
      );
      $err_obj = $this->error_check($request, $rules);
      //$reservation_sessionでミーティングチケットを取る
      if (!$reservation_data = $this->objReservation->getDetail($reservation_session)) {
        $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
      } else {
        $info = $reservation_data["info"];
        // ログインユーザ以外の予約
        if ($info["user_key"] != $user_info["user_key"]) {
          $err_obj->set_error(API_RESERVATION_ID, "RESERVATION_SESSION_INVALID");
        }
      }
      $this->logger->info($reservation_data);
      //$document_idあるいはclip_keyでストレージファイル情報を取る
      require_once ("classes/N2MY_Storage.class.php");
        $obj_Storage = new N2MY_Storage($this->get_dsn());
        if (!$file_info = $obj_Storage->getStorageInfoByDocumentID($request["document_id"],$user_info["user_key"])) {
          $err_obj->set_error("document_id", "this file does not exist.");
        } else {
          if($file_info["status"] != "2") {
            $err_obj->set_error("document_id", "this file is prepaing...");
          }
        }
      if (EZValidator::isError($err_obj)) {
        return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
      } else {
        if($file_info["clip_key"]) {
          //$this->logger->info("ok",$file_info["clip_key"]);
          //重複か確認
          $reservation_clips = $this->getMeetingClips($reservation_data['info']['meeting_key']);
          if($reservation_clips) {
            foreach($reservation_clips as $clip) {
              $this->logger->info("clip name", $clip["clip_key"]);
              if($clip["clip_key"] == $file_info["clip_key"]) {
                $err_obj->set_error("document_id", "this video has been added.");
                return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
              }
            }
          }
          //追加
          $file_info["name"] = $name ? $name:$file_info["file_name"];
          $clips[] = $file_info;
          require_once ("classes/N2MY_Reservation.class.php");
          $obj_Reservation = new N2MY_Reservation($this->get_dsn());
          $obj_Reservation->addClipRelations($reservation_data['info']['meeting_key'], $clips, $user_info["user_key"]);
          $data["doucment_id"] = $file_info["clip_key"];

        } elseif($file_info["document_id"]) {
          require_once ("classes/N2MY_Reservation.class.php");
          $obj_Reservation = new N2MY_Reservation($this->get_dsn());
          $file_info["name"] = $name ? $name:$file_info["file_name"];
          $obj_Reservation->_addStorageFile($file_info, $reservation_data["info"]["room_key"], $reservation_data["info"]["meeting_key"], $user_info["user_id"]);
          require_once("classes/core/dbi/Document.dbi.php");
          $obj_Document = new DBI_Document($this->get_dsn());
          $where = "document_path like '%".$reservation_data["info"]["meeting_key"]."%'";
          $sort = array("document_key" => "desc");
          $data["doucment_id"] = $obj_Document->getOne($where, "document_id", $sort);
        }
        return $this->output($data);
      }
    }

    private function getMeetingClips($meeting_ticket) {
      require_once ("classes/dbi/meeting.dbi.php");
      $meeting_table = new MeetingTable($this->get_dsn());
      $meeting_key   = $meeting_table->findMeetingKeyByMeetingTicket($meeting_ticket);
      require_once ("classes/dbi/meeting_clip.dbi.php");
      $meeting_clip_table = new MeetingClipTable($this->get_dsn());
      return $meeting_clip_table->findByMeetingKey($meeting_key);
    }

    /**
     * 更新
     */
    function action_update_document ()
    {
        $request = $this->request->getAll();
        $reservation_session = $this->request->get(API_RESERVATION_ID);
        $document_id = $this->request->get("document_id");
        $user_info = $this->session->get("user_info");
        $rules = array(
                API_RESERVATION_ID => array(
                        "required" => true
                ),
                "document_id" => array(
                        "required" => true
                ),
                "output_type" => array(
                        "allow" => $this->get_output_type_list()
                )
        );
        $err_obj = $this->error_check($request, $rules);
        if (! $reservation_data = $this->objReservation->getDetail(
                $reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID,
                    "RESERVATION_SESSION_INVALID");
        } else {
            $info = $reservation_data["info"];
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID,
                        "RESERVATION_SESSION_INVALID");
            }
            if(substr($document_id, 0, 3) === 'MC_') {
              $clip_files = $this->getMeetingClips($reservation_data['info']['meeting_key']);
              if(!$clip_files) {
                $err_obj->set_error("document_id", "DOCUMENT_INVALID");
              } else {
                $clip_key = 0;
                foreach($clip_files as $clip) {
                  if($document_id == $clip["clip_key"]) {
                    $clip_key = $clip["meeting_clip_key"];
                    break;
                  }
                }
                if($clip_key == 0) {
                  $err_obj->set_error("document_id", "CLIP_INVALID");
                }
              }
            } else {
              if (!array_key_exists($document_id, $reservation_data["documents"])) {
                $err_obj->set_error("document_id", "DOCUMENT_INVALID");
              }
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR",
                    $this->get_error_info($err_obj));
        } else {
          if(substr($document_id, 0, 3) === 'MC_') {
            require_once ("classes/dbi/meeting_clip.dbi.php");
            $obj_meetingClip = new MeetingClipTable($this->get_dsn());
            $where = sprintf( "meeting_clip_key='%s' AND is_deleted=0", $clip_key );
            $data = array(
                    "meeting_clip_name" => $this->request->get("name"),
                    "updatetime" => date("Y-m-d H:i:s"),
                );
            $obj_meetingClip->update($data, $where);
          } else {
            $data = array(
                "name" => $this->request->get("name"),
                "sort" => $this->request->get("sort"),
            );
            $this->objReservation->updateDocument($document_id, $data);
          }
            return $this->output();
        }
    }

    /**
     * 削除
     */
    function action_delete_document ()
    {
        $request = $this->request->getAll();
        $reservation_session = $this->request->get(API_RESERVATION_ID);
        $document_id = $this->request->get("document_id");
        $user_info = $this->session->get("user_info");
        $rules = array(
                API_RESERVATION_ID => array(
                        "required" => true
                ),
                "document_id" => array(
                        "required" => true
                ),
                "output_type" => array(
                        "allow" => $this->get_output_type_list()
                )
        );
        $err_obj = $this->error_check($request, $rules);
        if (! $reservation_data = $this->objReservation->getDetail(
                $reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID,
                    "RESERVATION_SESSION_INVALID");
        } else {
            $info = $reservation_data["info"];
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID,
                        "RESERVATION_SESSION_INVALID");
            }
            if(substr($document_id, 0, 3) === 'MC_') {
              $clip_files = $this->getMeetingClips($reservation_data['info']['meeting_key']);
              if(!$clip_files) {
                $err_obj->set_error("document_id", "DOCUMENT_INVALID");
              } else {
                $clip_key = 0;
                foreach($clip_files as $clip) {
                  if($document_id == $clip["clip_key"]) {
                    $clip_key = $clip["meeting_clip_key"];
                    break;
                  }
                }
                if($clip_key == 0) {
                  $err_obj->set_error("document_id", "CLIP_INVALID");
                }
              }
            } else {
              if (!array_key_exists($document_id, $reservation_data["documents"])) {
                $err_obj->set_error("document_id", "DOCUMENT_INVALID");
              }
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR",
                    $this->get_error_info($err_obj));
        } else {
           if(substr($document_id, 0, 3) === 'MC_') {
            require_once ("classes/dbi/meeting_clip.dbi.php");
            $obj_meetingClip = new MeetingClipTable($this->get_dsn());
            $where = sprintf( "meeting_clip_key='%s' AND is_deleted=0", $clip_key );
            $data = array(
                    "is_deleted" => "1",
                    "updatetime" => date("Y-m-d H:i:s"),
                );
            $obj_meetingClip->update($data, $where);
          } else {
            //require_once ("classes/N2MY_Document.class.php");
            //$obj_N2MYDocument = new N2MY_Document($this->get_dsn());
            $document_info =  $reservation_data["documents"][$document_id];//$obj_N2MYDocument->getDetail($document_id);
            $this->logger2->debug($document_info);
            $this->objReservation->deleteDocument($document_info);
          }
            return $this->output();
        }
    }

    /**
     * 一覧
     */
    function action_get_document ()
    {
        $request = $this->request->getAll();
        $reservation_session = $this->request->get(API_RESERVATION_ID);
        $user_info = $this->session->get("user_info");
        $rules = array(
                API_RESERVATION_ID => array(
                        "required" => true
                ),
                "output_type" => array(
                        "allow" => $this->get_output_type_list()
                )
        );
        $err_obj = $this->error_check($request, $rules);
        if (! $reservation_data = $this->objReservation->getDetail(
                $reservation_session)) {
            $err_obj->set_error(API_RESERVATION_ID,
                    "RESERVATION_SESSION_INVALID");
        } else {
            $info = $reservation_data["info"];
            // ログインユーザ以外の予約
            if ($info["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_RESERVATION_ID,
                        "RESERVATION_SESSION_INVALID");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR",
                    $this->get_error_info($err_obj));
        } else {
            $document_list = array();
            if ($reservation_data["documents"]) {
                foreach ($reservation_data["documents"] as $_document) {
                    switch ($_document["status"]) {
                        case DOCUMENT_STATUS_WAIT:
                            $_document["status"] = "uploading";
                            break;
                        case DOCUMENT_STATUS_DO:
                            $_document["status"] = "pending";
                            break;
                        case DOCUMENT_STATUS_SUCCESS:
                            $_document["status"] = "done";
                            break;
                        case DOCUMENT_STATUS_DELETE:
                            $_document["status"] = "delete";
                            break;
                        case DOCUMENT_STATUS_ERROR:
                        default:
                            $_document["status"] = "error";
                            break;
                    }
                    unset($_document["create_datetime"]);
                    unset($_document["update_datetime"]);
                    $document_list[] = $_document;
                }
            }
            $data["documents"]["document"] = $document_list;

            $clip_files = $this->getMeetingClips($reservation_data['info']['meeting_key']);
            if($clip_files) {
              foreach($clip_files as $clip) {
                $clip_data["meeting_clip_name"] = $clip["meeting_clip_name"];
                $clip_data["clip_key"] = $clip["clip_key"];
                $data["clips"]["clip"][] = $clip_data;
              }
            }
            return $this->output($data);
        }
    }

    function action_preview_document ()
    {
        require_once ("classes/N2MY_Document.class.php");
        require_once ("classes/dbi/meeting.dbi.php");
        $objDocument = new N2MY_Document($this->get_dsn());
        $objMeeting = new MeetingTable($this->get_dsn());

        $request = $this->request->getAll();
        $document_id = $this->request->get("document_id");
        $page = $this->request->get("page", 1);
        $user_info = $this->session->get("user_info");

        // 入力チェック
        $rules = array(
                "document_id" => array(
                        "required" => true
                ),
                "page" => array(
                        "required" => true,
                        "integer" => true
                ),
                "output_type" => array(
                        "allow" => $this->get_output_type_list()
                )
        );
        $err_obj = $this->error_check($request, $rules);
        $documentInfo = $objDocument->getDetail($document_id);
        if (! $documentInfo) {
            $err_obj->set_error("document_id", "NOT_FOUND");
        } else {
            $where = sprintf("meeting_key='%s' AND is_deleted=0",
                    $documentInfo["meeting_key"]);
            $document_user_key = $objMeeting->getOne($where, "user_key");
            if ($document_user_key != $user_info["user_key"]) {
                $err_obj->set_error("document_id", "DOCUMENT_ID_INVALID");
            }
        }
        if (EZValidator::isError($err_obj)) {
            // エラー
            return $this->display_error(0, "PARAMETER_ERROR",
                    $this->get_error_info($err_obj));
        } else {
            // ファイル情報取得
            $fileInfo = $objDocument->getPreviewFileInfo($documentInfo, $page);
            // resize
            $max_width = 480;
            $max_height = 320;
            $percent = 0.5;

            list ($width, $height) = getimagesize($fileInfo["targetImage"]);
            $x_ratio = $max_width / $width;
            $y_ratio = $max_height / $height;

            if (($width <= $max_width) && ($height <= $max_height)) {
                $tn_width = $width;
                $tn_height = $height;
            } elseif (($x_ratio * $height) < $max_height) {
                $tn_height = ceil($x_ratio * $height);
                $tn_width = $max_width;
            } else {
                $tn_width = ceil($y_ratio * $width);
                $tn_height = $max_height;
            }
            header('Content-type: image/jpeg');
            // 再サンプル
            $image_p = imagecreatetruecolor($tn_width, $tn_height);
            $image = imagecreatefromjpeg($fileInfo["targetImage"]);
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $tn_width,
                    $tn_height, $width, $height);
            imagejpeg($image_p, null, 100);
        }
    }

    function get_reservation_list ($options = array(), $user_timezone = null)
    {
        if (! $where = $this->set_where($options)) {
            return array();
        }
        $this->logger2->debug($where);
        $limit = isset($options["limit"]) ? $options["limit"] : null;
        $offset = isset($options["offset"]) ? $options["offset"] : null;
        if (isset($options["sort_key"])) {
            $sort = array(
                    $options["sort_key"] => $options["sort_type"]
            );
        } else {
            $sort = array();
        }
        require_once ("classes/dbi/reservation.dbi.php");
        $obj_Reservation = new ReservationTable($this->get_dsn());
        $reservations = $obj_Reservation->getList($where, $sort, $limit,
                $offset);
        if (DB::isError($reservations)) {
            $this->logger->error($reservations->getUserInfo());
            return false;
        }
        $room_info = $this->session->get("room_info");
        $reservation_list = array();
        // 一覧表示
        foreach ($reservations as $reservation_info) {
            if (strtotime($reservation_info['reservation_starttime']) <
                     strtotime("now") &&
                     strtotime($reservation_info['reservation_extend_endtime']) >
                     strtotime("now")) {
                $status = "now"; // 開催中
            } elseif (strtotime($reservation_info['reservation_extend_endtime']) <
                     strtotime("now")) {
                $status = "end"; // 終了
            } else {
                $status = "wait"; // 開催待
            }
            $reservation_info["status"] = $status;
            if ($user_timezone !== "") {
                $reservation_info["reservation_starttime"] = EZDate::getLocateTime(
                        $reservation_info["reservation_starttime"],
                        $user_timezone, N2MY_SERVER_TIMEZONE);
                $reservation_info["reservation_endtime"] = EZDate::getLocateTime(
                        $reservation_info["reservation_extend_endtime"], $user_timezone,
                        N2MY_SERVER_TIMEZONE);
            }
            $_room_info = $room_info[$reservation_info["room_key"]];
            $reservation_info["max_seat"] = $_room_info["room_info"]["max_seat"];
            $reservation_list[] = $reservation_info;
        }
        return $reservation_list;
    }

    function get_reservation_detail_list ($options = array(), $user_timezone = null)
    {
        if (! $where = $this->set_where($options)) {
            return array();
        }
        $this->logger2->info($where);
        $limit = isset($options["limit"]) ? $options["limit"] : null;
        $offset = isset($options["offset"]) ? $options["offset"] : null;
        if (isset($options["sort_key"])) {
            $sort = array(
                    $options["sort_key"] => $options["sort_type"]
            );
        } else {
            $sort = array();
        }
        require_once ("classes/dbi/reservation.dbi.php");
        $obj_Reservation = new ReservationTable($this->get_dsn());
        $reservations = $obj_Reservation->getList($where, $sort, $limit,
                $offset);
        if (DB::isError($reservations)) {
            $this->logger->error($reservations->getUserInfo());
            return false;
        }
        $reservation_list = array();
        // 一覧表示
        $room_info = $this->session->get("room_info");
        $room_keys = array_keys($room_info);
        foreach ($reservations as $reservation_info) {
            if (strtotime($reservation_info['reservation_starttime']) <
                     strtotime("now") &&
                     strtotime($reservation_info['reservation_extend_endtime']) >
                     strtotime("now")) {
                $status = "now"; // 開催中
            } elseif (strtotime($reservation_info['reservation_extend_endtime']) <
                     strtotime("now")) {
                $status = "end"; // 終了
            } else {
                $status = "wait"; // 開催待
            }
            $reservation_info["status"] = $status;
            if ($user_timezone !== "") {
                $reservation_info["reservation_starttime"] = EZDate::getLocateTime(
                        $reservation_info["reservation_starttime"],
                        $user_timezone, N2MY_SERVER_TIMEZONE);
                $reservation_info["reservation_endtime"] = EZDate::getLocateTime(
                        $reservation_info["reservation_extend_endtime"], $user_timezone,
                        N2MY_SERVER_TIMEZONE);
            }
            $_room_info = $room_info[$reservation_info["room_key"]];
            $reservation_info["max_seat"] = $_room_info["room_info"]["max_seat"];
            $reservation_info["guests"] = $this->objReservation->getGuestsList($reservation_info["reservation_key"]);
            $reservation_list[] = $reservation_info;
        }
        return $reservation_list;
    }

    function action_send_reservation_id ()
    {
      $request = $this->request->getAll();
      $reservation_id = $this->request->get(API_RESERVATION_ID);
      //$reservation_sessions = split(",", $_reservation_session);
      //$this->logger2->info($reservation_sessions);
      $user_info = $this->session->get("user_info");
      $rules = array(
          API_RESERVATION_ID => array(
              "required" => true
          ),
          "output_type" => array(
              "allow" => $this->get_output_type_list()
          )
      );
      $err_obj = $this->error_check($request, $rules);
//     	$err_message = "";
//     	$no_sendable_reservation = 1;
//     	foreach($reservation_sessions as $reservation_session) {
//     		if (! $reservation_data = $this->objReservation->getDetail(
//     				$reservation_session)) {
//     			$err_message = "RESERVATION_SESSION_INVALID";
//     		} else {
//     			$info = $reservation_data["info"];
//     			// ログインユーザ以外の予約
//     			if ($info["user_key"] != $user_info["user_key"]) {
//     				$err_message = "RESERVATION_SESSION_INVALID";
//     			} else {
//     				$no_sendable_reservation = 0;
//     				$reservations[] = $reservation_data;
//     			}
//     		}
//     	}
//     	if($err_message && $no_sendable_reservation){
//     		$err_obj->set_error(API_RESERVATION_ID,
//     				$err_message);
//     	}
      if (EZValidator::isError($err_obj)) {
        return $this->display_error(0, "PARAMETER_ERROR",
            $this->get_error_info($err_obj));
      } else {
        require_once 'classes/dbi/mcu_notification.dbi.php';
        $obj_McuNotification = new McuNotificationTable(N2MY_MDB_DSN);
        $last_mcu_notification = $obj_McuNotification->getRow("","*",array("server_down_datetime" => "desc"));
        $update_data = array(
          "reservation_ids" => $reservation_id,
          "update_datetime" => date('Y-m-d H:i:s'),
            );
        $where = "mcu_notification_key = '".addslashes($last_mcu_notification["mcu_notification_key"])."'";
        $obj_McuNotification->update($update_data, $where);
        $this->logger2->info($update_data);

//     		//会議名、開始及び終了時間、予約者をメール情報で送る
//     		$send_reservations = array();
//     		$now_reservations = array();
//     		foreach($reservations as $key => $reservation_data) {
//     			$this->logger2->info($reservation_data);
//     			//有効している（終わってない（開催中も含め））予約のみ送信
//     			if($reservation_data["info"]["status"] != "end") {
//     				$reservation_data["info"]["guest_count"] = count($reservation_data["guests"]);
//     				require_once 'classes/dbi/member.dbi.php';
//     				$obj_Member = new MemberTable($this->get_dsn());
//     				$where = "member_status = 0 AND member_key = '".addslashes($reservation_data["info"]["member_key"])."'";
//     				$member_info = $obj_Member->getRow($where);
//     				$reservation_info["member_info"] = $member_info;
//     				$reservation_info["info"] = $reservation_data["info"];
//     				if($reservation_data["info"]["status"] == "wait") {
//     					$send_reservations[] = $reservation_info;
//     				} else if($reservation_data["info"]["status"] == "now") {
//     					//extend参加人数
//     					require_once("classes/dbi/reservation_extend.dbi.php");
//     					$obj_reservationExtend = new ReservationExtendTable($this->get_dsn());
//     					$reservation_extends = $obj_reservationExtend->getRowsAssoc("reservation_session_id = '".$reservation_info["info"]["reservation_session"]."'");
//     					$this->logger2->info($reservation_extend);
//     					foreach ($reservation_extends as $reservation_extend) {
//     						if($reservation_extend["extend_seat"]) {
//     							$reservation_info["info"]["max_seat"] += $reservation_extend["extend_seat"];
//     						}
//     					}
//     					$now_reservations[] = $reservation_info;
//     				}

//     				//mcu_down_flgを立つ
//     				$data["mcu_down_flg"] = 1;
//     				$where = "reservation_key = '".addslashes($reservation_data["info"]["reservation_key"])."'";
//     				require_once ("classes/dbi/reservation.dbi.php");
//     				$obj_Reservation = new ReservationTable($this->get_dsn());
//     				$obj_Reservation->update($data, $where);
//     			}
//     		}
//     		//１５分内落ちたMCU Serverチェック
//     		require_once("classes/mgm/dbi/media_mixer.dbi.php");
//     		$objMcuServer = new McuServerTable(N2MY_MDB_DSN);
//     		$where = "system_down_flg = 1 AND system_down_datetime > '".date('Y-m-d H:i:s' , strtotime("- 15 min"))."'";
//     		$this->logger2->info($where);
//     		$mcu_servers = $objMcuServer->getRowsAssoc($where);

//     		require_once ("lib/EZLib/EZMail/EZSmtp.class.php");
//     		$mail = new EZSmtp(null, $lang_cd, "UTF-8");
//     		$mail->setSubject("ＶＡ 会議システム　予約再割り振り実施のお知らせ");
//     		$mail->setFrom(N2MY_RETRY_RESERVATION_FROM);
//     		$mail->setReturnPath(NOREPLY_ADDRESS);
//     		$mail->setTo(N2MY_RETRY_RESERVATION_TO);
//     		$template = new EZTemplate($this->config->getAll('SMARTY_DIR'));
//     		$template->assign('reservations', $send_reservations);
//     		$template->assign('now_reservations', $now_reservations);
//     		if($mcu_servers) {
//     			$template->assign('has_mcu_server_down', 1);
//     		} else {
//     			$template->assign('has_mcu_server_down', 0);
//     		}
//     		$body = $template->fetch('common/mail/send_reservation.t.txt');
//     		$this->logger2->info($body);
//     		$mail->setBody($body);
//     		$mail->send();

        return $this->output();
      }
    }

    function set_where ($options = array())
    {
        if (empty($options["user_key"])) {
            $this->logger->error(array(
                    $options
            ), "Parameter error!!");
            return "";
        }
        // 基本条件
        $cond = array();
        $cond[] = "reservation_status = 1";
        if (isset($options["user_key"])) {
            $cond[] = "user_key = '" . addslashes($options["user_key"]) . "'";
        }
        if (isset($options["start_time"]) || isset($options["end_time"])) {
            $start_limit = $options["start_time"] ? mysql_real_escape_string(
                    $options["start_time"]) : "0000-00-00 00:00:00";
            $end_limit = $options["end_time"] ? mysql_real_escape_string(
                    $options["end_time"]) : "9999-12-31 23:59:59";
            $cond[] = "(" .
                     "( reservation_starttime between '$start_limit' AND '$end_limit' )" .
                     " OR ( reservation_extend_endtime between '$start_limit' AND '$end_limit' )" .
                     " OR ( '$start_limit' BETWEEN reservation_starttime AND reservation_extend_endtime )" .
                     " OR ( '$end_limit' BETWEEN reservation_starttime AND reservation_extend_endtime ) )";
        }
        if (isset($options["updatetime"])) {
            $updatetime = $options["updatetime"];
            $cond[] = "( reservation_registtime >= '$updatetime')" .
                     " OR ( reservation_updatetime >= '$updatetime' )";
        }
        if (isset($options["status"])) {
            switch ($options["status"]) {
                case 'wait':
                    $cond[] = "reservation_starttime > '" .
                             date('Y-m-d H:i:s') . "'";
                    break;
                case 'now':
                    $cond[] = "'" . date('Y-m-d H:i:s') .
                             "' BETWEEN reservation_starttime AND reservation_extend_endtime";
                    break;
                case 'end':
                    $cond[] = "reservation_starttime < '" .
                             date('Y-m-d H:i:s') . "'";
                    break;
                case 'valid':
                    $start_limit = date('Y-m-d H:i:s');
                    $end_limit = "9999-12-31 23:59:59";
                    $cond[] = "(" .
                             "( reservation_starttime between '$start_limit' AND '$end_limit' )" .
                             " OR ( reservation_extend_endtime between '$start_limit' AND '$end_limit' )" .
                             " OR ( '$start_limit' BETWEEN reservation_starttime AND reservation_extend_endtime )" .
                             " OR ( '$end_limit' BETWEEN reservation_starttime AND reservation_extend_endtime ) )";
                    break;
            }
        }
        if ($options["reservation_name"]) {
            $reservation_name = $options["reservation_name"];
            $cond[] = "reservation_name LIKE '%" .
                     addslashes($reservation_name) . "%'";
        }

        if ($options["room_key"]) {
            $room_key = $options["room_key"];
            $cond[] = "room_key = '" . addslashes($room_key) . "'";
        }

        $where = join(" AND ", $cond);

        if ($options["member_id"] || $options["member_name"] ) {
            $member_id = $options['member_id'];
            $member_name = $options["member_name"];
            $where = "(" . $where .
                     " AND member_key IN (SELECT member.member_key FROM member WHERE member.member_id LIKE '%" .
                     addslashes($member_id) . "%' AND (member.member_name LIKE '%" .
                     addslashes($member_name) . "%' OR member.member_name_kana LIKE '%" .
                     addslashes($member_name) . "%')))";
            /*
            $where = "(" . $where .
                     " AND reservation_key IN (SELECT reservation_user.reservation_key " .
                     "FROM reservation_user WHERE reservation_user.r_member_key" .
                     " IN (SELECT member.member_key FROM member WHERE member.member_id LIKE '%" .
                     addslashes($member_id) . "%')))" . "OR (" . $where .
                     " AND member_key IN (SELECT member.member_key FROM member WHERE member.member_id LIKE '%" .
                     addslashes($member_id) . "%'))";
            */
        }

        // ユーザ全件
        return $where;
    }

    function _format_guests ($_guests, $is_sales = null)
    {
        $guests = array();
        if ($_guests) {
            if ($is_sales) {
                foreach ($_guests as $key => $guest) {
                    $this->logger2->info($guest);
                    if ($guest["member_key"]) {
                        $staff = array(
                                "invite_url" => N2MY_BASE_URL . "r/" .
                                         $guest["r_user_session"] . '&c=' .
                                         $country_key["country_key"] . '&lang=' .
                                         $guest["lang"],
                                        API_GUEST_ID => $guest["r_user_session"]
                        );
                        $guests["staff"] = $staff;
                    } else {
                        $customer = array(
                                "invite_url" => N2MY_BASE_URL . "r/" .
                                         $guest["r_user_session"] . '&c=' .
                                         $country_key["country_key"] . '&lang=' .
                                         $guest["lang"],
                                        API_GUEST_ID => $guest["r_user_session"]
                        );
                        $guests["customer"] = $customer;
                    }
                }
            } else {

                $country_key = $this->session->get("country_key");
            foreach ($_guests as $key => $guest) {
              if($guest["r_organizer_flg"] == 0){
                $guests[] = array(
                    "invite_url" => N2MY_BASE_URL."r/".$guest["r_user_session"].'&c='.$country_key.'&lang='.$guest["lang"],
                    API_GUEST_ID => $guest["r_user_session"],
                );
              }
            }
            }
        }
        return $guests;
    }

    function _format_organizer($_organizer){
      if($_organizer){
        $formatted_organizer = array();
        foreach ($_organizer as $organizer_attr => $organizer_value){
        $formatted_organizer[substr($organizer_attr, 9, strlen($organizer_attr)-1)] = $organizer_value;
        }
      }

      return $formatted_organizer;
    }

}
$main = new N2MY_Meeting_Resevation_API();
$main->execute();
