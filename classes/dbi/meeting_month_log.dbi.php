<?php
/*
 * Created on 2006/09/19
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

require_once("classes/N2MY_DBI.class.php");
require_once("lib/EZLib/EZXML/EZXML.class.php");
require_once("lib/EZLib/EZUtil/EZUrl.class.php");

class MeetingMonthLogTable extends N2MY_DB {

    var $table = "meeting_month_log";
    var $logger = null;

    var $rules = array(
        "log_no"       		=> array("required" => true),
        "log_date"         	=> array("required" => true),
        "meeting_cnt"     	=> array("required" => true),
        "use_time"    		=> array("required" => true),
        "participant_cnt"   => array("required" => true)
    );

    function MeetingMonthLogTable($dsn)
    {
        $this->init($dsn, $this->table);
    }


    function update($month, $user_id, $room_key, $type, $data) {
        $where = "log_date = '".addslashes($month)."'" .
                " AND user_id = '".addslashes($user_id)."'" .
                " AND room_key = '".addslashes($room_key)."'" .
                " AND type = '".addslashes($type)."'";
        // すでにデータが存在する場合
        if ($this->numRows($where) > 0) {
            // 更新
            $this->logger->debug(__FUNCTION__,__FILE__,__LINE__, array($where, $data));
            $ret = parent::update($data, $where);
        } else {
            // 追加
            $this->logger->debug(__FUNCTION__,__FILE__,__LINE__, array($where, $data));
#            $data["create_datetime"] = date("Y-m-d H:i:s");
            $ret = parent::add($data);
        }
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret);
        }
    }


    /**
     * core APIの結果取得
     *
     * @param string $path 実行先のパス
     * @param array $param パラメタ
     * @return string core側のurl（指定がなければベースURl）
     */
    function _get_data($path = "", $param = array()) {
        $query_str = "";
        foreach ($param as $_key => $_value) {
            $query_str .= "&" . $_key . "=" . urlencode($_value);
        }
        if ($query_str) {
            $query_str = "?" . substr($query_str, 1);
        }
        $url = $this->base_url."/".$path.$query_str;
        // 実行
        $ezurl = new EZUrl();
        $contents = $ezurl->wget($url);
        return $contents;
    }


}
?>
