<?php
require_once(dirname(__FILE__)."/../params/WSDLParams.php");

class SetCompositionTypeParams extends WSDLParams {
	protected $confId;
	protected $mosaicId;
	protected $compType;
	protected $size;
	protected $profileId;
	public function __construct($obj) {
		parent::__construct($this, $obj);
	}
	
	public function getParams() {
		return parent::getParams($this);
	}
}