-- 部屋ごとのセキュリティ設定
ALTER TABLE `room` ADD `default_layout` VARCHAR( 20 ) NULL COMMENT 'レイアウト' AFTER `mfp`;
ALTER TABLE `room` ADD `rtmp_protocol` TEXT NULL COMMENT 'RTMPプロトコル' AFTER `default_layout`;
ALTER TABLE `room` ADD `whiteboard_page` INT NULL  DEFAULT '100' COMMENT '資料枚数' AFTER `default_layout`;
ALTER TABLE `room` ADD `whiteboard_size` INT DEFAULT '20' NULL COMMENT '資料サイズ' AFTER `whiteboard_page`;
ALTER TABLE `room` ADD `whiteboard_filetype` TEXT NULL COMMENT '資料ファイルタイプ' AFTER `whiteboard_size`;
ALTER TABLE `room` ADD `cabinet_size` INT NULL DEFAULT '20' COMMENT 'キャビネットサイズ' AFTER `whiteboard_filetype`;
ALTER TABLE `room` ADD `cabinet_filetype` TEXT NULL COMMENT '資料ファイルタイプ' AFTER `cabinet_size`;
ALTER TABLE `room` ADD `is_auto_transceiver` INT( 1 ) NULL DEFAULT 0 COMMENT 'トランシーバーモード' AFTER `cabinet_filetype`;
ALTER TABLE `room` ADD `is_auto_voice_priority` INT( 1 ) NULL DEFAULT 0 COMMENT '音声優先モード' AFTER `is_auto_transceiver`;
ALTER TABLE `room` ADD `is_netspeed_check` VARCHAR( 10 ) NULL DEFAULT '' COMMENT '回戦速度チェック' AFTER `is_auto_voice_priority`;
ALTER TABLE `room` CHANGE `is_netspeed_check` `is_netspeed_check` VARCHAR( 10 ) NULL DEFAULT '' COMMENT '回戦速度チェック';

-- 操作ログ
CREATE TABLE IF NOT EXISTS `operation_log` (
  `operation_key` int(11) NOT NULL auto_increment COMMENT 'キー',
  `user_key` int(11) NOT NULL COMMENT 'ユーザーキー',
  `member_key` int(11) default NULL COMMENT 'メンバーキー',
  `member_id` text COMMENT 'メンバーID',
  `action_name` varchar(255) NOT NULL COMMENT 'アクション',
  `remote_addr` text NOT NULL,
  `operation_datetime` datetime NOT NULL COMMENT '操作日付',
  `info` text NOT NULL COMMENT '操作内容',
  `create_datetime` datetime NOT NULL COMMENT '作成日時',
  PRIMARY KEY  (`operation_key`),
  KEY `user_key` (`user_key`,`member_key`,`action_name`,`operation_datetime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='操作履歴';

-- 参加者のポート、プロトコル
ALTER TABLE `participant` ADD `participant_protocol` VARCHAR( 20 ) NULL COMMENT 'プロトコル' AFTER `participant_locale` ;
ALTER TABLE `participant` ADD `participant_port` VARCHAR( 20 ) NULL COMMENT 'ポート' AFTER `participant_protocol` ;

-- 機能制限
ALTER TABLE `reservation` ADD `is_multicamera` TINYINT NOT NULL DEFAULT '0' COMMENT 'マルチカメラ 0:使用しない、1:使用する' AFTER `reservation_info` ;
ALTER TABLE `reservation` ADD `is_limited_function` TINYINT NOT NULL DEFAULT '0' COMMENT '機能制限 0:使用しない、1:使用する' AFTER `is_multicamera`;
ALTER TABLE `reservation` ADD `is_telephone` TINYINT NOT NULL DEFAULT '0' COMMENT '音声通話 0:使用しない、1:使用する' AFTER `is_limited_function`;
ALTER TABLE `reservation` ADD `is_h323` TINYINT NOT NULL DEFAULT '0' COMMENT 'H.323 0:使用しない、1:使用する' AFTER `is_telephone`;
ALTER TABLE `reservation` ADD `is_mobile_phone` TINYINT NOT NULL DEFAULT '0' COMMENT '携帯 0:使用し>ない、1:使用する' AFTER `is_h323`;
ALTER TABLE `reservation` ADD `is_high_quality` TINYINT NOT NULL DEFAULT '0' COMMENT '高画質 0:使用しない、1:使用する' AFTER `is_mobile_phone`;
ALTER TABLE `reservation` ADD `is_desktop_share` TINYINT NOT NULL DEFAULT '0' COMMENT '共有 0:使用しない、1:使用>する' AFTER `is_high_quality`;
ALTER TABLE `reservation` ADD `is_meeting_ssl` TINYINT NOT NULL DEFAULT '0' COMMENT '暗号化 0:使用しない、1:使用す>る' AFTER `is_desktop_share`;
ALTER TABLE `reservation` ADD `is_invite_flg` TINYINT NOT NULL DEFAULT '0' COMMENT '会議室内招待 0:使用しない、1:使>用する' AFTER `is_meeting_ssl`;
ALTER TABLE `reservation` ADD `is_rec_flg` TINYINT NOT NULL DEFAULT '0' COMMENT '録画 0:使用しない、1:使用する' AFTER `is_invite_flg`;
ALTER TABLE `reservation` ADD `is_cabinet_flg` TINYINT NOT NULL DEFAULT '0' COMMENT 'キャビネット 0:使用しない、1:使用する' AFTER `is_rec_flg`;
ALTER TABLE `reservation` ADD `reservation_pw_type` TINYINT NOT NULL DEFAULT '1' COMMENT 'パスワードタイプ 1:会議と記録 2:記録のみ' AFTER `reservation_pw`;

-- 参加者のプロトコル
ALTER TABLE `participant` ADD `participant_protocol` VARCHAR( 20 ) NULL COMMENT 'プロトコル' AFTER `participant_locale` ;
ALTER TABLE `participant` ADD `participant_port` VARCHAR( 20 ) NULL COMMENT 'ポート' AFTER `participant_protocol` ;

-- セキュリティー情報
ALTER TABLE `user` ADD `is_compulsion_pw` INT NULL COMMENT '強制パスワードフラグ' AFTER `user_deletetime`;
ALTER TABLE `user` ADD `compulsion_pw` VARCHAR( 255 ) NULL COMMENT '会議デフォルトPW' AFTER `is_compulsion_pw`;
ALTER TABLE `user` ADD `is_minutes_delete` INT NULL COMMENT '自動議事録削除' AFTER `compulsion_pw`;

-- エコーキャンセラー
CREATE TABLE IF NOT EXISTS `echo_canceler_log` (
  `meeting_key` int(10) NOT NULL,
  `participant_id` varchar(255) NOT NULL,
  `machine_id` int(64) NOT NULL,
  `start_datetime` datetime NOT NULL,
  KEY `meeting_key` (`meeting_key`,`participant_id`,`machine_id`,`start_datetime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
