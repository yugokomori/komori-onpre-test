 <?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once ('classes/AppFrame.class.php');
require_once ('classes/mgm/dbi/user.dbi.php');
require_once("classes/N2MY_Account.class.php");
require_once ('classes/dbi/member.dbi.php');
require_once ('classes/dbi/member_group.dbi.php');
require_once ('classes/dbi/member_status.dbi.php');
require_once ('classes/dbi/user.dbi.php');
require_once ('config/config.inc.php');
require_once ('lib/EZLib/EZUtil/EZEncrypt.class.php');
require_once( "lib/EZLib/EZCore/EZLogger2.class.php" );
require_once ('lib/EZLib/EZUtil/EZCsv.class.php');
require_once ('classes/dbi/video_conference_ip_list.dbi.php');

class AppMember extends AppFrame
{
    var $logger = null;
    var $_name_space = null;
    var $videoCnferenceTable = null;
    var $groupTable = null;
    var $statusTable = null;
    var $account_dsn = null;
    var $max_address_cnt = null;

    function init() {
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->videoCnferenceTable = new VideoConferenceIpListTable($this->get_dsn());
        $this->groupTable = new MemberGroupTable($this->get_dsn());
        $this->_name_space = md5(__FILE__);
        $this->max_address_cnt  = 50000;
    }

    /**
     * ログイン認証
     */
    function auth() {
        $this->checkAuth();
        $this->checkAdminAuth('admin/session_error.t.html');
    }
    /**
     * デフォルト処理
     */
    function default_view() {
        $this->logger->trace("test", __FILE__, __LINE__, __FUNCTION__);
        $this->action_show_top();
    }

    function action_show_top()
    {
        $this->display('admin/member/index.t.html');
    }

    function action_admin_room($message = ""){
        $this->session->remove("conditions");
        $this->render_admin_room();
    }

    function action_search_member() {
        $this->setCond();
        $this->render_admin_room();
    }

    function action_pager() {
        $this->render_admin_room();
    }

    function render_admin_room($message = ""){

        if($message){
            $this->template->assign('message', $message);
        }
        //user_key取得
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];

        //user_keyからメンバーを取得
        $conditions = $this->getCond();
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$conditions);
        $sort_key  = ($conditions["sort_key"]) ? $conditions["sort_key"] : "video_conference_ip_list_key";
        $sort_type = ($conditions["sort_type"]) ? $conditions["sort_type"] : "asc";
        $sort_key_rules = array("video_conference_ip_list_key","room_num","connect_ip","priority");
        if (!EZValidator::valid_allow($sort_key, $sort_key_rules)) {
            $sort_key = "video_conference_ip_list_key";
        }
        $sort_type_rules = array("asc","desc");
        if (!EZValidator::valid_allow($sort_type, $sort_type_rules)) {
            $sort_type = "desc";
        }
        $limit = $this->request->get("page_cnt");
        if (!$limit) {
            $limit = 10;
        }
        $page      = $this->request->get("page", 1);
        $offset    = ($page - 1) * $limit;
        // 条件
        $where = "user_key = ".addslashes($user_key).
            " AND status = 1";
//         if ($conditions["member_group"]) {
//             $where .= " AND member_group = ".addslashes($conditions["member_group"]);
//         }
        if ($conditions["room_num"]) {
            $where .= " AND (room_num like '%".addslashes($conditions["room_num"])."%'";
            $where .= " OR connect_ip like '%".addslashes($conditions["room_num"])."%'";
            $where .= " OR priority like '%".addslashes($conditions["room_num"])."%')";
        }

        $room_list = $this->videoCnferenceTable->getRowsAssoc($where, array($sort_key => $sort_type), $limit, $offset);
        $count = $this->videoCnferenceTable->numRows($where);
//        $obj_MgmUserTable = new MgmUserTable( $this->get_auth_dsn() );
        $room_info = array();
        foreach( $room_list as $room ){
            $room_info[] = array(
//                           "auth_user_key" => $obj_MgmUserTable->getOne( sprintf( "user_id='%s'", $room["member_id"] ), "user_key" ),
                            "status"     =>  $room['status'],
                            "key"        => $room['video_conference_ip_list_key'],
                            "connect_ip" => $room['connect_ip'],
                            "priority" => $room['priority'],
                            "room_num"          => $room['room_num'],
                            );
        }
        $add_flg = ( ($user_info["account_model"] == "room" || $user_info["account_model"] == "centre") &&
            $user_info["max_member_count"] <= count( $room_info ) ) ? false : true ;
        $this->template->assign("add_flg", $add_flg );
        $this->template->assign('info', $room_info);
        $this->template->assign('vcubeid_add_use_flg', $user_info["vcubeid_add_use_flg"]);
        // ページング
        $this->template->assign('conditions', $conditions);
        $pager = $this->setPager($limit, $page, $count);
        $this->template->assign('pager', $pager);
        $this->display('admin/member/roomlist/index.t.html');
    }

    function setCond() {
        $conditions  = $this->request->get("conditions");
        if ($conditions) {
            $this->session->set("conditions", $conditions);
        }
    }

    function getCond() {
        $condition = $this->session->get("conditions");
        return $condition;
    }

    function action_add_room($message = "")
    {
        $room_info = ( 1 == $this->request->get( "action_add_room") ) ? "" : $this->session->get('new_room_info');
        //user_key取得
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];

        //エラー時にユーザインプットの再表示
        if( $room_info ){
            foreach($room_info as $key => $value){
                $this->template->assign($key, $value);
            }
        }
        if($message){
            $this->template->assign('message', $message);
        }

        $this->template->assign('user_info', $user_info);
        //$this->template->assign('member_group', $member_group);
        //$this->template->assign('member_status', $member_status);
        $this->display('admin/member/roomlist/add/index.t.html');
    }

    function action_add_room_confirm()
    {
        $this->set_submit_key($this->_name_space);
        // 入力値を保持
        $request = $this->request->getAll();
        $this->session->set('new_room_info', $request);
        $user_info = $this->session->get( "user_info");

        $message = null;
        $message = $this->check_room_info($request, $user_info);
        if( $message ){
            $this->action_add_room($message);
        } else {
            foreach($request as $key => $value){
                $this->template->assign($key, $value);
            }
            $this->display('admin/member/roomlist/add/confirm.t.html');
        }
    }

    private function _isMemberExist($member_id,$user_key) {
        $where = sprintf( "user_key = '%s' AND member_id='%s'", $user_key, $member_id );
        $whitelist_key = $this->videoCnferenceTable->getOne($where, "whitelist_key");
        return $whitelist_key;
    }

    private function _getMemberCount($user_key) {
        $where = sprintf( "user_key = '%s' AND whitelist_status >='0'", $user_key );
        $member_count = $this->videoCnferenceTable->numRows($where);
        return $member_count;
    }

    private function _isMemberInfo($data, $type = null) {
        $message = array();
        //ID
        $regex_rule = '/^[[:alnum:]._-]{3,20}$/';
        $valid_result = EZValidator::valid_regex($data['member_id'], $regex_rule);
        if ($valid_result == false){
          $message[] = MEMBER_ERROR_ID_LENGTH;
        }
        if(!$data['name']){
            $message[] = MEMBER_ERROR_NAME;
        } elseif (mb_strlen($data['name']) > 50) {
            $message[] = MEMBER_ERROR_NAME_LENGTH;
        }
        if (mb_strlen($data['name_kana']) > 50) {
            $message[] = MEMBER_ERROR_NAME_KANA_LENGTH;
        }
        //経理単位コード
        $regex_rule = '/^[[:alnum:]]{1,8}$/';
        $valid_result = EZValidator::valid_regex($data['accounting_unit_code'], $regex_rule);
        if ($valid_result == false){
            $message[] = ACCOUNTING_ERROR_ID_LENGTH;
        }
        //費用部課コード
        $regex_rule = '/^[[:alnum:]]{1,5}$/';
        $valid_result = EZValidator::valid_regex($data['expense_department_code'], $regex_rule);
        if ($valid_result == false){
            $message[] = DEPARTMENT_ERROR_ID_LENGTH;
        }

        return $message;
    }

    //member_status_key からそのmember_status_nameの取得
    private function _getRoomStatusName($key){
        if ($key == 0) {
            return false;
        } elseif ($key == 1) {
            return true;
        }
    }

    /**
     * numからそのユーザーが存在するかチェック
     */
    private function _isExist($num,$user_key){
        //member, user, 認証DBのuser のテーブルをチェック
//         $obj_UserTable = new UserTable( $this->get_dsn() );
//         $obj_MgmUserTable = new MgmUserTable( $this->get_auth_dsn() );

        if ( $this->videoCnferenceTable->numRows( sprintf("user_key = '%s' AND room_num='%s' AND status=1 ", $user_key,$num ) )  > 0 ){
            return true;
        } else {
            return false;
        }
    }

    private function check_room_info( $data, $user_info){

        //プライオリティ
        $message = null;
        $regex_rule = '/^[1-9]$|^[1-9][0-9]$|^100$/';
        $valid_result = EZValidator::valid_regex($data['priority'], $regex_rule);
        if ($valid_result == false){
            $message .= '<li>'.ROOM_ERROR_PRIORITY_LENGTH.'</li>';
        }

        //会議室番号
        if($data['room_num'] == "*") {
        	$message .= '<li>'.ROOM_ERROR_NUM_LENGTH.'</li>';
        } elseif(strpos($data['room_num'],"*") !== false) {
        	if(mb_substr($data['room_num'], 0, 1) == '*') {
        		$regex_rule = '/^[0-9*]{0,30}$/';
        		$valid_result = EZValidator::valid_regex($data['room_num'], $regex_rule);
        		if ($valid_result == false) {
        			$message .= '<li>'.ROOM_ERROR_NUM_LENGTH.'</li>';
        		} else {
        			$message .= '<li>'.ROOM_ERROR_ASTERISK.'</li>';
        		}
        	} else {
        		$regex_rule = '/^[0-9*]{0,30}$/';
        		$valid_result = EZValidator::valid_regex($data['room_num'], $regex_rule);
        		if($valid_result == false) {
        			$message .= '<li>'.ROOM_ERROR_NUM_LENGTH.'</li>';
        		} else {
        			if(mb_substr($data['room_num'],-1) == "*"){
        			$regex_rule = '/^[1-9][0-9]{0,28}[*]$/';
        			$valid_result = EZValidator::valid_regex($data['room_num'], $regex_rule);
        				if ($valid_result == false) {
        					$message .= '<li>'.ROOM_ERROR_ASTERISK.'</li>';
        				} elseif($this->_isExist($data['room_num'],$user_info['user_key'])) {
        					$message .= '<li>'.ROOM_ERROR_NUM_EXIST . '</li>';
        				}
        			} else {
        				$regex_rule = '/^[0-9*]{0,30}$/';
        				$valid_result = EZValidator::valid_regex($data['room_num'], $regex_rule);
        				if($valid_result == false) {
        					$message .= '<li>'.ROOM_ERROR_NUM_LENGTH.'</li>';
        				} else {
        					$message .= '<li>'.ROOM_ERROR_ASTERISK.'</li>';
        				}
        			}
        		}
        	}
        } else {
            $regex_rule = '/^[1-9]$|^[1-9][0-9]{0,29}$/';
            $valid_result = EZValidator::valid_regex($data['room_num'], $regex_rule);
            if ($valid_result == false){
                $message .= '<li>'.ROOM_ERROR_NUM_LENGTH.'</li>';
            } elseif ($this->_isExist($data['room_num'],$user_info['user_key'])){
                $message .= '<li>'.ROOM_ERROR_NUM_EXIST . '</li>';
            }
        }
        //接続先IPアドレス
//        $regex_rule = '/^[0-9A-Fa-f.:]{0,50}$/';
        $regex_rule = '/^[0-9.]{0,50}$/';
        $valid_result = EZValidator::valid_regex($data['connect_ip'], $regex_rule);
        if(mb_strlen($data['connect_ip']) <= 0 || mb_strlen($data['connect_ip']) > 50) {
            $message .= '<li>'.ROOM_ERROR_IP_LENGTH . '</li>';
        } elseif ($valid_result == true) {
            $regex_rule = '/^(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/';
        	$valid_result = EZValidator::valid_regex($data['connect_ip'], $regex_rule);
        	if ($valid_result == false){
                $message .= '<li>'.ROOM_ERROR_IP_FORMAT.'</li>';
        	}
        	// v6チェック含むバリデーション
//             if ((strpos($data['connect_ip'], '.') == true)) {
//                 $regex_rule = '/^(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/';
//                 $valid_result = EZValidator::valid_regex($data['connect_ip'], $regex_rule);
//                 if ($valid_result == false){
//                     $message .= '<li>'.ROOM_ERROR_IP_FORMAT.'</li>';
//                 }
//             } else {
//                 $regex_rule = '/^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$/ ';
//                 $valid_result = EZValidator::valid_regex($data['connect_ip'], $regex_rule);
//                 if ($valid_result == false){
//                     $message .= '<li>'.ROOM_ERROR_IP_FORMAT.'</li>';
//                 }
//             }
        } elseif ($valid_result == false){
        	$message .= '<li>'.ROOM_ERROR_IP_FORMAT.'</li>';
        }
        return $message;
    }

    /**
     * video_conference_ip_list_keyとroom_numで同一のものがないかチェック
     */
    private function _isExist_edit($key, $num, $user_key ){
        if ( $this->videoCnferenceTable->numRows( sprintf("video_conference_ip_list_key='%s' AND user_key = '%s' AND room_num='%s' ", $key,$user_key,$num )  ) > 0) {
            $this->logger2->info(array( "video_conference_ip_list_key =>".$key ,"room_num => ".$num ));
            return false;
        } elseif ($this->_isExist($num,$user_key)){
            return true;
        }
        return false;
   }

    /**
     * ＤＢにメンバー情報の登録
     */
    function action_add_room_complete()
    {
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            header( "Location: ?action_add_room=1" );
            exit;
        }
        //部屋追加
        $user_info = $this->session->get( "user_info" );
        $request = $this->session->get('new_room_info');

        $user_key = $user_info['user_key'];
        $temp = array(
            "user_key"           => $user_key,
//            "video_conference_ip_list_key" => $request['key'],
            "room_num"    => $request['room_num'],
            "connect_ip" => $request['connect_ip'],
            "priority"    => $request['priority'],
            "status"      => 1,
        );
        $ret = $this->videoCnferenceTable->add( $temp );
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
        } else {
            $this->logger->info(__FUNCTION__."#add video_conference successful!",__FILE__,__LINE__,$temp);
        }

        $this->logger->info(__FUNCTION__."#video_conference_add",__FILE__,__LINE__,$temp);
        $this->session->remove('new_room_info');
        // 操作ログ
        $this->add_operation_log('video_conference_add', array('video_conference_ip_list_key' => $request['key']));
        $this->display('admin/member/roomlist/add/done.t.html');
    }

    function action_edit_room($message = ""){
        if($message){
            $this->template->assign('message', $message);
        }
        //user_key取得
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];

        if($this->request->get('mode') == "edit_room"){
            //member_keyからメンバーを取得
            $key = $this->request->get('key');
            $info = $this->videoCnferenceTable->getRow( sprintf( "user_key='%s' AND video_conference_ip_list_key='%s'", $user_key , $key ) );
            // 不正アクセス対策
            if(!$info){
                $this->logger2->warn(__FUNCTION__, __FILE__, __LINE__,$user_info["user_key"]);
                return $this->action_admin_room();
            }
//            $room_info['auth_user_key']      = $this->request->get( "auth_user_key" );
            $room_info['video_conference_ip_list_key'] = $key;
            $room_info['room_num']          = $info['room_num'];
            $room_info['connect_ip']  = $info['connect_ip'];
            $room_info['priority']    = $info['priority'];
            $room_info['status']      = $info['status'];
        } else {
            $room_info = $this->session->get('m_room_edit_info');
        }
        foreach($room_info as $key => $value){
            $this->template->assign($key, $value);
        }
        $this->display('admin/member/roomlist/edit/index.t.html');
    }

    function action_edit_room_confirm(){
        $this->set_submit_key($this->_name_space);
        // 入力値を保持
        $request = $this->request->getAll();

        $this->session->set('m_room_edit_info', $request);
        $user_info = $this->session->get( "user_info");

        // プライオリティ
        $message = null;
        $regex_rule = '/^[1-9]$|^[1-9][0-9]$|^100$/';
        $valid_result = EZValidator::valid_regex($request['priority'], $regex_rule);
        if ($valid_result == false){
            $message .= '<li>'.ROOM_ERROR_PRIORITY_LENGTH.'</li>';
        }

        //会議室番号
        if($request['room_num'] == "*") {
        	$message .= '<li>'.ROOM_ERROR_NUM_LENGTH.'</li>';
        } elseif(strpos($request['room_num'],"*") !== false) {
        	if(mb_substr($request['room_num'], 0, 1) == '*') {
        		$regex_rule = '/^[0-9*]{0,30}$/';
        		$valid_result = EZValidator::valid_regex($request['room_num'], $regex_rule);
        		if ($valid_result == false) {
        			$message .= '<li>'.ROOM_ERROR_NUM_LENGTH.'</li>';
        		} else {
        			$message .= '<li>'.ROOM_ERROR_ASTERISK.'</li>';
        		}
        	} else {
        		$regex_rule = '/^[0-9*]{0,30}$/';
        		$valid_result = EZValidator::valid_regex($request['room_num'], $regex_rule);
        		if($valid_result == false) {
        			$message .= '<li>'.ROOM_ERROR_NUM_LENGTH.'</li>';
        		} else {
        			if(mb_substr($request['room_num'],-1) == "*"){
        			$regex_rule = '/^[1-9][0-9]{0,28}[*]$/';
        			$valid_result = EZValidator::valid_regex($request['room_num'], $regex_rule);
        				if ($valid_result == false) {
        					$message .= '<li>'.ROOM_ERROR_ASTERISK.'</li>';
        				} elseif($this->_isExist_edit($request['video_conference_ip_list_key'],$request['room_num'],$user_info['user_key'])) {
        					$message .= '<li>'.ROOM_ERROR_NUM_EXIST . '</li>';
        				}
        			} else {
        				$regex_rule = '/^[0-9*]{0,30}$/';
        				$valid_result = EZValidator::valid_regex($request['room_num'], $regex_rule);
        				if($valid_result == false) {
        					$message .= '<li>'.ROOM_ERROR_NUM_LENGTH.'</li>';
        				} else {
        					$message .= '<li>'.ROOM_ERROR_ASTERISK.'</li>';
        				}
        			}
        		}
        	}
        } else {
            $regex_rule = '/^[1-9]$|^[1-9][0-9]{0,29}$/';
            $valid_result = EZValidator::valid_regex($request['room_num'], $regex_rule);
            if ($valid_result == false){
                $message .= '<li>'.ROOM_ERROR_NUM_LENGTH.'</li>';
            } elseif ($this->_isExist_edit($request['video_conference_ip_list_key'],$request['room_num'],$user_info['user_key'])){var_dump($request['room_num']);
            	$message .= '<li>'.ROOM_ERROR_NUM_EXIST . '</li>';var_dump("aaaaaa");
            }
        }
        // 接続先IPアドレス
    //        $regex_rule = '/^[0-9A-Fa-f.:]{0,50}$/';
        $regex_rule = '/^[0-9.]{0,50}$/';
        $valid_result = EZValidator::valid_regex($request['connect_ip'], $regex_rule);
        if(mb_strlen($request['connect_ip']) <= 0 || mb_strlen($request['connect_ip']) > 15) {
            $message .= '<li>'.ROOM_ERROR_IP_LENGTH . '</li>';
        } elseif ($valid_result == true) {
            $regex_rule = '/^(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/';
        	$valid_result = EZValidator::valid_regex($request['connect_ip'], $regex_rule);
        	if ($valid_result == false){
                $message .= '<li>'.ROOM_ERROR_IP_FORMAT.'</li>';
        	}
        	// v6チェック含むバリデーション
//             if ((strpos($request['connect_ip'], '.') == true)) {
//                 $regex_rule = '/^(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/';
//                 $valid_result = EZValidator::valid_regex($request['connect_ip'], $regex_rule);
//                 if ($valid_result == false){
//                     $message .= '<li>'.ROOM_ERROR_IP_FORMAT.'</li>';
//                 }
//             } else {
//                 $regex_rule = '/^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$/ ';
//                 $valid_result = EZValidator::valid_regex($request['connect_ip'], $regex_rule);
//                 if ($valid_result == false){
//                     $message .= '<li>'.ROOM_ERROR_IP_FORMAT.'</li>';
//                 }
//             }
        } elseif ($valid_result == false){
        	$message .= '<li>'.ROOM_ERROR_IP_FORMAT.'</li>';
        }
        if ($message) {
            $this->action_edit_room($message);
        } else {

            foreach($request as $key => $value){
                $this->template->assign($key, $value);
            }
            $this->display('admin/member/roomlist/edit/confirm.t.html');
        }
    }

    function action_edit_room_complete()
    {
        $request = $this->session->get('m_room_edit_info');
        $temp = array(
            "video_conference_ip_list_key"   => $request['video_conference_ip_list_key'],
            "room_num"   => $request['room_num'],
            "connect_ip" => $request['connect_ip'],
            "priority"   => $request['priority'],

        );

        $user_info = $this->session->get('user_info');

        // 更新処理
        $ret = $this->videoCnferenceTable->update($temp, sprintf( "video_conference_ip_list_key='%s'", $request['video_conference_ip_list_key'] ) );
        // エラー処理
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
        } else {
            $this->logger->info(__FUNCTION__."#edit teleconference successful!",__FILE__,__LINE__,$temp);
        }
        $roomInfo = $this->videoCnferenceTable->getRow( sprintf( "video_conference_ip_list_key='%s' AND room_num='%s'",$user_info["video_conference_ip_list_key"] , $request['room_num'] ) );

        $this->logger->info(__FUNCTION__."#room_edit",__FILE__,__LINE__,$temp);
        $this->session->remove('m_user_edit_info');
        // 操作ログ
        $this->add_operation_log('member_update', array('video_conference_ip_list_key' => $request['video_conference_ip_list_key']));
        $this->display('admin/member/roomlist/edit/done.t.html');
    }

    function action_delete_room(){
        $user_info = $this->session->get( "user_info");
        $room_key = $this->request->get('room_key');
        $roomInfo = $this->videoCnferenceTable->getRow( sprintf( "user_key='%s' AND video_conference_ip_list_key='%s'",$user_info["user_key"] , $room_key ) );
        // 不正アクセス対策
        if(!$roomInfo){
            $this->logger2->warn(__FUNCTION__, __FILE__, __LINE__,$user_info["user_key"]);
            return $this->action_admin_room();
        }

        $temp['status']=0;
        $ret = $this->videoCnferenceTable->update($temp, sprintf( "video_conference_ip_list_key = '%s'", $room_key ) );

        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
        } else {
            $this->logger->info(__FUNCTION__."#delete teleconference successful!",__FILE__,__LINE__,array(
                "video_conference_ip_list_key" => $room_key)
                );
            $data["room_num"] = $temp["room_num"];
        }
        // 操作ログ
        $this->add_operation_log('teleconference_delete', array('video_conference_ip_list_key' => $roomInfo['video_conference_ip_list_key']));
        $message .= "<li>".ROOM_DELETE_KEY . "</li>";
        $this->render_admin_room($message);
    }

    function action_admin_group($message = ""){

        if ($message) {
            $this->template->assign('message', $message);
        }
        //user_key取得
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];

        $group_list = $this->groupTable->getRowsAssoc( sprintf( "user_key='%s'", $user_key ) );
        foreach( $group_list as $group_info ){
            $member_group[] = array(
                                "key" => $group_info['member_group_key'],
                                "name" => $group_info['member_group_name']
                                );
        }

        $this->template->assign('info', $member_group);
        $this->display('admin/member/group/index.t.html');
    }

    function action_add_group(){

        $message = "";
        //user_key取得
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];
        $group_name =$this->request->get('group_name');
        //user_keyからグループを取得
        if (!$this->request->get('group_name')) {
            $message .= "<li>".MEMBER_ERROR_GROUP_NAME . "</li>";
        } elseif (mb_strlen($group_name) > 50) {
            $message .= "<li>".MEMBER_ERROR_GROUP_NAME_LENGTH . "</li>";
        } elseif ( $this->groupTable->numRows( sprintf( "user_key='%s' AND member_group_name='%s'", $user_key, addslashes($group_name) ) ) > 0) {
            $message .= "<li>".MEMBER_ERROR_GROUP_NAME_EXIST . "</li>";
        } else {
            $temp = array(
                "user_key" => $user_key,
                "member_group_name" => $this->request->get('group_name')
            );
            $this->groupTable->add($temp);
            $message .= "<li>".MEMBER_NEW_GROUP_NAME . "</li>";
        }
        // 操作ログ
        $this->add_operation_log('member_group_add', array('group_name' => $group_name));
        $this->action_admin_group($message);
    }

    public function action_edit_group()
    {
        $message = "";
        //user_key取得
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];
        $group_key = $this->request->get('group_key');
        $group_name = $this->request->get('group_name');

        $group_info = $this->groupTable->getRow(sprintf( "user_key='%s' AND member_group_key='%s'",$user_info["user_key"] , $group_key ));
        // 不正アクセス対策
        if(!$group_info){
            $this->logger2->warn(__FUNCTION__, __FILE__, __LINE__,$user_info["user_key"]);
            return $this->action_admin_group();
        }

        // 未入力
        if (!$group_name) {
            $message .= "<li>".MEMBER_ERROR_GROUP_NAME . "</li>";
        // グループ名変更無しで、
        } elseif ( $this->groupTable->numRows( sprintf( "user_key='%s' AND member_group_name='%s'", $user_key, addslashes($group_name))) > 0 ) {
            $message .= "<li>".MEMBER_ERROR_GROUP_NAME_EXIST . "</li>";
        } else {
            $temp = array(
                "member_group_name" => $group_name
            );
            $this->groupTable->update($temp, sprintf( "member_group_key='%s'", $group_key ) );
            $message .= "<li>".MEMBER_EDIT_GROUP_NAME . "</li>";
        }
        // 操作ログ
        $this->add_operation_log('member_group_update', array('group_name' => $group_name));
        $this->action_admin_group($message);
    }

    function action_delete_group()
    {
        $message = "";
        $group_key = $this->request->get('group_key');

        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];
        $group_info = $this->groupTable->getRow(sprintf( "user_key='%s' AND member_group_key='%s'",$user_info["user_key"] , $group_key ));
        // 不正アクセス対策
        if(!$group_info){
            $this->logger2->warn(__FUNCTION__, __FILE__, __LINE__,$user_info["user_key"]);
            return $this->action_admin_group();
        }

        $temp['user_key'] = 0;
        $this->groupTable->update( $temp, sprintf( "member_group_key='%s'", $group_key) );
        $message .= "<li>".MEMBER_DELETE_GROUP_NAME . "</li>";
        // 操作ログ
        $group_name = $this->groupTable->getOne(sprintf( "member_group_key='%s'", $group_key), 'member_group_name');
        $this->add_operation_log('member_group_delete', array('group_name' => $group_name));
        $this->action_admin_group($message);
    }

    function showAdminLogin($message = ""){

        if ($message) {
            $this->template->assign('message', $message);
        }
        $this->display('user/admin_login.tmpl');
    }

    function showAdminTop(){
        $main = new Main();
        if ($message = $main->checkAdminLoginInfo()) {
            showAdminLogin($message);
            exit;
        }
        header("Location: /service/admin.php");
    }
}

$main =& new AppMember();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>

