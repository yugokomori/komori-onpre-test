<?php
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_DBI.class.php");
require_once("lib/EZLib/EZUtil/EZEncrypt.class.php");

class EncryptPw extends AppFrame {

    function init() {
        // DBサーバ情報取得
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $this->dsn = $this->get_dsn();
    }

    function default_view() {

        print $this->_header();
print <<<EOM
<form action="/admin_tool/member_pw_encrypt.php" name="encrypt" method="post">
<input type="hidden" name="action_chenge_encrypt" value="">
<table border="0" cellspacing="0" cellpadding="5">
<input type="submit" name="submit" value="変換">
</form>
EOM;
        print $this->_footer();
    }

    function action_chenge_encrypt() {
        $objMember = new N2MY_DB($this->dsn, "member");
        $member_list = $objMember->getRowsAssoc("", null, null, 0, "member_id, member_pass");

        if (DB::isError($member_list)) {
            $this->logger2->error($member_list->getUserInfo());
            return false;
        }

        print $this->_header();

print <<<EOM
<table border="0" cellspacing="0" cellpadding="5">
<tr>
    <th>&nbsp;</th>
    <th>メンバーーID</th>
    <th>変更ステータス</th>
</tr>
EOM;
        if ($member_list) {
            $count = 1;
            foreach ($member_list as $member) {
                $data = array();
                if ($member["member_id"]) {
                    $data = array("member_pass" => EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $member["member_pass"]),
                    );
                    $where = "member_id = '".mysql_real_escape_string($member["member_id"])."'";
                    $ret = $objMember->update($data, $where);
                    if (DB::isError($ret)) {
                        $this->logger2->info("member_id error :".$member["member_id"]);
                        $status = "error";
                    } else {
                        $status = "OK";
                    }
                } else {
                    $this->logger2->info("member_id error :".$member["member_id"]);
                    $status = "error";
                }

                print "<tr>";
                print "<td>".$count++."</td>";
                print "<td>".htmlspecialchars($member["member_id"])."</td>";
                print "<td>".htmlspecialchars($status)."</td>";
                print "</tr>";
            }
        }
        print "</table>";
        print "成功総数：".$count;
        print "</div>";
        print $this->_footer();
    }

function _header() {
        return <<<EOM
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>一括難読化処理</title>
<style TYPE="text/css">
<!--
body {
text-align: center;
font-size: 12px;
font-family: "Arial, sans-serif, ＭＳ Ｐゴシック", Osaka, "ヒラギノ角ゴ Pro W3" ;
margin: 10px 0px 0px 0px;
}
.title {
font-size: 16px;
font-weight: bold;
margin: 20px auto 0px auto;
width: 760px
}
#content {
text-align: center;
}
#content table {
border-collapse: collapse;
border-spacing: 0;
empty-cells: show;
border: 1px solid #999999;
margin: 20px auto;
width: 760px
}
#content th {
border: 1px solid #999999;
padding: 4px 5px;
background-color: #EFEFEF;
text-align: left;
}
#content td {
border: 1px solid #999999;
padding: 4px 10px;
text-align: left;
}
.errorSection {
color: #FF0000;
}
-->
</style>
</head>
<body>
<div id="content">
<h1>一括難読化処理</h1>
EOM;
    }

    function _footer() {
       print "</body></html>";
    }

}
$main = new EncryptPw();
$main->execute();