<?php

require_once ('classes/AppFrame.class.php');
require_once("classes/N2MY_Account.class.php");
require_once ('classes/dbi/address_book.dbi.php');
require_once ('classes/dbi/user.dbi.php');
require_once ('config/config.inc.php');
require_once( "lib/EZLib/EZCore/EZLogger2.class.php" );
require_once ('lib/EZLib/EZUtil/EZCsv.class.php');

class AppAdminAddressBook extends AppFrame
{
    var $logger = null;
    var $_name_space = null;
    var $addressbookTable = null;

    function init() {
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->objUser          = new UserTable($this->get_dsn());
        $this->objAddressBook   = new AddressBookTable($this->get_dsn());
        //$this->max_address_cnt  = $this->config->get("N2MY", "address_book_count");
        $this->max_address_cnt  = 2000;
        $this->max_address_ex   = 500;
        $this->_name_space      = md5(__FILE__);
    }

    /**
     * ログイン認証
     */
    function auth() {
        $this->checkAuth();
        $this->checkAdminAuth('admin/session_error.t.html');
    }

    /**
     * デフォルト処理
     */
    function default_view() {
        $this->action_top();
    }

    /**
     * top画面
     */
    function action_top() {
        $this->template->assign('max_address_cnt',  $this->max_address_ex);
        $this->display('admin/addressbook/index.t.html');
    }

    /**
     * テンプレートダウンロード
     */
    function action_format_download() {
    	$output_encoding = $this->get_output_encoding();
    	$csv = new EZCsv(true,$output_encoding,"UTF-8");
        $dir = $this->get_work_dir();
        $tmpfile = tempnam($dir, "csv_");
        $csv->open($tmpfile, "w");

        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="sample_addressbook.csv"');

        // ヘッダ
        $header = array("name", "name_kana", "email", "lang(ja.en.zh-cn.zh-tw.fr.th.in)", "timezone", "flag","error");
        // サンプル行
        $sample = array("ＳＡＭＰＬＥ","sample","vcube@example.jp","en","","A","This is a sample data. Please delete.");
        $this->logger2->debug(__FUNCTION__, __FILE__, __LINE__, $header);
        $csv->setHeader($header);
        $csv->write($header);
        $csv->write($sample);
        $csv->close();
        $fp = fopen($tmpfile, "r");
        $contents = fread($fp, filesize($tmpfile));
        fclose($fp);
        print $contents;
        unlink($tmpfile);
    }

    /**
     * インポート
     */
    function action_import() {
        $message = "";
        $user_info = $this->session->get("user_info");
        $user_key = $user_info["user_key"];

        //現段階でアドレス帳に登録されている件数
        $sql = "SELECT count(*) FROM address_book";
        $where = " where user_key =".$user_key.
                 " AND member_key IS NULL".
                 " AND is_deleted = 0";
        $sql .= $where;
        $count = $this->objAddressBook->_conn->getOne($sql);
        if (DB::isError($count)) {
            print $count->getUserInfo();
        }

        // ファイルアップロード
        $request["upload_file"] = $_FILES["import_file"];
        $data = pathinfo($request["upload_file"]["name"]);

        //CSVファイルチェック
        if (empty($data['filename'])) {
            $message .= CSV_NO_FILE_ERROR;
            return $this->action_import_complete($message);
        } elseif($data['extension'] != "csv") {
            $message .= CSV_FILE_FORMAT_ERROR ;
            return $this->action_import_complete($message);
        }

        //ローカル保管
        $filename = $this->get_work_dir()."/".tmpfile();
        $this->logger2->debug($filename);
        move_uploaded_file($request["upload_file"]["tmp_name"], $filename);
        $request["upload_file"]["tmp_name"] = $filename;
        $this->session->set('address_book_info', $request);
        // メッセージ
        $msg_format = $this->get_message("error");
        $msg = $this->get_message("ERROR");
        $output_encoding = $this->get_output_encoding();
        $csv = new EZCsv(true,$output_encoding,"UTF-8");
        $csv->open($filename, "r");
        $rows = array();
        $err_flg = 0;
        $i = 0;

        //正しいheader
        $head_ex = "name,name_kana,email,lang(ja.en.zh-cn.zh-tw.fr.th.in),timezone,flag,error";
        //取り込んだファイルのheder
        $h_ar = implode(",", $csv->getHeader());
        if($head_ex !== $h_ar) {
            $message .= CSV_FILE_FORMAT_ERROR;
            return $this->action_import_complete($message);
        }
        //現在登録されているデータ取得
        $where = "user_key = " . $user_key.
        " AND member_key IS NULL";
        $_list_ad = $this->objAddressBook->getRowsAssoc($where);
        while($row = $csv->getNext(true)) {

            $row["user_key"] = $user_key;
            $row["lang"]     = $row["lang(ja.en.zh-cn.zh-tw.fr.th.in)"];
            $row["memo"]     = NULL;
            if(empty($row["timezone"])) {
                $row["timezone"] = 100;
            }
            //名前変わるので消す。
            unset($row["error"]);
            unset($row["lang(ja.en.zh-cn.zh-tw.fr.th.in)"]);

            //削除flagの場合、is_deletedに1。
            if($row["flag"] == "D") {
                $row["is_deleted"] = 1;
            } elseif($row["flag"] == "A"){
                $i++;
                $row["is_deleted"] = 0;
            }

            //削除と更新しないフラグの時はcheckしないよ☆
                if($row["flag"] != "X") {
                    $error = array();
                    if(!$this->address_check($row)) {
                        $err_fields = $this->_error_obj->error_fields();
                        foreach ($err_fields as $key) {
                            $type = $this->_error_obj->get_error_type($key);
                            $err_rule = $this->_error_obj->get_error_rule($key, $type);
                            if (($type == "allow") || $type == "deny") {
                                $err_rule = join("，", $err_rule);
                            }
                            if($key == "timezone"){
                                $err_rule = "-12～14、100";
                            }
                            $_msg = sprintf($msg[$type], $err_rule);
                            $error[$key] = $_msg;
                            $err_flg = 1;
                        }
                    }

                    if($row["flag"] == "D" && $row["is_deleted"] == 1) {
                        foreach($_list_ad as $_Lad) {
                            if($row["email"] == $_Lad["email"] && $_Lad["is_deleted"] == 1) {
                                $key = "delete";
                                $_msg = CSV_RECORD_ERROR;
                                $error[$key] = $_msg;
                                $err_flg = 1;
                                break;
                            }
                        }
                    }
                    $row["error"] = $error;
                }
            $rows[] = $row;
        }

        //空のファイルだったら。
        if(empty($rows)) {
            $message .= CSV_FILE_FORMAT_ERROR;
            return $this->action_import_complete($message);
        }

        //インポートファイル削除
        unlink($filename);

        //上限件数チェック
        $address_count = $i + $count;
        if( $address_count > $this->max_address_cnt) {
            $message .= CSV_LIMIT_ERROR;
            return $this->action_import_complete($message);
            break;
        }

        //エラー数チェック。
        $a_add = array();
        $a_err = array();
        foreach($rows as $ad_val) {
            if($ad_val["error"]) {
                $a_err[] = $ad_val;
            } else {
                $a_add[] = $ad_val;
            }
        }

        //処理
        foreach($a_add as $ad_val) {
            // 既にメールアドレスが登録されていたら編集フラグにします。
            $where = sprintf("user_key = %s AND email = '%s' AND member_key IS NULL",$user_key , addslashes($ad_val["email"]));
            if ($ad_val["flag"] == "A" && $this->objAddressBook->numRows($where) > 0){
                $ad_val["flag"] = "M";
            }
            if($ad_val["flag"] == "A") { //登録
                unset($ad_val["flag"]);
                unset($ad_val["error"]);
                $ret = $this->address_book_add($ad_val);
                if (DB::isError($ret)) {
                    $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                } else {
                    $this->logger->debug(__FUNCTION__."#add address_book successful!",__FILE__,__LINE__,array($rows["user_key"], $rows["email"]));
                }
            } elseif($ad_val["flag"] == "M") { //編集
                //編集
                unset($ad_val["flag"]);
                unset($ad_val["error"]);
                $where = sprintf("user_key = %s AND email = '%s' AND member_key IS NULL",$user_key , addslashes($ad_val["email"]));
                $ret = $this->address_book_update($ad_val,$where);
                if (DB::isError($ret)) {
                    $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                } else {
                    $this->logger->debug(__FUNCTION__."#add address_book successful!",__FILE__,__LINE__,array($rows["user_key"], $rows["email"]));
                }
            }  elseif($ad_val["flag"] == "D") {//削除
                 //削除
                 unset($ad_val["flag"]);
                 unset($ad_val["error"]);
                 $where = sprintf("user_key = %s AND email = '%s'  AND member_key IS NULL",$user_key , addslashes($ad_val["email"]));
                 $ret = $this->address_book_update($ad_val,$where);
                 if (DB::isError($ret)) {
                    $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                 } else {
                    $this->logger->debug(__FUNCTION__."#add address_book successful!",__FILE__,__LINE__,array($rows["user_key"], $rows["email"]));
                 }
            } elseif($ad_val["flag"] == "X") {//変更しない {
                $this->logger->debug(__FUNCTION__."#add address_book successful!",__FILE__,__LINE__,array($rows["user_key"], $rows["email"]));
            }
        }

        //エラー画面もしくは完了画面
        if(!empty($a_err)) {
            return $this->action_err_complete($rows);
        } else {
            return $this->action_import_complete();
        }

     }


    /**
     * エラー完了画面
     */
    function action_err_complete($rows="") {
        if(!empty($rows)) {
            $_SESSION['rows'] = $rows;
            $this->display('admin/addressbook/err_complete.t.html');
        } else {
            $this->display('admin/addressbook/index.t.html');
        }
    }


    /**
     * エラー内容ダウンロード
     */
    function action_err_csv_download() {
        $rows = $_SESSION['rows'];
        if(!empty($rows)) {
            // エラーの出力
            $output_encoding = $this->get_output_encoding();
            $csv = new EZCsv(true,$output_encoding,"UTF-8");
            $dirName = "/address";
            $dir = $this->get_work_dir().$dirName;
            if( !is_dir( $dir ) ){
                mkdir( $dir );
                chmod( $dir ,0777 );
            }
            $tmpfile = tempnam($dir, "csv_");
            $csv->open($tmpfile, "w");

            header('Content-Type: application/octet-stream;');
            header('Content-Disposition: attachment; filename="address_book_err.csv"');

            // ヘッダ
            $header = array(
                "name"       => "name",
                "name_kana"  => "name_kana",
                "email"      => "email",
                "lang"       => "lang(ja.en.zh-cn.zh-tw.fr.th.in)",
                "timezone"   => "timezone",
                "flag"       => "flag",
                "memo"       => "error"
            );

            $this->logger2->debug(__FUNCTION__, __FILE__, __LINE__, $header);

            $csv->setHeader($header);
            $csv->write($header);

            foreach($rows as $a_key => $add_row) {

                $err_list["name"]           = $add_row["name"];
                $err_list["name_kana"]      = $add_row["name_kana"];
                $err_list["email"]          = $add_row["email"];
                $err_list["lang"]           = $add_row["lang"];
                $err_list["timezone"]       = $add_row["timezone"];
                $err_list["flag"]           = $add_row["flag"];

                if($add_row["error"]) {
                    $err = array();
                    foreach($add_row["error"] as $e_key => $err_mg) {
                        $err[] = "[".$e_key."]".$err_mg;
                        $err_list["memo"] = implode("，", $err);
                    }
                } else {
                    $err_list["memo"]         = "";
                }
                $this->logger2->debug(__FUNCTION__, __FILE__, __LINE__, $err_list);
                $csv->write($err_list);

            }

            // CSV出力
            $csv->close();
            $fp = fopen($tmpfile, "r");
            $contents = fread($fp, filesize($tmpfile));
            fclose($fp);
            chmod($tmpfile, 0777);
            print $contents;

            unlink($tmpfile);

        }

        unset($_SESSION['tmp_file']);

    }


    /**
     * add
     */
    function address_book_add($address_book_info) {
        $ret = $this->objAddressBook->add($address_book_info);
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
            return false;
        } else {
            $this->logger->debug(__FUNCTION__."#add address_book successful!",__FILE__,__LINE__,$address_book_info);
                return true;
            }
    }

    /**
     * edit
     */
    function address_book_update($address_book_info,$where) {
        $ret = $this->objAddressBook->update($address_book_info,$where);
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
            return false;
        } else {
            $this->logger->debug(__FUNCTION__."#update address_book successful!",__FILE__,__LINE__,$address_book_info);
                return true;
            }
    }

    /**
     * delete
     */
    function address_book_delete($address_book_info,$where) {
        $ret = $this->objAddressBook->update($address_book_info["is_deleted"],$where);
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
            return false;
        } else {
            $this->logger->debug(__FUNCTION__."#delete address_book successful!",__FILE__,__LINE__,$address_book_info);
                return true;
            }
    }

    /**
     * 完了画面
     */
    function action_import_complete($message="") {
        // エラーを表示用
        if ($message) {
            $this->template->assign('message', $message);
        }
        $this->display('admin/addressbook/complete.t.html');
    }

    /**
     * エクスポート
     */
     function action_export() {
        //ユーザKey取得
        $user_info = $this->session->get("user_info");
        $user_key = $user_info["user_key"];

        // CSV出力
        $output_encoding = $this->get_output_encoding();
        $csv = new EZCsv(true,$output_encoding,"UTF-8");
        $dir = $this->get_work_dir();
        $tmpfile = tempnam($dir, "csv_");
        $csv->open($tmpfile, "w");

        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="address_book'.date("Ymd").'.csv"');

        // ヘッダ
        $header = array(
            "name"       => "name",
            "name_kana"  => "name_kana",
            "email"      => "email",
            "lang"       => "lang(ja.en.zh-cn.zh-tw.fr.th.in)",
            "timezone"   => "timezone",
            "flag"       => "flag",
            "memo"       => "error"
        );
        $this->logger2->debug(__FUNCTION__, __FILE__, __LINE__, $header);
        $csv->setHeader($header);
        $csv->write($header);

        //一覧
        $where = "user_key = " . $user_key .
    " AND member_key IS NULL".
        " AND is_deleted = 0";
        $sort = array (
          "name" => "asc",
          "email" => "asc"
        );

        $_list = $this->objAddressBook->getRowsAssoc($where, $sort);
        foreach($_list as $_key => $row) {
              $line = array();
              $line["name"]               = $row["name"];
              $line["name_kana"]          = $row["name_kana"];
              $line["email"]              = $row["email"];
              $line["lang"]               = $row["lang"];
              $line["timezone"]           = $row["timezone"];
              $line["flag"]               = "X";
              $line["memo"]               = "";

              $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $line);
              $csv->write($line);
        }

        // CSV出力
        $csv->close();
        $fp = fopen($tmpfile, "r");
        $contents = fread($fp, filesize($tmpfile));
        fclose($fp);

        print $contents;
        unlink($tmpfile);
    }

    /**
     * 厳密な入力チェック
     */
    function address_check($data) {
        $rules = $this->objAddressBook->rules;
        $rules["name"]["regex"]      = "/^[^\\r\\n]*$/D";
        $rules["name_kana"]["regex"] = "/^[^\\r\\n]*$/D";
        $rules["email"]["regex"]     = "/^[^\\r\\n]*$/D";
        $rules["lang"]["allow"]      = array_keys($this->get_language_list());
        $rules["country"]["allow"]   = array_keys($this->get_country_list());
        $rules["timezone"]["allow"]  = $this->get_timezone_list();
        foreach($rules["timezone"]["allow"] as $key => $val){
            $time[] = $val["key"];
            $rules["timezone"]["allow"] = $time;
        }
        array_push($rules["timezone"]["allow"], 100);

        $rules["flag"] = array("required" => true, "allow" => array("A", "D", "X"));

        $this->_error_obj = $this->objAddressBook->check($data, $rules);
        if (EZValidator::isError($this->_error_obj)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 出力するCSVファイルのエンコーディング形式
     */
    function get_output_csv_encoding() {
        // 現在の言語設定で判断。
        $lang = $this->get_language();

        switch($lang) {
            case 'ja_JP':
                return 'SJIS';
            case 'en_US':
            case 'zh_CN':
                return 'UTF-8';
            default:
                return 'SJIS';
        }
    }
}

$main = new AppAdminAddressBook();
$main->execute();
