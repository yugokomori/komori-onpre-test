<?php
/*
 * Created on 2009/10/09
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once "classes/AppFrame.class.php";
require_once "classes/N2MY_Eco.class.php";
require_once "classes/core/dbi/Meeting.dbi.php";

class AdminEcoMeter extends AppFrame {

    var $meeting = null;
    var $eco = null;

    function init() {
        $this->meeting = new DBI_Meeting($this->get_dsn());
        $this->eco = new N2MY_Eco( $this->get_dsn(), N2MY_MDB_DSN );
    }

    function default_view() {
        $this->render_form();
    }

    /**
     * フォーム
     */
    function render_form() {
        $start = $this->request->get("start", date("Y-m-01 00:00:00", strtotime("-1 month")));
        $end = $this->request->get("end", date("Y-m-d H:i:s"));
        $form = <<<EOM
<html>
<head>
<style type="text/css">
        body, table, textarea, pre {
          font-family:Verdana, Arial, Helvetica, sans-serif; color:#000000; font-size:12px;
        }

        .pager a, .pager b {
#            border:1px solid #CCCCCC;
#            padding:4px 5px;
        }

        .data {
            border-left:1px solid #666;
            border-spacing:0pt;
            border-top:1px solid #666;
        }
        .data th {
            padding: 0.5em 1em;
            border-bottom:1px solid #666;
            border-right:1px solid #666;
            color:#fff;
            font-weight:normal;
            text-align:center;
            background-color:#999;
        }
        .data td {
            padding: 0.5em 1em;
            border-bottom:1px solid #666;
            border-right:1px solid #666;
#            text-align:center;
        }

        .data tr.even td {
            background: #ddd;
        }

        .response {
            border:1px solid #666;
            border-spacing:0pt;
        }

        .response th {
            padding: 0.5em 1em;
            color:#fff;
            font-weight:normal;
            text-align:center;
            background-color:#999;
        }

        .response td {
            padding: 0.5em 1em;
        }

        .response tr.even td {
            background: #ddd;
        }

        h2 {
            background-color:transparent;
            color:#003;
            font-size:150%;
            margin:14px 0;
            padding:0 0 0 5px;
        }

        h3 {
            background-color:transparent;
            color:#007;
            font-size:130%;
            margin:14px 0;
            padding:0 0 0 5px;
        }

        h4 {
            background-color:transparent;
            border-left:5px solid #00283F;
            color:#009;
            font-size:100%;
            margin:14px 0;
            padding:0 0 0 5px;
        }

        .section {
            margin-bottom:30px;
            margin-left:30px;
        }

        .subsection {
            margin-bottom:30px;
            margin-left:20px;
        }

        .section dl {
        }
</style>
</head>
<body>
<h3>ECOメーター</h3>
<form action="eco.php" method="post">
start: <input type="text" name="start" value="$start" />
end: <input type="text" name="end" value="$end" />
<input type="submit" name="action_recal" value="再計算" />
<input type="submit" name="action_summary" value="集計" />
</form>
EOM;
        print $form;
    }

    /**
     * ECOメーター再集計
     */
    function action_recal() {
        set_time_limit(0);
        $this->render_form();
        print "<pre>";
        $start = $this->request->get("start", date("Y-m-d H:i:s"));
        $end = $this->request->get("end", date("Y-m-d H:i:s"));
        // 利用実績のある会議のみ
        $where = "use_flg = 1" .
            " AND is_active = 0" .
            " AND eco_move > 0" .
            " AND (" .
            "( actual_start_datetime between '$start' AND '$end' )" .
            " OR ( actual_end_datetime between '$start' AND '$end' )" .
            " OR ( '$start' BETWEEN actual_start_datetime AND actual_end_datetime )" .
            " OR ( '$end' BETWEEN actual_start_datetime AND actual_end_datetime ) )";
        $meeting_list = $this->meeting->getRowsAssoc($where, null, null, null, "meeting_key, eco_move, eco_co2, eco_time, eco_fare, eco_info");
        print '<textarea cols="100" rows="7">'.$this->meeting->_conn->last_query."</textarea>";
        if (DB::isError($meeting_list)) {
            $this->logger2->error($meeting_list->getUserInfo());
        }
        print "<br />";
        print "<table class='data'>";
        print "<tr>";
        print "<th>Meeting Key</th>";
        print "<th>distance</th>";
        print "<th>Time</th>";
        print "<th>Cost</th>";
        print "<th>CO2</th>";
        print "</tr>";
        foreach ($meeting_list as $key => $meeting) {
            if ($meeting["eco_info"]) {
                $meeting["eco_info"] = unserialize($meeting["eco_info"]);
            }
            $this->eco->createReport($meeting["meeting_key"], $meeting["eco_info"]["end"], false, false);
            sleep(3);
            $where = "meeting_key = ".$meeting["meeting_key"];
            $meeting_after = $this->meeting->getRow($where, "eco_move, eco_co2, eco_time, eco_fare");
            print "<tr>";
            print "<td rowspan=2>".$meeting["meeting_key"]."</td>";
            print "<td>".$meeting["eco_move"]."</td>";
            print "<td>".$meeting["eco_time"]."</td>";
            print "<td>".$meeting["eco_fare"]."</td>";
            print "<td>".$meeting["eco_co2"]."</td>";
            print "</tr>";
            print "<tr>";
            print "<td";
            if ($meeting["eco_move"] != $meeting_after["eco_move"]) {
                print " bgcolor='#ffcccc'";
            }
            print ">".$meeting_after["eco_move"]."</td>";
            print "<td";
            if ($meeting["eco_time"] != $meeting_after["eco_time"]) {
                print " bgcolor='#ffcccc'";
            }
            print ">".$meeting_after["eco_time"]."</td>";
            print "<td";
            if ($meeting["eco_fare"] != $meeting_after["eco_fare"]) {
                print " bgcolor='#ffcccc'";
            }
            print ">".$meeting_after["eco_fare"]."</td>";
            print "<td";
            if ($meeting["eco_co2"] != $meeting_after["eco_co2"]) {
                print " bgcolor='#ffcccc'";
            }
            print ">".$meeting_after["eco_co2"]."</td>";
            print "</tr>";
        }
        print "</table>";
    }

    /**
     * 統計
     */
    function action_summary() {
        $this->render_form();
        print "<pre>";
        $start = $this->request->get("start", date("Y-m-d H:i:s"));
        $end = $this->request->get("end", date("Y-m-d H:i:s"));
        // 会議数
        $sql = "SELECT DATE_FORMAT(meeting.actual_start_datetime, '%Y-%m') as ym" .
            ", COUNT(meeting.meeting_key) as cnt" .
            " FROM meeting, user" .
            " WHERE meeting.user_key = user.user_key" .
            " AND user.invoice_flg = 1" .
            " AND meeting.meeting_use_minute > 0" .
            " AND meeting.actual_start_datetime > '$start'" .
            " AND meeting.actual_start_datetime < '$end'" .
            " GROUP BY DATE_FORMAT(meeting.actual_start_datetime, '%Y-%m')";
        $rs = $this->meeting->_conn->query($sql);
        if (DB::isError($rs)) {
            $this->logger2->error($rs->getUserInfo());
        } else {
            while ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                print_r($row);
            }
        }

        // 月別のECOメーター利用状況
        $sql = "SELECT DATE_FORMAT( meeting.actual_start_datetime, '%Y-%m' ) as ym" .
            ", COUNT( meeting.meeting_key ) as cnt" .
            ", SUM( meeting.eco_co2 ) as co2" .
            ", SUM( meeting.eco_fare ) as fare" .
            ", SUM( meeting.eco_time ) as time" .
            " FROM meeting, user" .
            " WHERE meeting.user_key = user.user_key" .
            " AND user.invoice_flg =1" .
            " AND meeting.eco_move > 0" .
            " AND meeting.meeting_use_minute >0" .
            " AND meeting.actual_start_datetime > '$start'" .
            " AND meeting.actual_start_datetime < '$end'" .
            " GROUP BY DATE_FORMAT( meeting.actual_start_datetime, '%Y-%m' )";
        $rs = $this->meeting->_conn->query($sql);
        if (DB::isError($rs)) {
            $this->logger2->error($rs->getUserInfo());
        } else {
            while ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                print_r($row);
            }
        }

        // エコメーター利用者数
        $sql = "SELECT DATE_FORMAT(participant.uptime_start, '%Y-%m') as ym" .
            ", COUNT(participant.participant_key) as cnt" .
            " FROM meeting, user, participant" .
            " WHERE meeting.user_key = user.user_key" .
            " AND meeting.meeting_key = participant.meeting_key" .
            " AND user.invoice_flg = 1" .
            " AND meeting.meeting_use_minute > 0" .
            " AND meeting.actual_start_datetime > '$start'" .
            " AND meeting.actual_start_datetime < '$end'" .
            " AND participant.participant_name is not null" .
            " AND participant.participant_station is not null" .
            " GROUP BY DATE_FORMAT(participant.uptime_start, '%Y-%m')";
        $rs = $this->meeting->_conn->query($sql);
        if (DB::isError($rs)) {
            $this->logger2->error($rs->getUserInfo());
        } else {
            while ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                print_r($row);
            }
        }

        // エコメーター利用者数
        $sql = "SELECT DATE_FORMAT(participant.uptime_start, '%Y-%m') as ym" .
            ", COUNT(participant.participant_key) as cnt" .
            " FROM meeting, user, participant" .
            " WHERE meeting.user_key = user.user_key" .
            " AND meeting.meeting_key = participant.meeting_key" .
            " AND user.invoice_flg = 1" .
            " AND meeting.meeting_use_minute > 0" .
            " AND meeting.actual_start_datetime > '$start'" .
            " AND participant.participant_name is not null" .
            " AND participant.participant_station is null" .
            " GROUP BY DATE_FORMAT(participant.uptime_start, '%Y-%m')";
        $rs = $this->meeting->_conn->query($sql);
        if (DB::isError($rs)) {
            $this->logger2->error($rs->getUserInfo());
        } else {
            while ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                print_r($row);
            }
        }
    }
}

$main = new AdminEcoMeter();
$main->execute();