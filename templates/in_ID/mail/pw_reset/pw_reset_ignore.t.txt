━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Pemberitahuan dari Pertemuan V-CUBE
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Terima kasih telah menggunakan Pertemuan V-CUBE

Permohonan perubahan kata sandi anda telah ditolak oleh akun admin. 
Silahkan hubungi akun admin untuk keterangan lebih lanjut.


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■Pertemuan V-CUBE
<{$smarty.const.N2MY_BASE_URL}>
■Dikembangkan dan dikelola oleh:
V-cube, Inc. <http://www.vcube.com/>
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Customer Support Center:
telepon: 0570-002192（Buka 24/7）
E-mail: <vsupport@vcube.co.jp>
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
