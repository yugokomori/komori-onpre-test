<?php
require_once("classes/N2MY_Api.class.php");
require_once("classes/dbi/member.dbi.php");
class N2MY_MeetingAdminMemberAPI extends N2MY_Api
{
	var $objMember    = null;
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        // SSL対応
        header('Pragma:');
    }

    function auth()
    {
        $this->checkAuthorizationAdmin();
        // ログイン以外の場合は認証させる
        $this->objMember = new MemberTable($this->get_dsn());
    }

    /**
     * メンバー一覧
     */
    function action_get_list() {
        $sort_key  = $this->request->get("sort_key", "name");
        $sort_type = $this->request->get("sort_type", "asc");
        $page      = $this->request->get("page", 1);
        $limit     = $this->request->get("limit", 20);
        $keyword   = $this->request->get("keyword");
        return $this->output();
    }

    /**
     * メンバー追加
     */
    function action_add() {
        $member_id = $this->request->get("member_id");
        $member_pw = $this->request->get("member_pw");
        $name      = $this->request->get("name");
        $name_kana = $this->request->get("name_kana");
        $email     = $this->request->get("email");
        $group     = $this->request->get("group");
        $timezone  = $this->request->get("timezone");
        $lang      = $this->request->get("lang");
        return $this->output();
    }


    /**
     * メンバー更新
     */
    function action_update() {
        $request = array(
        	"member_id"           => $this->request->get("member_id", null),
        	"member_new_id"       => $this->request->get("member_new_id", null),
            "member_pass"         => $this->request->get("password", null),
            "member_email"        => $this->request->get("member_email", null),
            "member_name"         => $this->request->get("member_name", null),
            "member_name_kana"    => $this->request->get("member_name_kana", null),
            "timezone"            => $this->request->get("timezone", null),
            "lang"                => $this->request->get("lang", null),
            "output_type"         => $this->request->get("output_type", null),
        );
        $member_info = $this->objMember->getDetailByMemerID($request["member_id"]);
        if(!$member_info) {
        	return $this->display_error(101, "this member does not exist, please enter a correct member id.");
        }
        $rules = array(
            "member_new_id"     => array(
                "email" => true,
                "maxlen" => 255,
            ),
            "member_email"  => array(
                "email" => true,
                "maxlen" => 255,
            ),
            "member_name"     => array(
                "maxlen" => 255,
            ),
            "member_name_kana"     => array(
                "maxlen" => 255,
            ),
            "timezone"     => array(
                "allow" => array_keys($this->get_timezone_list()),
            ),
            "lang"     => array(
                "allow" => array_keys($this->get_language_list()),
            ),
            "output_type"     => array(
                "allow" => $this->get_output_type_list(),
            ),
        );
        $err_obj = $this->error_check($request, $rules);
        if($request["member_pass"]) {
            if (!preg_match('/^[!-\[\]-~]{8,128}$/', $request["member_pass"])) {
                $err_obj->set_error("pw", "USER_ERROR_PASS_INVALID_01");
            } elseif (!preg_match('/[[:alpha:]]+/', $request["member_pass"]) || preg_match('/^[[:alpha:]]+$/', $request["member_pass"])) {
                $err_obj->set_error("pw", "USER_ERROR_PASS_INVALID_02");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(100, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        
		// メンバー情報更新
        require_once ('classes/mgm/dbi/user.dbi.php');
        $obj_MgmUserTable = new MgmUserTable( $this->get_auth_dsn() );
        $auth_data = array(
        		"user_id"     => $request["member_new_id"]?$request["member_new_id"]:$member_info["member_id"]
        );
        $result = $obj_MgmUserTable->update( $auth_data, sprintf( "user_id='%s'", $request['member_id'] ) );
        if (DB::isError($result)) {
        	$this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$result->getUserInfo());
        } else {
        	$this->logger->info(__FUNCTION__."#update auth_user successful!",__FILE__,__LINE__,$auth_data);
        }
        
        require_once ('lib/EZLib/EZUtil/EZEncrypt.class.php');
        $data = array(
            "member_id"         => $request["member_new_id"]?$request["member_new_id"]:$member_info["member_id"],
            "member_pass"       => $request["member_pass"]?EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $request["member_pass"]):$member_info["member_pass"],
            "member_email"      => $request["member_email"]?$request["member_email"]:$member_info["member_email"],
            "member_name"       => $request["member_name"]?$request["member_name"]:$member_info["member_name"],
            "member_name_kana"  => $request["member_name_kana"]?$request["member_name_kana"]:$member_info["member_name_kana"],
            "timezone"          => $request["timezone"]?$request["timezone"]:$member_info["timezone"],
            "lang"              => $request["lang"]?$request["lang"]:$member_info["lang"],

        );
        $this->logger->info(__FUNCTION__."#member info",__FILE__,__LINE__,$data);
        $where = "member_key = ".$member_info["member_key"];
        $this->objMember->update($data, $where);
        return $this->output();
    }


    /**
     * メンバー削除
     */
    function action_delete() {
        $member_id = $this->request->get("member_id");
        return $this->output();
    }

    function action_get_group() {
        return $this->output();
    }

    function action_add_group() {
        $group_name       = $this->request->get("group_name");
        return $this->output();
    }

    function action_update_group() {
        $group_id       = $this->request->get("group_id");
        $group_name       = $this->request->get("group_name");
        return $this->output();
    }

    function action_delete_group() {
        $group_id       = $this->request->get("group_id");
        return $this->output();
    }

}
$main = new N2MY_MeetingAdminMemberAPI();
$main->execute();