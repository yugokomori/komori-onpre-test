<?php

require_once("classes/N2MY_DBI.class.php");

class StorageFolderTable extends N2MY_DB {

    var $table = "storage_folder";
    var $logger = null;
    protected $primary_key = "storage_folder_key";

    public function __construct($dsn)
    {
        $this->init($dsn, $this->table );
    }

    function StorageFolderTable($dsn)
    {
        $this->logger =& EZLogger::getInstance();
        $this->init($dsn, $this->table);
    }

    //ルートの取得
    function getRoot($options)
    {
        $sql = "SELECT * FROM storage_folder WHERE path = CONCAT('.', storage_folder_key, '.')";
        $sql .= sprintf(" AND user_key='%s'", $options["user_key"]);
        $sql .= sprintf(" AND member_key='%s'", $options["member_key"]);
        $this->logger->info($sql);

        $ret = $this->_conn->query($sql);
        if ($this->isError()) {
            return false;
        }
        while($row = $ret->fetchRow(DB_FETCHMODE_ASSOC)) {
            $rows[] = $row;
        }
        return $rows[0];
    }

    //リーフの取得（1件ずつ）
    function getLeaf($options)
    {
        $sql = "SELECT * FROM storage_folder as Parents";
        $sql .= sprintf(" WHERE user_key='%s'", $options["user_key"]);
        $sql .= sprintf(" AND member_key='%s'", $options["member_key"]);
        $sql .= sprintf(" AND folder_id='%s'", $options["folder_id"]);
        $sql .= " AND NOT EXISTS (SELECT * FROM storage_folder AS Children WHERE Children.node_path LIKE CONCAT(Parents.node_path, '_%'))";

        $this->logger->info($sql);

        $ret = $this->_conn->query($sql);
        if ($this->isError()) {
            return false;
        }
        while($row = $ret->fetchRow(DB_FETCHMODE_ASSOC)) {
            $rows[] = $row;
        }
        return $rows;
    }

    //ノードの深さを取得
    function getNodeDepth($path)
    {
        $sql = sprintf( "select folder_id, LENGTH('%s') - LENGTH(REPLACE('%s', '.', '')) -1 As lvl From storage_folder",  $path, $path);
        $ret = $this->_conn->query($sql);
        if ($this->isError()) {
            return false;
        }
        while($row = $ret->fetchRow(DB_FETCHMODE_ASSOC)) {
            $rows[] = $row;
        }
        return $rows;
    }

    //木の高さを取得
    function getMaxHeight()
    {
        $sql = "SELECT MAX( LENGTH( node_path ) - LENGTH( REPLACE( node_path, '.', '' ) ) -1 ) AS height FROM storage_folder";
        $ret = $this->_conn->query($sql);
        if ($this->isError()) {
            return false;
        }
        $row = $ret->fetchRow(DB_FETCHMODE_ASSOC);
        return $row["height"];
    }

    //直属の子を取得
    function getChildrenNode($options)
    {
        $sql = "SELECT Children.folder_id, Children.folder_name, Children.node_path FROM storage_folder Boss LEFT OUTER JOIN storage_folder Children";
        $sql .= sprintf(" ON Boss.folder_id = %s AND Boss.node_path = (SELECT MAX(node_path) FROM storage_folder", $options["folder_id"]);
        $sql .= " WHERE Children.node_path LIKE CONCAT(node_path, '_%')) ORDER BY Children.update_datetime";

        $this->logger->info($sql);

        $ret = $this->_conn->query($sql);
        if ($this->isError()) {
            return false;
        }
        while($row = $ret->fetchRow(DB_FETCHMODE_ASSOC)) {
            $rows[] = $row;
        }
        return $rows;
    }

//    //パスの列挙を取得する
//    function getNodePath($folderId = "", $count)
//    {
//        $sql = "SELECT O1.folder_id,";
//
//        for($i = 0; $i <= $count; $i++) {
//          $sql .= sprintf("(SELECT folder_id FROM storage_folder WHERE node_path = MAX('$s'.node_path)) AS level_'%s',), $i + 2", $i);
//        }
//        $sql .= "FROM storage_folder O1 ";
//        for($i = 0; $i <= $count; $i++) {
//          $sql .= sprintf(
//              "LEFT OUTER JOIN storage_folder '%02d' ON '%02d'.node_path LIKE concat('%02d'.node_path , '_%'), $i + 2, $i + 1, $i + 2");
//        }
//        if (isset($folder_id)) {
//            $sql .= sprintf(" where = '$s'", $folderId);
//        }
//        $sql .= " GROUP BY O1.folder_id";
//
//        $ret = $this->conn->query($sql);
//        if ($this->isError()) {
//            return false;
//        }
//        while($row = $ret->fetchRow(DB_FETCHMODE_ASSOC)) {
//            $rows[] = $row;
//        }
//        return $rows;
//    }

    //ノード以下のコレクションを削除
    function deleteNode($folderId)
    {
        $sql = "DELETE FROM storage_folder";
        $sql .= " WHERE node_path LIKE '%.".$folderId.".%'";

        $this->logger->info($sql);

        $ret = $this->_conn->query($sql);
        if ($this->isError()) {
            $this->logger->info("error");
            return $ret;
        }
    }

//    //ノードを追加（ノード末尾）
//    function addLastNode($parentfolderId, $folderName)
//    {
//        $data = array();
//        $data["folder_name"] = $folderName;
//        $ret = $this->add($data);
//        if ($this->isError()) {
//            return false;
//        }
//        $where = sprintf( "folder_id = MAX(folder_id) and folder_name = '%s'", $folderName);
//        $row = $this->getRow($where);
//        if ($this->isError()) {
//            return false;
//        }
//        $where = sprintf("node_path = '%s'", $row["folder_id"]);
//        $where .= concat($parentfolderId, $row["folder_id"], '.');
//        $row = $this->getRow($where);
//        if ($this->isError()) {
//            return false;
//        }
//        $data["node_path"] = $row["node_path"];
//        $ret = $this->update($data, $where);
//        if ($this->isError()) {
//            return false;
//        }
//        return true;
//    }

//    //ノードを追加（ノード間）
//    function addBetweenNode($parentfolderId, $parentNodePath, $folderName)
//    {
//        $data["folder_name"] = $folderName;
//        $ret = $this->add($data);
//        if ($this->isError()) {
//            return false;
//        }
//        $data = "";
//        $where = sprintf( "folder_id = MAX(folder_id) and folder_name = '%s'", $folderName);
//        $row = $this->getRow($where);
//        if ($this->isError()) {
//            return false;
//        }
//        $where = sprintf( "node_path = '%s'", $row["folder_id"]);
//        $where .= concat($parentfolderId, $row["folder_id"], '.');
//        $row = $this->getRow($where);
//        if ($this->isError()) {
//            return false;
//        }
//        $data["node_path"] = $row["node_path"];
//        $ret = $this->update($data, $where);
//        if ($this->isError()) {
//            return false;
//        }
//
//        // ノードの深さを取得する
//        $depth = $this->getNodeDepth($parentNodePath);
//
//        // 木の高さを取得する
//        $height = $this->getMaxHeight();
//
//        $count = $height - $depth;
//
//        // パス（木の高さ - ノードの深さ + 1）を列挙する
//        $ret = $this->getNodePath($row["folder_id"], $count);
//
//        $ret = $this->update($data, $where);
//        if ($this->isError()) {
//            return false;
//        }
//        for($i = 0; $i <= $count; $i++) {
//          // 配下のノードをUPDATE
//          $level = sprintf("level_'%s'" ,$i);
//          $data["node_path"] = sprintf("REPLACE(node_path, concat('.', '%s', '.'), concat('.', '%s', '.''.', $ret[$level]), '.')", $ret[$level], $row["folder_id"]);
//
//          $where = "node_path LIKE ";
//          $where .= concat('%/', $ret[$level], '/%');
//
//          $ret = $this->update($data, $where);
//          if ($this->isError()) {
//              return false;
//          }
//          return true;
//        }
//    }

    //部分木を列挙する
    public function getPartNode($options)
    {
        $sql = "SELECT Parent.folder_id AS parent_folder_id, Parent.folder_name AS parent_folder_name, Children.folder_id AS children_folder_id, Children.folder_name AS children_folder_name";
        $sql .= " FROM storage_folder Parent, storage_folder Children";
        $sql .= " WHERE Children.node_path LIKE CONCAT(Parent.node_path , '_%')";
//        $sql .= sprintf(" WHERE Parent.user_key = '%s'" , $options["user_key"]);
//        $sql .= sprintf(" AND Parent.member_key = '%s'" , $options["member_key"]);
        if (!empty($options["folder_id"])) {
            $sql .= sprintf("AND Parent.folder_id = '%s'" , $options["folder_id"]);
        }
        $sql .= " ORDER BY Parent.node_path";

        $this->logger->info($sql);

        $ret = $this->_conn->query($sql);
        if ($this->isError()) {
          return false;
        }
        while($row = $ret->fetchRow(DB_FETCHMODE_ASSOC)) {
            $rows[] = $row;
        }
        $this->logger->info($rows);
        return $rows;
    }

    //子から見た場合の親を列挙する
    public function getParentOfChildrenNode($options)
    {
        $sql = "SELECT Parent.folder_id AS parent_folder_id, Parent.folder_name AS parent_folder_name, Children.folder_id AS children_folder_id, Children.folder_name AS children_folder_name, length(Parent.node_path) - length(REPLACE(Parent.node_path, '.', '')) - 1 as length";
        $sql .= " FROM storage_folder Children";
        $sql .= " LEFT OUTER JOIN storage_folder Parent";
        $sql .= " ON Children.node_path > Parent.node_path";
        $sql .= " AND Parent.node_path = (SELECT MAX(node_path) FROM storage_folder";
        $sql .= " WHERE Children.node_path LIKE CONCAT(node_path, '_%'))";
        $sql .= sprintf(" WHERE Children.user_key = '%s'" , $options["user_key"]);
        $sql .= sprintf(" AND Children.member_key = '%s'" , $options["member_key"]);
        if (!empty($options["folder_id"])) {
            $sql .= " AND Children.node_path LIKE '%.".$options["folder_id"].".%'";
        }
        $sql .= " ORDER BY Parent.node_path";

        $this->logger->info($sql);

        $ret = $this->_conn->query($sql);
        if ($this->isError()) {
          return false;
        }
        while($row = $ret->fetchRow(DB_FETCHMODE_ASSOC)) {
            $rows[] = $row;
        }
        $this->logger->info($rows);
        return $rows;
    }

    public function maxfolderId($where)
    {
        $sql = "SELECT MAX(folder_id) AS max_folder_id FROM storage_folder";
        $this->logger->info($sql);

        $ret = $this->_conn->query($sql);
        if ($this->isError()) {
          return false;
        }
        $row = $ret->fetchRow(DB_FETCHMODE_ASSOC);

        $this->logger->debug($row);
        $this->logger->debug($row["max_folder_id"]);
        return $row["max_folder_id"];
    }

    public function add($data)
    {
        $data["create_datetime"] = date("Y-m-d H:i:s");
        $data["update_datetime"] = date("Y-m-d H:i:s");
        return parent::add($data);
    }

    public function update($data, $where)
    {
        $data["update_datetime"] = date("Y-m-d H:i:s");
        return parent::update($data, $where);
    }
    function getList($where = "", $sort = array(), $limit = null, $offset = 0, $columns = "*") {
        return $this->getRowsAssoc($where, $sort, $limit, $offset, $columns);
    }
    //ノードを追加（ノードの間）
    function isError($ret) {
        if (DB::isError($ret)) {
            $this->logger->error("DB Error", __FILE__, __LINE__, $ret);
            return $ret;
        }
    }
}