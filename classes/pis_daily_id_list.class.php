#!/usr/local/bin/php
<?php
set_time_limit(0);
ini_set("memory_limit","2048M");

require_once("lib/EZLib/EZUtil/EZCsv.class.php");
require_once("lib/EZLib/EZHTML/EZValidator.class.php");
require_once("classes/dbi/member.dbi.php");
require_once("classes/dbi/whitelist.dbi.php");
require_once("classes/dbi/whitecompanylist.dbi.php");
require_once("classes/dbi/blacklist.dbi.php");

require_once ("classes/mgm/dbi/user.dbi.php");
require_once("classes/N2MY_DBI.class.php");

define("ERROR_USER_LIST_FLG", "Failed to USER_LIST flg");
define("ERROR_USER_LIST", "Failed to USER_LIST");
define("ERROR_WHITE_CODE", "Failed to get white_code");
define("ERROR_WHITE_ID", "Failed to get white_id");
define("ERROR_MONTHLY_TABLE", "Failed to monthly member table");
define("ERROR_MONTHLY", "Failed to monthly");
define("ERROR_BLACK_ID", "Failed to get black_id");
define("ERROR_BLACK_UPDATE", "Failed to update(black)");

class AppDailyIdListClass {

    var $dsn = null;
    var $obj_white_company_list = null;
    var $obj_whitelist = null;
    var $obj_black_list = null;
    var $obj_member = null;
    var $result_log_dir = null;

    var $white_company_list = null;
    var $white_list = null;

    // USER_LISTの配列情報
    var $index_ul_tid = 0;
    var $index_ul_t_name = 1;
    var $index_ul_t_kana = 2;
    var $index_ul_t_mail = 3;
    var $index_ul_t_company_code = 4;
    var $index_ul_t_company_name = 5;
    var $index_ul_t_classification = 6;
    var $index_ul_t_accounting_unit_code = 7;
    var $index_ul_t_expense_department_code = 8;
    var $index_ul_gid = 9;
    var $index_ul_g_name = 10;
    var $index_ul_g_kana = 11;
    var $index_ul_g_mail = 12;
    var $index_ul_g_company_code = 13;
    var $index_ul_g_company_name = 14;
    var $index_ul_g_accounting_unit_code = 15;
    var $index_ul_g_expense_department_code = 16;

    // ホワイトコード・リスト マッチパターン
    // *** ホワイトコードマッチパターン ***
    // 統合ID：有り, グローバルID：有り, 統合ID:マッチ, グローバルID： -
    var $wc_tg_t = "WC_TG_T";
    // 統合ID：有り, グローバルID：有り, 統合ID: - , グローバルID： -
    var $wc_tg_g = "WC_TG_G";
    // 統合ID： - , グローバルID：有り, 統合ID: - , グローバルID： マッチ
    var $wc_g_g = "WC_G_G";
    // 統合ID：有り, グローバルID： - , 統合ID:マッチ, グローバルID： -
    var $wc_t_t = "WC_T_T";
    // *** ホワイトIDマッチパターン ***
    // 統合ID：有り, グローバルID：有り, 統合ID:マッチ, グローバルID： -
    var $wi_tg_t = "WI_TG_T";
    // 統合ID：有り, グローバルID：有り, 統合ID: - , グローバルID： -
    var $wi_tg_g = "WI_TG_G";
    // 統合ID： - , グローバルID：有り, 統合ID: - , グローバルID： マッチ
    var $wi_g_g = "WI_G_G";
    // 統合ID：有り, グローバルID： - , 統合ID:マッチ, グローバルID： -
    var $wi_t_t = "WI_T_T";

    /**
     * コンストラクタ
     */
    function AppDailyIdListClass($dsn){
        $this->logger2 = & EZLogger2::getInstance();

        $this->obj_whitelist     = new WhitelistTable($dsn);
        $this->obj_white_company_list     = new white_company_listTable($dsn);
        $this->obj_black_list    = new BlacklistTable($dsn);
        $this->obj_member        = new MemberTable($dsn);

        // ログ出力前提作業
        $config = parse_ini_file( sprintf( "%sconfig/config.ini", N2MY_APP_DIR ), true);
        $log_dir = $config["LOGGER"]["log_dir"];
        $this->result_log_dir = $log_dir."result_logs";
        if (!file_exists ( $this->result_log_dir )) {
            mkdir ( $this->result_log_dir );
        }
    }

    /**
     * 前提処理
     * @return [type] [description]
     */
    public function prepare() {

        // USER_LIST 作成成功チェック
        if(file_get_contents(USER_LIST_FLG_PATH) !== date("Ymd")){
            return ERROR_USER_LIST_FLG;
        }

        // USER_LIST 存在チェック
        if(!file_exists(USER_LIST_PATH)){
            return ERROR_USER_LIST;
        }

        // USER_LIST 取得
        $csv = new EZCsv(false, "UTF-8", "UTF-8", "\t", true);
        $csv->open(USER_LIST_PATH, "r");
        $user_list = array();
        while($row = $csv->getNext(true)) {
            $user_list[] = $row;
        }
        $csv->close();

        // ホワイトコード取得
        $white_company_where = "white_company_list_status = 0 AND user_key=" . ADMIN_USER_KEY;

        // 会社コードが複数あるため全行取得
        $this->white_company_list = $this->obj_white_company_list->getRowsAssoc($white_company_where);

        // 取得エラー時は停止
        if (DB::isError($this->white_company_list)) {
            return ERROR_WHITE_CODE;
        }

        // ホワイトID取得
        $white_where = "whitelist_status = 0 AND user_key=" . ADMIN_USER_KEY;
        $this->white_list = $this->obj_whitelist->getRowsAssoc( $white_where , array() , null , 0 , "*" , "member_id");

        // 取得エラー時は停止
        if (DB::isError($this->white_list)) {
            return ERROR_WHITE_ID;
        }

        // 正常時は配列を返す
        return $user_list;
    }

    /**
     * ホワイトコードチェック処理
     * @param  [type] $user USER_LIST(1行)
     * @return [type]       正常：パターン情報, 異常：false
     */
    public function check_white_code($user) {
        $result = false;

        // 統合ID、またはグローバルIDが存在する
        if (($user[$this->index_ul_tid] == null) && ($user[$this->index_ul_gid] == null)) {
            // 異常データ
            $this->_log("tid and gid is none [". print_r($user, true) ."]", $this->result_log_dir, "error");
        } else {

            // ホワイトカンパニーリストと比較
            foreach ($this->white_company_list as $comp ) {
                $pattern = null;

                // 会社コード、経理単位コード、費用部課コードチェック
                if (($comp["company_id"] == null) && ($comp["accounting_unit_code"] == null)
                && ($comp["expense_department_code"] == null)) {
                    // 異常データ
                    $this->_log("white code error [". print_r($comp, true) ."]", $this->result_log_dir, "error");
                    continue;
                } else {
                    if ($user[$this->index_ul_tid]) {
                        // 統合IDで判定
                        $pattern = $this->_check_company_data($comp, $user, true);
                    }

                    if (($pattern == null) && ($user[$this->index_ul_gid])) {
                        // グローバルIDで判定
                        $pattern = $this->_check_company_data($comp, $user, false);
                    }

                    if ($pattern){
                        // パターンマッチ
                        $result = $pattern;

                        // WC_TG_G の場合、TIDでのマッチが無いか最後まで確認する
                        if ($result != $this->wc_tg_g) {
                            break;
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * ホワイトコード項目チェック処理
     * @param  [type] $comp  ホワイトコード情報
     * @param  [type] $user  USER_LIST(1行)
     * @param  [type] $isTid true: 統合ID情報を使用, false： グローバルID情報を使用
     * @return [type]        正常：パターン情報, 異常：null
     */
    private function _check_company_data($comp, $user, $isTid) {
        $result = false;

        // ホワイトカンパニーリストの比較対象カラム数によって、$tempのカラム数合わせて比較を行う。
        if ($comp["expense_department_code"]) {
            // 会社コード＋経理単位コード＋費用部課コード
            $comp_check = $comp["company_id"] . "_" . $comp["accounting_unit_code"] . "_" . $comp["expense_department_code"];
            if ($isTid) {
                // 統合ID情報を使用
                $user_check = $user[$this->index_ul_t_company_code] . "_" . $user[$this->index_ul_t_accounting_unit_code] . "_" . $user[$this->index_ul_t_expense_department_code];
            } else {
                // グローバルID情報を使用
                $user_check = $user[$this->index_ul_g_company_code] . "_" . $user[$this->index_ul_g_accounting_unit_code] . "_" . $user[$this->index_ul_g_expense_department_code];
            }
        } else if ($comp["accounting_unit_code"]) {
            // 会社コード＋経理単位コード
            $comp_check = $comp["company_id"] . "_" . $comp["accounting_unit_code"];
            if ($isTid) {
                // 統合ID情報を使用
                $user_check = $user[$this->index_ul_t_company_code] . "_" . $user[$this->index_ul_t_accounting_unit_code];
            } else {
                // グローバルID情報を使用
                $user_check = $user[$this->index_ul_g_company_code] . "_" . $user[$this->index_ul_g_accounting_unit_code];
            }
        } else {
            // 会社コード
            $comp_check = $comp["company_id"];
            if ($isTid) {
                // 統合ID情報を使用
                $user_check = $user[$this->index_ul_t_company_code];
            } else {
                // グローバルID情報を使用
                $user_check = $user[$this->index_ul_g_company_code];
            }
        }

        // カラム数を合わせたUSER_LIST=ホワイトテーブル情報
        if ($comp_check == $user_check) {
            // 付替区分のチェック
            if (($comp["classification"] == "-1") ||
            ($user[$this->index_ul_t_classification] == $comp ["classification"])){
                if ($isTid) {
                    if ($user[$this->index_ul_gid]) {
                        // パターン：WC_TG_T
                        $result = $this->wc_tg_t;
                    } else {
                        // パターン：WC_T_T
                        $result = $this->wc_t_t;
                    }
                } else {
                    if ($user[$this->index_ul_tid]) {
                        // パターン：WC_TG_G
                        $result = $this->wc_tg_g;
                    } else {
                        // パターン：WC_G_G
                        $result = $this->wc_g_g;
                    }
                }
            } else {
                // 付替区分が違うため、グローバルIDチェックまたは終了
            }
        }

        return $result;
    }

    /**
     * ホワイトIDチェック処理
     * @param  [type] $user USER_LIST(1行)
     * @return [type]       正常： パターン情報, 異常： false
     */
    public function check_white_list($user) {
        $result = false;

        // 統合IDを持っている
        if ($user[$this->index_ul_tid]) {
           	// 統合IDと一致するホワイトリストを確認
            $wl_tid = $this->white_list[$user[$this->index_ul_tid]];
            if ($wl_tid) {
                // グローバルIDを持っているか確認
                if ($user[$this->index_ul_gid]) {
                    // グローバルIDがホワイトリストに登録されているか
                    $wl_gid = $this->white_list[$user[$this->index_ul_gid]];

                    if ($wl_gid) {
                        // 登録が新しいものを優先とする
                        if ($wl_tid["whitelist_key"] > $wl_gid["whitelist_key"]) {
                            // 統合IDでマッチとする
                            // パターン：WI_TG_T
                            $result = $this->wi_tg_t;
                        } else {
                            // グローバルIDでマッチとする
                            // パターン：WI_TG_G
                            $result = $this->wi_tg_g;
                        }
                    } else {
                        // パターン：WI_TG_T
                        $result = $this->wi_tg_t;
                    }
                } else {
                    // パターン：WI_T_T
                    $result = $this->wi_t_t;
                }
            } else if ($this->white_list[$user[$this->index_ul_gid]]) {
                // パターン：WI_TG_G
                $result = $this->wi_tg_g;
            } else {
                // パターンなし
            }
        } else {
            // グローバルIDと一致するホワイトリストを確認
            if ($this->white_list[$user[$this->index_ul_gid]]) {
                // パターン： WI_G_G;
                $result = $this->wi_g_g;
            }
        }

        return $result;
    }

    /**
     * DB登録処理
     * @param  [type] $user       USER_LIST(1行)
     * @param  [type] $wc_pattern ホワイトコードマッチパターン
     * @param  [type] $wi_pattern ホワイトIDマッチパターン
     * @return [type]             正常: true, 異常: array("table" => テーブル名, "error" => エラーメッセージ)
     */
    public function user_list_refrection($user, $wc_pattern, $wi_pattern) {
        // 処理結果
        $result = false;
        // パターン
        $pattern = null;

        // パターン判定(ホワイトID > ホワイトコードの優先順位)
        if ($wi_pattern) {
            $pattern = $wi_pattern;
        } else if ($wc_pattern) {
            $pattern = $wc_pattern;
        } else {
            // 該当パターンなし（ログ出力）
            $this->_log("#No pattern much! TID = [".($user[$this->index_ul_tid])."] GID = [".($user[$this->index_ul_gid])."]", $this->result_log_dir);
            return $result;
        }

        // 登録データ取得
        $member_temp = $this->_get_member_data($pattern, $user);
        $member_id = null;

        if ($member_temp === false) {
            // 登録データなし
            return $result;
        }

        // 登録対象ID
        if (($pattern == $this->wc_t_t) || ($pattern == $this->wi_t_t)) {
            // 統合IDを登録
            $member_id = $user[$this->index_ul_tid];
        } else {
            // グローバルIDを登録
            $member_id = $user[$this->index_ul_gid];
        }

        // マッチ情報取得
        $list_info = $this->_get_list_info($pattern);

        // result_logs出力用
        $log_info = "member_id = [ $member_id ] accounting_code = [ ".$member_temp["accounting_unit_code"]." ] expense_code = [ ".$member_temp ["expense_department_code"]." ] list = [ $list_info ] pattern = [ $pattern ]";

        // IDの存在を確認
        if ($member_id == null)  {

            // CSV エラー(異常データ)
            $this->_log ( "CSV ErrorData member_id = [ null ]" , $this->result_log_dir, "error" );

        } else {

            // メンバーIDを設定
            $member_temp ["member_id"] = $member_id;

            if ($this->_validate_member_info($member_temp, $log_info) == false) {
                // validation error
                return $result;
            }

            // ユーザ存在確認
            if ($this->_exists_member($member_id)) {

                // $member_temp ["member_id"]を消す
                unset($member_temp ["member_id"]);

                $where = "member_id = '" . $member_id . "' ";
                // メンバーIDがあれば変更
                $result = $this->obj_member->update ( $member_temp, $where );
                $this->log_writer ( "Member update ", $result, $this->result_log_dir, $log_info );

                // エラー判定
                if (DB::isError($result )) {
                    // 登録エラー(テーブル情報設定)
                    $result = array("table" => "member", "error" => DB::errorMessage($result_mem));
                } else {
                    // 正常終了
                    $result = true;
                }

            } else {


                // 統合ID -> グローバルIDへの更新対象か確認
                if ($this->_is_change_gid($pattern)) {
                    // 統合IDのデータ確認
                    if ($this->_exists_member($user[$this->index_ul_tid])) {
                        // 統合IDをグローバルIDに更新
                        $result = $this->_update_tid_member($member_temp, $user[$this->index_ul_tid], $log_info);
                    } else {
                        // 新規登録
                        $result = $this->_add_new_member($member_temp, $log_info);
                    }
                } else {
                    // 新規登録
                    $result = $this->_add_new_member($member_temp, $log_info);
                }
            }
        }

        return $result;
    }

    /**
     * メンバー情報新規追加処理
     * ※member、user table にデータを追加
     * @param [type] $member_data [description]
     */
    private function _add_new_member($member_data, $log_info) {

        $result = false;

        $result_mem = $this->obj_member->add ( $member_data );
        $this->log_writer ( "Member insert ", $result_mem, $this->result_log_dir, $log_info );

        // エラー判定
        if (DB::isError($result_mem)){
            // 登録エラー(テーブル情報設定)
            $result = array("table" => "member", "error" => DB::errorMessage($result_mem));
        } else {
            // 認証サーバーへ追加
            $obj_MgmUserTable = new MgmUserTable ( N2MY_MDB_DSN );
            $auth_data = array (
                    "user_id" => $member_data["member_id"],
                    "server_key" => 1
            );

            $result_mgm = $obj_MgmUserTable->add ( $auth_data );

            $this->log_writer ( "MgmUser insert", $result_mgm, $this->result_log_dir, "member_id = [ ".$member_data["member_id"]." ]" );

            // エラー判定
            if (DB::isError($result_mgm)) {
                // 登録エラー(テーブル情報設定)
                $result = array("table" => "user", "error" => DB::errorMessage($result_mgm));
            } else {
                // 正常終了
                $result = true;
            }
        }

        return $result;
    }

    /**
     * [_update_tid_member description]
     * @param  [type] $member_data 更新データ
     * @param  [type] $tid         更新対象の統合ID
     * @return [type]              [description]
     */
    private function _update_tid_member($member_data, $tid, $log_info) {

        $result = false;

        $where_mem = "member_id = '" . $tid . "'";
        $where_mgm = "user_id = '" . $tid . "'";

        // member talble を更新
        $result_mem = $this->obj_member->update ( $member_data, $where_mem );
        $this->log_writer ( "Member update ", $result, $this->result_log_dir, $log_info );

        // エラー判定
        if (DB::isError($result_mem)) {
            // 登録エラー(テーブル情報設定)
            $result = array("table" => "member", "error" => DB::errorMessage($result_mem));
        } else {
            // 認証サーバーデータを更新
            $obj_MgmUserTable = new MgmUserTable ( N2MY_MDB_DSN );
            $auth_data = array ("user_id" => $member_data["member_id"]);
            $result_mgm = $obj_MgmUserTable->update ( $auth_data, $where_mgm );
            $this->log_writer ( "MgmUser update", $result_mgm, $this->result_log_dir, "member_id = [ ".$member_data["member_id"]." ]" );

            // エラー判定
            if (DB::isError($result_mgm)) {
                // 登録エラー(テーブル情報設定)
                $result = array("table" => "user", "error" => DB::errorMessage($result_mgm));
            } else {
                // 正常終了
                $result = true;
            }
        }

        return $result;
    }

    /**
     * ユーザ登録情報生成処理
     * @param  [type] $pattern パターン情報
     * @param  [type] $user    USER_LIST(1行)
     * @return [type]          ユーザ登録情報, 該当パターンなし: false
     */
    private function _get_member_data($pattern, $user) {
        $member_data = array(
                "user_key" => ADMIN_USER_KEY,
                "member_pass" => EZEncrypt::encrypt ( VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, MEMBER_PASSWORD ),
                "member_group" => 0,
                "member_type" => "", // インサート時にNot null制約エラーとなるため空文字追加
                "member_status" => 9, // 初期値を設定
                "timezone" => 9,
                "lang" => "ja",
                "use_shared_storage" => 0
        );

        // パターン判定
        switch ($pattern) {
            case $this->wc_tg_t:
            case $this->wc_t_t:
            case $this->wi_tg_t:
            case $this->wi_tg_g:
            case $this->wi_t_t:
                // 統合ID情報をベースに作成
                $member_data["member_company_id"] = $user[$this->index_ul_t_company_code];
                $member_data["member_company_name"] = $user[$this->index_ul_t_company_name];
                $member_data["member_email"] = $user[$this->index_ul_t_mail];
                $member_data["member_name"] = $user[$this->index_ul_t_name];
                $member_data["member_name_kana"] = $user[$this->index_ul_t_kana];
                $member_data["accounting_unit_code"] = $user[$this->index_ul_t_accounting_unit_code];
                $member_data["expense_department_code"] = $user[$this->index_ul_t_expense_department_code];
                $member_data ["member_status"] = 0;
                break;
            case $this->wc_tg_g:
            case $this->wc_g_g:
            case $this->wi_g_g:
                // グローバルID情報をベースに作成
                $member_data["member_company_id"] = $user[$this->index_ul_g_company_code];
                $member_data["member_company_name"] = $user[$this->index_ul_g_company_name];
                $member_data["member_email"] = $user[$this->index_ul_g_mail];
                $member_data["member_name"] = $user[$this->index_ul_g_name];
                $member_data["member_name_kana"] = $user[$this->index_ul_g_kana];
                $member_data["accounting_unit_code"] = $user[$this->index_ul_g_accounting_unit_code];
                $member_data["expense_department_code"] = $user[$this->index_ul_g_expense_department_code];
                $member_data ["member_status"] = 0;
                break;
            default:
                // 該当パターンなし
                return false;
                break;
        }

        // ホワイトIDマッチ
        switch ($pattern) {
            case $this->wi_tg_t:
            case $this->wi_t_t:
                // 統合ID情報を参照
                // expense_department_code accounting_unit_codeはホワイトリストが優先
                $expense_department_code = isset ( $this->white_list[$user[$this->index_ul_tid]]["expense_department_code"] ) ? $this->white_list[$user[$this->index_ul_tid]] ["expense_department_code"] : $user[$this->index_ul_t_expense_department_code];

                $accounting_unit_code = isset ( $this->white_list[$user[$this->index_ul_tid]]["accounting_unit_code"] ) ? $this->white_list[$user[$this->index_ul_tid]]["accounting_unit_code"] : $user[$this->index_ul_t_accounting_unit_code];

                $member_data["expense_department_code"] = $expense_department_code;
                $member_data["accounting_unit_code"] = $accounting_unit_code;

                break;
            case $this->wi_tg_g:
            case $this->wi_g_g:
                // グローバルID情報を参照
                // expense_department_code accounting_unit_codeはホワイトリストが優先
                $expense_department_code = isset ( $this->white_list[$user[$this->index_ul_gid]]["expense_department_code"] ) ? $this->white_list[$user[$this->index_ul_gid]] ["expense_department_code"] : $user[$this->index_ul_g_expense_department_code];

                $accounting_unit_code = isset ( $this->white_list[$user[$this->index_ul_gid]]["accounting_unit_code"] ) ? $this->white_list[$user[$this->index_ul_gid]]["accounting_unit_code"] : $user[$this->index_ul_g_accounting_unit_code];

                $member_data["expense_department_code"] = $expense_department_code;
                $member_data["accounting_unit_code"] = $accounting_unit_code;

                break;
            default:
                // 処理なし
                break;
        }

        return $member_data;
    }

    /**
     * リスト情報取得処理(ログ出力用)
     * @param  [type] $pattern パターン情報
     * @return [type]          ホワイトコードマッチ: "classification", ホワイトIDマッチ: "whiteList"
     */
    private function _get_list_info($pattern) {
        $list_info = null;

        switch ($pattern) {
            case $this->wc_tg_t:
            case $this->wc_tg_g:
            case $this->wc_g_g:
            case $this->wc_t_t:
                $list_info = "classification";
                break;
            case $this->wi_tg_t:
            case $this->wi_tg_g:
            case $this->wi_g_g:
            case $this->wi_t_t:
                $list_info = "whiteList";
                break;
            default:
                // 該当パターンなし
                $list_info = null;
                break;
        }

        return $list_info;
    }

    /**
     * 統合ID -> グローバルID変更確認
     * ※以下のパターンの場合 true
     *  ・WC_TG_T
     *  ・WC_TG_G
     *  ・WI_TG_T
     *  ・WI_TG_G
     * @param  [type]  $pattern [description]
     * @return boolean          [description]
     */
    private function _is_change_gid($pattern) {

        $result = false;

        switch ($pattern) {
            case $this->wc_tg_t:
            case $this->wc_tg_g:
            case $this->wi_tg_t:
            case $this->wi_tg_g:
                // 統合ID -> グローバルID変更対象
                $result = true;
                break;
            default:
                // 対象外
                break;
        }

        return $result;
    }

    /**
     * グローバルID重複チェック
     * ※ 下記パターンの場合、既にグローバルIDが存在しないかを確認
     *  ・WC_TG_T
     *  ・WC_TG_G
     *  ・WI_TG_T
     *  ・WI_TG_G
     * @param  [type] $pattern [description]
     * @param  [type] $user    [description]
     * @return [type]          [description]
     */
    private function _exists_gid($pattern, $user) {

        $result = false;

        if ($this->_is_change_gid($pattern)) {
            $result = $this->_exists_member($user[$this->index_ul_gid]);
        }

        return $result;
    }

    /**
     * メンバー情報存在確認
     * @param  [type] $member_id 対象メンバーID
     * @return [type]            true: あり, false: なし
     */
    private function _exists_member($member_id) {

        $result = false;

        $where = "member_id = '" . $member_id . "'";
        $is_member = $this->obj_member->numRows ( $where );
        if ($is_member) {
            // データあり
            $result = true;
        }

        return $result;
    }

    // 月次処理
    function monthly_process(){
        $monthly_where = "user_key=" . ADMIN_USER_KEY;
        if($this->obj_member->numRows($monthly_where)){
            $month_where = "is_admin = 0";
            $data = array(
                    "member_status"      => -1,
            );
            $result_update = $this->obj_member->update($data , $month_where);
            if (DB::isError($result_update )) {
                return array (false,ERROR_MONTHLY_TABLE);
            }
        }else{
            return array (false,ERROR_MONTHLY);
        }
        return array (true,"");
    }

    function clean_white_list(){
        unset($this->white_list);
    }
    function clean_white_company_list(){
        unset($this->white_company_list);
    }

    // ブラックリスト取得
    function black_list_reflection(){
        $black_where = "blacklist_status = 0 AND user_key=" . ADMIN_USER_KEY;
        $black_list = $this->obj_black_list->getRowsAssoc( $black_where , array() , null , 0 , "*" , "member_id");
        if (DB::isError ( $black_list )) {
            return array (false,ERROR_BLACK_ID);
        }

        $temp = array ("member_status" => -1);

        // ブラックリストの対象memberを無効化
        $black_result = true;
        foreach ( $black_list as $black ) {
            $member_check = $this->obj_member->get_member_status(ADMIN_USER_KEY , $black["member_id"]);

            if($member_check){
                $where = "user_key = '" . ADMIN_USER_KEY . "' AND member_id = '".$black["member_id"]."'" ;
                $result = $this->obj_member->update($temp , $where);
                $this->log_writer ( "Member update (black)", $result, $this->result_log_dir, "member_id = [".$black["member_id"]."]" );
                if (DB::isError ( $result )) {
                    $black_result = false;
                }
            }
        }
        return array ($black_result,ERROR_BLACK_UPDATE);
    }

    private function log_writer($table_name , $result , $result_log_dir ,  $log_info){
        if (DB::isError ( $result )) {
            $error_info = $result->getUserInfo();
            $this->_log ( "#$table_name  DB ERROR!   -> $log_info"  , $result_log_dir , "error");
            $this->_log ( "\n $error_info \n" , $result_log_dir , "error" );
        } else {
            $this->_log ( "#$table_name  successful! -> $log_info"  , $result_log_dir);
        }
    }

    private function _log($string , $result_log_dir , $error_flg=null){
        if ($error_flg === "error") {
            $log_prefix = "daily_id_error_list";
        } else {
            $log_prefix = "daily_id_success_list";
        }

        $log_file = sprintf("%s/%s_%s.log", $result_log_dir, $log_prefix, date('Ymd', time()));
        $fp = fopen($log_file, "a");

        if($fp){
            fwrite($fp, $string."\n");
        }
        fclose($fp);
    }

    /**
     * メンバー情報Validation
     * @param  [type]  $data [description]
     * @return boolean       [description]
     */
    private function _validate_member_info($data, $log_info) {
        $result = false;

        //ID
        if (isset($data['member_id'])) {
            $regex_rule = '/^[[:alnum:]._-]{3,20}$/';
            $valid_result = EZValidator::valid_regex($data['member_id'], $regex_rule);
            if ($valid_result == false){
                $this->_log ( "validation error member_id, " . $log_info, $this->result_log_dir, "error" );
                return $result;
            }
        }

        //会社ID
        //(CSV取り込み時はcompany_codeだが、member_company_idと同一情報)
        $regex_rule = '/^[[:alnum:]]{1,9}$/';
        $valid_result = EZValidator::valid_regex($data['member_company_id'], $regex_rule);
        if ($valid_result == false){
            $this->_log ( "validation error member_company_id, " . $log_info, $this->result_log_dir, "error" );
            return $result;
        }

        //会社名
        //(CSV取り込み時はcompany_nameだが、member_company_nameと同一情報)
        if (mb_strlen($data['member_company_name']) > 40) {
            $this->_log ( "validation error member_company_name, " . $log_info, $this->result_log_dir, "error" );
            return $result;
        }elseif(mb_strlen($data['member_company_name']) === 0){
            $this->_log ( "validation error member_company_name, " . $log_info, $this->result_log_dir, "error" );
            return $result;
        }

        if(!$data['member_name']){
            $this->_log ( "validation error member_name, " . $log_info, $this->result_log_dir, "error" );
            return $result;
        } elseif (mb_strlen($data['member_name']) > 50) {
            $this->_log ( "validation error member_name, " . $log_info, $this->result_log_dir, "error" );
            return $result;
        }
        if (mb_strlen($data['member_name_kana']) > 50) {
            $this->_log ( "validation error member_name_kana, " . $log_info, $this->result_log_dir, "error" );
            return $result;
        }
        if ($data['member_email']) {
            if (!EZValidator::valid_email($data['member_email'])){
                $this->_log ( "validation error member_email, " . $log_info, $this->result_log_dir, "error" );
                return $result;
            } elseif (mb_strlen($data['member_email']) > 255){
                $this->_log ( "validation error member_email, " . $log_info, $this->result_log_dir, "error" );
                return $result;
            }
        }

        //経理単位コード
        $regex_rule = '/^[[:alnum:]]{1,8}$/';
        $valid_result = EZValidator::valid_regex($data['accounting_unit_code'], $regex_rule);
        if ($valid_result == false){
            $this->_log ( "validation error accounting_unit_code, " . $log_info, $this->result_log_dir, "error" );
            return $result;
        }
        //費用部課コード
        $regex_rule = '/^[[:alnum:]]{1,5}$/';
        $valid_result = EZValidator::valid_regex($data['expense_department_code'], $regex_rule);
        if ($valid_result == false){
            $this->_log ( "validation error expense_department_code, " . $log_info, $this->result_log_dir, "error" );
            return $result;
        }

        $result = true;

        return $result;
    }

	function multipleCeheck() {
		$_status = "";
		$_pid = 0;
		//プロセスkill
		$killflg  = false;
		$pidFile = dirname ( __FILE__ ) . "/daily_id_list.pid";
		if (file_exists ( $pidFile )) {
			$_pid = trim ( file_get_contents ( $pidFile ) );
			system ( "ps {$_pid} 2>&1 >/dev/null", $_status );
			if ($_status) {
				unlink ( $pidFile );
			} else {
				$file_time = filemtime ( $pidFile );
				$time = time () - $file_time;
				if ($time < 600) {
					// 多重起動エラー
					return ERROR_MUITIPLE_START_UP;
				} else {
					// ハングと見なし
					// ログを残し、古いプロセスをkill
					system ( "kill -9 {$_pid}", $_status );
					$killflg  =true;
				}
			}
		}
		// pidファイル作成
		system ( 'echo ' . getmypid () . ' > ' . $pidFile, $_status );

		if($killflg){
			return ERROR_PROCESS_KILL;
		}else{
			return true;
		}
	}
}

