<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/N2MY_DBI.class.php");

define("DB_NEWS_TYPE_MAINTENANCE", "1");

class NewsTable extends N2MY_DB {

    var $maintenance = "";
    var $lang = "";

    /**
     * コンストラクタ
     *
     */
    function NewsTable($dsn) {
        $this->rules = array(
            "news_key"     => array(
                "required" => true),
            "news_show"     => array(
                "required" => true),
            "news_delete"     => array(
                "required" => true),
            "news_date"     => array(
                "required" => true),
            "news_title"    => array(
                "required"  => true),
            "news_contents" => array(
                "required" => true,)
        );
        $this->init($dsn, "news");
    }

       /**
     * データ取得
     */
    function getList($lang = ""){
        $sql = "SELECT * FROM news" .
                " WHERE news_delete = '0'" .
                " AND news_maintenance = '". DB_NEWS_TYPE_MAINTENANCE."'" .
                " AND news_lang = '".addslashes($lang)."'" .
                " ORDER BY news_show DESC" .
                ", news_date DESC";
        $ret = $this->_conn->query($sql);
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, $sql);
            return $ret;
        }
        return $ret;
    }

    /**
     * 生成用データ取得
     */
    function getReleaseList($lang = "", $limit = 0, $offset = null){
        $sql = "SELECT * FROM news" .
                " WHERE news_delete = '0'" .
                " AND news_show = '1'" .
                " AND news_maintenance = '". DB_NEWS_TYPE_MAINTENANCE ."'" .
                " AND news_lang = '".addslashes($lang)."'" .
                " ORDER BY news_date desc";
        if ($offset !== null) {
            $sql .= " LIMIT ".$offset.",".$limit;
        }

        $ret = $this->_conn->query($sql);

        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, $sql);
            return $ret;
        }
        return $ret;
    }

    /**
     * 表示ステータスの変更
     */
    function changeStatus($id, $type){
        //////////////
        // 有効化
            if($type == "activate"){
                $data = array("news_show" => 1);
                $where = "news_key = ".$id;
                $sth = $this->_conn->autoExecute($this->table, $data, DB_AUTOQUERY_UPDATE, $where);
            if (PEAR::isError($sth)) {
                return $sth;
            }
        }

        //////////////
        // 無効化
          elseif($type == "deactivate"){
                $data = array("news_show" => 0);
                $where = "news_key = ".$id;
                $sth = $this->_conn->autoExecute($this->table, $data, DB_AUTOQUERY_UPDATE, $where);
                if (PEAR::isError($sth)) {
                    return $sth;
                }
          }
    }

    /**
     *
     */
    function complete($news_info, $id = ''){
        if ($news_info) {
            if($id){
                $data = array(
                    "news_date"         => $news_info['date'],
                    "news_title"        => $news_info['title'],
                    "news_contents"     => $news_info['contents'],
                );
                $where = "news_key = ".$id;
                $ret = $this->update($data, $where);
            } else {
                $data = array(
                    "news_date"         => $news_info['date'],
                    "news_title"        => $news_info['title'],
                    "news_contents"     => $news_info['contents'],
                    "news_show"         => 1,
                    "news_delete"       => 0,
                    "news_maintenance"  => DB_NEWS_TYPE_MAINTENANCE,
                    "news_lang"         => $news_info['lang'],
                );
                $ret = $this->add($data);
            }
            if (DB::isError($ret)) {
                $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, $ret->getUserInfo());
                return $ret;
            }
            return $ret;
        }
    }

    /**
     *
     */
    function newsDelete($id){

        $query = "UPDATE news SET news_delete='1'"
                       . " WHERE news_key={$id}";

        $ret = $this->_conn->query($query);

        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, $query);
            return $ret;
        }
        return $ret;
    }

    /**
     * ニュースリリースとメンテナンスの詳細ページをデータベースからデータを取り
     * サイトを生成します。
     */
    function getNewsDesc($id){

        $sql = "SELECT * FROM news" .
                " WHERE news_delete = '0'" .
                " AND news_show = '1'" .
                " AND news_key = '{$id}'" .
                " ORDER BY news_date desc";
        $ret = $this->_conn->query($sql);

        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, $sql);
            return $ret;
        }
        return $ret;

    }
}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
