<?php
require_once("classes/mcu/config/McuConfigProxy.php"); 
require_once("classes/mcu/latest/model/MCUControllerProxy.php");
require_once("classes/mgm/dbi/mcu_server.dbi.php");

$configProxy= new McuConfigProxy();

class FatalException extends Exception {}

/**
 * DBに登録されているサーバーキーを設定してください
 */

$serverKey = $_REQUEST["key"] ? $_REQUEST["key"] : 1;

try {
	$mcuServer		= new McuServerTable($configProxy->getAuthDsn());
	
	if ($serverKey == null)
		throw new FatalException("The server_key is indispensable value to determine the mcu server.");
	
	$mcuServerRecord= $mcuServer->getRow(sprintf("server_key = '%s'", $serverKey));
	if ($mcuServerRecord == null)
		throw new FatalException("The server record could not loaded.");
	
	$wsdl = sprintf("%s:%d", $mcuServerRecord["server_address"], $mcuServerRecord["controller_port"]);
	
	$mcuProxy	= new MCUControllerProxy();
	$mcuProxy->setWSDL($wsdl);
	
	$url = sprintf("http://".$configProxy->get("conference_mngr_listener"), $_SERVER["HTTP_HOST"]);
	
// 	$mcuProxy->unregisterConferenceMngrListener($url);
	
	$mcuProxy->registerConferenceMngrListener($url);
	
	echo true;
}
catch (FatalException $e) {
	$logger2->error($e);
}
catch (Exception $e) {
	$logger2->warn($e);
}
