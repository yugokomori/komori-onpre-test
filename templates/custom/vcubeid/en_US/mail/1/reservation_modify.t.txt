━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from V-CUBE Meeting
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Thank you for using web conferencing service.
V-cube has sent a notice of change to your e-mail invitations.

■Topic:
{$info.reservation_name}

■Date and Time:
{$info.reservation_starttime} ～ {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

■{if $info.reservation_pw && $info.reservation_pw_type == 2}Archive{/if}Password
{if $info.reservation_pw}{$info.reservation_pw}{else}No Password{/if}

------------------------------
Other Meeting Options
------------------------------
{if $info.pin_cd}
■Passcode for V-CUBE Mobile
{$info.pin_cd}
※You can attend the meeting with iPad/Android.  Start the V-CUBE Mobile application on iPad/Android, go to “Login with Passcode” and enter this passcode.

{/if}
{if $temporaryAddress && (!$info.reservation_pw || ($info.reservation_pw && $info.reservation_pw_type == 2))}

■Guest Meeting Address for Hardware Video Conference System
{$temporaryAddress}
※Enter this meeting address when you connect to V-CUBE Meeting from hardware type video conference system such as Polycom.
{/if}
{if $teleconf_info && $teleconf_info.tc_type != ''}

■Teleconferencing Number and Code
{if $teleconf_info.tc_type == 'pgi'}
Call the Dial -in number, listen to the command prompt, and enter in the access code.
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- Dial-in Number:
{$teleconf_info.main_phone_number.Number|japan_tel}  ({$teleconf_info.main_phone_number.Location})
-- Other Dial-in Number:
{if $info.guest_url_format ==1}
{$teleconf_info.phone_number_url}
{else}
<{$teleconf_info.phone_number_url}>
{/if}
{/if}
-- Access Code:
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
{/if}
{/if}

■Meeting Participants:
The notice has been sent to these participants.

{if $guests}{foreach from=$guests item=guest}
- {$guest.name} {if $guest.type == 1} (Audience){elseif $guest.type == 2} (Whiteboard){/if}
　{$guest.email}
　Invitation URL: <{$base_url}guest/r/{$guest.r_user_session}&c={$guest.country_key}&lang={$guest.lang}>
{/foreach}{/if}

- Message for Participants
{$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
V-CUBE Meeting powered by V-cube, Inc.
