<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("classes/N2MY_DBI.class.php");

/*
 * ビジネスロジックを含まない純粋なClipテーブルのDBI
 * */

class MeetingClipTable extends N2MY_DB {

    var $table = "meeting_clip";
    var $logger = null;
    protected $primary_key = null;

    public function __construct( $dsn )
    {
        $this->init($dsn, $this->table );
    }

    public function countByMeetingKeys($meeting_keys)
    {
        if (!$meeting_keys){
            throw new Exception("countByMeetingKeys meeting_keys is empty");
        }
        $where  = sprintf("meeting_key in ('%s') AND is_deleted = 0 ",
                          implode ($meeting_keys,"','"));

        $db_res = $this->select($where);
        if (!$db_res || PEAR::isError($db_res)) {
            throw new Exception("db errror where:".$db_res->getQuery());
        }
        $res = array();
        while ($row = $db_res->fetchRow(DB_FETCHMODE_ASSOC)) {
            if (isset($res[$row['meeting_key']])) {
                $res[$row['meeting_key']] ++;
            } else {
                $res[$row['meeting_key']] = 1;
            }
        }
        return $res;
    }
    public function findByMeetingKey($meeting_key)
    {
        if (!$meeting_key){
            throw new Exception("findByMeetingKey meeting_key is empty");
        }
        $where  = sprintf("meeting_key='%s' AND is_deleted = 0 ", mysql_real_escape_string($meeting_key));
        $db_res = $this->select($where);
        if (!$db_res || PEAR::isError($db_res)) {
            throw new Exception("db errror where:".$db_res->getQuery());
        }
        $res = array();
        while ($row = $db_res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $res[] = $row;
        }
        return $res;
    }
    public function findByRoomKey($room_key){
        $where  = sprintf("room_key='%s' AND is_deleted = 0 ", mysql_real_escape_string($room_key));
        $db_res = $this->select($where);
        if (!$db_res || PEAR::isError($db_res)) {
            throw new Exception("db errror where:".$db_res->getQuery());
        }
        $res = array();
        while ($row = $db_res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $res[] = $row;
        }
        return $res;
    }
    public function deleteByMeetingKey($meeting_key)
    {
        if (!$meeting_key){
            throw new Exception("deleteMeetingKey meeting_key is empty");
        }
        $where = "meeting_key='".mysql_real_escape_string($meeting_key)."' and is_deleted=0";
        $data  = array("updatetime" => date("Y-m-d H:i:s"),
                       "is_deleted" => 1);

        $res = parent::update($data, $where);
        if (PEAR::isError($res)) {
            throw new Exception("db errror where:$where , data:".print_r($data,true));
        }
    }
    public function deleteByClipKey($clip_key)
    {
        if (!$clip_key){
            throw new Exception("deleteByClipKey clip_key is empty");
        }
        $where = "clip_key='".mysql_real_escape_string($clip_key)."' and is_deleted=0";
        $data  = array("updatetime" => date("Y-m-d H:i:s"),
                       "is_deleted" => 1);
        $res = parent::update($data, $where);
        if (PEAR::isError($res)) {
            throw new Exception("db errror where:$where , data:".print_r($data,true));
        }
    }
    public function addClipsToMeeting($clip_keys, $meeting_key , $room_key = null)
    {
        if ($meeting_key === null || (count($clip_keys) <= 0)){
            throw new Exception("clip_keys or  meeting_key is empty meeting_key: ".
                                "$meeting_key, clip_keys ".print_r($clip_keys, true));
        }
        foreach ($clip_keys as $clip) {
            $data = array("meeting_key" => $meeting_key,
                          "clip_key"    => $clip["clip_key"],
                          "is_loaded"   => 0,
                          "is_deleted"  => 0,
                          "storage_file_key"  => $clip["storage_file_key"],
						  "meeting_clip_name" => $clip["name"]?$clip["name"]:$clip["meeting_clip_name"],
                          "room_key"    => $room_key,
                          "createtime" => date("Y-m-d H:i:s"));
            $this->add($data);
        }

    }
}
