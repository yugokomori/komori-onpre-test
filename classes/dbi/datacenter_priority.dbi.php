<?php

require_once("classes/N2MY_DBI.class.php");

class DatacenterPriorityTable extends N2MY_DB {

    var $table = "datacenter_priority";
    var $logger = null;
    protected $primary_key = "datacenter_priority_key";

    public function __construct( $dsn )
    {
        $this->init($dsn, $this->table );
    }

    function DatacenterIgnoreTable( $dsn )
    {
        $this->logger =& EZLogger::getInstance();
        $this->init($dsn, $this->table);
    }


   //優先的なデータセンター（Priority）
    function get_datacenter_priority($user_key, $meeting_country) {
        $dc_priority_list = array();
        $where = sprintf( "user_key='%s' AND country='%s'", $user_key, $meeting_country);
        $dc_priority = $this->getRowsAssoc( $where, array( "sort" => "ASC"), null, 0, "datacenter_key");
        foreach( $dc_priority as $p_dc ){
            $dc_priority_list[] = $p_dc["datacenter_key"];
        }
        return $dc_priority_list;
    }

}