<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("classes/N2MY_DBI.class.php");
require_once("lib/EZLib/EZSession/EZSession.class.php");

class RoomTable extends N2MY_DB {

    var $table = 'room';
    var $logger = null;
    protected $primary_key = null;
    var $session = null;
    var $dsn = "";

    /**
     * DB接続
     */
    function RoomTable($dsn) {
        $this->init($dsn, "room");
        $this->session = EZSession::getInstance();
        $this->dsn = $dsn;
    }

    function get_extend_room($max_seat , $user_ksy , $media_mixer_key){
        $sql = "SELECT  room.* ".
        " FROM room, ives_setting " .
        " WHERE room.max_seat = " .$max_seat.
        " AND room.user_key = " .$user_ksy.
        " AND ives_setting.media_mixer_key = " .$media_mixer_key.
        " AND room.is_extend_room =1 " .
        " AND room.room_status =1 " .
        " AND room.room_key = ives_setting.room_key " .
        " AND  ives_setting.is_deleted = 0 ".
        " ORDER BY room.room_sort ASC  ";

        $rs = $this->_conn->query($sql);
        if (DB::isError($rs)) {
            $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, $sql);
            return $rs;
        }

        while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            if ($key) {
                $ret[$row[$key]] = $row;
            } else {
                $ret[] = $row;
            }
        }
        return $ret;
    }

    /**
     * 使用可能なミーティングキー一覧
     *
     * @param integer $user_key
     */
    function get_use_list($user_key) {
        // 契約部屋一覧取得
        $where = sprintf( "user_key='%s' AND room_status=1", $user_key );
        //SALES対応
        if($this->session->get("service_mode") == "sales") {
            $where .= " AND use_sales_option=1";
        } else {
            $where .= " AND use_sales_option=0";
        }
        $roomlist = $this->getRowsAssoc( $where, array( "room_key" => "ASC") );
        $room_keys = array();
        foreach( $roomlist as $room ){
            array_push($room_keys , $room['room_key']);
        }
        return $room_keys;
    }

    function create_meeting_key($room_key) {
        return $room_key."_".md5(uniqid(rand(), true));
    }

    /**
     * meeting_keyを保存する
     */
    function last_meeting_update($room_key, $meeting_key, $create_flg = 0) {
        $where = "room_key = '".addslashes($room_key)."'";
        // 更新
        $data = array(
            "meeting_key" => $meeting_key,
            "room_updatetime" => date("Y-m-d H:i:s"),
            "creating_meeting_flg" => $create_flg
            );
        return $this->update($data, $where);
    }

    function use_room($user_key) {
        $query = "SELECT room_key" .
                ", room_name" .
                " FROM room" .
                " WHERE user_key = '".addslashes($user_key)."'" .
                " AND room_status !=0" .
                " ORDER BY room_key";
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $query);
//        return $this->getList($query);
        return $this->getRowsAssoc($query);
    }

    function _isTrial($room_key){
        $now_date = getdate();
        $now_date['mon']++;
        if($now_date['mon'] > 10){
            $now_date['mon'] = 1;
            $now_date['year']++;
        }
        if ($now_date['mon'] < 10) $now_date['mon'] = (string)'0'.$now_date['mon'];
        $nextmonth = $now_date['year'] . "-" . $now_date['mon'] . "-00 00:00:00";

        $query = "SELECT service_name" .
                " FROM room_plan rp, service s" .
                " WHERE rp.room_key = '$room_key'" .
                " AND rp.service_key = s.service_key" .
                " AND rp.room_plan_starttime < '$nextmonth'" .
                " ORDER BY rp.room_plan_registtime" .
                " DESC LIMIT 0,1";
        $rows = $this->getList($query);
        $row = $rows[0];

        if($row['service_name'] == "Trial" || $row['service_name'] == "Seminar_Trial"){
            return 1;
        }else{
            return 0;
        }
    }

    /**
     * オーディエンス席のキー取得
     */
    function audience_seet_key($room_key){
        $query = "SELECT room_seat_max_audience_seat
            FROM room_seat
            WHERE room_key = '$room_key'";

        $rows = $this->getList($query);
        $row = $rows[0];
        return $row["room_seat_max_audience_seat"];
    }

    /**
     * プレゼンルーム判別
     */
    function isPresenRoom($room_key) {
        $query = "SELECT count(*)" .
                " FROM room_plan p , service s" .
                " WHERE p.room_key LIKE '$room_key'" .
                " AND p.service_key = s.service_key" .
                " AND s.service_name LIKE 'Seminar%'" .
                " AND p.room_plan_status = 1";
        $rows = $this->getList($query);
        $row = $rows[0];
        return $row['count(*)'];
    }

    /**
     * 現在の会議キー取得
     */
    function getMeetingKey($room_key) {
        $where = "room_key = '".$room_key."'";
        $this->getOne($where, "");
    }

    function add($data) {
        $data["room_sort"] = $this->get_max_sort($data["user_key"]) + 1;
        return parent::add($data);
     }

    function update( $data, $where ) {
        $data["room_updatetime"] = date( "Y-m-d H:i:s" );
        return parent::update( $data, $where );
    }

     /**
     * ユーザーの中で最大のソート値を返す
     */
    function get_max_sort($user_key) {
        //ルームキーからソート番号取得
        $where = " user_key = '".addslashes($user_key)."'";
        $room_count = $this->numRows($where);
        if($room_count > 0){
            $query = "SELECT max(room_sort) FROM room WHERE user_key = ". $user_key;
            $ret = $this->_conn->getOne($query);
            $this->logger->info("max_sort",__FILE__,__LINE__,$ret);
            if (DB::isError($ret)) {
                $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, $query);
                return $ret;
            }
            return $ret;
        } else {
            return 0;
        }
    }

    /**
     * ルームキーをもとにすぐ上にあるルームと順番を入れ替える
     */
    function up_sort($user_key, $room_key) {
        //ルームキーからソート番号取得
        $where_now = " room_key = '".addslashes($room_key)."'";
        $d_row = $this->getRow($where_now);
        //ひとつ上のソート番号取得
        $query = "SELECT max(room_sort) FROM room WHERE user_key = '". $user_key ."' AND room_status = 1 AND room_sort < ". $d_row["room_sort"];
        $u_row = $this->_conn->getOne($query);
        $this->logger2->info($u_row);
        if($u_row && $u_row > 0){
            //もとのソート番号を上に挿入
            $query = "SELECT room_key FROM room WHERE user_key = '". $user_key ."' AND room_status = 1 AND room_sort = ". $u_row;
            $a_row = $this->_conn->getOne($query);
            $data1 = array("room_sort" => $d_row["room_sort"]);
            $where1 = "room_key = '".addslashes($a_row)."'";
            $this->update($data1, $where1);
            //すぐ上のソート番号を挿入
            $data2 = array("room_sort" => $u_row);
            $where2 = " room_key = '".addslashes($room_key)."'";
            $this->update($data2, $where2);
            return true;
        }
        return false;
    }

     /**
     * ルームキーをもとにすぐ下にあるルームと順番を入れ替える
     */
    function down_sort($user_key, $room_key) {
        //ルームキーからソート番号取得
        $where_now = " room_key = '".addslashes($room_key)."'";
        $d_row = $this->getRow($where_now);
        //ひとつ下のソート番号取得
        $query = "SELECT min(room_sort) FROM room WHERE user_key = '". $user_key ."' AND room_status = 1 AND room_sort > ". $d_row["room_sort"];
        $u_row = $this->_conn->getOne($query);
        $this->logger2->info($u_row);
        if($u_row && $u_row > 0){
            //もとのソート番号を下に挿入
            $query = "SELECT room_key FROM room WHERE user_key = '". $user_key ."' AND room_status = 1 AND room_sort = ". $u_row;
            $a_row = $this->_conn->getOne($query);
            $data1 = array("room_sort" => $d_row["room_sort"]);
            $where1 = "room_key = '".addslashes($a_row)."'";
            $this->update($data1, $where1);
            //すぐ下のソート番号を挿入
            $data2 = array("room_sort" => $u_row);
            $where2 = " room_key = '".addslashes($room_key)."'";
            $this->update($data2, $where2);
            return true;
        }
        return false;
    }

    //部屋パスワード確認
    function checkPassword($room_key, $password) {
        $where = "room_key = '".addslashes($room_key)."'".
            " AND room_status = 1";
        $rs = $this->select($where);
        if ($rs) {
            $row = $rs->fetchRow(DB_FETCHMODE_ASSOC);
            // パスワードが設定されていないか、入力したパスワードと一致する場合
            if ($row["room_password"] == $password) {
                return true;
            }
        }
        return false;
    }
    public function findByKey($room_key)
    {
        if (!$room_key) {
            throw new Exception("room_key is empty");
        }
        $where  = sprintf("room_key ='%s'", mysql_real_escape_string($room_key));

        $db_res = $this->select($where, array(), 1, 0, '*');
        if (!$db_res || PEAR::isError($db_res)) {
            throw new Exception("db errror where:".$db_res->getQuery());
        }
        return $db_res->fetchRow(DB_FETCHMODE_ASSOC);

    }

    function getDetailByMemberKey($member_key) {
    	$where = "member_key = '".$member_key."'";
    	require_once("classes/dbi/member_room_relation.dbi.php");
    	$objMemberRoom = new MemberRoomRelationTable($this->dsn);
    	$room_key = $objMemberRoom->getOne($where, "room_key");
    	return $this->get_detail($room_key);
    }

  /**
     * 部屋の情報を取得
     */
    function get_detail($room_key) {
        $where = "room_key = '".addslashes($room_key)."'";
        $row = $this->getRow($where);
        if (DB::isError($row)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$row->getUserInfo());
            return $row;
        }
        return $row;
    }
}
