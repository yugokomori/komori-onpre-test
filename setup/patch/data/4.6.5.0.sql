--
-- パスワードリセット機能
--

CREATE TABLE `pw_reset` (
  `pw_reset_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `authorize_key` varchar(255) NOT NULL UNIQUE,
  `ignore_key` varchar(255) NOT NULL UNIQUE,
  `user_key` int(11) NOT NULL,
  `member_key` int(11) DEFAULT NULL,
  `name` text NOT NULL,
  `create_datetime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

-- PGi連携用
ALTER TABLE  `meeting` ADD  `pgi_conference_status` TINYINT NOT NULL COMMENT  'PGi GM利用ステータス 0:利用なし、1:利用、2:バッチ削除完了' AFTER  `pgi_service_name`;

ALTER TABLE  `meeting_invitation` ADD INDEX (  `meeting_session_id` );
