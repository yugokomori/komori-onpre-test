<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("config/config.inc.php");
require_once("classes/N2MY_DBI.class.php");
require_once("lib/EZLib/EZXML/EZXML.class.php");
require_once("lib/EZLib/EZUtil/EZRtmp.class.php");
require_once("lib/EZLib/EZMail/EZSmtp.class.php");

class DBI_FmsServer extends N2MY_DB
{
    public $logger = null;
    public $rules = array();
    public $table = "fms_server";
    public $obj_MGM_DB = null;
    private $config;
    protected $primary_key = "server_key";

    public function __construct( $dsn ) {
        $this->init($dsn, $this->table );
        $this->config = parse_ini_file( sprintf( "%sconfig/config.ini", N2MY_APP_DIR ), true);
    }

    /**
     * データセンターから優先順位の最も高いサーバーのプライマリKEYを取得する
     *
     * @access public
     * @param  integer $datacenter_key データセンタープライマリーKEY
     * @return integer
     */
    function getKeyByDatacenterKey($datacenter_key, $datacenter_country, $is_ssl = 0, $failover = true, array $fms_deny_list, $fms_version = "") {
        $is_ssl = ($is_ssl) ? 1 : 0;
        $server_list = array();
        // 指定されたデータセンタのサーバ一覧
        $where  = "datacenter_key = '".addslashes($datacenter_key)."'".
                " AND is_available = 1".
                " AND server_priority > 0".
                " AND is_ssl = ".$is_ssl;
        if ( $fms_version ) {
            $where .= " AND server_version >= '".mysql_real_escape_string($fms_version)."'";
        }
        if ($fms_deny_list) {
            $fms_deny_list_comma_separated = implode(",", $fms_deny_list);
            $where .= " AND NOT server_key IN (". $fms_deny_list_comma_separated .")";
        }

        $sort = array(
                "server_count" =>  "asc",
                "server_priority" => "desc");
        if ($server_list = $this->getRowsAssoc($where, $sort)) {
            if ($server_key = $this->getActiveServer($server_list)) {
                return $server_key;
            }
        }
        if ($failover) {
            // 指定されたデータセンタ以外で同一地域のサーバ一覧
            $where  = "datacenter_key != '".addslashes($datacenter_key)."'".
                    " AND server_country = '".addslashes($datacenter_country)."'".
                    " AND is_available = 1".
                    " AND server_priority > 0".
                    " AND is_ssl = ".$is_ssl;
            if ( $fms_version ) {
                $where .= " AND server_version >= '".mysql_real_escape_string($fms_version)."'";
            }
            if ($fms_deny_list) {
                $fms_deny_list_comma_separated = implode(",", $fms_deny_list);
                $where .= " AND NOT fms_server_key IN (". $fms_deny_list_comma_separated .")";

            }
            $sort = array(
                    "server_count" =>  "asc",
                    "server_priority" => "desc");
            if ($server_list = $this->getRowsAssoc($where, $sort)) {
                if ($server_key = $this->getActiveServer($server_list)) {
                    return $server_key;
                }
            }
        }
        return false;
    }

    /**
    * IgnoreKeyがあったら＆通常処理）優先順位の最も高いサーバーのプライマリKEYを取得する
     */
    function getKeyByPriority($country, $is_ssl, $fms_deny_list = null,$ignore_datacenter_keys = null, $fms_version = "") {
        if($ignore_datacenter_keys){
            $ignore_datacenter_comma_separated = implode(",",$ignore_datacenter_keys);
            $where = " datacenter_key NOT IN (". $ignore_datacenter_comma_separated .")";
            $i_dc_key = $this->getRowsAssoc($where, null, null, 0, "datacenter_key");
                foreach( $i_dc_key as $i_dc ){
                    $dc_list[] = $i_dc["datacenter_key"];
                }
            $this->logger2->debug($dc_list);
        } else if ($contry_dc = $this->config['FMS_DC'][$country]) {
            $dc_list = split(',', $contry_dc);
            $this->logger2->debug($dc_list);
        }

        if ($dc_list) {
            foreach($dc_list as $dc_key) {
                // 指定されたデータセンタ以外で同一地域のサーバ一覧
                $where  = " datacenter_key = '".addslashes($dc_key)."'".
                          " AND server_country = '".addslashes($country)."'".
                          " AND server_priority > 0".
                          " AND is_available = 1".
                          " AND is_ssl = ".$is_ssl;
                if ( $fms_version ) {
                    $where .= " AND server_version >= '".mysql_real_escape_string($fms_version)."'";
                }
                if ($fms_deny_list) {
                    $fms_deny_list_comma_separated = implode(",", $fms_deny_list);
                    $where .= " AND NOT server_key IN (". $fms_deny_list_comma_separated .")";
                }
                $sort = array(
                        "server_count" =>  "asc",
                        "server_priority" => "desc");
                if ($server_list = $this->getRowsAssoc($where, $sort)) {
                    if ($server_key = $this->getActiveServer($server_list)) {
                        return $server_key;
                    }
                }
            }
        } else {
            // 指定された地域以外のサーバ一覧
            $where = "server_country = '".addslashes($country)."'".
                    " AND is_available = 1".
                    " AND server_priority > 0".
                    " AND is_ssl = ".$is_ssl;
            if ( $fms_version ) {
                $where .= " AND server_version >= '".mysql_real_escape_string($fms_version)."'";
            }
            if ($ignore_datacenter_keys) {
                $ignore_datacenter_comma_separated = implode(",",$ignore_datacenter_keys);
                $where .= " AND NOT datacenter_key IN (".$ignore_datacenter_comma_separated .")";
            }
            if ($fms_deny_list) {
                $fms_deny_list_comma_separated = implode(",", $fms_deny_list);
                $where .= " AND NOT server_key IN (". $fms_deny_list_comma_separated .")";
            }
            $sort = array("server_priority" => "desc");
            if ($server_list = $this->getRowsAssoc($where, $sort)) {
                if ($server_key = $this->getActiveServer($server_list)) {
                    $this->logger2->warn($server_key);
                    return $server_key;
                }
            }
        }

        // 指定された地域以外のサーバ一覧
        $where = "server_country != '".addslashes($country)."'".
                " AND is_available = 1".
                " AND server_priority > 0".
                " AND is_ssl = ".$is_ssl;
        if ( $fms_version ) {
            $where .= " AND server_version >= '".mysql_real_escape_string($fms_version)."'";
        }
        if ($ignore_datacenter_keys) {
               $ignore_datacenter_comma_separated = implode(",",$ignore_datacenter_keys);
               $where .= " AND NOT datacenter_key IN (".$ignore_datacenter_comma_separated .")";
        }

        if ($fms_deny_list) {
            $fms_deny_list_comma_separated = implode(",", $fms_deny_list);
            $where .= " AND NOT server_key IN (". $fms_deny_list_comma_separated .")";

        }
        $sort = array("server_priority" => "desc");
        if ($server_list = $this->getRowsAssoc($where, $sort)) {
            if ($server_key = $this->getActiveServer($server_list)) {
                $this->logger2->warn($server_key);
                return $server_key;
            }
        }
        return false;
    }


    /**
     * 渡されたDCリストから接続可能なFMSサーバーを取得する
     */
    function getKeyByDataCenterList($is_ssl, $fms_deny_list = null,$datacenter_keys = null, $fms_version = "", $use_global_link = 0) {
        foreach($datacenter_keys as $dc_key) {
            $where  = " datacenter_key = '".addslashes($dc_key)."'".
                      " AND server_priority > 0".
                      " AND is_available = 1".
                      " AND is_ssl = ".$is_ssl.
                      " AND use_global_link = $use_global_link";
            if ( $fms_version ) {
                $where .= " AND server_version >= '".mysql_real_escape_string($fms_version)."'";
            }
            if ($fms_deny_list) {
                $fms_deny_list_comma_separated = implode(",", $fms_deny_list);
                $where .= " AND NOT server_key IN (". $fms_deny_list_comma_separated .")";
            }
            $sort = array(
                    "server_count" =>  "asc",
                    "server_priority" => "desc");
                    $this->logger2->debug($where);
            if ($server_list = $this->getRowsAssoc($where, $sort)) {
                if ($server_key = $this->getActiveServer($server_list)) {
                    $this->logger2->debug($server_key);
                    return $server_key;
                }
            }
        }
        return false;
    }

    function getGlobalLinkFmsList($is_ssl, $fms_deny_list, $country_list ,$fms_version) {
        foreach($country_list as $country_key) {
            $where  = " datacenter_key = 2001".
                      " AND server_priority > 0".
                      " AND is_available = 1".
                      " AND is_ssl = ".$is_ssl.
                      " AND use_global_link = 1".
                      " AND server_country = '".$country_key."'";
            if ( $fms_version ) {
                $where .= " AND server_version >= '".mysql_real_escape_string($fms_version)."'";
            }
            if ($fms_deny_list) {
                $fms_deny_list_comma_separated = implode(",", $fms_deny_list);
                $where .= " AND NOT server_key IN (". $fms_deny_list_comma_separated .")";
            }
            $sort = array(
                    "server_count" =>  "asc",
                    "server_priority" => "desc");
            if ($server_list = $this->getRowsAssoc($where, $sort)) {
                if ($server_key = $this->getActiveServer($server_list)) {
                    $this->logger2->debug($server_key);
                    return $server_key;
                }
            }
        }
        return false;
    }


    /**
     * 指定したFMSサーバ一覧から接続可能FMSを検出する
     */
    public function getActiveServer($server_list = array()) {
        $cnt = 1;
        // FMSがなくなるまで
        while($server_list) {
            // ランダムな選択
            $sum = 0;
            foreach($server_list as $server) {
                $sum += $server['server_priority'];
            }
            $rand = rand(1,$sum);
            $i = 0;
            foreach($server_list as $key => $server) {
                $server_priority = $server['server_priority'];
                if (($i <= $rand) && ($rand <= $i + $server_priority)) {
                    break;
                }
                $i += $server_priority;
            }
            // 接続可能なFMS
            if ($server['server_key'] > 1000) {
                $fms_address = $server['server_address'];
            } else {
                $fms_address = $server['local_address'] ? $server['local_address'] : $server['server_address'];
            }
            if ($this->isActive($fms_address)) {
                $where = "server_key = ".$server['server_key'];
                $data = array(
                    "server_count" => $server["server_count"] + 1,
                    "error_count" => 0
                    );
                $ret = $this->update($data, $where);
                if (DB::isError($ret)) {
                    $this->logger2->error($ret->getUserInfo());
                    return false;
                }
                return $server['server_key'];
            // 接続できない
            } else {
                // 一回だけ送信
                $where = "server_key = ".$server['server_key'];
                $data  = array("error_count" => $server["error_count"] + 1);
                $ret = $this->update($data, $where);
                if (DB::isError($ret)) {
                    $this->logger2->error($ret->getUserInfo());
                    return false;
                }
                if ($server["error_count"] > 10) {
                    // 一回だけ送信
                    $semaphore_file = N2MY_APP_DIR."/var/fms_error".$server['server_address'];
                    if (!file_exists($semaphore_file) && 1 != $this->config["ALERT_MAIL"]["sendFmsReport"]) {
                        touch($semaphore_file);
                        $mail_subject = "[警告] FMS接続障害";
                        $mail_body = "会議開始時のFMS接続失敗しました" ."\n" .
                            "至急、以下のFMS、および設定を確認してください。" ."\n\n" .
                            "■ URL: ".N2MY_BASE_URL."\n" .
                            "■ FMS サーバ: ".print_r($server, true) ."\n\n";
                        $mail_body .= $this->logger2->error();
                        $obj_SendMail = new EZSmtp(null, "ja", "UTF-8");
                        if (defined("N2MY_ALERT_FROM") && defined("N2MY_ALERT_TO") && N2MY_ALERT_FROM && N2MY_ALERT_TO) {
                            $obj_SendMail->send2(N2MY_ALERT_FROM, N2MY_ALERT_TO, $mail_subject, $mail_body);
                        }
                        $this->logger2->warn(array(N2MY_ALERT_FROM, N2MY_ALERT_TO, $mail_subject, $mail_body));
                    }
                }
                // 永久ループ対策
                if ($cnt > 1000) {
                    return false;
                }
                // 指定から外す
                unset($server_list[$key]);
                $cnt++;
            }
        }
        // 一回だけ送信
        $semaphore_file = N2MY_APP_DIR."/var/fms_error";
        if (!file_exists($semaphore_file) && 1 != $this->config['ALERT_MAIL']['sendFmsReport']) {
            touch($semaphore_file);
            $mail_subject = "[警告] FMS接続障害";
            $mail_body = "接続可能なFMSがありませんでした。" ."\n" .
                "至急、以下のFMSおよび設定を確認してください。" ."\n\n" .
                "■ URL: ".N2MY_BASE_URL."\n" .
                "■ FMS サーバ: ".print_r($server_list, true) ."\n\n";
            $mail_body .= $this->logger2->error();
            $obj_SendMail = new EZSmtp(null, "ja", "UTF-8");
            if (defined("N2MY_ALERT_FROM") && defined("N2MY_ALERT_TO") && N2MY_ALERT_FROM && N2MY_ALERT_TO) {
                $obj_SendMail->send2(N2MY_ALERT_FROM, N2MY_ALERT_TO, $mail_subject, $mail_body);
            }
            $this->logger2->warn(array(N2MY_ALERT_FROM, N2MY_ALERT_TO, $mail_subject, $mail_body));
            return false;
        }
/*
        // 指定回数以上、または同一サーバキーに回ってきた場合は終了
        if (is_array($server_list)) {
            foreach($server_list as $server) {
                // 優先度変更
                $this->_counterIncrease($server['server_key'], 1);
                $fms_address = $server["local_address"] ? $server["local_address"] : $server["server_address"];
                if ($this->isActive($fms_address) == false) {
                    $where = "server_key = ".$server['server_key'];
                    $data  = array("error_count" => $server["error_count"] + 1);
                    $ret = $this->update($data, $where);
                    if (DB::isError($ret)) {
                        $this->logger2->error($ret->getUserInfo());
                        return false;
                    }
                    if ($server["error_count"] > 10) {
                        // 一回だけ送信
                        $semaphore_file = N2MY_APP_DIR."/var/fms_error".$server['server_address'];
                        if (!file_exists($semaphore_file) && 1 != $this->config["ALERT_MAIL"]["sendFmsReport"]) {
                            touch($semaphore_file);
                            $mail_subject = "[警告] FMS接続障害";
                            $mail_body = "会議開始時のFMS接続失敗しました" ."\n" .
                                "至急、以下のFMS、および設定を確認してください。" ."\n\n" .
                                "■ URL: ".N2MY_BASE_URL."\n" .
                                "■ FMS サーバ: ".print_r($server, true) ."\n\n";
                            $mail_body .= $this->logger2->error();
                            $obj_SendMail = new EZSmtp(null, "ja", "UTF-8");
                            if (defined("N2MY_ALERT_FROM") && defined("N2MY_ALERT_TO") && N2MY_ALERT_FROM && N2MY_ALERT_TO) {
                                $obj_SendMail->send2(N2MY_ALERT_FROM, N2MY_ALERT_TO, $mail_subject, $mail_body);
                            }
                            $this->logger2->warn(array(N2MY_ALERT_FROM, N2MY_ALERT_TO, $mail_subject, $mail_body));
                        }
                    }
                } else {
                    $where = "server_key = ".$server['server_key'];
                    $data  = array(
                        "server_count" => $server["server_count"] + 1,
                        "error_count" => 0
                        );
                    $ret = $this->update($data, $where);
                    if (DB::isError($ret)) {
                        $this->logger2->error($ret->getUserInfo());
                        return false;
                    }
                    $fms_server_key = $server['server_key'];
                    break;
                }
            }
        }
        if ($fms_server_key) {
            return $fms_server_key;
        } else {
            // 一回だけ送信
            $semaphore_file = N2MY_APP_DIR."/var/fms_error";
            if (!file_exists($semaphore_file) && 1 != $this->config['ALERT_MAIL']['sendFmsReport']) {
                touch($semaphore_file);
                $mail_subject = "[警告] FMS接続障害";
                $mail_body = "接続可能なFMSがありませんでした。" ."\n" .
                    "至急、以下のFMSおよび設定を確認してください。" ."\n\n" .
                    "■ URL: ".N2MY_BASE_URL."\n" .
                    "■ FMS サーバ: ".print_r($server_list, true) ."\n\n";
                $mail_body .= $this->logger2->error();
                $obj_SendMail = new EZSmtp(null, "ja", "UTF-8");
                if (defined("N2MY_ALERT_FROM") && defined("N2MY_ALERT_TO") && N2MY_ALERT_FROM && N2MY_ALERT_TO) {
                    $obj_SendMail->send2(N2MY_ALERT_FROM, N2MY_ALERT_TO, $mail_subject, $mail_body);
                }
                $this->logger2->warn(array(N2MY_ALERT_FROM, N2MY_ALERT_TO, $mail_subject, $mail_body));
                return false;
            }
        }
        */
    }

    /**
     * 動作可能なFMSか確認する
     */
    function isActive($server_address, $instance = "") {
        // Windowsで動作しないのは困るので暫定処理
        if (substr(PHP_OS,0,3) == "WIN") {
            // アプリ一覧
            $id       = $this->config['GLOBAL']['fms_id'];
            $pw       = $this->config['GLOBAL']['fms_password'];
            $port     = $this->config['GLOBAL']['fms_port'];
            $app_name = $this->config['CORE']['app_name'];
            $url = 'http://'.$server_address.':'.$port.'/admin/getApps?auser='.urlencode($id).'&apswd='.urlencode($pw);
            $curl = curl_init();
            $option = array(
                CURLOPT_URL => $url,
                CURLOPT_HEADER => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 10,
                CURLOPT_TIMEOUT => 10,
            );
            curl_setopt_array($curl, $option);
            $str = curl_exec($curl);
            curl_close($curl);
            if (!$str) {
                $this->logger2->warn($url, "getApps Error");
                return false;
            } else {
                $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array($url, $str));
            }
            $xml = new EZXML();
            $xml_data = $xml->openXML($str);
            if (!$xml_data) {
                $this->logger2->error($xml_data);
                return false;
            } else {
                if ($xml_data["result"]["code"][0]["_data"] != "NetConnection.Call.Success") {
                    $this->logger->warn(__FUNCTION__,__FILE__,__LINE__,$xml_data["result"]["code"][0]["_data"]);
                    return false;
                } else {
                    $datas = $xml_data["result"]["data"][0];
                    foreach($datas as $key => $appName){
                        if (($key != "_attr") && ($appName[0]["_data"])) {
                            $appList[] = $appName[0]["_data"];
                        }
                    }
                }
            }
            // 指定したアプリケーションが存在するか確認
            if (!in_array($app_name, $appList)) {
                $this->logger->warn(__FUNCTION__."#The application doesn't exist.",__FILE__,__LINE__,array($appList,$app_name));
                return false;
            } else {
                return true;
            }
        } else {
            $app_name    = $this->config['CORE']['app_name'];
            $rtmp = new EZRtmp();
            // 画像貼り付け
            $uri = "rtmp://".$server_address."/".$app_name;
            if ($instance) {
                $uri .= "/".$instance;
            }
            $cmd = "\"".
                "h = RTMPHack.new;" .
                " h.connection_uri = '".$uri."';" .
                " h.connection_args = [0x900];" .
                " h.method_name = 'aliveCheck';" .
                " h.execute;".
                "\"";
            $str = $rtmp->run($cmd);
            if ($str == "alive") {
                return true;
            } else {
                $this->logger2->warn(array($cmd, $str));
                return false;
            }
        }
    }

    private function _counterIncrease( $server_key, $step )
    {
        $where = "server_key = ".$server_key;
        $data  = array("error_count" => $step,
                        "update_datetime" => date( "Y-m-d H:i:s" ) );
        $ret = $this->update($data, $where);
        if (DB::isError($ret)) {
            $this->logger2->error($ret->getUserInfo());
            return false;
        }
    }

    /**
     * FMSのメンテナンスモード設定
     */
    public function setStatus($fms_address, $status = "stop") {
        if ($status == "start") {
            $status = 1;
        } elseif ($status == "stop") {
            $status = 0;
        } else {
            $this->logger2->error(array($fms_address, $status));
            return false;
        }
        $this->logger2->info(array($fms_address, $status));
        $where = "server_address = '".addslashes($fms_address)."'";
        $data  = array("is_available" => $status);
        $ret = $this->update($data, $where);
        if (DB::isError($ret)) {
            $this->logger2->error($ret->getUserInfo());
            return false;
        }
        return true;
    }
    
    public function getServerRecordByServerKey($serverKey) {
    	$where = "server_key = '".addslashes($serverKey)."'";
    	return $this->getRow($where);
    }
}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
