<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once ("classes/AppFrame.class.php");
require_once ("classes/N2MY_Reservation.class.php");
require_once ("classes/mgm/MGM_Auth.class.php");
require_once ('lib/EZLib/EZUtil/EZDate.class.php');
require_once ("lib/EZLib/EZUtil/EZString.class.php");
require_once ("classes/dbi/reservation_user.dbi.php");
require_once ("classes/dbi/ordered_service_option.dbi.php");

class AppReservation extends AppFrame {

    var $_name_space               = null;
    var $_reservation              = null;
    var $_ssl_mode                 = null;
    var $_reservation_obj          = null;
    var $is_saved_checked_clip     = false;
    private $obj_N2MY_Reservation  = null;
    static $MAX_CLIP_LIST = 12;

    /**
     * 初期処理
     */
    function init() {
        $this->_name_space = md5(__FILE__);
    }

    /**
     * ログイン認証
     */
    function auth() {
        $this->checkAuth();
        // 認証後のみ
        $server_info = $this->session->get("server_info");
        $this->obj_N2MY_Reservation = new N2MY_Reservation($this->get_dsn(), $server_info["host_name"]);
    }

    function action_detail() {

        $member_info = $this->session->get( "member_info" );
        $user_info   = $this->session->get( "user_info" );

        $reservation_session = $this->request->get('reservation_session');
        $reservation_data    = $this->obj_N2MY_Reservation->getDetail($reservation_session);
        if(!$reservation_data || $reservation_data["info"]["user_key"] != $user_info["user_key"]){
            header("Location: /services/reservation/");
        }
        if (!$this->isAuthorized($reservation_data, '?action_detail=&reservation_session='.$reservation_session)) {
            return false;
        }
        // 部屋のオプション確認
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $reservation_data["info"]["room_option"] = $obj_N2MY_Account->getRoomOptionList($reservation_data["info"]["room_key"]);
        require_once ("classes/dbi/room.dbi.php");
        $room_obj  = new RoomTable($this->get_dsn());
        $room_info = $room_obj->getRow("room_key = '".$reservation_data["info"]["room_key"]."'", "cabinet,disable_rec_flg");
        $reservation_data["info"]["room_option"]["cabinet"] = $room_info["cabinet"];
        $reservation_data["info"]["room_option"]["disable_rec_flg"] = $room_info["disable_rec_flg"];
        $this->setReservationData($reservation_data,"modify");

        if ((($user_info["account_model"] == "member" && !$user_info["member_multi_room_flg"]) || $user_info["account_model"] == "centre") &&
            $member_info["room_key"] != $reservation_data["info"]["room_key"] ){
            $reservation_data["info"]["status"] = "end";
        }

        $service_option  = new OrderedServiceOptionTable($this->get_dsn());
        $enable_teleconf = $service_option->enableOnRoom(23, $reservation_data["info"]["room_key"]); // 23 = teleconf
        $is_trial        = $user_info['user_status'] == 2;

        if (!$this->config->get("IGNORE_MENU", "teleconference") && !$is_trial && $enable_teleconf){
            $reservation_data['info'] = array_merge($reservation_data['info'],
                                                    $this->getPGiDetail($reservation_data['info']['meeting_key']));
        }

        $reservation_data['info']["reservation_starttime"] = EZDate::getLocateTime($reservation_data['info']["reservation_starttime"], N2MY_USER_TIMEZONE,
                                           $reservation_data['info']["reservation_place"]);
        $reservation_data['info']["reservation_endtime"] = EZDate::getLocateTime($reservation_data['info']["reservation_endtime"], N2MY_USER_TIMEZONE,
            $reservation_data['info']["reservation_place"]);

        try {
            //$reservation_info['info']['clips']  = $this->findClipKeysByMeetingTicket($reservation_data['info']['meeting_key']);
            //$this->template->assign('clip_list' , $this->getClipValueByKeys($reservation_info['info']['clips'], $user_info['user_key']));
          $reservation_data['clips'] = $this->getMeetingClips($reservation_data["info"]["meeting_key"]);
        } catch (Exception $e) {
            $this->logger2->error($e->getMessage());
            return $this->action_list();
        }

        require_once("classes/dbi/meeting.dbi.php");
        $objMeeting = new MeetingTable($this->get_dsn());
        $pin_cd     = $objMeeting->getOne("meeting_ticket = '".addslashes($reservation_data['info']['meeting_key'])."'", "pin_cd");
        $this->template->assign("pin_cd" , $pin_cd);
        $reservation_data['up_file_count'] = count($reservation_data["documents_dsp"]);
        $this->template->assign(array('enable_teleconf'   => $enable_teleconf,
                                      'service_name'      => $user_info['service_name'],
                                      'is_trial'          => $is_trial,
                                      'is_detail'         => true,
                                      'main_phone_number' => @$main_phone_number,
                                      'reservation_data'  => $reservation_data,
                                      ));
        $this->display('user/reservation/detail_view.t.html');
    }

    /**
     * 予約画面一覧を表示
     */
    function action_list() {
        $this->session->remove('reservation_auth', $this->_name_space);
        $user_info   = $this->session->get( "user_info" );
        $member_info = $this->session->get( "member_info" );
        ( (($user_info["account_model"] == "member") || $user_info["account_model"] == "centre") && $member_info )?
            $this->render_participantedList():
            $this->render_list();
    }

    function action_timezone() {
        $this->display('user/reservation/timezone.t.html');
    }
    function action_pgi_description() {
        $this->display('user/reservation/pgi_description.t.html');
    }
    /**
     * 予約一覧
     */
    //{{{action_create_clip_relation
    function action_create_clip_relation($is_modify = false)
    {
        require_once ("classes/dbi/clip.dbi.php");
        require_once ("classes/ClipFormat.class.php");

        $user_info    = $this->session->get("user_info");
        $member_info  = $this->session->get("member_info");
        $clip_table   = new ClipTable($this->get_dsn());
        $meeting_table= new MeetingTable($this->get_dsn());

        if ($is_modify) {
            $reservation  = $this->getReservationData("modify");
            if (!$reservation) {
                return $this->action_top();
            }
        }
        $clips = $this->saveCheckedClipStatus();
        // アップロードしているクリップを持ってくる
        $page = @intval($this->request->get("page"));
        $page = $page ? $page : 1;
        try {
            $total     = $clip_table->countConvertedByUserKey($user_info['user_key']);
            $clip_list = $clip_table->findConvertedByUserKey($user_info['user_key'],
                                                             $sort    = array('createtime'=>'desc'),
                                                             $limit   = self::$MAX_CLIP_LIST,
                                                             $offset  = self::$MAX_CLIP_LIST * ($page - 1));
            $clip_keys = array();
            if ($clip_list) {
                foreach ($clip_list as $clip) {
                    $clip_keys[] = $clip['clip_key'];
                }
            }
        } catch (Exception $e) {
            $this->logger2->error($e->getMessage());
            return $this->action_top();
        }
        $pager = $this->setPager(self::$MAX_CLIP_LIST, $page  , $total);

        // 共通テンプレ内のaction切り替え
        $video_config = $this->config->getAll("VIDEO");
        $this->template->assign(array('max_relation'    => $video_config['max_relation'],
                                      'base_action'     => $is_modify ? 'modify_clip_relation' : 'create_clip_relation',
                                      "clip_list"       => $this->mergeClipDisplayInfo($clip_list),
                                      "relay_clip_keys" => $clips,
                                      "pager"           => $pager));
        if ($is_modify) {
            $enable_delete = $reservation['info']['status'] != 'now';
            if (!$enable_delete) {
                $checked_clip_keys = $this->findClipKeysByMeetingTicket($reservation['info']['meeting_key']);
                $this->template->assign('checked_clips', $this->arrayToJavaScript($checked_clip_keys));
            }
            $this->template->assign('enable_delete', $enable_delete);
        }
        $this->set_submit_key($this->_name_space);
        $this->display('user/reservation/relation_clip.t.html');
    }
    //}}}
    function action_top($room_key = "") {
        $this->session->remove("not_change_reservation_data");
        $this->session->remove('reservation_auth', $this->_name_space);
        $room_list = $this->get_room_info();
        if (!$room_key) {
            if (count($room_list) == 1) {
                $room_info = array_shift($room_list);
                $room_key  = $room_info["room_info"]["room_key"];
            }
        }
        $user_info = $this->session->get("user_info");
        $room_key  = $this->request->get("room_key", $room_key);
        $page      = $this->request->get("page", 1);
        $limit     = $this->request->get("page_cnt", 20);
        $options = array(
            "user_key"   => $user_info["user_key"],
            "room_key"   => $room_key,
            "start_time" => date("Y-m-d H:i:s"),
            "page"       => $page,
            "limit"      => $limit,
            "offset"     => $limit * ($page - 1),
            "sort_key"   => $this->request->get("sort_key")  ? $this->request->get("sort_key")  : "reservation_starttime",
            "sort_type"  => $this->request->get("sort_type") ? $this->request->get("sort_type") : "asc",
        );
        $sort_columns = array("reservation_name" , "reservation_starttime");
        // ソートタイプのバリデート
        if(!in_array($options["sort_key"], $sort_columns)){
            $options["sort_key"] = "reservation_starttime";
        }
        $sort_types = array("asc" , "desc" , "ASC" , "DESC");
        if(!in_array($options["sort_type"], $sort_types)){
            $options["sort_type"] = "asc";
        }
        $this->session->set("condition", $options, $this->_name_space);
        $this->render_top();
    }

    public function action_sort_list() {
        $search = $this->session->get("condition", $this->_name_space);
        $search["page"]         = 1;
        $search["sort_key"]     = $this->request->get("sort_key")  ? $this->request->get("sort_key")  : "reservation_starttime";
        $search["sort_type"]    = $this->request->get("sort_type") ? $this->request->get("sort_type") : "desc";
        $sort_columns = array("reservation_name" , "reservation_starttime");
        if(!in_array($search["sort_key"], $sort_columns)){
            $search["sort_key"] = "reservation_starttime";
        }

        $sort_types = array("asc" , "desc" , "ASC" , "DESC");
        if(!in_array($search["sort_type"], $sort_types)){
            $search["sort_type"] = "asc";
        }
        $this->session->set("condition", $search, $this->_name_space);
        $this->render_top();
    }

    /**
     * 予約一覧表示
     */
    function render_top() {
        $options     = $this->session->get("condition", $this->_name_space);
        $member_info = $this->session->get("member_info");
        $user_info   = $this->session->get("user_info");
        $room_info   = $this->session->get("room_info");

        if ( (($user_info["account_model"] == "member" && !$user_info["member_multi_room_flg"]) || $user_info["account_model"] == "centre") && $member_info ){
            $reservation_list = $this->obj_N2MY_Reservation->getParticipantedMeetingList( $member_info, $options, $this->session->get('time_zone'), $room_info );
            $total            = $this->obj_N2MY_Reservation->getParticipantedMeetingCount( $member_info, $options );
        } else {
            $reservation_list = $this->obj_N2MY_Reservation->getList($options["room_key"],
                                                                     $options, $this->session->get('time_zone'));
            $total            = $this->obj_N2MY_Reservation->getCount($options["room_key"], $options);
        }
        $pager = $this->setPager($options["limit"], $options["page"], $total);

        $this->template->assign("room_key"        , $options["room_key"]);
        $this->template->assign("pager"           , $pager);
        $this->template->assign("conditions"      , $options);
        $this->template->assign("reservation_list", $reservation_list);

        //$this->display('user/reservation/list.t.html');
    }

    /**
     * 会議予約確認画面
     */
    function action_create() {
        // 新規作成時
        if ($this->request->get("new") == "1" || $this->session->get("reservation_edit")) {
            $this->clearReservationData("create"); // 入力をキャンセル
            $this->session->remove("reservation_edit");
            // デフォルトの日時を指定
            $now_time     = EZDate::getLocateTime(time(), N2MY_USER_TIMEZONE, N2MY_SERVER_TIMEZONE);
            $start_time   = $now_time + (300 - ($now_time % 300));
            // 選択肢を初期化
            $room_list    = $this->get_room_info();
            $default_room = array_shift($room_list);
            $user_info    = $this->session->get("user_info");
            $member_info  = $this->session->get("member_info");
            $room_key     = $this->request->get("room_key", $default_room["room_info"]["room_key"]);

            //部屋のオプション確認
            require_once("classes/N2MY_Account.class.php");
            $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
            $room_option      = $obj_N2MY_Account->getRoomOptionList($room_key);

            require_once ("classes/dbi/room.dbi.php");
            $room_obj  = new RoomTable($this->get_dsn());
            $room_col  = "cabinet,disable_rec_flg,is_wb_no,is_convert_wb_to_pdf";
            if (!$this->config->get("IGNORE_MENU", "teleconference")) {
                $room_col .= ",use_teleconf,use_pgi_dialin,use_pgi_dialin_free,use_pgi_dialout";
            }
            $room_info = $room_obj->getRow("room_key = '".addslashes($room_key)."'", $room_col);
            // 予約内容
            $reservation_data["info"] = array(
                "country_id"            => $this->session->get("country_key"),
                "reservation_starttime" => $start_time,
                "reservation_endtime"   => strtotime("next hour", $start_time),
                "reservation_place"     => $this->session->get("time_zone", _EZSESSION_NAMESPACE, N2MY_SERVER_TIMEZONE),
                "user_key"              => $user_info["user_key"],
                "room_key"              => $room_key,
                "send_flg"              => 1,
                "sender"                => ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") ? $member_info["member_name"] :"",
                "sender_mail"           => ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") ? $member_info["member_email"] :"",
                "mail_body"             => "",
                "reservation_place"     => $this->session->get("time_zone"),
                "auto_addressbook_flg"  => 0,
                "security_pw_flg"       => $user_info["is_compulsion_pw"],
                "is_limited_function"   => 0,
                "is_multicamera"        => $room_option["multicamera"]         ? $room_option["multicamera"]        : "0",
                "is_telephone"          => $room_option["telephone"]           ? $room_option["telephone"]          : "0",
                "is_h323"               => $room_option["h323_number"]         ? $room_option["h323_number"]        : "0",
                "is_mobile_phone"       => $room_option["mobile_phone_number"] ? $room_option["mobile_phone_number"]: "0",
                "is_high_quality"       => $room_option["high_quality"]        ? $room_option["high_quality"]       : "0",
                "is_desktop_share"      => $room_option["desktop_share"]       ? $room_option["desktop_share"]      : "0",
                "is_meeting_ssl"        => $room_option["meeting_ssl"]         ? $room_option["meeting_ssl"]        : "0",
                "is_invite_flg"         => 1,
                "is_cabinet_flg"        => $room_info["cabinet"] ? $room_info["cabinet"] :"0",
                "is_wb_no_flg"          => $room_info["is_wb_no"],
                );
            if($room_info["disable_rec_flg"] == 1){
              $reservation_data["info"]["is_rec_flg"] = 0;
            }
            else {
              $reservation_data["info"]["is_rec_flg"] = 1;
            }
            // クッキーに保存されてる差出人情報を初期値にする
            if(empty($reservation_data["info"]["sender"]) && $_COOKIE["sender_name_secure"]){
              $reservation_data["info"]["sender"] = $_COOKIE["sender_name_secure"];
              $reservation_data["name_save"] = 1;
            }

            if(empty($reservation_data["info"]["sender_mail"]) && $_COOKIE["sender_email_secure"]){
              $reservation_data["info"]["sender_mail"] = $_COOKIE["sender_email_secure"];
              $reservation_data["email_save"] = 1;
            }
            // クッキーに保存されてる差出人情報を初期値にする
            if($_COOKIE["organizer_name_secure"]){
                $reservation_data["organizer"]["name"] = $_COOKIE["organizer_name_secure"];
                $reservation_data["organizer_name_save"] = 1;
            }

            if($_COOKIE["organizer_email_secure"]){
                $reservation_data["organizer"]["email"] = $_COOKIE["organizer_email_secure"];
                $reservation_data["organizer_email_save"] = 1;
            }
            if($user_info["account_model"] == "member" || $user_info["account_model"] == "centre"){
              $reservation_data["info"]["sender"] = $member_info["member_name"];
              $reservation_data["info"]["sender_mail"] = $member_info["member_email"];
            }
            $reservation_data["info"]["room_option"]            = $room_option;
            $reservation_data["info"]["room_option"]["cabinet"] = $room_info["cabinet"];
            $reservation_data["info"]["room_option"]["disable_rec_flg"] = $room_info["disable_rec_flg"];
            $reservation_data["info"]["room_option"]["is_convert_wb_to_pdf"] = $room_info["is_convert_wb_to_pdf"];
            $reservation_data["guest_flg"] = 1;
            $reservation_data["guests"]    = array();
            if (!$this->config->get("IGNORE_MENU", "teleconference") && $room_info['use_teleconf']){
                $reservation_data["info"]["use_pgi_dialin"]      = $room_info['use_pgi_dialin'];
                $reservation_data["info"]["use_pgi_dialin_free"] = $room_info['use_pgi_dialin_free'];
                $reservation_data["info"]["use_pgi_dialout"]     = $room_info['use_pgi_dialout'];
            }
            $this->setReservationData($reservation_data,"create");
        } else {
            if ($room_key = $this->request->get("room_key")) {
                $reservation_data = $this->getReservationData("create");
                $reservation_data["info"]["room_key"] = $room_key;
                $this->setReservationData($reservation_data,"create");
            }else{
               // header("Location: /services/reservation/");
            }
        }
        return $this->render_create();
    }

    /**
     * 会議予約画面の表示
     */
    function render_create($message = "") {
        $room_list    = $this->get_room_info();
        $default_room = array_shift($room_list);
       // エラーメッセージ表示
        if ($message && !$this->request->get('back')) {
            $this->template->assign('message', $message);
        }
        $_now = time() - (3600 * 24);

        $this->template->assign("start_limit", $_now);
        $this->template->assign("end_limit"  , $_now + (3600 * 24 * 367));

        // 会議予約情報取得、表示
        $reservation_data = $this->getReservationData("create");

        if($reservation_data["info"]["room_key"] == ""){
            header("Location: /services/reservation/");
        }

        $room_info        = $this->get_room_info($reservation_data["info"]["room_key"]);
        if($this->session->get("service_mode") == "sales") {
          //メンバー情報を更新
          require_once ("classes/dbi/member.dbi.php");
          $objMember = new MemberTable( $this->get_dsn());
          $member_info = $objMember->getDetailByRoomKey($reservation_data["info"]["room_key"]);
          $this->template->assign("member",$member_info);
          //窓口デフォルト設定を更新
          require_once ("classes/dbi/room.dbi.php");
          $objRoom = new RoomTable($this->get_dsn());
          $column = "default_auto_rec_use_flg,wb_allow_flg,chat_allow_flg,print_allow_flg,customer_camera_use_flg,customer_sharing_button_hide_status,customer_camera_status";
          $default_option = $objRoom->getRow("room_key='".$reservation_data["info"]["room_key"]."'", $column);
          $this->logger2->info($default_option);
          $reservation_data["info"]["is_auto_rec_flg"]               = $default_option["default_auto_rec_use_flg"];
          $reservation_data["info"]["is_wb_allow_flg"]               = $default_option["wb_allow_flg"];
          $reservation_data["info"]["is_chat_allow_flg"]             = $default_option["chat_allow_flg"];
          $reservation_data["info"]["is_print_allow_flg"]            = $default_option["print_allow_flg"];
          $reservation_data["info"]["is_customer_camera_flg"]        = $default_option["customer_camera_use_flg"];
          $reservation_data["info"]["is_customer_desktop_share_flg"] = $default_option["customer_sharing_button_hide_status"];
          $reservation_data["info"]["is_customer_camera_display"]    = $default_option["customer_camera_status"];
        }
        // 対応フォーマット取得
        require_once ("classes/N2MY_Document.class.php");
        $objDocument       = new N2MY_Document($this->get_dsn());
        $support_documents = $objDocument->getSupportFormat($reservation_data["info"]["room_key"]);
        $service_option    = new OrderedServiceOptionTable($this->get_dsn());
        $enable_teleconf   = $service_option->enableOnRoom(23, $reservation_data["info"]["room_key"]); // 23 = teleconf
        if (isset($support_documents["document_list"]["xdw"])) {
            unset($support_documents["document_list"]["xdw"]);
        }
        $this->template->assign(array(
                "document_list"    => join(", ", array_keys($support_documents["document_list"])),
                "image_list"       => join(", ", array_keys($support_documents["image_list"])),
                "display_filetype" => join(", ", array_keys($support_documents["image_list"] + $support_documents["document_list"])),
                "room_info"        => $room_info,
                "reservation_data" => $reservation_data,
                "enable_teleconf"  => $enable_teleconf));

        if (!$this->config->get("IGNORE_MENU", "teleconference")){
            $user_info = $this->session->get("user_info");
            $this->template->assign(array("pgi_dial_configs" => $this->pgiDialConfigs($room_info['room_info']),
                                          "service_name"     => $user_info['service_name'],
                                          "is_trial"         => $user_info['user_status'] == 2));
        }
        $cookie_lang = $this->request->getCookie("lang");
        $this->template->assign("cookie_lang", $cookie_lang);
        $this->template->assign("varsion", $this->_getMeetingVersion());
        //$this->display('user/reservation/create.t.html');
    }

    /**
     * 入力データを保持したまま遷移
     */
    function action_change_room() {
        $request          = $this->request->getAll();
        $reservation_info = $this->formatReservationData($request,"create");

        //部屋のオプション確認
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $reservation_info["info"]["room_option"] = $obj_N2MY_Account->getRoomOptionList($reservation_info["info"]["room_key"]);

        require_once ("classes/dbi/room.dbi.php");
        $room_col  = "cabinet,disable_rec_flg,is_convert_wb_to_pdf";
        if (!$this->config->get("IGNORE_MENU", "teleconference")) {
            $room_col .= ",use_teleconf, use_pgi_dialin, use_pgi_dialin_free, use_pgi_dialout";
        }
        $room_obj   = new RoomTable($this->get_dsn());
        $room_info  = $room_obj->getRow("room_key = '".$reservation_info["info"]["room_key"]."'", $room_col);

        $reservation_info["info"]["room_option"]["cabinet"] = $room_info["cabinet"];
        $reservation_info["info"]["room_option"]["disable_rec_flg"] = $room_info["disable_rec_flg"];
        //機能制限データ初期化
        $reservation_info["info"]["is_multicamera"] = $reservation_info["info"]["room_option"]["multicamera"] ? $reservation_info["info"]["room_option"]["multicamera"] :"0";
        $reservation_info["info"]["is_limited_function"] = 0;
        $reservation_info["info"]["is_telephone"] = $reservation_info["info"]["room_option"]["telephone"] ? $reservation_info["info"]["room_option"]["telephone"] :"0";
        $reservation_info["info"]["is_h323"] = $reservation_info["info"]["room_option"]["h323_number"] ? $reservation_info["info"]["room_option"]["h323_number"] :"0";
        $reservation_info["info"]["is_mobile_phone"] = $reservation_info["info"]["room_option"]["mobile_phone_number"] ? $reservation_info["info"]["room_option"]["mobile_phone_number"] :"0";
        $reservation_info["info"]["is_high_quality"] = $reservation_info["info"]["room_option"]["high_quality"] ? $reservation_info["info"]["room_option"]["high_quality"] :"0";
        $reservation_info["info"]["is_desktop_share"] = $reservation_info["info"]["room_option"]["desktop_share"] ? $reservation_info["info"]["room_option"]["desktop_share"] :"0";
        $reservation_info["info"]["is_meeting_ssl"] = $reservation_info["info"]["room_option"]["meeting_ssl"] ? $reservation_info["info"]["room_option"]["meeting_ssl"] :"0";
        $reservation_info["info"]["is_invite_flg"]  = 1;
        if( $reservation_info["info"]["room_option"]["disable_rec_flg"] == 1){
          $reservation_info["info"]["is_rec_flg"] = 0;
        }
        else {
          $reservation_info["info"]["is_rec_flg"] = 1;
        }
        $reservation_info["info"]["is_cabinet_flg"] = $reservation_info["info"]["room_option"]["cabinet"] ? $reservation_info["info"]["room_option"]["cabinet"] :"0";
        $reservation_info["info"]["is_wb_no_flg"]   = 1;
        $reservation_info["info"]["room_option"]["is_convert_wb_to_pdf"] = $room_info["is_convert_wb_to_pdf"];

        if (!$this->config->get("IGNORE_MENU", "teleconference")) {
            $reservation_info["info"]["use_pgi_dialin"]      = ($room_info['use_pgi_dialin'] && $reservation_info["info"]['use_pgi_dialin']) ? 1: 0;
            $reservation_info["info"]["use_pgi_dialin_free"] = ($room_info['use_pgi_dialin_free'] && $reservation_info["info"]['use_pgi_dialin_free']) ? 1: 0;
            $reservation_info["info"]["use_pgi_dialout"]     = ($room_info['use_pgi_dialout'] && $reservation_info["info"]['use_pgi_dialout']) ? 1: 0;
            if (!$reservation_info["info"]["use_pgi_dialin"] &&
                !$reservation_info["info"]["use_pgi_dialin_free"]&&
                !$reservation_info["info"]["use_pgi_dialout"]) {
                $reservation_info["info"]["tc_type"] = 'voip';
            }
        }
        //メンバー情報を更新ために
        unset($reservation_info["staff"]);
        $this->setReservationData($reservation_info,"create");

        return $this->render_create();
    }

    /**
     * 確認処理
     * 入力画面１から
     *  case A : そのまま確認画面 : clip_flg  = false
     *  case B : クリップ選択     : clip_flg = true
     * 入力画面2(クリップ選択）からの確認 : after_relation_clip =true
     */
    function action_create_confirm() {
        // 入力値を保持
        $request   = $this->request->getAll();
        $user_info = $this->session->get("user_info");
        if ($user_info['use_clip_share'] && $request['after_relation_clip']) { // クリップ編集からの確認
            $this->saveCheckedClipStatus();
            $reservation_info = $this->getReservationData("create");
            $render_option    = array('back_action' => 'action_create_clip_relation');
        } else {
            $reservation_info = $this->formatReservationData($request,"create");
            $this->setReservationData($reservation_info,"create");
        }
        if ($message = $this->check($reservation_info)) {
            return $this->render_create($message);
        }
        if (!$this->config->get("IGNORE_MENU", "teleconference")){
            $service_option  = new OrderedServiceOptionTable($this->get_dsn());
            $enable_teleconf = $service_option->enableOnRoom(23, $reservation_info['info']['room_key']); // 23 = teleconf
            $this->template->assign("enable_teleconf" ,$enable_teleconf);
        }
        return $this->render_create_confirm($render_option);
    }

    /*
     * 資料アップ画面
     */
    function action_create_storage_relation(){
        $this->display('user/reservation/relation_storage.t.html');
    }

    /**
     * 確認画面
     */
    function render_create_confirm($options = array()) {
        // 二重登録回避
        $this->set_submit_key($this->_name_space);
        $reservation_data = $this->getReservationData("create");
        $user_info        = $this->session->get("user_info");
        if($reservation_data['clips']){
            try {
                $this->template->assign("clip_list", @$reservation_data['clips']);
            } catch (Exception $e) {
                $this->logger2->error($e->getMessage());
                return $this->action_top();
            }
        }
        if ($options['back_action']) {
            $this->template->assign("back_action", $options['back_action']);
        }
        $this->template->assign("is_create" , 1);
        $this->template->assign(array("service_name"     => $user_info['service_name'],
                                      "reservation_data" => $reservation_data,
                                      "upload_status"    => $this->getUploadStatus($reservation_data)));
        $this->display('user/reservation/create_confirm.t.html');
    }

    /**
     * 会議予約登録
     */
    function action_create_complete() {
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_top();
        }
        $add_addrres = $_REQUEST["add_address"];
        $reservation_data = $this->getReservationData("create");
        $this->logger2->info($reservation_data);
        // 入力チェック
        if ($message = $this->check($reservation_data)) {
            return $this->render_create($message);
        }
        // 資料事前アップロード設定
        $check_documents = $reservation_data["documents"];
        $create_documents = array();
        if (!$reservation_data["upload_flg"]) {
            foreach ($check_documents as $document_id => $document) {
                if (!$check_documents[$document_id]["tmp_name"]) {
                    $create_documents[$document_id] = $document[$document_id];
                }
            }
            $reservation_data["documents"] = $create_documents;
        }
        // いずれかのページでユーザーキーが設定なしの状態になったら
        if (!$reservation_data['info']['user_key']) {
            $this->logger2->warn($reservation_data, 'Invalid reservation.user_key');
            $user_info = $this->session->get("user_info");
            $reservation_data['info']['user_key'] = $user_info['user_key'];
        }

        if (!$this->config->get("IGNORE_MENU", "teleconference")){
            if ($reservation_data['info']['tc_type'] != 'pgi') {
                // デフォルトンのPGI設定反映
                $room_table = new RoomTable($this->get_dsn());
                $room       = $room_table->getRow("room_key = '".$reservation_data["info"]["room_key"]."'");
                $reservation_data['info']['use_pgi_dialin']      = $room['use_pgi_dialin'];
                $reservation_data['info']['use_pgi_dialin_free'] = $room['use_pgi_dialin_free'];
                $reservation_data['info']['use_pgi_dialout']     = $room['use_pgi_dialout'];
            }
            if ($reservation_data['info']['tc_type'] != 'etc') {
                $reservation_data['info']['tc_teleconf_note'] = '';
            }
        }
        // 差出人情報記憶
        // 有効期限
        $limit_time = time() + 365 * 24 * 3600;
        setcookie("sender_name_secure", "", (time()-1800), N2MY_COOKIE_PATH,$_SERVER["HTTP_HOST"],N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("sender_email_secure", "", (time()-1800), N2MY_COOKIE_PATH,$_SERVER["HTTP_HOST"],N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        if($reservation_data["name_save"]){
            setcookie("sender_name_secure", $reservation_data["info"]["sender"],  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        } else {
          setcookie("sender_name_secure", "", (time()-1800), N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        }
        if($reservation_data["email_save"]){
            setcookie("sender_email_secure", $reservation_data["info"]["sender_mail"],  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        } else {
          setcookie("sender_email_secure", "", (time()-1800), N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        }

        if($reservation_data["info"]["is_organizer_flg"] == 1){
            if($reservation_data["organizer_name_save"]){
                setcookie("organizer_name_secure", $reservation_data["organizer"]["name"],  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
            } else {
              setcookie("organizer_name_secure", "", (time()-1800), N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
            }
            if($reservation_data["organizer_email_save"]){
                setcookie("organizer_email_secure", $reservation_data["organizer"]["email"],  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
            } else {
              setcookie("organizer_email_secure", "", (time()-1800), N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
            }
        }
        $reservation_data["info"]["sender_lang"] =  $this->session->get("lang");
        $reservation_data["info"]["reservation_url"] = N2MY_BASE_URL;

        // polycom
        if (!$this->config->get("IGNORE_MENU", "video_conference") &&
            $reservation_data["info"]["room_option"]["video_conference"] != 0){
          /**
           * didのみ取得に変更
           */
            require_once("classes/dbi/ives_setting.dbi.php");
            $ivesOption = new IvesSettingTable($this->get_dsn());
            $ivesInfo = $ivesOption->getRow("room_key = '".$reservation_data["info"]["room_key"]."' AND is_deleted = 0");
            $reservation_data["info"]["ives_did"] = $ivesInfo["ives_did"];
        }
        $user_info = $this->session->get("user_info");
        $member_info = $this->session->get("member_info");
        $reservation_data["info"]["member_key"] = $member_info["member_key"];

        $ret = $this->obj_N2MY_Reservation->add($reservation_data); // 予約登録
        // アドレス帳自動保存 (ここで行っておいた方がセッションが切り替わらず安全)

        if($this->session->get("service_mode") == "sales"){
            if($add_addrres){
                // 関数の仕様上配列化
                $add_addrres_guest[] = $reservation_data["guest"];
                $this->obj_N2MY_Reservation->addAddressBookGuests($add_addrres_guest , $user_info["user_key"] , $member_info["member_key"]);
            }
        }else{
            foreach($add_addrres as $key => $val){
                $add_addrres_guest[] = $reservation_data["guests"][$key];
            }
            if(count($add_addrres_guest) >= 1){
               $this->obj_N2MY_Reservation->addAddressBookGuests($add_addrres_guest , $user_info["user_key"] , $member_info["member_key"]);
            }
        }
        $this->clearReservationData("create");// 入力データをクリア
        $this->action_top($reservation_data["info"]["room_key"]); // 登録完了（一覧表示）
    }

    /**
     * 認証画面
     */
    function action_auth() {
        $reservation_info = $this->obj_N2MY_Reservation->getDetail($this->request->get('reservation_session'));
        if (!$this->isAuthorized($reservation_info, '?action_auth=&reservation_session='.$this->request->get('reservation_session'))) {
            return false;
        }
        //部屋のオプション確認
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $reservation_info["info"]["room_option"] = $obj_N2MY_Account->getRoomOptionList($reservation_info["info"]["room_key"]);

        require_once ("classes/dbi/room.dbi.php");
        $room_obj  = new RoomTable($this->get_dsn());
        $room_info = $room_obj->getRow("room_key = '".$reservation_info["info"]["room_key"]."'", "cabinet,disable_rec_flg,is_convert_wb_to_pdf");
        $reservation_info["info"]["room_option"]["cabinet"] = $room_info["cabinet"];
        $reservation_info["info"]["room_option"]["disable_rec_flg"] = $room_info["disable_rec_flg"];
        $reservation_info["info"]["room_option"]["is_convert_wb_to_pdf"] = $room_info["is_convert_wb_to_pdf"];

        if (!$this->config->get("IGNORE_MENU", "teleconference")){
            $obj_Meeting  = new DBI_Meeting($this->get_dsn());
            $user_info = $this->session->get("user_info");
            $is_trial  = $user_info['user_status'] == 2;
            $this->template->assign("is_trial" , $is_trial);
            if (!$is_trial) {
                $meeting_data = $obj_Meeting->getRow(sprintf("meeting_ticket='%s' AND is_deleted = 0",
                                                     $reservation_info['info']['meeting_key']));
                $reservation_info['info'] = array_merge($reservation_info['info'],
                    array('tc_type'             => $meeting_data['tc_type'] ? $meeting_data['tc_type'] : 'voip',
                          'tc_teleconf_note'    => $meeting_data['tc_teleconf_note'],
                          'use_pgi_dialin'      => $meeting_data['use_pgi_dialin'],
                          'use_pgi_dialin_free' => $meeting_data['use_pgi_dialin_free'],
                          'use_pgi_dialout'     => $meeting_data['use_pgi_dialout']));
                $reservation_info['info'] = array_merge($reservation_info['info'],
                                                        $this->getPGiDetail($reservation_info['info']['meeting_key']));
            } else {
                $reservation_info['info']['tc_type'] = 'voip';
            }
        }

        // task : ユーザーオプション判定
        $user_info = $this->session->get("user_info");
        try {
            $reservation_info['clips'] = $this->getMeetingClips($reservation_info['info']['meeting_key']);
        } catch (Exception $e) {
            $this->logger2->error($e->getMessage());
            return $this->action_list();
        }
        $this->setReservationData($reservation_info,"modify");
        return $this->action_view();
    }

    /**
     * パスワードを入力
     */
    function render_pw($message = null) {
        if ($message){
            $this->template->assign('message', $message);
        }
        $this->display('user/reservation/auth.t.html');
    }

    /**
     * 参加者情報取得、表示
     */
    function action_view() {
        $data = $this->getReservationData("modify");
        $user_info = $this->session->get("user_info");
        if ($data["info"]['reservation_pw']) {
            $data["pw_flg"] = 2;
            $data["info"]['old_reservation_pw'] = $data["info"]['reservation_pw'];
        }
        $data["info"]["security_pw_flg"] = $user_info["is_compulsion_pw"];
        // 登録有りの場合はフォームを表示
        if ($data["guests"]||($data["guest"]&&$data["staff"])) {
            $data["guest_flg"] = 1;
        } else {
            $data["guest_flg"] = 0;
        }
        if ($data["documents"] || $data["clips"]) {
            $data["upload_flg"] = 1;
        }
        $this->setReservationData($data,"modify");
        $this->render_modify();
    }

    /**
     * 会議日程修正
     */
    function action_modify() {
        if ($this->request->get("first") == "1") {
            $reservation_session = $this->request->get('reservation_session');
            $reservation_data    = $this->obj_N2MY_Reservation->getDetail($reservation_session);
            //$this->logger2->info($reservation_data);
            $reservation_data["info"]["sender"]      = "";
            $reservation_data["info"]["sender_mail"] = "";
            $reservation_data["info"]["mail_body"]   = "";
            $reservation_data["info"]["auto_addressbook_flg"] = 0;
            //部屋のオプション確認
            require_once("classes/N2MY_Account.class.php");
            $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
            $reservation_data["info"]["room_option"] = $obj_N2MY_Account->getRoomOptionList($reservation_data["info"]["room_key"]);

            require_once ("classes/dbi/room.dbi.php");
            $room_obj  = new RoomTable($this->get_dsn());
            $room_info = $room_obj->getRow("room_key = '".$reservation_data["info"]["room_key"]."'", "cabinet,disable_rec_flg,is_convert_wb_to_pdf");
            $reservation_data["info"]["room_option"]["cabinet"] = $room_info["cabinet"];
            $reservation_data["info"]["room_option"]["disable_rec_flg"] = $room_info["disable_rec_flg"];
            $reservation_data["info"]["room_option"]["is_convert_wb_to_pdf"] = $room_info["is_convert_wb_to_pdf"];
            $this->setReservationData($reservation_data,"modify");
        }
        $this->render_modify();
    }

    /**
     * 予約詳細
     */
    function render_modify($message = "")
    {
        // エラーメッセージ表示
        if ($message && !$this->request->get('back')) {
            $this->template->assign('message', $message);
        }
        // カレンダー
        $_now = time() - (3600 * 24);
        $this->template->assign("start_limit", $_now);
        $this->template->assign("end_limit", $_now + (3600 * 24 * 367));
        $user_info = $this->session->get("user_info");
        // 編集中の詳細
        $reservation_data = $this->getReservationData("modify");
        if(!$reservation_data || $reservation_data["info"]["user_key"] != $user_info["user_key"]){
            header("Location: /services/reservation/");
        }
        // 主催者が差出人と同じなら「主催者を入力する」に変更する
        if($reservation_data["info"]["is_organizer_flg"] == 2){
            $reservation_data["info"]["is_organizer_flg"] = 1;
        }
        $reservation_data["is_modified"] = 1;
        //初期情報
        if(!isset($reservation_data["has_guest"]))
          $reservation_data["has_guest"] = $reservation_data["guest_flg"];
        if(!isset($reservation_data["mail_send_type"]))
          $reservation_data["mail_send_type"] = 2;
        $room_info        = $this->get_room_info($reservation_data["info"]["room_key"]);

        // 対応フォーマット情報取得
        $this->logger2->info($reservation_data);
        require_once ("classes/N2MY_Document.class.php");
        $objDocument           = new N2MY_Document($this->get_dsn());
        $support_document_list = $objDocument->getSupportFormat($reservation_data["info"]["room_key"]);
        if ($support_document_list["document_list"]["xdw"]) {
            unset($support_document_list["document_list"]["xdw"]);
        }
        // 変更前予約情報
        if (!$this->session->get( "not_change_reservation_data" )) {
            $this->session->set("not_change_reservation_data", $reservation_data);
        }
        $this->template->assign("document_list"    , join(", ", array_keys($support_document_list["document_list"])));
        $this->template->assign("image_list"       , join(", ", array_keys($support_document_list["image_list"])));
        $this->template->assign("display_filetype" , join(", ", array_keys($support_document_list["image_list"] + $support_document_list["document_list"])));
        $this->template->assign("room_info"        , $room_info);
        if (!$this->config->get("IGNORE_MENU", "teleconference")){
            $service_option  = new OrderedServiceOptionTable($this->get_dsn());
            $enable_teleconf = $service_option->enableOnRoom(23, $reservation_data["info"]["room_key"]); // 23 = teleconf
            $this->template->assign(array("enable_teleconf" => $enable_teleconf,
                                          "is_trial"        => $user_info['user_status'] == 2));
            if ($enable_teleconf) {
                $this->template->assign(array("pgi_dial_configs" => $this->pgiDialConfigs($room_info['room_info']),
                                              "service_name"     => $user_info['service_name']));
            }
        }
        $this->template->assign("is_modified",true);
        $this->template->assign("reservation_data" , $reservation_data);
        $this->template->assign("varsion", $this->_getMeetingVersion());
        $this->session->set("delete_documents", $reservation_data["delete_documents"]);
        $this->display('user/reservation/update.t.html');
    }
    /**
     * 予約変更確認画面表示
     */
    function action_modify_confirm() {
        $this->session->set("reservation_edit", "1");
        $request   = $this->request->getAll();
        $user_info = $this->session->get("user_info");

        if ($user_info['use_clip_share'] && $request['after_relation_clip']) {
            $this->saveCheckedClipStatus();
            $reservation_data = $this->getReservationData("modify");
            $render_option    = array('back_action' => 'action_modify_clip_relation');
        } else {
            $reservation_data = $this->formatReservationData($request,"modify");
        }
        $old_reservation_data = $this->session->get("not_change_reservation_data");
        $reservation_data["has_guest"] = $old_reservation_data["has_guest"];
        if($reservation_data["guest_flg"] == 0)
          $reservation_data["mail_send_type"] = 0;
        if($reservation_data["mail_send_type"] == 1)
        {
          //招待者情報を変更判別
          $old_guests = $old_reservation_data["guests"];
          $this->logger2->info($old_guests,"old guest");
          foreach($reservation_data["guests"] as $guset_key => $guest_info)
          {
            if(!empty($guest_info["r_user_session"]))
            {
              $flag = 0;
              if($guest_info["name"]!=$old_guests[$guest_info["r_user_session"]]["name"])
                $flag = 1;
              if($guest_info["email"]!=$old_guests[$guest_info["r_user_session"]]["email"])
                $flag = 1;
              if($guest_info["type"]!=$old_guests[$guest_info["r_user_session"]]["type"])
                $flag = 1;
              if($guest_info["timezone"]!=$old_guests[$guest_info["r_user_session"]]["timezone"])
                $flag = 1;
              if($guest_info["lang"]!=$old_guests[$guest_info["r_user_session"]]["lang"])
                $flag =1;
              if($flag == 1)
              {
                $reservation_data["guests"][$guset_key]["is_changed"] = 1;
                   $this->logger2->info($guest_info,"modify");
              }
            }
          }
        }
        $this->setReservationData($reservation_data,"modify");
        // 入力チェック
        if ($message = $this->check($reservation_data)) {
            return $this->render_modify($message);
        }
        if ($user_info['use_clip_share'] && $request['clip_flg']) {
            //return $this->action_modify_clip_relation();
        }
        return $this->render_modify_confirm($render_option);
    }
    //{{{action_modify_clip_relation
    function action_modify_clip_relation()
    {
        $this->action_create_clip_relation($is_modify = true);
    }
    //}}}
    //{{{render_modify_confirm
    function render_modify_confirm($options = array()) {
        // 二重登録回避
        $this->set_submit_key($this->_name_space);
        $reservation_data = $this->getReservationData("modify");
        $user_info = $this->session->get("user_info");
        try {
            $this->template->assign("clip_list", $this->getClipValueByKeys(@$reservation_data['info']['clips'],
                                                                           $user_info['user_key']));
        } catch (Exception $e){
            $this->logger2->error($e->getMessage());
            return $this->action_top();
        }
        if ($options['back_action']) {
            $this->template->assign("back_action", $options['back_action']);
        }
        if (!$this->config->get("IGNORE_MENU", "teleconference")){
            $service_option  = new OrderedServiceOptionTable($this->get_dsn());
            $enable_teleconf = $service_option->enableOnRoom(23, $reservation_data["info"]["room_key"]); // 23 = teleconf
            $this->template->assign("enable_teleconf" ,$enable_teleconf);
        }
        $this->logger2->debug($reservation_data);
        $this->template->assign("is_modified",true);
        $this->template->assign("not_change_reservation_data", $this->session->get("not_change_reservation_data"));
        $this->template->assign("reservation_data", $reservation_data);
        $this->template->assign("upload_status", $this->getUploadStatus($reservation_data));
        $this->display('user/reservation/update_confirm.t.html');
    }
    //}}}
    /**
     * 予約変更完了画面
     */
    function action_modify_complete() {
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_top();
        }
        $reservation_data = $this->getReservationData("modify");
        $this->logger2->info($reservation_data);
        $add_addrres = $_REQUEST["add_address"];
        // 入力チェック
        if ($message = $this->check($reservation_data)) {
            return $this->render_modify($message);
        }
        // 資料事前アップロード設定
        $check_documents = $reservation_data["documents"];
        $create_documents = array();
        if (!$reservation_data["upload_flg"]) {
            foreach ($check_documents as $document_id => $document) {
                if (!$check_documents[$document_id]["tmp_name"]) {
                    $create_documents[$document_id] = $document[$document_id];
                }
            }
            $reservation_data["documents"] = $create_documents;
        }
        if (!$this->config->get("IGNORE_MENU", "teleconference")){
            if ($reservation_data['info']['tc_type'] != 'pgi') {
                // デフォルトンのPGI設定反映
                $room_table   = new RoomTable($this->get_dsn());
                $room         = $room_table->getRow("room_key = '".$reservation_data["info"]["room_key"]."'");
                $reservation_data['info']['use_pgi_dialin']      = $room['use_pgi_dialin'];
                $reservation_data['info']['use_pgi_dialin_free'] = $room['use_pgi_dialin_free'];
                $reservation_data['info']['use_pgi_dialout']     = $room['use_pgi_dialout'];
            }
        }
        // 差出人情報記憶
        // 有効期限
        $limit_time = time() + 365 * 24 * 3600;

        $reservation_data["info"]["sender_lang"] =  $this->session->get("lang");

        // 開始時間が変更されていたらリマインダーメール送信フラグの更新
        $old_reservation_data = $this->session->get("not_change_reservation_data");
        $ord_start_time = strtotime($old_reservation_data["info"]["reservation_starttime"]);
        $new_start_time = strtotime($reservation_data["info"]["reservation_starttime"]);
        if($ord_start_time != $new_start_time){
            //更新用フラグ
            $reservation_data["start_time_modify"] = 1;
        }else {
            $reservation_data["start_time_modify"] = 0;
        }

        $this->obj_N2MY_Reservation->update($reservation_data); // 更新

        // アドレス帳自動保存 (ここで行っておいた方がセッションが切り替わらず安全)
        $user_info   = $this->session->get("user_info");
        $member_info = $this->session->get("member_info");
        if($this->session->get("service_mode") == "sales"){
            if($add_addrres){
                // 関数の仕様上配列化
                $add_addrres_guest[] = $reservation_data["guest"];
                $this->obj_N2MY_Reservation->addAddressBookGuests($add_addrres_guest , $user_info["user_key"] , $member_info["member_key"]);
            }
        }else{
            foreach($add_addrres as $key => $val){
                $add_addrres_guest[] = $reservation_data["guests"][$key];
            }
            if(count($add_addrres_guest) >= 1){
                $this->obj_N2MY_Reservation->addAddressBookGuests($add_addrres_guest , $user_info["user_key"] , $member_info["member_key"]);
            }
        }
        $this->clearReservationData("modify"); // 入力データをクリア
        $this->action_top($reservation_data["info"]["room_key"]); // 登録完了（一覧表示）
    }

    /**
     *
     */
    function sendMail($mail_list, $info, $mode) {
        $this->logger2->debug(array($mail_list, $info, $mode));
        $user_info = $this->session->get("user_info");
        // 送信者に送る
        require_once ("lib/EZLib/EZMail/EZSmtp.class.php");
        require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
        // Template
        $template = new EZTemplate($this->config->getAll('SMARTY_DIR'));
        // 招待者に送信
        $custom = $info["custom"]?$info["custom"]:$this->config->get('SMARTY_DIR','custom');
        if ($mail_list){
            $this->logger->info($mail_list);
            // 時差変換
            foreach ($mail_list as $key => $guest) {
                $lang_cd = EZLanguage::getLangCd($guest["lang"]);
                // 件名
                $subject = $this->getGuestMailSubject($mode, $lang_cd, $custom);
                if ($guest["timezone"] == "100") {
                    $time_zone = $info["reservation_place"];
                } else {
                    $time_zone = $guest["timezone"];
                }
                $mail_list[$key]["timezone"] = $time_zone;
                $mail_list[$key]["starttime"] = date("Y-m-d H:i:s" ,EZDate::getLocateTime($info["reservation_starttime"], $time_zone, N2MY_SERVER_TIMEZONE));;
                $mail_list[$key]["endtime"]   = date("Y-m-d H:i:s" ,EZDate::getLocateTime($info["reservation_endtime"], $time_zone, N2MY_SERVER_TIMEZONE));;
                $subject .= " [" . $info['reservation_name'] . "]";

                $mail = new EZSmtp(null, $lang_cd, "UTF-8");
                $mail->setSubject($subject);

                // From の表示を差出人とする
                if ($info ["sender_mail"]) {
                    $mail->setFrom ( $info ["sender_mail"], $info ["sender"] );
                } else {
                    $mail->setFrom ( RESERVATION_FROM, $info ["sender"] );
                }

                $mail->setReturnPath($info["sender_mail"]);
                $mail->setTo($guest["email"], $guest["name"]);
                // 本文
                $info["reservation_address"] = sprintf( "%sr/%s&c=%s", N2MY_BASE_URL, $guest["r_user_session"], $guest["lang"] );
                $template->assign("base_url" , N2MY_BASE_URL);
                $template->assign("info"     , $info);
                $template->assign("guest"    , $mail_list[$key]);

                if ($user_info["account_model"] == "free") {
                    //VCUBE IDから代理店IDを取得
                    $wsdl = $this->config->get('VCUBEID','wsdl');
                    $serverName = $_SERVER["SERVER_NAME"];
                    if($serverName == $this->config->get('VCUBEID','meeting_server_name')){
                      //meetingFree
                      $consumerKey = $this->config->get('VCUBEID','meeting_free_consumer_key');
                    }elseif($serverName == $this->config->get('VCUBEID','paperless_server_name')){
                      //paperLess
                      $consumerKey = $this->config->get('VCUBEID','paperless_free_consumer_key');
                    }
                    try {
                        require_once("classes/dbi/member_room_relation.dbi.php");
                        $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
                        $where = "room_key = '".$info["room_key"]."'";
                        $relation_info = $objRoomRelation->getRow($where);
                        require_once("classes/dbi/member.dbi.php");
                        $objMember = new MemberTable($this->get_dsn());
                        $where = "member_key = ".$relation_info["member_key"];
                        $member_info = $objMember->getRow($where, "member_id");
                        if ($member_info) {
                            $soap = new SoapClient($wsdl,array('trace' => 1));
                            $response = $soap->getAgencyId($consumerKey, $member_info["member_id"]);
                            $agency_id = $response["agencyId"];
                        }
                    } catch (Exception $e) {
                        $this->logger2->warn($e->getMessage());
                    }
                    if ($agency_id) {
                        $custom = 'custom/vcubeid/';
                        $custom_template_file = $custom.$lang_cd."/mail/".$agency_id."/reservation/user/reservation_" . $mode . ".t.txt";
                    }
                    $this->logger->debug($template->template_dir.$custom_template_file);
                    if ($custom_template_file && file_exists($template->template_dir.$custom_template_file)) {
                        $template_file = $custom_template_file;
                    } else {
                        $template_file = $lang_cd."/mail/reservation/user/reservation_" . $mode . ".t.txt";
                    }
                } else if ($custom) {
                    $custom_dir = 'custom/'.$custom.'/';
                    $custom_template_file = $custom_dir.$lang_cd."/mail/reservation/user/reservation_" . $mode . ".t.txt";
                    $this->logger2->debug($custom_template_file);
                    if (file_exists($template->template_dir.$custom_template_file)) {
                        $template_file = $custom_template_file;
                    } else {
                        $template_file = $lang_cd."/mail/reservation/user/reservation_" . $mode . ".t.txt";
                    }
                } else {
                    $template_file = $lang_cd."/mail/reservation/user/reservation_" . $mode . ".t.txt";
                }
                //html化
                if($info["mail_type"] == "html") {
                    $mail->setTextBody($template->fetch($template_file));
                    if($info["custom"] == "sales") {
                        $custom = 'custom/'.$info["custom"].'/';
                        $template_file = $custom."common/mail/reservation/user/reservation_".$mode.".t.html";
                    } else {
                        $template_file = "common/mail/reservation/user/reservation_".$mode.".t.html";
                    }
                    $frame["lang"] = $lang_cd;
                    $template->assign("__frame",$frame);
                    $mail->setHtmlBody($template->fetch($template_file));
                    $this->logger->debug(array($info["sender_mail"], $guest["email"], $subject, $body, $template_file));
                    $mail->setBody($body);
                    $mail->sendHtml();
                } else {
                    $body = $template->fetch($template_file);
                    $this->logger2->debug(array($guest["email"], $body));
                    $mail->setBody($body);
                    $mail->send();
                }
            }
        }
        $subject = $this->getOwnerMailSubject($mode, $this->_lang, $custom);
        $subject = $subject . " [" . $info['reservation_name'] . "]";
        $mail    = new EZSmtp(null, $this->_lang, "UTF-8");
        $mail->setSubject($subject);
        $mail->setFrom(RESERVATION_FROM);
        $mail->setReturnPath(NOREPLY_ADDRESS);
        $mail->setTo($info["sender_mail"], $info["sender"]);
        // 本文
        // 時差変換
        $template->assign("info"     , $info);
        $template->assign("guests"   , $mail_list);
        $template->assign("base_url" , N2MY_BASE_URL);
        if ($custom) {
            $custom_dir = 'custom/'.$custom.'/';
            $custom_template_file = $custom_dir.$this->_lang."/mail/reservation_" . $mode . ".t.txt";
            $this->logger2->debug($custom_template_file);
            if (file_exists($template->template_dir.$custom_template_file)) {
                $template_file = $custom_template_file;
            } else {
                $template_file = $this->_lang."/mail/reservation_" . $mode . ".t.txt";
            }
        } else {
            $template_file = $this->_lang."/mail/reservation_" . $mode . ".t.txt";
        }
        //html化
        if($info["mail_type"] == "html") {
            $mail->setTextBody($template->fetch($template_file));
            if($info["custom"] == "sales") {
                $custom = 'custom/'.$info["custom"].'/';
                $template_file = $custom."common/mail/reservation_".$mode.".t.html";
            } else {
                $template_file = "common/mail/reservation_".$mode.".t.html";
            }
            $frame["lang"] = $this->_lang;
            $template->assign("__frame",$frame);
            $body = $template->fetch($template_file);
            $mail->setHtmlBody($template->fetch($template_file));
            $this->logger->debug(array($info["sender_mail"], $guest["email"], $subject, $body, $template_file));
            $mail->setBody($body);
            $mail->sendHtml();
        } else {
            $body = $template->fetch($template_file);
            $this->logger2->debug(array($info["sender_mail"], $body));
            $mail->setBody($body);
            $mail->send();
        }
    }

    function getMessage($lang, $group, $id, $custom = "") {
        static $message;
        $config = EZConfig::getInstance();

        if(!isset($message[$lang])) {
            $config_file = N2MY_APP_DIR."config/lang/".$lang."/message.ini";
            $message[$lang] = parse_ini_file($config_file, true);
            if(!empty($custom)) {
                $custom_config_file = N2MY_APP_DIR . "config/custom/$custom/" . $lang . "/message.ini";

                if(file_exists($custom_config_file)) {
                    $custom_msg_list = parse_ini_file($custom_config_file, true);

                    foreach($custom_msg_list as $grp => $vals) {
                        if(isset($message[$lang][$grp]) && is_array($message[$lang][$grp])) {
                            $message[$lang][$grp] = array_merge($message[$lang][$grp], $vals);
                        }
                        else {
                            $message[$lang][$grp] = $vals;
                        }
                    }
                }
            }
        }
        if (!$id) {
            return $message[$lang][$group];
        }

        return $message[$lang][$group][$id];
    }

    function action_delete_list() {
        $reservation_session_ids = $this->request->get("reservation_session_id");
        foreach($reservation_session_ids as $reservation_session_id) {
            $this->obj_N2MY_Reservation->cancel($reservation_session_id);
        }
        $this->render_list();
    }

    /**
     * 予約削除確認画面
     */
    function action_delete_confirm()
    {
      // 二重登録回避
        $this->set_submit_key($this->_name_space);
        $member_info = $this->session->get( "member_info" );
        $user_info   = $this->session->get( "user_info" );
        $reservation_session = $this->request->get('reservation_session');
        $reservation_data    = $this->obj_N2MY_Reservation->getDetail($reservation_session);

        if(!$reservation_data || $reservation_data["info"]["user_key"] != $user_info["user_key"]){
            header("Location: /services/reservation/");
        }

        if (!$this->isAuthorized($reservation_data, '?action_delete_confirm=&reservation_session='.$reservation_session)) {
            return false;
        }
        // 部屋のオプション確認
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $reservation_data["info"]["room_option"] = $obj_N2MY_Account->getRoomOptionList($reservation_data["info"]["room_key"]);
        require_once ("classes/dbi/room.dbi.php");
        $room_obj  = new RoomTable($this->get_dsn());
        $room_info = $room_obj->getRow("room_key = '".$reservation_data["info"]["room_key"]."'", "cabinet,disable_rec_flg");
        $reservation_data["info"]["room_option"]["cabinet"] = $room_info["cabinet"];
        $reservation_data["info"]["room_option"]["disable_rec_flg"] = $room_info["disable_rec_flg"];
        $this->setReservationData($reservation_data,"delete");


        if (($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") &&
            $member_info["room_key"] != $reservation_data["info"]["room_key"] ){
            $reservation_data["info"]["status"] = "end";
        }

        $service_option  = new OrderedServiceOptionTable($this->get_dsn());
        $enable_teleconf = $service_option->enableOnRoom(23, $reservation_data["info"]["room_key"]); // 23 = teleconf
        $is_trial        = $user_info['user_status'] == 2;

        if (!$this->config->get("IGNORE_MENU", "teleconference") && !$is_trial && $enable_teleconf){
            $reservation_data['info'] = array_merge($reservation_data['info'],
                                                    $this->getPGiDetail($reservation_data['info']['meeting_key']));
        }

        try {
            $reservation_info['info']['clips']  = $this->findClipKeysByMeetingTicket($reservation_data['info']['meeting_key']);
            $this->template->assign('clip_list' , $this->getClipValueByKeys($reservation_info['info']['clips'], $user_info['user_key']));
        } catch (Exception $e) {
            $this->logger2->error($e->getMessage());
            return $this->action_list();
        }
        $this->setReservationData($reservation_data,"delete");
        require_once("classes/dbi/meeting.dbi.php");
        $objMeeting = new MeetingTable($this->get_dsn());
        $pin_cd     = $objMeeting->getOne("meeting_ticket = '".addslashes($reservation_data['info']['meeting_key'])."'", "pin_cd");
        $this->template->assign("pin_cd" , $pin_cd);
        $this->template->assign(array('enable_teleconf'   => $enable_teleconf,
                                      'service_name'      => $user_info['service_name'],
                                      'is_trial'          => $is_trial,
                                      'is_detail'         => true,
                                      'main_phone_number' => @$main_phone_number,
                                      'reservation_data'  => $reservation_data));
        $this->display('user/reservation/delete_confirm.t.html');
    }

    /**
     * 予約削除完了画面
     */
    function action_delete_complete() {
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_top();
        }
        $reservation_data = $this->getReservationData("delete");
        $this->logger2->debug($reservation_data);
        $reservation_session = $reservation_data["info"]["reservation_session"];
        // 予約中止の場合はDBから招待者を取得する
//        $reservation_data["guests"] = $this->obj_N2MY_Reservation->getGuestsList($reservation_data["info"]["reservation_key"]);
        // キャンセル
        $this->obj_N2MY_Reservation->cancel($reservation_session);
        // メール送信
        if ($reservation_data["guests"]||($reservation_data["staff"] && $reservation_data["guest"])) {
            if(empty($reservation_data["guests"])) {
                $reservation_data["info"]["custom"] = "sales";
                $guests[] = $reservation_data["staff"];
                $guests[] = $reservation_data["guest"];
            } else {
                $guests = $reservation_data["guests"];
            }
            // 主催者も招待者として追加
            if($reservation_data["info"]["is_organizer_flg"] == 1 && $reservation_data["info"]["sender_mail"] != $reservation_data["organizer"]["email"]){
                 $guests[] = $reservation_data["organizer"];
            }
            $user_info = $this->session->get("user_info");
            $reservation_data["info"]['guest_url_format'] = $user_info['guest_url_format'];
            $this->sendMail($guests, $reservation_data["info"], "delete");
        }
        // 入力データをクリア
        $this->clearReservationData("delete");
        // 登録完了（一覧表示）
        $this->action_top($reservation_data["info"]["room_key"]);
    }

    /**
     * パスワード確認処理
     */
    function action_pw_check()
    {
        $this->logger->trace(__FUNCTION__."#start",__FILE__,__LINE__);
        $this->_reservation->checkLogin();
        $data = $this->_reservation->checkPw();
        if (($data['reservation_pw'] == "") ||
            ($data['reservation_pw'] === $this->request->get('reservation_pw'))) {
            // 名前空間取得
            $name_space = $this->request->get("ns");
            // リダイレクト先取得
            $redirect_url = $this->session->get("redirect_url", $name_space);
            // セッションから消去
            $this->session->remove("redirect_url", $name_space);
            header("Location: ".$redirect_url);
            return;
        }
        return $this->render_login();
    }

    /**
     * ログイン処理
     */
    function action_login()
    {
        $this->logger->trace(__FUNCTION__."#start",__FILE__,__LINE__);
        $this->_reservation->checkLogin();
        $data = $this->_reservation->checkPw();

        // パスワード存在確認
        if (!$data['reservation_pw']) {
            $name_space   = $this->request->get("ns"); // 名前空間取得
            $redirect_url = $this->session->get("redirect_url", $name_space); // リダイレクト先取得
            $this->session->remove("redirect_url", $name_space); // セッションから消去
            header("Location: ".$redirect_url);
            return;
        }

        return $this->render_login();
    }

    /**
     * ログイン画面表示
     */
    function render_login()
    {
        $this->logger->trace(__FUNCTION__."#start",__FILE__,__LINE__);
        $this->template->assign('ns'             , $this->request->get("ns"));
        $this->template->assign('reservation_key', $this->request->get("reservation_key"));
        $this->display('user/reservation/login.t.html');
    }

    /**
     * デフォルトページ
     */
    function default_view()
    {
        $this->action_top();
    }

    /**
     * カレンダーのデータ作成
     */
    function _calender($year, $month, $limit = 1)
    {
        $EZDate     = new EZDate();
        $_now       = $EZDate->getLocateTime(time(), $this->session->get("time_zone"), N2MY_SERVER_TIMEZONE);
        $now        = mktime(0,0,0,date("m",$_now),date("d",$_now),date("Y",$_now));
        $limit_date = mktime(0,0,0,date("m",$_now) + $limit, date("d",$_now), date("Y",$_now));
        // 初日の曜日
        $fast_week  = date("w", mktime(0,0,0,$month,1, $year));
        $last_day   = date("t", mktime(0,0,0,$month,1, $year));
        $week = array();

        for ($i = 0, $j = 0, $w = 0, $flg = 0, $num = 1; $i < 42; $i++) {
            // 開始
            if ($fast_week == $i) {
                $flg = 1;
            } elseif ($last_day + 1 == $num) {
                $flg = 2;
                $num = 1;
            }
            if (($i > 0) && ($i % 7) == 0) {
                $j ++;
                $w = 0;
            }
            switch ($flg) {
            // 当月分
            case 1:
                $_day = mktime(0,0,0,$month,$num,$year);
                // 無効
                if (($now > $_day) || ($_day > $limit_date)) {
                    $week[$j][$w]["type"] = "1";
                // 有効
                } else {
                    $week[$j][$w]["type"] = "2";
                }
                $week[$j][$w]["num"] = $num;
                $num++;
                break;
            // 次月分
            case 2:
                $week[$j][$w]["type"] = "3";
                $week[$j][$w]["num"] = $num;
                $num++;
                break;
            default :
                $week[$j][$w]["type"] = "0";
                $week[$j][$w]["num"] = "";
            }
            $w++;
        }
        return $week;
    }

    /**
     * スケジュール一覧取得
     */
    function _render_schedule_list($room_key, $start_datetime, $end_datetime)
    {
        $target_date = $start_datetime;
        if ($end_datetime) {
            $next_date = $end_datetime;
        } else {
            $next_date = $target_date + (3600 * 24);
        }
        $_start_date = date("Y-m-d 00:00:00", $target_date);
        $_end_date = date("Y-m-d 00:00:00", $next_date);
        $where = "room_key = '$room_key'" .
                " AND reservation_status = 1" .
                " AND (" .
                "( reservation_starttime between '$_start_date' AND '$_end_date' )" .
                " OR ( reservation_endtime between '$_start_date' AND '$_end_date' )" .
                " OR ( '$_start_date' BETWEEN reservation_starttime AND reservation_endtime )" .
                " OR ( '$_end_date' BETWEEN reservation_starttime AND reservation_endtime ) )";
        $list = $this->_reservation_obj->getList($where, array("reservation_starttime" => "ask"));
        $this->template->assign("start_datetime", $start_datetime);
        $this->template->assign("end_datetime", $end_datetime - (3600 * 24));
        $this->template->assign("list", $list);
        $content = $this->fetch("user/reservation/schedule.t.html");
        return $content;
    }

    /**
     * スケジュール一覧の表示
     */
    function render_list()
    {
        $date_obj = new EZDate();
        // 日付計算
        $_target = $this->request->get("time", time());
        $target  = $date_obj->getLocateTime($_target, $this->session->get('time_zone'), N2MY_SERVER_TIMEZONE);
        $target  = mktime(0,0,0,date("m", $target),date("d", $target), date("Y", $target));
        $weekend = $target + (3600 * 24 * 6); // 次週

        $this->template->assign('start'    , date('Y/m/d', $target));
        $this->template->assign('end'      , date('Y/m/d', $weekend));
        $this->template->assign('weekend'  , $weekend);
        $this->template->assign('prev'     , $_target - (3600 * 24 * 7));
        $this->template->assign('yesterday', $_target - (3600 * 24 * 1));
        $this->template->assign('now'      , mktime(0,0,0,date("m"),date("d"),date("Y")));
        $this->template->assign('tomorrow' , $_target + (3600 * 24 * 1));
        $this->template->assign('next'     , $_target + (3600 * 24 * 7));

        $table = null;
        // １週間分のスケジュール
        $schedule = $target;
        for ($w = 0; $w < 7; $w++) {
            $week[] = array(
                "date"      => $schedule,
                "weekday"   => date("w", $schedule)
            );
            $schedule = $schedule + (3600 * 24);
        }
        $this->template->assign('week', $week);
        $user_info = $this->session->get('user_info');// 利用可能な会議室一覧
        $room_list = $this->session->get("room_info"); // 部屋一覧取得
        $target    = $date_obj->getLocateTime($target, N2MY_SERVER_TIMEZONE ,$this->session->get('time_zone'));
        foreach ($room_list as $room_key => $room_info) {
            // 会議室一覧取得
            $schedule = $target;
            // 一覧取得
            $next_day = $schedule + (3600 * 24 * 7);
            $options = array(
                "start_time" => date("Y-m-d H:i:s", $schedule),
                "end_time"   => date("Y-m-d H:i:s", $next_day),
                "sort_key"   => "reservation_starttime",
                "sort_type"  => "asc",
                );
            $rows = $this->obj_N2MY_Reservation->getList($room_key, $options, $this->session->get('time_zone'));
            $room = array();
            foreach ($rows as $_key => $row) {
                $room[] = array(
                    "session"   => $row["reservation_session"],
                    "from"      => $row["reservation_starttime"],
                    "to"        => $row["reservation_endtime"],
                    "name"      => $row["reservation_name"],
                );
            }
            $rooms[$room_key] = $room;
        }
        $this->template->assign('rooms', $rooms);
        $this->display("user/reservation/weekly.t.html");
    }

    private function render_participantedList()
    {
        $date_obj = new EZDate();
        // 日付計算
        $_target = $this->request->get("time", time());
        $target  = $date_obj->getLocateTime($_target, $this->session->get('time_zone'), N2MY_SERVER_TIMEZONE);
        $target  = mktime(0,0,0,date("m", $target),date("d", $target), date("Y", $target));
        $weekend = $target + (3600 * 24 * 6); // 次週
        $this->template->assign('start'    , date('Y/m/d', $target));
        $this->template->assign('end'      , date('Y/m/d', $weekend));
        $this->template->assign('weekend'  , $weekend);
        $this->template->assign('prev'     , $_target - (3600 * 24 * 7));
        $this->template->assign('yesterday', $_target - (3600 * 24 * 1));
        $this->template->assign('now'      , mktime(0,0,0,date("m"),date("d"),date("Y")));
        $this->template->assign('tomorrow' , $_target + (3600 * 24 * 1));
        $this->template->assign('next'     , $_target + (3600 * 24 * 7));
        $table = null;
        // １週間分のスケジュール
        $schedule = $target;
        for ($w = 0; $w < 7; $w++) {
            $week[] = array(
                "date"      => $schedule,
                "weekday"   => date("w", $schedule)
            );
            $schedule = $schedule + (3600 * 24);
        }
        $this->template->assign('week', $week);
        // 利用可能な会議室一覧
        $user_info   = $this->session->get('user_info');
        $member_info = $this->session->get('member_info');
        $room_info   =  $this->session->get("room_info");

        $target = $date_obj->getLocateTime($target, N2MY_SERVER_TIMEZONE ,$this->session->get('time_zone'));
        $room["name"] = $room_info[$member_info["room_key"]]["room_info"]["room_name"];

        $schedule = $target;
        // 一覧取得
        $next_day = $schedule + (3600 * 24 * 7);
        $options = array(
            "start_time" => date("Y-m-d H:i:s", $schedule),
            "end_time"   => date("Y-m-d H:i:s", $next_day),
            "sort_key"   => "reservation_starttime",
            "sort_type"  => "asc",
            );
        $rows = $this->obj_N2MY_Reservation->getParticipantedMeetingList( $member_info, $options, $this->session->get('time_zone'), $room_info);
        $day = array();
        if ($rows) {
            $room = array();
            foreach ($rows as $_key => $row) {
                $room[] = array(
                    "session"   => $row["reservation_session"],
                    "from"      => $row["reservation_starttime"],
                    "to"        => $row["reservation_endtime"],
                    "name"      => $row["reservation_name"],
                );
            }
            $rooms[$member_info["room_key"]] = $room;
            $this->logger->trace("reservation", __FILE__, __LINE__, $row);
        }

        $this->template->assign('rooms', $rooms);
        $this->display("user/reservation/weekly.t.html");
    }

    function action_monthly_list() {
      $date_obj = new EZDate();
      $room_key = $this->request->get("room_key");
      if($room_key){
        $this->session->set("monthly_room_key", $room_key);
      } else {
        if($this->session->get("monthly_room_key") && !$this->request->get("reset")){
          //get session room_key
          $room_key = $this->session->get("monthly_room_key");
        } else {
          //get default room_key
          $room_list =  $this->session->get("room_info");
          $room_key = key($room_list);
          $this->session->set("monthly_room_key", $room_key);
        }
      }
      $time = $this->request->get("time", time());
      $_firstday = date('Y-m-01 00:00:00', $time);
      $_firstday_stamp = strtotime($_firstday);
    // Last day of the month.
    $_lastday = date('Y-m-t 00:00:00', $time);
    $_lastday_stamp = strtotime($_lastday);
    $firstday_stamp = $_firstday_stamp - (3600 * 24) * date("w",$_firstday_stamp);
    $lastday_stamp = $_lastday_stamp + (3600 * 24) * (6 - date("w",$_lastday_stamp));
    //$this->logger2->info(date("Y-m-d H:i:s", $firstday_stamp), $firstday_stamp);
    //$this->logger2->info(date("Y-m-d H:i:s", $lastday_stamp), $lastday_stamp);

    for ($i=$firstday_stamp; $i<=$lastday_stamp; ){
      $month[] = array(
          "date" => $i);
      $i += (3600*24);
    }
    //$this->logger2->info($month);

    $options = array(
        "start_time" => date("Y-m-d H:i:s", $firstday_stamp),
        "end_time"   => date("Y-m-d H:i:s", $lastday_stamp + (3600 * 24)),
        "sort_key"   => "reservation_starttime",
        "sort_type"  => "asc",
    );
    $rows = $this->obj_N2MY_Reservation->getList($room_key, $options, $this->session->get('time_zone'));
    $this->logger2->info($rows);
    $reservation = array();
    foreach ($rows as $_key => $row) {
      $reservation[] = array(
          "session"   => $row["reservation_session"],
          "from"      => $row["reservation_starttime"],
          "to"        => $row["reservation_endtime"],
          "name"      => $row["reservation_name"],
      );
    }
    $this->logger2->info($reservation);
    $this->template->assign('month', $month);
    $this->template->assign('reservations', $reservation);
    $this->template->assign('room_key', $room_key);
    $this->template->assign('now_month', time());
    $this->template->assign('current_month', $time);
    $this->template->assign('last_month', $firstday_stamp - (3600 * 24));
    $this->template->assign('next_month', $lastday_stamp + (3600 * 24));

    $this->display("user/reservation/monthly.t.html");
    }

    /**
     * 予約入力チェック
     */
    function reservation_check($room_key, $starttime, $endtime, $reservation_key = null)
    {
        $ret = $this->_reservation_obj->getListBetween($room_key, $starttime, $endtime, $reservation_key);
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $ret);
        $message = "";
        if (count($ret) > 0) {
            $message = '<li>'.RESERVATION_ERROR_TIME;
            foreach ($ret as $_key => $row) {
                $message .= "<br/>&nbsp;&nbsp;&nbsp;[ " . htmlspecialchars($row['reservation_name']) . " ] " .
                    date("Y/m/d H:i", EZDate::getLocateTime($row['reservation_starttime'], N2MY_USER_TIMEZONE, N2MY_SERVER_TIMEZONE)) . " - " .
                    date("Y/m/d H:i", EZDate::getLocateTime($row['reservation_endtime'], N2MY_USER_TIMEZONE, N2MY_SERVER_TIMEZONE));
            }
            $message .= '</li>';
        }
        return $message;
    }

    /**
     * レイアウトタイプ
     */
    function get_layout_type($room_key) {
        require_once ("classes/dbi/room.dbi.php");
        $room_obj = new RoomTable($this->get_dsn());
        $now_row = $room_obj->get_detail($room_key);
        if ($now_row["room_layout_type"]) {
            return $now_row["room_layout_type"];
        }

        return "1";
    }

    /**
     * 入力フォーマットを整形
     */
    private function formatReservationData($reservation_data,$mode) {
        if ($reservation_data["info"]) {
            $request = $reservation_data["info"];
            // 開始日
            if ($request['startDate']) {
                $starttime = $request['startDate'] . " ".
                    $request['startHour'] . ":".
                    $request['startMin'] . ":00";
                // 終了日
                $endtime = $request['endDate'] . " ".
                    $request['endHour'] . ":".
                    $request['endMin'] . ":00";
                $reservation_data["info"]["reservation_starttime"] = $starttime;
                $reservation_data["info"]["reservation_endtime"] = $endtime;
                if(!is_numeric($request["reservation_place"])){
                    $reservation_data["info"]["reservation_place"] = 9;
                }
            }
            $reservation_data["info"]["send_flg"] = isset($request["send_flg"]) ? 1 : 0;
        }
        $data = $this->getReservationData($mode);
        // パスワード無し
        if ($reservation_data["pw_flg"] == 0) {
            $reservation_data["info"]["reservation_pw"] = "";
            $reservation_data["info"]['reservation_pw_confirm'] = "";
        // パスワード保持
        } elseif (isset($data["info"]["reservation_key"]) && $reservation_data["change_flg"] == 2) {
            $reservation_data["info"]["reservation_pw"]         = $data["info"]["old_reservation_pw"];
            $reservation_data["info"]["reservation_pw_confirm"] = $data["info"]["old_reservation_pw"];
        }
        if(!$reservation_data["info"]["mail_type"]){
            $reservation_data["info"]["mail_type"] = null;
        }
        if(!$reservation_data["info"]["is_reminder_flg"]){
            $reservation_data["info"]["is_reminder_flg"] = 0;
        }
        require_once("lib/EZLib/EZUtil/EZArray.class.php");
        $objArray = new EZArray();
        $reservation_data["info"] = $objArray->mergeRecursiveReplace($data["info"], $reservation_data["info"]);
        // マルチカメラの利用制限
        if ($reservation_data["info"]["is_multicamera"]) {
            $reservation_data["info"]["is_telephone"] = 0;
            $reservation_data["info"]["is_h323"] = 0;
            $reservation_data["info"]["is_mobile_phone"] = 0;
        }
        // ゲスト
        $guests = array();
        if (isset($reservation_data["guests"])) {
            require_once ("classes/dbi/address_book.dbi.php");
            require_once('lib/EZLib/EZUtil/EZLanguage.class.php');
            $objAddressBook = new AddressBookTable($this->get_dsn());
            $user_info = $this->session->get('user_info');
            $member_info = $this->session->get('member_info');
            // 件数
            $count = count($reservation_data["guests"]["name"]);
            for($i = 0; $i < $count; $i++) {
                if ($reservation_data["guests"]["email"][$i]) {
                    if(!is_numeric($reservation_data["guests"]["timezone"][$i])){
                        $reservation_data["guests"]["timezone"][$i] = 100;
                    }

                    if(!array_key_exists($reservation_data["guests"]["lang"][$i], $this->get_language_list())) {
                        $lang_cd = $this->get_language();
                        $guest_lang = EZLanguage::getLang($lang_cd);
                        $this->logger2->warn(
                            array(
                                'user_key'                => (isset($user_info['user_key']) ? $user_info['user_key'] : ''),
                                'requested guest lang'    => $reservation_data['guests']['lang'][$i],
                                'user-side lang_allow'    => (isset($user_info['lang_allow']) ? $user_info['lang_allow'] : ''),
                                'language list'           => array_keys($this->get_language_list()),
                                'lang_cd'                 => $lang_cd,
                            ),
                            "No appropriate language found. Trying to use '$guest_lang' for guest invitation."
                        );
                    }
                    else {
                        $guest_lang = $reservation_data["guests"]["lang"][$i];
                    }

                    $has_address_book = $objAddressBook->recordCheck($reservation_data["guests"]["email"][$i], $user_info["user_key"],$member_info["member_key"]);
                    if($mode == "modify" && $member_info["member_key"] && $member_info["member_key"] != $data["info"]["member_key"]) {
                        $has_address_book = 1;
                    }
                    if(!$has_address_book){
                        $reservation_data["disp_address_info"] = 1;
                    }

                    $guests[] = array(
                        "r_user_session"  => $reservation_data["guests"]["r_user_session"][$i],
                        "name"            => $reservation_data["guests"]["name"][$i],
                        "email"           => $reservation_data["guests"]["email"][$i],
                        "type"            => $reservation_data["guests"]["type"][$i],
                        "timezone"        => $reservation_data["guests"]["timezone"][$i],
                        "lang"            => $guest_lang,
                        "r_organizer_flg" => 0,
                        "has_address_book" => $has_address_book,
                    );
                }
            }
            if($reservation_data["info"]["is_organizer_flg"] == 1){
                $organizer = array(
                        "r_user_session"  => $reservation_data["organizer"]["r_user_session"],
                        "name"            => $reservation_data["organizer"]["name"],
                        "email"           => $reservation_data["organizer"]["email"],
                        "type"            => $reservation_data["organizer"]["type"],
                        "timezone"        => $reservation_data["organizer"]["timezone"],
                        "lang"            => $reservation_data["organizer"]["lang"],
                        "r_organizer_flg" => 1,
                );
            }else if($reservation_data["info"]["is_organizer_flg"] == 2){
                $organizer = array(
                        "r_user_session"  => $reservation_data["organizer"]["r_user_session"],
                        "name"            => $reservation_data["info"]["sender"],
                        "email"           => $reservation_data["info"]["sender_mail"],
                        "type"            => $reservation_data["organizer"]["type"],
                        "timezone"        => 100,
                        "lang"            =>  $this->request->getCookie("lang"),
                        "r_organizer_flg" => 1,
                        );
            }
            $reservation_data["organizer"] = $organizer;
        }

        if($this->session->get("service_mode") == "sales") {
            $user_info = $this->session->get('user_info');
            $member_info = $this->session->get('member_info');
            $objAddressBook = new AddressBookTable($this->get_dsn());
            $has_address_book = $objAddressBook->recordCheck($reservation_data["guest"]["email"], $user_info["user_key"],$member_info["member_key"]);
            if($mode == "modify" && $member_info["member_key"] && $member_info["member_key"] != $data["info"]["member_key"]) {
                $has_address_book = 1;
            }
            if(!$has_address_book){
                $reservation_data["disp_address_info"] = 1;
            }
            $reservation_data["guest"]["has_address_book"] = $has_address_book;
            // セールスは参加者に主催者としない
            $reservation_data["guest"]["r_organizer_flg"] = 0;
            $reservation_data["staff"]["r_organizer_flg"] = 0;

        }

        if(!$reservation_data["guest_flg"]){
            $reservation_data["info"]["is_reminder_flg"] = 0;
        }
        $reservation_data["guests"] = $guests;
        // ファイル一覧
        $filename         = ($reservation_data["filename"]) ? $reservation_data["filename"] : array();
        $sortlist         = ($reservation_data["sort"])     ? $reservation_data["sort"]     : array();
        $doc_format       = ($reservation_data["format"])   ? $reservation_data["format"]   : array();
        $doc_version      = ($reservation_data["version"])  ? $reservation_data["version"]  : array();
        $_documents       = isset($data["documents"]) ? $data["documents"] : null;
        $documents        = array();
        $delete_documents = array();
        $session_delete_documents = $this->session->get("delete_documents");
        if (isset($session_delete_documents)) {
            foreach ($session_delete_documents as $document_id => $document) {
                if (!$reservation_data["upload_flg"]) {
                    $documents[$document_id] = $session_delete_documents[$document_id];
                    $documents[$document_id]["not_delete"] = "true";
                } else {
                    $delete_documents["$document_id"] = $session_delete_documents[$document_id];
                }
            }
        }
        if (isset($_documents)) {
            foreach($_documents as $document_id => $document) {
                if (array_key_exists($document_id, $filename)) {
                    if ($reservation_data["upload_flg"] || !$_documents[$document_id]["tmp_name"] ||
                        !$reservation_data["upload_flg"] && $_documents[$document_id]["tmp_name"]) {
                        $documents[$document_id] = $_documents[$document_id];
                        $documents[$document_id]["name"] = $filename[$document_id];
                        $documents[$document_id]["sort"] = $sortlist[$document_id];
                        $documents[$document_id]["upload_status"] = true;
                    }
                } else {
                    // アップロード済み資料
                    if (isset($_documents[$document_id]["document_id"])) {
                        // 削除予定の資料
                        if ($reservation_data["upload_flg"]) {
                            $documents[$document_id] = $_documents[$document_id];
                            $documents[$document_id]["delete"] = true;
                            $delete_documents[$document_id] = $_documents[$document_id];
                        } else {
                            $documents[$document_id] = $_documents[$document_id];
                            $documents[$document_id]["sort"] = $sortlist[$document_id];
                            $documents[$document_id]["not_delete"] = true;
                        }
                    // フォームの資料
                    } elseif (file_exists($_documents[$document_id]["tmp_name"])) {
                        unlink($_documents[$document_id]["tmp_name"]);
                    }
                }
            }
        }
        $room_info = $this->get_room_info($data["info"]["room_key"]);
        // 対応フォーマット情報取得
        require_once ("classes/N2MY_Document.class.php");
        $objDocument = new N2MY_Document($this->get_dsn());
        $support_document_list = $objDocument->getSupportFormat($data["info"]["room_key"]);
        if(isset($_FILES["document"])) {
            $count = count($_FILES["document"]["name"]);
            for ($i = 0; $i < $count; $i++) {
                $fileinfo = pathinfo($_FILES["document"]["name"][$i]);
                $string = new EZString();
                $document_id = $string->create_id();
                $error = $_FILES["document"]["error"][$i];
                // 追加エラー処理
                if ($error === 0) {
                    // 拡張子判定
                    if (!in_array(strtolower($fileinfo["extension"]), $support_document_list['support_ext_list'])) {
                        $error = 1000;
                    // 空のファイル
                    } elseif ($_FILES["document"]["size"][$i] == 0) {
                        $error = 4;
                    // 容量制限
                    } else {
                        if (in_array(strtolower($fileinfo["extension"]), $support_document_list['image_ext_list'])) {
                            $maxsize = ($room_info["room_info"]["whiteboard_size"] <= 5) ? $room_info["room_info"]["whiteboard_size"] : 5;
                        } else {
                            $maxsize = isset($room_info["room_info"]["whiteboard_size"]) ? $room_info["room_info"]["whiteboard_size"] : 20;
                        }
                        if ($_FILES["document"]["size"][$i] > $maxsize * 1024 * 1024) {
                            $error = 4;
                        }
                    }
                }
                $tmp_name = "";
                if ($error === 0) {
                    $dir = $this->get_work_dir();
                    $tmp_name = $dir."/".$document_id.".".$fileinfo["extension"];
                    move_uploaded_file($_FILES["document"]["tmp_name"][$i], $tmp_name);
                    $name = ($filename[$i]) ? $filename[$i] : $_FILES["document"]["name"][$i];
                    $sort = $sortlist[$i];
                    if (("xbd" == $fileinfo["extension"] || "xdw" == $fileinfo["extension"]) && $fileinfo["extension"]) {
                        $convet_format = "vector";
                    } else {
                        $convet_format = ($doc_format[$i]) ? $doc_format[$i] : "bitmap";
                    }
                    $flash_version = ($doc_version[$i]) ? $doc_version[$i] : "";
                    $documents[$document_id] = array(
                        "name"        => $name,
                        "type"        => $_FILES["document"]["type"][$i],
                        "tmp_name"    => $tmp_name,
                        "error"       => $error,
                        "size"        => $_FILES["document"]["size"][$i],
                        "sort"        => $sort,
                        "format"      => $convet_format,
                        "version"     => $flash_version,
                    );
                }
            }
        }
        // 資料をソート
        $sort_arr = array();
        foreach($documents AS $uniqid => $row){
            foreach($row AS $key=>$value){
                $sort_arr[$key][$uniqid] = $value;
            }
        }
        if ($documents) {
            array_multisort($sort_arr["sort"], SORT_ASC, $documents);
        }

    $documents_dsp = $documents;
        foreach($delete_documents as $document_id => $document) {
          unset($documents_dsp[$document_id]);
        }

        $reservation_data["documents"] = $documents;
        $reservation_data["documents_dsp"] = $documents_dsp;
        $reservation_data["delete_documents"] = $delete_documents;

        //Clips
        $_clips = isset($data["clips"]) ? $data["clips"] : null;
        //$this->logger2->info($_clips);
        $clips = array();
        foreach($reservation_data["meeting_clip_name"] as $clip_id => $clip_name) {
          if($_clips[$clip_id]["is_storage"])
            $_clips[$clip_id]["name"] = $clip_name;
          else
            $_clips[$clip_id]["meeting_clip_name"] = $clip_name;
          $clips[] = $_clips[$clip_id];
        }
        //$this->logger2->info($clips);
        $reservation_data["clips"] = $clips;

        // ストレージ
        require_once ("classes/N2MY_Storage.class.php");
        $objStorage = new N2MY_Storage($this->get_dsn(), $this->session->get("server_info"));
        $storage_count = count($reservation_data["storage_file_id"]);
        for($i = 0; $i < $storage_count; $i++){
            $storage_info = $objStorage->getStorageInfo($reservation_data["storage_file_id"][$i]);
            $storage_info["name"] = $reservation_data["storage_file_name"][$i]?$reservation_data["storage_file_name"][$i]:$storage_info["user_file_name"];
            $storage_info["is_storage"] = 1;
            if($storage_info["category"] == "document" || $storage_info["category"] == "image"){
                $reservation_data["storage_documents"][] = $storage_info;
                $reservation_data["documents_dsp"][] = $storage_info;
            }else if ($storage_info["category"] == "video"){
                $reservation_data['clips'][] = $storage_info;
            }
            else{

            }
        }

        //電話連携
        if ($reservation_data["teleconf_flg"]) {
            $reservation_data["info"]["tc_type"] = "pgi";
        } else {
            $reservation_data["info"]["tc_type"] = "voip";
        }

        $reservation_data["up_file_count"] = count($reservation_data["documents"]) + count($reservation_data["storage_documents"]);
        $user_info = $this->session->get("user_info");
        if ($user_info['use_clip_share']) {
            //$reservation_data['clips'] = is_array($data['clips']) ? $data['clips'] : array();
        }
        if($this->session->get("service_mode") == "sales") {
            $reservation_data["info"]["reservation_custom"] = "sales";
        }else {
            $reservation_data["info"]["reservation_custom"] = $this->config->get('SMARTY_DIR','custom') != "sales"? $this->config->get('SMARTY_DIR','custom'):"";
        }
        $this->logger2->debug($reservation_data);
        return $reservation_data;
    }
    //{{{setReservationData
    private function setReservationData($reservation_data,$mode) {
      if($mode == "modify")
        $this->session->set("modify_reservation_data", $reservation_data);
      elseif($mode == "create")
          $this->session->set("create_reservation_data", $reservation_data);
      elseif($mode == "delete")
          $this->session->set("delete_reservation_data", $reservation_data);
    }
    //}}}
    //{{{getReservationData
    private function getReservationData($mode) {
      if($mode == "modify")
          return $this->session->get("modify_reservation_data");
      elseif($mode == "create")
          return $this->session->get("create_reservation_data");
      elseif($mode == "delete")
          return $this->session->get("delete_reservation_data");
    }
    //}}}
    //{{{ clearReservationData
    private function clearReservationData($mode) {
      if($mode == "modify")
        return $this->session->remove("modify_reservation_data");
      elseif($mode == "create")
          return $this->session->remove("create_reservation_data");
      elseif($mode == "delete")
          return $this->session->remove("delete_reservation_data");
    }
    //}}}
    /**
     * 入力チェック
     */
    function check($reservation_data, $method = "") {
        $reservation_info = $reservation_data["info"];
        $room_key         = $reservation_info["room_key"];
        if ($method != "delete") {
            // パスワードチェック
            $message = "";
            if ($reservation_data['pw_flg'] == 1 && $reservation_data['change_flg'] == 1) {
                if (!$reservation_info['reservation_pw'] && !$reservation_info['reservation_pw_confirm']) {
                    $this->logger2->info(array($reservation_info['reservation_pw'], $reservation_info['reservation_pw_confirm']));
                    $message .= '<li>'.RESERVATION_ERROR_PW . '</li>';
                } elseif ( 0 != strcmp( $reservation_info['reservation_pw'], $reservation_info['reservation_pw_confirm'] )) {
                    $message .= '<li>'.RESERVATION_ERROR_DISAGREE_PW . '</li>';
                } elseif (!ctype_alnum($reservation_info['reservation_pw'])) {
                    $message .= '<li>'.RESERVATION_ERROR_INVALIDURL_PW . '</li>';
                } elseif (strlen($reservation_info['reservation_pw']) < 6) {
                    $message .= '<li>'.RESERVATION_ERROR_SHORTLENGTHL_PW . '</li>';
                } elseif (strlen($reservation_info['reservation_pw']) > 16) {
                    $message .= '<li>'.RESERVATION_ERROR_LONGLENGTHL_PW . '</li>';
                }
            }
            //会議名チェック
            if (mb_strlen($reservation_info['reservation_name']) > 100) {
                $message .= '<li>'.RESERVATION_ERROR_NAME_LENGTH . '</li>';
            }

            // 開始、終了時間をチェック
            $limit_start_time = time() - (2 * 24 * 3600); // サーバーの現地時刻で２日前
            $limit_end_time   = time() + (367 * 24 * 3600); // サーバーの現地時刻で１年後の次の日
            $_starttime       = EZDate::getLocateTime($reservation_info['reservation_starttime'], N2MY_SERVER_TIMEZONE,
                                                      $reservation_info["reservation_place"]); // 比較対照をサーバーの地域にあわせる
            $_endtime         = EZDate::getLocateTime($reservation_info['reservation_endtime'], N2MY_SERVER_TIMEZONE,
                                                      $reservation_info["reservation_place"]); // 比較対照をサーバーの地域にあわせる
            if ($_starttime <= $limit_start_time) {
                $message .= '<li>'.RESERVATION_ERROR_STARTTIME_INVALID.'</li>';
            } elseif ($_endtime >= $limit_end_time) {
                $message .= '<li>'.RESERVATION_ERROR_ENDTIME_INVALID.'</li>';
            }
            //終了時間が現時刻より前だった場合のエラー処理
            elseif ($_endtime < time() ) {
                $message .= '<li>'.RESERVATION_ERROR_ENDTIME_INVALID.'</li>';
            } else {
                $message .= $this->obj_N2MY_Reservation->check($room_key, $reservation_data);
            }
            // [他社の電話会議サーヒビスを使用する]詳細
            if ($reservation_info['tc_type'] == 'etc') {
                if (!$reservation_info['tc_teleconf_note']) {
                    $message .= '<li>'.RESERVATION_ERROR_TC_ETC  . '</li>';
                } elseif (mb_strlen($reservation_info['tc_teleconf_note']) > 1000) {
                    $message .= '<li>'.RESERVATION_ERROR_TC_ETC_LENGTH  . '</li>';
                }
            }
            // メンテナンス時間をチェック
            if ($this->config->get("N2MY", "maintenance_notification")) {
                if (N2MY_MDB_DSN) {
                    require_once('classes/core/dbi/Notification.dbi.php');
                    $objNotification = new NotificationTable(N2MY_MDB_DSN);
                    $count = $objNotification->check(date("Y-m-d H:i:s", $_starttime), date("Y-m-d H:i:s", $_endtime));
                    if ($count > 0) {
                        $message .= '<li>'.RESERVATION_ERROR_MEINTENANCE_TIME.'</li>';
                    }
                }
            }
        }
        $room_info = $this->get_room_info($room_key);
        if(!$room_info){
            $message .= '<li>UNKNOWN_ERROR</li>';
        }
        if ($reservation_data["guest_flg"]) {
            // メール
            if($this->session->get("service_mode") == "sales") {
              if (mb_strlen($guest['name']) > 50) {
                $message .= '<li>'.RESERVATION_ERROR_STAFF_NAME_LENGTH . '</li>';
              }
            } else {
              // 人数制限
              $normal_num = 0;
              $audience_num = 0;
              foreach ($reservation_data['guests'] as $guest) {
                if ($guest["email"]) {
                  if ($guest["type"] == 1) {
                    $audience_num++;
                  } else {
                    $normal_num++;
                  }
                }
              }
              if (!$reservation_info["reservation_key"] && ( $normal_num + $audience_num ) == 0) {
                $message .= '<li>'.INVITATION_ERROR_NOUSER.'</li>';
              }
              if ($room_info["room_info"]["max_seat"] < $normal_num) {
                $this->logger2->debug(array($room_info["room_info"], $normal_num, $audience_num));
                $message .= '<li>'.sprintf(RESERVATION_ERROR_INVITE_NORMAL, $room_info["room_info"]["max_seat"]) . '</li>';
              }
              if ($room_info["room_info"]["max_audience_seat"] < $audience_num) {
                $this->logger2->debug(array($room_info["room_info"], $normal_num, $audience_num));
                $message .= '<li>'.sprintf(RESERVATION_ERROR_INVITE_AUDIENCE, $room_info["room_info"]["max_audience_seat"]). '</li>';
              }
              foreach ($reservation_data["guests"] as $_key => $guest) {
                if (mb_strlen($guest['name']) > 50) {
                  $message .= '<li>'.RESERVATION_ERROR_GUEST_NAME_LENGTH . '</li>';
                }
              }
            }
            if (!$reservation_info['sender']) {
                $message .= '<li>'.RESERVATION_ERROR_SENDER_NAME . '</li>';
            } elseif (mb_strlen($reservation_info['sender']) > 50) {
                $message .= '<li>'.RESERVATION_ERROR_SENDER_NAME_LENGTH . '</li>';
            }
            if (!$reservation_info['sender_mail']) {
                $message .= '<li>'.RESERVATION_ERROR_SENDER_MAIL . '</li>';
            } elseif (!EZValidator::valid_email($reservation_info['sender_mail'])) {
                $message .= '<li>'.RESERVATION_ERROR_SENDER_INVALIDMAIL . '</li>';
            }

            // 送信者
            if (mb_strlen($reservation_info['mail_body']) > 10000) {
                $message .= '<li>'.RESERVATION_ERROR_MAIL_BODY_LENGTH . '</li>';
            }

            // 招待者
            if($reservation_info["is_organizer_flg"] == 1){
                if (!$reservation_data["organizer"]['name']) {
                    $message .= '<li>'.RESERVATION_ERROR_ORGANIZER_NAME . '</li>';
                } elseif (mb_strlen($reservation_data["organizer"]['name']) > 50) {
                    $message .= '<li>'.RESERVATION_ERROR_ORGANIZER_NAME_LENGTH . '</li>';
                }
                if (!$reservation_data["organizer"]["email"]) {
                    $message .= '<li>'.RESERVATION_ERROR_ORGANIZER_MAIL . '</li>';
                } elseif (!EZValidator::valid_email($reservation_data["organizer"]["email"])) {
                    $message .= '<li>'.RESERVATION_ERROR_ORGANIZER_INVALIDMAIL . '</li>';
                }
            }
        }
        // 事前ファイルアップロード
        if ($reservation_data["upload_flg"]) {
            if(isset($_FILES["document"])) {
                // 対応フォーマット取得
                require_once ("classes/N2MY_Document.class.php");
                $objDocument = new N2MY_Document($this->get_dsn());
                $support_document_list = $objDocument->getSupportFormat($room_key);
                foreach($_FILES["document"]["name"] as $key => $filename) {
                    if ($filename) {
                        $file_param = pathinfo($filename);
                        $file_extension = strtolower($file_param["extension"]);
                        if (!in_array($file_extension, $support_document_list['support_ext_list'])) {
                            $this->logger2->info(array($file_param["extension"], $support_document_list['support_ext_list']));
                            $message .= '<li>'.$this->get_message("ERROR", "filetype"). '</li>';
                        } else {
                            // 画像
                            if (in_array($file_extension, $support_document_list['image_ext_list'])) {
                                $maxsize = ($room_info["room_info"]["whiteboard_size"] <= 5) ? $room_info["room_info"]["whiteboard_size"] : 5;
                            // 資料
                            } else {
                                $maxsize = ($room_info["room_info"]["whiteboard_size"] <= 20) ? $room_info["room_info"]["whiteboard_size"] : 20;
                            }
                            if ($_FILES["document"]["size"][$key] > $maxsize * 1024 * 1024) {
                                $message .= '<li>'.$this->get_message("ERROR", "maxfilesize"). '</li>';
                            }
                            // 主に upload_max_filesize 以上の大きさのファイルがアップロードされた場合
                            else if ($_FILES["document"]["error"][$key] == UPLOAD_ERR_INI_SIZE) {
                                $message .= '<li>' . $this->get_message("ERROR", "maxfilesize") . '</li>';
                            }
                        }
                    }
                }
            }
            // アップロード済みの資料名
            foreach($reservation_data['documents'] as $document_id => $document) {
                if (!$document['name']) {
                    $message .= '<li>'.RESERVATION_ERROR_DOCUMENT_NAME. '</li>';
                    break;
                }
            }
        }

        // ストレージファイルのチェック
        if($reservation_data["storage_documents"]){
            // 対応フォーマット取得
            require_once ("classes/N2MY_Document.class.php");
            $objDocument = new N2MY_Document($this->get_dsn());
            $support_document_list = $objDocument->getSupportFormat($room_key);
            foreach($reservation_data["storage_documents"] as $storage){
                $extension = strtolower($storage["extension"]);
                if (!in_array($extension, $support_document_list['support_ext_list'])) {
                    $message .= '<li>'.$this->get_message("ERROR", "filetype"). '</li>';
                }else if($room_info["room_info"]["whiteboard_page"] < $storage["document_index"]){
                    $message .= '<li>'.$this->get_message("ERROR", "maxindex"). '</li>';
                }else{
                    if (in_array($extension, $support_document_list['image_ext_list'])) {
                        $maxsize = ($room_info["room_info"]["whiteboard_size"] <= 5) ? $room_info["room_info"]["whiteboard_size"] : 5;
                        // 資料
                    }else {
                        $maxsize = ($room_info["room_info"]["whiteboard_size"] <= 20) ? $room_info["room_info"]["whiteboard_size"] : 20;
                    }
                    if ($storage["file_size"] > $maxsize * 1024 * 1024) {
                        $message .= '<li>'.$this->get_message("ERROR", "maxfilesize"). '</li>';
                    }
                }

            }
        }
        // 動画の重複チェック
        if($reservation_data["clips"]){
            foreach($reservation_data["clips"] as $clips){
                $clip_keys[] = $clips["clip_key"];
            }
            // クリックキーで重複チェックする
            $clip_count = count($clip_keys);
            $count_unique = count(array_unique($clip_keys));
            if($clip_count != $count_unique){
                $message .= '<li>'.$this->get_message("ERROR", "uniquevideo"). '</li>';
            }

        }


        if (!$this->config->get("IGNORE_MENU", "teleconference")){
            if ($reservation_info['tc_type'] == 'pgi') {
                if (!$reservation_info['use_pgi_dialin'] && !$reservation_info['use_pgi_dialin_free']&& !$reservation_info['use_pgi_dialout']) {
                    $message .= '<li>'.TELECONF_ERROR_SELECT. '</li>';
                }
            }
        }
            // 入力エラー
        if ($message) {
            $this->logger2->info(array($reservation_data, $message));
        }
        return $message;
    }

    function action_wb_status() {
        $mode = $this->request->get("mode");
        $reservation_data = $this->getReservationData($mode);
        $meeting_key      = $reservation_data["info"]["meeting_key"];

        $this->logger2->debug($reservation_data);

        $rows = $this->obj_N2MY_Reservation->getDocumentList($meeting_key);
        if (DB::isError($rows)) {
            $this->logger2->error($rows->getUserInfo());
            return false;
        }
        $document_list = array();
        foreach($rows as $document) {
            $document_id = $document["document_id"];
            $document_list[] = array(
                "document_id"     => $document["document_id"],
                "name"            => $document["document_name"],
                "size"            => $document["document_size"],
                "category"        => $document["document_category"],
                "extension"       => $document["document_extension"],
                "status"          => $document["document_status"],
                "document_index"  => $document["document_index"],
                "sort"            => $document["document_sort"],
                "create_datetime" => $document["create_datetime"],
                "update_datetime" => $document["update_datetime"],
            );
        }
        print json_encode($document_list);
    }

    function action_wb_view() {
        $document_id      = $this->request->get("document_id");
        $page             = $this->request->get("page", 1);
        $reservation_data = $this->getReservationData();

        $this->logger2->debug($reservation_data);
        $meeting_key = $reservation_data["info"]["meeting_key"];
        if ($document_data = $this->obj_N2MY_Reservation->getDocumentData($meeting_key, $document_id)) {
            $this->logger2->debug($document_data);
            $file = N2MY_DOC_DIR.$document_data["document_path"].$document_id."/".$document_id."_".$page.".jpg";
            if (file_exists($file)) {
                header("Content-Type: image/jpg;");
                header("Cache-Control: public, must-revalidate");
                print file_get_contents($file);
                exit();
            }
        }
        print "error!";
    }

    /**
     * プレビュー取得
     */
    public function action_show_preview()
    {
        $request = $this->request->getAll();
        require_once ("classes/N2MY_Document.class.php");
        $objDocument  = new N2MY_Document( $this->get_dsn() );
        $documentInfo = $objDocument->getDetail( $request["document_id"] );
        if ($documentInfo["document_format"] == "vector") {
            $return["fileName"] = sprintf( "%s_%d.swf", $documentInfo["document_id"], $request["index"] );
            $path = N2MY_DOC_DIR.$documentInfo["document_path"].$documentInfo["document_id"]."/".$return["fileName"];
            if (file_exists($path)){
                header('Content-type: application/x-shockwave-flash');
                header('Content-Length: '.filesize($path));
                $fp = fopen($path, "r");
                if ($fp) {
                    while (!feof($fp)) {
                        $buffer = fgets($fp, 4096);
                        echo $buffer;
                    }
                    fclose($fp);
                }
            }
            return;
        }

        $fileInfo = $objDocument->getPreviewFileInfo( $documentInfo, $request["index"] );
        //resize
        $max_width = 480;
        $max_height = 320;
        $percent = 0.5;

        list($width, $height) = getimagesize($fileInfo["targetImage"]);
        $x_ratio = $max_width / $width;
        $y_ratio = $max_height / $height;

        if( ($width <= $max_width) && ($height <= $max_height) ){
            $tn_width  = $width;
            $tn_height = $height;
        } elseif (($x_ratio * $height) < $max_height) {
            $tn_height = ceil($x_ratio * $height);
            $tn_width  = $max_width;
        } else {
            $tn_width  = ceil($y_ratio * $width);
            $tn_height = $max_height;
        }

        header('Content-type: image/jpeg');
        // 再サンプル
        $image_p = imagecreatetruecolor($tn_width, $tn_height);
        $image = imagecreatefromjpeg($fileInfo["targetImage"]);
        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $tn_width, $tn_height, $width, $height);
        imagejpeg($image_p, null, 100);
    }

    /**
     * 予約のキャンセル
     */
    function action_cancel() {
        $reservation_session = $this->request->get('reservation_session');
        $reservation_data    = $this->obj_N2MY_Reservation->getDetail($reservation_session);
        if (!$this->isAuthorized($reservation_data, '?action_cancel=&reservation_session='.$reservation_session)) {
            return false;
        }
        $this->obj_N2MY_Reservation->cancel($reservation_session);
        header("Location: /services/index.php");
    }

    /**
     * 認証済みか確認
     */
    function isAuthorized($reservation_data, $callback) {
        // パスワード無し
        if (!$reservation_data["info"]["reservation_pw"] || $reservation_data["info"]["reservation_pw_type"] != "1") {
            return true;
        }

        // 認証済み
        $reservation_auth = $this->session->get('reservation_auth', $this->_name_space);
        if ($reservation_auth == $reservation_data["info"]["reservation_session"]) {
            return true;
        }
        // 認証無し
        $redirect_url = '/services/reservation/'.$callback;
        $this->session->set("redirect_url", $redirect_url, $this->_name_space);
        $this->template->assign('reservation_session', $reservation_data["info"]["reservation_session"]);
        $this->display('user/reservation/auth2.t.html');
false;
    }

    function action_check_pw() {
        $reservation_session = $this->request->get("reservation_session");
        $reservation_pw      = $this->request->get("reservation_pw");
        $user_info           = $this->session->get("user_info");
        $reservation_data = $this->obj_N2MY_Reservation->getDetail($reservation_session);
        require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
        require_once 'classes/dbi/user.dbi.php';
        $user_db = new UserTable($this->get_dsn());
        $admin_password = $user_db->getOne('user_key = ' . $user_info["user_key"] , "user_admin_password");
        if ($reservation_data["info"]["reservation_pw"] == $reservation_pw ||
            EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $admin_password) == $reservation_pw) {
            $redirect_url = $this->session->get("redirect_url", $this->_name_space);
            $reservation_auth = $this->session->set('reservation_auth', $reservation_session, $this->_name_space);
            header('Location: '.$redirect_url);
            //exit();
        }
        $this->logger->trace(__FUNCTION__."#start",__FILE__,__LINE__);
        $reservation_pw = $this->request->get("reservation_pw");
        $this->template->assign('reservation_session', $reservation_data["info"]["reservation_session"]);
        $this->display('user/reservation/auth2.t.html');
    }

    function action_change_document_sort(){
        $request = $this->request->getAll();
        require_once("classes/N2MY_Document.class.php");
        $objDocument = new N2MY_Document($this->get_dsn());
        echo json_encode($objDocument->change_sort($request["id"],$request["move"]));
    }

    private function getGuestMailSubject($mode, $langCd, $custom) {
        switch ($mode) {
        case "create":
            return $this->getMessage($langCd, "DEFINE", "RESERVATION_MAIL_SUBJECT", $custom);
            break;
        case "modify":
            return $this->getMessage($langCd, "DEFINE", "RESERVATION_MAIL_MODIFY_SUBJECT", $custom);
            break;
        case "delete":
            return $this->getMessage($langCd, "DEFINE", "RESERVATION_MAIL_DELETE_SUBJECT", $custom);
            break;
        case "addlist" :
            return $this->getMessage($langCd, "DEFINE", "RESERVATION_MAIL_ADDLIST_SUBJECT", $custom);
            break;
        case "deletelist":
            return $this->getMessage($langCd, "DEFINE", "RESERVATION_MAIL_DELLIST_SUBJECT", $custom);
            break;
        case "modify_pw":
            return $this->getMessage($langCd, "DEFINE", "RESERVATION_MAIL_MODIFY_PW_SUBJECT", $custom);
            break;
        }
    }
    private function getOwnerMailSubject($mode, $langCd, $custom)
    {
        switch ($mode) {
        case "create":
            return $this->getMessage($langCd, "DEFINE", "RESERVATION_OWNER_SUBJECT", $custom);
            break;

        case "modify":
            return $this->getMessage($langCd, "DEFINE", "RESERVATION_OWNER_MODIFY_SUBJECT", $custom);
            break;

        case "delete" :
            return $this->getMessage($langCd, "DEFINE", "RESERVATION_OWNER_DELETE_SUBJECT", $custom);
            break;

        case "addlist":
            return $this->getMessage($langCd, "DEFINE", "RESERVATION_OWNER_ADDLIST_SUBJECT", $custom);
            break;

        case "deletelist":
            return $this->getMessage($langCd, "DEFINE", "RESERVATION_OWNER_DELLIST_SUBJECT", $custom);
            break;

        case "modify_pw" :
            return $this->getMessage($langCd, "DEFINE", "RESERVATION_OWNER_MODIFY_PW_SUBJECT", $custom);
            break;
        }
    }
    //{{{array_sub
    private function array_sub($array1, $array2)
    {
        if ((0 < count($array1)) && (0 < @count($array2))) {
            foreach ($array2 as $val2){
                $pos = array_search($val2, $array1);
                if ($pos !== false) {
                    unset($array1[$pos]);
                }
            }
        }
        return $array1;
    }
    //}}}
    //{{{findClipKeysByMeetingKey
    private function findClipKeysByMeetingKey($meeting_key)
    {
        require_once ("classes/dbi/meeting_clip.dbi.php");
        $meeting_clip_table = new MeetingClipTable($this->get_dsn());
        $meeting_clips = $meeting_clip_table->findByMeetingKey($meeting_key);
        if (0 ==  count($meeting_clips)) {
            return array();
        }
        $res = array();
        foreach ($meeting_clips as $v) {
            $res[] = $v['clip_key'];
        }
        return $res;
    }
    //}}}
    //{{{mergeClipDisplayInfo
    private function mergeClipDisplayInfo($clip_list)
    {
        if (count($clip_list) <= 0) {
            return $clip_list;
        }

        foreach ($clip_list as $k => $clip) {
            $clip_list[$k]['thumbnail_url'] = ClipFormat::thumbnailUrl($clip['clip_key'], $clip['storage_no']);
            $clip_list[$k]["duration"]      = gmdate('H:i:s', $clip["duration"]);
        }
        return $clip_list;
    }
    //}}}
    //{{{getClipValueByKeys
    private function getMeetingClips($meeting_ticket) {
      $meeting_table = new MeetingTable($this->get_dsn());
      $meeting_key   = $meeting_table->findMeetingKeyByMeetingTicket($meeting_ticket);
      require_once ("classes/dbi/meeting_clip.dbi.php");
      $meeting_clip_table = new MeetingClipTable($this->get_dsn());
      return $meeting_clip_table->findByMeetingKey($meeting_key);
    }

    private function getClipValueByKeys($clip_keys, $user_key)
    {
        if (!$user_key) {
            throw new Exception(__FILE__." ".__LINE__.' user_key is empty');
        }
        if (count($clip_keys) <= 0) {
           return array();
        }
        require_once ("classes/dbi/clip.dbi.php");
        $clip_table = new ClipTable($this->get_dsn());

        $_clip_list = $clip_table->findByUsersClipKeys($clip_keys, $user_key);
        if (count($_clip_list) <= 0) {
            return array();
        }
        $clip_list = array();
        foreach ($clip_keys as $clip_key) {
            foreach ($_clip_list as $_clip) {
                if ($_clip['clip_key'] == $clip_key){
                    $clip_list[$clip_key] = $_clip;
                }
            }
        }
        return $clip_list;
    }
    ////}}}
    //{{{findClipKeysByMeetingTicket
    private function findClipKeysByMeetingTicket($meeting_ticket)
    {
        if (!$meeting_ticket) {
            throw new Exception(__FILE__." ".__LINE__." ".'meeting_ticket is empty');
        }
        $meeting_table = new MeetingTable($this->get_dsn());
        $meeting_key   = $meeting_table->findMeetingKeyByMeetingTicket($meeting_ticket);
        return $meeting_key ? $this->findClipKeysByMeetingKey($meeting_key) : null;
    }

    private function findClipKeysStorageByMeetingTicket($meeting_ticket)
    {
        if (!$meeting_ticket) {
            throw new Exception(__FILE__." ".__LINE__." ".'meeting_ticket is empty');
        }
        $meeting_table = new MeetingTable($this->get_dsn());
        $meeting_key   = $meeting_table->findMeetingKeyByMeetingTicket($meeting_ticket);
        return $meeting_key ? $this->findClipKeysStorageByMeetingKey($meeting_key) : null;
    }

    private function findClipKeysStorageByMeetingKey($meeting_key)
    {
        require_once ("classes/dbi/meeting_clip.dbi.php");
        $meeting_clip_table = new MeetingClipTable($this->get_dsn());
        $meeting_clips = $meeting_clip_table->findByMeetingKey($meeting_key);
        if (0 ==  count($meeting_clips)) {
            return array();
        }
        //require_once ("classes/dbi/storage_file.dbi.php");
        //$obj_storageFile = new StorageTable($this->get_dsn());
        $res = array();
        foreach ($meeting_clips as $v) {
          //$where = "storage_file_key ='".$v["storage_file_key"]."'";
          //$file_name = $obj_storageFile->getOne($where, "meeting_clip_name");
            $res[] = $v;
        }
        return $res;
    }

    //}}}
    //{{{mergeCheckedClipKeys
    private function mergeCheckedClipKeys($src_array_keys, $add_conma_keys, $del_conma_keys)
    {
        if (!$add_conma_keys && !$del_conma_keys) {
            return $src_array_keys;
        }
        $src_array_keys  = is_array($src_array_keys) ? $src_array_keys : array();

        $add_array_keys  = $add_conma_keys ? explode(',' , $add_conma_keys) : array();
        $del_array_keys  = $del_conma_keys ? explode(',' , $del_conma_keys) : array();

        $res_array_keys  = array_merge($src_array_keys, $add_array_keys);
        return array_unique($this->array_sub($res_array_keys, $del_array_keys));

    }
    //}}}
    //{{{saveCheckedClipStatus
    private function saveCheckedClipStatus()
    {
        require_once ("classes/dbi/clip.dbi.php");
        $clip_table = new ClipTable($this->get_dsn());
        $user_info  = $this->session->get("user_info");

        if (isset($_REQUEST["action_create_confirm"]) || isset($_REQUEST["action_create_clip_relation"])) {
            $is_modify = false;
        } elseif (isset($_REQUEST["action_modify_confirm"]) || isset($_REQUEST["action_modify_clip_relation"])) {
            $is_modify = true;
        }
        if($is_modify)
            $reserve_data = $this->getReservationData("modify");
        else
            $reserve_data = $this->getReservationData("create");
        // 作成,編集画面でネームスペースが違う
        $clips        = $is_modify ? $reserve_data['info']['clips'] : $reserve_data['clips'];
        if ($this->is_saved_checked_clip) {
            return $clip_table->activeUsersClipKeysIn($clips, $user_info['user_key']);
        }
        $clips = $this->mergeCheckedClipKeys($clips, $this->request->get("add_clips"), $this->request->get("del_clips"));
        $clips = $clip_table->activeUsersClipKeysIn($clips, $user_info['user_key']); // 削除clipを選択対象から無くす
        if ($is_modify) { // 編集画面と登録画面は入力内容のネームスペースが違う
            $reserve_data['info']['clips'] = $clips;
            $this->setReservationData($reserve_data,"modify");
        } else {
            $reserve_data['clips'] = $clips;
            $this->setReservationData($reserve_data,"create");
        }

        $this->is_saved_checked_clip = true;
        return $clips;
    }
    //}}}
    private function arrayToJavaScript($array)
    {
        if (count($array) <= 0) {
            return '[]';
        }
        return '["'.implode($array,'","').'"]';
    }
    private function pgiDialConfigs($room)
    {
        $res = array();
        foreach (array('use_pgi_dialin', 'use_pgi_dialin_free', 'use_pgi_dialout') as $name) {
             $res[] = array('name'   => array('enable'  => $name,
                                              'disable' => 'un_'.$name),
                            'enable' => $room[$name] ? true : false);
        }
        return $res;
    }

    //{{{getPGiDetail
    private function getPGiDetail($meeting_ticket)
    {
        $obj_Meeting  = new DBI_Meeting($this->get_dsn());
        $meeting      = $obj_Meeting->getRow(sprintf("meeting_ticket='%s' AND is_deleted = 0", $meeting_ticket));
        if (!$meeting) {
            return false;
        }
        if ($meeting['pgi_conference_id']) {
            $pgi_info = array('pgi_conference_id' => $meeting['pgi_conference_id'],
                              'pgi_m_pass_code'   => $meeting['pgi_m_pass_code'],
                              'pgi_p_pass_code'   => $meeting['pgi_p_pass_code'],
                              'pgi_l_pass_code'   => $meeting['pgi_l_pass_code'],
                              'pgi_phone_numbers' => unserialize($meeting['pgi_phone_numbers']));
        } else {
            $pgi_info = null;
        }
        if ($meeting['pgi_api_status'] == 'complete') {
            require_once("classes/pgi/PGiPhoneNumber.class.php");
            if ($meeting['use_pgi_dialin'] || $meeting['use_pgi_dialin_free'])  {
                $main_phone_number = unserialize($meeting['pgi_phone_numbers']);
                $main_phone_number = PGiPhoneNumber::toMultilangLocationName($main_phone_number, $this->_lang);
                if (!$meeting['use_pgi_dialin']){
                    $main_phone_number = PGiPhoneNumber::extractFreeMain($main_phone_number);
                } else {
                    $main_phone_number = PGiPhoneNumber::extractLocalMain($main_phone_number);
                }
            }
        }
        $pgi_detail = array('tc_type'             => $meeting['tc_type'] ? $meeting['tc_type'] : 'voip',
                            'pgi_info'            => $pgi_info,
                            'tc_teleconf_note'    => $meeting['tc_teleconf_note'],
                            'use_pgi_dialin'      => $meeting['use_pgi_dialin'],
                            'use_pgi_dialin_free' => $meeting['use_pgi_dialin_free'],
                            'use_pgi_dialout'     => $meeting['use_pgi_dialout'],
                            'meeting_session_id'  => $meeting['meeting_session_id'],
                        );
        if (@$main_phone_number) {
            $pgi_detail['main_phone_number'] = $main_phone_number;
        }
        return $pgi_detail;
    }
    //}}}
    //{{{getUploadStatus
    private function getUploadStatus($reservation_data)
    {
        $upflag = false;
        foreach ($reservation_data["documents"] as $document_id => $document) {
            if ($reservation_data["upload_flg"] && !$document["delete"] ||
                !$reservation_data["upload_flg"] && !$document["tmp_name"] &&
                ($document["upload_status"] || $document["not_delete"])) {
                $upflag = true;
            }
        }
        if($reservation_data["storage_documents"]){
            $upflag = true;
        }
        return $upflag;
    }

    private function _getMeetingVersion() {
        $version_file = N2MY_APP_DIR."version.dat";
        if ($fp = fopen($version_file, "r")) {
            $version = trim(fgets($fp));
        }
        return $version;
    }

    //}}}
}

$main = new AppReservation();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
