<?php

require_once("classes/N2MY_DBI.class.php");

class DatacenterIgnoreTable extends N2MY_DB {

    var $table = "datacenter_ignore";
    var $logger = null;
    protected $primary_key = "datacenter_ignore_key";

    public function __construct( $dsn )
    {
        $this->init($dsn, $this->table );
    }

    function DatacenterIgnoreTable( $dsn )
    {
        $this->logger =& EZLogger::getInstance();
        $this->init($dsn, $this->table);
    }


   //繋がないデータセンター（ignore）
    function get_datacenter_ignore($user_key) {
        $dc_ignore_list = array();
        $where = sprintf( "user_key='%s'", $user_key);
        $dc_ignore = $this->getRowsAssoc($where, null, null, 0, "datacenter_key") ;
        foreach( $dc_ignore as $i_dc ){
            $dc_ignore_list[] = $i_dc["datacenter_key"];
        }
        return $dc_ignore_list;
    }


}