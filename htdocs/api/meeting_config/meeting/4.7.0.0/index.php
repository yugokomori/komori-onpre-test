<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/AppFrame.class.php");

class API_LoadConfig extends AppFrame
{
    function default_view() {
        $this->render_default();
    }

    private function render_default() {
        //FEP用カスタマイズ
        if ($this->session->get("invitedGuest") || $this->session->get("is_invite")) {
            $frame["fep_directory"] = $this->config->get("SMARTY_DIR", "fep_directory").$this->config->get("SMARTY_DIR", "fep_guest_directory");
        } else if($this->session->get("admin_login")) {
            $frame["fep_directory"] = $this->config->get("SMARTY_DIR", "fep_admin_directory");
        } else {
            $frame["fep_directory"] = $this->config->get("SMARTY_DIR", "fep_directory");
        }
        $this->template->assign( "pathToLibrary"                     , $frame["fep_directory"]."/lang/".$this->_lang."/flash/meeting/Lib.swf" );
        $this->template->assign( "pathToApplicationAppearanceDataXML", "application_appearance.php" );
        $this->template->assign( "pathToConfigDataXML"               , "vrms.php" );
        $this->template->assign( "pathToCoreAppearanceDataXML"       , "core_appearance.php" );
        $this->template->assign( "pathTolanguageXML"                 , $frame["fep_directory"]."/lang/".$this->_lang."/flash/language.xml" );
        $this->template->assign( "pathToLayoutXML"                   , "layout.php" );
        $this->display( "core/api/meeting_config/meeting/project.t.xml");
    }
}

$main = new API_LoadConfig();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
