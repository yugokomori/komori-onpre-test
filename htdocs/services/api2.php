<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once ("classes/dbi/reservation.dbi.php");
require_once ("classes/dbi/room.dbi.php");
require_once ('lib/EZLib/EZUtil/EZDate.class.php');
require_once ("classes/AppFrame.class.php");
require_once ("classes/N2MY_Reservation.class.php");
require_once ("classes/core/Core_Meeting.class.php");
require_once ("classes/core/dbi/Meeting.dbi.php");
//require_once ("classes/mgm/MGM_auth.class.php");

class AppCoreApi extends AppFrame {

    var $obj_Reservation = null;
    var $obj_Room = null;

    var $_name_space = null;
    private $dsn;

    function init() {
        if (!$this->dsn = $this->get_dsn()) {
            $this->dsn = $this->_get_dsn( $this->request->get( "serverDsnKey", "data1" ) );
        }
        $this->obj_Reservation = new ReservationTable( $this->dsn );
        $this->obj_Room = new RoomTable( $this->dsn );
        $this->_name_space = md5(__FILE__);
    }

    function action_api() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    /**
     * 会議中のユーザ一覧を取得する
     */
    function action_room_userlist() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        // パラメタ取得
        $room_key = $this->get_room_key();
        $meeting_key = $this->request->get("meeting_key");
        // meeting_keyの指定が無い場合は、最後に開始したmeeging_keyを取得
        if (!$meeting_key) {
            $meeting = new Meeting();
            $meeting_info = $meeting->serviceLogin($room_key);
            $meeting_key = $meeting_info["meeting_key"];
            $this->logger->trace(__FUNCTION__."#meeting_key", __FILE__, __LINE__, array($room_key, $meeting_key));
        }
        // 会議参加者一覧を取得
        $query = array(
            "meeting_ticket" => $meeting_key,
            "provider_id" => $this->provider_id,
            "provider_password" => $this->provider_pw,
        );
        $url = $this->get_core_url("api/meeting/MeetingParticipants.php", $query);
        $this->logger->trace(__FUNCTION__."#url", __FILE__, __LINE__, array($query,$url));
        $output = $this->wget($url);
        $logs = array(
            "room_key" => $room_key,
            "meeting_key" => $meeting_key,
            "list" => $output,
            );
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $logs);
        header("Content-Type: text/xml; charset=UTF-8");
        $this->nocache_header();
        print $output;
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__, $output);
    }

    function action_room_status() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        // パラメタ取得
        $room_key = $this->get_room_key();
        $meeting_key = $this->request->get("meeting_key");
        // meeting_keyの指定が無い場合は、最後に開始したmeeging_keyを取得
        if (!$meeting_key) {
            $meeting = new Meeting();
            $meeting_info = $meeting->serviceLogin($room_key);
            $meeting_key = $meeting_info["meeting_key"];
            $this->logger->trace(__FUNCTION__."#meeting_key", __FILE__, __LINE__, array($room_key, $meeting_key));
        }
        // 会議参加者一覧を取得
        $query = array(
            "meeting_ticket" => $meeting_key,
            "provider_id" => $this->provider_id,
            "provider_password" => $this->provider_pw,
        );
        $url = $this->get_core_url("api/meeting/MeetingStatus.php", $query);
        $output = $this->wget($url);
        $output = str_replace("</status>", "</status><meeting_key>".$meeting_key."</meeting_key>", $output);
        $logs = array(
            "room_key" => $room_key,
            "meeting_key" => $meeting_key,
            "status" => $output,
            );
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $logs);
        header("Content-Type: text/xml; charset=UTF-8");
        $this->nocache_header();
        print $output;
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__, $output);
    }

    /**
     * 指定部屋の状態を取得
     */
    function action_all_status() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        $_rooms = $this->request->get("rooms");
        $reservations = array();
        // 部屋と会議キーを指定
        if (is_array($_rooms)) {
            foreach ($_rooms as $room_key => $meeting_key) {
                // 契約した部屋のみ取得可
                $query["rooms[".$room_key."]"] = $meeting_key;
                // 予約情報取得
//                $reservations[$room_key] = $this->_get_reservation_list($room_key);
            }
        // 部屋のみ指定（現在の状態）
        } else {
            $roomkeys = explode(",", $_rooms);
            $rooms = array();
            foreach($roomkeys as $room_key) {
                $rooms[] = $room_key;
            }
            $where = "room_key in ('".join("','", $rooms)."')";
            $rooms = $this->obj_Room->getRowsAssoc($where, null, null, null, "room_key, meeting_key");
            if ( DB::isError( $rooms ) ) {
                $this->logger->error( $rs->getUserInfo() );
                return $rooms;
            }
            foreach( $rooms as $key => $val ){
                $query[$val["room_key"]] = $val["meeting_key"];
//                $reservation_list = $this->_get_reservation_list( $val["room_key"] );
            }
        }
        // 指定会議の予約情報取得
        $obj_CoreMeeting = new Core_Meeting( $this->dsn );
        $room_list = $obj_CoreMeeting->getMeetingStatus( $query );
        header("Content-Type: text/javascript; charset=UTF-8");
        $this->nocache_header();
        $str = '<meetingStatus api_class="'.__CLASS__.'" api_error="'.$this->_error.'"><meetingList>';
        foreach($room_list as $room_info) {
            $str .= '<meeting><roomKey>'.htmlspecialchars($room_info["room_key"]).'</roomKey>' .
                '<meetingTicket>'.htmlspecialchars($room_info["meeting_ticket"]).'</meetingTicket>' .
                '<status>'.$room_info["status"].'</status>' .
                '<participantCount>'.$room_info["pcount"].'</participantCount>' .
                '<users>';
            foreach ($room_info["participants"] as $participant) {
                $str .= '<user key="'.$participant["participant_key"].'">' .
                    '<name>'.htmlspecialchars($participant["participant_name"]).'</name>' .
                    '<mode>'.htmlspecialchars($participant["participant_mode_name"]).'</mode>' .
                    '<role>'.htmlspecialchars($participant["participant_role_name"]).'</role>' .
                    '<type>'.htmlspecialchars($participant["participant_type_name"]).'</type>' .
                '</user>';
            }
            $str .= '</users><extension>'.htmlspecialchars($room_info["room_key"]).'</extension></meeting>';
        }
        $str .= '</meetingList></meetingStatus>';
        // エンティティ処理
        print $str;
    }

    private function _get_reservation_list( $room_key ) {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        // パラメタ
        $start      = $this->request->get("start", date("Y-m-d H:i:s"));
        $end        = $this->request->get("end", "");
        $sort        = array(  $this->request->get("sort_key", "reservation_starttime") => $this->request->get("sort_type", "asc") );
        $limit      = $this->request->get("limit", RESERVATION_MAX_VIEW);
        $offset     = $this->request->get("offset", null);
        // 部屋情報取得
        $room["room_key"] = $room_key;
        $this->template->assign("room", $room);
        // 現在の予約件数
        $where = "room_key = '".$room_key."'" .
                " AND reservation_status = 1" .
                " AND reservation_starttime <= '".date('Y-m-d H:i:s')."'".
                " AND reservation_endtime > '".date('Y-m-d H:i:s')."'";
        $rowNowMeeting = $this->obj_Reservation->getRow( $where );
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$rowNowMeeting);
        $now = array();
        if ($rowNowMeeting) {
            $now = array(
                "meeting_key" => $rowNowMeeting["meeting_key"],
                "session" => $rowNowMeeting["reservation_session"],
                "name" => $rowNowMeeting["reservation_name"],
                "start" => $this->makeDate( $rowNowMeeting["reservation_starttime"] ),
                "end" => $this->makeDate( $rowNowMeeting["reservation_endtime"] )
                );
        }
//        $this->template->assign("now", $now);
        // 予約情報取得
        $where = "room_key = '".$room_key."'" .
                " AND reservation_status = 1" .
                " AND reservation_starttime > '".$start."'";
        $count = $this->obj_Reservation->numRows($where);
        $this->template->assign("count", $count);
        // 予約情報取得
        $where = "room_key = '".$room_key."'" .
                " AND reservation_status = 1" .
                " AND reservation_starttime >= '".$start."'";
        if ($end != "") {
            $where .= " AND reservation_endtime < '".$end."'";
        }
        $reservation_list = $this->obj_Reservation->getRowsAssoc($where, $sort, $limit, $offset);
        $reservations = array();
        $reservations["now"] = $now;
        if ($reservation_list) {
            foreach($reservation_list as $_key => $row) {
                // 開始前
                $reservations["list"][] = array(
                    "room_key"    =>    $room_key,
                    "meeting_key" => $row["meeting_key"],
                    "session" => $row["reservation_session"],
                    "name" => $row["reservation_name"],
                    "start" => $this->makeDate( $row["reservation_starttime"] ),
                    "end" => $this->makeDate( $row["reservation_endtime"] )
                    );
            }
        }
        return $reservations;
    }

    function action_reservation_list() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        // パラメタ
        $room_key   = $this->get_room_key();
        $start      = $this->request->get("start", date("Y-m-d H:i:s"));
        $end        = $this->request->get("end", "");
        $sort_key   = $this->request->get("sort_key", "reservation_starttime");
        $sort_type  = $this->request->get("sort_type", "asc");
        $limit      = $this->request->get("limit", null);
        $offset     = $this->request->get("offset", null);
        // 部屋情報取得
        $room_table = new RoomTable($this->get_dsn());
        $room["room_key"] = $room_key;
        $this->template->assign("room", $room);
        // 現在の予約件数
        $where = "room_key = '".$room_key."'" .
                " AND reservation_status = 1" .
                " AND reservation_starttime <= '".date('Y-m-d H:i:s')."'".
                " AND reservation_endtime > '".date('Y-m-d H:i:s')."'";
        $_rowNowMeeting = $this->obj_Reservation->getList($where);
        $rowNowMeeting = $_rowNowMeeting[0];
        $now = array();
        if ($rowNowMeeting) {
            $now = array(
                "meeting_key" => $rowNowMeeting["meeting_key"],
                "session" => $rowNowMeeting["reservation_session"],
                "name" => $rowNowMeeting["reservation_name"],
                "start" => $rowNowMeeting["reservation_starttime"],
                "end" => $rowNowMeeting["reservation_endtime"]
                );
        }
        $this->template->assign("now", $now);
        // 予約情報取得
        $where = "room_key = '".$room_key."'" .
                " AND reservation_status = 1" .
                " AND reservation_starttime > '".$start."'";
        $count = $this->obj_Reservation->numRows($where);
        $this->template->assign("count", $count);
        // 予約情報取得
        $where = "room_key = '".$room_key."'" .
                " AND reservation_status = 1" .
                " AND reservation_starttime >= '".$start."'";
        if ($end != "") {
            $where .= " AND reservation_endtime < '".$end."'";
        }
        $rowReservation = $this->obj_Reservation->getList($where, $sort_key, $sort_type, $limit, $offset);
        $reservations = array();
        if ($rowReservation) {
            foreach($rowReservation as $_key => $row) {
                // 開始前
                $reservations[] = array(
                    "meeting_key" => $row["meeting_key"],
                    "session" => $row["reservation_session"],
                    "name" => $row["reservation_name"],
                    "start" => $row["reservation_starttime"],
                    "end" => $row["reservation_endtime"]
                    );
            }
        }
        $this->template->assign("reservations", $reservations);
        $output = $this->fetch('user/reservation/reservation.t.xml');
        header("Content-Type: text/xml; charset=UTF-8");
        $this->nocache_header();
        print $output;
        $this->logger->trace(__FUNCTION__."#end",__FILE__,__LINE__);
    }

    /**
     * 会議終了
     */
    function action_stop_meeting() {
        $obj_CoreMeeting = new Core_Meeting( $this->dsn );
        $objMeeting      = new DBI_Meeting( $this->dsn );
        $meeting_ticket = $this->request->get("meeting_ticket");
        if (!$meeting_ticket) {
            $this->logger2->warn($meeting_ticket);
            return false;
        }
        $where = "meeting_ticket = '".addslashes($meeting_ticket)."'";
        $meeting_key = $objMeeting->getOne($where, "meeting_key");
        if (!$meeting_key) {
            $this->logger2->warn($meeting_key);
            return false;
        }
        $obj_CoreMeeting->stopMeeting($meeting_key);
    }

    /**
     * スケジュール一覧を整形してHTMLで出力
     */
    function action_schedule_list() {
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $_GET);
        // 開始時間
        $room_key   = $this->get_room_key();
        $s_year     = $this->request->get("s_year");
        $s_month    = $this->request->get("s_month");
        $s_day      = $this->request->get("s_day");
        $days       = $this->request->get("days", 1);
        $reservation_place       = $this->request->get("reservation_place", N2MY_SERVER_TIMEZONE);
        $start_datetime = mktime(0,0,0,$s_month, $s_day, $s_year);
        // 終了時間
        $end_datetime = $start_datetime + ($days * 3600 * 24);
        return $this->render_schedule_list($room_key, $start_datetime, $end_datetime, $reservation_place);
    }

    /**
     * デフォルトページ
     */
    function default_view() {
        $this->logger->trace(__FUNCTION__."#START", __FILE__, __LINE__);
        print "";
        $this->logger->trace(__FUNCTION__."#END", __FILE__, __LINE__);
    }

    /**
     * 指定日のスケジュール一覧
     */
    function render_schedule_list($room_key, $start_datetime, $end_datetime = null, $reservation_place = N2MY_SERVER_TIMEZONE) {
        $target_date = $start_datetime;
        if ($end_datetime) {
            $next_date = $end_datetime;
        } else {
            $next_date = $target_date + (3600 * 24);
        }
        $_start_date = date("Y-m-d 00:00:00", $target_date);
        $_end_date = date("Y-m-d 00:00:00", $next_date);
        // TimeZone指定
        $EZDate = new EZDate();
        $_start_date = $EZDate->getLocateTime($_start_date, $this->obj_Reservation->serverTimeZone, $reservation_place);
        $_start_date = date("Y-m-d H:i:s", $_start_date);
        $_end_date = $EZDate->getLocateTime($_end_date, $this->obj_Reservation->serverTimeZone, $reservation_place);
        $_end_date = date("Y-m-d H:i:s", $_end_date);
        $obj_Reservation = new N2MY_Reservation($this->get_dsn());
        $options = array(
            "limit" => 10,
            "offset" => 0,
            "start_time" => $_start_date,
            "end_time" => $_end_date,
        );
        $resevation_list = $obj_Reservation->getList($room_key, $options, $reservation_place);
        $this->logger2->debug($resevation_list);
        $total = $obj_Reservation->getCount($room_key, $options);
        $rest = $total - 10;
        $this->template->assign("list", $resevation_list);
        $this->template->assign("start_datetime", $start_datetime);
        $this->template->assign("end_datetime", $end_datetime - (3600 * 24));
        $this->template->assign("timezone", $reservation_place);
        $this->template->assign("total", $total);
        $this->template->assign("rest", $rest);
        $this->display("user/reservation/schedule.t.html");
    }

    function nocache_header() {
        // 日付が過去
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        // 常に修正されている
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        // HTTP/1.1
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        // HTTP/1.0
        header("Pragma: no-cache");
    }

    private function makeDate( $date )
    {
        return preg_replace( "#-#", "/", $date );
    }

    private function _get_dsn( $server_dsn_key )
    {
        static $server_list;
        if (!$server_list) {
            $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        }
        return $server_list["SERVER_LIST"][$server_dsn_key];
    }

}

$main =& new AppCoreApi();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
