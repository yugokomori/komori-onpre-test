// カスタマイズ定数
/////////////////////////////////////////////////////////////////////
var DEBUG_MODE				= false;
var SERVER_URI				= "vcbstr02.mew.co.jp";
var SERVER_URI_GL			= SERVER_URI;
var AMF_SERVICE_URL			= "http://webmeeting.arrow.mew.co.jp/vcube/guest/services/core/gateway/";
var EXTERNAL_GW_URL			= "rtmp://vcbstr02.mew.co.jp/externalComminicationGateway/";
var START_USER_LENGTH		= 2;
var END_USER_LENGTH			= 0;
var FRAMERATE_AUTO_CALC_MAX_USER_LENGTH = 10;
var FRAMERATE_STATIC_MIN_CALC = 5;

//会議終了後にデータをストレージに移行するかどうか true/false
var TRANSFER_MODE			= false;

//イントラFMSかどうか
var INTRA_MODE				= false;
var MY_AMF_SERVICE_PORT		= "8081";
var MY_AMF_SERVICE_ROOT		= "http://" + SERVER_URI + ":" + MY_AMF_SERVICE_PORT;
var MY_AMF_SERVICE_URL		= MY_AMF_SERVICE_ROOT + "/gateway.php";

//PingFailedの統計をとるかどうか
var POST_PING_FAILED_DATA	= false;
//ユーザー帯域統計を送るかどうか
var POST_USER_STATISTIC_DATA	= false;

// amf失敗時のメールを飛ばす
var AMF_ERROR_MAIL = false;
var AMF_ERROR_MAIL_TO = "";

var ENABLE_TELECONFERENCE	= false;

// globalでは設定ミスの判別ができないためapplicationobject配下に設定
application.ENABLE_POLYCOM_OPTION		= true;

