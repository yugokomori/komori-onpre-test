<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
// +----------------------------------------------------------------------+
// |                                                                      |
// | [            ]                                                       |
// |                                                                      |
// +----------------------------------------------------------------------+
// | PHP version 4                                                        |
// +----------------------------------------------------------------------+
// | Authors:kei                                                          |
// |                                                                      |
// +----------------------------------------------------------------------+
// | Copyright (c) 2008               all rights reserved.                |
// +----------------------------------------------------------------------+
//
require_once('lib/EZLib/EZDB/EZDB.class.php');
require_once('lib/EZLib/EZHTML/EZValidator.class.php');

class MeetingUseLogTable extends EZDB {

    var $table = "meeting_use_log";
    var $logger = null;
    var $rules = array(
#        "log_no"          => array("required" => false),
        "meeting_key"          => array("required" => true),
#        "room_key"      => array("required" => true),
#        "meeting_tag"  => array("required" => false),
    );

#    var $error_rules = array( "meeting_tag", "required", "内容が入力されていません");


#    function MeetingUseLogTable($dsn) {
#        parent::EZDB($dsn);
#        $this->logger = EZLogger::getInstance();
#    }
    function MeetingUseLogTable($dsn) {
        $this->init($dsn, $this->table);
    }

    /**
     * 入力チェック
     *
     * @author kei
     * @param array $data フィールド名を連想配列形式で渡す
     * @return object EZValidatorのインスタンス
     *
     * EZValidator::isError()などでエラーの存在チェックを行ってください。
     */
    function check($data) {
        $check_obj = new EZValidator($data);
        foreach($this->rules as $field => $rules) {
            foreach($rules as $rule => $_val) {
                $this->logger->trace(__FUNCTION__, __FILE__, __LINE__, array($field, '%aaa%', $rule, $_val));
                $check_obj->addRule($field, '%aaa%', $rule, $_val);
            }
        }
        return $check_obj;
    }
}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>