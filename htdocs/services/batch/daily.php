<?php
require_once 'classes/AppFrame.class.php';
require_once 'classes/N2MY_DBI.class.php';
require_once 'classes/core/dbi/MeetingSequence.dbi.php';
require_once 'classes/core/dbi/MeetingUseLog.dbi.php';
require_once 'classes/core/Core_Meeting.class.php';
require_once 'htdocs/admin_tool/dBug.php';

class AppDailyBatch extends AppFrame
{
    /**
     *
     */
    function default_view() {
        $this->html_header();
    }

    /**
     * 前日までに終了していない会議一覧
     */
    function action_stop_meeting_list() {
        $this->html_header();
        $server_list = parse_ini_file(sprintf("%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        // 当日
        $today = date("Y-m-d H:i:s");
        // 前日
        $yesterday = date("Y-m-d H:i:s", strtotime("-1 Day"));
        foreach ( $server_list["SERVER_LIST"] as $key => $dsn ) {
            print "[ $key ]\n";
            print "当日";
            $this->_getNoEndMeetingList($dsn, $yesterday, $today);
            print "前日";
            $this->_getNoEndMeetingList($dsn, "0000-00-00 00:00:00", $yesterday);
        }
    }

    /**
     * 終了しない会議を終了させる
     */
    function action_stop_meeting() {
        $this->html_header();
        $server_list = parse_ini_file(sprintf("%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        // 前日に終了していない会議を終わらせる
        $yesterday = date("Y-m-d H:i:s", strtotime("-1 Day"));
        foreach ( $server_list["SERVER_LIST"] as $key => $dsn ) {
            $obj_CoreMeeting     = new Core_Meeting($dsn, N2MY_MDB_DSN );
            $obj_Meeting         = new DBI_Meeting($dsn);
            $obj_MeetingSequence = new DBI_MeetingSequence($dsn);
            $obj_MeetingUseLog   = new DBI_MeetingUseLog($dsn);
            // 終了時間を過ぎて入室人数が０人の終了していない予約会議の取得
            while ($row = $this->_getNoEndMeeting($dsn, $yesterday)) {
                $stop_where = "meeting_key = ".$row["meeting_key"];
                $count = $obj_MeetingUseLog->numRows($stop_where);
                // 利用なしの場合、利用不可にするだけ
                if ($count == 0) {
                    print "利用がないため無効にする\n";
                    $this->logger2->info($row["meeting_key"], "会議終了：利用がないため無効にする");
                    $data = array("status" => 0);
                    $ret = $obj_MeetingSequence->update($data, $stop_where);
                    if (DB::isError($ret)) {
                        $this->logger2->error($ret->getUserInfo());
                        return false;
                    }
                    $data = array("is_active" => 0);
                    $ret = $obj_Meeting->update($data, $stop_where);
                    if (DB::isError($ret)) {
                        $this->logger2->error($ret->getUserInfo());
                        return false;
                    }
                // 利用あり
                } else {
                    print "利用が有るため通常の会議終了\n";
                    $this->logger2->info($row["meeting_key"], "会議終了：利用が有るため通常の会議終了");
                    // イントラFMSの場合は録画容量を取得しない
                    if ($row["intra_fms"]) {
                        $obj_CoreMeeting->stopMeeting($row["meeting_key"], true, false);
                    } else {
                        $obj_CoreMeeting->stopMeeting($row["meeting_key"], null, false);
                    }
                }
                new dBug($row);
            }
        }
    }

    /**
     * 終了時刻をすぎた参加者が０人の予約会議を取得
     *
     * @todo 自分のユーザーだけに絞って処理を最小限にするか、全ユーザーの会議を対象にするか検討中
     */
    function _getNoEndMeeting($dsn, $date = null) {
        // 全件を処理すると負荷が怖いので、10件ずつ処理させることにした。永久ループの対策でもある。
        static $i;
        if ($i >= 10) {
            return false;
        }
        if (!$date) {
            $date = date('Y-m-d H:i:s');
        }
        $i++;
        $obj_Meeting = new DBI_Meeting( $dsn );
        $sql = "SELECT meeting.meeting_key" .
                ", meeting.intra_fms" .
                ", COUNT(participant.participant_key) AS cnt" .
                " FROM meeting LEFT JOIN participant" .
                " ON meeting.meeting_key = participant.meeting_key" .
                " AND participant.is_active = 1" .
                " WHERE meeting_stop_datetime < '".$date."'" .
                " AND meeting.is_active = 1" .
                " AND meeting.is_reserved = 1" .
                " AND meeting.is_deleted = 0" .
                " GROUP BY meeting.meeting_key, meeting.intra_fms" .
                " HAVING cnt = 0" .
                " LIMIT 1";
        $row = $obj_Meeting->_conn->getRow($sql, DB_FETCHMODE_ASSOC);
        if (DB::isError($row)) {
            $this->logger->error($row->getUserInfo());
            return false;
        }
        if ($row) {
            return $row;
        } else {
            return false;
        }
    }

     /**
     * 終了時刻をすぎた参加者が０人の予約会議を取得
     *
     * @todo 自分のユーザーだけに絞って処理を最小限にするか、全ユーザーの会議を対象にするか検討中
     */
    function _getNoEndMeetingList($dsn, $start, $end) {
        $sql = "SELECT meeting.meeting_key" .
                ", meeting.room_key" .
                ", meeting.is_active" .
                ", meeting.is_reserved" .
                ", meeting.is_deleted" .
                ", meeting.meeting_start_datetime" .
                ", meeting.meeting_stop_datetime" .
                ", COUNT(participant.participant_key) as cnt" .
                " FROM meeting LEFT JOIN participant" .
                " ON meeting.meeting_key = participant.meeting_key" .
                " AND participant.is_active = 1" .
                " WHERE meeting_stop_datetime BETWEEN '".$start."' AND '".$end."'" .
                " AND meeting.is_active = 1" .
                " AND meeting.is_reserved = 1" .
                " AND meeting.is_deleted = 0" .
                " GROUP BY meeting.meeting_key" .
                ", meeting.room_key" .
                ", meeting.is_active" .
                ", meeting.is_reserved" .
                ", meeting.is_deleted" .
                ", meeting.meeting_start_datetime" .
                ", meeting.meeting_stop_datetime" .
                " HAVING cnt = 0";
        $obj_Meeting = new DBI_Meeting( $dsn );
        $rs = $obj_Meeting->_conn->query($sql);
        if (DB::isError($rs)) {
            print $rs->getUserInfo();
        }
        new dBug($rs->result);
    }

    function html_header() {
        print '<html>';
        print '<head><title>日時バッチ</title></head>';
        print '<body>';
        print '<pre>';
        print '<a href="daily.php">トップ</a> | ';
        print '<a href="?action_stop_meeting_list">終わらない会議リスト</a> | ';
        print '<a href="?action_stop_meeting">終わらない会議を終了</a><hr size="1">';
    }
}

$main = new AppDailyBatch();
$main->execute();
?>