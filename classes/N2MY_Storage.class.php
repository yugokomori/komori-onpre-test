<?php

require_once("classes/core/dbi/Storage.dbi.php");
require_once('classes/core/dbi/Meeting.dbi.php');
require_once("classes/dbi/storage_file.dbi.php");
require_once("classes/dbi/storage_folder.dbi.php");
require_once("classes/dbi/storage_favorite.dbi.php");
require_once("classes/core/dbi/DocumentConvertProcess.dbi.php");
require_once("classes/core/dbi/ConvertFile.dbi.php");
require_once("lib/EZLib/EZUtil/EZImage.class.php");
require_once("lib/EZLib/EZCore/EZLogger.class.php");
require_once("lib/EZLib/EZUtil/EZString.class.php");
require_once("lib/EZLib/EZUtil/EZFile.class.php");
require_once("lib/EZLib/EZUtil/EZDate.class.php");
require_once("lib/EZLib/EZMail/EZSmtp.class.php");
require_once("lib/EZLib/EZHTML/EZTemplate.class.php");
require_once("lib/pear/File/Archive.php");
require_once("classes/N2MY_DBI.class.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/dbi/member.dbi.php");
require_once("classes/core/dbi/Document.dbi.php");


class N2MY_Storage {

    var $objProcess            = null;
    var $doc_dir               = null;
    var $dsn                   = null;
    var $dbHost                = "";
    var $logger                = "";
    var $_objStorage = null;
    var $_objStorageFolder     = null;
    var $_obj_N2MYDB           = null;
    var $_objStorageFavorite   = null;
    var $_objFile              = null;
    var $_obj_Document          = null;


    function __construct($dsn, $dbHost = null) {
        $this->dbHost                     = $dbHost;
        $this->logger                     = EZLogger2::getInstance();
        $this->objProcess                 = new DBI_DocumentConvertProcess(N2MY_MDB_DSN);
        $this->objConvertFile             = new DBI_ConvertFile(N2MY_MDB_DSN);
        $this->_objStorage      = new DBI_StorageFile($dsn);
        $this->_objWhiteboardStorageTable = new StorageTable($dsn);
        $this->_objStorageFolder          = new StorageFolderTable($dsn);
        $this->_obj_N2MYDB                = new N2MY_DB($dsn);
        $this->_objStorageFavorite        = new StorageFavoriteTable($dsn);
        $this->_obj_Document              = new DBI_Document($dsn);
        $this->_objFile                   = new EZFile();
        $this->dsn                        = $dsn;
    }

    /**
     * 資料情報取得
     */
    function getStorageList($options = array(), $addrowStatus = false) {
        if (!$where = $this->_getWhere($options, $addrowStatus)) {
            return array();
        }
        $limit  = isset($options["limit"])  ? $options["limit"]  : null;
        $offset = isset($options["offset"]) ? $options["offset"] : null;

        if (isset($options["sort_key"])) {
            $sort = array($options["sort_key"] => "desc");
        } else {
            $sort = array();
        }
        $this->logger->info($options);

        $memberKey = $options["member_key"];

        if ("favorite" == $options["search_type"]) {
            $favorite = $this->_getWhereFavorite($memberKey);
        }
        if ("folder" == $options["search_type"]) {
            $folder = $this->_getWhereFolder($options["folder_id"], $options["position"], $memberKey);
        }

        $ws = "whiteboard_storage.";
        $columns = "DISTINCT " . $ws."storage_id, " .
                    $ws."storage_key, " .
                    $ws."user_key, " .
                    $ws."member_key, " .
                    $ws."storage_path, ".
                    $ws."storage_file_name, ".
                    $ws."storage_name, ".
                    $ws."storage_file_format, ".
                    $ws."storage_file_version, ".
                    $ws."storage_file_size, ".
                    $ws."storage_file_category, ".
                    $ws."storage_file_extension, ".
                    $ws."storage_status, ".
                    $ws."storage_index, ".
                    $ws."storage_sort, ".
                    $ws."storage_transmitted, ".
                    $ws."storage_owner, ".
                    $ws."create_datetime, ".
                    $ws."update_datetime, ".
                    "member.member_id";

        $this->logger->info($columns);

        $whiteboardStorageList = $this->getList($where, $sort, $limit, $offset, $columns, $favorite, $folder);

        $this->logger->info($whiteboardStorageList);
        if (DB::isError($whiteboardStorageList)) {
            $this->logger->error($whiteboardStorageList->getUserInfo());
            return false;
        }

        return $whiteboardStorageList;
    }

    function getFolderFirstNode($options = array()) {
        $this->logger->info($options);

        $ret = $this->_objStorageFolder->getRoot($options);
        $this->logger->info($ret);
        if (DB::isError($ret)) {
            $this->logger->error($ret->getMessage());
            return array("error" => "db");
        }
        return $ret;
    }

    public function getFolderNode($options = array()) {
        $this->logger->info($options);

        if (empty($options["member_key"])) $options["member_key"] = 0;

        $ret = $this->_objStorageFolder->getChildrenNode($options);
        $this->logger->info($ret);
        if (DB::isError($ret)) {
            $this->logger->error($ret->getMessage());
            return array("error" => "db");
        }

        return $ret;
    }

    public function getPartNode($options = array()) {
        $this->logger->info($options);

        $ret = $this->_objStorageFolder->getPartNode($options);
        $this->logger->info($ret);
        if (DB::isError($ret)) {
            $this->logger->error($ret->getMessage());
            return array("error" => "db");
        }

        return $ret;
    }

    public function getLeaf($options = array()) {
        $this->logger->info($options);

        if (empty($options["member_key"])) $options["member_key"] = 0;

        $ret = $this->_objStorageFolder->getLeaf($options);
        $this->logger->info($ret);
        if (DB::isError($ret)) {
            $this->logger->error($ret->getMessage());
            return array("error" => "db");
        }

        return $ret;
    }

    public function getParentOfChildrenNode($options = array()) {
        $this->logger->info($options);

        $ret = $this->_objStorageFolder->getParentOfChildrenNode($options);
        $this->logger->info($ret);
        if (DB::isError($ret)) {
            $this->logger->error($ret->getMessage());
            return array("error" => "db");
        }

        return $ret;
    }

    public function getPresenceFolderNode($options = array()) {
        $this->logger->info($options);

        if (empty($options["member_key"])) $options["member_key"] = 0;

        $ret = $this->_objStorageFolder->getPresenceChildrenNode($options);
        $this->logger->info($ret);
        if (DB::isError($ret)) {
            $this->logger->error($ret->getMessage());
            return array("error" => "db");
        }

        return $ret;
    }



    function getOneFolder($folderId = null) {
        $where = "folder_id = '".addslashes($folderId)."'";
        $row = $this->_objStorageFolder->getRow($where);

        return $row;
    }

    function updateFolderName($folderId, $folderName, $userKey) {
        $where  = "folder_id       = '".addslashes($folderId)."'";
        $where .= " AND user_key    = '".addslashes($userKey)."'";
        $this->logger->info($where);

        $data = array("folder_name"     => $folderName,
                      "update_datetime" => date("Y-m-d H:i:s"));
        $this->logger->info($data);

        $ret = $this->_objStorageFolder->update($data, $where);
        $this->logger->info($ret);
    }

    public function deleteFolder_ander($folderId, $options = array()) {

        $error     = array();
        $userKey   = $options["user_key"];
        $userKey   = $options["user_key"];
        $memberKey = $options["member_key"];
        $error     = array();

        $where = "whiteboard_storage.user_key = '" . $userKey . "'";
        if (!empty($options["member_key"])) {
            $where .= " AND whiteboard_storage.member_key = '" . $memberKey . "'";
            $where .= " AND whiteboard_storage.storage_id = storage_folder_doc.storage_id";
            $where .= " AND whiteboard_storage.storage_status != 1";
        }
        $options["folder_id"] = $folderId;
        $this->logger->info($options);

        $resultNode = array_filter($this->getParentOfChildrenNode($options));

        $this->logger->info($resultNode);

        for ($i = 0; $i < count($resultNode); $i++) {
            $rowColumns = $resultNode[$i];
            $this->logger->info($rowColumns);
            $where .= " AND folder_id = '" . $rowColumns["children_folder_id"] . "'";
            $this->logger->info($where);

            $storageList = $this->getFolderStorageList($where);
            $this->logger->info($storageList);

            foreach ($storageList as $column) {
                $storageId = $column["storage_id"];
                $this->delete($storageId, $userKey, $memberKey, $column["member_key"]);

                $this->modifyCharges(
                    $userKey, $memberKey, $this->getStorageFileSize($storageId), false);
            }
        }
        $folderRow = $this->getFolderRow($folderId);
        $this->logger->info($folderRow);

        $result = $this->deleteFolderNode($folderId);
        $this->logger->info($result);

        if (!empty($result)) {
            $error[] = array("name" => $folderRow["folder_name"], "message" => $result["error"]);
        }
        if (!empty($error)) {
            return $error;
        }
        return "";
    }

    function deleteFolderNode($value) {
        $ret = $this->_objStorageFolder->deleteNode($value);
        $this->logger->info($ret);

        if (DB::isError($ret)) {
            $this->logger->error($ret->getMessage());
            return array("error" => "db");
        }
    }

    public function modifyFolderDoc($options = array()) {
        $list    = array();
        $message = array();

        $this->logger->info(count($options["add_folder_ids"]));
        if (0 < count($options["add_folder_ids"])) {
            $addRet = $this->addFolderDoc($options);
            if (!empty($addRet["error"])) {
                $message = $addRet["error"];
            }
            $list = array("add" => $addRet["list"]);
            $this->logger->info($list);
        }

        $this->logger->info(count($options["del_folder_ids"]));
        if (0 < count($options["del_folder_ids"])) {
            $delRet = $this->deleteFolderDoc($options);
            $this->logger->info($delRet);
            if (!empty($delRet["error"])) {
                $message = array_merge($message, $delRet["error"]);
            }
            $list = array_merge($list, array("del" => $delRet["list"]));
            $this->logger->info($list);
        }
        $this->logger->info($list);
        return array("list" => $list, "error" => $message);
    }
    private function addFolderDoc($options = array()) {
        $userKey       = $options["user_key"];
        $memberKey     = $options["member_key"];
        $storageIds    = $options["storage_ids"];
        $addFolderIds  = $options["add_folder_ids"];
        $delFolderIds  = $options["del_folder_ids"];
        $list          = array();
        $message       = array();

        $this->logger->info($storageIds);
        $this->logger->info($addFolderIds);

        foreach ($addFolderIds as $FolderKey => $folderId) {
            $this->logger->info($folderId);
            if (array_search($folderId, $delFolderIds)) {
                continue;
            }
            foreach ($storageIds as $storageKey => $storageId) {
                $this->logger->info($storageId);

                $where = "storage_folder_doc.user_key = '" . $userKey . "'";
                if (!empty($options["member_key"])) {
                    $where .= " AND storage_folder_doc.member_key = '" . $memberKey . "'";
                    $where .= " AND storage_folder_doc.folder_id  = '" . $folderId . "'";
                    $where .= " AND storage_folder_doc.storage_id = '" . $storageId . "'";
                }
                if ($this->_objStorageFolderDoc->getRow($where)) {
                    continue;
                }
                $option = array(
                    "folder_id"     => $folderId,
                    "storage_id"    => $storageId,
                    "member_key"    => $memberKey,
                    "user_key"       => $userKey
                );
                $this->logger->info($option);
                $ret = $this->_objStorageFolderDoc->add($option);
                $this->logger->info($ret);
                if (DB::isError($ret)) {
                    $this->logger->info("error" . $ret);
                    $this->logger->error($ret->getMessage());
                    $message[] = array("message" => "db");
                    continue;
                }
                $list[] = array("storage_name" => $this->getStorageName($storageId),
                                "folder_id"    => $folderId,
                                "folder_name"  => $this->getFolderName($folderId));
            }
        }
        $this->logger->info($list);
        return array("list" => $list, "error" => $message);
    }

    private function deleteFolderDoc($options = array(), $where) {
        $userKey       = $options["user_key"];
        $memberKey     = $options["member_key"];
        $storageIds    = $options["storage_ids"];
        $delFolderIds  = $options["del_folder_ids"];
        $addFolderIds  = $options["add_folder_ids"];
        $list          = array();
        $message       = array();

        $this->logger->info($storageIds);
        $this->logger->info($delFolderIds);

        foreach ($delFolderIds as $folderKey => $folderId) {
            $this->logger->info($folderId);
            if (array_search($folderId, $addFolderIds)) {
                continue;
            }

            foreach ($storageIds as $storageKey => $storageId) {
                $this->logger->info($storageId);

                $where = "storage_folder_doc.user_key = '" . $userKey . "'";
                if (!empty($options["member_key"])) {
                    $where .= " AND storage_folder_doc.member_key = '" . $memberKey . "'";
                    $where .= " AND storage_folder_doc.folder_id  = '" . $folderId . "'";
                    $where .= " AND storage_folder_doc.storage_id = '" . $storageId . "'";
                }
                if (!$this->_objStorageFolderDoc->getRow($where)) {
                    continue;
                }
                $this->logger->info($where);
                $ret = $this->_objStorageFolderDoc->remove($where);
                $this->logger->info($ret);
                if (DB::isError($ret)) {
                    $this->logger->info("error" . $ret);
                    $this->logger->error($ret->getMessage());
                    $message[] = array("message" => "db");
                    continue;
                }
                $list[] = array("storage_name" => $this->getStorageName($storageId),
                                "folder_id"    => $folderId,
                                "folder_name"  => $this->getFolderName($folderId));

            }
        }
        $this->logger->info($list);
        return array("list" => $list, "error" => $message);
    }

     /**
     * 指定した条件の件数を取得
     */
    public function getCount($options = array()) {
        if (!$where = $this->_getWhere($options)) {
            return 0;
        }
        $limit  = isset($options["limit"])  ? $options["limit"]  : null;
        $offset = isset($options["offset"]) ? $options["offset"] : null;

        if ("favorite" == $options["search_type"]) {
            $favorite = $this->_getWhereFavorite($options["member_key"]);
        }

        $count = $this->numRows($where, $favorite);
        return $count;
    }
    /**
     * 条件文取得
     */
    private function _getWhere($options = "", $addrowStatus = false) {
        $userKey = $options["user_key"];
        $memberKey = $options["member_key"];

        if (empty($userKey) && empty($memberKey)) {
            $this->logger->error(array($userKey, $memberKey), "Parameter error!!");
            return false;
        }

        $cond = array();
        $cond[] = "whiteboard_storage.user_key = '".addslashes($userKey)."'";

        if(empty($memberKey)){
            $cond[] = "whiteboard_storage.member_key = 0";
        } else {
            $cond[] = "(whiteboard_storage.member_key = 0 OR whiteboard_storage.member_key = '".addslashes($memberKey)."')";
        }

        $searchKeyword = $options["search_keyword"];
        $formatKeyword = addslashes("%".$searchKeyword."%");

        if(!empty($searchKeyword)) {
            $cond[] = "(whiteboard_storage.storage_name LIKE '".$formatKeyword."'"
                ." OR whiteboard_storage.storage_file_format LIKE '".$formatKeyword."'"
                ." OR whiteboard_storage.storage_file_category LIKE '".$formatKeyword."'"
                ." OR whiteboard_storage.storage_file_extension LIKE '".$formatKeyword."')";
        }

        $searchType = $options["search_type"];

        if(!empty($searchType)) {
            switch ($searchType) {
                case "owner":
                    $cond[] = "whiteboard_storage.storage_owner = '".addslashes($memberKey)."'";
                    break;
                case "memo":
                    $cond[] = "whiteboard_storage.meeting_key != NULL";
                    break;
                case "document":
                    $cond[] = "whiteboard_storage.storage_file_category   = '".addslashes('document')."'";
                    break;
                case "image":
                    $cond[] = "whiteboard_storage.storage_file_category   = '".addslashes('image')."'";
                    break;
                case "pdf":
                    $cond[] = "whiteboard_storage.storage_file_extension  = '".addslashes('pdf')."'";
                    break;
                case "ppt":
                    $cond[] = "whiteboard_storage.storage_file_extension  = '".addslashes('ppt')."'";
                    break;
                case "xls":
                    $cond[] = "(whiteboard_storage.storage_file_extension = '".addslashes('xls')."'
                    OR whiteboard_storage.storage_file_extension  = '".addslashes('xlsx')."')";
                    break;
                case "common":
                    $cond[] = "(whiteboard_storage.member_key = 0)";
                    break;
                case "private":
                    $cond[] = "(whiteboard_storage.member_key = '".addslashes($memberKey)."')";
                    break;
            }
        }

        $cond[] = "whiteboard_storage.storage_status != 3";

        $where = join(" AND ", $cond);
        $this->logger->info($where);

        return $where;
    }

    function _getWhereFavorite($memberKey) {
            $favorite = " AND storage_file.storage_file_key = storage_favorite_file.storage_file_key"
                      . " AND storage_file.user_key   = storage_favorite_file.user_key";
            if (!empty($memberKey)) {
                $favorite .= " AND storage_favorite_file.member_key = '".addslashes($memberKey)."'";
            } else {
                $favorite .= " AND storage_favorite_file.member_key = 0";
            }
            return $favorite;
    }

    private function _getWhereFolder($node, $position, $memberKey) {
        $folder = " AND whiteboard_storage.storage_id = storage_folder_doc.storage_id";

        if ("root" != $position) {
            $folder .= " AND storage_folder_doc.folder_id = '".$node."'";
        }
        if (!empty($memberKey)) {
            return $folder .= " AND storage_folder_doc.member_key = '".addslashes($memberKey)."'";
        } else {
            return $folder .= " AND storage_folder_doc.member_key = 0";
        }
    }

    function getMessage($lang, $group, $id = "") {
        static $message;
        if (!$message[$lang]) {
            $configFile = N2MY_APP_DIR."config/lang/".$lang."/message.ini";
            $message[$lang] = parse_ini_file($configFile, true);
        }
        if (!$id) {
            return $message[$lang][$group];
        }

        return $message[$lang][$group][$id];
    }

    public function download($storageKey, $storage_conversion_status, $userKey = null, $memberKey = null)
    {
        if (empty($storageKey)) {
            return false;
        }
        if (empty($userKey) && empty($memberKey)) {
            return array();
        }
        $this->downloadWhiteboardStorage($storageKey, $storage_conversion_status, $userKey, $memberKey); // 更新
    }

    function getUserId($memberKey = null, $userKey = null) {
        $objUser = new UserTable($this->dsn);
        if ($userKey) {
            $result =  $objUser->find($userKey);
        } else {
            $objMember = new MemberTable($this->dsn);
            $member_row = $objMember->getDetail($memberKey);
            $result = $objUser->find($member_row["user_key"]);
        }

        return $result["user_id"];
    }

    function getMemberId($memberKey = null) {
        $objMember = new MemberTable($this->dsn);
        $result = $objMember->getDetail($memberKey);

        return $result["member_id"];
    }

    private function getFolderDoc($option = null) {
        $where  = "folder_id     = '".addslashes($option["folder_id"])."'";
        $where .= "storage_id    = '".addslashes($option["storage_id"])."'";
        $where .= "user_key      = '".addslashes($option["user_key"])."'";
        $where .= "member_key    = '".addslashes($option["member_key"])."'";
        $this->logger->info($where);
        $row = $this->_objStorageFolderDoc->getList($where);
        $this->logger->info($row);

        return $row;
    }

    function getFolderId($storageId = null) {
        $where = "storage_id = '".addslashes($storageId)."'";
        $this->logger->info($where);
        $row = $this->_objStorageFolderDoc->getList($where);
        $this->logger->info($row);

        return $row;
    }

    function getFolderRow($folderId = null) {
        $where = "folder_id = '".addslashes($folderId)."'";
        //$this->logger->info($where);
        $row = $this->_objStorageFolderDoc->getList($where);
        return $row;
    }

    function getFolderInfoByexternal($where){
        $row = $this->_objStorageFolder->getRow($where);
        return $row;
    }

    function getFolderStorageId($option) {
        $where  = "folder_id  = '".addslashes($option["folder_id"])."'"
                . "user_key   = '".addslashes($option["user_key"])."'"
                . "member_key = '".addslashes($option["member_key"])."'";
        $this->logger->info($where);
        $row = $this->_objStorageFolderDoc->getList($where);
        $this->logger->info($row);

        return $row;
    }

    function getStorageInfo($storage_id = null) {
        $where = "storage_file_key = '".addslashes($storage_id)."'";
        $row = $this->_objStorage->getRow($where);
        //$this->logger->info($row);

        return $row;
    }
    function getStorageInfoByDocumentID($document_id = null, $user_key) {
    	if(substr($document_id, 0, 3) === 'MC_') {
    		$where = "clip_key = '".addslashes($document_id)."'";
    	} else {
    		$where = "document_id = '".addslashes($document_id)."'";
    	}
    	$where .= "AND user_key = '".addslashes($user_key)."'";
    	$row = $this->_objStorage->getRow($where);    
    	return $row;
    }

    function getStorageKey($storage_id = null) {
        $where = "storage_file_key = '".addslashes($storage_id)."'";
        $row = $this->_objWhiteboardStorage->getRow($where);
        $this->logger->info($row);

        return $row["storage_key"];
    }

    function getStorageName($storageId = null) {
        $where = "storage_file_key = '".addslashes($storageId)."'";
        $row = $this->_objWhiteboardStorage->getRow($where);
        $this->logger->info($row);

        return $row["storage_name"];
    }

    function getStorageFileSize($storage_id = null) {
        $where = "storage_file_key = '".addslashes($storage_id)."'";
        $row = $this->_objWhiteboardStorage->getRow($where);
        $this->logger->info($row);

        return $row["storage_file_size"];
    }

    function getStorageDatetime($storageKey = null) {
        $where = "storage_file_key = '".addslashes($storageKey)."'";
        $row = $this->_objStorage->getRow($where);
        $this->logger->info($row);

        return $row["update_datetime"];
    }

    function getStorageOwner($storageKey = null) {
        $row = $this->getDetail($storageKey);
        $this->logger->info($row);

        return $row["storage_owner"];
    }

    /**
     * 資料追加 TODO 小森修正中
     */
    function add($user_key , $member_key , $file_path , $file_name , $file_size, $format, $category, $extension, $version, $document_id = null, $clip_key = null, $personal_white_id = null , $storage_folder_key = 0 , $page_count = 0 , $external_service_file_key = null) {

        if($category == "personal_white"){
            $status = 2;
        }else if ($category == "image" || $category == "document"){
            $status = 0;
            $page_count = 1;
            if(!$document_id){
                $status = -1;
            }
        }else{
            $status = 0;
            $page_count = 1;
        }

        $external_flg = 0;
        if($external_service_file_key != null){
            $external_flg = 1;
        }

        $data = array(
            "document_id"                => $document_id,
            "clip_key"                   => $clip_key,
            "user_key"                   => $user_key,
            "personal_white_id"          => $personal_white_id,
            "member_key"                 => $member_key,
            "owner"                      => $member_key,
            "file_path"                  => $file_path,
            "file_name"                  => $file_name,
            "user_file_name"             => $file_name,
            "file_size"                  => $file_size,
            "format"                     => $format,
            "category"                   => $category,
            "extension"                  => $extension,
            "version"                    => $version,
            "document_index"             => $page_count,
            "status"                     => $status,
            "storage_folder_key"         => $storage_folder_key,
            "external_service_file_key"  => $external_service_file_key,
            "external_flg"               => $external_flg,
            "last_update_member_key"     => $member_key,
            "create_datetime"            => date("Y-m-d H:i:s"),
            "update_datetime"            => date("Y-m-d H:i:s")
        );

//        $this->logger->info($data);
        $ret = $this->_objStorage->add($data);
        if (DB::isError($ret)) {
            $this->logger->info($ret->getMessage());
            return false;
        }
    }

    /**
     * ファイルは物理削除、DBは論理削除
     */
    /*
    function delete($storageId, $userKey, $memberKey, $terroitory) {
        $storageKey = $this->getStorageKey($storageId);
        $convert = $this->getStatus($storageKey);
        $this->logger->info($convert);
        if (isset($convert["status"])) {
            $accountPath = $this->getDeleteAccountPath($userKey, $memberKey, $terroitory);

            if (STORAGE_STATUS_WAIT == $convert["status"]) {
            $this->logger->info($convert["status"]);
                // 変換前の場合
                $this->_deleteOriginFile($storageKey, $accountPath);
            } else {
                // 変換前後のファイルを削除
                $file = N2MY_DOC_DIR.$accountPath.$storageKey.".".$convert["extension"];
                $dir  = N2MY_DOC_DIR.$accountPath.$storageKey;
                $this->logger->info($dir);

                $objFile = $this->_objFile;
                $objFile->rmtree($dir);

                if (file_exists($file)) {
                    $this->logger->info($file);
                    unlink($file);
                }
            }
            $this->setStatus($storageKey, STORAGE_STATUS_DELETE, $userKey, $terroitory, $memberKey, $storageId);
            $this->deleteRowFolderDoc($userKey, $memberKey, $storageId);
            $this->deleteRowFavorite($storageKey, $userKey, $memberKey, $storageId);
            return true;
        } else {
            // convert無し時の削除
            $this->setStatus($storageKey, STORAGE_STATUS_DELETE, $userKey, $terroitory, $memberKey, $storageId);
            $this->deleteRowFavorite($storageKey, $userKey, $memberKey, $storageId);
            return false;
        }
    }
*/

    /**
     * ファイル削除 TODO
     * @param unknown_type $storageKey
     * @param unknown_type $userKey
     * @param unknown_type $memberKey
     */
    function delete_file($storageKey , $userKey , $memberKey){
        $this->logger->info("DELETE STORAGE FILE :" . $storageKey);
        $file_info = $this->getStorageInfo($storageKey);
        $user_id = $this->getUserId($memberKey,$userKey);

        // DB倫理削除 ストレージテーブル
        $this->deleteRowFavorite($storageKey, $userKey, $memberKey);
        $result = $this->setStatus($storageKey, STORAGE_STATUS_DELETE,$memberKey);
        if($file_info["category"] == "image" || $file_info["category"] == "document"){
            $data = array();
            $data["document_status"] = STORAGE_STATUS_DELETE;
            $data["update_datetime"] = date("Y-m-d H:i:s");
            $where = sprintf("document_id = '%s' AND document_mode = '%s'", $file_info["document_id"] , "storage");
            $this->_obj_Document->update($data, $where);

            // 実ファイル削除
            $dir =  N2MY_DOC_DIR.$file_info["file_path"].$file_info["document_id"];
            if(file_exists($dir)){
                $this->_objFile->rmtree($dir);
            }
        }else if($file_info["category"] == "personal_white"){
            // 実ファイル削除
            $file_path =  N2MY_DOC_DIR.$file_info["file_path"].$file_info["file_name"];
            if(file_exists($file_path)){
                unlink($file_path);
            }
        }
        return $result;
    }

    function deleteRowFolderDoc($userKey, $memberKey, $storageId) {
        $where  = "user_key        = '".addslashes($userKey)."'";
        $where .= " AND member_key = '".addslashes($memberKey)."'";
        $where .= " AND storage_id = '".addslashes($storageId)."'";
        $this->logger->info($where);
        $ret = $this->_objStorageFolderDoc->remove($where);
    }

    /**
     * お気に入り削除 TODO
     * @param unknown_type $storageKey
     * @param unknown_type $userKey
     * @param unknown_type $memberKey
     * @param unknown_type $storageId
     * @return multitype:string
     */
    function deleteRowFavorite($storageKey, $userKey, $memberKey) {
        $where         = "storage_file_key      = '".addslashes($storageKey)."'";
        $where        .= " AND user_key   = '".addslashes($userKey)."'";
        if (empty($memberKey)){
            $where    .= " AND member_key = '".addslashes(0)."'";
        } else {
            $where    .= " AND member_key = '".addslashes($memberKey)."'";
        }
        if ($this->_objStorageFavorite->getStorageKey($where)) {
            $result = $this->deleteFavorite($storageKey, $userKey, $memberKey);
            if (isset($result)) {
                $this->logger->warn("favorite notfound error");
                return array("error" => "favorite");
            }
        }
    }

    function _deleteOriginFile($storageKey, $accountPath) {
        $whiteboardStorage = $this->getStatus($storageKey);
        $this->logger->info($whiteboardStorage["status"]);

        if (isset($whiteboardStorage["status"]) && $whiteboardStorage["status"] != STORAGE_STATUS_DO) {
            $file = N2MY_DOC_DIR.$accountPath.$storageKey.".".$whiteboardStorage["extension"];
            if (file_exists($file)) {
                $this->logger->info($file);
                unlink($file);
            }
            return true;
        }
        return false;
    }

    function addQueue($db, $storage_path, $document_id, $ext, $priority, $addition = null) {
        $data = array(
            "db_host"          => $db,
            "document_path"    => $storage_path,
            "document_id"      => $document_id,
            "document_format"  => $addition["format"],
            "document_version" => $addition["version"],
            "extension"        => $ext,
            "status"           => storage_status_WAIT,
            "priority"         => $priority,
            "addition"         => serialize($addition),
            "create_datetime"  => date("Y-m-d H:i:s"),
            "update_datetime"  => date("Y-m-d H:i:s")
        );
        $this->objConvertFile->add($data);
    }

    /**
     * キューにたまったファイルを抽出
     */
    function getQueue() {
        $where = "status = 0";
        $sort = array(
            "priority" => "asc",
            "create_datetime" => "asc",
        );
        $row = $this->objConvertFile->getRow($where, "*", $sort);
        $this->logger->info($row);
        if ($row) {
            $this->setStatus($row["document_id"], STORAGE_STATUS_DO);
        }
        return $row;
    }

    /**
     * 変換中のステータス取得
     */
    function getStatus($storageKey) {
        $where = "document_id = '".$storageKey."'";
        $this->logger->info($where);
        $row = $this->objConvertFile->getRow($where);
        $this->logger->info($row);
        return $row;
    }

    /**
     * ステータスを更新 TODO
     */
    function setStatus($storageKey, $status, $memberKey = null, $document_id = null , $userKey = null, $terroitory = null) {

        $where = "storage_file_key = ". $storageKey;
        if (!empty($userKey)) {
            $where .= " AND user_key   = '".$userKey."'";
            $where .= " AND member_key = '".$terroitory."'";
        }

        // 資料ステータス
        $data = array();
        $data["status"]  = $status;
        $data["update_datetime"] = date("Y-m-d H:i:s");

        if($memberKey){
            $data["last_update_member_key"] = $memberKey;
        }

        $this->logger->info($where);
        $this->logger->info($data);
        $result = $this->_objStorage->update($data, $where);

        if (!empty($result)) {
            return false;
        }
        return true;
    }

    function getDetail($storageKey, $territory = '0') {
        $where  = "storage_file_key     = '".addslashes($storageKey)."'";
        $where .= " AND member_key = '".$territory."'";
        $this->logger->info($where);

        $row = $this->_objStorage->getRow($where);
        $this->logger->info($row);

        return $row;
    }

    function getStorageDetail($storageKey, $memberKey) {
        $where  = "storage_file_key     = '".addslashes($storageKey)."'";
        $where .= " AND member_key = '".addslashes($memberKey)."'";
        $this->logger->info($where);

        $row = $this->_objStorage->getRow($where);
        $this->logger->info($row);

        return $row;
    }

    function getRowStorage($where) {

        $row = $this->_objStorage->getRow($where);
        $this->logger->info($row);

        return $row;
    }

    function successful($storageKey, $pageCnt) {
        // ホワイトボードページ数上限
        $whiteboardPage = "";
        // 指定あり
        if ($whiteboardPage) {
            // アップロード上限チェック
            if ($whiteboardPage < $pageCnt) {
                $this->logger->warn(array($storageKey, $pageCnt, $whiteboardPage), "ページ上限オーバーにより変換しませんでした");
                $this->setStatus($storageKey, STORAGE_STATUS_PAGE_ERROR);
                return false;
            }
        }
        $whiteboardStorage = $this->getStatus($storageKey);
        $this->logger->info($whiteboardStorage);

        if ($whiteboardStorage["status"] == STORAGE_STATUS_DELETE) {
            $this->logger->info($storageKey, "削除済み");
            return false;
        }
        $this->logger->info(N2MY_DOC_DIR.$whiteboardStorage["document_path"].$storageKey);
        $baseDir = N2MY_DOC_DIR.$whiteboardStorage["document_path"].$storageKey;
        // オプション
        $addition = unserialize($whiteboardStorage["addition"]);
        $filesize = 0;
        for($i = 1; $i <= $pageCnt; $i++) {
            $filesize += filesize($baseDir."/".$storageKey."_".$i.".".$addition["ext"]);
        }
        $this->logger->info("successfulllllllllllllllllllllllllllllllllll");

        // キューの状態を正常完了
        $this->setStatus($storageKey, STORAGE_STATUS_SUCCESS);
        // ユーザDBの資料情報を更新
        $where = "storage_key = '".$storageKey."'";
        $data = array(
            "storage_index"     => $pageCnt,
            "storage_file_size" => $filesize,
            "storage_status"    => STORAGE_STATUS_SUCCESS,
            "update_datetime"   => date("Y-m-d H:i:s")
        );
        $this->_objWhiteboardStorage->update($data, $where);

        $this->logger->info($addition["success_url"]);

        // コールバックURL
        if (isset($addition["success_url"])) {
            $ch = curl_init();
            $option = array(
                CURLOPT_URL => $addition["success_url"],
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_CONNECTTIMEOUT => 1,
                CURLOPT_TIMEOUT => 1,
                );
            curl_setopt_array($ch, $option);
            $this->logger->info($option);
            $ret = curl_exec($ch);
        }
    }

    function error($storageKey, $status) {
        $whiteboardStorage = $this->getStatus($storageKey);
        $baseDir = N2MY_DOC_DIR.$whiteboardStorage["document_path"].$storageKey;
        $addition = unserialize($whiteboardStorage["addition"]);
        // キューのステータス
        $where = "document_id = '".$storageKey."'";
        $data = array(
            "status"          => STORAGE_STATUS_ERROR,
            "update_datetime" => date("Y-m-d H:i:s")
            );
        $this->objConvertFile->update($data, $where);
        // 資料ステータス
        $data = array(
            "storage_status"  => $status,
            "update_datetime" => date("Y-m-d H:i:s")
            );
        $this->_objWhiteboardStorage->update($data, $where);
        $whiteboardStorage = $this->getStatus($storageKey);
        // コールバックURL
        if (isset($addition["error_url"])) {
            $ch = curl_init();
            $option = array(
                CURLOPT_URL => $addition["error_url"],
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_CONNECTTIMEOUT => 1,
                CURLOPT_TIMEOUT => 1,
                );
            $this->logger->info($option);

            curl_setopt_array($ch, $option);
            $ret = curl_exec($ch);
        }
    }

    function getActiveProcess() {
        $data = $this->objProcess->getRow(1);
        $this->logger->info($data);
        return $data;
    }

    function start($url) {
        $data = array(
            "status" => "1",
            "create_datetime" => date("Y-m-d H:i:s"),
            "update_datetime" => date("Y-m-d H:i:s"),
            );
        $this->objProcess->add($data);
        // 変換開始
        if ($url) {
            $ch = curl_init();
            $option = array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_CONNECTTIMEOUT => 1,
                CURLOPT_TIMEOUT => 1,
                );
            curl_setopt_array($ch, $option);
            $this->logger->info($option);
            $ret = curl_exec($ch);
        }
    }

    function keepAlive($no) {
        $where = "no = ".$no;
        $data = array(
            "update_datetime" => date("Y-m-d H:i:s")
            );
        $this->objProcess->update($data, $where);
    }

    function stop($no) {
        $where = "no = ".$no;
        $this->objProcess->remove($where);
        return true;
    }

    function check($file) {
        if (!$this->file_info($file["name"])) {
            return false;
        }
        return true;
    }

    function convertImage($input_file, $output_file, $opt) {
        $objImage = new EZImage(N2MY_IMGMGC_DIR);
        if (!file_exists(dirname($output_file))) {
            EZFile::mkdir_r(dirname($output_file), 0777);
            chmod(dirname($output_file), 0777);
        }
        // 画像変換
        $ret = $objImage->convert($input_file, $output_file, $opt["x"], $opt["y"]);
        if ($ret) {
            $this->logger->info($ret);
            return count($ret);
        } else {
            return false;
        }
    }

    function file_info($file) {
        // 拡張子でチェック
        $pathParts = pathinfo($file);
        $pathParts['extension'] = strtolower($pathParts['extension']);
        $config = EZConfig::getInstance();
        $imageExt = $config->getAll("DOCUMENT_IMAGES");

         $this->logger->info($pathParts['extension']);

        // 画像
        foreach($imageExt as $type => $extensions) {
         $this->logger->info(split(",", $extensions));
            $_extensions = split(",", $extensions);
            if (in_array($pathParts['extension'], $_extensions)) {
                $pathParts["type"] = "image";
                return $pathParts;
            }
        }
        // 資料
        $whiteboardStorage_ext = $config->getAll("DOCUMENT_FILES");
        foreach($whiteboardStorage_ext as $type => $extensions) {
         $this->logger->info(split(",", $extensions));
            $_extensions = split(",", $extensions);
            if (in_array($pathParts['extension'], $_extensions)) {
                $pathParts["type"] = "document";
                return $pathParts;
            }
        }
        return false;
    }

    public function getPreviewFileInfo( $info, $index = 1)
    {
        $fileInfo = array();
        $this->logger->info($info);
        switch ($info["status"]) {
            case "1":    //変換中
                $fileInfo = $this->_getConvertingFileInfo( $info, $index );
                break;

            case "2":    //正常完了
                $fileInfo = $this->_getPreviewFileInfo( $info, $index );
                break;

            case "3":    //削除
            default :    //失敗（-1）
                $fileInfo = $this->_getVoidImageInfo();
                break;
        }
        $this->logger->info($fileInfo);
        return $fileInfo;
    }

    /**
     * サポートする拡張子をconfigファイル取得
     * TODO 小森修正中
     * @return multitype:unknown multitype:
     */
    public function getSupportFormat() {
        static $room_list;
        // アプリで有効なフォーマット一覧
        $config       = EZConfig::getInstance();
        $document_ext = $config->getAll('DOCUMENT_FILES');
        $imageExt     = $config->getAll('DOCUMENT_IMAGES');
        $videoExt     = $config->getAll('VIDEO_EXTENSION');

        // 資料
        foreach($document_ext as $type => $extensions) {
            $_extensions = split(",", $extensions);
            $documentList[$type] = $_extensions;
            foreach($_extensions as $ext) {
                $filetype[]           = $ext;
                $documentExtList[]    = $ext;
            }
        }
        // 画像
        foreach($imageExt as $type => $extensions) {
            $_extensions = split(",", $extensions);
            $imageList[$type] = $_extensions;
            foreach($_extensions as $ext) {
                $filetype[]         = $ext;
                $imageExtList[]   = $ext;
            }
        }

        // 動画
        foreach($videoExt as $type => $extensions) {
            $_extensions = split(",", $extensions);
            $videoList[$type] = $_extensions;
            foreach($_extensions as $ext) {
                $filetype[]         = $ext;
                $videoExtList[]   = $ext;
            }
        }

        $userList = array(
            'support_ext_list'  => $filetype,
            'document_list'     => $documentList,
            'document_ext_list' => $documentExtList,
            'image_list'        => $imageList,
            'image_ext_list'    => $imageExtList,
            'video_list'        => $videoList,
            'video_ext_list'    => $videoExtList,
        );

        return $userList;
    }

    private function _getConvertingFileInfo()
    {
        $targetImage = N2MY_DOCUMENT_ROOT."shared/images/lb/image-converting.jpg";
        $imageInfo = getimagesize($targetImage);
        $this->logger->info($imageInfo);
        if( $imageInfo != false ){
            $return["fileName"] = "image-converting.jpg";
            $return["targetImage"] = $targetImage;
            $return["mime"] = $imageInfo["mime"];
            return $return;
        } else {
            return $this->_getVoidImageInfo();
        }
    }

    private function _getPreviewFileInfo( $info, $index )
    {
        $return = array();
        $return["fileName"] = sprintf( "%s_%d.jpg", $info["document_id"], $index );
        $targetImage = sprintf( "%s%s%s/%s", N2MY_DOC_DIR, $info["file_path"], $info["document_id"], $return["fileName"] );
        $imageInfo = getimagesize( $targetImage );

        if( $imageInfo && $imageInfo != false ){
            $return["targetImage"] = $targetImage;
            $return["mime"] = $imageInfo["mime"];
            return $return;
        } else {
            return $this->_getVoidImageInfo();
        }
    }

    private function _getVoidImageInfo()
    {
        $errorImage = N2MY_DOCUMENT_ROOT."shared/images/lb/image-error.jpg";
        $imageInfo = getimagesize($errorImage);
        $return["fileName"] = "image-error.jpg";
        $return["targetImage"] = $errorImage;
        $return["mime"] = $imageInfo["mime"];
        //$this->logger->info($return);
        return $return;
    }

    function getRowsAssoc($where = "", $sort = array(), $limit = null, $offset = 0, $columns = "*", $key = null) {
        $rs = $this->select($where, $sort, $limit, $offset, $columns);
        if (DB::isError($rs)) {
            $this->logger2->error($rs->getUserInfo());
            return array();
        }
        $ret = array();
        while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            if ($key) {
                $ret[$row[$key]] = $row;
            } else {
                $ret[] = $row;
            }
        }
        return $ret;
    }

    /**
     * ドキュメント変換処理に追加 TODO
     * @param unknown_type $filepath
     * @param unknown_type $name
     * @param unknown_type $format
     * @param unknown_type $userKey
     * @param unknown_type $memberKey
     * @return mixed
     */
    function addDocument($filepath, $name, $format, $userKey = null, $memberKey = null) {
        $documentPath = $this->getAccountPath($this->getUserId('', $userKey), $memberKey);
        //$this->logger->info($documentPath);
        if(!file_exists(N2MY_DOC_DIR.$documentPath)){
            mkdir(N2MY_DOC_DIR.$documentPath , 0777 , true);
            chmod(N2MY_DOC_DIR.$documentPath, 0777);
        }else{
            chmod(N2MY_DOC_DIR.$documentPath, 0777);
        }

        $url = N2MY_LOCAL_URL."/api/document/index.php";
        $postData = array(
            "action_accept" => "",
            "db_host"       => $this->dbHost,
            "user_key"      => $userKey,
            "user_id"       => $this->getUserId('', $userKey),
            "member_key"    => $memberKey,
            "name"          => $name,
            "format"        => $format,
            "document_path" => $documentPath,
            "file"          => "@".$filepath,
            "db_data"       => "storage",
            "sort"          => 1,
            );
        $ch = curl_init();
        $option = array(
            CURLOPT_URL            => $url,
            CURLOPT_POST           => 1,
            CURLOPT_POSTFIELDS     => $postData,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT        => 5,
            );
        //$this->logger->info($option);
        curl_setopt_array($ch, $option);
        $ret = curl_exec($ch);
        return $ret;
    }

    function getCopyAccountPath($userId, $memberKey, $selectedMemberKey) {
        if (empty($selectedMemberKey)) {
            return $userId.'/storage/'.$memberKey.'/';
        }
        return $userId.'/storage/';
    }

    function getAccountPath($userId, $memberKey) {
        if (empty($memberKey)) {
            return $userId.'/storage/';
        }
        return $userId.'/storage/'.$memberKey.'/';
    }

    function getDeleteAccountPath($userId, $memberKey, $terroitory) {
        if (empty($terroitory)) {
            return $userId.'/';
        }
        return $userId.'/'.$memberKey.'/';
    }

    function isSelectedFile($storageKey, $userKey, $memberKey, $territory) {
        $this->logger->info($storageKey);

        if (empty($territory)) {
            $setMemberKey = "0";
        } else {
            $setMemberKey = $memberKey;
        }
        $where  = "storage_key         = '".addslashes($storageKey)."'";
        $where .= " AND user_key       = '".addslashes($userKey)."'";
        $where .= " AND member_key     = '".addslashes($setMemberKey)."'";
        $where .= " AND storage_status != '3'";

        $this->logger->info($where);
        $row = $this->_objWhiteboardStorage->getRow($where);
        $this->logger->info($row);
        $userId = $this->getUserId('', $userKey);

        // ファイルをコピー
        $newAccountPath = $this->getCopyAccountPath($userId, $memberKey, $row["member_key"]);
        $oldFile = N2MY_DOC_DIR.$row["storage_path"].$storageKey.".".$row["storage_file_extension"];

        $this->logger->info($row["storage_path"]);
        $this->logger->info($newAccountPath);

        $newFile = N2MY_DOC_DIR.$newAccountPath.$storageKey.".".$row["storage_file_extension"];
        if (file_exists($newFile)) {
            $this->logger->error("file duplicate error");
            return false;;
        }
        return true;
    }

    function getCharges($userKey, $memberKey) {
        // 共有用
        $objUser              = new UserTable($this->dsn);
        $result               = $objUser->find($userKey);
        $commonStorageSize    = $result["whiteboard_storage_size"];
        $commonStorageMaxSize = $result["whiteboard_storage_max_size"];

        // 個人用
        if (!empty($memberKey)) {
            $objMember             = new MemberTable($this->dsn);
            $result                = $objMember->getDetail($memberKey);
            $privateStorageSize    = $result["whiteboard_storage_size"];
            $privateStorageMaxSize = $result["whiteboard_storage_max_size"];
        }
        return array(
            "commonStorageSize"     => round($commonStorageSize / 1024),
            "commonStorageMaxSize"  => round($commonStorageMaxSize / 1024),
            "privateStorageSize"    => round($privateStorageSize / 1024),
            "privateStorageMaxSize" => round($privateStorageMaxSize / 1024)
        );
    }

    function modifyCharges($userKey, $memberKey, $size, $addType = false) {

        if (!empty($memberKey)) {
            $objMember = new MemberTable($this->dsn);

            $result = $objMember->getDetail($memberKey);
            $this->logger->info($result);
            if ($addType) {
                $privateUploadSize = $result["whiteboard_storage_size"] + $size;
            } else {
                $privateUploadSize = $result["whiteboard_storage_size"] - $size;
            }
            $this->logger->info($privateUploadSize);

            $where = "member_key = '".$memberKey."'";
            $data  = array(
                "whiteboard_storage_size" => $privateUploadSize,
                "update_datetime"         => date("Y-m-d H:i:s")
            );
            $objMember->update($data, $where);
            return;
        }
        $objUser = new UserTable($this->dsn);
        $result =  $objUser->find($userKey);
        $this->logger->info($result);

        if ($addType) {
            $commonUploadSize = $result["whiteboard_storage_size"] + $size;
        } else {
            $commonUploadSize = $result["whiteboard_storage_size"] - $size;
        }

        $data = array(
            "whiteboard_storage_size" => $commonUploadSize,
            "user_updatetime"         => date("Y-m-d H:i:s")
            );

        $this->logger->info($data);

        $where = "user_key = '".$userKey."'";

        $objUser->update($data, $where);
    }

    /**
     * サイズの確認
     * @param $userKey
     * @param  $memberKey
     * @param  $size
     * @param  $addType
     * @return multitype:string
     */
    function checkCharges($userKey, $memberKey, $size, $addType = false) {

        $objUser   = new UserTable($this->dsn);
        $objMember = new MemberTable($this->dsn);
/* member ストレージがあったら追加
        if (!empty($memberKey)) {
            $result = $objMember->getDetail($memberKey);
            $this->logger->info($result);
            if ($addType) {
                $privateUploadSize = $result["whiteboard_storage_size"] + $size;
            } else {
                $privateUploadSize = $result["whiteboard_storage_size"] - $size;
            }
            $this->logger->info($privateUploadSize);

            if ($addType) {
                if ($result["whiteboard_storage_max_size"] < $privateUploadSize) {
                    $this->logger->warn("private maxsize over");
                    return array("error" => "size over");
                }
            }
            return;
        }
*/
        $result = $objUser->find($userKey);
//        $this->logger->info($result);



        $size = $this->_objStorage->get_total_size($userKey);
        //$this->logger->info($commonUploadSize);

        if ($addType) {
            if ($result["whiteboard_storage_max_size"] < $commonUploadSize) {
                $this->logger->warn("common maxsize over");
                return array("error" => "size over");
            }
        }
    }

    /**
     * 使用サイズを取得する TODO
     * @parame $user_key
     * @return int
     */
    function get_total_size($user_key){
        $storage_size = $this->_objStorage->get_total_size($user_key);
        $obj_Meeting        = new DBI_Meeting( $this->dsn );
        $meeting_log_size = $obj_Meeting->getMemberUsedSize($user_key);
        return $storage_size + $meeting_log_size;
    }

    /**
     * ファイルを物理コピー、DBをコピー 旧式
     */
/*
    function fileCopy($storageKey, $userKey, $memberKey, $territory) {
        $where .= "storage_key = '".addslashes($storageKey)."'";
        if (empty($territory)) {
            $setMemberKey = "0";
        } else {
            $setMemberKey = $memberKey;
        }
        $where .= " AND member_key     = '".addslashes($setMemberKey)."'";
        $where .= " AND storage_status != '3'";
        $this->logger->info($where);

        $row = $this->_objWhiteboardStorage->getRow($where);
        $this->logger->info($row);

        $userId = $this->getUserId('', $userKey);

        $newAccountPath = $this->getCopyAccountPath($userId, $memberKey, $row["member_key"]);

        $oldFile = N2MY_DOC_DIR.$row["storage_path"].$storageKey.".".$row["storage_file_extension"];
        $this->logger->info($oldFile);

        $newFile = N2MY_DOC_DIR.$newAccountPath.$storageKey.".".$row["storage_file_extension"];
        $this->logger->info($newFile);

        $oldDir  = N2MY_DOC_DIR.$row["storage_path"].$storageKey;
        $this->logger->info($oldDir);

        $newDir  = N2MY_DOC_DIR.$newAccountPath.$storageKey;
        $this->logger->info($newDir);

        $objFile = $this->_objFile;

        if (!file_exists($oldFile)) {
            $this->logger->warn("old copy file notfound");
            return array("error" => "old file notfound");
        } else {
            $objFile->fileCopy($oldFile, $newFile);
            if (!file_exists($newFile)) {
                $this->logger->warn("new copy file notfound");
                return array("error" => "new file notfound");
            }
        }
        if (!file_exists($oldDir)) {
            $this->logger->warn("old copy dir notfound");
            return array("error" => "old dir notfound");
        } else {
            $objFile->dirCopy($oldDir, $newDir);
            if (!file_exists($newDir)) {
                $this->logger->warn("new copy dir notfound");
                return array("error" => "new dir notfound");
            }
        }
        $copyMemberKey = 0;
        if (empty($row["member_key"])) {
            $copyMemberKey = $memberKey;
        }

        // DBをコピー
        $data = array(
            "storage_key"               => $row["storage_key"],
            "user_key"                  => $userKey,
            "member_key"                => $copyMemberKey,
            "storage_path"              => $newAccountPath,
            "storage_file_name"         => $row["storage_file_name"],
            "storage_name"              => $row["storage_name"],
            "storage_file_size"         => $row["storage_file_size"],
            "storage_file_category"     => $row["storage_file_category"],
            "storage_file_extension"    => $row["storage_file_extension"],
            "storage_file_format"       => $row["storage_file_format"],
            "storage_file_version"      => $row["storage_file_version"],
            "storage_owner"             => $memberKey,
            "storage_sort"              => $row["storage_sort"],
            "storage_status"            => $row["storage_status"],
            "create_datetime"           => date("Y-m-d H:i:s"),
            "update_datetime"           => date("Y-m-d H:i:s")
        );
        $this->logger->info($data);

        $ret = $this->_objWhiteboardStorage->add($data);
        if (DB::isError($ret)) {
            $this->logger->error($ret->getMessage());
            return array("error" => "db");
        }
    }
*/
    /**
     * ファイル名変更
     * @param int $storageKey
     * @param string $storageName
     * @param int $userKey
     */
    function updateFileName($storageKey, $storageName, $userKey) {
        $where  = "storage_file_key       = '".addslashes($storageKey)."'";
        $where .= " AND user_key     = '".addslashes($userKey)."'";

        $data = array("user_file_name"    => $storageName,
                      "update_datetime" => date("Y-m-d H:i:s"));
        $result = $this->_objStorage->update($data, $where);
        if (!empty($result)) {
            return false;
        }
        return true;
    }

    function formatStorageDatetime($datetime) {
        if (strtotime(date("Y-m-d 00:00:00")) <= strtotime($datetime)) {
        $this->logger->info($datetime);
            $datetime = date("H:i", strtotime($datetime));
        } else {
            $datetime = date("m月d日", strtotime($datetime));
        }
        $this->logger->info($datetime);
        return $datetime;
    }

    function downloadFile($storageIds, $userKey, $memberKey) {
        if (1 < count($storageIds)) {
            $files   = array();
            $objFile = $this->_objFile;
            $archive = array();
            $userId  = $this->getUserId('', $userKey);

                    foreach ($storageIds as $key => $storageId) {
                $storageKey = $this->getStorageKey($storageId);
                $where = "storage_key = '".addslashes($storageKey)."'";
                $row = $this->_objWhiteboardStorage->getRow($where);
                $this->logger->info($row);

                $newAccountPath = $this->getCopyAccountPath($userId, $memberKey, $row["member_key"]);

                $oldFile = N2MY_DOC_DIR.$row["storage_path"].$storageKey.".".$row["storage_file_extension"];
                $this->logger->info($oldFile);

                $dir = N2MY_APP_DIR."tmp/".$newAccountPath;
                $copyFile = $dir.$row["storage_file_name"];
                $this->logger->info($copyFile);

                if (!file_exists($dir)) {
                    EZFile::mkdir_r($dir, 0777);
                    chmod($dir, 0777);
                }
                $objFile->fileCopy($oldFile, $copyFile);
                $archive = array_merge($archive, array($copyFile));
            }
            File_Archive::extract(
                $archive,
                File_Archive::toArchive(
                    "download_".date(Ymd).".zip",
                    File_Archive::toOutput()
                )
            );
            $objFile->rmtree($dir);
        } else {
            $storageKey = $this->getStorageKey(array_shift($storageIds));
            $this->logger->info($storageKey);

            $where = "storage_key = '".addslashes($storageKey)."'";
            $row = $this->_objWhiteboardStorage->getRow($where);
            $file = N2MY_DOC_DIR.$row["storage_path"].$storageKey.".".$row["storage_file_extension"];
            $this->logger->info($file);

            if (!file_exists($file)) {
                $this->logger->error("download file notfound");
                return false;
            }
            if (!$fp = fopen($file, "r")) {
                $this->logger->error("download file can't open");
                return false;
            }
            fclose($fp);

            header('Content-Disposition: attachment; filename="'.$row["storage_file_name"].'"');
            header('Content-Type: application/octet-stream');

            if(!readfile($file)) {
                $this->logger->error("download file can't read");
                return false;
            }
        }
    return true;
    }

    /**
     * お気に入り追加 TODO 小森修正中
     * @param unknown_type $storageId
     * @param unknown_type $userKey
     * @param unknown_type $memberKey
     * @return multitype:string
     */
    function addFavorite($storageId, $userKey, $memberKey) {
        if (empty($memberKey)) {
            $memberKey = 0;
        }
        $data = array(
            "storage_file_key" => $storageId,
            "member_key" => $memberKey,
            "user_key"   => $userKey,
            "create_datetime" => date("Y-m-d H:i:s"),
        );
        $this->logger->info($data);

        // 重複登録禁止
        $where = sprintf("storage_file_key=%s AND member_key=%s AND user_key=%s",$storageId,$memberKey,$userKey);
        $count = $this->_objStorageFavorite->numRows($where);
        if($count == 0){
            $ret = $this->_objStorageFavorite->add($data);
        }

       // $this->logger->info($ret);
        if (DB::isError($ret)) {
            //$this->logger->error($ret->getMessage());
            return array("error" => "db");
        }
    }

    /**
     * お気に入り削除 TODO 小森修正中
     * @param unknown_type $storageId
     * @param unknown_type $userKey
     * @param unknown_type $memberKey
     * @return multitype:string
     */
    function deleteFavorite($storageId, $userKey, $memberKey) {
        $where  = "storage_file_key      = '".$storageId."'";
        $where .= " AND user_key   = '".$userKey."'";
        $where .= " AND member_key = '".$memberKey."'";
        $this->logger->info($where);

        $ret = $this->_objStorageFavorite->remove($where);
        //$this->logger->info($ret);
        if (DB::isError($ret)) {
            //$this->logger->error($ret->getMessage());
            return array("error" => "db");
        }
    }

    function getPastMeetingList($where = "") {
        $rows = $this->_objWhiteboardStorage->getRowsAssoc($where, array("storage_sort" => "asc"));

        return $rows;
    }

    /**
     * TODO 小森修正中
     * @param unknown_type $where
     * @param unknown_type $sort
     * @param unknown_type $limit
     * @param unknown_type $offset
     * @param unknown_type $columns
     * @param unknown_type $favorite
     * @param unknown_type $folder
     * @return multitype:|multitype:unknown
     */
    function getList($where = "", $sort = array(), $limit = null, $offset = 0, $columns = "*", $favorite = null, $folder = null) {
        $rs = $this->select($where, $sort, $limit, $offset, $columns, $favorite, $folder);
        if (DB::isError($rs)) {
            //$this->logger->error($rs->getUserInfo());
            return array();
        }
        $ret = array();
        while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            $ret[] = $row;
        }
        return $ret;
    }

    /**
     * TODO 小森修正中
     * @param unknown_type $where
     * @param unknown_type $sort
     * @param unknown_type $limit
     * @param unknown_type $offset
     * @param unknown_type $columns
     * @param unknown_type $favorite
     * @param unknown_type $folder
     * @return boolean|unknown
     */
    function select($where = "", $sort = array(), $limit = null, $offset = 0, $columns = "storage_file.*", $favorite = null, $folder = null){
        if (empty($columns)) {
            $columns = "storage_file.*";
        }
        $sql  = "SELECT ". $columns . " FROM ". "storage_file ";
        //$sql .= " LEFT JOIN member ON storage_file.member_key = member.member_key";
        if (!empty($favorite)) {
            $sql .= " , storage_favorite_file";
        }
        if ($where) {
            $sql .= " WHERE ".$where;
        }
        if ($favorite) {
            $sql .= " ". $favorite;
        }
        if ($folder) {
            $sql .= " ". $folder;
        }
        if ($sort) {
            $sql .= " ORDER BY ";
            $i = 0;
            foreach($sort as $field => $type) {
                if ($i > 0) {
                    $sql .= ",";
                }
                $sql .= "storage_file." . $field." ".$type;
                $i++;
            }
        }
        //$this->logger->info($sql);

        $ret = $this->_obj_N2MYDB->_conn->limitQuery($sql, $offset, $limit);

        if (DB::isError($ret)) {
            $this->logger->info("error");
            //$this->logger->error($ret->getUserInfo());
            return false;
        }
        return $ret;
    }

    function getFolderStorageList($where = "") {
        $sql  = "SELECT * whiteboard_storage.storage_id, * FROM ". "whiteboard_storage";
        $sql .= " LEFT JOIN storage_folder_doc ON whiteboard_storage.member_key = storage_folder_doc.member_key";
        $sql .= " WHERE ".$where;
        $this->logger->info($sql);

        $ret = $this->_obj_N2MYDB->_conn->query($sql);

        if (DB::isError($ret)) {
            $this->logger->error($ret->getUserInfo());
            return false;
        }
        while($row = $ret->fetchRow(DB_FETCHMODE_ASSOC)) {
            $rows[] = $row;
        }
        $this->logger->info($rows);
        return $rows;
    }

    function action_addListRow($addRow = false, $listOffset = null) {
        $rowset     = $this->session->get("rowset");
        $searchType = $rowset["search_type"] ? $rowset["search_type"] : "";
        $request = $this->request->getAll();
        if (empty($listOffset)) {
            $listOffset = $request["offset"] ? $request["offset"] : 0;
        }
        $upRow = $rowset["up_row"] ? $rowset["up_row"] : 0;

        if ($addRow) {
            $limit     = $listOffset;
            $offset    = 0;
        } else {
            $limit     = 10;
            $offset    = $listOffset;
        }

        $userKey   = $this->_userKey;
        $memberKey = $this->_memberKey;
        $options = array(
            "user_key"       => $userKey,
            "member_key"     => $memberKey,
            "limit"          => $limit,
            "offset"         => $offset,
            "sort_key"       => $rowset["sort_key"]  ? $rowset["sort_key"]  : "update_datetime",
            "search_type"    => $rowset["search_type"],
            "search_keyword" => $rowset["search_keyword"]
        );
        if ($addRow) {
            $upRow = 0;
        }
        $this->logger->info($options);
        $this->session->set("condition", $options, $this->_name_space);

        $ret = $this->action_list($options, true);
        $this->logger->info($ret);

        if ($addRow) {
            $limit = 10;
            $ret   = array_reverse($ret);
        }

        $options["offset"] = $offset + $limit;
        $nextRow           = true;
        $objWhiteboardStorage = $this->_obj_WhiteboardStorage;
        if (!$objWhiteboardStorage->getStorageList($options)) {
            $nextRow = false;
        }

        $rowset["offset"] = $offset;
        $this->session->set("rowset", array(
            "offset"         => $offset,
            "limit"          => $limit,
            "sort_key"       => $rowset["sort_key"],
            "up_row"         => $upRow,
            "search_type"    => $rowset["search_type"],
            "search_keyword" => $rowset["search_keyword"]
        ));

       $charges = $objWhiteboardStorage->getCharges($userKey, $memberKey);

       $retArray = array("rowset" => $rowset, "list" => $ret, "next_row" => $nextRow, "up_row" => $upRow, "charges" => $charges);
       if ($addRow) {
           return $retArray;
       }
       echo json_encode($retArray);
    }
    function numRows($where = "",$favorite = null) {
        $sql  = "SELECT count(*) FROM ". "storage_file";
        if (!empty($favorite)) {
            $sql .= " , storage_favorite";
        }
        if ($where) {
            $sql .= " WHERE ".$where;
        }
        if ($favorite) {
            $sql .= " ". $favorite;
        }
//        $this->logger->info($sql);

        $count = $this->_obj_N2MYDB->_conn->getOne($sql);
//        $this->logger->info($count);

        if (DB::isError($count)) {
            $this->logger->error($count->getUserInfo());
            return false;
        }
        return $count;
    }

    /**
     * ファイルの移動 TODO
     * @param int $storage_file_key
     * @param int $to_member_key
     * @param int $to_storage_folder_key
     * @param int $my_member_key
     * @return boolean
     */
    function moveFile($storage_file_key, $to_member_key, $to_storage_folder_key , $my_member_key , $user_key=null){
        $file_info = $this->getStorageInfo($storage_file_key);
        $user_id = $this->getUserId($to_member_key,$user_key);
        $old_dir = N2MY_DOC_DIR.$file_info["file_path"].$file_info["document_id"];
        $file_path = $this->getAccountPath($user_id , $to_member_key);
        $new_dir = N2MY_DOC_DIR.$file_path.$file_info["document_id"];
        //$this->logger->info($old_dir);
        //$this->logger->info($new_dir);
        if($file_info["category"] == "image" || $file_info["category"] == "document"){
            // DBデータ移動(更新)
            $data = array();
            $data["member_key"] = $to_member_key;
            $data["storage_folder_key"] = $to_storage_folder_key;
            $data["last_update_member_key"] = $my_member_key;
            $data["update_datetime"] = date("Y-m-d H:i:s");
            $data["file_path"] = $file_path;
            $where = "storage_file_key = " . $storage_file_key;
            $result   = $this->_objStorage->update($data, $where);
            if (!empty($result)) {
                return false;
            }

            // Documentテーブルの更新
            $data = array();
            $data["document_path"] = $file_path;
            $data["update_datetime"] = date("Y-m-d H:i:s");
            $where = sprintf("document_id = '%s' AND document_mode = '%s'", $file_info["document_id"] , "storage");
            $this->_obj_Document->update($data, $where);

            // 実ファイルの移動
            if(!file_exists(N2MY_DOC_DIR.$file_path)){
                mkdir(N2MY_DOC_DIR.$file_path,0777,true);
                chmod(N2MY_DOC_DIR.$file_path, 0777);
            }
            if(is_dir($old_dir)){
                $this->_mv($old_dir, $new_dir);
            }
        }else if ($file_info["category"] == "video"){
            // DBデータ移動(更新)
            $data = array();
            $data["member_key"] = $to_member_key;
            $data["storage_folder_key"] = $to_storage_folder_key;
            $data["last_update_member_key"] = $my_member_key;
            $data["update_datetime"] = date("Y-m-d H:i:s");
            $where = "storage_file_key = " . $storage_file_key;
            $result   = $this->_objStorage->update($data, $where);
            if (!empty($result)) {
                return false;
            }
        } else if ($file_info["category"] == "personal_white"){
            $wb_id = $this->createID();
            $wb_path = $file_path."wb/".$wb_id."/";
            $data = array();
            $data["member_key"] = $to_member_key;
            $data["storage_folder_key"] = $to_storage_folder_key;
            $data["last_update_member_key"] = $my_member_key;
            $data["update_datetime"] = date("Y-m-d H:i:s");
            $data["file_path"] = $wb_path;
            $where = "storage_file_key = " . $storage_file_key;
            $result   = $this->_objStorage->update($data, $where);
            // 実ファイルの移動
            $old_dir = N2MY_DOC_DIR.$file_info["file_path"];
            $new_dir = N2MY_DOC_DIR.$wb_path;
            if(!file_exists(N2MY_DOC_DIR.$file_path."wb/")){
                mkdir(N2MY_DOC_DIR.$file_path."wb/",0777,true);
                chmod(N2MY_DOC_DIR.$file_path."wb/", 0777);
            }
            if(is_dir($old_dir)){
                $this->_mv($old_dir, $new_dir);
            }
            if (!empty($result)) {
                return false;
            }

        }
        return true;
    }

    /**
     * mvコマンド
     * @param string $old_dir
     * @param string $new_dir
     */
    private function _mv($old_dir, $new_dir){
        if(file_exists($old_dir)){
            // ※コマンド関数を使用するので使う環境で注意が必要
            $cmd = sprintf("mv %s %s", $old_dir, $new_dir);
            $cmd_res = exec($cmd);
            //$this->logger->info($cmd_res);
        }
    }

    /**
     * cpコマンド
     * @param string $old_dir
     * @param string $new_dir
     */
    private function _cp($old_path, $new_path){
        if(file_exists($old_path)){
            if(is_dir($old_path)){
                $cmd = sprintf("cp -r %s %s", $old_path, $new_path);
            }else{
                $cmd = sprintf("cp %s %s", $old_path, $new_path);
            }
            // ※コマンド関数を使用するので使う環境で注意が必要
            $cmd_res = exec($cmd);
            //$this->logger->info($old_path);
            //$this->logger->info($new_path);
        }
    }

    /*
     * TODO会議室内貼り付け
     */
    function meetingCopyFile($storage_file_key , $meeting_info, $participant_id , $user_id){
        $file_info = $this->getStorageInfo($storage_file_key);
        if(!$file_info){
            return false;
        }
        $document_id = $this->createID();
        $path = $user_id."/".$meeting_info["room_key"]."/wb_".str_pad($meeting_info["meeting_key"], 10, 0, STR_PAD_LEFT)."/";

        // ファイルのコピー DBコピー
        if($file_info["category"] == "image" || $file_info["category"] == "document"){
            // documentテーブルにデータ追加
            $where = "document_id = '".addslashes($file_info["document_id"])."'";
            $doucument_info = $this->_obj_Document->getRow($where);
            unset($doucument_info["document_key"]);
            $doucument_info["document_id"] = $document_id;
            $doucument_info["document_name"] = $file_info["user_file_name"];
            $doucument_info["meeting_key"] = $meeting_info["meeting_key"];
            $doucument_info["document_path"] = $path;
            $doucument_info["document_mode"] = "meeting";
            $doucument_info["is_storage"] = "1";
            $doucument_info["document_transmitted"] = "1";
            $doucument_info["participant_id"] = $participant_id;
            $doucument_info["create_datetime"] = date("Y-m-d H:i:s");
            $doucument_info["update_datetime"] = date("Y-m-d H:i:s");

            $where = $where = "meeting_key = '".addslashes($meeting_info["meeting_key"])."'";
            $doucument_info["document_sort"] = $this->_obj_Document->getOne($where, "MAX(document_sort)", array("document_sort" => "desc")) + 1;

            $this->_obj_Document->add($doucument_info);

            // 実ファイルのコピー
            $old_dir = N2MY_DOC_DIR.$file_info["file_path"].$file_info["document_id"];
            $new_dir = N2MY_DOC_DIR.$path.$document_id;

            if(file_exists($old_dir)){
                if(!file_exists(N2MY_DOC_DIR.$path)){
                    mkdir(N2MY_DOC_DIR.$path,0777,true);
                    chmod(N2MY_DOC_DIR.$path, 0777);
                }
                // コピー
                $this->_cp($old_dir, $new_dir);
                // ファイルを一つ一つリネームする
                $files = scandir($new_dir);
                $this->logger->info($files);
                // 「.」と「..」は除去
                unset($files[array_search('.',$files)],$files[array_search('..',$files)]);
                $count = 1;
                foreach($files as $file){
                    // ID部分だけ変更する
                    $new_file_name = preg_replace('/([a-zA-Z0-9]*)(_)([0-9]*)(.[a-zA-Z]*)/',$document_id.'$2$3$4',$file);
                    $target_file_path = $new_dir . "/" .$file;
                    $new_file_path = $new_dir . "/" .$new_file_name;
                    rename($target_file_path, $new_file_path);
                    $count++;
                    $this->logger->info($new_file_path);
                }
            }

        }

        return true;
    }

    /**
     * ファイルコピー TODO
     * @param int $storage_file_key
     * @param int $to_member_key
     * @param int $to_storage_folder_key
     * @param int $my_member_key
     * @param int $user_key
     */
    function copyFile($storage_file_key , $to_member_key, $to_storage_folder_key , $my_member_key , $user_key){
        $file_info = $this->getStorageInfo($storage_file_key);
        if(!$file_info){
            return false;
        }
        $user_id = $this->getUserId($to_member_key,$user_key);
        $old_dir = N2MY_DOC_DIR.$file_info["file_path"].$file_info["document_id"];
        $file_path = $this->getAccountPath($user_id , $to_member_key);
        $new_dir = N2MY_DOC_DIR.$file_path.$file_info["document_id"];
        $this->logger->info($file_info["format"]);
        // ファイルのコピー DBコピー
        if($file_info["category"] == "image" || $file_info["category"] == "document"){
            //$this->logger->info($file_info["format"]);
            $document_id = $this->createID();
            $data = array(
                    "document_id"            => $document_id,
                    "clip_key"               => $file_info["clip_key"],
                    "personal_white_id"      => $file_info["personal_white_id"],
                    "user_key"               => $file_info["user_key"],
                    "member_key"             => $to_member_key,
                    "owner"                  => $file_info["owner"],
                    "file_path"              => $file_path,
                    "file_name"              => $file_info["file_name"],
                    "user_file_name"         => $file_info["user_file_name"],
                    "file_size"              => $file_info["file_size"],
                    "format"                 => $file_info["format"],
                    "category"               => $file_info["category"],
                    "extension"              => $file_info["extension"],
                    "version"                => $file_info["version"],
                    "document_index"         => $file_info["document_index"],
                    "status"                 => $file_info["status"],
                    "storage_folder_key"     => $to_storage_folder_key,
                    "last_update_member_key" => $my_member_key,
                    "create_datetime"        => date("Y-m-d H:i:s"),
                    "update_datetime"        => date("Y-m-d H:i:s")
            );

            //        $this->logger->info($data);
            $ret = $this->_objStorage->add($data);

            if (DB::isError($ret)) {
                $this->logger->info($ret->getMessage());
                return false;
            }
            // documentテーブルにデータ追加
            $where = "document_id = '".addslashes($file_info["document_id"])."'";
            $doucument_info = $this->_obj_Document->getRow($where);
            unset($doucument_info["document_key"]);
            $doucument_info["document_id"] = $document_id;
            $doucument_info["document_path"] = $file_path;
            $doucument_info["create_datetime"] = date("Y-m-d H:i:s");
            $doucument_info["update_datetime"] = date("Y-m-d H:i:s");
            $this->_obj_Document->add($doucument_info);

            // 実ファイルのコピー
            $old_dir = N2MY_DOC_DIR.$file_info["file_path"].$file_info["document_id"];
            $new_dir = N2MY_DOC_DIR.$file_path.$document_id;
            if(!file_exists(N2MY_DOC_DIR.$file_path)){
                mkdir(N2MY_DOC_DIR.$file_path,0777,true);
                chmod(N2MY_DOC_DIR.$file_path, 0777);
            }
            // コピー
            $this->_cp($old_dir, $new_dir);
            // ファイルを一つ一つリネームする
            $files = scandir($new_dir);
            // 「.」と「..」は除去
            unset($files[array_search('.',$files)],$files[array_search('..',$files)]);
            $count = 1;
            foreach($files as $file){
                // ID部分だけ変更する
                $new_file_name = preg_replace('/([a-zA-Z0-9]*)(_)([0-9]*)(.[a-zA-Z]*)/',$document_id.'$2$3$4',$file);
                $old_file_path = $new_dir . "/" .$file;
                $new_file_path = $new_dir . "/" .$new_file_name;
                rename($old_file_path, $new_file_path);
                $count++;
                $this->logger->info($new_file_path);
            }

        }else if ($file_info["category"] == "video"){
            //$this->logger->info($file_info["format"]);
            $data = array(
                    "document_id"            => $file_info["document_id"],
                    "clip_key"               => $file_info["clip_key"],
                    "personal_white_id"      => $file_info["personal_white_id"],
                    "user_key"               => $file_info["user_key"],
                    "member_key"             => $to_member_key,
                    "owner"                  => $file_info["owner"],
                    "file_path"              => $file_info["file_path"],
                    "file_name"              => $file_info["file_name"],
                    "user_file_name"         => $file_info["user_file_name"],
                    "file_size"              => $file_info["file_size"],
                    "format"                 => $file_info["format"],
                    "category"               => $file_info["category"],
                    "extension"              => $file_info["extension"],
                    "version"                => $file_info["version"],
                    "document_index"         => $file_info["document_index"],
                    "status"                 => $file_info["status"],
                    "storage_folder_key"     => $to_storage_folder_key,
                    "last_update_member_key" => $my_member_key,
                    "create_datetime"        => date("Y-m-d H:i:s"),
                    "update_datetime"        => date("Y-m-d H:i:s")
            );

            //        $this->logger->info($data);
            $ret = $this->_objStorage->add($data);

            if (DB::isError($ret)) {
                $this->logger->info($ret->getMessage());
                return false;
            }

        } else if ($file_info["category"] == "personal_white"){
            $document_id = $this->createID();
            $wb_path = $file_path."wb/".$document_id."/";
            $data = array(
                    "document_id"            => $file_info["document_id"],
                    "clip_key"               => $file_info["clip_key"],
                    "personal_white_id"      => $file_info["personal_white_id"],
                    "user_key"               => $file_info["user_key"],
                    "member_key"             => $to_member_key,
                    "owner"                  => $file_info["owner"],
                    "file_path"              => $wb_path,
                    "file_name"              => $file_info["file_name"],
                    "user_file_name"         => $file_info["user_file_name"],
                    "file_size"              => $file_info["file_size"],
                    "format"                 => $file_info["format"],
                    "category"               => $file_info["category"],
                    "extension"              => $file_info["extension"],
                    "version"                => $file_info["version"],
                    "document_index"         => $file_info["document_index"],
                    "status"                 => $file_info["status"],
                    "storage_folder_key"     => $to_storage_folder_key,
                    "last_update_member_key" => $my_member_key,
                    "create_datetime"        => date("Y-m-d H:i:s"),
                    "update_datetime"        => date("Y-m-d H:i:s")
            );

            //        $this->logger->info($data);
            $ret = $this->_objStorage->add($data);

            if (DB::isError($ret)) {
                $this->logger->info($ret->getMessage());
                return false;
            }
            // 実ファイルのコピー
            $old_dir = N2MY_DOC_DIR.$file_info["file_path"];
            $new_dir = N2MY_DOC_DIR.$wb_path;
            if(!file_exists(N2MY_DOC_DIR.$file_path."wb/")){
                mkdir(N2MY_DOC_DIR.$file_path."wb/",0777,true);
                chmod(N2MY_DOC_DIR.$file_path."wb/", 0777);
            }
            // コピー
            $this->_cp($old_dir, $new_dir);
        }
        return true;
    }

    /**
     * 資料ID生成
     */
    function createID() {
        $document_id = sha1(uniqid(rand(), true));
        return $document_id;
    }

    function salesRoomDocUpdate($document_data , $room_key ){
        // 資料追加
        $session   = EZSession::getInstance();
        $user_info = $session->get("user_info");
        // 追加処理
        if($document_data["storage_documents"]){
            foreach($document_data["storage_documents"] as $data){
                $this->_addStorageFileSales($data, $room_key, $user_info["user_id"]);
            }
        }
        // 更新処理
        if($document_data["documents"]){
            foreach($document_data["documents"] as $document){
                $this->_updateSalesDoc($document);
            }
        }

        // 削除処理
        if($document_data["delete_documents"]){
            require_once ("classes/N2MY_Document.class.php");
            $odj_N2MYdocument = NEW N2MY_Document($this->dsn);
            foreach($document_data["delete_documents"] as $delete_document){
                $odj_N2MYdocument->delete($delete_document);
            }
        }

        // ビデオ リレーションに貼り直し
        $this->_restoreClipRelations($document_data["clips"] , $room_key);
    }

    private function _restoreClipRelations($clips, $room_key)
    {
        require_once ("classes/N2MY_Clip.class.php");
        require_once ("classes/dbi/clip.dbi.php");
        $n2my_clip     = new N2MY_Clip($this->dsn);
        $clip_table    = new ClipTable($this->dsn);
        try {
            $meeting_key = 0;
            //            $clips       = $clip_table->activeUsersClipKeysIn($clips, $user_key);
            $n2my_clip->salesResoreRelations($room_key , $clips);
        } catch (Exception $e) {
            $this->logger->error('restoreClipRelations failed '.$e->getMessage().' meeting_ticket:'.$meeting_ticket.'
                    clip_keys:', print_r($clips,true));
            return false;
        }
        return true;
    }

    /*
     * セールス用 アップデート
     */
    function _updateSalesDoc($file_info){
        require_once("classes/core/dbi/Document.dbi.php");
        $obj_Document = new DBI_Document($this->dsn);
        $doucument_info["document_name"] = $file_info["document_name"];
        $doucument_info["document_sort"] = $file_info["document_sort"];
        $doucument_info["update_datetime"] = date("Y-m-d H:i:s");
        $where = sprintf("document_id = '%s'" , $file_info["id"]);
        $obj_Document->update($doucument_info, $where);
    }

    /*
     * ストレージデータから事前アップへコピー (sales用)
    */
    function _addStorageFileSales($file_info , $room_key , $user_id){
        $document_id = $this->createID();
        $path = $user_id."/".$room_key."/sales/";
        require_once("classes/core/dbi/Document.dbi.php");
        $obj_Document = new DBI_Document($this->dsn);
        require_once('classes/core/dbi/Meeting.dbi.php');
        $obj_Meeting  = new DBI_Meeting($this->dsn);
        // ファイルのコピー DBコピー
        if($file_info["category"] == "image" || $file_info["category"] == "document"){
            // documentテーブルにデータ追加
            $where = "document_id = '".addslashes($file_info["document_id"])."'";
            $doucument_info = $obj_Document->getRow($where);
            unset($doucument_info["document_key"]);
            $doucument_info["document_id"] = $document_id;
            $doucument_info["meeting_key"] = 0;
            $doucument_info["document_path"] = $path;
            $doucument_info["document_mode"] = "sales_pre";
            $doucument_info["document_name"] = $file_info["name"];
            $doucument_info["is_storage"] = "1";
            $doucument_info["document_transmitted"] = "1";
            $doucument_info["participant_id"] = $participant_id;
            $doucument_info["document_sort"] = $file_info["document_sort"];
            $doucument_info["create_datetime"] = date("Y-m-d H:i:s");
            $doucument_info["update_datetime"] = date("Y-m-d H:i:s");

//            $where = $where = "meeting_key = '".addslashes($meeting_info["meeting_key"])."'";
//            $doucument_info["document_sort"] = $obj_Document->getOne($where, "MAX(document_sort)", array("document_sort" => "desc")) + 1;

            $obj_Document->add($doucument_info);

            // 実ファイルのコピー
            $old_dir = N2MY_DOC_DIR.$file_info["file_path"].$file_info["document_id"];
            $new_dir = N2MY_DOC_DIR.$path.$document_id;

            if(file_exists($old_dir)){
                if(!file_exists(N2MY_DOC_DIR.$path)){
                    mkdir(N2MY_DOC_DIR.$path,0777,true);
                    chmod(N2MY_DOC_DIR.$path, 0777);
                }
                // コピー
                $this->_cp($old_dir, $new_dir);
                // ファイルを一つ一つリネームする
                $files = scandir($new_dir);
                // 「.」と「..」は除去
                unset($files[array_search('.',$files)],$files[array_search('..',$files)]);
                $count = 1;
                foreach($files as $file){
                    // ID部分だけ変更する
                    $new_file_name = preg_replace('/([a-zA-Z0-9]*)(_)([0-9]*)(.[a-zA-Z]*)/',$document_id.'$2$3$4',$file);
                    $target_file_path = $new_dir . "/" .$file;
                    $new_file_path = $new_dir . "/" .$new_file_name;
                    rename($target_file_path, $new_file_path);
                    $count++;
                    //$this->logger->info($new_file_path);
                }
            }

        }

        return true;
    }


    //------------ 4930


    // フォルダ削除、配下ファイルも削除する
    public function deleteFolder($folder_key,$member_key , $user_key){
        $this->logger->info("DELETE FOLDER :" . $folder_key);
        $where = "storage_folder_key = '".addslashes($folder_key)."'";
        $data = array(
                "status" => 0,
                );

        // フォルダステータス更新
        $ret = $this->_objStorageFolder->update($data , $where);
        if (DB::isError($ret)) {
            $this->logger->info($ret->getMessage());
            return false;
        }
        //配下ファイル取得
        $where = "status  != 3 AND user_key = ".$user_key." AND member_key = ".$member_key." AND  storage_folder_key = '".$folder_key."'";
        $files =  $this->_objStorage->getRowsAssoc($where);
        foreach($files as $file){
            $this->delete_file($file["storage_file_key"], $file["user_key"], $file["member_key"]);
        }
        return 1;

    }

    // 個人ストレージのフォルダ取得
    public function getPrivateFolder($user_key , $member_key){
            $where = "status =1 AND user_key=" . $user_key. " AND member_key = " . $member_key;
            return $this->_objStorageFolder->getRowsAssoc($where);
    }

    // 共有ストレージのフォルダ取得
    public function getSharedFolder($user_key , $member_key){
        $where = "status =1 AND user_key=" . $user_key. " AND member_key = 0";
        return $this->_objStorageFolder->getRowsAssoc($where);
    }

    // 配列のフォルダをツリー構造に変化する
    public function setFolderTree($folders , $root_key = 0){

        $idField = 'storage_folder_key';
        $parentField = 'parent_storage_folder_key';

        if (! is_array($folders)){
            return false;
        }
        $tree = array();
        $index = array();

        foreach($folders as $value){
            $id = $value[$idField];
            $pid = $value[$parentField];

            if (isset($index[$id])){
                $value["kids"] = $index[$id]["kids"];
                $index[$id] = $value;
            } else {
                $index[$id] = $value;
            }

            if ($pid == $root_key){
                $tree[] = & $index[$id];
            } else {
                $index[$pid]["kids"][] = & $index[$id];
            }
        }
        return $tree;
    }

    // フォルダー情報取得
    function getFolderInfo($storage_folder_key = null) {
        $where = "storage_folder_key = '".addslashes($storage_folder_key)."'";
        $row = $this->_objStorageFolder->getRow($where);
        return $row;
    }

    // 指定されたフォルダの直下フォルダ取得
    function getInFolder($parent_storage_folder_key = 0 , $member_key = 0 , $user_key = 0){
        $where = 'status = 1 AND user_key = '.$user_key.' AND member_key = ' .$member_key. ' AND parent_storage_folder_key = '. $parent_storage_folder_key;
        return $this->_objStorageFolder->getRowsAssoc($where);
    }

    // 指定されたフォルダの配下フォルダ取得
    function getChildrenFolder($storage_folder_key = 0 , $member_key = 0){
        $where = 'status = 1 AND member_key = ' .$member_key. ' AND path LIKE "%'. $storage_folder_key .'%"';
        return $this->_objStorageFolder->getRowsAssoc($where);
    }

    // TODO 4930 フォルダ移動
    public function moveFolder($folder_key, $to_member_key, $to_folder_key , $my_member_key , $user_key=null){

        // 移動先が同じならエラー
        if($folder_key == $to_folder_key){
            // 移動先が自分の場合もエラー
            return -3;
        }

        $folder_info = $this->getFolderInfo($folder_key);
        if(!$folder_info || $folder_info["user_key"] != $user_key){
            // 不正なフォルダ
            return -1;
        }

        // パスを確認
        $path = "0";
        if($to_folder_key != 0){
            $to_folder_info = $this->getFolderInfo($to_folder_key);
            // 移動先が配下フォルダだったらエラー
            if(strstr($to_folder_info["path"] , $folder_key) ||$to_folder_info["user_key"] != $user_key){
                return -3;
            }
            $path = $to_folder_info["path"] . "," . $to_folder_key;
        }
        // 階層のチェック(現状4階層まで)
        $path_array = explode("," , $path );
        if(count($path_array) >= 4){
            // 階層エラー
            return -2;
        }
        // 配下ディレクト取得
        $childrenFolders = $this->getChildrenFolder($folder_key , $folder_info["member_key"]);
        $tree = $this->setFolderTree($childrenFolders , $folder_key);

        // 配下フォルダが4階層以上になるならエラー
        $childrenFolders_tree = $this->folderPathCreate($tree , $path , $folder_key);
        if($childrenFolders_tree == -1){
            // 階層エラー
            return -2;
        }


        // フォルダ情報更新
        $data = array(
                "parent_storage_folder_key" => $to_folder_key,
                "path"                      => $path,
                "member_key"                => $to_member_key,
                );
        $where = "storage_folder_key = " . $folder_key;

        $this->logger->info($data);

        $this->_objStorageFolder->update($data , $where);

        if(count($childrenFolders_tree) != 0){
            $this->updateFolders($childrenFolders_tree , $to_member_key);
        }
        // フォルダ内のファイル更新
        $file_data = array(
                "member_key" => $to_member_key,
                );

        $folder_keys[] = $folder_key;
        foreach($childrenFolders as $folder){
            $folder_keys[] = $folder["storage_folder_key"];
        }
        $file_where = "storage_folder_key IN (". implode(",",$folder_keys) . ") ";
        $this->_objStorage->update($file_data , $file_where);

        return 1;
    }

    // パスを書き換える
    private function updateFolders($folders , $to_member_key){
        $data = array(
                "path"                      => $folders["0"]["path"],
                "member_key"                => $to_member_key,
        );
        $where = "storage_folder_key IN (";
        foreach($folders as $folder){
            $folder_keys[] = $folder["storage_folder_key"];
            if($folder["kids"]){
                $this->updateFolders($folder["kids"] , $to_member_key);
            }
        }
        $where = $where . implode(",",$folder_keys) . ") ";
        $this->logger->info($where);
        $this->_objStorageFolder->update($data , $where);
    }

    // フォルダのパスを作成
    private function folderPathCreate($folders , $parent_path , $parent_folder_key ){
        $folders_count = count($folders);
        for($i = 0 ; $i < $folders_count; $i++){
            $folders[$i]["path"] = $parent_path.",".$parent_folder_key;
            $path_array = explode("," , $folders[$i]["path"] );
            // 4階層以上になるならエラー
            if(count($path_array) >= 4){
                return -1;
            }
            if($folders[$i]["kids"]){
                $folders[$i]["kids"] = $this->folderPathCreate($folders[$i]["kids"] , $folders[$i]["path"] , $folders[$i]["storage_folder_key"]);
                if($folders[$i]["kids"] == -1){
                    return -1;
                }
            }
        }
        return $folders;
    }

    // フォルダ名変更
    public function updataNameFolder($folder_key , $folder_name){

        $data = array(
                "folder_name" => $folder_name,
                );
        $where = "storage_folder_key = '".addslashes($folder_key)."'";
        $res = $this->_objStorageFolder->update($data , $where);
        if(!empty($res)){
            return -1;
        }
        return 1;
    }

    // TODO 4930 フォルダ作成
    public function addFolder($parent_folder , $user_key , $member_key , $folder_name) {

        $path = 0;
        if($parent_folder != 0){
            $where = "storage_folder_key = '".addslashes($parent_folder)."'";
            $parent_folder_info = $this->_objStorageFolder->getRow($where);
            if(!$parent_folder_info || $parent_folder_info["user_key"] != $user_key){
                return -100;
            }
            $path = $parent_folder_info["path"].",".$parent_folder_info["storage_folder_key"];
            // 階層のチェック
           $path_array = explode("," , $parent_folder_info["path"] );
           if(count($path_array) >= 3){
               return -1;
           }
        }

        // 数のチェック
        $where = "status = 1 AND user_key = " .addslashes($user_key). " AND member_key = " .addslashes($member_key);
        $folder_count = $this->_objStorageFolder->numRows($where);
//$this->logger->info($folder_count);
        if($folder_count > 30){
            return -2;
        }

        $data = array(
                "user_key"                  => $user_key,
                "member_key"                => $member_key,
                "folder_name"               => $folder_name,
                "parent_storage_folder_key" => $parent_folder,
                "path" => $path,
                "status" => 1,
        );

        $this->logger->info($data);
        $this->_objStorageFolder->add($data);
        return 1;

        /*
         $data = array();
        $data["user_key"] = $userKey;
        $data["member_key"] = $memberKey;
        $this->logger->info($data);

        $ret = $this->_objStorageFolder->add($data);
        $this->logger->info($ret);

        $maxFolderId = $this->_objStorageFolder->maxFolderId();
        $this->logger->info($maxFolderId);

        $where = "user_key      = '".addslashes($userKey)."'";
        if (!empty($options["member_key"])) {
        $where .= " AND member_key   = '".addslashes($memberKey)."'";
        }
        $where  .= " AND folder_id       = '".addslashes($folderId)."'";
        $this->logger->info($where);

        $result   = $this->_objStorageFolder->getRow($where);

        $nodePath = "";
        if (!empty($result)) {
        $nodePath = $result["node_path"];
        }
        $nodePath = sprintf("%s%s.", $nodePath, $maxFolderId);

        $data["node_path"] = $nodePath;
        $this->logger->info($data);

        $where = "user_key      = '".addslashes($userKey)."'";
        if (!empty($options["member_key"])) {
        $where .= " AND member_key   = '".addslashes($memberKey)."'";
        }
        $where  .= " AND folder_id       = '".addslashes($maxFolderId)."'";
        $this->logger->info($where);

        $result   = $this->_objStorageFolder->update($data, $where);

        if (!empty($result)) {
        return false;
        }
        return $maxFolderId;
        */
    }

    // TODO 4950 フォルダ作成
    public function addExternalServiceFolder($parent_folder , $user_key , $member_key , $folder_name , $external_service_account_key , $external_service_name) {

        $path = 0;
        if($parent_folder != 0){
            $where = "storage_folder_key = '".addslashes($parent_folder)."'";
            $parent_folder_info = $this->_objStorageFolder->getRow($where);
            if(!$parent_folder_info || $parent_folder_info["user_key"] != $user_key){
                return -1;
            }
            $path = $parent_folder_info["path"].",".$parent_folder_info["storage_folder_key"];
            // 階層のチェック
            $path_array = explode("," , $parent_folder_info["path"] );
            if(count($path_array) >= 3){
                return -1;
            }
        }

        // 数のチェック
        $where = "status = 1 AND user_key = " .addslashes($user_key). " AND member_key = " .addslashes($member_key);
        $folder_count = $this->_objStorageFolder->numRows($where);
        //$this->logger->info($folder_count);
        if($folder_count > 30){
            return -1;
        }

        $external_service_folder_key = $this->createID();

        $data = array(
                "user_key"                  => $user_key,
                "member_key"                => $member_key,
                "folder_name"               => $folder_name,
                "parent_storage_folder_key" => $parent_folder,
                "path" => $path,
                "status" => 1,
                "external_flg" => 1,
                "external_service_account_key" => $external_service_account_key,
                "external_service_name" => $external_service_name,
                "external_service_folder_key" => $external_service_folder_key,
        );

        $this->logger->info($data);
        $this->_objStorageFolder->add($data);
        return $external_service_folder_key;
    }

}


