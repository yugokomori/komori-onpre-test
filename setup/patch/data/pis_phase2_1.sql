ALTER TABLE `ives_setting` ADD `media_mixer_key` INT( 11 ) NOT NULL AFTER `mcu_server_key`;
ALTER TABLE `ives_setting` ADD INDEX ( `media_mixer_key` );
ALTER TABLE `reservation` ADD `media_mixer_key` INT( 11 ) NOT NULL AFTER `meeting_key`;
ALTER TABLE `reservation` ADD INDEX ( `media_mixer_key` );

ALTER TABLE  `member` ADD  `accounting_unit_code` VARCHAR( 10 ) NULL AFTER  `member_id` ,
ADD  `expense_department_code` VARCHAR( 10 ) NULL AFTER  `accounting_unit_code` ,
ADD INDEX (  `accounting_unit_code` ,  `expense_department_code` );

ALTER TABLE  `member` CHANGE  `member_id`  `member_id` VARCHAR( 64 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE  `member` CHANGE  `member_pass`  `member_pass` VARCHAR( 64 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE  `member` ADD INDEX (  `user_key` );
ALTER TABLE  `member` ADD INDEX (  `member_id` );

ALTER TABLE  `member_usage_details` CHANGE  `member_id`  `member_id` VARCHAR( 64 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

ALTER TABLE  `participant` ADD  `is_mobile` TINYINT( 1 ) NOT NULL DEFAULT  '0' AFTER  `is_narrowband`;
