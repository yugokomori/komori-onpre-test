-- phpMyAdmin SQL Dump
-- version 2.10.2
-- http://www.phpmyadmin.net
-- 
-- ホスト: localhost
-- 生成時間: 2009 年 12 月 17 日 04:06
-- サーバのバージョン: 5.0.41
-- PHP のバージョン: 5.2.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- データベース: `meeting_trunk_meeting`
-- 

-- --------------------------------------------------------

-- 
-- テーブルの構造 `meeting_invitation`
-- 

CREATE TABLE `meeting_invitation` (
  `invitation_key` int(10) NOT NULL auto_increment,
  `meeting_key` int(10) NOT NULL COMMENT '会議キー',
  `meeting_session_id` varchar(255) NOT NULL,
  `meeting_ticket` varchar(255) NOT NULL,
  `user_session_id` varchar(255) default NULL COMMENT '通常ユーザー用ハッシュ値',
  `audience_session_id` varchar(255) default NULL COMMENT 'オーディエンスユーザー用ハッシュ値',
  `status` tinyint(1) NOT NULL default '0' COMMENT '0: 無効, 1: 有効, 9: 終了',
  `entrytime` datetime NOT NULL,
  `updatetime` datetime default NULL,
  PRIMARY KEY  (`invitation_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
