ALTER TABLE  `meeting` ADD INDEX (  `user_key` );
ALTER TABLE  `service` ADD  `meeting_limit_time` INT NOT NULL DEFAULT  '0' COMMENT  'デフォルト会議利用制限時間' AFTER  `max_whiteboard_seat`;
ALTER TABLE  `service` ADD  `max_room_bandwidth` INT( 11 ) NOT NULL DEFAULT  '0' COMMENT  '部屋上限帯域' AFTER  `meeting_limit_time` ,
ADD  `max_user_bandwidth` INT( 11 ) NOT NULL DEFAULT  '0' COMMENT  '拠点上限帯域' AFTER  `max_room_bandwidth` ,
ADD  `min_user_bandwidth` INT( 11 ) NOT NULL DEFAULT  '0' COMMENT  '拠点下限限帯域' AFTER  `max_user_bandwidth`;
ALTER TABLE  `service` ADD  `default_camera_size` VARCHAR( 64 ) NULL DEFAULT NULL COMMENT  'デフォルトカメラサイズ' AFTER  `min_user_bandwidth`;
ALTER TABLE  `service` ADD  `h264` INT NOT NULL DEFAULT  '0' COMMENT  'H.264オプション' AFTER  `record_gw`;
ALTER TABLE  `service` ADD  `disable_rec_flg` TINYINT NOT NULL DEFAULT  '0' COMMENT  '録画不可フラグ' AFTER  `default_camera_size`;

UPDATE  `service` SET  `meeting_limit_time` =  '45' WHERE  `service_name` = 'Paperless_Free';

INSERT INTO `service` VALUES(72, 1, 'Meeting_Free', 3, 0, 0, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NOW( ), 1);

INSERT INTO `options` VALUES(21, 'teleconference', NOW(), NULL);
INSERT INTO `options` VALUES(22, 'video_conference', NOW(), NULL);
INSERT INTO `options` VALUES(23, 'minimumBandwidth80', NOW(), NULL);
INSERT INTO `options` VALUES(24, 'h264', NOW(), NULL);
INSERT INTO `service_option` (`service_option_key` ,`country_key` ,`service_option_name` ,`service_option_price` ,`service_option_init_price` ,`service_option_running_price`)VALUES (24 ,  '1',  'video_conference',  '0',  '0',  '0');
INSERT INTO `service_option` (`service_option_key` ,`country_key` ,`service_option_name` ,`service_option_price` ,`service_option_init_price` ,`service_option_running_price`)VALUES (25 ,  '1',  'minimumBandwidth80',  '0',  '0',  '0');
INSERT INTO `service_option` (`service_option_key` ,`country_key` ,`service_option_name` ,`service_option_price` ,`service_option_init_price` ,`service_option_running_price`)VALUES (26 ,  '1',  'h264',  '0',  '0',  '0');

ALTER TABLE  `fms_server` ADD  `server_version` VARCHAR( 20 ) NOT NULL COMMENT  'FMSバージョン' AFTER  `server_priority`;

CREATE TABLE IF NOT EXISTS `mcu_server` (
  `server_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `server_country` varchar(32) DEFAULT 'jp',
  `server_address` varchar(255) NOT NULL,
  `prefix_number` int(11) NOT NULL,
  `is_available` tinyint(4) NOT NULL DEFAULT '1',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`server_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

ALTER TABLE  `service` ADD  `service_expire_day` INT NOT NULL DEFAULT  '0' COMMENT  '自動利用停止日数' AFTER  `service_status`;
