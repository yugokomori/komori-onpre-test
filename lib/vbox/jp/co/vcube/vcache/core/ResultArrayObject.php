<?php

class ResultArrayObject extends ArrayObject
{

  public function __construct( $input=array(), $flags=ArrayObject::ARRAY_AS_PROPS, $iterator_class=null )
  {
    if ( $iterator_class )
      parent::__construct( $input, $flags, $iterator_class );
    else
      parent::__construct( $input, $flags );

    if( !$this->validateProperties( $input ) )
      throw new Exception( "Invalid properties" );
  }

  private function validateProperties( $input )
  {
    return true;
  }


}


