--
-- テーブルの構造 `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `question_id` int(11) NOT NULL auto_increment,
  `questionnaire_id` int(11) NOT NULL COMMENT 'questionnaireのID',
  `lang1` text character set utf8,
  `lang2` text character set utf8,
  `lang3` text character set utf8,
  `lang4` text character set utf8,
  `lang5` text character set utf8,
  `type` int(4) NOT NULL COMMENT '表示タイプ?1:radio,2:checkbox,3:radio+comment,4:checkbox+comment,5:comment',
  `sort` int(11) NOT NULL,
  `created_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `updated_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `deleted_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`question_id`),
  KEY `questionnaire_id` (`questionnaire_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- テーブルのデータをダンプしています `question`
--

INSERT INTO `question` (`question_id`, `questionnaire_id`, `lang1`, `lang2`, `lang3`, `lang4`, `lang5`, `type`, `sort`, `created_datetime`, `updated_datetime`, `deleted_datetime`) VALUES
(1, 1, '今回のWeb会議利用に満足されていますか？', 'How satisfied are you with this Web Conferencing service?', '您对本次网络视频会议的使用满意吗？', NULL, NULL, 1, 1, '2011-09-27 17:24:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, '映像について（複数選択可）', 'About Video (multiple selections)', '关于图像（可以多选）', NULL, NULL, 2, 2, '2011-09-28 12:35:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 1, '音声について（複数選択可）', 'About Audio (multiple selections)', '关于语音（可以多选）', NULL, NULL, 2, 3, '2011-09-29 20:06:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 1, 'デザインについて（複数選択可）', 'About Design  (multiple selections)', '关于设计（可以多选）', NULL, NULL, 4, 4, '2011-09-29 20:07:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 1, 'その他（複数選択可）', 'Others  (multiple selections)', '其他（可以多选）', NULL, NULL, 4, 5, '2011-09-29 20:06:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 1, 'Web会議の利用頻度をお教え下さい。', 'How often do you use this Web Conferencing service?', '请选择您使用网络视频会议的频率。', NULL, NULL, 1, 6, '2011-09-29 20:07:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 1, '今回のWeb会議のタイプはどのようなものですか？', 'What type of meeting was this?', '您这次的网络视频会议属于什么类型？', NULL, NULL, 1, 7, '2011-10-03 15:41:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 1, '今回の会議に参加した方はどのような方ですか？（複数選択可）', 'Who participated in this meeting? (multiple selections)', '这次会议的参会者身份是什么？（可以多选）', NULL, NULL, 1, 8, '2011-10-03 15:41:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 1, 'その他ご意見・ご要望があればお願いいたします（自由記述）', 'Please give us your comments and suggestions (Free description)', '请您填写其他的意见或建议。（自由阐述）', NULL, NULL, 5, 9, '2011-10-03 15:42:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');


-- --------------------------------------------------------

--
-- テーブルの構造 `questionnaire`
--

CREATE TABLE IF NOT EXISTS `questionnaire` (
  `questionnaire_id` int(11) NOT NULL auto_increment,
  `type` varchar(64) character set utf8 NOT NULL COMMENT '表示箇所タイプ',
  `is_active` tinyint(4) NOT NULL default '1' COMMENT '公開:1、非公開:0',
  `is_deleted` tinyint(4) NOT NULL default '0' COMMENT '削除フラグ',
  `created_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `updated_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `deleted_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`questionnaire_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- テーブルのデータをダンプしています `questionnaire`
--

INSERT INTO `questionnaire` (`questionnaire_id`, `type`, `is_active`, `is_deleted`, `created_datetime`, `updated_datetime`, `deleted_datetime`) VALUES
(1, 'meeting_end', 1, 0, '2011-09-27 17:20:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- テーブルの構造 `question_branch`
--

CREATE TABLE IF NOT EXISTS `question_branch` (
  `question_branch_id` int(11) NOT NULL auto_increment,
  `question_id` int(11) NOT NULL COMMENT '紐付くquestion_id',
  `lang1` text NOT NULL,
  `lang2` text NOT NULL,
  `lang3` text NOT NULL,
  `lang4` text NOT NULL,
  `lang5` text NOT NULL,
  `sort` int(11) NOT NULL,
  `created_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `updated_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `deleted_datetime` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`question_branch_id`),
  KEY `question_id` (`question_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

--
-- テーブルのデータをダンプしています `question_branch`
--

INSERT INTO `question_branch` (`question_branch_id`, `question_id`, `lang1`, `lang2`, `lang3`, `lang4`, `lang5`, `sort`, `created_datetime`, `updated_datetime`, `deleted_datetime`) VALUES
(1, 1, '大変満足', 'Very satisfied', '非常满意', '', '', 1, '2011-09-27 21:14:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 'やや満足', 'Somewhat satisfied', '比较满意', '', '', 3, '2011-09-27 21:15:07', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 1, '満足', 'Satisfied', '满意', '', '', 2, '2011-09-27 21:15:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 1, 'どちらとも言えない', 'Unsure', '都不是', '', '', 4, '2011-09-28 12:50:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 1, 'やや不満', 'Somewhat dissatisfied', '比较不满意', '', '', 5, '2011-09-28 12:50:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 1, '不満', 'Dissatisfied', '不满意', '', '', 6, '2011-09-28 12:51:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 1, '非常に不満', 'Very dissatisfied', '非常不满意', '', '', 7, '2011-09-28 12:51:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 2, '相手の映像が表示されない', 'Cannot see others'' video', '未显示对方图像', '', '', 1, '2011-09-29 19:46:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 2, '自分の映像が表示されない', 'Cannot see your own video', '未显示我方图像', '', '', 2, '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 2, '映像の品質が悪い', 'Video quality is not good', '图像质量差', '', '', 3, '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 3, '自分の音声が取得できない', 'Cannot hear your own voice', '无法接通我方语音', '', '', 1, '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 3, '相手の音声が聞こえない', 'Cannot hear others'' voice', '无法听到对方语音', '', '', 2, '2011-09-28 12:52:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 3, '音声が遅延する', 'Cause delay', '语音延迟', '', '', 4, '2011-09-28 21:24:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 3, '音声がエコーする（やまびこのように聞こえる）', 'Cause echo', '语音回响（听起来有回音）', '', '', 5, '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 3, '音声がハウリングする（キーンという耳障りな大きな音がする）', 'Cause howling noise', '语音啸响（声音尖锐刺耳、大声作响）', '', '', 6, '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 4, '機能（ボタン）が多く操作に迷う', 'Too many features (buttons) and difficult to operate', '功能（按钮）太多令操作困惑', '', '', 1, '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 4, 'デザインがわかりにくい（使いにくい）箇所がある。（お手数ですが詳細をご記入ください）', 'Design is not user-friendly (please enter details)', '有些部分设计难懂（不易使用）。（麻烦您填写详细内容）', '', '', 2, '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 5, '突然切断される', 'Suddenly disconnect', '突然中断', '', '', 1, '2011-09-28 12:52:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 5, 'デスクトップ共有が開始出来ない', 'Cannot start Desktop Sharing', '桌面共享无法开始', '', '', 2, '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 5, 'デスクトップ共有が閲覧出来ない', 'Cannot view Desktop Sharing', '桌面共享无法阅览', '', '', 3, '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 5, '全体的に動作が遅い', 'Slow in performance', '整体反应慢', '', '', 4, '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 5, 'ホワイトボード操作に不具合があった（機能が多くあるため、お手数ですが詳細をご記入ください）', 'Trouble with Whiteboard operation (Since there are many features, please enter details)', '白板操作有些不便（因为功能较多、麻烦您填写详细内容）', '', '', 5, '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 6, '月1回', 'Monthly', '每月1次', '', '', 1, '2011-09-29 20:12:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 6, '月2～3回程度', '2-3 times per month', '每月2~3次', '', '', 2, '2011-09-29 20:12:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 6, '月4回程度（週1回）', '4 times per month (weekly)', '每月4次（每周一次）', '', '', 3, '2011-09-29 20:12:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 6, '月4回以上', 'more than 4 times per month', '每月4次以上', '', '', 4, '2011-09-29 20:12:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 6, '不定期', 'Irregular', '不定期', '', '', 5, '2011-09-29 20:12:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 7, '営業会議', 'Sales meeting', '营业会议', '', '', 1, '2011-09-29 20:13:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 7, '企画会議', 'Planning meeting', '企划会议', '', '', 2, '2011-09-29 20:13:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 7, '報告会議', 'Debrief meeting', '报告会议', '', '', 3, '2011-09-29 20:13:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 7, '商談', 'Business negotiation', '商业谈判', '', '', 4, '2011-09-29 20:13:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 7, '研修・教育', 'Training', '培训•教育', '', '', 5, '2011-09-29 20:13:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 7, '経営会議・取締役会議', 'Management meeting/Board meeting', '经营会议•董事会议', '', '', 6, '2011-09-29 20:13:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 7, 'その他', 'Others', '其他', '', '', 7, '2011-09-29 20:13:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 8, '経営者', 'Executive', '经营者', '', '', 1, '2011-09-29 20:14:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 8, '役職者', 'Manager', '管理者', '', '', 2, '2011-09-29 20:14:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 8, '担当者', 'Person in charge', '负责人', '', '', 3, '2011-09-29 20:14:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 8, '取引先', 'Business partner', '交易户', '', '', 4, '2011-09-29 20:14:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 8, '顧客', 'Customer', '顾客', '', '', 5, '2011-09-29 20:14:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 8, '在宅勤務者', 'Homeworker', '在家办公者', '', '', 6, '2011-09-29 20:14:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 8, 'その他', 'Others', '其他', '', '', 7, '2011-09-29 20:14:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 3, '音が頻繁に途切れる', 'Lose sound very often', '经常听不到声音', '', '', 3, '2011-11-15 16:00:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
