<?php

/**
 * @author Yuki Asano <y-asano@vcube.co.jp>
 * @copyright Vcube, Inc. All Rights Reserved.
 * @version 0.1
 */

require_once( "jp/co/vcube/vcache/storages/remote/RemoteStorage.php");

abstract class LimeLightStorage extends RemoteStorage
{

  public function copyTo( $target ) {

  }
  public function moveTo( $target ) {

  }
  public function filelist() {

  }
  public function delete() {

  }
  public function exists() {

  }

}
