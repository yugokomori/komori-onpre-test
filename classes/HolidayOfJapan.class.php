<?php
//日付などをもとに日本に休日や土日を返します。
/*
春分、秋分の日は、毎年2月に翌年の日が決まるようです。
（※厳密には、国立天文台が作成する暦象年表という小冊子に
基づき、閣議で決定され、これが官報によって公報されることによって正式に決
まります）
http://koyomi.vis.ne.jp/directjp.cgi?http://koyomi.vis.ne.jp/syunbun.htm
http://www.astroarts.co.jp/news/2001/02/02nao413/index-j.shtml
*/
class HolidayOfJapan{


    var $springday; // 春分の日
    var $fallday;   // 秋分の日
    var $holidays; // 固定された休日


    function HolidayOfJapan()
    {
        $this->springday =array(2004=>20,
                                2005=>20,
                                2006=>21,
                                2007=>21,
                                2008=>20,
                                2009=>20,
                                2010=>21,
                                2011=>21,
                                2012=>20,
                                2013=>20,
                                2014=>21,
                                2015=>21);

        $this->fallday = array(2004=>23,
                                2005=>23,
                                2006=>23,
                                2007=>23,
                                2008=>23,
                                2009=>23,
                                2010=>23,
                                2011=>23,
                                2012=>22,
                                2013=>23,
                                2014=>23,
                                2015=>23);

        $this->holidays = array(1=>array(1),
                                2=>array(11),
                                3=>array(),
                                4=>array(29),
                                5=>array(3,4,5),
                                6=>array(),
                                7=>array(20),
                                8=>array(),
                                9=>array(15),
                                10=>array(),
                                11=>array(3,23),
                                12=>array(23));
    }


    /**
     * 
     * 祝日リスト
     */
    function getList()
    {
        return "1月1日    　元旦
                1月第2月曜日　成人の日
                2月11日　  建国記念の日
                3月21日頃　 春分の日
                4月29日　  みどりの日
                5月3日　   憲法記念日
                5月4日　   国民の休日
                5月5日　   こどもの日
                7月20日　  海の日
                9月15日　  敬老の日
                9月23日頃　 秋分の日
                10月第2月曜日　体育の日
                11月3日           文化の日
                11月23日  　勤労感謝の日
                12月23日  　天皇誕生日";
    }


    /**
     * 
     * 月と年から休みの日付を配列で返します
     */
    function getHoliday($month, $year)
    {
    
        $month = ltrim($month, "0");
        if($month == 1 || $month == 10){
            array_push($this->holidays[$month], $this->getSecondMonday($month, $year));
        }
        if($month == 3){
            array_push($this->holidays[$month], $this->springday[$year]);
        }
        if($month == 9){
            array_push($this->holidays[$month], $this->fallday[$year]);
        }
        return $this->holidays[$month];

    }


    /**
     * 
     * 成人の日と体育の日は第2月曜日
     * 月と年から第2月曜日の日付を返す
     */
    function getSecondMonday($month, $year)
    {
    
        for($day=8;$day<15;$day++){
            if('Mon' == date("D", mktime(0, 0, 0, $month, $day, $year))){
                $holiday = $day;
            }
        }   
        return $holiday;

    }


    /**
     * 
     * 年月日からその日が土日祝日か判定
     */
    function isHoliday($year, $month, $day)
    {
    
        $holidays = $this->getHoliday($month, $year);
        if('Sat' == date("D", mktime(0, 0, 0, $month, $day, $year))||
            'Sun'==  date("D", mktime(0, 0, 0, $month, $day, $year))||
            in_array($day, $holidays)){
            return true;
        }else{
            return false;
        }

    }


    /**
     * 携帯電話の利用時間
     * 平日8時xA訓9時　　　：\50/分 $special_charge
     * 19時xA畦眥・時＋休日：\80/分 $normal_charge
     * ※休日は、土、日曜＋日本の祝日
     * for ver4 2006/4/26
     * 
     * DB->mobile_phone_chargeテーブルから
     * mobile_phone_charge_entrytime と
     * mobile_phone_charge_time から携帯の課金時間を日ごとに算出したがって
     * mobile_phone_charge_exitとはズレはあり
     * 
     * Variables:
     * $start_time  e.g(2005-08-23 18:57:15)
     * $passed_time (sec)
     * Return:
     * array(標準の課金時間（秒）、特別な課金時間（秒）)
     * 
     * author: Oneda
     */
    function getChargeTime($start_time, $passed_time)
    {
    
        $price_start_time = 8;
        $price_end_time = 19;

        //$start_time から日付を取得
        $yea = substr($start_time, 0, 4);
        $mon = substr($start_time, 5, 2);
        $day = substr($start_time, 8, 2);
        $hou = substr($start_time, 11, 2);
        $min = substr($start_time, 14, 2);
        $sec = substr($start_time, 17, 2);

        $start_ftime = mktime($hou, $min, $sec, $mon, $day, $yea);  //特別な課金のスタート時間
        $end_ftime = mktime($hou, $min, $sec + $passed_time, $mon, $day, $yea);//終わる時間

        $next_day_ftime = mktime(0, 0, 0, $mon, $day+1, $yea);

        //日付がまたがる場合　日ごとに配列に格納
        $day++;
        $sets = array();
        while($next_day_ftime < $end_ftime){
            
            $set = array($start_ftime, $next_day_ftime, date("M-d-Y-H-i-s",$start_ftime), date("M-d-Y-H-i-s",$next_day_ftime));
            array_push($sets, $set);
            $start_ftime = $next_day_ftime;
            $day++;
            $next_day_ftime = mktime(0, 0, 0, $mon, $day, $yea);
        }
        $set = array($start_ftime, $end_ftime, date("M-d-Y-H-i-s",$start_ftime), date("M-d-Y-H-i-s",$end_ftime));
        array_push($sets, $set);
        

        //日ごとに課金時間の処理
        $special_charge_time = 0;
        $normal_charge_time = 0;
        foreach($sets as $key => $set){
        
            $start_mktime = $set[0];
            $end_mktime = $set[1];
            
            $yea = date("Y",$start_mktime);
            $mon = date("n",$start_mktime);
            $day = date("j",$start_mktime);
            
            if($this->isHoliday($yea, $mon, $day)){
                $normal_charge_time += $end_mktime - $start_mktime;
                
            }else{
                $sp_start_mktime = mktime($price_start_time, 0, 0, $mon, $day, $yea);
                $sp_end_mktime = mktime($price_end_time, 0, 0, $mon, $day, $yea);
               
                
                $start_is_in = $end_is_in = false;
                
                if($start_mktime >= $sp_start_mktime && $start_mktime < $sp_end_mktime){
                    $start_is_in = true;
                }
                if($end_mktime >= $sp_start_mktime && $end_mktime < $sp_end_mktime){
                    $end_is_in = true;
                }
                    
                if($start_is_in && $end_is_in){
                    $special_charge_time =+ $end_mktime - $start_mktime;
                    
                }elseif($start_is_in && !$end_is_in){
                    $special_charge_time += $sp_end_mktime - $start_mktime;
                    $normal_charge_time += $end_mktime - $sp_end_mktime;
                    
                }elseif(!$start_is_in && $end_is_in){
                    $special_charge_time += $end_mktime - $sp_start_mktime;
                    $normal_charge_time += $sp_start_mktime - $start_mktime;
                    
                }elseif(!$start_is_in && !$end_is_in && $start_mktime < $sp_start_mktime && $end_mktime > $sp_end_mktime){
                    $special_charge_time += $sp_end_mktime - $sp_start_mktime;
                    $normal_charge_time += $sp_start_mktime - $start_mktime;
                    $normal_charge_time += $end_mktime - $sp_end_mktime;
                
                }else{
                    $normal_charge_time += $end_mktime - $start_mktime;
                }
            }
    
        }

        return array($normal_charge_time, $special_charge_time);

    }


}

//$t = new HolidayOfJapan;

//print_r($t->getChargeTime('2005-08-23 18:57:15','143380'));

?>