<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once('classes/core/dbi/DataCenter.dbi.php');
require_once('classes/core/dbi/Document.dbi.php');
require_once('classes/core/dbi/Meeting.dbi.php');
require_once('classes/core/dbi/MeetingOptions.dbi.php');
require_once('classes/core/dbi/MeetingSequence.dbi.php');
require_once('classes/core/dbi/Participant.dbi.php');
require_once('classes/core/dbi/Options.dbi.php');
require_once('classes/dbi/reservation.dbi.php');
require_once('classes/dbi/room.dbi.php');
require_once('classes/mgm/dbi/FmsServer.dbi.php');
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_Document.class.php");
require_once("lib/EZLib/EZUtil/EZMath.class.php");
require_once("classes/dbi/ordered_service_option.dbi.php");

class API_Config extends AppFrame
{
    function init() {
        $this->dsn = $this->get_dsn();
    }

    function default_view() {
        $this->render_default();
    }

    function render_default() {
        $obj_Datacenter      = new DBI_DataCenter( N2MY_MDB_DSN );
        $obj_Document        = new DBI_Document( $this->dsn );
        $obj_Meeting         = new DBI_Meeting( $this->dsn );
        $obj_MeetingSequence = new DBI_MeetingSequence( $this->dsn );
        $obj_Participant     = new DBI_Participant( $this->dsn );
        $obj_FmsServer       = new DBI_FmsServer( N2MY_MDB_DSN );
        $obj_MeetingOptions  = new DBI_MeetingOptions( $this->dsn );
        $obj_Options         = new DBI_Options( N2MY_MDB_DSN );
        $obj_Room            = new RoomTable( $this->dsn );

        $session         = $this->session->getAll();
        $meeting_key     = $session["meeting_key"];
        $participant_key = $session["participant_key"];

        if (!is_numeric($meeting_key) || !$participant_key) {
            $this->logger->error("missing parameters.", __FILE__, __LINE__);
            return;
        }

        //GL契約部屋があるかの確認（オンプレで表示しないように対応）
        $_room_info = $this->session->get("room_info");
        $room_keys = array_keys($_room_info);
//        $where = "room_key IN ('".join("','", $room_keys)."')".
//                 " AND ordered_service_option_status = 1".
//                 " AND service_option_key = 30";
//        $obj_OrderedServiceOption  = new OrderedServiceOptionTable($this->dsn);
//        $gl_count = $obj_OrderedServiceOption->numRows($where);
//        $hasGlobalLinkOptionAccount = ($gl_count > 0) ? 1 : 0;
        $hasGlobalLinkOptionAccount = 1;
        $this->template->assign("hasGlobalLinkOptionAccount", $hasGlobalLinkOptionAccount);
        $this->logger2->debug($hasGlobalLinkOptionAccount);

        $config_info      = $this->config->getAll('CONFIG_HOST');
        $meeting_info     = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND is_deleted=0", $meeting_key ) );
        // ユーザー情報取得
        $_user_info = $this->session->get("user_info");
        require_once 'classes/dbi/user.dbi.php';
        $objUser = new UserTable($this->get_dsn());
        $where = "user_key = ". $_user_info["user_key"];
        $user_info = $objUser->getRow($where);

        // オプション情報
        $option_list = $obj_Options->getList();
        $where = "meeting_key = ".$meeting_key;
        $meeting_option_list = $obj_MeetingOptions->getRowsAssoc($where);
        foreach($meeting_option_list as $key => $row) {
            $option_name = $option_list[$row["option_key"]]["option_name"];
            $meeting_option[$option_name] = $row["meeting_option_quantity"];
        }
        $this->logger2->debug($meeting_option);

        $country = $this->session->get("country_key");
        if (!$meeting_info["intra_fms"]) {
            //除外DC
            require_once("classes/dbi/datacenter_ignore.dbi.php");
            $objDatacenterIgnore = new DatacenterIgnoreTable($this->dsn);
            $ignore_dc = $objDatacenterIgnore->get_datacenter_ignore($meeting_info["user_key"]);
            $this->logger2->debug($ignore_dc);
            $where = "status = 1";
            if ($user_info["account_model"] == "free") {
                $where .= " AND free_use_flg = 1";
            } else {
                $where .= " AND free_use_flg != 1";
            }
            if ($meeting_option["ChinaFastLine"] && $meeting_option["ChinaFastLine2"]) {
                $where .= " AND datacenter_key < 1201";
            } else if ($meeting_option["ChinaFastLine"]) {
                $where .= " AND datacenter_key < 1101";
            } else {
                $where .= " AND datacenter_key < 1001";
            }
            if ($meeting_option["GlobalLink"]) {
                $where .= " OR datacenter_key = 2001";
            }
            if($ignore_dc){
                $ignoreDC = implode(",",$ignore_dc);
                $where .= " AND datacenter_key NOT IN (". $ignoreDC .")";
                $datacenter_info = $obj_Datacenter->getRowsAssoc($where, array("datacenter_key" => "ASC"));
            } else {
                $datacenter_info = $obj_Datacenter->getRowsAssoc($where, array("datacenter_key" => "ASC"));
            }

            $server_info      = $obj_FmsServer->getRow(sprintf( "server_key='%s'", $meeting_info["server_key"]));
            // 再生時にFMSに接続できない場合
            if (($server_info["server_key"] == 1001 && $country == "cn")) {
                $fms_address = $server_info["local_address"] ? $server_info["local_address"] : $server_info["server_address"];
            } else {
                $fms_address = $server_info["server_address"];
            }
            $this->logger2->info($participant_key." : ".$fms_address);
            require_once("classes/dbi/fms_deny.dbi.php");
            $objFmsDeny = new FmsDenyTable( $this->dsn );
            $_fms_deny_keys = $objFmsDeny->findByUserKey($meeting_info["user_key"], null, null, 0, "fms_server_key");
            $fms_deny_keys = array();
            if ($_fms_deny_keys) {
                foreach ($_fms_deny_keys as $fms_key) {
                    $fms_deny_keys[] = $fms_key["fms_server_key"];
                }
            }
            if (!$obj_FmsServer->isActive($fms_address)) {
                $nowParticipantNumber = $obj_Participant->numRows('meeting_key = '.$meeting_info["meeting_key"].' AND is_active = 1');
                $this->logger2->info($nowParticipantNumber, '現在の参加人数');
                //一秒おいて再度、接続できない場合は切り替えを行う
                sleep(1);
                if (!$obj_FmsServer->isActive($fms_address)) {
                    //MCU側のカンファレンスがあれば停止
                    require_once("classes/mcu/resolve/Resolver.php");
                    if (($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($meeting_info["room_key"]))) {
                      if ($videoInfo = $videoConferenceClass->getActiveVideoConferenceByMeetingKey($meeting_key)) {
                        $videoConferenceClass->cancelReservation($videoInfo["conference_id"]);
                        sleep(3);
                      }
                    }
                    $this->logger2->warn($server_info, "FMS接続障害(自動切換え)");
                    if ($_server_key = $obj_FmsServer->getKeyByDatacenterKey($server_info["datacenter_key"], $server_info["server_country"], null, true, $fms_deny_keys)) {
                        require_once("classes/core/Core_Meeting.class.php");
                        $obj_CoreMeeting = new Core_Meeting($this->dsn, N2MY_MDB_DSN );
                        $obj_CoreMeeting->changeFMS($meeting_key, $_server_key);
                        $server_key = $_server_key;
                        $server_info = $obj_FmsServer->getRow( sprintf( "server_key='%s'", $_server_key ));
                        $fms_address = $server_info["server_address"];
                        $this->logger2->info("自動切換え成功");
                    } else {
                        $this->logger2->warn("自動切換え失敗");
                    }
                }
            }
            $reload_type = $this->session->get("reload_type");
            $this->logger2->debug($reload_type);
            $fms_front_token = "";
            $fms_front_maintenance_time = "";
            if ($server_info["datacenter_key"] == "2001" && ($server_info["server_country"] == $country)) {
                $fms_front_address = $server_info["server_address"];
                $fms_front_location = strtoupper($server_info["server_country"]);
            } else if ($server_info["datacenter_key"] == "2001") {
                $fms_country_priority = $this->config->get("FMS_COUNTRY_PRIORITY", $country);
                if (!$fms_country_priority) {
                    $fms_country_priority = $this->config->get("FMS_COUNTRY_PRIORITY", "etc");
                }
                $datacenter_country_list = split(',', $fms_country_priority);
                $is_ssl = $meeting_option["ssl"] ? $meeting_option["ssl"] : 0;
                $reboot_master_server = $this->session->get("fms_master_address");
                $reboot_front_server = $this->session->get("fms_front_address");
                if ($reload_type == "front_reboot" && $reboot_front_server) {
                    $front_server_list = $obj_FmsServer->getRowsAssoc(sprintf("local_address='%s' AND use_global_link=1", $reboot_front_server));
                    foreach ($front_server_list as $front_server) {
                        $fms_deny_keys[] = $front_server["server_key"];
                    }
                } else if ($reload_type == "master_reboot" && $reboot_master_server) {
                    $master_server_list = $obj_FmsServer->getRowsAssoc(sprintf("server_address='%s'", $reboot_master_server));
                    foreach ($master_server_list as $master_server) {
                        $fms_deny_keys[] = $master_server["server_key"];
                    }
                }
                $server_key = $obj_FmsServer->getGlobalLinkFmsList($is_ssl, $fms_deny_keys, $datacenter_country_list ,$meeting_info["need_fms_version"]);
                $front_server_info = $obj_FmsServer->getRow(sprintf("server_key='%s'", $server_key));
                $fms_front_address = $front_server_info["server_address"];
                $fms_front_location = strtoupper($front_server_info["server_country"]);
                if (!$fms_front_address) {
                    $fms_front_address = $server_info["local_address"] ? $server_info["local_address"] : $server_info["server_address"];
                }
                $reboottime = null;
                $remaining_time = "";
                if ($front_server_info["maintenance_time"]) {
                    $maintenancetime = split(":", $front_server_info["maintenance_time"]);
                    $reboottime      = mktime($maintenancetime[0], $maintenancetime[1], $maintenancetime[2]);
                    $now             = mktime(date("H"), date("i"),      date("s"));
                    if ($reboottime < $now) {
                        $reboottime = $reboottime + 86400;
                    }
                    $now5            = mktime(date("H"), date("i") + 5,  date("s"));
                    $remaining_time = $reboottime - $now5;
                }
                $fms_front_maintenance_time = $remaining_time;
                if ($meeting_info["server_key"] != $front_server_info["server_key"]) {
                    $fms_front_token = $server_info["server_key"];
                }
            }
            $this->logger2->info($participant_key." : ".$fms_front_address." (front)");
            $this->session->set("fms_master_address", $fms_address);
            $this->session->set("fms_front_address", $fms_front_address);
            $this->session->set("fms_front_location", $fms_front_location);
        } else {

            require_once("classes/dbi/user_fms_server.dbi.php");
            $objUserFmsServer       = new UserFmsServerTable($this->dsn);
            $server_info = $objUserFmsServer->getRow(sprintf("fms_key='%s'", $meeting_info['server_key']));
            $server_info["datacenter_key"] = $server_info["fms_key"];

            $datacenter_info[] = array(
                                "datacenter_key"=>$server_info["fms_key"],
                                "datacenter_name_ja"=>$server_info["server_address"],
                                "datacenter_name_en"=>$server_info["server_address"]);
        }
        $participant_info = $obj_Participant->getDetail( sprintf( "participant_key='%s'", $participant_key ) );
        $server_key       = $meeting_info["server_key"];
        $sequence_info    = $obj_MeetingSequence->getRow(sprintf( "meeting_key='%s'", $meeting_key ), null, array( "meeting_sequence_key" => "desc"));

        $where = "room_key = '". addslashes($meeting_info["room_key"])."'";
        $room_info = $obj_Room->getRow($where);
        //セールス部屋毎の事前アップロード取得
        if ($room_info["use_sales_option"]) {
            $obj_N2MYDocument = new N2MY_Document($this->dsn);
            $document_path = $user_info["user_id"]."/".$meeting_info["room_key"]."/sales/";
            $where = "document_path = '".addslashes($document_path)."'".
                " AND document_mode = 'sales_pre' AND document_status =2";
            $document_list_room = $obj_N2MYDocument->getList($where);
            $this->logger2->trace($document_list_room);
        }

        //予約事前資料
        $obj_N2MYDocument = new N2MY_Document($this->dsn);
        $where = "meeting_key = ".$meeting_key.
            " AND document_mode = 'pre'".
            " AND document_status = '".DOCUMENT_STATUS_SUCCESS."'";
        $document_list = $obj_N2MYDocument->getList($where);
        if ($document_list_room) {
            $document_list = array_merge($document_list_room, $document_list);
        }
        $this->logger2->debug($document_list);

        //set values for permission
        if ($user_info["account_model"] == "free" || $room_info["disable_rec_flg"]) {
            $participant_permission_user = "0x31000000000000BFFFFFFFFFFFFFFFFB";
        } else {
            $participant_permission_user = "0x31000000000000BFFFFFFFFFFFFFFFFF";
        }
        switch ($participant_info['participant_type_name']) {
            case 'normal':
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x110";
                $participant_info['participant_permission_user'] = $participant_permission_user;
                break;
            case 'staff':
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x210";
                if ($user_info["account_model"] == "free" || $room_info["disable_rec_flg"]) {
                  $participant_info['participant_permission_user'] = "0x110000000000003FFFFFFFFFFFFFFFFB";
                } else {
                  $participant_info['participant_permission_user'] = "0x110000000000003FFFFFFFFFFFFFFFFF";
                }
                break;
            case 'customer':
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x220";
                //$participant_info['participant_permission_user'] = $participant_permission_user;
                //$participant_info['participant_permission_user'] = "0x000000000000002000000000213FFFF2";
                $participant_info['participant_permission_user'] = "0x000000000000002000000020013FFFF2";
                //ホワイトボード権限
                if ("1" == $room_info["wb_allow_flg"]) {
                    //操作許可
                    $wb_allow_flg = "1";
                    $add_mask = "0x00000000000000000000000010000000";
                    $origin_permission = EZMath::hexbin($participant_info['participant_permission_user']);
                    $mask_bin = EZMath::hexbin($add_mask);
                    $participant_info['participant_permission_user'] = EZMath::binhex($origin_permission | $mask_bin);
                } else {
                    $wb_allow_flg = "0";
                }
                //印刷権限
                if ("1" == $room_info["print_allow_flg"]) {
                    //操作許可
                    $print_allow_flg = "1";
                    $add_mask = "0x10000000000000000000000000000000";
                    $origin_permission = EZMath::hexbin($participant_info['participant_permission_user']);
                    $mask_bin = EZMath::hexbin($add_mask);
                    $participant_info['participant_permission_user'] = EZMath::binhex($origin_permission | $mask_bin);
                } else {
                    $print_allow_flg = "0";
                }
                //チャット権限
                if ("1" == $room_info["chat_allow_flg"]) {
                    //操作許可
                    $chat_allow_flg = "7";
                    $add_mask = "0x00000000000000000000000000700000";
                    $origin_permission = EZMath::hexbin($participant_info['participant_permission_user']);
                    $mask_bin = EZMath::hexbin($add_mask);
                    $participant_info['participant_permission_user'] = EZMath::binhex($origin_permission | $mask_bin);
                } else {
                    $chat_allow_flg = "3";
                }
                //$participant_info['participant_permission_user'] = "0x".$print_allow_flg."00000000000002000000020".$wb_allow_flg."1".$chat_allow_flg."FFFF2";
                //$this->logger2->info($participant_info['participant_permission_user']);
                break;
            case 'audience':
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x120";
                $participant_info['participant_permission_user'] = "0x1000000000000000000000000DB3C020";
                //チャット権限
                if($room_info["audience_chat_flg"] == "1") {
                  $add_mask = "0x00000000000000000000000000400000";
                  $origin_permission = EZMath::hexbin($participant_info['participant_permission_user']);
                    $mask_bin = EZMath::hexbin($add_mask);
                    $participant_info['participant_permission_user'] = EZMath::binhex($origin_permission | $mask_bin);
                }
                break;
            case 'document':
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x120";
                $participant_info['participant_permission_user'] = "0x00000000000000000000000020130020";
                break;
            case 'whiteboard':
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x130";
                $participant_info['participant_permission_user'] = $participant_permission_user;
                break;
            case 'whiteboard_audience':
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x140";
                $participant_info['participant_permission_user'] = "0x1000000000000040000000000DB3C020";
                //チャット権限
                if($room_info["audience_chat_flg"] == "1") {
                  $add_mask = "0x00000000000000000000000000400000";
                  $origin_permission = EZMath::hexbin($participant_info['participant_permission_user']);
                  $mask_bin = EZMath::hexbin($add_mask);
                  $participant_info['participant_permission_user'] = EZMath::binhex($origin_permission | $mask_bin);
                }
                break;
            case 'multicamera':
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x110";
                $participant_info['participant_permission_user'] = "0x110000000000003FFFFFFFFFFFFFFFFF";
                break;
            case 'multicamera_audience':
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x120";
                $participant_info['participant_permission_user'] = "0x1000000000000000000000000DB3C020";
                //チャット権限
                if($room_info["audience_chat_flg"] == "1") {
                  $add_mask = "0x00000000000000000000000000400000";
                  $origin_permission = EZMath::hexbin($participant_info['participant_permission_user']);
                  $mask_bin = EZMath::hexbin($add_mask);
                  $participant_info['participant_permission_user'] = EZMath::binhex($origin_permission | $mask_bin);
                }
                break;
            case 'phone':
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x310";
                $participant_info['participant_permission_user'] = "";
                break;
            case 'h323':
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x320";
                $participant_info['participant_permission_user'] = "";
                break;
            case 'compact':
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x110";
                $participant_info['participant_permission_user'] = "0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
                break;
            case 'invisible_wb':
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x110";
                $participant_info['participant_permission_user'] = $participant_permission_user;
                break;
            case 'invisible_wb_audience':
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x120";
                $participant_info['participant_permission_user'] = "0x1000000000000000000000000DB3C020";
                //チャット権限
                if($room_info["audience_chat_flg"] == "1") {
                  $add_mask = "0x00000000000000000000000000400000";
                  $origin_permission = EZMath::hexbin($participant_info['participant_permission_user']);
                  $mask_bin = EZMath::hexbin($add_mask);
                  $participant_info['participant_permission_user'] = EZMath::binhex($origin_permission | $mask_bin);
                }
                break;
            case 'whiteboard_staff':
            case 'invisible_wb_staff':
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x230";
                $participant_info['participant_permission_user'] = $participant_permission_user;
                break;
            case 'whiteboard_customer':
            case 'invisible_wb_customer':
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x240";
                //$participant_info['participant_permission_user'] = $participant_permission_user;
                //$participant_info['participant_permission_user'] = "0x000000000000002000000000213FFFF2";
                $participant_info['participant_permission_user'] = "0x000000000000002000000020013FFFF2";
                //ホワイトボード権限
                if ("1" == $room_info["wb_allow_flg"]) {
                    //操作許可
                    $wb_allow_flg = "1";
                    $add_mask = "0x00000000000000000000000010000000";
                    $origin_permission = EZMath::hexbin($participant_info['participant_permission_user']);
                    $mask_bin = EZMath::hexbin($add_mask);
                    $participant_info['participant_permission_user'] = EZMath::binhex($origin_permission | $mask_bin);
                } else {
                    $wb_allow_flg = "0";
                }
                //印刷権限
                if ("1" == $room_info["print_allow_flg"]) {
                    //操作許可
                    $print_allow_flg = "1";
                    $add_mask = "0x10000000000000000000000000000000";
                    $origin_permission = EZMath::hexbin($participant_info['participant_permission_user']);
                    $mask_bin = EZMath::hexbin($add_mask);
                    $participant_info['participant_permission_user'] = EZMath::binhex($origin_permission | $mask_bin);
                } else {
                    $print_allow_flg = "0";
                }
                //チャット権限
                if ("1" == $room_info["chat_allow_flg"]) {
                    //操作許可
                    $chat_allow_flg = "7";
                    $add_mask = "0x00000000000000000000000000700000";
                    $origin_permission = EZMath::hexbin($participant_info['participant_permission_user']);
                    $mask_bin = EZMath::hexbin($add_mask);
                    $participant_info['participant_permission_user'] = EZMath::binhex($origin_permission | $mask_bin);
                } else {
                    $chat_allow_flg = "3";
                }
                //$participant_info['participant_permission_user'] = "0x".$print_allow_flg."00000000000002000000020".$wb_allow_flg."1".$chat_allow_flg."FFFF2";
                //$this->logger2->info($participant_info['participant_permission_user']);
                break;
            case 'centre':
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x110";
                $participant_info['participant_permission_user'] = "0x11000000000000BFFFFFFFFFFFFFFFFF";
                break;
            default:
                $meeting_info['meeting_permission_room']     = "";
                $participant_info['participant_type']            = "0x110";
                $participant_info['participant_permission_user'] = "0x11000000000000BFFFFFFFFFFFFFFFFF";
                break;
        }
        switch ($participant_info['participant_mode_name']) {
            case 'trial':
                $participant_info['is_trial']             = 'true';
                $participant_info['use_welcome_message']  = 'true';
                break;
            default:
                $participant_info['is_trial']             = 'false';
                $participant_info['use_welcome_message']  = 'false';
                break;
        }

        switch ($participant_info['participant_role_name']) {
            case 'invite':
                $participant_info['is_guest']     = 'true';
                if($participant_info['use_welcome_message'] == 'true'){
                    $participant_info['use_welcome_message']  = 'true';
                }else{
                    $participant_info['use_welcome_message']  = 'false';
                }
                break;
            default:
                $participant_info['is_guest']             = 'false';
                if($participant_info['use_welcome_message'] == 'true'){
                    $participant_info['use_welcome_message']  = 'true';
                }else{
                    $participant_info['use_welcome_message']  = 'false';
                }
                break;
        }
        //API（モバイルから入室の場合はメンバー課金ユーザーはis_guestを付与して、会議内招待できないように対応）
        if ($this->session->get("api_login_type") == "member" && !$user_info["external_member_invitation_flg"]) {
            $participant_info['is_guest']     = 'true';
        }
        if ($this->session->get("mobile_start_flg")) {
            $participant_info['is_guest']     = 'false';
        }

        //create values for meeting_reservation
        $meeting_start_datetime = null;
        if (isset($meeting_info['meeting_start_datetime']) && $meeting_info['meeting_start_datetime']) {
            $meeting_info['meeting_start_datetime'] = strtotime($meeting_info['meeting_start_datetime']);
        }

        $meeting_stop_datetime = null;
        if (isset($meeting_info['meeting_stop_datetime'])  && $meeting_info['meeting_stop_datetime']) {
            $meeting_info['meeting_stop_datetime']  = strtotime($meeting_info['meeting_stop_datetime']);
        }

        //sharing
        $shringSring = "";
        //ignore or intra利用時は利用不可能
        $shringSring .= $this->config->get('IGNORE_MENU','tool_sharing2_app') || 1 == $meeting_info["intra_fms"] ? 0 : 1;
        $shringSring .= $this->config->get('IGNORE_MENU','tool_sharing3_app') ? 0 : 1;
        $this->template->assign("availableSharingVersion", $shringSring);

        //set additional values
        $meeting_info['meeting_publish_id']       = substr(md5(uniqid(rand(), true)), 1, 16);

        // one time for voc
        $oneTime = array(
                    "certificate"    =>    $meeting_info['meeting_publish_id'],
                    "createtime"    =>    time());
        $this->session->set("oneTime", $oneTime);

        $app_name = $this->config->get('CORE', 'app_name', 'Vrms');
        $meeting_info['meeting_application_name'] = $app_name;
        if (isset($meeting_info['meeting_log_password']) && $meeting_info['meeting_log_password']) {
            $meeting_info['meeting_log_password'] = 'true';
        } else {
            $meeting_info['meeting_log_password'] = 'false';
        }

        if (isset($meeting_info['is_reserved'])   && $meeting_info['is_reserved']   == 1) {
            $meeting_info['is_reserved']   = 'true';
        } else {
            $meeting_info['is_reserved']   = 'false';
        }

        if (isset($participant_info['is_narrowband']) && $participant_info['is_narrowband'] == 1 && $participant_info['participant_type_name'] != "whiteboard") {
            $participant_info['is_narrowband'] = 'true';
        } else {
            $participant_info['is_narrowband'] = 'false';
        }

        // オプション情報
        $option_list = $obj_Options->getList();
        $where = "meeting_key = ".$meeting_key;
        $meeting_option_list = $obj_MeetingOptions->getRowsAssoc($where);
        foreach($meeting_option_list as $key => $row) {
            $option_name = $option_list[$row["option_key"]]["option_name"];
            $meeting_option[$option_name] = $row["meeting_option_quantity"];
        }

        //セールス＆サポート バナー、お客様映像表示
        $room_info["banner_args"] = "__NULL__";
        $room_addition = unserialize($room_info["addition"]);
        //banner_values
        if($room_addition["banner_width"]){
            $meeting_info["banner_args"] =
                    N2MY_BASE_URL."banner/".$user_info["user_id"]."/".$meeting_info["room_key"].".jpg?cachebuster=".time().",". //banner url
                    $room_addition["banner_url"].",".       // banner link
                    $room_addition["banner_width"].",".$room_addition["banner_height"];  // banner width & height
        }
        $this->logger2->debug($meeting_info);

        if ($participant_info['participant_type_name'] == "customer" && $room_info["customer_sharing_button_hide_status"] == "1") {
            $participant_info["customer_sharing_button_hide_status"] = "false";
        }

        //会議制限時間
        $login_member_info = $this->session->get('member_info');
        if ($room_info["meeting_limit_time"] == 0 && $user_info["meeting_limit_time"] > 0) {
            $room_info["meeting_limit_time"] = $user_info["meeting_limit_time"];
        } else if ($login_member_info["member_type"] == "free" && $room_info["invited_limit_time"]) {
            $room_info["meeting_limit_time"] = $room_info["invited_limit_time"];
        }
        // 予約会議だった場合パスワードの有無をチェック
        //録画禁止の場合に、自動録画ができないように
        if ($user_info["account_model"] == "free" || $room_info["disable_rec_flg"]) {
          $room_info["default_auto_rec_use_flg"] = 0;
        }
        $reservation_pw_flg = "false";
        if ("true" == $meeting_info["is_reserved"]) {
            $objReservation = new ReservationTable($this->dsn);
            $reservationInfo = $objReservation->getRow(sprintf("meeting_key='%s'", $meeting_info["meeting_ticket"]));
            $this->logger2->debug($reservationInfo);
            $this->template->assign("reservation_info", $reservationInfo);
            //入室認証済みかチェック
            $meeting_auth_flg = $this->session->get("meeting_auth_flg");
//            if ($reservationInfo["reservation_pw_type"] == "1" && $reservationInfo["reservation_pw"] && !$meeting_auth_flg) {
//                    $this->logger2->warn("password auth error");
//                    return;
//            }

//            if ($reservationInfo["reservation_pw"] && $reservationInfo["reservation_pw_type"] == "1") {
//                $reservation_pw_flg = "true";
//            }
            //マルチカメラ
            $meeting_option["multicamera"] = $reservationInfo["is_multicamera"];
            // 機能制限
            if ($reservationInfo["is_limited_function"]) {
                //音声通話
                if (!$reservationInfo["is_telephone"]) $meeting_option["telephone"] = 0;
                //H.323
                if (!$reservationInfo["is_h323"]) $meeting_option["h323"] = 0;
                //携帯
                if (!$reservationInfo["is_mobile_phone"]) $meeting_option["mobile"] = 0;
                //高画質
                if (!$reservationInfo["is_high_quality"]) $meeting_option["hispec"] = 0;
                //共有
                $meeting_option["sharing"] = $reservationInfo["is_desktop_share"];
                //ワイトボードPDF化制限
                $room_info["is_convert_wb_to_pdf"] = $room_info["is_convert_wb_to_pdf"] ? $reservationInfo["is_convert_wb_to_pdf"] : 0;
                //カスタマーカメラ
                $room_info["customer_camera_use_flg"] = $reservationInfo["is_customer_camera_flg"];
                //カスタマー映像
                $room_info["customer_camera_status"] = $reservationInfo["is_customer_camera_display"];
                //自動録画
                if($reservationInfo["is_rec_flg"])
                    $room_info["default_auto_rec_use_flg"] = $reservationInfo["is_auto_rec_flg"];
                else
                  $room_info["default_auto_rec_use_flg"] = 0;
                //暗号化
                if ($meeting_option["ssl"] == 1 && $reservationInfo["is_meeting_ssl"] == 0) {
                    // 暗号化で契約した状態で、暗号化を外す
                    $room_info["rtmp_protocol"] = "";
                }
                $meeting_option["ssl"] = $reservationInfo["is_meeting_ssl"];
                //会議室内招待
                if (!$reservationInfo["is_invite_flg"]) {
                    $this->logger2->info($participant_info['participant_permission_user']);
                    $mask = "0xFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
                    $origin_permission = EZMath::hexbin($participant_info['participant_permission_user']);
                    $mask_bin = EZMath::hexbin($mask);
                    $participant_info['participant_permission_user'] = EZMath::binhex($origin_permission & $mask_bin);
                    $this->logger2->info($participant_info['participant_permission_user']);
                }
                //録画
                if (!$reservationInfo["is_rec_flg"]) {
                    $this->logger2->info($participant_info['participant_permission_user']);
                    $mask = "0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB";
                    $origin_permission = EZMath::hexbin($participant_info['participant_permission_user']);
                    $mask_bin = EZMath::hexbin($mask);
                    $participant_info['participant_permission_user'] = EZMath::binhex($origin_permission & $mask_bin);
                    $this->logger2->info($participant_info['participant_permission_user']);
                }
                //カスタマー予約制限
                if ($participant_info['participant_type_name'] == "customer") {
                    if ("1" == $reservationInfo["is_wb_allow_flg"]) {
                    //操作許可
                    $wb_allow_flg = "1";
                    } else {
                        $wb_allow_flg = "0";
                    }
                    //印刷権限
                    if ("1" == $reservationInfo["is_print_allow_flg"]) {
                        //操作許可
                        $print_allow_flg = "1";
                    } else {
                        $print_allow_flg = "0";
                    }
                    //チャット権限
                    if ("1" == $reservationInfo["is_chat_allow_flg"]) {
                        //操作許可
                        $chat_allow_flg = "7";
                    } else {
                        $chat_allow_flg = "3";
                    }
                    $mask = "0x".$print_allow_flg."FFFFFFFFFFFFFFFFFFFFFFF".$wb_allow_flg."F".$chat_allow_flg."FFFFF";
                    //1-0,3-6補助計算マスク
                    $aid_mask = "0x10000000000000000000000010500000";
                    $origin_permission = EZMath::hexbin($participant_info['participant_permission_user']);
                    $mask_bin = EZMath::hexbin($mask);
                    $aid_mask_bin = EZMath::hexbin($aid_mask);
                    $participant_info['participant_permission_user'] = EZMath::binhex(($origin_permission | $aid_mask_bin) & $mask_bin);
                    $this->logger2->info($participant_info['participant_permission_user']);
                }
                //キャビネット
                $room_info["cabinet"] = $reservationInfo["is_cabinet_flg"];
                // ホワイトボードページ番号
                //$room_info["is_wb_no"] = $reservationInfo["is_wb_no_flg"];
            } else if ($meeting_option["multicamera"]) {
                //マルチカメラ時排他制御
                $meeting_option["telephone"] = "0";
                $meeting_option["smartphone"] = "0";
                $meeting_option["h323"] = "0";
                $meeting_option["mobile"] = "0";
            }
        } else if ($meeting_option["multicamera"]) {
            //マルチカメラ時排他制御
            $meeting_option["telephone"] = "0";
            $meeting_option["smartphone"] = "0";
            $meeting_option["h323"] = "0";
            $meeting_option["mobile"] = "0";
        }
        if (!$room_info["rtmp_protocol"]) {
            if($meeting_option["ssl"]) {
                //rtmpe + rtmps-tls
                $room_info["rtmp_protocol"] = "rtmpe:1935,rtmpe:80,rtmpe:8080,rtmps-tls:443,rtmps:443,rtmpte:80,rtmpte:8080";
            } else {
                //rtmp
                $room_info["rtmp_protocol"] = "rtmp:1935,rtmp:80,rtmp:8080,rtmps-tls:443,rtmps:443,rtmpt:80,rtmpt:8080";
            }
        }
        if (strpos($_SERVER['HTTP_USER_AGENT'], "iOS;") || strpos($_SERVER['HTTP_USER_AGENT'], "iPad;") || strpos($_SERVER['HTTP_USER_AGENT'], "Android") || strpos($_SERVER['HTTP_USER_AGENT'],"iPhone") || strpos($_SERVER['HTTP_USER_AGENT'],"iPod") || strpos($_SERVER['HTTP_USER_AGENT'],"iPad")) {
            $room_info["rtmp_protocol"] = "rtmps-tls:443,rtmps:443,rtmp:1935";
        }
        $this->template->assign("reservationPasswordFlg", $reservation_pw_flg);

        if ($participant_info['participant_type_name'] == "customer" && $room_info["customer_sharing_button_hide_status"] == "1") {
          $participant_info["customer_sharing_button_hide_status"] = "false";
        }
        if ($participant_info['participant_type_name'] == "customer" && $room_info["customer_camera_status"] == "0") {
          $participant_info["customer_camera_status"] = "true";
        }

        // メンバー且つ、センターユーザか、ターミナル
        $login_type = $this->session->get('login_type');
        if ($login_type == 'centre' || $login_type == 'terminal') {
            $room_info["default_layout"] = 'small';
        } else {
            if ($this->session->get("display_size") == "16to9") {
              if($participant_info['participant_type_name'] == "staff" || $participant_info['participant_type_name'] == "customer") {
                $blank_default_layout = "normal";
              } else {
                  if($user_info["meeting_version"] == null){
                      // 4650ユーザーの場合はデフォルト画面を修正
                      if($room_info["default_layout_16to9"] == "small"){
                          $room_info["default_layout_16to9"] = null;
                      } else if ($room_info["default_layout_16to9"] == "video,small"){
                          $room_info["default_layout_16to9"] = "video";
                      }
                      $blank_default_layout = "normal";
                  }else{
                      $blank_default_layout = "small";
                  }
              }
                $room_info["default_layout"] = $room_info["default_layout_16to9"]?$room_info["default_layout_16to9"]:$blank_default_layout;
            } else {
                $room_info["default_layout"] = $room_info["default_layout_4to3"];
            }
        }

        if ($room_info["meeting_limit_time"] > 0) {
            $this->template->assign("timeout_message_title", TIMEOUT_TITLE_FREEPLAN);
            $this->template->assign("timeout_message", TIMEOUT_MESSAGE_FREEPLAN);
        }

        // フリーのユーザーか確認
        $needUserLogin = "false";
        $freePlanAdsDir = $this->config->get("DOCUMENT", "free_plan_ads_dirname");
        $freeAdsDefaultDir = $this->config->get("DOCUMENT", "free_plan_ads_default_dirname");
        if ($room_info["meeting_limit_time"] > 0 && $user_info["account_model"] != "free") {
            $this->template->assign("useWelcomeMessage", "true");
            $this->template->assign("welcomeMessageType", "limit_time");
            $participant_info['use_welcome_message']  = 'true';
        } else if ($user_info["account_model"] == "free") {
            $needUserLogin = "true";
            $this->template->assign("useWelcomeMessage", "true");
            $this->template->assign("welcomeMessageType", "free");
            $participant_info['use_welcome_message']  = 'true';
        }
        //広告の表示確認
        //フリーの場合は初期表示の広告も取得
        if ($user_info["account_model"] == "free" || $user_info["user_whiteboard_advertise"]) {
            if ($freePlanAdsDir) {
                $document_path = $freePlanAdsDir."/";
                $agency_id = "";
                try{
                    $wsdl = $this->config->get('VCUBEID','wsdl');
                    $serverName = $_SERVER["SERVER_NAME"];
                    if($serverName == $this->config->get('VCUBEID','meeting_server_name')){
                      //meetingFree
                      $consumerKey = $this->config->get('VCUBEID','meeting_free_consumer_key');
                    }elseif($serverName == $this->config->get('VCUBEID','paperless_server_name')){
                      //paperLess
                      $consumerKey = $this->config->get('VCUBEID','paperless_free_consumer_key');
                    }
                    require_once("classes/dbi/member_room_relation.dbi.php");
                    $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
                    $where = "room_key = '".$meeting_info["room_key"]."'";
                    $relation_info = $objRoomRelation->getRow($where);
                    if ($relation_info) {
                        require_once("classes/dbi/member.dbi.php");
                        $objMember = new MemberTable($this->get_dsn());
                        $where = "member_key = ".$relation_info["member_key"];
                        $room_member_info = $objMember->getRow($where, "member_id");
                        if ($room_member_info) {
                            $soap = new SoapClient($wsdl,array('trace' => 1));
                            $response = $soap->getAgencyId($consumerKey, $room_member_info["member_id"]);
                            $this->logger2->debug($response);
                            $agency_id = $response["agencyId"];
                        }
                    }
                  }
                catch(Exception $e){
                    $this->logger2->warn($e->getMessage());
                }
                //代理店毎の広告を表示。指定がなければデフォルト
                if ($agency_id) {
                    $document_path .= $agency_id."/";
                } else {
                    $document_path .= $freeAdsDefaultDir."/";
                }
                $where = "document_path = '".addslashes($document_path)."'".
                    " AND document_mode = 'pre'".
                    " AND document_status = '".DOCUMENT_STATUS_SUCCESS."'";
                $document_list_free = $obj_N2MYDocument->getList($where);
                $document_list = array_merge($document_list_free, $document_list);
            }
        }
        $this->template->assign("needUserLogin", $needUserLogin);

        // キャビネット無効化
        if ($room_info["cabinet"] == "0") {
            $this->logger2->info($participant_info['participant_permission_user']);
            $mask = "0xFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFF";
            $origin_permission = EZMath::hexbin($participant_info['participant_permission_user']);
            $mask_bin = EZMath::hexbin($mask);
            $participant_info['participant_permission_user'] = EZMath::binhex($origin_permission & $mask_bin);
            $this->logger2->info($participant_info['participant_permission_user']);
        }
        //ホワイトボードPDF化制限
        if ($room_info["is_convert_wb_to_pdf"] == "0") {
          $this->logger2->info($participant_info['participant_permission_user'],"ホワイトボードPDF化制限");
          $mask = "0x1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
          $origin_permission = EZMath::hexbin($participant_info['participant_permission_user']);
          $mask_bin = EZMath::hexbin($mask);
          $participant_info['participant_permission_user'] = EZMath::binhex($origin_permission & $mask_bin);
          $this->logger2->info($participant_info['participant_permission_user']);
        }
        $participant_info['reload_type'] = $this->session->get('reload_type');

        if ($room_info["cabinet_filetype"]) {
            $cabinet_filetype = @unserialize($room_info["cabinet_filetype"]);
            if (is_array($cabinet_filetype)) {
                $room_info["cabinet_filetype"] = join(",", $cabinet_filetype);
            } else {
                $room_info["cabinet_filetype"] = "";
            }
        }
        //セールス会議室内招待が制限になる
        if ($meeting_info["is_reserved"] == "true" && $participant_info['participant_type_name'] == "staff") {
          $this->logger2->info($participant_info['participant_permission_user']);
          $mask = "0xFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
          $origin_permission = EZMath::hexbin($participant_info['participant_permission_user']);
          $mask_bin = EZMath::hexbin($mask);
          $participant_info['participant_permission_user'] = EZMath::binhex($origin_permission & $mask_bin);
          $this->logger2->info($participant_info['participant_permission_user']);
        }

        if($meeting_option["sharing"] == 0 && $meeting_info["is_reserved"] == "true") {
            $add_mask_white_sync = "0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEF";
            $origin_permission = EZMath::hexbin($participant_info['participant_permission_user']);
            $mask_bin = EZMath::hexbin($add_mask_white_sync);
            $participant_info['participant_permission_user'] = EZMath::binhex($origin_permission & $mask_bin);
        }

        // 回線速度設定
        $netspeed_check = $this->get_message("NET_SPEED_LIST", $room_info["is_netspeed_check"]);
        $room_info["is_netspeed_check"] = ($netspeed_check) ? substr($netspeed_check, 0, strpos($netspeed_check, ":")) : "";
        // 自動トランシーバー切り替え人数
        if ($room_info["is_auto_transceiver"]) {
            if (!$room_info["transceiver_number"]) $room_info["transceiver_number"] = 11;
        } else {
            $room_info["transceiver_number"] = 100;
        }
        $room_info["is_auto_transceiver"] = false;

        //接続FMSサーバを登録
        $where = "participant_key = ".$participant_key;
        $fms_server_key = $front_server_info["server_key"] ? $front_server_info["server_key"] : $server_info["server_key"];
        $fms_data = array("fms_server_key" => $fms_server_key);
        $obj_Participant->update($fms_data, $where);

        $endlink = urlencode(N2MY_BASE_URL ."services/api.php?action_reenter_meeting&room_key=" .$meeting_info["room_key"]."&meeting_ticket=" .$meeting_info["meeting_ticket"]."&is_narrow=0&type=staff&fl_ver=".$session["fl_ver"]);
        $this->template->assign("endlink" , $endlink);

        //assign values
//        $this->template->assign("c" , $config_info);
        $this->template->assign("member_info" , $this->session->get('member_info'));
        $this->template->assign("d" , $datacenter_info);
        $this->template->assign("document_list" , $document_list);
        $this->template->assign("m" , $meeting_info);
        $this->template->assign("mo", $meeting_option);
        $this->template->assign("p" , $participant_info);
        $this->template->assign("s" , $server_info);
        $this->template->assign("fms_address" , $fms_address);
        $this->template->assign("fms_front_address" , $fms_front_address);
        $this->template->assign("fms_front_token" , $fms_front_token);
        $this->template->assign("fms_front_location" , $fms_front_location);
        $this->template->assign("fms_front_maintenance_time" , $fms_front_maintenance_time);
        $this->template->assign("member" , $this->session->get("member_info"));
        $this->template->assign("country" , $country);
        $mcu_profile_type = "";
        if ($user_info["meeting_version"] >= "4.7.0.0" && $this->config->get('N2MY', 'mcu_profile_type')) {
            $mcu_profile_type = $this->config->get('N2MY', 'mcu_profile_type');

        }
        $this->template->assign("mcu_profile_type" , $mcu_profile_type);
        // 対応フォーマット取得
        require_once ("classes/N2MY_Document.class.php");
        $objDocument = new N2MY_Document($this->get_dsn());
        $support_document_list = $objDocument->getSupportFormat($meeting_info["room_key"]);
        if (isset($support_document_list['document_ext_list'])) {
            $room_info["document_type_list"] = join(",", $support_document_list['document_ext_list']);
        }
        if (isset($support_document_list['image_ext_list'])) { {}
          $room_info["image_type_list"]    = join(",", $support_document_list['image_ext_list']);
        }
        $this->logger2->debug(array(
            "document_type_list" => $room_info["document_type_list"],
            "image_type_list"    => $room_info["image_type_list"]
            ));
        $user_info["addition"] = unserialize($user_info["addition"]);
        $this->logger2->debug(array(
            "user_info" => $user_info,
            "room_info" => $room_info,
            "reservation_info" => $reservationInfo
            ));
        $this->template->assign("user_info" , $user_info);
        $extension = array();
        $total_seat = $meeting_info["meeting_max_seat"] + $meeting_info["meeting_max_audience"];
        $bandwidth_level = ceil($total_seat / 10);
        $maxRoomBandwidth = $bandwidth_level * 2048;
        //帯域
        if ($maxRoomBandwidth < $room_info["max_room_bandwidth"]) {
            $extension["maxRoomBandwidth"] = $room_info["max_room_bandwidth"];
        } else {
            $extension["maxRoomBandwidth"] = $maxRoomBandwidth;
        }
        $extension["maxUserBandwidth"] = $room_info["max_user_bandwidth"] ? $room_info["max_user_bandwidth"] : 256;
        //高画質
        if ($meeting_option["hispec"] > 0) {
            $hispecBandWidth = $meeting_option["hispec"] * 2048;
            $extension["maxHighQualityRoomBandwidth"] = $extension["maxRoomBandwidth"] + $hispecBandWidth;
            if ($room_info["max_user_bandwidth"] > 512) {
                $extension["maxHighQualityUserBandwidth"] = $room_info["max_user_bandwidth"];
            } else {
                $extension["maxHighQualityUserBandwidth"] = 512;
            }
        } else {
            $extension["maxHighQualityRoomBandwidth"] = $room_info["max_room_bandwidth"] ? $room_info["max_room_bandwidth"] : 2048;
            $extension["maxHighQualityUserBandwidth"] = $room_info["max_user_bandwidth"] ? $room_info["max_user_bandwidth"] : 256;
        }
        if ($meeting_option["minimumBandwidth80"]) {
            $extension["minUserBandwidth"] = 80;
        } else {
            $extension["minUserBandwidth"] = $room_info["min_user_bandwidth"] ? $room_info["min_user_bandwidth"] : 30;
        }
        //デフォルトカメラサイズ
        if ($room_info["default_camera_size"] && $room_info["default_camera_size"] != 0) {
            $camera_size = explode("x", $room_info["default_camera_size"]);
            $room_info["default_camera_width"] = $camera_size[0];
            $room_info["default_camera_height"] = $camera_size[1];
        }
        if ($room_info["default_camera_width"] > 320) {
            $room_info["default_highquality_camera_width"] = $room_info["default_camera_width"];
        }
        if ($room_info["default_camera_height"] > 240) {
            $room_info["default_highquality_camera_height"] = $room_info["default_camera_height"];
        }

        //h.264
        $enable_transcoder = 0;
        if ($meeting_option["h264"] && $room_info["default_h264_use_flg"] == 1) {
            $extension["video_codec"] = "h264avc";
            //configでも有効であれば、Transcorderも有効にする
            $enable_transcoder = $this->config->get('N2MY', 'enable_h264_transcoder') ? 1 : 0;
            $this->template->assign("enable_transcoder", $enable_transcoder);
        } else {
            $extension["video_codec"] = "";
        }

        //Sound Codec
        if ($user_info["meeting_version"] >= "4.7.0.0" && $room_info["use_sound_codec"] != "nellymoser") {
            //$extension["sound_codec"] = "speex";
            $extension["sound_codec"] = "";
        } else {
            $extension["sound_codec"] = "";
        }

        $this->template->assign("room_info" , $room_info);

        //CabinetのPATH
        $dateInfo = date_parse( $meeting_info["create_datetime"] );
        $cabinet_path = sprintf( "%04d%02d/%02d/", $dateInfo["year"], $dateInfo["month"], $dateInfo["day"]);
        $this->template->assign("cabinet_path", $cabinet_path);
        //$this->logger->info(__FUNCTION__,__FILE__,__LINE__,array($meeting_info, $extension));
        // オーディエンス契約数に応じて帯域を変更
        $this->template->assign("extension", $extension);
        //server host key
        $this->template->assign("server_dsn_key", $this->get_dsn_key() );
        //シークエンスキー
        $this->template->assign("meeting_sequence_key", $sequence_info["meeting_sequence_key"]);
        // セッションの値
        $this->template->assign("session", $session);
        // ユーザごとの同時接続ユーザー数制限値取得
        $this->template->assign("user_limit", isset($user_info["meeting_max_connecct"]) ? $user_info["meeting_max_connecct"] : "");

        // HTTP
        if ($_SERVER["SERVER_PORT"] == 80) {
            $http_schema = "http";
            $http_port = "80";
        } elseif ($_SERVER["SERVER_PORT"] == 443) {
            $http_schema = "https";
            $http_port = "443";
        } else {
            $http_schema = "http";
            $http_port = $_SERVER['SERVER_PORT'];
        }
        $base_url = N2MY_BASE_URL;
        if ($this->session->get("invitedGuest") || $this->session->get("login_type") == "invitedGuest" || $this->session->get("is_invite")) {
            $base_url = N2MY_BASE_URL ? N2MY_BASE_GUEST_URL :  N2MY_BASE_URL;
        }
        $this->template->assign("base_url", $base_url);
        $base_guest_url = N2MY_BASE_GUEST_URL ? N2MY_BASE_GUEST_URL : N2MY_BASE_URL;
        $this->template->assign("base_guest_url", $base_guest_url);
        $live_base_url = N2MY_LIVE_URL;
        $this->template->assign("live_base_url", $live_base_url);

        //会議内からの招待メールのためユーザー、オーディエンス用のハッシュ値を生成
        require_once "classes/N2MY_Invitation.class.php";
        $objInvitation = new N2MY_Invitation($this->dsn);

        require_once "classes/dbi/meeting_invitation.dbi.php";
        $objMeetingInvitationTable = new MeetingInvitationTable($this->dsn);
        if (!$invitationData = $objMeetingInvitationTable->getRow(
                                                            sprintf("meeting_key='%s' AND status = 1", $meeting_key),
                                                            "*",
                                                            array("invitation_key"=>"desc"))) {
            $user_session_id = md5(uniqid(rand(), 1));
            $audience_session_id = md5(uniqid(rand(), 1));
            $invitationData = array(
                                "meeting_key" => $meeting_key,
                                "meeting_session_id" => $meeting_info["meeting_session_id"],
                                "user_session_id" => $user_session_id,
                                "audience_session_id" => $audience_session_id,
                                "status" => 1
                                );
            if ($meeting_info["is_reserved"])
                $invitationData["meeting_ticket"] = $meeting_info["meeting_ticket"];
            $objMeetingInvitationTable->add($invitationData);
        }
        $this->template->assign("typeNormal", $invitationData["user_session_id"]);
        $this->template->assign("typeAudience", $invitationData["audience_session_id"]);

        $telephone_data = array();
        if ($meeting_option["telephone"]) {
            $tel_no = $this->config->getAll("TELEPHONE");
            $location_list = $this->get_message("TELEPHONE");
            foreach($location_list as $key => $location) {
                $telephone_data[$key]["name"] = $location;
                $telephone_data[$key]["list"] = split(",", $tel_no[$key]);
            }
        }
        //ポリコム連携
        require_once("classes/mcu/resolve/Resolver.php");
        if (($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($meeting_info["room_key"]))) {
          $ivesSettingInfo = $videoConferenceClass->getIvesInfoByRoomKey($meeting_info["room_key"]);
          if ($ivesSettingInfo) {
            $video_conference_address = "(SIP,H323)".$videoConferenceClass->getRegularAddress($ivesSettingInfo["ives_did"]);
            $this->template->assign("video_conference_address", $video_conference_address);
            $this->template->assign("ignore_video_conference_address", $ivesSettingInfo["ignore_video_conference_address"]);
          }
          else {
        $this->logger2->warn($meeting_info, "set video_conference_address error");
          }
        }
        // echo cancel
        $this->template->assign('enableEchoCancel', !$this->config->get("IGNORE_MENU", "echo_canceler"));
        if (strpos($_SERVER["HTTP_USER_AGENT"], "Windows") !== false) {
             if (strpos($_SERVER["HTTP_USER_AGENT"], "MSIE") !== false) {
                $this->template->assign('enableActiveX', 1);
             }
        }

        $this->template->assign("enable_teleconf", $this->getEnableTeleconf($meeting_info["room_key"], $user_info));
        $this->template->assign("lang", $this->session->get("lang"));
        // ミーティングID
        $meeting_id = $obj_Meeting->getMeetingID($meeting_key);
        $this->template->assign("meeting_id", $meeting_id);
        $this->template->assign("telephone_data", $telephone_data);
        // highPriorityMicrophoneNames
        $this->template->assign("highPriorityMicrophoneNames", $this->config->get('ECHO_CANCEL', 'highPriorityMicrophoneNames'));
        // excludeEchoCancelNames
        $this->template->assign("excludeEchoCancelNames", $this->config->get('ECHO_CANCEL', 'excludeEchoCancelNames'));
        $this->logger2->debug($participant_info['participant_permission_user']);
        $this->logger2->debug(sprintf( "core/api/meeting_config/meeting/resources/config_%s.t.xml", $participant_info['participant_type_name'] ));
        //display xml_template
        $this->display( sprintf( "core/api/meeting_config/meeting/resources/config_%s.t.xml", $participant_info['participant_type_name'] ) );
    }

    private function getEnableTeleconf($room_key, $user)
    {
        //$is_trial        = $user_info['user_status'] == 2;

        if ($this->config->get("IGNORE_MENU", "teleconference")) {
            return false;
        }
        if ($user['user_status'] == 2) { // trial ?
            return false;
        }
        $service_option  = new OrderedServiceOptionTable($this->dsn);
        if (!$service_option->enableOnRoom(23, $room_key)) { // 23=teleconf
            return false;
        }

        return true;
    }
}

$main = new API_Config();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
