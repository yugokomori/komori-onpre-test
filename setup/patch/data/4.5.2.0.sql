-- 電話連携
ALTER TABLE `meeting` ADD `pin_cd` VARCHAR( 8 ) NULL COMMENT 'PINコード' AFTER `meeting_session_id`;

-- 録画GW
ALTER TABLE `meeting_sequence` ADD `record_second` DOUBLE NOT NULL COMMENT '録画時間' AFTER `has_recorded_video`;
ALTER TABLE `meeting_sequence` ADD `record_id` VARCHAR( 50 ) NULL COMMENT '動画生成発行ID' AFTER `record_second`;
ALTER TABLE `meeting_sequence` ADD `record_endtime` DATETIME NULL COMMENT '動画生成完了時間' AFTER `record_id`;
ALTER TABLE `meeting_sequence` ADD `record_status` VARCHAR( 20 ) NULL COMMENT '動画生成状態' AFTER `record_endtime`;
ALTER TABLE `meeting_sequence` ADD `record_result` VARCHAR( 20 ) NULL COMMENT '動画生成結果' AFTER `record_status`;
ALTER TABLE `meeting_sequence` ADD `record_end_proc` VARCHAR( 50 ) NULL COMMENT '完了後の処理' AFTER `record_result`;
ALTER TABLE `meeting_sequence` ADD `record_filepath` TEXT NULL COMMENT '動画生成後のパス' AFTER `record_end_proc`;
