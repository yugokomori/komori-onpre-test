<?php
require_once("classes/N2MY_DBI.class.php");

class MgmRelationTable extends N2MY_DB {

    function __construct( $dsn ) {
        $this->init( $dsn, "relation" );
    }

    public function add( $data )
    {
        $data["create_datetime"] = date( "Y-m-d H:i:s" );
        return parent::add( $data );
    }
}
