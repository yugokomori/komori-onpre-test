<?php
#---------
# 環境設定
#---------
# アプリケーションルートディレクトリ
define('N2MY_APP_DIR',      '{N2MY_APP_DIR}/');
# バーチャル毎に設定を切り替えたい場合のみ、.htaccessなどで指定してください
define('N2MY_CONFIG_FILE',   isset($_SERVER['CONFIG_FILE']) ? $_SERVER['CONFIG_FILE'] : N2MY_APP_DIR."config/config.ini");
# video conference設定ファイル
define('N2MY_VC_CONFIG_FILE',   N2MY_APP_DIR."config/mcu_config.ini");
define('N2MY_MCU_SERVER_XML',   N2MY_APP_DIR."config/mcu.xml");
# PGi Hybrid設定ファイル
define('N2MY_PGI_CONFIG_FILE',   N2MY_APP_DIR."config/pgi_config.ini");
# ドキュメントルートパス
define('N2MY_DOCUMENT_ROOT', N2MY_APP_DIR.'htdocs/');
# 資料変換時のディレクトリ
define('N2MY_DOC_DIR',       N2MY_DOCUMENT_ROOT.'docs/');
# ファイルキャビネットディレクトリ
define('N2MY_FILES_DIR',     N2MY_DOCUMENT_ROOT.'files/');
# 共有メモのディレクトリ
define('N2MY_SHARE_DIR',     N2MY_APP_DIR.'shared/');
# 録画GW生成先ディレクトリ
define('N2MY_MOVIES_DIR',    N2MY_APP_DIR.'webapp/movies/');
# ベースURL
define('N2MY_BASE_URL',      '{N2MY_BASE_URL}');
# サーバ内から実行する差異のURL
define('N2MY_LOCAL_URL',     '{N2MY_LOCAL_URL}');
# PHPディレクトリ
define('N2MY_PHP_DIR',       '{N2MY_PHP_DIR}');
# Rubyディレクトリ
define('N2MY_RUBY_DIR',      '{N2MY_RUBY_DIR}');
# ImageMagickディレクトリ
define('N2MY_IMGMGC_DIR',    '{N2MY_IMGMGC_DIR}');
# デフォルトDB
define('N2MY_DEFAULT_DB',    '{N2MY_DEFAULT_DB}');
# 可逆暗号化
define('N2MY_ENCRYPT_KEY',   '{N2MY_ENCRYPT_KEY}');
# 可逆暗号化 IV(８文字)
define('N2MY_ENCRYPT_IV',    '{N2MY_ENCRYPT_IV}');
# 可逆暗号化2
define('VCUBE_ENCRYPT_KEY',   '{VCUBE_ENCRYPT_KEY}');
# 可逆暗号化2 IV(８文字)
define('VCUBE_ENCRYPT_IV',    '{VCUBE_ENCRYPT_IV}');
# GIPS共通鍵
define('N2MY_IDENTITY_KEY',  '{N2MY_IDENTITY_KEY}');
# fmsとのSCPコマンドを行うユーザー名
define('N2MY_SCP_USER',  '{N2MY_SCP_USER}');
# mcuとのSCPコマンドを行うユーザー名
define('N2MY_MCU_SCP_USER',  '{N2MY_MCU_SCP_USER}');

#---------
# メールアドレス
#---------
# メールアドレス設定
ini_set("SMTP", '{SMTP}');
# NO REPLY
define('NOREPLY_ADDRESS',      '{NOREPLY_ADDRESS}');
# メール送信元
define('N2MY_MAIL_FROM',      '{N2MY_MAIL_FROM}');
# アカウント発行情報の送信先
define('N2MY_ACCOUNT_TO',     '{N2MY_ACCOUNT_TO}');
# アカウント発行情報の送信元
define('N2MY_ACCOUNT_FROM',   '{N2MY_ACCOUNT_FROM}');
# サービス管理者問い合わせ先
define('ADMIN_INQUIRY_TO',    '{ADMIN_INQUIRY_TO}');
# サービス管理者問い合わせ元
define('ADMIN_INQUIRY_FROM',  '{ADMIN_INQUIRY_FROM}');
# ユーザページお問い合わせ先
define('USER_ASK_TO',         '{USER_ASK_TO}');
# ユーザページお問い合わせ元
define('USER_ASK_FROM',       '{USER_ASK_FROM}');
# パスワードリセットお問い合わせ先（管理者メールアドレスが登録されていない場合）
define('PW_RESET_TO',         '{PW_RESET_TO}');
# パスワードリセットお問い合わせ元
define('PW_RESET_FROM',       '{PW_RESET_FROM}');
# 予約時の送信元
define('RESERVATION_FROM',    '{RESERVATION_FROM}');
# システムの警告メール送信元
define('N2MY_ALERT_TO',       '{N2MY_ALERT_TO}');
# システムの警告メール送信先
define('N2MY_ALERT_FROM',     '{N2MY_ALERT_FROM}');
# 会議記録移動エラーメール送信先
define('N2MY_LOG_ALERT_TO',   '{N2MY_LOG_ALERT_TO}');
# 会議記録移動エラーメール送信元
define('N2MY_LOG_ALERT_FROM', '{N2MY_LOG_ALERT_FROM}');
# システムログがエラー以上の場合に警告メールを送信
define('N2MY_ERROR_FROM',     '{N2MY_ERROR_FROM}');
# システムログがエラー以上の場合に警告メールを送信
define('N2MY_ERROR_TO',       '{N2MY_ERROR_TO}');
# Cron実行メールを送信
define('N2MY_CRON_FROM',     '{N2MY_ERROR_FROM}');
# Cron実行メールを送信
define('N2MY_CRON_TO',       '{N2MY_ERROR_TO}');
#メール設定箇所に
# MCUアラートFROM
define('N2MY_MCU_NOTIFICATION_FROM',     '{N2MY_MCU_NOTIFICATION_FROM}');
# MCUアラートTO
define('N2MY_MCU_NOTIFICATION_TO',       '{N2MY_MCU_NOTIFICATION_TO}');

#---------
# アプリケーション設定
#---------
# ユーザー用メインメニュー会議室表示件数
define('MAINMENU_MAX_VIEW', '{MAINMENU_MAX_VIEW}');
# 会議予約一覧表示件数
define('RESERVATION_MAX_VIEW', '{RESERVATION_MAX_VIEW}');
# 録画容量(Mbyte)
define('RECORD_SIZE', '{RECORD_SIZE}');
# サーバー側で使用したCO2（1人/分）
define('ECO_CLIENT_CO2', 2.053628);
# ログレベルのエラー検出時のメール送信フラグ
define("N2MY_ERROR_NOTIFICATION", '{N2MY_ERROR_NOTIFICATION}');
# ストレージ容量 1ユーザー
define('STORAGE_USER_SIZE' , 500);
# ストレージ容量 1部屋
define('STORAGE_ROOM_SIZE' , 500);
# ストレージ容量 オプション
define('STORAGE_OPTION_SIZE' , 1000);
# 登録可能バナー横幅
define('BANNER_WIDTH_MAX', 500);
# 登録可能バナー縦幅
define('BANNAR_HEIGHT_MAX', 300);


#---------
# php.ini
#---------
ini_set("display_errors",  '{display_errors}');
ini_set("log_errors",      '{log_errors}');
ini_set("error_reporting", '{error_reporting}');
ini_set("error_log",       N2MY_APP_DIR.'logs/php_error.log');

#---------
# session
#---------
ini_set("session.save_path", N2MY_APP_DIR.'tmp');
ini_set("session.gc_maxlifetime", '{session.gc_maxlifetime}');
ini_set("session.cache_limiter", 'none');
ini_set("session.save_handler", 'files');

#---------
# API利用時のセッション名 (.htaccessのSetEnvで変更可能)
#---------
if (!defined('N2MY_SESSION')) {
    define('N2MY_SESSION', 'n2my_session');
}

class N2MY_SESSION_DB {

    var $sess_db = null;

    function sess_open() {
        require_once('lib/EZLib/EZCore/EZConfig.class.php');
        require_once('lib/EZLib/EZDB/EZDB.class.php');
        $config = EZConfig::getInstance(N2MY_CONFIG_FILE);
        EZLogger::getInstance($config);
        EZLogger2::getInstance($config);
        $auth_dsn = $config->get("GLOBAL", "auth_dsn");
        $this->sess_db = new EZDB();
        $this->sess_db->init($auth_dsn, "php_session");
    }

    function sess_close() {
    }

    function sess_read($id) {
        $where = "id = '".mysql_real_escape_string($id)."'";
        $data = $this->sess_db->getOne($where, "data");
        return $data;
    }

    function sess_write($id, $data) {
        $access = time();
        $data = array(
            "id" => $id,
            "access" => $access,
            "data" => $data,
        );
        return $this->sess_db->replace($data);
    }

    function sess_destroy($id) {
        $where = "id = '".mysql_real_escape_string($id)."'";
        return $this->sess_db->remove($where);
    }

    function sess_clean($max) {
        $old = time() - $max;
        $where = "access = '".mysql_real_escape_string($old)."'";
        return $this->sess_db->remove($where);
    }
}

if (ini_get("session.save_handler") == "user") {

    $_n2my_session = new N2MY_SESSION_DB();
    session_set_save_handler(
        array($_n2my_session, 'sess_open'),
        array($_n2my_session, 'sess_close'),
        array($_n2my_session, 'sess_read'),
        array($_n2my_session, 'sess_write'),
        array($_n2my_session, 'sess_destroy'),
        array($_n2my_session, 'sess_clean')
        );
}
