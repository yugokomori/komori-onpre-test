<?php
/*
 * Created on 2008/01/31
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once("classes/N2MY_Api.class.php");

class N2MY_Meeting_API extends N2MY_Api
{
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        $this->_name_space = md5(__FILE__);
    }

    /**
     * 認証処理
     *
     * @param
     * @return
     */
    function auth()
    {
        if ("action_invite_start" != $this->getActionName() && "action_meeting_display" != $this->getActionName()) {
            $this->checkAuthorization();
        }
    }

    function action_invite_start() {
        require_once 'classes/mgm/MGM_Auth.class.php';
        require_once 'classes/N2MY_Auth.class.php';
        require_once 'classes/N2MY_Account.class.php';
        require_once 'classes/dbi/meeting.dbi.php';
        require_once 'classes/dbi/user.dbi.php';
        require_once 'classes/N2MY_Meeting.class.php';
        // DB選択
        $request = $this->request->getAll();
        $meeting_id  = $request[API_MEETING_ID];
        $output_type = $this->request->get("output_type");
        // パラメタチェック
        $rules = array(
            API_MEETING_ID => array(
                "required" => true,
                "valid_integer" => true,
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        $dsn_info = $obj_MGMClass->getRelationDsn($meeting_id, "meeting");
        $server_info = $obj_MGMClass->getServerInfo($dsn_info["server_key"]);
        if (!$dsn_info) {
            $err_msg["errors"][] = array(
                "field"   => API_MEETING_ID,
                "err_cd"  => "invalid",
                "err_msg" => "ミーティングキーが不正です",
                );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        // 会議情報
        $objMeeting = new MeetingTable($server_info["dsn"]);
        $where = "meeting_session_id = '".addslashes($meeting_id)."'" .
            " AND is_active = 1" .
            " AND is_deleted = 0";
        $meeting_info = $objMeeting->getRow($where);
        if (!$meeting_info) {
            $err_msg["errors"][] = array(
                "field"   => API_MEETING_ID,
                "err_cd"  => "invalid",
                "err_msg" => "ミーティングキーが不正です",
                );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        if ($meeting_info["is_reserved"] &&
            ((strtotime($meeting_info['meeting_start_datetime']) > time() ||
             (strtotime($meeting_info['meeting_stop_datetime'])) < time())
            )
        ) {
            $err_msg["errors"][] = array(
                "field"   => API_MEETING_ID,
                "err_cd"  => "invalid",
                "err_msg" => "予約時間内ではありません",
                );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        // ユーザー
        $objUser = new UserTable($server_info["dsn"]);
        $where = "user_key = ".$meeting_info["user_key"];
        $user_info = $objUser->getRow($where);
        if (!$user_info) {
            return $this->display_error("101", "User not found", $this->get_error_info($err_obj));
        }
        $lang = $this->get_language();
        $lang_conf_file = N2MY_APP_DIR."config/lang/".$lang."/message.ini";
        $this->_message = parse_ini_file($lang_conf_file, true);
        // パラメタ取得
        $id          = $user_info["user_id"];
        $pw          = $user_info["user_password"];
        $lang        = $this->request->get("lang", "ja");
        $country     = $this->request->get("country", "jp");
        $time_zone   = $this->request->get(API_TIME_ZONE, "9");
        $enc         = $this->request->get("enc", null);
        $rules = array(
            "lang"     => array(
                "allow" => array_keys($this->get_language_list()),
                ),
            "country"     => array(
                "allow" => array_keys($this->get_country_list()),
                ),
            API_TIME_ZONE     => array(
                "allow" => array_keys($this->get_timezone_list()),
                ),
        );
        $request = $this->request->getAll();
        //古いAPI利用者用対応(zhの場合はzh-cnに変更)
        if ($request["lang"] == "zh") {
            $request["lang"] = "zh-cn";
        }
        $err_obj = $this->error_check($request, $rules);
        if (!$user_info = $obj_MGMClass->getUserInfoById( $id ) ){
            $err_obj->set_error("id", "SELECT_USER_ERROR", $id);
        } else {
            if (!$server_info = $obj_MGMClass->getServerInfo( $user_info["server_key"] )){
                $err_obj->set_error("", "SELECT_USER_ERROR", $id);
                return $this->display_error("1", "SELECT_SERVER_ERROR");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            $login = new N2MY_Auth($server_info["dsn"]);
            $user_type = "";
            // ユーザ確認
            $login_info = $login->check($id, $pw, $enc);
            if (!$login_info) {
                return $this->display_error("1", "USER_ID_PASS_INVALID");
            } else {
                // セッションスタート
                $session = EZSession::getInstance();
                // ID を取得
                $session_id = session_id();
                // ユーザ情報配置
                if  ($output_type) {
                    $session->set("output_type", $output_type);
                }
                $session->set("login", "1");
                $session->set("lang", $lang);
                $session->set("country_key", $country);
                $session->set("time_zone", $time_zone);
                $session->set("dsn_key", $server_info["host_name"]);
                $session->set("server_info", $server_info);
                // ユーザー情報
                $user_info = $login_info["user_info"];
                $session->set("user_info", $login_info["user_info"]);
                $type = "user";
                // 使用可能な部屋
                $obj_N2MY_Account = new N2MY_Account($server_info["dsn"]);
                $rooms = $obj_N2MY_Account->getOwnRoomByRoomKey( $meeting_info["room_key"], $user_info["user_key"]);
                // 既存のセッションデータにも書き込む
                $session->set('room_info', $rooms);
                // ユーザごとのオプションを取得
                $type        = $this->request->get("type", "normal");
                $mode = ( $user_info["user_status"] == 2 ) ? "trial" : "";
                // ホワイトボードアップロードメールアドレス
                $mail_upload_address = ($this->config->get("N2MY", "mail_wb_host")) ? $meeting_info["room_key"]."@".$this->config->get("N2MY", "mail_wb_host") : "";
                $user_options = array(
                    "user_key"              => $user_info["user_key"],
                    "name"                  => $this->request->get("name"),
                    "narrow"                => $this->request->get("is_narrow"),
                    "lang"                  => $this->request->get("lang"),
                    "skin_type"             => $this->request->get("skin_type", ""),
                    "mode"                  => $mode,
                    "role"                  => $this->request->get("role"),
                    "mail_upload_address"   => $mail_upload_address,
                    "account_model"         => $user_info["account_model"],
                    );
                // 会議入室
                $obj_N2MY_Meeting = new N2MY_Meeting($server_info["dsn"]);
                $meetingDetail = $obj_N2MY_Meeting->startMeeting($meeting_id, $type, $user_options);
                //リダイレクト先の core/htdocs/sercie/*　の処理を直接書いています。
                $session->set("room_key", $meeting_info["room_key"] );
                $session->set("type", $meetingDetail["participant_type_name"] );
                $session->set("meeting_key", $meetingDetail["meeting_key"] );
                $session->set("participant_key", $meetingDetail["participant_key"] );
                $session->set("participant_name", $this->request->get("name"));
                $session->set("mail_upload_address", $mail_upload_address );
                $session->set("fl_ver", $this->request->get("flash_version", "as2"));
                $session->set("reload_type", 'normal');
                $session->set( "meeting_version", $user_info["meeting_version"] );
                $this->session->set( "user_agent_ticket", uniqid());
                // 会議入室
                $url = N2MY_BASE_URL."services/?action_meeting_display=&".N2MY_SESSION."=".session_id();
                if ($meeting_info["meeting_password"]) {
                    $this->session->set("redirect_url", $url, $this->_name_space);
                    $url = N2MY_BASE_URL."services/" .
                            "?action_login=" .
                            "&".N2MY_SESSION."=".session_id().
                            "&reservation_session=".$meeting_info["reservation_session"].
                            "&ns=".$this->_name_space;
                }
                $data = array(
                    "url"           => $url,
                    API_MEETING_ID  => $meeting_id,
                    "meeting_name"  => $meetingDetail["meeting_name"],
                );
                return $this->output($data);
            }
        }
    }


    /**
     * セールス用 入室API
     * @return array
     */
    function action_inbound_start(){
        require_once 'classes/mgm/MGM_Auth.class.php';
        require_once 'classes/N2MY_Auth.class.php';
        require_once 'classes/N2MY_Account.class.php';
        require_once 'classes/dbi/meeting.dbi.php';
        require_once 'classes/dbi/user.dbi.php';
        require_once 'classes/N2MY_Meeting.class.php';
        // DB選択
        $request = $this->request->getAll();
        $meeting_id  = $request[API_MEETING_ID];
        $output_type = $this->request->get("output_type");
        // パラメタチェック
        $rules = array(
            API_MEETING_ID => array(
                "required" => true,
            ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
            ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        $dsn_info = $obj_MGMClass->getRelationDsn($meeting_id, "meeting");
        $server_info = $obj_MGMClass->getServerInfo($dsn_info["server_key"]);
        if (!$dsn_info) {
            $err_msg["errors"][] = array(
              "field"   => API_MEETING_ID,
              "err_cd"  => "invalid",
              "err_msg" => "ミーティングキーが不正です",
            );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        // 会議情報
        $objMeeting = new MeetingTable($server_info["dsn"]);
        $where = "meeting_session_id = '".addslashes($meeting_id)."'" .
            " AND is_active = 1" .
            " AND is_deleted = 0";
        $meeting_info = $objMeeting->getRow($where);
        if (!$meeting_info) {
            $err_msg["errors"][] = array(
                "field"   => API_MEETING_ID,
                "err_cd"  => "invalid",
                "err_msg" => "ミーティングキーが不正です",
            );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        } else {
            require_once('classes/core/dbi/Participant.dbi.php');
            $obj_Participant    = new DBI_Participant( $server_info["dsn"] );
            $pcount      = $obj_Participant->getCount($meeting_info["meeting_key"]);
            if ($pcount != 1) {
                $err_msg["errors"][] = array(
                "field"   => API_MEETING_ID,
                "err_cd"  => "invalid",
                "err_msg" => "MEETING_INVALID",
                );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
            }
        }
        if ($meeting_info["is_reserved"] &&
           ((strtotime($meeting_info['meeting_start_datetime']) > time() ||
           (strtotime($meeting_info['meeting_stop_datetime'])) < time())
           )
        ) {
            $err_msg["errors"][] = array(
                "field"   => API_MEETING_ID,
                "err_cd"  => "invalid",
                "err_msg" => "予約時間内ではありません",
            );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        // ユーザー
        $objUser = new UserTable($server_info["dsn"]);
        $where = "user_key = ".$meeting_info["user_key"];
        $user_info = $objUser->getRow($where);
        if (!$user_info) {
            return $this->display_error("101", "User not found", $this->get_error_info($err_obj));
        }
        $lang = $this->get_language();
        $lang_conf_file = N2MY_APP_DIR."config/lang/".$lang."/message.ini";
        $this->_message = parse_ini_file($lang_conf_file, true);
        // パラメタ取得
        $id          = $user_info["user_id"];
        $lang        = $this->request->get("lang", "ja");
        $country     = $this->request->get("country", "jp");
        $time_zone   = $this->request->get(API_TIME_ZONE, "9");
        $account_model = $user_info["meeting_version"];
        $rules = array(
            "lang"     => array(
                "allow" => array_keys($this->get_language_list()),
            ),
            "country"     => array(
                "allow" => array_keys($this->get_country_list()),
            ),
            API_TIME_ZONE     => array(
                "allow" => array_keys($this->get_timezone_list()),
            ),
        );
        $request = $this->request->getAll();
        //古いAPI利用者用対応(zhの場合はzh-cnに変更)
        if ($request["lang"] == "zh") {
            $request["lang"] = "zh-cn";
        }
        $err_obj = $this->error_check($request, $rules);
        if (!$mgm_info = $obj_MGMClass->getUserInfoById( $id ) ){
            $err_obj->set_error("id", "SELECT_USER_ERROR", $id);
        } else {
            if (!$server_info = $obj_MGMClass->getServerInfo( $mgm_info["server_key"] )){
                $err_obj->set_error("", "SELECT_USER_ERROR", $id);
                return $this->display_error("1", "SELECT_SERVER_ERROR");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }

        // セッションスタート
        $this->session->set("lang", $lang);
        $this->session->set("country_key", $country);
        $this->session->set("time_zone", $time_zone);
        $this->session->set("dsn_key", $server_info["host_name"]);
        $this->session->set("server_info", $server_info);

        // ユーザー情報
        $this->session->set("user_info", $user_info);
        // 使用可能な部屋
        $obj_N2MY_Account = new N2MY_Account($server_info["dsn"]);
        $rooms = $obj_N2MY_Account->getOwnRoomByRoomKey( $meeting_info["room_key"], $user_info["user_key"]);
        // 既存のセッションデータにも書き込む
        $this->session->set('room_info', $rooms);
        // ユーザごとのオプションを取得
        $type = $this->request->get("type", "customer");
        $mode = ( $user_info["user_status"] == 2 ) ? "trial" : "";
        // ホワイトボードアップロードメールアドレス
        $mail_upload_address = ($this->config->get("N2MY", "mail_wb_host")) ? $meeting_info["room_key"]."@".$this->config->get("N2MY", "mail_wb_host") : "";
        $user_options = array(
            "user_key"              => $user_info["user_key"],
            "name"                  => $this->request->get("name"),
            "narrow"                => $this->request->get("is_narrow"),
            "lang"                  => $this->request->get("lang"),
            "skin_type"             => $this->request->get("skin_type", ""),
            "mode"                  => $mode,
            "role"                  => $this->request->get("role"),
            "mail_upload_address"   => $mail_upload_address,
            "account_model"         => $user_info["account_model"],
        );
        // 会議入室
        $obj_N2MY_Meeting = new N2MY_Meeting($server_info["dsn"]);
        $meetingDetail = $obj_N2MY_Meeting->startMeeting($meeting_id, $type, $user_options);

        //リダイレクト先の core/htdocs/sercie/*　の処理を直接書いています。
        $this->session->set("room_key", $meeting_info["room_key"] );
        require_once("classes/dbi/member_room_relation.dbi.php");
        $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
        $where = "room_key = '".$meeting_info["room_key"]."'";
        $relation_info = $objRoomRelation->getRow($where);
        require_once("classes/dbi/member.dbi.php");
        $objMember = new MemberTable($this->get_dsn());
        $where = "member_key = ".$relation_info["member_key"];
        $member_info = $objMember->getRow($where);
        $this->session->set( "member_info", $member_info );

        $this->session->set("type", $meetingDetail["participant_type_name"] );
        $this->session->set("meeting_key", $meetingDetail["meeting_key"] );
        $this->session->set("participant_key", $meetingDetail["participant_key"] );
        $this->session->set("participant_name", $this->request->get("name"));
        $this->session->set("mail_upload_address", $mail_upload_address );
        $this->session->set("fl_ver", $this->request->get("flash_version", "as3"));
        $this->session->set("reload_type", 'customer');
        $this->session->set( "meeting_version", $account_model );
        $this->session->set( "meeting_type", "customer" );
        $screen_mode = $this->request->get("screen_mode", "wide");
        $this->session->set( "user_agent_ticket", uniqid());
        $display_size = '16to9';
        $this->session->set( "display_size", $display_size);
        // 会議入室
        $url = N2MY_BASE_URL."services/?action_meeting_display=&".N2MY_SESSION."=".session_id();
        if ($meeting_info["meeting_password"]) {
            $this->session->set("redirect_url", $url, $this->_name_space);
            $url = N2MY_BASE_URL."services/" .
                "?action_login=" .
                "&".N2MY_SESSION."=".session_id().
                "&reservation_session=".$meeting_info["reservation_session"].
                "&ns=".$this->_name_space;
        }
        $data = array(
            "url"           => $url,
            API_MEETING_ID  => $meeting_id,
            "meeting_name"  => $meetingDetail["meeting_name"],
//                "meeting_version" => $session->get( "meeting_version" ),
        );
        return $this->output($data);


    }

    function action_inbound_enter(){
        require_once 'classes/mgm/MGM_Auth.class.php';
        require_once 'classes/N2MY_Auth.class.php';
        require_once 'classes/N2MY_Account.class.php';
        require_once 'classes/dbi/meeting.dbi.php';
        require_once 'classes/dbi/user.dbi.php';
        require_once 'classes/N2MY_Meeting.class.php';
        // DB選択
        $request = $this->request->getAll();
        $meeting_id  = $request[API_MEETING_ID];
        $output_type = $this->request->get("output_type");
        // パラメタチェック
        $rules = array(
            API_MEETING_ID => array(
                "required" => true,
            ),
            "mobile"  => array(
                "allow"    => array('1', '0'),
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
            ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        $dsn_info = $obj_MGMClass->getRelationDsn($meeting_id, "meeting");
        $server_info = $obj_MGMClass->getServerInfo($dsn_info["server_key"]);
        if (!$dsn_info) {
            $err_msg["errors"][] = array(
              "field"   => API_MEETING_ID,
              "err_cd"  => "invalid",
              "err_msg" => "ミーティングキーが不正です",
            );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        // 会議情報
        $objMeeting = new MeetingTable($server_info["dsn"]);
        $where = "meeting_session_id = '".addslashes($meeting_id)."'" .
            " AND is_active = 1" .
            " AND is_deleted = 0";
        $meeting_info = $objMeeting->getRow($where);
        if (!$meeting_info) {
            $err_msg["errors"][] = array(
                "field"   => API_MEETING_ID,
                "err_cd"  => "invalid",
                "err_msg" => "ミーティングキーが不正です",
            );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        } else {
            require_once('classes/core/dbi/Participant.dbi.php');
            $obj_Participant    = new DBI_Participant( $server_info["dsn"] );
            $pcount      = $obj_Participant->getCount($meeting_info["meeting_key"]);
            if ($pcount != 1) {
                $err_msg["errors"][] = array(
                "field"   => API_MEETING_ID,
                "err_cd"  => "invalid",
                "err_msg" => "MEETING_INVALID",
                );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
            }
        }

        // ユーザー
        $objUser = new UserTable($server_info["dsn"]);
        $where = "user_key = ".$meeting_info["user_key"];
        $user_info = $objUser->getRow($where);
        if (!$user_info) {
            return $this->display_error("101", "User not found", $this->get_error_info($err_obj));
        }

        // モバイル判別
        $mobile_flg = $this->request->get("mobile");
        if ($mobile_flg || strpos($_SERVER['HTTP_USER_AGENT'], "Android") || strpos($_SERVER['HTTP_USER_AGENT'],"iPhone") || strpos($_SERVER['HTTP_USER_AGENT'],"iPod") || strpos($_SERVER['HTTP_USER_AGENT'],"iPad")) {
            require_once("classes/dbi/inbound.dbi.php");
            $obj_Inbound = new InboundTable( $server_info["dsn"] );
            $where = "user_key = '".addslashes($user_info["user_key"])."'";
            $inbound_info = $obj_Inbound->getRow($where);
            $base_url = parse_url(N2MY_BASE_URL);
            $referer = $_SERVER["HTTP_REFERER"];
            $url    = $this->config->get("N2MY","mobile_protcol_sales", 'vcube-sales')."://info?inbound_id=".$inbound_info["inbound_id"]."&entrypoint=".$base_url["host"]."&callbackurl=".urlencode($referer)."&name=".$this->request->get("name");
            $this->logger2->info($url);
            $data = array(
                "url"               => $url,
                API_MEETING_ID      => $this->ticket_to_session($meeting_info["meeting_ticket"]),
                "meeting_name"      => $meeting_info["meeting_name"],
                "password_flg"      => 0,
                "need_login_flg"    => 0,
            );
            return $this->output($data);
            exit;
        }

        if ($meeting_info["is_reserved"] &&
           ((strtotime($meeting_info['meeting_start_datetime']) > time() ||
           (strtotime($meeting_info['meeting_stop_datetime'])) < time())
           )
        ) {
            $err_msg["errors"][] = array(
                "field"   => API_MEETING_ID,
                "err_cd"  => "invalid",
                "err_msg" => "予約時間内ではありません",
            );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        $lang = $this->get_language();
        $lang_conf_file = N2MY_APP_DIR."config/lang/".$lang."/message.ini";
        $this->_message = parse_ini_file($lang_conf_file, true);
        // パラメタ取得
        $id          = $user_info["user_id"];
        $lang        = $this->request->get("lang", "ja");
        $country     = $this->request->get("country", "jp");
        $time_zone   = $this->request->get(API_TIME_ZONE, "9");
        $account_model = $user_info["meeting_version"];
        $rules = array(
            "lang"     => array(
                "allow" => array_keys($this->get_language_list()),
            ),
            "country"     => array(
                "allow" => array_keys($this->get_country_list()),
            ),
            API_TIME_ZONE     => array(
                "allow" => array_keys($this->get_timezone_list()),
            ),
        );
        $request = $this->request->getAll();
        //古いAPI利用者用対応(zhの場合はzh-cnに変更)
        if ($request["lang"] == "zh") {
            $request["lang"] = "zh-cn";
        }
        $err_obj = $this->error_check($request, $rules);
        if (!$mgm_info = $obj_MGMClass->getUserInfoById( $id ) ){
            $err_obj->set_error("id", "SELECT_USER_ERROR", $id);
        } else {
            if (!$server_info = $obj_MGMClass->getServerInfo( $mgm_info["server_key"] )){
                $err_obj->set_error("", "SELECT_USER_ERROR", $id);
                return $this->display_error("1", "SELECT_SERVER_ERROR");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }

        // セッションスタート
        $this->session->set("lang", $lang);
        $this->session->set("country_key", $country);
        $this->session->set("time_zone", $time_zone);
        $this->session->set("dsn_key", $server_info["host_name"]);
        $this->session->set("server_info", $server_info);

        // ユーザー情報
        $this->session->set("user_info", $user_info);
        // 使用可能な部屋
        $obj_N2MY_Account = new N2MY_Account($server_info["dsn"]);
        $rooms = $obj_N2MY_Account->getOwnRoomByRoomKey( $meeting_info["room_key"], $user_info["user_key"]);
        // 既存のセッションデータにも書き込む
        $this->session->set('room_info', $rooms);
        // ユーザごとのオプションを取得
        $type = $this->request->get("type", "customer");
        $mode = ( $user_info["user_status"] == 2 ) ? "trial" : "";
        // ホワイトボードアップロードメールアドレス
        $mail_upload_address = ($this->config->get("N2MY", "mail_wb_host")) ? $meeting_info["room_key"]."@".$this->config->get("N2MY", "mail_wb_host") : "";

        $user_options = array(
            "user_key"              => $user_info["user_key"],
            "name"                  => $this->request->get("name"),
            "narrow"                => $this->request->get("is_narrow"),
            "lang"                  => $this->request->get("lang"),
            "skin_type"             => $this->request->get("skin_type", ""),
            "mode"                  => $mode,
            "role"                  => $this->request->get("role"),
            "mail_upload_address"   => $mail_upload_address,
            "account_model"         => $user_info["account_model"],
        );
        // 会議入室
        $obj_N2MY_Meeting = new N2MY_Meeting($server_info["dsn"]);
        $meetingDetail = $obj_N2MY_Meeting->startMeeting($meeting_id, $type, $user_options);

        //リダイレクト先の core/htdocs/sercie/*　の処理を直接書いています。
        $this->session->set("room_key", $meeting_info["room_key"] );
        require_once("classes/dbi/member_room_relation.dbi.php");
        $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
        $where = "room_key = '".$meeting_info["room_key"]."'";
        $relation_info = $objRoomRelation->getRow($where);
        require_once("classes/dbi/member.dbi.php");
        $objMember = new MemberTable($this->get_dsn());
        $where = "member_key = ".$relation_info["member_key"];
        $member_info = $objMember->getRow($where);

        $this->session->set( "member_info", $member_info );

        $this->session->set("type", $meetingDetail["participant_type_name"] );
        $this->session->set("meeting_key", $meetingDetail["meeting_key"] );
        $this->session->set("participant_key", $meetingDetail["participant_key"] );
        $this->session->set("participant_name", $this->request->get("name"));
        $this->session->set("mail_upload_address", $mail_upload_address );
        $this->session->set("fl_ver", $this->request->get("flash_version", "as3"));
        $this->session->set("reload_type", 'customer');
        $this->session->set( "meeting_version", $account_model );
        $this->session->set( "meeting_type", "customer" );
        $screen_mode = $this->request->get("screen_mode", "wide");
        $this->session->set( "user_agent_ticket", uniqid());
        $display_size = '16to9';
        $this->session->set( "display_size", $display_size);
        // 会議入室
        $url = N2MY_BASE_URL."services/?action_meeting_display=&".N2MY_SESSION."=".session_id();
        if ($meeting_info["meeting_password"]) {
            $this->session->set("redirect_url", $url, $this->_name_space);
            $url = N2MY_BASE_URL."services/" .
                "?action_login=" .
                "&".N2MY_SESSION."=".session_id().
                "&reservation_session=".$meeting_info["reservation_session"].
                "&ns=".$this->_name_space;
        }
        $data = array(
            "url"           => $url,
            API_MEETING_ID  => $meeting_id,
            "meeting_name"  => $meetingDetail["meeting_name"],
//                "meeting_version" => $session->get( "meeting_version" ),
        );
        return $this->output($data);

    }

    /**
     * 会議キー取得
     *
     * @param boolean $header_flg ヘッダを出力
     * @return string $output  会議キー、会議名をxmlで出力
     */
    function action_create()
    {
        $this->logger->trace(__FUNCTION__, __FILE__, __LINE__);
        require_once( "classes/N2MY_Meeting.class.php" );
        $obj_N2MYMeeting = new N2MY_Meeting( $this->get_dsn() );
        $room_key = $this->request->get(API_ROOM_ID);
        $user_info = $this->session->get("user_info");
        $room_info = $this->session->get("room_info");
        $this->session->remove("meeting_auth_flg");
        // チェックルール編集
        $rules = array(
            API_ROOM_ID     => array(
                "required" => true,
                "allow"    => array_keys($room_info),
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        // チェックデータ（リクエスト）編集
        $request = array(
            API_ROOM_ID    => $room_key,
            "output_type" => $this->request->get("output_type", null),
            );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $now_meeting = $obj_N2MYMeeting->getNowMeeting( $room_key );
        $meeting_key = $now_meeting["meeting_key"];
        $meeting_name = $now_meeting["meeting_name"];
        $meeting_password = $now_meeting["meeting_password"];
        // センタサーバ指定
        $country_id = "jp";
        if (isset($extension["locale"])) {
            switch ($extension["locale"]) {
            case "jp" :
            case "us" :
            case "cn" :
                $country_id = $extension["locale"];
                break;
            }
        }
        // オプション指定
        $option = array(
            "user_key"     => $user_info["user_key"],
            "meeting_name" => $meeting_name,
            "start_time"   => $now_meeting["start_time"],
            "end_time"     => $now_meeting["end_time"],
            "country_id"   => $country_id,
        );
        $password_flg = "0";
        if ($now_meeting["meeting_password"] ) {
            require_once ("classes/dbi/reservation.dbi.php");
            try {
                $reservation_obj = new ReservationTable($this->get_dsn());
                $where = "user_key = ".mysql_real_escape_string($user_info["user_key"])." AND reservation_session = '".mysql_real_escape_string($now_meeting["reservation_session"])."'";
                $reservation_info = $reservation_obj->getRow($where, "user_key,reservation_session, reservation_pw_type");
            } catch (Exception $e) {
                $this->logger2->error($e->getMessage());
                return $this->display_error("1000", "DATABESE_ERROR", $this->get_error_info($err_obj));
            }
            $this->logger->debug("reservation_info",__FILE__,__LINE__,$reservation_info);
            if ($reservation_info["reservation_pw_type"] == "1") {
                $password_flg = "1";
            }
        }
        $core_meeting_key = $obj_N2MYMeeting->createMeeting($room_key, $meeting_key, $option);
        if ($core_meeting_key) {
            $obj_Meeting = new DBI_Meeting($this->get_dsn());
            $where = "meeting_session_id = '".addslashes($core_meeting_key)."'".
                " AND is_deleted = 0";
            $meeting_info = $obj_Meeting->getRow($where);
        } else {
            $err_obj->set_error(0, "MEETING_INVALID");
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $this->logger2->info(array($core_meeting_key,$meeting_info));

        $data = array(
            API_MEETING_ID  => $this->ticket_to_session($now_meeting["meeting_key"]),
            "meeting_name" => $meeting_name,
            "password_flg" => $password_flg,
            "pin_cd"       => $meeting_info["pin_cd"],
        );
        // TODO : 作成できたか出来ないかのチェックは不要？
        return $this->output($data);
    }

    //会議認証
    function action_auth() {
        $this->logger->trace(__FUNCTION__, __FILE__, __LINE__);
        require_once( "classes/N2MY_Meeting.class.php" );
        $obj_N2MYMeeting = new N2MY_Meeting( $this->get_dsn() );
        $obj_Meeting = new DBI_Meeting($this->get_dsn());
        $meeting_session_id = $this->request->get(API_MEETING_ID);
        $password = $this->request->get("password");
        $user_info = $this->session->get("user_info");
        $room_info = $this->session->get("room_info");
        $enc = $this->request->get("enc", "md5");
        // チェックルール編集
        $rules = array(
            API_MEETING_ID     => array(
                "required" => true,
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        // チェックデータ（リクエスト）編集
        $request = array(
            API_MEETING_ID    => $meeting_session_id,
            "output_type" => $this->request->get("output_type", null),
            );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $where = "meeting_session_id = '".addslashes($meeting_session_id)."'" .
                " AND is_active = 1" .
                " AND is_deleted = 0";
        $meeting_info = $obj_Meeting->getRow($where);
        if (!$meetingInfo = $obj_Meeting->getRow($where)) {
            $err_obj->set_error(API_MEETING_ID, "MEETING_KEY_INVALID");
        } else {
            // ログインユーザ以外の予約
            if ($meetingInfo["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_MEETING_ID, "MEETING_KEY_INVALID");
            }
        }

        //予約情報からパスワード判定
        if ($meeting_info["is_reserved"]) {
            require_once ("classes/dbi/reservation.dbi.php");
            $reservation_obj = new ReservationTable($this->get_dsn());
            $reservation_info = $reservation_obj->getRow("meeting_key = '".addslashes($meetingInfo["meeting_ticket"])."'");
            if ($enc == "sha1") {
                $reservation_pass = sha1($reservation_info["reservation_pw"]);
            } else {
                $reservation_pass = md5($reservation_info["reservation_pw"]);
            }
            if (($reservation_info["reservation_pw"] === "") || ($reservation_pass === $password)) {
                $this->session->set("meeting_auth_flg", "1");
                return $this->output();
            } else {
                $err_obj->set_error("password", "PASSWORD_INVALID");
            }
        } else {
            $err_obj->set_error(API_MEETING_ID, "MEETING_KEY_INVALID");
        }
        return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
    }

    //会議認証


    //V-CUBE ID利用者ログイン用とメンバー課金での招待認証
    function action_auth_login () {
        require_once ('classes/N2MY_Auth.class.php');
        require_once ('classes/mgm/MGM_Auth.class.php');

        $meeting_session_id = $this->request->get(API_MEETING_ID);

        $id      = $this->request->get("id");
        $pw      = $this->request->get("pw");
        $pw_encode      = $this->request->get("enc");
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        $obj_Auth = new N2MY_Auth( $this->get_dsn() );
        $rules = array(
            API_MEETING_ID     => array(
                "required" => true,
                ),
            "id"     => array(
                "required" => true
                ),
            "pw"     => array(
                "required" => true
                ),
            "enc" => array(
                "allow" => array("md5", "sha1"),
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $request = array(
            API_MEETING_ID    => $meeting_session_id,
            "id"    => $id,
            "pw"    => $pw,
            "output_type" => $this->request->get("output_type", null),
            );
        $err_obj = $this->error_check($request, $rules);
        $where = "meeting_session_id = '".addslashes($meeting_session_id)."'" .
                " AND is_active = 1" .
                " AND is_deleted = 0";
        require_once("classes/N2MY_Meeting.class.php");
        $obj_Meeting = new DBI_Meeting($this->get_dsn());
        if (!$meetingInfo = $obj_Meeting->getRow($where)) {
            $err_obj->set_error(API_MEETING_ID, "MEETING_KEY_INVALID");
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        if ($meetingInfo["is_reserved"]) {
            $password_flg = "0";
            require_once ("classes/dbi/reservation.dbi.php");
            try {
                $reservation_obj = new ReservationTable($this->get_dsn());
                $where = "user_key = ".mysql_real_escape_string($meetingInfo["user_key"])." AND meeting_key = '".mysql_real_escape_string($meetingInfo["meeting_ticket"])."'";
                $reservation_info = $reservation_obj->getRow($where, "user_key,reservation_session, reservation_pw_type");
            } catch (Exception $e) {
                $this->logger2->error($e->getMessage());
                return $this->display_error("1000", "DATABESE_ERROR", $this->get_error_info($err_obj));
            }
            $this->logger->info("reservation_info",__FILE__,__LINE__,$reservation_info);
            if ($reservation_info["reservation_pw_type"] == "1") {
                $password_flg = "1";
            }
        }

        if ($login_info = $obj_Auth->check($id, $pw, $pw_encode)) {
            $this->logger2->debug($login_info);
            $user_info = $this->session->get("user_info");
            if($user_info["account_model"] == "free"){
              // vCubeID でのログイン
              if ($login_info["member_info"]["member_type"] != 'free' ) {
                $err_obj->set_error("id", "SELECT_USER_ERROR", $id);
                return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
              }
              $this->session->set("member_info", $login_info["member_info"]);
            } else if ($user_info["account_model"] == "member"){
              // member課金メンバーのログイン
              if(!$obj_Auth->checkMember($id, $pw, $user_info["user_key"], $pw_encode, "", $user_info["external_member_invitation_flg"])){
                $err_obj->set_error("id", "SELECT_USER_ERROR", $id);
                return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
              }
              $this->session->set("member_info", $login_info["member_info"]);
            } else {
              $err_obj->set_error("id", "SELECT_USER_ERROR", $id);
              return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
            }
        } else {
            $this->logger2->info($request);
            $err_obj->set_error("id", "SELECT_USER_ERROR", $id);
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $data = array(
            "password_flg"      => $password_flg,
            API_MEETING_ID      => $meeting_session_id,
            );
        $this->session->set("meeting_auth_flg", "1");
        return $this->output($data);
    }

    /**
     * 会議開始（会議を作成して、そのまま開始）
     *
     * @param
     * @return 会議データを作成し、リダイレクトで会議室に入室
     */
    function action_start() {
        $this->session->remove("meeting_auth_flg");
        require_once("classes/N2MY_Meeting.class.php");
        require_once("classes/dbi/room.dbi.php");
        // チェックデータ（リクエスト）編集
        $request = $this->request->getAll();
        // room_keyはミーティングIDからとる
        $meeting_key = $this->request->get(API_MEETING_ID);
        $obj_Meeting = new DBI_Meeting($this->get_dsn());
        $objRoom = new RoomTable($this->get_dsn());
        if ($meeting_key) {
            $meeting_where = "meeting_session_id = '".addslashes($meeting_key)."'" .
                    " AND is_active = 1" .
                    " AND is_deleted = 0";

            $room_key = $obj_Meeting->getOne($meeting_where, "room_key");
        } else {
            $room_key = $this->request->get(API_ROOM_ID);
        }
        $this->logger2->debug($room_key);

        if ($meeting_ticket = $this->session->get('invited_meeting_ticket')) {
            $where = "room_key = '".addslashes($room_key)."'" .
                    " AND meeting_ticket = '".addslashes($meeting_ticket)."'" .
                    " AND is_active = 1" .
                    " AND is_deleted = 0";
            $meeting_key = $obj_Meeting->getOne($where, 'meeting_session_id');
        } else {
            $meeting_key = $this->request->get(API_MEETING_ID);
        }
        $this->logger2->info($meeting_key);
        $obj_N2MY_Meeting = new N2MY_Meeting($this->get_dsn());
        // チェックルール編集
        $rules = array(
            API_ROOM_ID => array(
                "required" => true,
                "allow" => array_keys($this->session->get("room_info")),
                ),
            "flash_version" => array(
                "allow" => array("as2", "as3"),
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
            "screen_mode"  => array(
                "allow"    => array('normal', 'wide'),
                ),
            "url_encrypt"  => array(
                "allow"    => array(1, 0),
                ),
             "lang"     => array(
                "allow" => array_keys($this->get_language_list()),
                ),
        );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        //言語設定
        if($request["lang"]) {
            $this->session->set("lang", $request["lang"]);
        }
        // ログインタイプ取得
        $login_type = $this->session->get("login_type");
        $room_info = $objRoom->getRow("room_key='".addslashes($room_key)."'");
        if ($room_info["use_sales_option"] && ($login_type == "normal" || !$login_type )) {
            $login_type = "staff";
            $this->session->set("service_mode", "sales");
        } else if ($room_info["use_sales_option"] && ($login_type != "staff" && $login_type == "invitedGuest" )) {
            $login_type = "customer";
            $this->session->set("service_mode", "sales");
        } else {
            $this->session->set("service_mode", "meeting");
        }
        // ユーザ情報
        $user_info = $this->session->get("user_info");
        $member_info = $this->session->get( "member_info" );
        //member課金を利用していてメンバー以外が会議を利用しようとした場合
        if ( $user_info["account_model"] == "member" && !$member_info ) {
//            $err_obj->set_error(0, "MEMBER_INVALID");
//            return $this->display_error(0, "MEMBER_INVALID");
        }
        // 会議の指定が有る場合
        $meeting_info = array();
        if ($meeting_key) {
            $where = "room_key = '".addslashes($room_key)."'" .
                    " AND meeting_session_id = '".addslashes($meeting_key)."'" .
                    " AND is_active = 1" .
                    " AND is_deleted = 0";
            $meeting_info = $obj_Meeting->getRow($where);
            $this->logger2->info($meeting_info);
            // 予約で時間外
            if (!$meeting_info) {
                $err_obj->set_error(API_MEETING_ID, "MEETING_INVALID");
                return $this->display_error("101", "対象の会議が存在しないか、既に終了しています", $this->get_error_info($err_obj));
            } else {
                if ($meeting_info["is_reserved"]) {

                    // 予約情報を取得
                    require_once('classes/dbi/reservation.dbi.php');
                    $obj_reservation = new ReservationTable($this->get_dsn() );
                    $reservation_info = $obj_reservation->getRow("meeting_key='" .$meeting_info["meeting_ticket"] . "'");

                    if(((strtotime($meeting_info['meeting_start_datetime']) > time() || (strtotime($reservation_info['reservation_extend_endtime'])) < time()))){
                        $err_obj->set_error(API_MEETING_ID, "MEETING_INVALID");
                        return $this->display_error("101", "時間外の予約です", $this->get_error_info($err_obj));
                    }
                }
                // 直前の会議取得
                $last_meeting_key = $objRoom->getOne("room_key='".addslashes($room_key)."'", 'meeting_key');
                // 異なる会議
                if ($meeting_info['meeting_ticket'] != $last_meeting_key) {
                    // 開催中
                    $ret = $obj_N2MY_Meeting->getMeetingStatus($room_key, $last_meeting_key);
                    if ($ret['status'] == '1') {
                        return $this->display_error("101", "前回会議が開催中です", $this->get_error_info($err_obj));
                    }
                }

            }
        }
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $this->logger2->info($meeting_info['meeting_ticket']);
        // 会議情報取得
        if (!$meeting_info = $obj_N2MY_Meeting->getMeeting($room_key, $login_type, $meeting_info['meeting_ticket'])) {
            $err_obj->set_error(0, "MEETING_INVALID");
//            return $this->display_error(0, "MEETING_INVALID");
        }
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }


        //ポート課金の場合は人数チェック
        if ($user_info["max_connect_participant"] > 0 && $user_info["use_port_plan"]) {
            require_once('classes/core/dbi/Participant.dbi.php');
            $obj_Participant     = new DBI_Participant( $this->get_dsn() );
            $nowParticipantNumber = $obj_Participant->numRows('user_key = '.$user_info["user_key"].' AND is_active = 1');
            if ($nowParticipantNumber >= $user_info["max_connect_participant"]) {
                require_once( "classes/dbi/meeting.dbi.php" );
                $obj_Meeting = new MeetingTable($this->get_dsn());
                $where = "meeting_ticket = '".$meeting_info["meeting_key"]."'".
                    " AND is_active = 1";
                $rowNowMeeting = $obj_Meeting->getRow($where);
                require_once('classes/dbi/max_connect_log.dbi.php');
                $obj_MaxConnectLog     = new MaxConnectLog( $this->get_dsn() );
                $data = array(
                            "user_key"      => $user_info["user_key"],
                            "meeting_key"   => $rowNowMeeting["meeting_key"],
                            "room_key"      => $room_key,
                            "type"          => "port_plan",
                            "connect_count" => $nowParticipantNumber,
                            );
                $obj_MaxConnectLog->add($data);
                $err_obj->set_error(API_MEETING_ID, "MEETING_MAX_CONNECTION");
                return $this->display_error("102", "Max Connect Participant", $this->get_error_info($err_obj));
            }
        }
        $meeting_key = $meeting_info["meeting_key"];
        $country_key = $this->session->get("country_key");
$this->logger2->info(array($meeting_info["meeting_key"], $country_key));
        // 地域が特定できない場合は利用可能な地域をデフォルトとする
        $country_list = $this->get_country_list();
        if (!array_key_exists($country_key, $country_list)) {
            $this->logger2->info($country_list);
            $_country_data = array_shift($country_list);
            $country_key = $_country_data["country_key"];
        }
$this->logger2->info(array($meeting_info["meeting_key"], $country_key));
        // オプション指定
        $options = array(
            "meeting_name" => $meeting_info["meeting_name"],
            "user_key"     => $user_info["user_key"],
            "start_time"   => $meeting_info["start_time"],
            "end_time"     => $meeting_info["end_time"],
            "country_id"   => $country_key,
            "password"     => $meeting_info["meeting_password"]
        );
        // 現在の部屋のオプションで会議更新
        $core_session = $obj_N2MY_Meeting->createMeeting($room_key, $meeting_key, $options);

        //会議内からの招待メールのため会議作成毎にmgm_relationにmeeting_session_idを追加
        require_once("classes/mgm/dbi/relation.dbi.php");
        $objMgmRelationTable = new MgmRelationTable(
                                    $this->get_auth_dsn());
        $resultRelation = $objMgmRelationTable->getRow(sprintf("relation_key='%s'", $core_session));
        if (!$resultRelation) {
            $data = array(
                        "relation_key"  => $core_session,
                        "user_key"      => $user_info["user_id"],
                        "relation_type" => "meeting",
                        "status"        => 1,
                    );
            $add_data = $objMgmRelationTable->add($data);
        }

        $_room_info = $this->session->get("room_info");
        $room_options = $_room_info[$room_key]["options"];
        // ユーザごとのオプションを取得
        $type        = $this->request->get("type", "normal");
        if ($room_info["use_sales_option"] && ($type == "normal" || $type = "staff") && $login_type != "customer") {
            if ($room_options["whiteboard"]) {
                $type = "whiteboard_staff";
            } else {
                $type = "staff";
            }
        } else if ($room_info["use_sales_option"] && ($login_type != "staff" && $login_type == "customer" )) {
            if ($room_options["whiteboard"]) {
                $type = "whiteboard_customer";
            } else {
                $type = "customer";
            }
            $this->session->set("service_mode", "sales");
        }
        if ($is_audience = $this->session->get('invited_meeting_audience')) {
            switch($type) {
                case 'normal':
                    $type = 'audience';
                    break;
                case 'multicamera':
                    $type = 'multicamera_audience';
                    break;
                case 'whiteboard':
                    $type = 'whiteboard_audience';
                    break;
                case 'invisible_wb':
                    $type = 'invisible_wb_audience';
                    break;
            }
        }
//        $mode = "";
//        if ($meeting_info['is_trial']) {
        //ユーザーがトライアルである場合
        $mode = ( $user_info["user_status"] == 2 ) ? "trial" : "";
        //participant登録持はinvitedGuestのタイプはないため、inviteに変更する
        if ($login_type == "invitedGuest") {
            $login_type = "invite";
        }
        // ホワイトボードアップロードメールアドレス
        $mail_upload_address = ($this->config->get("N2MY", "mail_wb_host")) ? $room_key."@".$this->config->get("N2MY", "mail_wb_host") : "";
        $name = $this->request->get("name", $member_info["member_name"]);
        if ($this->session->get("contact_name") && $type == "customer") {
            $name = $this->session->get("contact_name");
        }
        //モバイルの場合はユーザーエージェント登録
        if (strpos($_SERVER['HTTP_USER_AGENT'], "iOS;") || strpos($_SERVER['HTTP_USER_AGENT'], "iPad;") || strpos($_SERVER['HTTP_USER_AGENT'], "Android") || strpos($_SERVER['HTTP_USER_AGENT'],"iPhone") || strpos($_SERVER['HTTP_USER_AGENT'],"iPod")) {
            $is_mobile = 1;
            $this->add_user_agent();
        } else {
            $is_mobile = 0;
            $this->session->set( "user_agent_ticket", uniqid());
        }
        $user_options = array(
            "user_key"            => $user_info["user_key"],
            "id"                  => $member_info["member_key"],
            "name"                => $name,
            "participant_email"   => $member_info["member_email"],
            "participant_station" => $this->request->get("station", $member_info["member_station"]),
            "participant_country" => $country_key,
            "member_key"          => $member_info["member_key"],
            "narrow"              => $this->request->get("is_narrow"),
            "lang"                => $this->session->get("lang"),
            "skin_type"           => $this->request->get("skin_type", ""),
            "mode"                => $mode,
            "role"                => $login_type,
            "mail_upload_address" => $mail_upload_address,
            "account_model"       => $user_info["account_model"],
            "is_mobile"           => $is_mobile,
            );
        // 会議入室
        $meetingDetail = $obj_N2MY_Meeting->startMeeting($core_session, $type, $user_options);

        // 先に入室しているpolycom clientが存在する場合 participant record にmeeting_keyを追加更新
        /*
        if ($meetingDetail["ives_conference_id"] && $meetingDetail["ives_did"]) {
          require_once("classes/polycom/Polycom.class.php");
          $polycom = new PolycomClass($this->get_dsn());
          $polycom->addMeetingKey2PolycomParticipant($meetingDetail["meeting_key"], $meetingDetail["ives_conference_id"], $meetingDetail["ives_did"]);
        }
        */

        //マルチカメラと資料共有対応
        if ($type == "normal" && $room_options["multicamera"]) {
             $type = "multicamera";
        } else if ($type == "normal" && $room_options["whiteboard"]) {
            $type = "whiteboard";
        } else if ($type == "audience" && $room_options["multicamera"]) {
             $type = "multicamera_audience";
        } else if ($type == "audience" && $room_options["whiteboard"]) {
            $type = "whiteboard_audience";
        }
        //リダイレクト先の core/htdocs/sercie/*　の処理を直接書いています。
        $this->session->set( "room_key", $room_key );
        $this->session->set( "type", $meetingDetail["participant_type_name"] );
        $this->session->set( "meeting_type", $type );
        $this->session->set( "meeting_key", $meetingDetail["meeting_key"] );
        $this->session->set( "participant_key", $meetingDetail["participant_key"] );
        $this->session->set( "participant_name", $this->request->get("name", $member_info["member_name"]) );
        $this->session->set( "mail_upload_address", $mail_upload_address );
        $this->session->set( "fl_ver", $this->request->get("flash_version", "as2") );
        $this->session->set( "reload_type", 'normal');
        $this->session->set( "meeting_version", $user_info["meeting_version"] );
        // 画面サイズ
        $screen_mode = $this->request->get("screen_mode", "normal");
        if ($room_info["use_sales_option"]) {
            $screen_mode = "wide";
            $this->session->set( "fl_ver", "as3");
        }
        switch ($screen_mode) {
            case 'normal' :
                $display_size = '4to3';
                break;
            case 'wide' :
                $display_size = '16to9';
                break;
            default :
                $display_size = '4to3';
                break;
        }
        $this->session->set( "display_size", $display_size);
        // 会議入室
        $base_url = N2MY_BASE_FEP_URL ? N2MY_BASE_FEP_URL : N2MY_BASE_URL;
        $url = $base_url."services/?action_meeting_display=&".N2MY_SESS_NAME."=".session_id();

        if (($this->request->get("contact_name") && $this->request->get("contact_email")) || $request["url_encrypt"]) {
            require_once("lib/EZLib/EZUtil/EZEncrypt.class.php");
            $key = N2MY_ENCRYPT_KEY;
            $iv = N2MY_ENCRYPT_IV;
            $encrypted_data = EZEncrypt::encrypt($key, $iv, session_id());
            $encrypted_data = str_replace("/", "-", $encrypted_data);
            $encrypted_data = str_replace("+", "_", $encrypted_data);
            $url = $base_url."direct/meeting/".$encrypted_data;
        }
        // if ($meeting_info["meeting_password"]) {

        //     $this->session->set("redirect_url", $url, $this->_name_space);
        //     $url = $base_url."services/" .
        //             "?action_login=" .
        //             "&".N2MY_SESS_NAME."=".session_id().
        //             "&reservation_session=".$meeting_info["reservation_session"].
        //             "&ns=".$this->_name_space;

        //     $password_flg = "0";
        //     require_once ("classes/dbi/reservation.dbi.php");
        //     try {
        //         $reservation_obj = new ReservationTable($this->get_dsn());
        //         $where = "user_key = ".mysql_real_escape_string($user_info["user_key"])." AND reservation_session = '".mysql_real_escape_string($meeting_info["reservation_session"])."'";
        //         $reservation_info = $reservation_obj->getRow($where, "user_key,reservation_session, reservation_pw_type");
        //     } catch (Exception $e) {
        //         $this->logger2->error($e->getMessage());
        //         return $this->display_error("1000", "DATABESE_ERROR", $this->get_error_info($err_obj));
        //     }
        //     $this->logger->debug("reservation_info",__FILE__,__LINE__,$reservation_info);
        //     if ($reservation_info["reservation_pw_type"] == "1") {
        //         $password_flg = "1";
        //     }
        // }
        //Freeの場合はログインが必要なようにflgで渡す(ログインフラグがある時はパスワードフラグは認証時に渡す)
        $need_login_flg = 0;
        require_once("classes/dbi/member_room_relation.dbi.php");
        $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
        $member_info = $this->session->get( "member_info" );
        $relation_list = "";
        if ($member_info) {
            $where = "member_key = ".$member_info["member_key"].
                     " AND room_key = '".$room_key."'";
            $relation_list = $objRoomRelation->getRowsAssoc($where);
        }
        //自分の部屋の場合はログイン不要
        if ( ($user_info["account_model"] == "member"  || $user_info["account_model"] == "free") && !$relation_list && !$user_info["external_user_invitation_flg"]) {
            $need_login_flg = 1;
            $password_flg   = "0";
        }
        //SFDC対応。入室処理と同時に招待メールを送付。
        if ($this->request->get("contact_name") && $this->request->get("contact_email")) {
            require_once("classes/dbi/meeting.dbi.php");
            $obj_Meetng = new MeetingTable($this->get_dsn());
            // 会議のステータス取得
            $where = "meeting_ticket = '".addslashes($meeting_key)."'";
            $meeting_data = $obj_Meetng->getRow($where);
            require_once "classes/dbi/meeting_invitation.dbi.php";
            $objMeetingInvitationTable = new MeetingInvitationTable($this->get_dsn());
            if (!$invitationData = $objMeetingInvitationTable->getRow(
                                                                sprintf("meeting_key='%s' AND status = 1", $meeting_data["meeting_key"]),
                                                                "*",
                                                                array("invitation_key"=>"desc"))) {
                $user_session_id = md5(uniqid(rand(), 1));
                $audience_session_id = md5(uniqid(rand(), 1));
                $invitationData = array(
                                    "meeting_key" => $meeting_data["meeting_key"],
                                    "meeting_session_id" => $meeting_data["meeting_session_id"],
                                    "meeting_ticket" => $meeting_data["meeting_ticket"],
                                    "user_session_id" => $user_session_id,
                                    "audience_session_id" => $audience_session_id,
                                    "contact_name" => $this->request->get("contact_name"),
                                    "status" => 1
                                    );
                $this->logger2->debug($invitationData);
                $objMeetingInvitationTable->add($invitationData);
            }
            require_once("classes/N2MY_Invitation.class.php");
            $obj_Invitation = new N2MY_Invitation($this->get_dsn());
            $lang = $this->session->get("lang") ? $this->session->get("lang") : "en";
            $obj_Invitation->sendInvitationMail($meetingDetail["meeting_key"], $this->request->get("contact_email"), $lang, $userType="normal", $reservationPassword=null, $this->request->get("contact_name"));
        }
        $data = array(
            "url"               => $url,
            API_MEETING_ID      => $this->ticket_to_session($meetingDetail["meeting_ticket"]),
            "meeting_name"      => $meetingDetail["meeting_name"],
            "password_flg"      => $password_flg,
            "need_login_flg"    => $need_login_flg,
//            "meeting_sequence_key"  => $meetingDetail["meeting_sequence_key"],
//            "participant_key"       => $meetingDetail["participant_key"],
//            "participant_id"        => $meetingDetail["participant_session_id"],
        );
        return $this->output($data);
    }



    /**
     * 会議開始。Mobaile用
     *
     * @param
     * @return 会議データを作成し、Mobile用のURLを返す
     */
    function action_start_mobile() {
        $this->session->remove("meeting_auth_flg");
        require_once("classes/N2MY_Meeting.class.php");
        require_once("classes/dbi/room.dbi.php");
        // チェックデータ（リクエスト）編集
        $request = $this->request->getAll();
        $room_key = $this->request->get(API_ROOM_ID);
        $obj_Meeting = new DBI_Meeting($this->get_dsn());
        $objRoom = new RoomTable($this->get_dsn());
        if ($meeting_ticket = $this->session->get('invited_meeting_ticket')) {
            $where = "room_key = '".addslashes($room_key)."'" .
                    " AND meeting_ticket = '".addslashes($meeting_ticket)."'" .
                    " AND is_active = 1" .
                    " AND is_deleted = 0";
            $meeting_key = $obj_Meeting->getOne($where, 'meeting_session_id');
        } else {
            $meeting_key = $this->request->get(API_MEETING_ID);
        }
        $this->logger2->info($meeting_key);
        $obj_N2MY_Meeting = new N2MY_Meeting($this->get_dsn());
        // チェックルール編集
        $rules = array(
            API_ROOM_ID => array(
                "required" => true,
                "allow" => array_keys($this->session->get("room_info")),
                ),
            "flash_version" => array(
                "allow" => array("as2", "as3"),
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
            "screen_mode"  => array(
                "allow"    => array('normal', 'wide'),
                ),
            "mobile"  => array(
                "allow"    => array('1', '0'),
                ),
        );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        // ログインタイプ取得
        $login_type = $this->session->get("login_type");
        // ユーザ情報
        $user_info = $this->session->get("user_info");
        $member_info = $this->session->get( "member_info" );
        //member課金を利用していてメンバー以外が会議を利用しようとした場合
        if ( $user_info["account_model"] == "member" && !$member_info ) {
//            $err_obj->set_error(0, "MEMBER_INVALID");
//            return $this->display_error(0, "MEMBER_INVALID");
        }
        // 会議の指定が有る場合
        $meeting_info = array();
        if ($meeting_key) {
            $where = "room_key = '".addslashes($room_key)."'" .
                    " AND meeting_session_id = '".addslashes($meeting_key)."'" .
                    " AND is_active = 1" .
                    " AND is_deleted = 0";
            $meeting_info = $obj_Meeting->getRow($where);
            $this->logger2->info($meeting_info);
            // 予約で時間外
            if (!$meeting_info) {
                $err_obj->set_error(API_MEETING_ID, "MEETING_INVALID");
                return $this->display_error("101", "対象の会議が存在しないか、既に終了しています", $this->get_error_info($err_obj));
            } else {
                if ($meeting_info["is_reserved"] ) {

                    // 予約情報を取得
                    require_once('classes/dbi/reservation.dbi.php');
                    $obj_reservation = new ReservationTable($this->get_dsn() );
                    $reservation_info = $obj_reservation->getRow("meeting_key='" .$meeting_info["meeting_ticket"] . "'");

                    if(((strtotime($meeting_info['meeting_start_datetime']) > time() || (strtotime($reservation_info['reservation_extend_endtime'])) < time()))){
                        $err_obj->set_error(API_MEETING_ID, "MEETING_INVALID");
                        return $this->display_error("101", "時間外の予約です", $this->get_error_info($err_obj));
                    }
                } else {
                    // 直前の会議取得
                    $last_meeting_key = $objRoom->getOne("room_key='".addslashes($room_key)."'", 'meeting_key');
                    // 異なる会議
                    if ($meeting_info['meeting_ticket'] != $last_meeting_key) {
                        // 開催中
                        $ret = $obj_N2MY_Meeting->getMeetingStatus($room_key, $last_meeting_key);
                        if ($ret['status'] == '1') {
                            return $this->display_error("101", "前回会議が開催中です", $this->get_error_info($err_obj));
                        }
                    }
                }
            }
        }
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $this->logger2->info($meeting_info['meeting_ticket']);
        // 会議情報取得
        if (!$meeting_info = $obj_N2MY_Meeting->getMeeting($room_key, $login_type, $meeting_info['meeting_ticket'])) {
            $err_obj->set_error(0, "MEETING_INVALID");
//            return $this->display_error(0, "MEETING_INVALID");
        }
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $meeting_key = $meeting_info["meeting_key"];
        $country_key = $this->session->get("country_key");
        // 地域が特定できない場合は利用可能な地域をデフォルトとする
        $country_list = $this->get_country_list();
        if (!array_key_exists($country_key, $country_list)) {
            $this->logger2->info($country_list);
            $_country_data = array_shift($country_list);
            $country_key = $_country_data["country_key"];
        }
        // オプション指定
        $options = array(
            "meeting_name" => $meeting_info["meeting_name"],
            "user_key"     => $user_info["user_key"],
            "start_time"   => $meeting_info["start_time"],
            "end_time"     => $meeting_info["end_time"],
            "country_id"   => $country_key,
            "password"     => $meeting_info["meeting_password"]
        );
        // 現在の部屋のオプションで会議更新
        $core_session = $obj_N2MY_Meeting->createMeeting($room_key, $meeting_key, $options);

        //会議内からの招待メールのため会議作成毎にmgm_relationにmeeting_session_idを追加
        require_once("classes/mgm/dbi/relation.dbi.php");
        $objMgmRelationTable = new MgmRelationTable(
                                    $this->get_auth_dsn());
        $resultRelation = $objMgmRelationTable->getRow(sprintf("relation_key='%s'", $core_session));
        if (!$resultRelation) {
            $data = array(
                        "relation_key"  => $core_session,
                        "user_key"      => $user_info["user_id"],
                        "relation_type" => "meeting",
                        "status"        => 1,
                    );
            $add_data = $objMgmRelationTable->add($data);
        }

        // ユーザごとのオプションを取得
        $type        = $this->request->get("type", "normal");
        if ($is_audience = $this->session->get('invited_meeting_audience')) {
            switch($type) {
                case 'normal':
                    $type = 'audience';
                    break;
                case 'multicamera':
                    $type = 'multicamera_audience';
                    break;
                case 'whiteboard':
                    $type = 'whiteboard_audience';
                    break;
                case 'invisible_wb':
                    $type = 'invisible_wb_audience';
                    break;
            }
        }
//        $mode = "";
//        if ($meeting_info['is_trial']) {
        //ユーザーがトライアルである場合
        $mode = ( $user_info["user_status"] == 2 ) ? "trial" : "";
        //participant登録持はinvitedGuestのタイプはないため、inviteに変更する
        if ($login_type == "invitedGuest") {
            $login_type = "invite";
        }

        //Freeの場合はログインが必要なようにflgで渡す(ログインフラグがある時はパスワードフラグは認証時に渡す)
        $need_login_flg = 0;
        require_once("classes/dbi/member_room_relation.dbi.php");
        $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
        $member_info = $this->session->get( "member_info" );
        $relation_list = "";
        if ($member_info) {
            $where = "member_key = ".$member_info["member_key"].
                     " AND room_key = '".$room_key."'";
            $relation_list = $objRoomRelation->getRowsAssoc($where);
        }
        //自分の部屋の場合はログイン不要
        if ( ($user_info["account_model"] == "member"  || $user_info["account_model"] == "free") && !$relation_list) {
            $need_login_flg = 1;
            $password_flg   = "0";
        }

        require_once( "classes/dbi/meeting.dbi.php" );
        $obj_Meeting = new MeetingTable($this->get_dsn());
        $where = "meeting_session_id = '".addslashes($core_session)."'" .
            " AND is_deleted = 0";
        $meeting_info = $obj_Meeting->getRow($where);
        if (!$meeting_info) {
            $message["title"] = MEETING_START_ERROR_TITLE;
            $message["body"] = MEETING_START_ERROR_BODY;
            return $this->render_valid($message);
        }
        $this->session->set("mobile_start_flg", true);
        if ($meeting_info['pin_cd'] && $member_info["member_key"]) {
            //メンバー紐付け用に乱数発行
            $_member_pin_cd = str_pad(mt_rand(0, 99999999), 8, "0", STR_PAD_LEFT);
            $pin_cd = $meeting_info['pin_cd']."-".$_member_pin_cd;
            require_once("classes/dbi/member.dbi.php");
            $objMember = new MemberTable($this->get_dsn());
            $where = "member_key = ".$member_info["member_key"];
            $update_data = array("member_pin_cd" => $pin_cd);
            $objMember->update($update_data, $where);
        } else {
            $pin_cd = $meeting_info['pin_cd'];
        }
        $name = $member_info["member_name"] ? $member_info["member_name"] : "";
        $url    = $this->config->get("N2MY", "mobile_protcol", 'vcube-meeting')."://join?session=".session_id()."&room_id=".$meeting_info["room_key"]."&name=".$name."&entrypoint=".N2MY_BASE_GUEST_URL."&pin_code=".$pin_cd;
$this->logger2->info($url);
        if ($meeting_info["meeting_password"]) {
            $password_flg = "0";
            require_once ("classes/dbi/reservation.dbi.php");
            try {
                $reservation_obj = new ReservationTable($this->get_dsn());
                $where = "user_key = ".addslashes($user_info["user_key"])." AND reservation_session = '".addslashes($meeting_info["reservation_session"])."'";
                $reservation_info = $reservation_obj->getRow($where, "user_key,reservation_session, reservation_pw_type");
            } catch (Exception $e) {
                $this->logger2->error($e->getMessage());
                return $this->display_error("1000", "DATABESE_ERROR", $this->get_error_info($err_obj));
            }
            $this->logger->debug("reservation_info",__FILE__,__LINE__,$reservation_info);
            if ($reservation_info["reservation_pw_type"] == "1") {
                $password_flg = "1";
            }
        }
        $data = array(
            "url"               => $url,
            API_MEETING_ID      => $this->ticket_to_session($meeting_info["meeting_ticket"]),
            "meeting_name"      => $meeting_info["meeting_name"],
            "password_flg"      => $password_flg,
            "need_login_flg"    => $need_login_flg,
        );
        return $this->output($data);
    }

    function action_meeting_display() {
        $id = $this->request->get("id");
        if (!$id) {
            $this->logger2->warn("id parameter error");
            return false;
        }
        require_once("lib/EZLib/EZUtil/EZEncrypt.class.php");
        $key = N2MY_ENCRYPT_KEY;
        $iv = N2MY_ENCRYPT_IV;
        $id = str_replace("-", "/", $id);
        $id = str_replace(' ', '+', $id);
        $id = str_replace('_', '+', $id);
        $decryptdata = EZEncrypt::decrypt($key, $iv, $id);
        $this->logger2->info($decryptdata);
        $this->checkAuthorizationMeeting($decryptdata);

        $type = $this->session->get( "type" );
        require_once ("lib/EZLib/EZHTML/EZTemplate.class.php");
        $this->template = new EZTemplate($this->config->getAll('SMARTY_DIR'));
        $this->template->assign('charset', "UTF-8" );

        // 会議情報取得
        $user_info = $this->session->get("user_info");
        $meeting_key = $this->session->get("meeting_key");
        require_once( "classes/dbi/meeting.dbi.php" );
        $obj_Meetng = new MeetingTable($this->get_dsn());
        $where = "meeting_key = '".addslashes($meeting_key)."'" .
                " AND user_key = '".$user_info["user_key"]."'" .
                " AND is_active = 1" .
                " AND is_deleted = 0";
        $meeting_info = $obj_Meetng->getRow($where);
        $this->template->assign('meeting_info' , $meeting_info );
        require_once("classes/dbi/room.dbi.php");
        $obj_Room = new RoomTable($this->get_dsn());
        $room_info = $obj_Room->getRow("room_key = '".addslashes($meeting_info['room_key'])."'");
        $this->template->assign("room_info" , $room_info);

        //Skinsの選択を追加
        $skinName = $user_info["user_logo"] ? "SkinsFuji" : "Skins";
        $this->template->assign('skinName' , $skinName );
        $this->template->assign('locale' , $this->get_language() );

        $meeting_version = $this->session->get("meeting_version");
        if ($meeting_version >= "4.7.0.0") {
           $version = "4.7.0.0";
        } else {
           $version = "4.6.5.0";
        }
        $this->template->assign('meeting_version' , $version );

        $content_delivery_base_url = $this->get_content_delivery_base_url();
        $this->template->assign("content_delivery_base_url", $content_delivery_base_url);

        // eco canceler
        $certificate = substr(md5(uniqid(rand(), true)), 1, 16);
        $oneTime = array(
                    "certificate"    =>    $certificate,
                    "createtime"    =>    time());
        $this->session->set("oneTime", $oneTime);
        $this->template->assign('certificateId', $certificate );
        $this->template->assign('apiUrl', '/services/api.php?action_issue_certificate');

        // UserAgent更新
        $this->template->assign('user_agent_ticket' , $this->session->get("user_agent_ticket") );
        // ディスプレイサイズ指定
        $this->template->assign('display_size', htmlspecialchars($this->session->get("display_size"), ENT_QUOTES));
        if ($this->session->get("fl_ver") == "as3") {
            $this->template->assign('meeting_type' , htmlspecialchars($this->session->get("meeting_type"),ENT_QUOTES) );
            $this->template->assign('participant_name' , $this->session->get("participant_name") );
            $this->template->assign('ignore_staff_reenter_alert' , $room_info["ignore_staff_reenter_alert"] );
            // V-Cube Meetingのバージョンが違う場合、キャッシュしたデータを使わせない。
            $version_file = N2MY_APP_DIR."version.dat";
            if ($fp = fopen($version_file, "r")) {
                $version = trim(fgets($fp));
            }
            $this->template->assign('cachebuster' , $version);
            $this->display_encrypt( sprintf( 'core/%s/meeting_as3.t.html', $type ) );
        } else {
            $this->display_encrypt( sprintf( 'core/%s/meeting_base.t.html', $type ) );
        }
    }

    private function get_content_delivery_base_url() {
        $protocol = '';
        $content_delivery_base_url = '';
        $country_key = $this->session->get("country_key");

        if($this->config->get("CONTENT_DELIVERY", "is_enabled", false)) {
            $country_key = $this->session->get("country_key");
            // 中国からの入室の場合、データの参照元をCDNからではなくBravからに設定する。
            $content_delivery_host = $this->config->get("CONTENT_DELIVERY_HOST", $country_key, "");
            if($content_delivery_host) {
                if(strlen($content_delivery_host)) {
                    $protocol = isset($_SERVER["HTTPS"]) ? 'https://' : 'http://';
                }
                $content_delivery_base_url = $protocol . $content_delivery_host;
                $meeting_file_dir = $this->config->get("CONTENT_DELIVERY", "meeting_file_dir", "");
                if(strlen($meeting_file_dir)) {
                    $content_delivery_base_url .= '/' . $meeting_file_dir;
                }
            }
        }
        return $content_delivery_base_url;
    }

    /**
     * 会議開始。Mobaileの場合はアプリにリダイレクト
     *
     * @param
     * @return 会議データを作成し、リダイレクトで会議室に入室
     */
    function action_enter() {
        $this->session->remove("meeting_auth_flg");
        require_once("classes/N2MY_Meeting.class.php");
        require_once("classes/dbi/room.dbi.php");
        // チェックデータ（リクエスト）編集
        $request = $this->request->getAll();
        $room_key = $this->request->get(API_ROOM_ID);
        $obj_Meeting = new DBI_Meeting($this->get_dsn());
        $objRoom = new RoomTable($this->get_dsn());
        if ($meeting_ticket = $this->session->get('invited_meeting_ticket')) {
            $where = "room_key = '".addslashes($room_key)."'" .
                    " AND meeting_ticket = '".addslashes($meeting_ticket)."'" .
                    " AND is_active = 1" .
                    " AND is_deleted = 0";
            $meeting_key = $obj_Meeting->getOne($where, 'meeting_session_id');
        } else {
            $meeting_key = $this->request->get(API_MEETING_ID);
        }
        $this->logger2->info($meeting_key);
        $obj_N2MY_Meeting = new N2MY_Meeting($this->get_dsn());
        // チェックルール編集
        $rules = array(
            API_ROOM_ID => array(
                "required" => true,
                "allow" => array_keys($this->session->get("room_info")),
                ),
            "flash_version" => array(
                "allow" => array("as2", "as3"),
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
            "screen_mode"  => array(
                "allow"    => array('normal', 'wide'),
                ),
            "mobile"  => array(
                "allow"    => array('1', '0'),
                ),
        );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        // ログインタイプ取得
        $login_type = $this->session->get("login_type");
        // ユーザ情報
        $user_info = $this->session->get("user_info");
        $member_info = $this->session->get( "member_info" );
        //member課金を利用していてメンバー以外が会議を利用しようとした場合
        if ( $user_info["account_model"] == "member" && !$member_info ) {
//            $err_obj->set_error(0, "MEMBER_INVALID");
//            return $this->display_error(0, "MEMBER_INVALID");
        }
        // 会議の指定が有る場合
        $meeting_info = array();
        if ($meeting_key) {
            $where = "room_key = '".addslashes($room_key)."'" .
                    " AND meeting_session_id = '".addslashes($meeting_key)."'" .
                    " AND is_active = 1" .
                    " AND is_deleted = 0";
            $meeting_info = $obj_Meeting->getRow($where);
            $this->logger2->info($meeting_info);
            // 予約で時間外
            if (!$meeting_info) {
                $err_obj->set_error(API_MEETING_ID, "MEETING_INVALID");
                return $this->display_error("101", "対象の会議が存在しないか、既に終了しています", $this->get_error_info($err_obj));
            } else {
                if ($meeting_info["is_reserved"] &&
                    ((strtotime($meeting_info['meeting_start_datetime']) > time() ||
                     (strtotime($meeting_info['meeting_stop_datetime'])) < time())
                    )
                ) {
                    $err_obj->set_error(API_MEETING_ID, "MEETING_INVALID");
                    return $this->display_error("101", "時間外の予約です", $this->get_error_info($err_obj));
                } else {
                    // 直前の会議取得
                    $last_meeting_key = $objRoom->getOne("room_key='".addslashes($room_key)."'", 'meeting_key');
                    // 異なる会議
                    if ($meeting_info['meeting_ticket'] != $last_meeting_key) {
                        // 開催中
                        $ret = $obj_N2MY_Meeting->getMeetingStatus($room_key, $last_meeting_key);
                        if ($ret['status'] == '1') {
                            return $this->display_error("101", "前回会議が開催中です", $this->get_error_info($err_obj));
                        }
                    }
                }
            }
        }
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $this->logger2->info($meeting_info['meeting_ticket']);
        // 会議情報取得
        if (!$meeting_info = $obj_N2MY_Meeting->getMeeting($room_key, $login_type, $meeting_info['meeting_ticket'])) {
            $err_obj->set_error(0, "MEETING_INVALID");
//            return $this->display_error(0, "MEETING_INVALID");
        }
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $meeting_key = $meeting_info["meeting_key"];
        $country_key = $this->session->get("country_key");
        // 地域が特定できない場合は利用可能な地域をデフォルトとする
        $country_list = $this->get_country_list();
        if (!array_key_exists($country_key, $country_list)) {
            $this->logger2->info($country_list);
            $_country_data = array_shift($country_list);
            $country_key = $_country_data["country_key"];
        }
        // オプション指定
        $options = array(
            "meeting_name" => $meeting_info["meeting_name"],
            "user_key"     => $user_info["user_key"],
            "start_time"   => $meeting_info["start_time"],
            "end_time"     => $meeting_info["end_time"],
            "country_id"   => $country_key,
            "password"     => $meeting_info["meeting_password"]
        );
        // 現在の部屋のオプションで会議更新
        $core_session = $obj_N2MY_Meeting->createMeeting($room_key, $meeting_key, $options);

        //会議内からの招待メールのため会議作成毎にmgm_relationにmeeting_session_idを追加
        require_once("classes/mgm/dbi/relation.dbi.php");
        $objMgmRelationTable = new MgmRelationTable(
                                    $this->get_auth_dsn());
        $resultRelation = $objMgmRelationTable->getRow(sprintf("relation_key='%s'", $core_session));
        if (!$resultRelation) {
            $data = array(
                        "relation_key"  => $core_session,
                        "user_key"      => $user_info["user_id"],
                        "relation_type" => "meeting",
                        "status"        => 1,
                    );
            $add_data = $objMgmRelationTable->add($data);
        }

        // ユーザごとのオプションを取得
        $type        = $this->request->get("type", "normal");
        if ($is_audience = $this->session->get('invited_meeting_audience')) {
            switch($type) {
                case 'normal':
                    $type = 'audience';
                    break;
                case 'multicamera':
                    $type = 'multicamera_audience';
                    break;
                case 'whiteboard':
                    $type = 'whiteboard_audience';
                    break;
                case 'invisible_wb':
                    $type = 'invisible_wb_audience';
                    break;
            }
        }
//        $mode = "";
//        if ($meeting_info['is_trial']) {
        //ユーザーがトライアルである場合
        $mode = ( $user_info["user_status"] == 2 ) ? "trial" : "";
        //participant登録持はinvitedGuestのタイプはないため、inviteに変更する
        if ($login_type == "invitedGuest") {
            $login_type = "invite";
        }

        //Freeの場合はログインが必要なようにflgで渡す(ログインフラグがある時はパスワードフラグは認証時に渡す)
        $need_login_flg = 0;
        require_once("classes/dbi/member_room_relation.dbi.php");
        $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
        $member_info = $this->session->get( "member_info" );
        $relation_list = "";
        if ($member_info) {
            $where = "member_key = ".$member_info["member_key"].
                     " AND room_key = '".$room_key."'";
            $relation_list = $objRoomRelation->getRowsAssoc($where);
        }
        //自分の部屋の場合はログイン不要
        if ( ($user_info["account_model"] == "member"  || $user_info["account_model"] == "free") && !$relation_list) {
            $need_login_flg = 1;
            $password_flg   = "0";
        }

        /* ブラウザ以外のデバイス */
        //モバイル判別
        if (strpos($_SERVER['HTTP_USER_AGENT'], "iPad;") || strpos($_SERVER['HTTP_USER_AGENT'], "Android") || strpos($_SERVER['HTTP_USER_AGENT'],"iPhone") || strpos($_SERVER['HTTP_USER_AGENT'],"iPod")) {
            require_once( "classes/dbi/meeting.dbi.php" );
            $obj_Meeting = new MeetingTable($this->get_dsn());
            $where = "meeting_session_id = '".addslashes($core_session)."'" .
                " AND is_deleted = 0";
            $meeting_info = $obj_Meeting->getRow($where);
            if (!$meeting_info) {
                $message["title"] = MEETING_START_ERROR_TITLE;
                $message["body"] = MEETING_START_ERROR_BODY;
                return $this->render_valid($message);
            }
            $url    = $this->config->get("N2MY", "mobile_protcol", 'vcube-meeting')."://join?session=".session_id()."&room_id=".$meeting_info["room_key"]."&entrypoint=".N2MY_GUEST_BASE_URL."&pin_code=".$meeting_info['pin_cd'];
            $data = array(
                "url"               => $url,
                API_MEETING_ID      => $this->ticket_to_session($meeting_info["meeting_ticket"]),
                "meeting_name"      => $meeting_info["meeting_name"],
                "password_flg"      => $password_flg,
                "need_login_flg"    => $need_login_flg,
            );
            return $this->output($data);
            exit();
        }

        // ホワイトボードアップロードメールアドレス
        $mail_upload_address = ($this->config->get("N2MY", "mail_wb_host")) ? $room_key."@".$this->config->get("N2MY", "mail_wb_host") : "";
        $user_options = array(
            "id"                  => $member_info["member_key"],
            "name"                => $this->request->get("name", $member_info["member_name"]),
            "participant_email"   => $member_info["member_email"],
            "participant_station" => $this->request->get("station", $member_info["member_station"]),
            "participant_country" => $country_key,
            "member_key"          => $member_info["member_key"],
            "narrow"              => $this->request->get("is_narrow"),
            "lang"                => $this->session->get("lang"),
            "skin_type"           => $this->request->get("skin_type", ""),
            "mode"                => $mode,
            "role"                => $login_type,
            "mail_upload_address" => $mail_upload_address,
            "account_model"       => $user_info["account_model"],
            );
        // 会議入室
        $meetingDetail = $obj_N2MY_Meeting->startMeeting($core_session, $type, $user_options);

        $mobile_flg = $this->request->get("mobile");
        $_room_info = $this->session->get("room_info");
        $room_options = $_room_info[$room_key]["options"];

        // 先に入室しているpolycom clientが存在する場合 participant record にmeeting_keyを追加更新
        if ($meetingDetail["ives_conference_id"] && $meetingDetail["ives_did"]) {
          require_once("classes/polycom/Polycom.class.php");
          $polycom = new PolycomClass($this->get_dsn());
          $polycom->addMeetingKey2PolycomParticipant($meetingDetail["meeting_key"], $meetingDetail["ives_conference_id"], $meetingDetail["ives_did"]);
        }

        //マルチカメラと資料共有対応
        if ($type == "normal" && $room_options["multicamera"]) {
             $type = "multicamera";
        } else if ($type == "normal" && $room_options["whiteboard"]) {
            $type = "whiteboard";
        } else if ($type == "audience" && $room_options["multicamera"]) {
             $type = "multicamera_audience";
        } else if ($type == "audience" && $room_options["whiteboard"]) {
            $type = "whiteboard_audience";
        }
        //リダイレクト先の core/htdocs/sercie/*　の処理を直接書いています。
        $this->session->set( "room_key", $room_key );
        $this->session->set( "type", $meetingDetail["participant_type_name"] );
        $this->session->set( "meeting_type", $type );
        $this->session->set( "meeting_key", $meetingDetail["meeting_key"] );
        $this->session->set( "participant_key", $meetingDetail["participant_key"] );
        $this->session->set( "participant_name", $this->request->get("name", $member_info["member_name"]) );
        $this->session->set( "mail_upload_address", $mail_upload_address );
        $this->session->set( "fl_ver", $this->request->get("flash_version", "as2") );
        $this->session->set( "reload_type", 'normal');
        $this->session->set( "meeting_version", $user_info["meeting_version"] );
        $this->session->set( "user_agent_ticket", uniqid());
        // 画面サイズ
        $screen_mode = $this->request->get("screen_mode", "normal");
        switch ($screen_mode) {
            case 'normal' :
                $display_size = '4to3';
                break;
            case 'wide' :
                $display_size = '16to9';
                break;
            default :
                $display_size = '4to3';
                break;
        }
        $this->session->set( "display_size", $display_size);
        // 会議入室
        $url = N2MY_BASE_URL."services/?action_meeting_display=&".N2MY_SESS_NAME."=".session_id();
        if ($meeting_info["meeting_password"]) {

            $this->session->set("redirect_url", $url, $this->_name_space);
            $url = N2MY_BASE_URL."services/" .
                    "?action_login=" .
                    "&".N2MY_SESS_NAME."=".session_id().
                    "&reservation_session=".$meeting_info["reservation_session"].
                    "&ns=".$this->_name_space;

            $password_flg = "0";
            require_once ("classes/dbi/reservation.dbi.php");
            try {
                $reservation_obj = new ReservationTable($this->get_dsn());
                $where = "user_key = ".addslashes($user_info["user_key"])." AND reservation_session = '".addslashes($meeting_info["reservation_session"])."'";
                $reservation_info = $reservation_obj->getRow($where, "user_key,reservation_session, reservation_pw_type");
            } catch (Exception $e) {
                $this->logger2->error($e->getMessage());
                return $this->display_error("1000", "DATABESE_ERROR", $this->get_error_info($err_obj));
            }
            $this->logger->debug("reservation_info",__FILE__,__LINE__,$reservation_info);
            if ($reservation_info["reservation_pw_type"] == "1") {
                $password_flg = "1";
            }
        }
        if ($this->request->get("callbackurl")) {
            $where = "room_key = '".addslashes($room_key)."'" .
                    " AND meeting_key = '".addslashes($meetingDetail["meeting_key"])."'" .
                    " AND is_active = 1" .
                    " AND is_deleted = 0";
            $meeting_data = $obj_Meeting->getRow($where, "room_addition");
            if ($meeting_info["room_addition"]) {
                $addition = unserialize($meeting_data["room_addition"]);
            }
            $addition["callbackurl"] = urldecode($this->request->get("callbackurl"));
            $update_data["room_addition"] = serialize($addition);
            $obj_Meeting->update($update_data, $where);
        }
        $data = array(
            "url"               => $url,
            API_MEETING_ID      => $this->ticket_to_session($meetingDetail["meeting_ticket"]),
            "meeting_name"      => $meetingDetail["meeting_name"],
            "password_flg"      => $password_flg,
            "need_login_flg"    => $need_login_flg,
//            "meeting_sequence_key"  => $meetingDetail["meeting_sequence_key"],
//            "participant_key"       => $meetingDetail["participant_key"],
//            "participant_id"        => $meetingDetail["participant_session_id"],
        );
        return $this->output($data);
    }

    function action_get_invite_url() {
        require_once 'classes/mgm/MGM_Auth.class.php';
        require_once 'classes/N2MY_Auth.class.php';
        require_once 'classes/N2MY_Account.class.php';
        require_once 'classes/dbi/meeting.dbi.php';
        require_once 'classes/dbi/user.dbi.php';
        require_once 'classes/N2MY_Meeting.class.php';
        // DB選択
        $request = $this->request->getAll();
        $meeting_id  = $request[API_MEETING_ID];
        $output_type = $this->request->get("output_type");
        // パラメタチェック
        $rules = array(
            API_MEETING_ID => array(
                "required" => true,
                "valid_integer" => true,
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        $dsn_info = $obj_MGMClass->getRelationDsn($meeting_id, "meeting");
        $server_info = $obj_MGMClass->getServerInfo($dsn_info["server_key"]);
        if (!$dsn_info) {
            $err_msg["errors"][] = array(
                "field"   => API_MEETING_ID,
                "err_cd"  => "invalid",
                "err_msg" => "ミーティングキーが不正です",
                );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        // 会議情報
        $objMeeting = new MeetingTable($server_info["dsn"]);
        $where = "meeting_session_id = '".addslashes($meeting_id)."'" .
            " AND is_active = 1" .
            " AND is_deleted = 0";
        $meeting_info = $objMeeting->getRow($where);
        if (!$meeting_info) {
            $err_msg["errors"][] = array(
                "field"   => API_MEETING_ID,
                "err_cd"  => "invalid",
                "err_msg" => "ミーティングキーが不正です",
                );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        //会議内からの招待メールのためユーザー、オーディエンス用のハッシュ値を生成
        require_once "classes/N2MY_Invitation.class.php";
        $objInvitation = new N2MY_Invitation($server_info["dsn"]);

        require_once "classes/dbi/meeting_invitation.dbi.php";
        $objMeetingInvitationTable = new MeetingInvitationTable($server_info["dsn"]);
        if (!$invitationData = $objMeetingInvitationTable->getRow(
                                                            sprintf("meeting_key='%s' AND status = 1", $meeting_info["meeting_key"]),
                                                            "*",
                                                            array("invitation_key"=>"desc"))) {
            $user_session_id = md5(uniqid(rand(), 1));
            $audience_session_id = md5(uniqid(rand(), 1));
            $invitationData = array(
                                "meeting_key" => $meeting_info["meeting_key"],
                                "meeting_session_id" => $meeting_info["meeting_session_id"],
                                "user_session_id" => $user_session_id,
                                "audience_session_id" => $audience_session_id,
                                "status" => 1
                                );
            if ($meeting_info["is_reserved"])
                $invitationData["meeting_ticket"] = $meeting_info["meeting_ticket"];
            $objMeetingInvitationTable->add($invitationData);
        }
        $lang = $this->session->get("lang") ? $this->session->get("lang") : "en";
        $invite_url = N2MY_BASE_URL."g/".$lang."/".$meeting_info["meeting_session_id"]."/";

        $data = array(
            API_MEETING_ID  => $meeting_id,
            "user_invite_url"           => $invite_url.$invitationData["user_session_id"],
            "audience_invite_url"       => $invite_url.$invitationData["audience_session_id"],
        );
        return $this->output($data);
    }

    /**
     *
     */
    function action_stop() {
        $request = $this->request->getAll();
        $room_key = $this->request->get(API_ROOM_ID);
        $meeting_session = $this->request->get(API_MEETING_ID);
        // チェックルール編集
        $rules = array(
            API_ROOM_ID => array(
                "required" => true,
                "allow" => array_keys($this->session->get("room_info")),
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        if (!$meeting_session) {
            // 部屋で直前に行われた会議を取得（今後、１部屋複数会議を実現した場合この機能は廃止になる可能性有り）
            require_once("classes/dbi/room.dbi.php");
            $objRoom = new RoomTable($this->get_dsn());
            $where = "room_key='".addslashes($room_key)."'" .
                    " AND room_status = 1";
            $meeting_ticket = $objRoom->getOne($where, "meeting_key");
            $meeting_session = $this->ticket_to_session($meeting_ticket);
        } else {
            $meeting_ticket = $this->session_to_ticket($meeting_session);
        }
        require_once ( "classes/core/Core_Meeting.class.php" );
        $obj_CoreMeeting = new Core_Meeting($this->get_dsn());
        $meeting_db = new N2MY_DB($this->get_dsn(), "meeting");
        $where = "meeting_session_id = '".addslashes($meeting_session)."'";
        $meeting_info = $meeting_db->getRow($where);
        // 予約があれば予約も終了
        if ($meeting_info["is_reserved"]) {
            require_once ("classes/dbi/reservation.dbi.php");
            $reservation_obj = new ReservationTable($this->get_dsn());
            $reservation_info = $reservation_obj->getRow("meeting_key = '".addslashes($meeting_ticket)."'");
            $reservation_obj->cancel($reservation_info["reservation_session"]);
        }
        $obj_CoreMeeting->stopMeeting($meeting_info["meeting_key"]);
        $obj_Meeting = new DBI_Meeting($this->get_dsn());
        $where = "meeting_session_id = '".addslashes($meeting_session)."'" .
                " AND is_deleted = 0";
        $data = $obj_Meeting->getRow($where);

        $data = array(
            API_ROOM_ID              => $data['room_key'],
            API_MEETING_ID           => $data['meeting_session_id'],
            'meeting_name'           => $data['meeting_name'],
            'meeting_size_used'      => $data['meeting_size_used'],
            'is_locked'              => $data['is_locked'],
            'is_reserved'            => $data['is_reserved'],
            'is_publish'             => $data['is_publish'],
            'meeting_use_minute'     => $data['meeting_use_minute'],
            'eco_move'               => $data['eco_move'],
            'eco_time'               => $data['eco_time'],
            'eco_fare'               => $data['eco_fare'],
            'eco_co2'                => $data['eco_co2'],
            'meeting_start_date'     => strtotime($data['actual_start_datetime']),
            'meeting_end_date'       => strtotime($data['actual_end_datetime']),
            API_MEETINGLOG_MINUTES   => $data['has_recorded_minutes'],
            API_MEETINGLOG_VIDEO     => $data['has_recorded_video']
        );
//        unset($data['meeting_key']);
//        unset($data['fms_path']);
//        unset($data['layout_key']);
//        unset($data['server_key']);
//        unset($data['sharing_server_key']);
//        unset($data['user_key']);
//        unset($data['meeting_ticket']);
//        unset($data['meeting_country']);
//        unset($data['meeting_room_name']);
//        unset($data['meeting_tag']);
//        unset($data['member_keys']);
//        unset($data['participant_names']);
//        unset($data['meeting_max_audience']);
//        unset($data['meeting_max_extendable']);
//        unset($data['meeting_max_seat']);
//        unset($data['meeting_size_recordable']);
//        unset($data['meeting_size_uploadable']);
//        unset($data['meeting_log_owner']);
//        unset($data['meeting_log_password']);
//        unset($data['meeting_start_datetime']);
//        unset($data['meeting_stop_datetime']);
//        unset($data['meeting_extended_counter']);
//        unset($data['is_active']);
//        unset($data['is_blocked']);
//        unset($data['is_extended']);
//        unset($data['version']);
//        unset($data['create_datetime']);
//        unset($data['update_datetime']);
//        unset($data['room_addition']);
//        unset($data['is_deleted']);
//        unset($data['use_flg']);
//        unset($data['eco_info']);
//        unset($data['meeting_keys']);
//        unset($data['meeting_names']);
//        //
//        $data['meeting_start_date'] = strtotime($data['actual_start_datetime']);
//        unset($data['actual_start_datetime']);
//        $data['meeting_end_date'] = strtotime($data['actual_end_datetime']);
//        unset($data['actual_end_datetime']);
//        $data[API_MEETINGLOG_MINUTES] = $data["has_recorded_minutes"];
//        unset($data["has_recorded_minutes"]);
//        $data[API_MEETINGLOG_VIDEO] = $data["has_recorded_video"];
//        unset($data["has_recorded_video"]);
//        // 統一
//        $data[API_ROOM_ID] = $data['room_key'];
//        unset($data['room_key']);
//        $data[API_MEETING_ID] = $data['meeting_session_id'];
//        unset($data['meeting_session_id']);

        return $this->output($data);
    }

    /**
     * ノッカー
     */
    function action_knocker() {
        $request = $this->request->getAll();
        $meeting_ticket = $this->request->get(API_MEETING_ID);
        // チェックルール編集
        $rules = array(
            API_MEETING_ID => array(
                "required" => true,
                ),
            "message" => array(
                "required" => true,
                "maxlen" => 100,
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        require_once("classes/dbi/meeting.dbi.php");
        require_once("classes/mgm/dbi/FmsServer.dbi.php");
        require_once("lib/EZLib/EZUtil/EZRtmp.class.php");
        $obj_Meetng = new MeetingTable($this->get_dsn());
        $obj_FmsServer = new DBI_FmsServer( N2MY_MDB_DSN );
        $rtmp = new EZRtmp();
        // 会議のステータス取得
        $where = "meeting_session_id = '".addslashes($meeting_ticket)."'";
        $meeting_info = $obj_Meetng->getRow($where);
        // サーバ情報
        $server_info = $obj_FmsServer->getRow( sprintf("server_key=%d", $meeting_info["server_key"] ));
        $msg = addslashes($request["message"]);
        $msg = "'".$msg."'";
        $meeting_id = $obj_Meetng->getMeetingID($meeting_info["meeting_key"]);
        $uri = "rtmp://".$server_info["server_address"]."/".$this->config->get( "CORE", "app_name" )."/".$meeting_info["fms_path"].$meeting_id;
        $cmd .= "\"h = RTMPHack.new; " .
                "h.connection_uri = '" . $uri . "';" .
                "h.connection_args = [0x310, {:target => 'ALL', :message => ".$msg. "}];" .
                "h.method_name = '';" .
                "h.execute\"";
        $this->logger2->info($cmd);
        $rtmp->run($cmd);
        $data = array();
        return $this->output($data);
    }

    /**
     * 資料追加
     *
     */
    function action_wb_upload() {
        $request = $this->request->getAll();
        $meeting_ticket = $this->request->get(API_MEETING_ID);
        $file = $_FILES["file"];
        // チェックルール編集
        $rules = array(
            API_MEETING_ID => array(
                "required" => true,
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        if (!$file) {
            $err_obj->set_error("file", "required");
        } else {
            if (!EZValidator::valid_file_ext($file["name"], split(",", $this->config->get("N2MY", "convert_format")))) {
                $err_obj->set_error("file", "file_ext", split(",", $this->config->get("N2MY", "convert_format")));
            }
        }
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        require_once("lib/EZLib/EZUtil/EZString.class.php");
        require_once( "classes/dbi/meeting.dbi.php" );
        $user_info = $this->session->get("user_info");
        // 会議のステータス取得
        $obj_Meetng = new MeetingTable($this->get_dsn());
        $where = "meeting_session_id = '".addslashes($meeting_ticket)."'";
        $meeting_info = $obj_Meetng->getRow($where);
        $this->logger2->info(array($meeting_info, $obj_Meetng->_conn->last_query));
        $meeting_key = $meeting_info["meeting_key"];
        $document_path = $user_info["user_id"]."/".$meeting_info["room_key"]."/";

        $file = $_FILES["file"];
        $dir = $this->get_work_dir();
        $string = new EZString();
        $document_id = $string->create_id();
        $fileinfo = pathinfo($file["name"]);
        $tmp_name = $dir."/".$document_id.".".$fileinfo["extension"];
        move_uploaded_file($file["tmp_name"], $tmp_name);
        $name = ($name) ? $name : $fileinfo["basename"];
        // 部屋一覧取得
        $url = N2MY_LOCAL_URL."/api/document/";
        $post_data = array(
            "action_wb_upload" => "1",
            "db_host"       => $this->get_dsn_key(),
            "meeting_key"   => $meeting_key,
            "document_path" => $document_path,
            "Filedata"      => "@".$tmp_name,
            );
        $ch = curl_init();
        $option = array(
            CURLOPT_URL => $url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $post_data,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => 10,
            );
        curl_setopt_array($ch, $option);
        $ret = curl_exec($ch);
        unlink($tmp_name);
        $this->logger2->info(array($option, $ret));
        $data = array("document_id" => $ret);
        return $this->output($data);
    }

    /**
     * ビデオ会議接続
     */
    function action_connect_videoconference(){
        $request = $this->request->getAll();
        $rules = array(
                'reservation_id' => array(
                        "required" => true,
                ),
                'videoconference_room_num' => array(
                        "required" => true,
                ),
                "videoconference_layout"  => array(
                        "required" => true,
                        "allow" => array("1x1","2x2","3x3","1+1","1+5","1+7"),
                ),
                "output_type"  => array(
                        "allow"    => $this->get_output_type_list(),
                ),
        );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }

        // TODO テスト用コード
        // ここから----------------------
        if($request["videoconference_room_num"] == "123"){
//            $err_obj->set_error(0, "connect error");
//            return $this->display_error("101", "CONNECT_ERROR", $this->get_error_info($err_obj));
        }
        // ----------------------ここまで

        $reservation_id = $request["reservation_id"];
        $videoconference_room_num =  $request["videoconference_room_num"];
        $videoconference_layout = $request["videoconference_layout"];

        $user_info = $this->session->get("user_info");

        require_once("classes/dbi/reservation_extend.dbi.php");
        require_once ("classes/N2MY_Reservation.class.php");
        require_once ("classes/dbi/reservation.dbi.php");
        $obj_reservation_extend = new ReservationExtendTable($this->get_dsn());
        $objReservation = new N2MY_Reservation($this->get_dsn(), $this->session->get("dsn_key"));
        $obj_Reservation     = new ReservationTable($this->get_dsn());
        $reservation_data = $objReservation->getDetail($reservation_id);

        if (!$reservation_data) {
            //エラー処理
            $err_obj->set_error(API_RESERVATION_ID,"RESERVATION_SESSION_INVALID");
            return $this->display_error(0, "PARAMETER_ERROR",$this->get_error_info($err_obj));
        }
        $obj_reservation_extend = new ReservationExtendTable($this->get_dsn());
        $where = "reservation_session_id = '" .mysql_real_escape_string($reservation_id) . "'";
        $sort = array(
                "extend_endtime" => "DESC"
        );
        $extend_infos = $obj_reservation_extend->getRowsAssoc($where , $sort );

        // 開催中かどうか判断
        $now = strtotime(date("Y-m-d H:i:s"));
        $meeting_start_time = strtotime($reservation_data["info"]["reservation_starttime_time_zone_9"]);
        $meeting_end_time = strtotime($reservation_data["info"]["reservation_endtime_time_zone_9"]);
        // 拡張データがあれば拡張データの終了日時を取得
        if($extend_infos){
            $meeting_end_time = strtotime($extend_infos[0]["extend_endtime"]);
        }
        // 開催中じゃなかったらエラー
        if($now < $meeting_start_time || $now > $meeting_end_time){
            $err_obj->set_error(API_RESERVATION_ID,"RESERVATION_SESSION_INVALID");
            return $this->display_error(0, "PARAMETER_ERROR",$this->get_error_info($err_obj));
        }

        // TODO テスト用コード
        // ここから----------------------
        if($request["videoconference_room_num"] == "111111"){
//            $this->update_video_conference_participant($reservation_data);
//            return $this->output();
        }
        // ----------------------ここまで

        // テレビ会議接続
        require_once("classes/mcu/config/McuConfigProxy.php");
        require_once("classes/mcu/resolve/Resolver.php");
        require_once("classes/dbi/video_conference_ip_list.dbi.php");
        $obj_video_conference_ip_list = new VideoConferenceIpListTable($this->get_dsn());
        $connect_ip = $obj_video_conference_ip_list->getConnectIp($videoconference_room_num, $user_info["user_key"]);
        if(!$connect_ip){
            $err_obj->set_error("VIDEOCONFERENCE_ROOM_NUM","VIDEOCONFERENCE_ROOM_NUM_INVALID");
            return $this->display_error(0, "PARAMETER_ERROR",$this->get_error_info($err_obj));
        }
        $this->logger2->info($connect_ip);

        require_once( "classes/dbi/meeting.dbi.php" );
        $obj_Meeting = new MeetingTable($this->get_dsn());
        $where = "meeting_ticket  = '". $reservation_data["info"]["meeting_key"] ."'";
        $meetingInfo = $obj_Meeting->getRow($where);
        $meeting_key = $meetingInfo["meeting_key"];

        $target = $connect_ip;
        $protocol = "SIP";
        $name = $videoconference_room_num;
$this->logger2->info(array($target,$protocol,$name));
        try {
            if (($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($meetingInfo["room_key"]))) {
                $participantId = $videoConferenceClass->callParticipant($meeting_key, $target, $protocol, $name);
               // $videoConferenceClass->setMosaic0CompositionType($meeting_key, $videoconference_layout);
            }
            else {
                throw new Exception("conference does not exists. room_key: ".$meetingInfo["room_key"]);
            }
        }
        catch (Exception $e) {
            $this->logger2->warn($e->getMessage());
            $err_obj->set_error("ERROR CONNECT","ERROR CONNECT");
            return $this->display_error(0, "ERROR CONNECT",$this->get_error_info($err_obj));
        }

        // 会議室番号、レイアウト更新
        $data = array(
                "videoconference_room_num" => $request["videoconference_room_num"],
                "videoconference_layout" => $request["videoconference_layout"]);
        $where = "reservation_key = " . $reservation_data["info"]["reservation_key"];

        // テレビ会議接続フラグ変更
        $meeting_data = array('connecting_videoconference_flg' => '1');
        $obj_Meeting->update($meeting_data, "meeting_key=".$meeting_key);


        $obj_Reservation -> update($data, $where);



        return $this->output();
    }

    /**
     * ビデオ会議切断
     *
     */
    function action_disconnect_videoconference(){
        $request = $this->request->getAll();
        $rules = array(
                'reservation_id' => array(
                        "required" => true,
                ),
                'videoconference_room_num' => array(
                        "required" => true,
                ),
                "output_type"  => array(
                        "allow"    => $this->get_output_type_list(),
                ),
        );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        if($request["videoconference_room_num"] == "123"){
//            $err_obj->set_error(0, "disconnect error");
//            return $this->display_error("101", "DISCONNECT_ERROR", $this->get_error_info($err_obj));
        }

        $reservation_id = $request["reservation_id"];

        require_once("classes/dbi/reservation_extend.dbi.php");
        require_once ("classes/N2MY_Reservation.class.php");
        require_once ("classes/dbi/reservation.dbi.php");
        $obj_reservation_extend = new ReservationExtendTable($this->get_dsn());
        $objReservation = new N2MY_Reservation($this->get_dsn(), $this->session->get("dsn_key"));
        $obj_Reservation     = new ReservationTable($this->get_dsn());
        $reservation_data = $objReservation->getDetail($reservation_id);

        if (!$reservation_data) {
            //エラー処理
            $err_obj->set_error(API_RESERVATION_ID,"RESERVATION_SESSION_INVALID");
            return $this->display_error(0, "PARAMETER_ERROR",$this->get_error_info($err_obj));
        }
        $obj_reservation_extend = new ReservationExtendTable($this->get_dsn());
        $where = "reservation_session_id = '" .mysql_real_escape_string($reservation_id) . "'";
        $sort = array(
                "extend_endtime" => "DESC"
        );
        $extend_infos = $obj_reservation_extend->getRowsAssoc($where , $sort );

        // 開催中かどうか判断
        $now = strtotime(date("Y-m-d H:i:s"));
        $meeting_start_time = strtotime($reservation_data["info"]["reservation_starttime_time_zone_9"]);
        $meeting_end_time = strtotime($reservation_data["info"]["reservation_endtime_time_zone_9"]);
        // 拡張データがあれば拡張データの終了日時を取得
        if($extend_infos){
            $meeting_end_time = strtotime($extend_infos[0]["extend_endtime"]);
        }
        // 開催中じゃなかったらエラー
        if($now < $meeting_start_time || $now > $meeting_end_time){
            $err_obj->set_error(API_RESERVATION_ID,"RESERVATION_SESSION_INVALID");
            return $this->display_error(0, "PARAMETER_ERROR",$this->get_error_info($err_obj));
        }


        // TODO テスト用コード
        // ここから----------------------
        if($request["videoconference_room_num"] == "111111"){
//            $this->update_video_conference_participant($reservation_data , 0);
//            return $this->output();
        }
        // ----------------------ここまで

        // テレビ会議切断
        require_once("classes/core/dbi/Meeting.dbi.php");
        $objMeeting  = new DBI_Meeting($this->get_dsn());
        $where = "meeting_ticket  = '". $reservation_data["info"]["meeting_key"] ."'";
        $meetingInfo = $objMeeting->getRow($where);
        $meeting_key = $meetingInfo["meeting_key"];
        try {

            if (($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($meetingInfo["room_key"]))) {
                if ($videoInfo = $videoConferenceClass->getActiveVideoConferenceByMeetingKey($meeting_key)) {
                    $videoConferenceClass->forceExitPolycomClients($meeting_key);
                }
            }
        }
        catch (SoapFault $fault) {
            $this->logger2->error($fault);
            $err_obj->set_error("ERROR DISCONNECT","ERROR DISCONNECT");
            return $this->display_error(0, "ERROR DISCONNECT",$this->get_error_info($err_obj));
        }

        return $this->output();
    }

    /*
     * 特定の会議にテレビ会議の接続状態を取得
     */
    function action_get_videoconference_status(){
        $request = $this->request->getAll();
        $rules = array(
                'reservation_id' => array(
                        "required" => true,
                ),
                "output_type"  => array(
                        "allow"    => $this->get_output_type_list(),
                ),
        );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }

        $reservation_id = $request["reservation_id"];
        require_once("classes/dbi/reservation_extend.dbi.php");
        require_once ("classes/N2MY_Reservation.class.php");
        require_once ("classes/dbi/reservation.dbi.php");
        $obj_reservation_extend = new ReservationExtendTable($this->get_dsn());
        $objReservation = new N2MY_Reservation($this->get_dsn(), $this->session->get("dsn_key"));
        $obj_Reservation     = new ReservationTable($this->get_dsn());
        $reservation_data = $objReservation->getDetail($reservation_id);

        if(!$reservation_data){
                $err_msg["errors"][] = array(
                "field"   => reservation_id,
                "err_cd"  => "invalid",
                "err_msg" => "予約IDが不正です",
                );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }

        require_once( "classes/dbi/meeting.dbi.php" );
        $obj_Meeting = new MeetingTable($this->get_dsn());
        $where = "meeting_ticket  = '". $reservation_data["info"]["meeting_key"] ."'";
        $meeting_info = $obj_Meeting->getRow($where);
        $meeting_key = $meeting_info["meeting_key"];
        $this->logger2->info($reservation_data);

        if(!$meeting_key){
                $err_msg["errors"][] = array(
                "field"   => reservation_id,
                "err_cd"  => "invalid",
                "err_msg" => "予約IDが不正です",
                );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }

        require_once( "classes/dbi/video_conference_participant.dbi.php" );
        $obj_video_conference_participant = new VideoConferenceParticipantTable($this->get_dsn());

        $where_video = "meeting_key = " . $meeting_key . " AND is_active = 1 AND participant_type = 'SIP'";
        // 一応レコードを取得しておく
        $rusult = $obj_video_conference_participant->getRow($where_video);

        if($rusult){
            $connect_status = 1;
        }else{
            $connect_status = 0;
        }

        $data = array(
                "connect_status" => $connect_status
                );

        return $this->output($data);

    }

    // 無理矢理テレビ会議接続のステータスを変更する
    private function update_video_conference_participant($reservation_data , $status = 1){

        require_once( "classes/dbi/meeting.dbi.php" );
        $obj_Meeting = new MeetingTable($this->get_dsn());
        $where = "meeting_ticket  = '". $reservation_data["info"]["meeting_key"] ."'";
        $meetingInfo = $obj_Meeting->getRow($where);
        $meeting_key = $meetingInfo["meeting_key"];
        $data = array(
                "video_conference_key" => 0,
                "media_mixer_key" => 0,
                "conference_id" => "",
                "did" => "test",
                "participant_session_id" => "",
                "part_id" => 0,
                "participant_name" => "111111",
                "participant_ip" => "1.1.1.1",
                "participant_type" => "SIP",
                "participant_state" => 90,
                "participant_key" => 0,
                "meeting_key" => $meeting_key,
                "is_active" => $status
                );
        require_once( "classes/dbi/video_conference_participant.dbi.php" );
        $obj_video_conference_participant = new VideoConferenceParticipantTable($this->get_dsn());
        $where_video = "meeting_key = " . $meeting_key;
        $rusult = $obj_video_conference_participant->getRow($where_video);
        if($rusult){
            $obj_video_conference_participant->update($data , $where_video);
        }else{
            $obj_video_conference_participant->add($data);
        }
    }
}
$main = new N2MY_Meeting_API();
$main->execute();
