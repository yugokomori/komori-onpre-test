#!/bin/sh
#
# /etc/init.d/sav-protect
#
# Copyright (C) 2004-2011 Sophos Ltd.
# All rights reserved.
#

# chkconfig: 2345 13 87
# description: Sophos Anti-Virus Daemon

. /etc/rc.d/init.d/functions

    if [ "$SAV_ROOT_OVERRIDE" != "" ] ; then
        SAV_ROOT=$SAV_ROOT_OVERRIDE
    else
        SAV_ROOT=/opt/sophos-av
    fi
    SAV_BIN="$SAV_ROOT/bin"
    SAV_LOG="$SAV_ROOT/log"
    SAV_PROTECT_LOG="$SAV_LOG/sav-protect.log"
    SAVD_CTL="$SAV_BIN/savdctl"
    TALPA_SELECT="$SAV_ROOT/engine/talpa_select"
    SAVUPDATE="$SAV_BIN/savupdate"
    SAVD_STATUS="$SAV_BIN/savdstatus"
    SAVCONFIG="$SAV_BIN/savconfig"

    if [ ! -x "$SAV_BIN" ] ; then
      exit 2
    fi

SERVICENAME=savd
DESCRIPTION="Sophos Anti-Virus daemon"
FUNCTION="Sophos Anti-Virus daemon"

SAVD_LOCKFILE=/var/lock/subsys/sav-protect

case "$1" in
    start)
        echo -n $"Starting $DESCRIPTION: "
        
    umask 077
    mkdir -p "$SAV_LOG"
    date > "$SAV_PROTECT_LOG"

    if [ -x "$TALPA_SELECT" -a -z "$SOPHOS_INSTALL_IN_PROGRESS" ]
    then
        onaccess=`LC_ALL=C LANG=C "$SAVCONFIG" query EnableOnStart`
        if [ "$onaccess" = "TRUE" -o "$onaccess" = "true" ]; then
            touch "$SAV_ROOT/etc/.enableonstart"
            preferfanotify=`LC_ALL=C LANG=C "$SAVCONFIG" query PreferFanotify`
            if [ x"$preferfanotify" != "xTRUE" -a x"$preferfanotify" != "xtrue" ] ; then
                if ! "$TALPA_SELECT" selectexisting >>"$SAV_PROTECT_LOG" </dev/null 2>&1; then
                    echo "No TBP available, running savupdate:"
                    "$SAVUPDATE" --nowait -v 0 >>"$SAV_PROTECT_LOG" 2>&1 </dev/null || true
                    # Compile if needed
                    "$TALPA_SELECT" select >>"$SAV_PROTECT_LOG" </dev/null 2>&1 || true
                fi
            fi
        fi
    fi

    "$SAVD_CTL" --daemon start sav-protect >>"$SAV_PROTECT_LOG" 2>&1 </dev/null

        RETVAL=$?
        if [ $RETVAL -eq 0 ] ; then
            success $"$SERVICENAME startup"
            touch "$SAVD_LOCKFILE" >/dev/null 2>&1
        else
            failure $"$SERVICENAME startup"
        fi
        echo
        ;;
    stop)
        echo -n $"Stopping $DESCRIPTION: "
        
    umask 077
    date >> "$SAV_PROTECT_LOG" 2>/dev/null </dev/null || true

    "$SAVD_CTL" stop sav-protect >> "$SAV_PROTECT_LOG" 2>&1 </dev/null

        RETVAL=$?
        if [ $RETVAL -eq 0 ] ; then
            success $"$SERVICENAME shutdown"
            rm -f "$SAVD_LOCKFILE" >/dev/null 2>&1
        else
            failure $"$SERVICENAME shutdown"
            if ! "$SAVD_STATUS" ; then
                rm -f $SAVD_LOCKFILE >/dev/null 2>&1
            fi
        fi
        echo
        ;;
    restart|reload)
        $0 stop
        $0 start
        RETVAL=$?
        ;;
    status)
        "$SAVD_STATUS" sav-protect
        RETVAL=$?
        ;;
    *)
        echo "Usage: $0 {start|stop|restart|status}"

    ;;
esac
exit $RETVAL
