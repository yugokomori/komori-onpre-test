<?php
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_DBI.class.php");
require_once("lib/EZLib/Transit.class.php");
require_once('dBug.php');

class TransitLog extends AppFrame {

    var $core_db = null;
    var $web_db = null;

    function init() {
        if (!$dsn = $this->get_dsn()) {
            header("Location: /admin_tool/index.php");
            exit();
        }
        $this->web_db = new N2MY_DB($dsn);
        $uri = $this->config->get("N2MY", "transit_uri");
        $transit_log = realpath(dirname(__FILE__)."/../../var/")."/transit.log";
        $this->obj_Transit = Transit::factory($uri, $transit_log);
    }

    function get_dsn() {
        if (isset($_SESSION[_adminauthsession]["dsn"])) {
            return $_SESSION[_adminauthsession]["dsn"];
        }
    }

    function default_view(){
        print '<html>';
        print '<head>';
        print '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        print '</head>';
        print '<body>';
        $y = $this->request->get("y", date("Y"));
        $m = $this->request->get("m", date("m"));
        $date = $y."-".$m;
        $prev = mktime(0,0,0,$m - 1,1, $y);
        $next = mktime(0,0,0,$m + 1,1, $y);
        print '<a href="?y='.date("Y", $prev).'&m='.date("m", $prev).'">＜</a>';
        print $date;
        print '<a href="?y='.date("Y", $next).'&m='.date("m", $next).'">＞</a>';
        $sql = "SELECT room_key" .
                ", count(*) as cnt" .
                " FROM meeting, participant" .
                " WHERE meeting.meeting_key = participant.meeting_key" .
                " AND meeting.eco_co2 > 0" .
                " AND participant.participant_station IS NOT NULL" .
                " AND meeting.actual_start_datetime like '".$date."%'" .
                " GROUP BY meeting.room_key" .
                " ORDER BY meeting.room_key";
        $rs = $this->web_db->_conn->query($sql);
        if (DB::isError($rs)) {
            die ($rs->getUserInfo());
        }
        $rows = array();
        while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            $room_key = $row["room_key"];
            $sql = "SELECT user.user_id" .
                    ",user.user_company_name as user_company_name" .
                    ",room.room_key" .
                    ",room.room_name" .
                    " FROM user, room" .
                    " WHERE user.user_key = room.user_key" .
                    " AND room.room_key = '".$room_key."'";
            $room_info = $this->web_db->_conn->getRow($sql, DB_FETCHMODE_ASSOC);
            if (DB::isError($room_info)) {
                die ($room_info->getUserInfo());
            }
            $row["user_id"] = $room_info["user_id"];
            $row["room_key"] = $room_info["room_key"];
            $row["user_name"] = $room_info["user_company_name"];
            $row["room_name"] = $room_info["room_name"];
            $rows[] = $row;
        }
        print '<hr>Transit Log';
        print '<table cellspacing=2 cellpadding=3 class="dBug_array">';
        print '<tr>';
        print '<td class="dBug_arrayHeader">user ID</td>';
        print '<td class="dBug_arrayHeader">conpany name</td>';
        print '<td class="dBug_arrayHeader">room ID</td>';
        print '<td class="dBug_arrayHeader">room name</td>';
        print '<td class="dBug_arrayHeader">use frequency count</td>';
        print '</tr>';
        $user_id = "";
        $user_cnt = 0;
        $transit_cnt = 0;
        foreach($rows as $row) {
            if ($user_id != $row["user_id"]) {
                $user_cnt++;
            }
            print '<tr>';
            print '<td class="">'.$row["user_id"].'</td>';
            print '<td class="">'.$row["user_name"].'</td>';
            print '<td class="">'.$row["room_key"].'</td>';
            print '<td class="">'.$row["room_name"].'</td>';
            print '<td class="">'.$row["cnt"].'</td>';
            print '</tr>';
            $transit_cnt += $row["cnt"];
            $user_id = $row["user_id"];
        }
        print '<tr>';
        print '<td class="dBug_arrayKey">user count</td>';
        print '<td class="dBug_arrayKey">'.$user_cnt.'</td>';
        print '<td class="dBug_arrayKey"></td>';
        print '<td class="dBug_arrayKey"></td>';
        print '<td class="dBug_arrayKey">'.$transit_cnt.'</td>';
        print '</tr>';
        print '</table>';

        print "<hr>Transit API use frequency count";
        $transit_log = $this->obj_Transit->getlog();
        new dBug($transit_log);
        print '</body>';
        print '</html>';
    }

}
$main = new TransitLog();
$main->execute();
?>