[MCU]

ignore_create_sip_account = 0

# flash
defaultId				= "5099083"
defaultPw				= "2GREO9yaQ0Yx"
defaultViewId				= "5099083"
defaultViewPw				= "2GREO9yaQ0Yx"

# mobile
smallId						= "5009294"
smallPw						= "6eAqzKgtC6"
smallViewId					= "5009294"
smallViewPw					= "6eAqzKgtC6"

# video conference
videoParticipantDomain		= "vcube"
videoParticipantGuestDomain	= "vcubeguest"

soap_domain					= ""

regist_conference			= "%s/ives/ConferenceMngrListener.php?wsdl"
callback_conference			= "%s/ives/ConferenceListener.php?wsdl"
create_client				= "http://%s/B2BAPI/controllerService?wsdl"

conference_listener			= "%s/ives/ConferenceListener_wsdl.php"
conference_listener_port	= "%s/ives/ConferenceListener.php"
conference_listener_xsd		= "%s/ives/ConferenceListener.xsd"
conference_mngr_listener	= "%s/ives/ConferenceMngrListener_wsdl.php"
conference_mngr_listener_port = "%s/ives/ConferenceMngrListener.php"
conference_mngr_listener_xsd= "%s/ives/ConferenceMngrListener.xsd"

conference_name				= ""
mixerId						= "mixer@http://127.0.0.1:9090"
profileId					= "sip_profile"
polycomProfileId			= "sip_profile"
flashProfileId				= "sip_profile"
mobileProfileId				= "mobile_profile"
size						= 2

didLength					= 10
guestWsdl					= "http://%s/mcuGuestController/DidManagerWsService?wsdl"
guestPrefix					= "meeting"
guestDidLength				= 7

invitationGuestCallbackUrl	= "%s/ives/validate/validateInvitationGuestDid.php"
reservationGuestCallbackUrl	= "%s/ives/validate/validateReservationGuestDid.php"
sipWsdl         = "http://ws.visioassistance.net/v2/soapAccountManager.wsdl"

backgroundImage				= "/var/lib/mediaserver/mute.png"
telephoneClientImage		= "/var/lib/mediaserver/mute.png"
# telephoneClientImage      = "/var/lib/mediaserver/telephone.png"

callout_prefix				= "sip:"
# callout_h323_template		= "sip:%s-%s@%s:5090"
callout_h323_template		= "sip:%s-%s@(ip or domain):5090"
callout_name				= "__CALLEE__PARTICIPANT__"

document_sharing_upload_path	= "/upload/mcu/app"
document_sharing_no_image_path	= "shared/images/mcu/noimage.jpg"