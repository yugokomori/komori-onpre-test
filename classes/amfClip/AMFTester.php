<?php
require_once("classes/core/amf/FlashGateway.php");

class AMFTester{

    var $amf;

    function AMFTester(){
        $this->amf = new FlashGateway();
    }

    function isSetMethod( $methodName ){

        if( $this->amf->methodTable[$methodName] )
            return true;
        else
            return false;
    }

    function calling(){

        $methodName = func_get_arg(0);
        if( $this->isSetMethod($methodName) ){
            $args = func_get_args();
            array_shift($args);
            return call_user_func_array( array( &$this->amf,$methodName ),  $args );
        }else{
            return "Method -- ".$methodName." -- can not find";
        }
    }

    function getData($obj,$key){
        return $obj[$key];
    }
}
?>