#! /bin/sh

HOSTNAME="`hostname`"
LOGFILE="/var/log/${HOSTNAME}.log"
SCRIPT_NAME="`basename $0`"
DROP_FROM="10.246.26.60"
INSERT_LINE_NUM="1"
FLG1=${1}
RC=0

LOG()
{
    STATUS=$1
    MSG=$2
    echo [`date '+%Y/%m/%d %T'`] [${STATUS}] "${HOSTNAME}" "${SCRIPT_NAME}" "${MSG}" | tee -a ${LOGFILE}
}

case ${FLG1} in
    on)
        /sbin/iptables -L INPUT -n | grep DROP | grep icmp | grep ${DROP_FROM}
        if [ $? -ne 0 ]; then
            /sbin/iptables -I INPUT ${INSERT_LINE_NUM} -p icmp -s ${DROP_FROM} -j DROP
            if [ $? -ne 0 ]; then
                LOG "err" "\"iptables DROP-insert failed.\""
                RC=1
            fi
        else
            LOG "warn" "\"It has been already inserted.\""
            RC=1
        fi
        ;;
    off)
        /sbin/iptables -L INPUT -n | grep DROP | grep icmp | grep ${DROP_FROM}
        if [ $? -eq 0 ]; then
            /sbin/iptables -D INPUT -p icmp -s ${DROP_FROM} -j DROP
            if [ $? -ne 0 ]; then
                LOG "err" "\"iptables DROP-delete failed.\""
                RC=1
            fi
        else
            LOG "warn" "\"It does not exist.\""
            RC=1
        fi
        ;;
    *)
        echo $"Usage: $0 {on|off}"
        RC=2
        ;;
esac

exit ${RC}
