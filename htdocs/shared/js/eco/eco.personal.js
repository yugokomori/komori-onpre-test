jQuery.noConflict();
var j$ = jQuery;



var ecoPersonal = new Object();

function ecoPersonalInit()
{
    ecoPersonal = new EcoPersonalClass();
};

var EcoPersonalClass = Class.create();
EcoPersonalClass.prototype =
{
    eco: "",
    FormClass: new FormClass(),

    setOnSubmit: function()
    {
        var form = document.getElementById( "login_form" );

        var eco_report = document.getElementById( "eco-repo" );
//        var eco_email = document.getElementById( "eco-email" );
        var eco_email = document.getElementsByName("user_email")[0];
        if (form.user_name.value.length > 40) {
            alert( "名前は40文字以内で入力してください。" );
            return false;
        };
        if( eco_report.checked && ! eco_email.value ){
        	//var text = document.createTextNode('メールアドレスを入力してください。');
        	var text;
        	if (!(text = document.getElementById("attention"))) {
        		text = document.createElement('div');
        		text.setAttribute("id","attention");
        		text.setAttribute("class","attention");
        		eco_email.parentNode.insertBefore(text, eco_email.nextSibling);
        	}
        	text.innerHTML = '入力必須です。';
        	
        	return false;
        } else if( eco_report.checked && ! eco_email.value.toLowerCase().match(/^([.0-9a-z_+-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,})$/) ) {
        	//var text = document.createTextNode('メールアドレスが正しくありません。');
        	var text;
        	if (!(text = document.getElementById("attention"))) {
        		text = document.createElement('div');
        		text.setAttribute("id","attention");
        		text.setAttribute("class","attention");
        		eco_email.parentNode.insertBefore(text, eco_email.nextSibling);
        	}
        	text.innerHTML = 'メールの入力が不正です 。';
        	
        	return false;
        }

        if( searchFlg ){
            eco.search( true );
            return false;
        }
        this.FormClass.excute();
    },

    setFormEvent: function()
    {
        var form = document.getElementById( "login_form" );
        form.onsubmit= null;
        form.onsubmit = function(){ EcoPersonalClass.prototype.setOnSubmit();return false;};
    },

    displayEcorepoForm: function()
    {
         if (document.getElementById('eco-repo').checked == false) {
                j$("#ecorepoForm").hide();
//                j$("#ecorepoForm").slideUp("fast");
         } else {
                j$("#ecorepoForm").show();
//                j$("#ecorepoForm").slideDown("fast");
         }
    },
    


    initialize: function()
    {
        this.setFormEvent();
        var el = document.getElementById('ecorepoForm');
        if ( document.getElementById('eco-repo').checked == true) {
            el.style.visibility = 'visible';
            el.style.display    = 'block';
        }
    }
}