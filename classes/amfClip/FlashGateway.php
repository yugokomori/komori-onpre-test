<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("lib/pear/PEAR.php");
require_once("config/config.inc.php");
require_once("classes/amf/AmfService.class.php");
require_once("classes/mgm/MGM_Auth.class.php");
require_once("classes/N2MY_DBI.class.php");
require_once("classes/dbi/clip.dbi.php");
require_once("lib/EZLib/EZCore/EZConfig.class.php");
require_once("lib/EZLib/EZCore/EZLogger.class.php");
require_once("lib/EZLib/EZSession/EZSession.class.php");
require_once("lib/Smarty/Smarty.class.php");
require_once("classes/ClipFormat.class.php");

class FlashGateway extends AmfService
{
    var $logger2  = null;
    var $config   = null;
    var $dsn      = null;
    var $session;

    /**
     * コンストラクタ
     */
    function FlashGateway()
    {
        $this->config = parse_ini_file( N2MY_CONFIG_FILE, true);
        if (!defined("N2MY_MDB_DSN")) {
            define("N2MY_MDB_DSN", $this->config["GLOBAL"]["auth_dsn"]);
        }
        $this->logger2 = EZLogger2::getInstance($this->config["LOGGER"]);
        $this->session = EZSession::getInstance();
        $this->_addCalledMethod('getClipInfo', array('clip_key', 'client_id'));
    }

    /**
     * getClipInfo
     *
     * @param string  $clip_key
     * @param integer $client_id
     * @return array
     */
    function getClipInfo($clip_key, $client_id)
    {

        if (!$clip_key || !$client_id) {
            $this->logger2->error("clip_key or client_id lost");
            return false;
        }

        $video_config = $this->config["VIDEO"];
        $clip_info = $this->_getClipInfo( $clip_key );
        if ($clip_info === false){
            $this->logger2->error("clip not found");
            return false;
        }

        $ret["clipDuration"]      = $clip_info["duration"];
        $ret["authFmsUrl"]        = $video_config["auth_fms_url"];
        $ret["authFmsAppName"]    = $video_config["auth_fms_appli"];
        $ret["partnerKey"]        = $video_config["partner_id"];
        $ret["clientPlayLimit"]   = $video_config["client_play_limit"];
        $ret["partnerPlayLimit"]  = $video_config["partner_play_limit"];
        $ret["clientKey"]         = $video_config["client_id"];
        $ret["clipThumbnailUrl"]  = ClipFormat::thumbnailUrl($clip_key, $clip_info["storage_no"]);
        $ret["countUrl"]          = $video_config["virtualhost_url"];
        $ret["limitPlayUrl"]      = $video_config["virtualhost_url"];
        $ret["playUrl"]           = $clip_key;
        $ret["type"]              = "rtmp";
        $ret["serverUrl"]         = $video_config["stream_fms_url"];
        $ret["serverAppName"]     = ClipFormat::streamFmsInctanceName($clip_info["storage_no"]);

        if ($video_config['use_limelight'] && $this->existFlvAtLimeLight($clip_key, $clip_info["storage_no"])) {
            $ret["limelightPlayUrl"] = ClipFormat::limeLightPlayUrl($clip_key, $clip_info["storage_no"]);
            $ret["limelightUrl"]     = $video_config["limelight_url"];
            $ret["limelightAppName"] = $video_config["limelight_appli"];
        }

        $this->logger2->info( print_r( $ret, true ) );

        return $ret;
    }

    private function _getClipInfo( $clip_key )
    {

        $user_info = $this->session->get( "user_info" );
        $this->clip_obj = new ClipTable( $this->get_dsn() );

        $wheres[] = sprintf( "clip_key = '%s'", mysql_real_escape_string( $clip_key ) );
        $wheres[] = sprintf( "user_key = '%s'", mysql_real_escape_string( $user_info["user_key"] ) );
        $wheres[] = sprintf( "clip_status = '%s'", 2 );
        $wheres[] = sprintf( "is_deleted = '%s'", 0 );

        $rs = $this->clip_obj->getRow( implode( " AND ", $wheres ) );
        if (DB::isError($rs) || empty($rs) ) {
            return false;
        }

        return $rs;
    }

    private function get_dsn()
    {
        if ($this->_dsn) {
            return $this->_dsn;
        }
        if ( $serverdata = $this->session->get("server_info")) {
            return $this->_dsn = $serverdata["dsn"];
        } else {
            $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);
            return $this->_dsn = $server_list["SERVER_LIST"][N2MY_DEFAULT_DB];
        }
    }
    private function existFlvAtLimeLight($clip_key, $storage_no)
    {
        //check cdn clip
        $limelight_flv_url = ClipFormat::limeLightFlvUrl($clip_key, $storage_no);
        $this->logger2->info("limelight_flv_url =".$limelight_flv_url);
        $head = get_headers($limelight_flv_url, true);
        list ($p, $code) = explode(" ", $head[0]);

        return  $code == 200;
    }
}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
