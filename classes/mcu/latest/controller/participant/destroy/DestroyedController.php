<?php

require_once("classes/mcu/config/McuConfigProxy.php");

require_once(dirname(__FILE__)."/../../../model/ConferenceProxy.php");
require_once(dirname(__FILE__)."/../../../controller/participant/destroy/client/vcube/flash/DestroyedFlashParticipant.php");
require_once(dirname(__FILE__)."/../../../controller/participant/destroy/client/video/DestroyedConferenceParticipant.php");

require_once("lib/EZLib/EZCore/EZLogger2.class.php");

class DestroyedController {
	public static function create($model) {
		$logger = EZLogger2::getInstance();
		$proxy = ConferenceProxy::getInstance();
		$result = $proxy->getConferenceParticipantByConfIdPartId($model->confId, $model->partId);
		
		$logger->info($result);
		if (!$result) {
// 			throw new Exception("no record on v-meeting. confid: ".$model->confId.", partId: ".$model->partId);
		}
		else if ($result["participant_type"] == "SIP") {
			return new DestroyedConferenceParticipant($model);
		}
		else {
			return new DestroyedFlashParticipant($model);
		}
	}
}
