-- プラン変更
ALTER TABLE `service` ADD `max_seat` INT NOT NULL COMMENT '最大参加者数' AFTER `service_name`;
ALTER TABLE `service` ADD `max_audience_seat` INT NOT NULL COMMENT '最大オーディエンス数' AFTER `max_seat`;
ALTER TABLE `service` ADD `meeting_ssl` INT NOT NULL COMMENT '暗号化' AFTER `max_audience_seat`;
ALTER TABLE `service` ADD `desktop_share` INT NOT NULL COMMENT 'デスクトップ共有' AFTER `meeting_ssl`;
ALTER TABLE `service` ADD `hdd_extention` INT NOT NULL COMMENT '容量' AFTER `desktop_share`;
ALTER TABLE `service` ADD `high_quality` INT NOT NULL COMMENT '高画質' AFTER `hdd_extention`;
ALTER TABLE `service` ADD `mobile_phone` INT NOT NULL COMMENT '携帯' AFTER `high_quality`;
ALTER TABLE `service` ADD `h323_client` INT NOT NULL COMMENT 'H323' AFTER `mobile_phone`;
ALTER TABLE `service` ADD `whiteboard` INT NOT NULL COMMENT '資料共有' AFTER `h323_client`;

-- 資料共有モード
INSERT INTO `service_option` (`service_option_key` ,`country_key` ,`service_option_name` ,`service_option_price` ,`service_option_init_price` ,`service_option_running_price`) VALUES
('16', '1', 'whiteboard', '0', '0', '0');

INSERT INTO `participant_type` (`participant_type_key`, `participant_type_name` ,`create_datetime` ,`update_datetime`) VALUES
(6, 'whiteboard', '0000-00-00 00:00:00', NULL),
(7, 'whiteboard_audience', '0000-00-00 00:00:00', NULL);


-- 会議利用ユーザの情報
CREATE TABLE `user_agent` (
  `user_agent_key` int(11) NOT NULL AUTO_INCREMENT,
  `db_key` varchar(100) NOT NULL COMMENT 'DB名',
  `user_id` varchar(64) DEFAULT NULL COMMENT 'ユーザーID',
  `meeting_key` int(11) NOT NULL COMMENT '会議キー',
  `participant_key` int(11) DEFAULT NULL COMMENT '参加者ID',
  `appli_type` varchar(64) DEFAULT NULL COMMENT 'タイプ(normal, audience...)',
  `appli_mode` varchar(64) DEFAULT NULL COMMENT 'モード(general, h323, mobile...)',
  `appli_role` varchar(64) DEFAULT NULL COMMENT 'ロール(default, invite...)',
  `appli_version` varchar(50) DEFAULT NULL COMMENT 'as2/as3',
  `user_agent` text COMMENT '生UserAgent',
  `os` varchar(100) DEFAULT NULL COMMENT 'OS',
  `browser` varchar(100) DEFAULT NULL COMMENT 'ブラウザ',
  `flash` varchar(50) DEFAULT NULL COMMENT 'Flashバージョン',
  `protcol` varchar(20) DEFAULT NULL COMMENT 'FMS接続プロトコル',
  `port` varchar(20) DEFAULT NULL COMMENT 'FMS接続ポート',
  `ip` varchar(100) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `monitor_size` varchar(100) DEFAULT NULL COMMENT 'モニターサイズ',
  `monitor_color` varchar(20) DEFAULT NULL COMMENT 'ビット',
  `camera` text COMMENT 'カメラデバイス名',
  `mic` text COMMENT 'マイクデバイス名',
  `speed` varchar(20) DEFAULT NULL COMMENT '回線速度(bps)',
  `create_datetime` datetime DEFAULT NULL COMMENT '作成日',
  `update_datetime` datetime DEFAULT NULL COMMENT '更新日',
  PRIMARY KEY (`user_agent_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- FMSサーバ一覧
ALTER TABLE `fms_server` CHANGE `server_address` `server_address` VARCHAR( 255 ) NOT NULL; -- FQDNは255文字が制約
ALTER TABLE `fms_server` ADD `local_address` VARCHAR( 255 ) NULL COMMENT 'ローカルアドレス' AFTER `server_address`; -- ローカル用のアドレスを追加
