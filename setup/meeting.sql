-- phpMyAdmin SQL Dump
-- version 3.0.1.1
-- http://www.phpmyadmin.net
--
-- 生成時間: 2013 年 11 月 21 日 20:43
-- サーバのバージョン: 5.5.23
-- PHP のバージョン: 5.2.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
--

-- --------------------------------------------------------

--
-- テーブルの構造 `address_book`
--

CREATE TABLE IF NOT EXISTS `address_book` (
  `address_book_key` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'KEY',
  `user_key` int(11) NOT NULL COMMENT 'ユーザKEY',
  `member_key` int(11) DEFAULT NULL COMMENT 'メンバーKEY',
  `name` varchar(200) NOT NULL COMMENT '名前',
  `name_kana` varchar(200) DEFAULT NULL COMMENT 'フリガナ',
  `email` varchar(255) NOT NULL COMMENT 'メールアドレス',
  `lang` varchar(10) NOT NULL COMMENT '言語',
  `timezone` varchar(16) NOT NULL COMMENT 'タイムゾーン',
  `memo` text COMMENT '備考',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `create_datetime` datetime NOT NULL COMMENT '作成日時',
  `update_datetime` datetime NOT NULL COMMENT '更新日時',
  `addition` text COMMENT '拡張項目',
  PRIMARY KEY (`address_book_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4273 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `agency_relation_member`
--

CREATE TABLE IF NOT EXISTS `agency_relation_member` (
  `agency_relation_member_id` int(11) NOT NULL AUTO_INCREMENT,
  `agency_id` int(11) NOT NULL,
  `member_key` int(11) NOT NULL,
  PRIMARY KEY (`agency_relation_member_id`),
  KEY `agency_id` (`agency_id`),
  KEY `member_key` (`member_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `agency_relation_user`
--

CREATE TABLE IF NOT EXISTS `agency_relation_user` (
  `agency_relation_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `agency_id` int(11) NOT NULL,
  `user_key` varchar(45) NOT NULL,
  PRIMARY KEY (`agency_relation_user_id`),
  KEY `agency_id` (`agency_id`),
  KEY `user_key` (`user_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `answer`
--

CREATE TABLE IF NOT EXISTS `answer` (
  `answer_id` int(11) NOT NULL AUTO_INCREMENT,
  `answersheet_id` varchar(11) NOT NULL,
  `question_id` varchar(11) NOT NULL,
  `comment` text,
  `created_datetime` datetime NOT NULL,
  PRIMARY KEY (`answer_id`),
  KEY `answersheet_id` (`answersheet_id`,`question_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `answersheet`
--

CREATE TABLE IF NOT EXISTS `answersheet` (
  `answersheet_id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaire_id` int(11) NOT NULL,
  `participant_key` int(11) NOT NULL,
  `participant_name` varchar(64) DEFAULT NULL,
  `created_datetime` datetime NOT NULL,
  PRIMARY KEY (`answersheet_id`),
  KEY `questionnaire_id` (`questionnaire_id`,`participant_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `answer_branch`
--

CREATE TABLE IF NOT EXISTS `answer_branch` (
  `answer_branch_id` int(11) NOT NULL AUTO_INCREMENT,
  `answer_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `question_branch_id` int(11) NOT NULL,
  `created_datetime` datetime NOT NULL,
  PRIMARY KEY (`answer_branch_id`),
  KEY `answer_id` (`answer_id`,`question_id`,`question_branch_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `clip`
--

CREATE TABLE IF NOT EXISTS `clip` (
  `clip_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'プライマリキー',
  `clip_key` varchar(64) NOT NULL COMMENT 'クリップキー',
  `user_key` int(11) NOT NULL COMMENT 'ユーザーキー',
  `title` varchar(128) NOT NULL COMMENT 'タイトル',
  `description` text NOT NULL COMMENT '説明',
  `upload_filesize` bigint(20) unsigned NOT NULL COMMENT '元ファイルサイズ',
  `flv_filesize` bigint(20) unsigned DEFAULT NULL COMMENT 'flvのファイルサイズ',
  `storage_no` tinyint(3) unsigned NOT NULL COMMENT 'ストレージNO',
  `clip_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '変換状況',
  `duration` float(12,2) NOT NULL COMMENT '動画再生時間',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `createtime` datetime NOT NULL COMMENT '作成時間',
  `updatetime` datetime NOT NULL COMMENT '更新時間',
  PRIMARY KEY (`clip_id`),
  KEY `clip_key` (`clip_key`),
  KEY `user_key` (`user_key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `conference_record`
--

CREATE TABLE IF NOT EXISTS `conference_record` (
  `conference_record_key` int(11) NOT NULL AUTO_INCREMENT,
  `methodName` varchar(30) NOT NULL,
  `confId` varchar(50) DEFAULT NULL,
  `partId` int(5) DEFAULT NULL,
  `partName` varchar(255) DEFAULT NULL,
  `partType` varchar(30) DEFAULT NULL,
  `state` varchar(30) DEFAULT NULL,
  `info` text,
  `createdate` varchar(30) NOT NULL,
  PRIMARY KEY (`conference_record_key`),
  KEY `confId` (`confId`,`partId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `customer_message`
--

CREATE TABLE IF NOT EXISTS `customer_message` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `user_key` int(11) NOT NULL DEFAULT '0',
  `room_key` varchar(64) NOT NULL DEFAULT '',
  `customer_name` text NOT NULL,
  `customer_company` text NOT NULL,
  `customer_tel` text NOT NULL,
  `customer_email` text NOT NULL,
  `customer_ip` text NOT NULL,
  `customer_comment` text NOT NULL,
  `addition` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `send_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `no` (`no`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `datacenter_ignore`
--

CREATE TABLE IF NOT EXISTS `datacenter_ignore` (
  `datacenter_ignore_key` int(11) NOT NULL AUTO_INCREMENT,
  `user_key` int(11) NOT NULL,
  `datacenter_key` int(11) NOT NULL,
  `create_datetime` datetime NOT NULL,
  PRIMARY KEY (`datacenter_ignore_key`),
  KEY `user_key` (`user_key`,`datacenter_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `datacenter_priority`
--

CREATE TABLE IF NOT EXISTS `datacenter_priority` (
  `datacenter_priority_key` int(11) NOT NULL AUTO_INCREMENT,
  `user_key` int(11) NOT NULL,
  `country` varchar(16) NOT NULL,
  `datacenter_key` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `create_datetime` datetime NOT NULL,
  PRIMARY KEY (`datacenter_priority_key`),
  KEY `user_key` (`user_key`,`datacenter_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `document`
--

CREATE TABLE IF NOT EXISTS `document` (
  `document_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `meeting_key` int(10) unsigned NOT NULL DEFAULT '0',
  `document_id` varchar(255) NOT NULL DEFAULT '',
  `participant_id` int(10) DEFAULT NULL COMMENT '資料をアップロードしたparticipant_key',
  `document_mode` varchar(10) DEFAULT NULL COMMENT '資料モード（"meeting":会議内,"pre":事前）',
  `document_path` varchar(255) NOT NULL COMMENT 'パス',
  `document_name` varchar(255) NOT NULL COMMENT '資料名',
  `document_format` varchar(64) DEFAULT NULL COMMENT '変換後のフォーマット',
  `document_version` varchar(64) DEFAULT NULL COMMENT 'アニメーションのバージョン',
  `document_size` bigint(20) NOT NULL DEFAULT '0' COMMENT '変換後の容量',
  `document_category` text,
  `document_extension` text,
  `document_status` int(11) NOT NULL DEFAULT '0',
  `document_index` int(10) unsigned DEFAULT '1',
  `document_sort` int(11) NOT NULL DEFAULT '0',
  `document_transmitted` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: 未送信, 2: 送信',
  `is_storage` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'ストレージファイルかどうかのフラグ',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`document_key`),
  KEY `document_id` (`document_id`),
  KEY `meeting_key` (`meeting_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `echo_canceler_log`
--

CREATE TABLE IF NOT EXISTS `echo_canceler_log` (
  `meeting_key` int(10) NOT NULL,
  `participant_id` text NOT NULL,
  `machine_id` varchar(255) NOT NULL,
  `start_datetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `eventlog`
--

CREATE TABLE IF NOT EXISTS `eventlog` (
  `eventlog_key` int(11) NOT NULL AUTO_INCREMENT COMMENT '連番',
  `meeting_key` int(11) NOT NULL DEFAULT '0' COMMENT 'ミーティングキー',
  `participant_session_id` varchar(255) DEFAULT '0' COMMENT 'ID',
  `participant_id` int(11) DEFAULT NULL,
  `participant_name` varchar(255) DEFAULT NULL COMMENT '名前',
  `participant_type` varchar(64) DEFAULT NULL COMMENT 'タイプ',
  `room_key` varchar(255) DEFAULT NULL,
  `event_id` varchar(255) NOT NULL COMMENT 'イベント',
  `data` text,
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '作成日',
  `elapsed_time` bigint(20) NOT NULL COMMENT '経過時間(ミリ秒)',
  PRIMARY KEY (`eventlog_key`),
  KEY `participant_session_id` (`participant_session_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `file_cabinet`
--

CREATE TABLE IF NOT EXISTS `file_cabinet` (
  `cabinet_id` int(11) NOT NULL AUTO_INCREMENT,
  `meeting_key` int(10) NOT NULL,
  `tmp_name` text NOT NULL,
  `type` text NOT NULL,
  `size` int(11) NOT NULL,
  `file_name` text NOT NULL,
  `file_cabinet_path` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `registtime` datetime NOT NULL,
  `updatetime` datetime DEFAULT NULL,
  PRIMARY KEY (`cabinet_id`),
  KEY `meeting_key` (`meeting_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `flash_version`
--

CREATE TABLE IF NOT EXISTS `flash_version` (
  `user_id` varchar(50) NOT NULL,
  `flash_version` varchar(20) NOT NULL,
  `create_datetime` datetime NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `flash_version` (`flash_version`),
  KEY `create_datetime` (`create_datetime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `fms_deny`
--

CREATE TABLE IF NOT EXISTS `fms_deny` (
  `fms_deny_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'KEY',
  `user_key` int(11) NOT NULL,
  `fms_server_key` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: 有効, 0: 無効',
  `create_datetime` datetime NOT NULL COMMENT '作成日時',
  `update_datetime` datetime DEFAULT NULL COMMENT '更新日時',
  PRIMARY KEY (`fms_deny_key`),
  KEY `user_key` (`user_key`),
  KEY `fms_server_key` (`fms_server_key`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `fms_protocol`
--

CREATE TABLE IF NOT EXISTS `fms_protocol` (
  `meeting_key` int(11) NOT NULL COMMENT '会議キー',
  `remote_addr` varchar(200) NOT NULL COMMENT '接続元IP',
  `fms_host` varchar(200) NOT NULL COMMENT 'FMSホスト',
  `fms_protocol` varchar(20) NOT NULL COMMENT 'FMSプロトコル',
  `fms_port` varchar(20) NOT NULL COMMENT 'FMSポート',
  `connect_time` double DEFAULT '0' COMMENT '接続時間',
  `create_datetime` datetime NOT NULL COMMENT '作成日時'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- テーブルの構造 `fms_watch_status`
--

CREATE TABLE IF NOT EXISTS `fms_watch_status` (
  `watch_status_key` int(11) NOT NULL AUTO_INCREMENT,
  `meeting_key` int(10) NOT NULL,
  `meeting_sequence_key` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: wait, 1: 実行中, 2: 成功, 9: 失敗',
  `registtime` datetime NOT NULL,
  `starttime` datetime NOT NULL,
  `endtime` datetime NOT NULL,
  PRIMARY KEY (`watch_status_key`),
  KEY `meeting_key` (`meeting_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `inbound`
--

CREATE TABLE IF NOT EXISTS `inbound` (
  `inbound_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'インバウンドキー',
  `inbound_id` varchar(255) NOT NULL COMMENT 'インバウンド用のID',
  `inbound_name` varchar(255) DEFAULT NULL COMMENT '名前',
  `user_key` int(11) NOT NULL COMMENT 'ユーザーキー',
  `reenter_time` int(11) NOT NULL DEFAULT '0' COMMENT '再入室時間（秒）',
  `design_type` varchar(32) NOT NULL DEFAULT 'normal' COMMENT 'normal：通常デザイン、original：オリジナル、custom：カスタマイズ',
  `design_key` int(11) NOT NULL DEFAULT '1' COMMENT 'ボタンのタイプ 0:オリジナル使用、100:カスタマイズ',
  `user_option_key` int(11) NOT NULL DEFAULT '0' COMMENT 'ユーザーオプションキー',
  `customer_info_flg` int(11) NOT NULL DEFAULT '0' COMMENT '0:表示、1:非表示、2:非表示、名前固定',
  `customer_name` varchar(64) DEFAULT NULL COMMENT '固定する場合のお客様名',
  `inbound_sort` int(11) NOT NULL DEFAULT '1' COMMENT 'ソート',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT 'ステータス',
  `addition` text COMMENT 'その他',
  `create_datetime` datetime DEFAULT NULL,
  `update_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`inbound_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `inbound_log`
--

CREATE TABLE IF NOT EXISTS `inbound_log` (
  `inbound_log_key` int(11) NOT NULL AUTO_INCREMENT,
  `user_key` int(11) NOT NULL,
  `inbound_id` varchar(255) NOT NULL COMMENT 'インバウンドID',
  `room_key` varchar(255) NOT NULL,
  `meeting_key` int(11) DEFAULT NULL,
  `participant_key` int(11) DEFAULT NULL,
  `participant_session_id` varchar(255) DEFAULT NULL,
  `http_referer` text,
  `remote_addr` text,
  `http_user_agent` text,
  `event` varchar(64) NOT NULL,
  `create_datetime` datetime NOT NULL,
  PRIMARY KEY (`inbound_log_key`),
  KEY `user_key` (`user_key`),
  KEY `room_key` (`room_key`),
  KEY `meeting_key` (`meeting_key`),
  KEY `user_key_2` (`user_key`),
  KEY `room_key_2` (`room_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `ives_setting`
--

CREATE TABLE IF NOT EXISTS `ives_setting` (
  `ives_setting_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'AUTO_INCREMENT',
  `mcu_server_key` int(10) NOT NULL,
  `user_key` int(11) NOT NULL,
  `room_key` varchar(64) NOT NULL,
  `ives_did` varchar(20) NOT NULL,
  `sip_uid` int(11) NOT NULL COMMENT 'SIP用uid',
  `client_id` varchar(64) NOT NULL,
  `client_pw` varchar(64) NOT NULL,
  `profile_id` int(10) NOT NULL COMMENT '課金タイプ, 社内テスト用: 281, デモ用: 321, 課金用: 323',
  `num_profiles` int(10) NOT NULL DEFAULT '0' COMMENT '課金タイプの申し込み数',
  `use_active_speaker` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'アクティブスピーカー使用/不使用フラグ',
  `ignore_video_conference_address` tinyint(1) NOT NULL DEFAULT '0',
  `startdate` datetime DEFAULT NULL,
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `registtime` datetime NOT NULL,
  `updatetime` datetime DEFAULT NULL,
  PRIMARY KEY (`ives_setting_key`),
  KEY `user_key` (`user_key`),
  KEY `mcu_server_key` (`mcu_server_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `mail_doc_conv`
--

CREATE TABLE IF NOT EXISTS `mail_doc_conv` (
  `uniq_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ユニークキー',
  `user_key` int(11) NOT NULL COMMENT 'ユーザテーブル：ユーザキー',
  `name` varchar(255) NOT NULL COMMENT '名前',
  `mail_address` varchar(255) NOT NULL COMMENT 'メールアドレス',
  `is_active` tinyint(1) NOT NULL COMMENT '0：無効、1：有効',
  `create_datetime` datetime NOT NULL COMMENT '作成日',
  `update_datetime` datetime NOT NULL COMMENT '更新日',
  `addition` text COMMENT '検索しない拡張項目',
  PRIMARY KEY (`uniq_key`),
  KEY `user_key` (`user_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='メールからのドキュメント変換許可リスト' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `max_connect_log`
--

CREATE TABLE IF NOT EXISTS `max_connect_log` (
  `max_connect_log_key` int(11) NOT NULL AUTO_INCREMENT,
  `user_key` int(11) NOT NULL,
  `meeting_key` int(10) DEFAULT NULL,
  `room_key` varchar(255) DEFAULT NULL,
  `type` varchar(64) DEFAULT NULL,
  `connect_count` int(11) NOT NULL,
  `create_datetime` datetime NOT NULL,
  PRIMARY KEY (`max_connect_log_key`),
  KEY `user_key` (`user_key`,`meeting_key`,`room_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `meeting`
--

CREATE TABLE IF NOT EXISTS `meeting` (
  `meeting_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fms_path` text COMMENT 'インスタンスのパス',
  `layout_key` int(10) unsigned NOT NULL DEFAULT '0',
  `server_key` int(10) unsigned NOT NULL DEFAULT '0',
  `sharing_server_key` int(10) unsigned NOT NULL DEFAULT '0',
  `user_key` int(11) NOT NULL COMMENT 'ユーザKEY',
  `room_key` varchar(255) NOT NULL DEFAULT '',
  `meeting_ticket` varchar(255) NOT NULL DEFAULT '',
  `meeting_session_id` varchar(255) NOT NULL DEFAULT '',
  `pin_cd` varchar(8) DEFAULT NULL COMMENT 'PINコード',
  `meeting_country` varchar(32) NOT NULL DEFAULT 'ja',
  `meeting_room_name` varchar(255) DEFAULT NULL,
  `meeting_name` varchar(255) DEFAULT NULL,
  `meeting_tag` varchar(255) DEFAULT NULL,
  `meeting_max_audience` int(10) unsigned DEFAULT '20',
  `meeting_max_whiteboard` int(11) NOT NULL DEFAULT '0' COMMENT 'ホワイトボードユーザー',
  `meeting_max_extendable` int(11) DEFAULT '2160',
  `meeting_max_seat` int(10) unsigned DEFAULT '10',
  `meeting_size_recordable` bigint(20) unsigned DEFAULT '0',
  `meeting_size_uploadable` bigint(20) unsigned DEFAULT '0',
  `meeting_size_used` bigint(20) unsigned DEFAULT '0',
  `meeting_log_owner` varchar(255) DEFAULT '',
  `meeting_log_password` varchar(255) NOT NULL DEFAULT '',
  `meeting_start_datetime` datetime DEFAULT NULL,
  `meeting_stop_datetime` datetime DEFAULT NULL,
  `meeting_extended_counter` int(10) unsigned DEFAULT '0',
  `has_recorded_minutes` tinyint(1) DEFAULT '0',
  `has_recorded_video` tinyint(1) DEFAULT '0',
  `has_shared_memo` tinyint(1) DEFAULT '0' COMMENT '共有メモ保持フラグ',
  `has_quick_vote` tinyint(1) NOT NULL DEFAULT '0' COMMENT '投票保持フラグ',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_extended` tinyint(1) NOT NULL DEFAULT '0',
  `is_locked` tinyint(1) NOT NULL DEFAULT '0',
  `is_blocked` tinyint(1) NOT NULL DEFAULT '0' COMMENT '入室制限フラグ',
  `is_reserved` tinyint(1) NOT NULL DEFAULT '0',
  `intra_fms` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: 共用fms, 1: intrafms',
  `is_publish` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: 非公開、1: 公開中',
  `version` text,
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT NULL,
  `room_addition` text,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `use_flg` varchar(1) DEFAULT '0' COMMENT '会議利用フラグ',
  `actual_start_datetime` datetime DEFAULT NULL COMMENT '実際の利用開始時間',
  `actual_end_datetime` datetime DEFAULT NULL COMMENT '実際の会議終了時間',
  `meeting_use_minute` int(11) DEFAULT '0' COMMENT '利用時間（分）',
  `member_keys` text COMMENT 'メンバーキー一覧',
  `participant_names` text COMMENT '参加者名一覧',
  `eco_move` double NOT NULL DEFAULT '0' COMMENT '距離(Km)',
  `eco_time` bigint(20) NOT NULL DEFAULT '0' COMMENT '時間(分)',
  `eco_fare` bigint(20) NOT NULL DEFAULT '0' COMMENT '料金(円)',
  `eco_co2` double NOT NULL DEFAULT '0' COMMENT 'CO2(kg)',
  `eco_info` text COMMENT 'エコ情報',
  `need_fms_version` varchar(20) NOT NULL COMMENT '必要FMSバージョン',
  `pgi_setting_key` varchar(20) NOT NULL COMMENT 'pgi_settingテーブルのプライマリキー',
  `pgi_conference_id` int(10) NOT NULL COMMENT 'PGI会議ID',
  `pgi_m_pass_code` varchar(15) NOT NULL COMMENT '主催者用',
  `pgi_p_pass_code` varchar(15) NOT NULL COMMENT '参加者用',
  `pgi_l_pass_code` varchar(15) NOT NULL COMMENT 'オーディエンス用',
  `pgi_phone_numbers` text NOT NULL COMMENT '複数保持可能（シリアライズ化）',
  `pgi_api_status` varchar(15) NOT NULL COMMENT 'PGIのAPIアクセス管理',
  `pgi_service_name` varchar(64) DEFAULT NULL COMMENT 'PGi利用サービス名',
  `pgi_conference_status` tinyint(4) NOT NULL COMMENT 'PGi GM利用ステータス 0:利用なし、1:利用、2:バッチ削除完了',
  `tc_type` varchar(20) NOT NULL COMMENT '音声通話タイプ  voip / teleconf / etc',
  `pgi_sip_address` varchar(255) DEFAULT NULL COMMENT 'PGi SIP Address',
  `tc_teleconf_note` text NOT NULL COMMENT 'etc 時（自由入力）',
  `use_pgi_dialin` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'pgiダイヤルイン使用有無',
  `use_pgi_dialin_free` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'pgiダイヤルイン使用有無(フリーダイヤル)',
  `use_pgi_dialout` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'pgiダイヤルアウト(携帯)使用有無',
  `use_sales_option` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'セールスオプション利用フラグ',
  `prohibit_extend_meeting_flg` tinyint(1) NOT NULL DEFAULT '0' COMMENT '会議延長禁止フラグ',
  `inbound_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'インバウンド利用フラグ',
  `inbound_id` varchar(255) DEFAULT NULL COMMENT 'インバウンドID',
  `temporary_did` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`meeting_key`),
  KEY `meeting_ticket` (`meeting_ticket`),
  KEY `meeting_session_id` (`meeting_session_id`),
  KEY `meeting_tag` (`meeting_tag`),
  KEY `room_key` (`room_key`),
  KEY `meeting_start_datetime` (`meeting_start_datetime`),
  KEY `meeting_stop_datetime` (`meeting_stop_datetime`),
  KEY `use_flg` (`use_flg`),
  KEY `meeting_key` (`meeting_key`),
  KEY `user_key` (`user_key`),
  KEY `pin_cd` (`pin_cd`),
  KEY `server_key` (`server_key`),
  KEY `temporary_did` (`temporary_did`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `meeting_function_count_log`
--

CREATE TABLE `meeting_function_count_log` (
`meeting_function_count_key` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '機能カウントKEY',
`meeting_key` int(11) NOT NULL COMMENT  'ミーティングKEY',
`meeting_sequence_key` int(11) NOT NULL COMMENT  'ミーティングシーケンスKEY',
`create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
`meeting_settings_button` int(4) DEFAULT '0' COMMENT  '会議設定',
`quick_vote_button` int(4) DEFAULT '0' COMMENT  'クイックアンケート',
`shared_note_button` int(4) DEFAULT '0' COMMENT  '共有メモ',
`share_video_button` int(4) DEFAULT '0' COMMENT  '動画共有',
`invite_button` int(4) DEFAULT '0' COMMENT  '会議内招待',
`users_list_button` int(4) DEFAULT '0' COMMENT  '参加者一覧',
`h323_button` int(4) DEFAULT '0' COMMENT  'H.323',
`screen_sharing_button` int(4) DEFAULT '0' COMMENT  '画面共有',
`teleconference_button` int(4) DEFAULT '0' COMMENT  '電話会議',
`record_button` int(4) DEFAULT '0' COMMENT  '録画',
`exit_button` int(4) DEFAULT '0' COMMENT  '退室',
`file_sharing_button` int(4) DEFAULT '0' COMMENT  'ファイル転送',
`send_chat_message_button` int(4) DEFAULT '0' COMMENT  'チャット入力',
`telephony_button` int(4) DEFAULT '0' COMMENT  '新電話会議',
`user_preferences_button` int(4) DEFAULT '0' COMMENT  '環境設定',
`choose_camera_button` int(4) DEFAULT '0' COMMENT  'カメラ変更',
`choose_microphone_button` int(4) DEFAULT '0' COMMENT  'マイク変更',
`camera_mute_buttton` int(4) DEFAULT '0' COMMENT  'カメラミュート',
`microphone_mute_button` int(4) DEFAULT '0' COMMENT  'マイクミュート',
`away_button` int(4) DEFAULT '0' COMMENT  '退席',
`audio_only_checkbox` int(4) DEFAULT '0' COMMENT  '音声優先',
`volume_slider` int(4) DEFAULT '0' COMMENT  '音量',
`reset_stream_button` int(4) DEFAULT '0' COMMENT  '遅延解消',
`change_layout_button` int(4) DEFAULT '0' COMMENT  'レイアウト変更',
`my_video_button` int(4) DEFAULT '0' COMMENT  '自映像',
`select_color_button` int(4) DEFAULT '0' COMMENT  'カラー変更',
`select_weight_button` int(4) DEFAULT '0' COMMENT  'サイズ変更',
`select_pen_tool_button` int(4) DEFAULT '0' COMMENT  'ペンツール',
`select_text_tool_button` int(4) DEFAULT '0' COMMENT  'テキストツール',
`select_shape_tool_button` int(4) DEFAULT '0' COMMENT  'シェイプツール',
`select_eraser_tool_button` int(4) DEFAULT '0' COMMENT  '消しゴムツール',
`select_finger_tool_button` int(4) DEFAULT '0' COMMENT  'フィンガーツール',
`select_hand_tool_button` int(4) DEFAULT '0' COMMENT  'ハンドツール',
`undo_tool_button` int(4) DEFAULT '0' COMMENT  '取り消し',
`remove_selected_page_button` int(4) DEFAULT '0' COMMENT  '削除',
`personal_wb_button` int(4) DEFAULT '0' COMMENT  '個人ホワイトボード',
`scale_slider` int(4) DEFAULT '0' COMMENT  'ズーム',
`add_new_page_button` int(4) DEFAULT '0' COMMENT  '新規',
`upload_file_button` int(4) DEFAULT '0' COMMENT  '資料貼り込み',
`delete_selected_page_button` int(4) DEFAULT '0' COMMENT  '削除',
`switch_sync_button` int(4) DEFAULT '0' COMMENT  '同期',
`select_page_button` int(4) DEFAULT '0' COMMENT  'ページ一覧',
`delete_pages_button` int(4) DEFAULT '0' COMMENT  '一括削除',
`move_top_page_button` int(4) DEFAULT '0' COMMENT  '先頭ページ',
`move_prev_page_button` int(4) DEFAULT '0' COMMENT  '前ページ',
`move_next_page_button` int(4) DEFAULT '0' COMMENT  '次ページ',
`move_end_page_button` int(4) DEFAULT '0' COMMENT  '末尾ページ',
`save_pdf_button` int(4) DEFAULT '0' COMMENT  'PDF保存',
`print_page_button` int(4) DEFAULT '0' COMMENT  '印刷',
`mcu_document_share_button` int(4) DEFAULT '0' COMMENT  'MCU共有',
PRIMARY KEY (`meeting_function_count_key`),
KEY `meeting_key` (`meeting_key`),
KEY `meeting_sequence_key` (`meeting_sequence_key`)
)ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `meeting_clip`
--

CREATE TABLE IF NOT EXISTS `meeting_clip` (
  `meeting_clip_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'プライマリキー',
  `meeting_key` int(11) NOT NULL COMMENT 'ミーティングキー',
  `clip_key` varchar(64) NOT NULL COMMENT 'クリップキー',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `is_loaded` tinyint(1) NOT NULL DEFAULT '0' COMMENT '会議での利用済みフラグ',
  `room_key` varchar(64) DEFAULT NULL COMMENT '会議室キー(主にセールスで使用する)',
  `storage_file_key` int(11) DEFAULT NULL COMMENT 'ストレージファイルキー',
  `meeting_clip_name` varchar(255) DEFAULT NULL COMMENT 'ミーティング中でのファイル名',
  `createtime` datetime DEFAULT NULL COMMENT '作成日時',
  `updatetime` datetime DEFAULT NULL COMMENT '更新日時',
  PRIMARY KEY (`meeting_clip_key`),
  KEY `meeting_key` (`meeting_key`),
  KEY `clip_key` (`clip_key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `meeting_extension_use_log`
--

CREATE TABLE IF NOT EXISTS `meeting_extension_use_log` (
  `log_no` int(11) NOT NULL AUTO_INCREMENT,
  `meeting_key` int(11) NOT NULL DEFAULT '0',
  `room_key` varchar(255) NOT NULL DEFAULT '',
  `meeting_tag` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(255) NOT NULL DEFAULT '',
  `participant_id` varchar(255) NOT NULL DEFAULT '',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`log_no`),
  KEY `meeting_key` (`meeting_key`),
  KEY `room_key` (`room_key`),
  KEY `tag` (`meeting_tag`),
  KEY `create_datetime` (`create_datetime`),
  KEY `type` (`type`),
  KEY `connect_id` (`participant_id`),
  KEY `connect_type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `meeting_invitation`
--

CREATE TABLE IF NOT EXISTS `meeting_invitation` (
  `invitation_key` int(10) NOT NULL AUTO_INCREMENT,
  `meeting_key` int(10) NOT NULL COMMENT '会議キー',
  `meeting_session_id` varchar(255) NOT NULL,
  `meeting_ticket` varchar(255) NOT NULL,
  `user_session_id` varchar(255) DEFAULT NULL COMMENT '通常ユーザー用ハッシュ値',
  `audience_session_id` varchar(255) DEFAULT NULL COMMENT 'オーディエンスユーザー用ハッシュ値',
  `contact_name` varchar(64) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: 無効, 1: 有効, 9: 終了',
  `entrytime` datetime NOT NULL,
  `updatetime` datetime DEFAULT NULL,
  PRIMARY KEY (`invitation_key`),
  KEY `meeting_key` (`meeting_key`),
  KEY `meeting_session_id` (`meeting_session_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `meeting_member_use_log`
--

CREATE TABLE IF NOT EXISTS `meeting_member_use_log` (
  `log_no` int(11) NOT NULL AUTO_INCREMENT,
  `meeting_key` int(11) NOT NULL DEFAULT '0',
  `user_key` int(11) NOT NULL,
  `member_key` int(11) NOT NULL,
  `participant_key` int(11) DEFAULT NULL,
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`log_no`),
  KEY `meeting_key` (`meeting_key`),
  KEY `create_datetime` (`create_datetime`),
  KEY `user_key` (`user_key`),
  KEY `member_key` (`member_key`),
  KEY `participant_key` (`participant_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `meeting_options`
--

CREATE TABLE IF NOT EXISTS `meeting_options` (
  `meeting_options_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `meeting_key` int(10) unsigned NOT NULL DEFAULT '0',
  `option_key` int(10) unsigned NOT NULL DEFAULT '0',
  `meeting_option_quantity` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`meeting_options_key`),
  KEY `meeting_key_option_key` (`meeting_key`,`option_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `meeting_sequence`
--

CREATE TABLE IF NOT EXISTS `meeting_sequence` (
  `meeting_sequence_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `server_key` int(10) unsigned NOT NULL DEFAULT '0',
  `meeting_key` int(10) unsigned NOT NULL DEFAULT '0',
  `meeting_size_used` bigint(20) NOT NULL DEFAULT '0',
  `has_recorded_minutes` tinyint(1) NOT NULL DEFAULT '0',
  `has_recorded_video` tinyint(1) NOT NULL DEFAULT '0',
  `has_shared_memo` tinyint(1) NOT NULL DEFAULT '0' COMMENT '共有メモ保持フラグ',
  `has_quick_vote` tinyint(1) NOT NULL DEFAULT '0' COMMENT '投票保持フラグ',
  `start_datetime` datetime DEFAULT NULL COMMENT '開始日時',
  `end_datetime` datetime DEFAULT NULL COMMENT '更新日時',
  `record_second` double NOT NULL COMMENT '録画時間',
  `record_id` varchar(50) DEFAULT NULL COMMENT '動画生成発行ID',
  `record_endtime` datetime DEFAULT NULL COMMENT '動画生成完了時間',
  `record_status` varchar(20) DEFAULT NULL COMMENT '動画生成状態',
  `record_result` varchar(20) DEFAULT NULL COMMENT '動画生成結果',
  `record_end_proc` varchar(50) DEFAULT NULL COMMENT '完了後の処理',
  `record_filepath` text COMMENT '動画生成後のパス',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT NULL,
  `last_play_datetime` datetime DEFAULT NULL COMMENT '最終再生日時',
  PRIMARY KEY (`meeting_sequence_key`),
  KEY `meeting_key` (`meeting_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `meeting_suggest_log`
--

CREATE TABLE IF NOT EXISTS `meeting_suggest_log` (
  `suggest_key` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'サジェストKEY',
  `meeting_key` int(11) NOT NULL COMMENT 'ミーティングKEY',
  `meeting_sequence_key` int(11) NOT NULL COMMENT 'ミーティングシーケンスKEY',
  `user_key` int(11) NOT NULL COMMENT 'ユーザーキー',
  `participant_key` int(10) NOT NULL COMMENT '参加者KEY',
  `user_name` varchar(255) NOT NULL COMMENT '参加者名',
  `level` varchar(11) DEFAULT NULL COMMENT 'レベル',
  `status_type` varchar(255) NOT NULL COMMENT 'サジェスト種類',
  `suggest_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'DBに入れる時間',
  PRIMARY KEY (`suggest_key`),
  KEY `meeting_key` (`meeting_key`),
  KEY `meeting_sequence_key` (`meeting_sequence_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `meeting_uptime`
--

CREATE TABLE IF NOT EXISTS `meeting_uptime` (
  `meeting_uptime_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `meeting_key` int(10) unsigned NOT NULL DEFAULT '0',
  `meeting_uptime_start` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `meeting_uptime_stop` datetime DEFAULT NULL,
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`meeting_uptime_key`),
  KEY `meeting_key` (`meeting_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `meeting_use_log`
--

CREATE TABLE IF NOT EXISTS `meeting_use_log` (
  `log_no` int(11) NOT NULL AUTO_INCREMENT,
  `meeting_key` int(11) NOT NULL DEFAULT '0',
  `room_key` varchar(255) NOT NULL DEFAULT '',
  `meeting_tag` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(255) DEFAULT NULL COMMENT 'ログ種別',
  `participant_id` varchar(255) DEFAULT NULL COMMENT '参加者ID',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`log_no`),
  KEY `meeting_key` (`meeting_key`),
  KEY `room_key` (`room_key`),
  KEY `tag` (`meeting_tag`),
  KEY `create_datetime` (`create_datetime`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `member_key` int(11) NOT NULL AUTO_INCREMENT,
  `user_key` int(11) NOT NULL DEFAULT '0',
  `member_id` text NOT NULL,
  `member_pass` text NOT NULL,
  `member_email` text NOT NULL,
  `member_name` text NOT NULL,
  `member_name_kana` text COMMENT 'フリガナ',
  `member_status` int(11) NOT NULL DEFAULT '0',
  `member_type` varchar(20) NOT NULL COMMENT 'メンバータイプ',
  `member_group` int(11) NOT NULL DEFAULT '0',
  `use_shared_storage` tinyint(1) NOT NULL DEFAULT '1' COMMENT '共有ストレージ使用フラグ',
  `create_datetime` datetime DEFAULT NULL COMMENT '作成日時',
  `update_datetime` datetime DEFAULT NULL COMMENT '更新日時',
  `room_key` varchar(64) DEFAULT NULL COMMENT 'メンバー課金用部屋ID',
  `timezone` varchar(16) DEFAULT NULL COMMENT 'タイムゾーン',
  `lang` varchar(10) DEFAULT NULL COMMENT '言語',
  `mac_address` text COMMENT 'MACアドレス',
  `use_sales` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'セールス機能利用フラグ',
  `outbound_id` varchar(64) DEFAULT NULL COMMENT 'アウトバウンドURL利用ID',
  `outbound_number_id` varchar(64) DEFAULT NULL COMMENT 'セールスアウトバウンドナンバー',
  PRIMARY KEY (`member_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `member_group`
--

CREATE TABLE IF NOT EXISTS `member_group` (
  `member_group_key` int(11) NOT NULL AUTO_INCREMENT,
  `user_key` int(11) NOT NULL DEFAULT '0',
  `member_group_name` text NOT NULL,
  `member_group_registtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`member_group_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `member_room_relation`
--

CREATE TABLE IF NOT EXISTS `member_room_relation` (
  `member_room_relation_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_key` int(11) NOT NULL,
  `room_key` varchar(64) NOT NULL,
  `create_datetime` datetime NOT NULL,
  PRIMARY KEY (`member_room_relation_id`),
  KEY `member_key` (`member_key`,`room_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `member_usage_details`
--

CREATE TABLE IF NOT EXISTS `member_usage_details` (
  `user_key` int(11) DEFAULT NULL,
  `user_id` varchar(64) DEFAULT NULL,
  `user_company_name` text,
  `user_company_postnumber` text,
  `user_company_address` text,
  `user_company_phone` text,
  `user_company_fax` text,
  `user_starttime` datetime DEFAULT NULL,
  `user_deletetime` datetime DEFAULT NULL,
  `user_plan_yearly` tinyint(1) DEFAULT NULL,
  `user_plan_yearly_starttime` datetime DEFAULT NULL,
  `room_key` varchar(64) DEFAULT NULL,
  `meeting_max_seat` int(11) DEFAULT NULL,
  `service_name` text,
  `intro` tinyint(10) DEFAULT NULL,
  `desktop_share` tinyint(1) DEFAULT NULL,
  `hdd_extension` int(11) DEFAULT NULL,
  `participant_key` int(11) DEFAULT NULL,
  `participant_station` text,
  `meeting_key` varchar(128) DEFAULT NULL,
  `meeting_room_name` text,
  `actual_start_datetime` datetime DEFAULT NULL,
  `actual_end_datetime` datetime DEFAULT NULL,
  `member_key` int(11) DEFAULT NULL,
  `member_id` text,
  `member_name` text,
  `member_name_kana` text,
  `member_email` text,
  `uptime_start` datetime DEFAULT NULL,
  `uptime_end` datetime DEFAULT NULL,
  `use_count` int(11) DEFAULT NULL,
  `create_datetime` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `operation_log`
--

CREATE TABLE IF NOT EXISTS `operation_log` (
  `operation_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'キー',
  `user_key` int(11) NOT NULL COMMENT 'ユーザーキー',
  `member_key` int(11) DEFAULT NULL COMMENT 'メンバーキー',
  `member_id` text COMMENT 'メンバーID',
  `session_id` varchar(200) DEFAULT NULL COMMENT 'セッションID',
  `action_name` varchar(255) NOT NULL COMMENT 'アクション',
  `remote_addr` text NOT NULL,
  `operation_datetime` datetime NOT NULL COMMENT '操作日付',
  `info` text NOT NULL COMMENT '操作内容',
  `create_datetime` datetime NOT NULL COMMENT '作成日時',
  PRIMARY KEY (`operation_key`),
  KEY `user_key` (`user_key`,`member_key`,`action_name`,`operation_datetime`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='操作履歴' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `ordered_service_option`
--

CREATE TABLE IF NOT EXISTS `ordered_service_option` (
  `ordered_service_option_key` int(11) NOT NULL AUTO_INCREMENT,
  `room_key` varchar(64) NOT NULL DEFAULT '0',
  `user_service_option_key` int(11) NOT NULL DEFAULT '0',
  `service_option_key` int(11) NOT NULL DEFAULT '0',
  `ordered_service_option_status` int(11) NOT NULL DEFAULT '1' COMMENT '停止中:0、利用中:1、開始待ち:2',
  `ordered_service_option_starttime` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '開始日',
  `ordered_service_option_registtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordered_service_option_deletetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordered_service_option_expiredatetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '停止予定日',
  PRIMARY KEY (`ordered_service_option_key`),
  KEY `room_key` (`room_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `outbound`
--

CREATE TABLE IF NOT EXISTS `outbound` (
  `outbound_key` int(11) NOT NULL AUTO_INCREMENT,
  `user_key` int(11) NOT NULL,
  `outbound_prefix` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `create_datetime` datetime DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`outbound_key`),
  KEY `user_key` (`user_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `participant`
--

CREATE TABLE IF NOT EXISTS `participant` (
  `participant_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `participant_session_id` varchar(255) NOT NULL DEFAULT '',
  `storage_login_id` varchar(255) DEFAULT NULL COMMENT 'ストレージファイル用のキー',
  `personal_wb_login_id` varchar(64) DEFAULT NULL COMMENT '個人WB保存用ID',
  `meeting_key` int(10) unsigned NOT NULL DEFAULT '0',
  `user_key` int(11) DEFAULT NULL,
  `member_key` int(11) DEFAULT NULL COMMENT 'メンバーキー',
  `fms_server_key` int(11) DEFAULT NULL COMMENT '利用FMSサーバーキー',
  `participant_id` text,
  `participant_name` text,
  `participant_email` text,
  `participant_station` text,
  `participant_locale` varchar(32) NOT NULL DEFAULT 'ja',
  `participant_country` varchar(32) DEFAULT NULL COMMENT '所在地',
  `participant_protocol` varchar(20) DEFAULT NULL COMMENT 'プロトコル',
  `participant_port` varchar(20) DEFAULT NULL COMMENT 'ポート',
  `participant_mode_key` int(10) unsigned NOT NULL DEFAULT '0',
  `participant_type_key` int(10) unsigned NOT NULL DEFAULT '0',
  `participant_role_key` int(11) unsigned NOT NULL DEFAULT '0',
  `participant_skin_type` int(10) unsigned DEFAULT NULL,
  `uptime_start` datetime DEFAULT NULL COMMENT '入室日時',
  `uptime_end` datetime DEFAULT NULL COMMENT '退出日時',
  `use_count` int(11) NOT NULL DEFAULT '0',
  `teleconference_pin_cd` int(11) NOT NULL DEFAULT '0' COMMENT 'PGi連携用Pin code',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_narrowband` tinyint(1) NOT NULL DEFAULT '0',
  `is_away` tinyint(4) NOT NULL DEFAULT '0' COMMENT '一時退室フラグ',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT NULL,
  `ives_part_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`participant_key`),
  KEY `participant_session_id` (`participant_session_id`),
  KEY `meeting_key` (`meeting_key`),
  KEY `member_key` (`member_key`),
  KEY `create_datetime` (`create_datetime`),
  KEY `is_active` (`is_active`),
  KEY `user_key` (`user_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `pgi_rate`
--

CREATE TABLE IF NOT EXISTS `pgi_rate` (
  `pgi_rate_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'プライマリキー',
  `pgi_setting_key` int(11) NOT NULL COMMENT 'pgi_settingテーブルのキー',
  `rate` int(11) NOT NULL COMMENT '料金',
  `rate_type` varchar(32) NOT NULL COMMENT 'dnisの対応するキー',
  `is_deleted` varchar(45) NOT NULL DEFAULT '0',
  `registtime` datetime NOT NULL COMMENT '登録日',
  `updatetime` datetime DEFAULT NULL COMMENT '更新日',
  PRIMARY KEY (`pgi_rate_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `pgi_setting`
--

CREATE TABLE IF NOT EXISTS `pgi_setting` (
  `pgi_setting_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'プライマリキー',
  `room_key` varchar(64) NOT NULL COMMENT 'roomテーブルのプライマリキー',
  `system_key` varchar(32) DEFAULT NULL COMMENT 'pgiのシステムキー',
  `company_id` varchar(64) NOT NULL COMMENT 'CompanyID',
  `client_id` varchar(10) NOT NULL COMMENT 'ClientID',
  `client_pw` varchar(8) NOT NULL COMMENT 'ClientPW',
  `admin_client_id` varchar(64) DEFAULT NULL,
  `admin_client_pw` varchar(64) DEFAULT NULL,
  `startdate` date DEFAULT NULL COMMENT '料金プランの開始月',
  `is_deleted` varchar(45) NOT NULL DEFAULT '0',
  `registtime` datetime NOT NULL COMMENT '登録日',
  `updatetime` datetime DEFAULT NULL COMMENT '更新日',
  PRIMARY KEY (`pgi_setting_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `ping_failed`
--

CREATE TABLE IF NOT EXISTS `ping_failed` (
  `ping_failed_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '連番',
  `type` varchar(20) NOT NULL COMMENT 'タイプ',
  `reload_type` varchar(20) NOT NULL COMMENT 'リロードタイプ',
  `fms_key` int(11) NOT NULL COMMENT 'FMS',
  `user_key` int(11) NOT NULL COMMENT 'ユーザーKEY',
  `meeting_key` int(11) NOT NULL COMMENT '会議キー',
  `sequence_key` int(11) NOT NULL COMMENT '会議シーケンス',
  `p_login_time` datetime NOT NULL COMMENT '入室時間',
  `p_time` datetime NOT NULL COMMENT '時間',
  `p_session` varchar(255) NOT NULL COMMENT '参加者KEY',
  `p_protocol` varchar(10) NOT NULL COMMENT 'プロトコル',
  `p_port` int(11) NOT NULL COMMENT 'ポート',
  `p_number` int(11) NOT NULL COMMENT '参加人数',
  `p_count` int(11) NOT NULL COMMENT '連続失敗回数',
  `fms_bytes_in` int(11) DEFAULT NULL COMMENT '受信した合計バイト数',
  `fms_bytes_out` int(11) DEFAULT NULL COMMENT '送信した合計バイト数',
  `fms_msg_in` int(11) DEFAULT NULL COMMENT '受信RTMPメッセージ合計数',
  `fms_msg_out` int(11) DEFAULT NULL COMMENT '送信RTMPメッセージ合計数',
  `fms_msg_dropped` int(11) DEFAULT NULL COMMENT '欠落RTMPメッセージ合計数',
  `fms_ping_rtt` int(11) DEFAULT NULL COMMENT 'pingメッセージに応答するまでの時間',
  `fms_audio_queue_msgs` int(11) DEFAULT NULL COMMENT 'オーディオメッセージ数',
  `fms_video_queue_msgs` int(11) DEFAULT NULL COMMENT 'ビデオメッセージ数',
  `fms_so_queue_msgs` int(11) DEFAULT NULL COMMENT '共有オブジェクトメッセージ数',
  `fms_data_queue_msgs` int(11) DEFAULT NULL COMMENT 'データメッセージ数',
  `fms_dropped_audio_msgs` int(11) DEFAULT NULL COMMENT '欠落オーディオメッセージ数',
  `fms_dropped_video_msgs` int(11) DEFAULT NULL COMMENT '欠落ビデオメッセージ数',
  `fms_audio_queue_bytes` int(11) DEFAULT NULL COMMENT 'オーディオメッセージサイズ',
  `fms_video_queue_bytes` int(11) DEFAULT NULL COMMENT 'ビデオメッセージサイズ',
  `fms_so_queue_bytes` int(11) DEFAULT NULL COMMENT '共有オブジェクトメッセージサイズ',
  `fms_data_queue_bytes` int(11) DEFAULT NULL COMMENT 'データメッセージサイズ',
  `fms_dropped_audio_bytes` int(11) DEFAULT NULL COMMENT '欠落オーディオメッセージサイズ',
  `fms_dropped_video_bytes` int(11) DEFAULT NULL COMMENT '欠落ビデオメッセージサイズ',
  `fms_bw_out` int(11) DEFAULT NULL COMMENT '上り帯域幅',
  `fms_bw_in` int(11) DEFAULT NULL COMMENT '下り帯域幅',
  `fms_client_id` varchar(200) DEFAULT NULL COMMENT '固有のID',
  `create_datetime` datetime NOT NULL COMMENT '作成日',
  PRIMARY KEY (`ping_failed_id`),
  KEY `meeting_key` (`meeting_key`),
  KEY `sequence_key` (`sequence_key`),
  KEY `type` (`type`),
  KEY `fms_key` (`fms_key`),
  KEY `user_key` (`user_key`),
  KEY `p_session` (`p_session`),
  KEY `reload_type` (`reload_type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `pw_reset`
--

CREATE TABLE IF NOT EXISTS `pw_reset` (
  `pw_reset_id` int(11) NOT NULL AUTO_INCREMENT,
  `authorize_key` varchar(255) NOT NULL,
  `ignore_key` varchar(255) NOT NULL,
  `user_key` int(11) NOT NULL,
  `member_key` int(11) DEFAULT NULL,
  `name` text NOT NULL,
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`pw_reset_id`),
  UNIQUE KEY `authorize_key` (`authorize_key`),
  UNIQUE KEY `ignore_key` (`ignore_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `quick_vote_log`
--

CREATE TABLE IF NOT EXISTS `quick_vote_log` (
  `quick_vote_log_key` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'アンケート結果KEY',
  `meeting_key` int(11) NOT NULL COMMENT 'ミーティングKEY',
  `meeting_sequence_key` int(11) NOT NULL COMMENT 'ミーティングシーケンスKEY',
  `number` int(11) NOT NULL COMMENT 'アンケート番号',
  `participant_key` int(10) NOT NULL COMMENT '参加者KEY',
  `participant_name` varchar(255) NOT NULL COMMENT '参加者名',
  `result` varchar(20) NOT NULL COMMENT '投票結果',
  `fcs_quick_vote_id` varchar(60) NOT NULL COMMENT 'fms側識別ID',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`quick_vote_log_key`),
  KEY `meeting_key` (`meeting_key`),
  KEY `meeting_sequence_key` (`meeting_sequence_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `reservation`
--

CREATE TABLE IF NOT EXISTS `reservation` (
  `reservation_key` int(11) NOT NULL AUTO_INCREMENT,
  `user_key` int(11) NOT NULL COMMENT 'ユーザKEY',
  `member_key` int(11) DEFAULT NULL COMMENT 'メンバーKEY',
  `room_key` varchar(64) NOT NULL DEFAULT '',
  `meeting_key` varchar(200) DEFAULT NULL,
  `reservation_name` varchar(64) NOT NULL DEFAULT '',
  `reservation_place` varchar(16) NOT NULL DEFAULT '',
  `reservation_starttime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reservation_endtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reservation_pw` varchar(16) NOT NULL DEFAULT '',
  `reservation_pw_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'パスワードタイプ',
  `sender_name` varchar(255) DEFAULT NULL COMMENT '送信者名',
  `sender_email` varchar(255) DEFAULT NULL COMMENT '送信者メール',
  `sender_lang` varchar(10) DEFAULT NULL COMMENT '送信者言語',
  `mail_type` varchar(20) DEFAULT NULL COMMENT 'メール形式(html/text)nullの場合だとtext',
  `reservation_url` varchar(100) DEFAULT NULL COMMENT '予約時のURL',
  `reservation_custom` varchar(64) DEFAULT NULL COMMENT 'カスタマードメイン情報保存',
  `reservation_info` text COMMENT '予約内容',
  `is_multicamera` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'マルチカメラ 0:使用しない、1:使用する',
  `is_limited_function` tinyint(4) NOT NULL DEFAULT '0' COMMENT '機能制限 0:使用しない、1:使用する',
  `is_telephone` tinyint(4) NOT NULL DEFAULT '0' COMMENT '音声通話 0:使用しない、1:使用する',
  `is_h323` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'H.323 0:使用しない、1:使用する',
  `is_mobile_phone` tinyint(4) NOT NULL DEFAULT '0' COMMENT '携帯 0:使用し>ない、1:使用する',
  `is_high_quality` tinyint(4) NOT NULL DEFAULT '0' COMMENT '高画質 0:使用しない、1:使用する',
  `is_desktop_share` tinyint(4) NOT NULL DEFAULT '0' COMMENT '共有 0:使用しない、1:使用>する',
  `is_customer_desktop_share_flg` tinyint(4) DEFAULT '0' COMMENT 'カスタマーPC共有 0:使用しない、1:使用する',
  `is_meeting_ssl` tinyint(4) NOT NULL DEFAULT '0' COMMENT '暗号化 0:使用しない、1:使用す>る',
  `is_invite_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT '会議室内招待 0:使用しない、1:使>用する',
  `is_rec_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT '録画 0:使用しない、1:使用する',
  `is_auto_rec_flg` tinyint(4) DEFAULT '0' COMMENT '自動録画 0:使用しない、1:使用する',
  `is_convert_wb_to_pdf` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'WBのPDF化デフォルトフラグ',
  `is_cabinet_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'キャビネット 0:使用しない、1:使用する',
  `is_customer_camera_flg` tinyint(4) DEFAULT '0' COMMENT 'お客様カメラ利用 0:使用しない、1:使用する',
  `is_customer_camera_display` tinyint(4) DEFAULT '1' COMMENT 'お客様側映像 0:非表示、1:表示',
  `is_wb_allow_flg` tinyint(4) DEFAULT '0' COMMENT 'WB操作許可 0:使用しない、1:使用する',
  `is_chat_allow_flg` tinyint(4) DEFAULT '0' COMMENT 'チャット操作許可 0:使用しない、1:使用する',
  `is_print_allow_flg` tinyint(4) DEFAULT '0' COMMENT '印刷操作許可 0:使用しない、1:使用する',
  `is_wb_no_flg` tinyint(4) DEFAULT '1',
  `is_reminder_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'リマインダーフラグ',
  `is_reminder_send_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'リマインダーメール送信フラグ、0:未送信、1:送信済み、2:エラー',
  `is_organizer_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT '主催者参加可否',
  `organizer_name` varchar(255) DEFAULT NULL COMMENT '主催者の名前',
  `organizer_email` varchar(255) DEFAULT NULL COMMENT '主催者メールアドレス',
  `reservation_session` varchar(64) NOT NULL DEFAULT '',
  `reservation_status` int(11) NOT NULL DEFAULT '0',
  `reservation_registtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reservation_updatetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`reservation_key`),
  KEY `room_key` (`room_key`,`reservation_starttime`,`reservation_endtime`),
  KEY `reservation_session` (`reservation_session`),
  KEY `meeting_key` (`meeting_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `reservation_user`
--

CREATE TABLE IF NOT EXISTS `reservation_user` (
  `r_user_key` int(11) NOT NULL AUTO_INCREMENT,
  `reservation_key` int(11) NOT NULL DEFAULT '0',
  `r_user_name` varchar(64) NOT NULL DEFAULT '',
  `r_user_email` varchar(255) NOT NULL DEFAULT '',
  `r_user_place` varchar(16) NOT NULL DEFAULT '',
  `r_user_lang` varchar(10) DEFAULT NULL,
  `r_user_audience` varchar(8) DEFAULT NULL,
  `r_user_session` varchar(64) NOT NULL DEFAULT '',
  `r_member_key` int(11) DEFAULT NULL,
  `r_user_status` int(11) NOT NULL DEFAULT '0',
  `r_organizer_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT '主催者フラグ',
  `r_user_registtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `r_user_updatetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`r_user_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `room`
--

CREATE TABLE IF NOT EXISTS `room` (
  `room_key` varchar(64) NOT NULL DEFAULT '',
  `max_seat` int(11) DEFAULT '0' COMMENT '最大参加者数',
  `max_audience_seat` int(11) DEFAULT '0' COMMENT '最大オーディエンス数',
  `max_whiteboard_seat` int(11) NOT NULL DEFAULT '0' COMMENT 'ホワイトボードユーザー',
  `max_guest_seat` int(11) DEFAULT NULL COMMENT '最大招待者数',
  `max_guest_seat_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT '最大招待者設定フラグ 1:ON 0:OFF',
  `extend_max_seat` int(11) NOT NULL,
  `extend_max_audience_seat` int(11) NOT NULL,
  `extend_seat_flg` int(11) NOT NULL,
  `member_multi_room_flg` tinyint(1) NOT NULL DEFAULT '0',
  `user_key` int(11) NOT NULL DEFAULT '0',
  `room_name` text,
  `room_layout_type` int(11) DEFAULT NULL,
  `room_status` int(11) NOT NULL DEFAULT '1',
  `room_sort` int(11) NOT NULL DEFAULT '1',
  `max_room_bandwidth` int(11) NOT NULL DEFAULT '2048' COMMENT '部屋上限帯域',
  `max_user_bandwidth` int(11) NOT NULL DEFAULT '256' COMMENT '拠点上限帯域',
  `min_user_bandwidth` int(11) NOT NULL DEFAULT '30' COMMENT '拠点下限限帯域',
  `cabinet` tinyint(4) NOT NULL DEFAULT '1',
  `max_shared_memo_size` int(11) DEFAULT '100000' COMMENT '共有メモ最大サイズ',
  `mfp` varchar(10) DEFAULT 'all',
  `default_layout_4to3` varchar(20) DEFAULT NULL COMMENT '4:3デフォルトレイアウト',
  `default_layout_16to9` varchar(20) DEFAULT NULL COMMENT '16:9デフォルトレイアウト',
  `default_microphone_mute` varchar(20) NOT NULL DEFAULT 'off' COMMENT 'マイクのデフォルトmuteフラグ',
  `default_camera_mute` varchar(20) NOT NULL DEFAULT 'off' COMMENT 'カメラのデフォルトmuteフラグ',
  `default_camera_size` varchar(64) DEFAULT NULL COMMENT 'カメラのデフォルトサイズ',
  `default_auto_rec_use_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT '自動録画フラグ',
  `disable_rec_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT '録画不可フラグ',
  `is_convert_wb_to_pdf` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'WBのPDF化デフォルトフラグ',
  `is_personal_wb` tinyint(4) NOT NULL DEFAULT '1' COMMENT '個人ホワイトボード：0:使用しない、1:使用する',
  `invited_limit_time` int(11) NOT NULL DEFAULT '0' COMMENT '時間制限を適用する場合は分をいれる',
  `hd_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'HDプランフラグ',
  `default_h264_use_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'h264利用フラグ',
  `default_agc_use_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'AGC利用フラグ',
  `use_sound_codec` varchar(64) DEFAULT NULL COMMENT '利用sound codec',
  `whiteboard_page` int(11) DEFAULT '100' COMMENT '資料枚数',
  `whiteboard_size` int(11) DEFAULT '20' COMMENT '資料サイズ',
  `whiteboard_filetype` text COMMENT '資料ファイルタイプ',
  `cabinet_size` int(11) DEFAULT '20' COMMENT 'キャビネットサイズ',
  `cabinet_filetype` text COMMENT '資料ファイルタイプ',
  `is_auto_transceiver` int(1) DEFAULT '0' COMMENT 'トランシーバーモード',
  `transceiver_number` int(11) NOT NULL COMMENT '切り替え拠点数',
  `is_auto_voice_priority` int(1) DEFAULT '0' COMMENT '音声優先モード',
  `is_netspeed_check` varchar(10) DEFAULT '' COMMENT '回戦速度チェック',
  `is_wb_no` int(1) NOT NULL DEFAULT '1' COMMENT 'ホワイトボードページ番号表示',
  `is_device_skip` int(1) NOT NULL DEFAULT '0' COMMENT '入室時のデバイス選択スキップ',
  `is_participant_notifier` tinyint(4) NOT NULL DEFAULT '1' COMMENT '参加者の入退室状況バルーン 0:非表示、1:表示',
  `use_teleconf` tinyint(1) NOT NULL DEFAULT '0' COMMENT '電話会議使用有無',
  `use_pgi_dialin` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'pgiダイヤルイン使用有無',
  `use_pgi_dialin_free` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'pgiダイヤルイン使用有無(フリーダイヤル)',
  `use_pgi_dialout` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'pgiダイヤルアウト(携帯)使用有無',
  `use_sales_option` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'セールスオプション利用フラグ',
  `use_extend_bandwidth` varchar(64) DEFAULT NULL COMMENT '利用帯域追加機能',
  `prohibit_extend_meeting_flg` tinyint(1) NOT NULL DEFAULT '0' COMMENT '会議延長禁止フラグ',
  `audience_chat_flg` tinyint(1) DEFAULT '0' COMMENT 'オーディエンスのチャットフラグ',
  `outbound_id` varchar(64) DEFAULT NULL COMMENT 'アウトバウンドURL利用ID',
  `inbound_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'インバウンド利用フラグ',
  `inbound_id` varchar(255) DEFAULT NULL COMMENT 'インバウンドID',
  `reenter_flg` tinyint(1) DEFAULT '0' COMMENT 'インバウンド優先再入室グラグ',
  `wb_allow_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'ホワイトボード操作の初期設定フラグ',
  `chat_allow_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'チャット操作の初期設定フラグ',
  `print_allow_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT '印刷操作の初期設定フラグ',
  `customer_camera_use_flg` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'お客様側カメラの利用0:禁止、1:許可',
  `customer_camera_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'お客様側映像 0:非表示、1:表示',
  `customer_sharing_button_hide_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'お客様側デスクトップ共有ボタン',
  `ignore_staff_reenter_alert` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'スタッフ再入室アラート非表示フラグ',
  `room_image` int(11) NOT NULL DEFAULT '0',
  `room_password` varchar(16) DEFAULT NULL,
  `addition` text COMMENT 'セールス用設定保存カラム',
  `comment` text COMMENT 'アウトバウンドページ',
  `rtmp_protocol` text COMMENT 'RTMPプロトコル',
  `rec_gw_convert_type` varchar(64) NOT NULL DEFAULT 'mov' COMMENT '録画GW変換形式',
  `rec_gw_userpage_setting_flg` tinyint(1) NOT NULL DEFAULT '0' COMMENT '録画GWをユーザーページで利用可否フラグ',
  `meeting_limit_time` int(11) NOT NULL DEFAULT '0',
  `creating_meeting_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT '会議データ作成中フラグ',
  `mobile_mix` int(11) NOT NULL DEFAULT '1',
  `mobile_4x4_layout` int(1) NOT NULL DEFAULT '0',
  `meeting_key` varchar(128) DEFAULT NULL,
  `room_registtime` datetime DEFAULT '0000-00-00 00:00:00',
  `room_updatetime` datetime DEFAULT '0000-00-00 00:00:00',
  `room_deletetime` datetime DEFAULT '0000-00-00 00:00:00',
  `room_expiredatetime` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '停止予定日',
  PRIMARY KEY (`room_key`),
  KEY `user_key` (`user_key`),
  KEY `mobile_mix` (`mobile_mix`),
  KEY `mobile_4x4_layout` (`mobile_4x4_layout`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `room_plan`
--

CREATE TABLE IF NOT EXISTS `room_plan` (
  `room_plan_key` int(11) NOT NULL AUTO_INCREMENT,
  `room_key` varchar(64) NOT NULL DEFAULT '',
  `service_key` int(11) NOT NULL DEFAULT '0',
  `discount_rate` int(11) NOT NULL DEFAULT '0' COMMENT '割引率',
  `contract_month_number` int(11) NOT NULL DEFAULT '0' COMMENT '契約期間（月数）',
  `room_plan_yearly` tinyint(4) NOT NULL DEFAULT '0',
  `room_plan_yearly_starttime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `room_plan_starttime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `room_plan_endtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'プラン終了日',
  `room_plan_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '停止中:0、利用中:1、開始待ち:2',
  `room_plan_registtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `room_plan_updatetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`room_plan_key`),
  KEY `room_status` (`room_key`,`room_plan_starttime`,`room_plan_registtime`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `shared_file`
--

CREATE TABLE IF NOT EXISTS `shared_file` (
  `shared_file_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'KEY',
  `meeting_sequence_key` int(11) NOT NULL,
  `meeting_key` int(11) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `file_path` text NOT NULL,
  `file_size` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: 有効, 0: 削除',
  `create_datetime` datetime NOT NULL COMMENT '作成日時',
  `update_datetime` datetime DEFAULT NULL COMMENT '更新日時',
  PRIMARY KEY (`shared_file_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `storage_favorite_file`
--

CREATE TABLE IF NOT EXISTS `storage_favorite_file` (
  `storage_file_key` int(11) NOT NULL COMMENT 'ストレージファイルKEY',
  `user_key` int(11) NOT NULL COMMENT 'ユーザーKEY',
  `member_key` int(11) NOT NULL COMMENT 'メンバーKEY',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `storage_file_key` (`storage_file_key`),
  KEY `user_key` (`user_key`),
  KEY `member_key` (`member_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `storage_file`
--

CREATE TABLE IF NOT EXISTS `storage_file` (
  `storage_file_key` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ストレージファイルKEY',
  `document_id` varchar(255) DEFAULT NULL COMMENT 'ドキュメントID',
  `clip_key` varchar(255) DEFAULT NULL COMMENT '動画ID',
  `personal_white_id` varchar(64) DEFAULT NULL COMMENT '個人ホワイトボードID',
  `user_key` int(11) NOT NULL COMMENT 'ユーザーKEY',
  `member_key` int(11) NOT NULL COMMENT 'メンバーKEY',
  `owner` int(11) NOT NULL COMMENT 'オーナー',
  `file_path` varchar(255) NOT NULL COMMENT '変換後のファイルパス',
  `file_name` varchar(255) NOT NULL COMMENT '実ファイル名',
  `user_file_name` varchar(255) NOT NULL COMMENT 'ユーザー表示用ファイル名',
  `description` varchar(500) DEFAULT NULL COMMENT '説明',
  `file_size` bigint(20) NOT NULL COMMENT '変換後のファイルサイズ',
  `format` varchar(64) NOT NULL COMMENT '変換後フォーマット',
  `category` varchar(64) NOT NULL COMMENT 'カテゴリー',
  `extension` varchar(64) NOT NULL COMMENT '元ファイル拡張子',
  `version` varchar(64) NOT NULL COMMENT 'アニメーションのバージョン',
  `document_index` int(11) NOT NULL DEFAULT '1' COMMENT 'ページ数',
  `status` int(11) NOT NULL COMMENT 'ステータス',
  `storage_folder_key` int(11) NOT NULL COMMENT '所属フォルダーKEY',
  `last_update_member_key` int(11) NOT NULL COMMENT 'ラスト編集者(メンバー)',
  `external_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT '外部ストレージサービス連携ファイルフラグ',
  `external_service_file_key` varchar(255) DEFAULT NULL COMMENT '外部ストレージサービス固有キー',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`storage_file_key`),
  KEY `user_key` (`user_key`),
  KEY `member_key` (`member_key`),
  KEY `document_id` (`document_id`),
  KEY `clip_key` (`clip_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `storage_folder`
--

CREATE TABLE IF NOT EXISTS `storage_folder` (
  `storage_folder_key` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'フォルダKEY',
  `user_key` int(11) NOT NULL COMMENT 'ユーザーKEY',
  `member_key` int(11) NOT NULL COMMENT 'メンバーKEY',
  `folder_name` varchar(255) NOT NULL COMMENT 'フォルダ名',
  `parent_storage_folder_key` int(11) NOT NULL DEFAULT '0' COMMENT '所属フォルダ',
  `path` text NOT NULL COMMENT 'パス',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'ステータス 0:無効、1:有効',
  `external_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT '外部ストレージサービス連携ファイルフラグ',
  `external_service_folder_key` varchar(255) DEFAULT NULL COMMENT '外部ストレージサービス固有キー',
  `external_service_account_key` varchar(255) DEFAULT NULL COMMENT '外部ストレージサービスアカウント情報',
  `external_service_name` varchar(255) DEFAULT NULL COMMENT '外部ストレージ名',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`storage_folder_key`),
  KEY `user_key` (`user_key`),
  KEY `member_key` (`member_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_key` int(11) NOT NULL AUTO_INCREMENT,
  `country_key` int(11) DEFAULT '0',
  `user_id` varchar(64) NOT NULL,
  `user_group` varchar(64) DEFAULT NULL COMMENT '課金対象のグループ',
  `user_password` varchar(64) NOT NULL,
  `user_admin_password` varchar(64) NOT NULL,
  `account_model` varchar(10) DEFAULT NULL COMMENT '課金モデル',
  `max_member_count` int(10) NOT NULL DEFAULT '0' COMMENT '最大メンバー数',
  `max_rec_size` int(10) NOT NULL DEFAULT '0' COMMENT '利用可能ストレージ容量（MB）',
  `max_connect_participant` int(11) DEFAULT NULL COMMENT '同時接続数上限',
  `meeting_limit_time` int(11) NOT NULL DEFAULT '0',
  `user_company_name` text,
  `user_company_postnumber` text,
  `user_company_address` text,
  `user_company_phone` text,
  `user_company_fax` text,
  `user_staff_department` varchar(255) DEFAULT NULL COMMENT '部署',
  `user_staff_firstname` text,
  `user_staff_lastname` text,
  `user_staff_email` text,
  `user_status` tinyint(4) DEFAULT '0',
  `user_delete_status` tinyint(4) DEFAULT '0',
  `intra_fms` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: 未契約, 1: 契約',
  `meeting_version` varchar(20) DEFAULT NULL,
  `lang_allow` varchar(100) DEFAULT NULL COMMENT '言語許可リスト(「,」区切りで指定nullの場合は全許可)',
  `max_storage_size` int(11) NOT NULL DEFAULT '0' COMMENT 'ストレージサイズ(メガ単位)',
  `guest_url_format` int(11) NOT NULL DEFAULT '0' COMMENT '招待URLの表示方法:0:<>あり 1:<>なし',
  `guest_mail_type` int(11) NOT NULL DEFAULT '0' COMMENT '招待メールHTML化:0:<>利用する 1:<>利用しない',
  `user_starttime` datetime DEFAULT '0000-00-00 00:00:00',
  `user_registtime` datetime DEFAULT '0000-00-00 00:00:00',
  `user_updatetime` datetime DEFAULT '0000-00-00 00:00:00',
  `user_expiredatetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '停止予定日',
  `user_deletetime` datetime DEFAULT '0000-00-00 00:00:00',
  `is_compulsion_pw` int(11) DEFAULT NULL COMMENT '強制パスワードフラグ',
  `compulsion_pw` varchar(255) DEFAULT NULL COMMENT '会議デフォルトPW',
  `is_minutes_delete` int(11) DEFAULT NULL COMMENT '自動議事録削除',
  `is_convert_wb_to_pdf` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'WBのPDF化デフォルトフラグ',
  `is_gl_reset_stream` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'GlobalLink用遅延解消フラグ',
  `has_chat` int(1) DEFAULT '0' COMMENT '0:無し、1:有り',
  `vcubeid_add_use_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'VCUBE IDからのアカウント登録フラグ',
  `external_user_invitation_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'ID制の外部招待許可フラグ',
  `external_member_invitation_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT '別ユーザーに紐付くメンバーの招待（メンバー課金用）',
  `member_multi_room_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'メンバー課金複数部屋',
  `force_stop_meeting_flg` tinyint(1) NOT NULL DEFAULT '0' COMMENT '強制会議終了機能使用フラグ',
  `service_name` varchar(255) NOT NULL COMMENT 'custom_domain_key',
  `addition` text,
  `memo` text,
  `staff_key` int(11) unsigned DEFAULT NULL COMMENT '担当キー',
  `invoice_flg` tinyint(4) unsigned DEFAULT '0' COMMENT '請求対象判別フラグ',
  `is_cybozu_option` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'cybozu連係オプション',
  `use_clip_share` tinyint(1) NOT NULL DEFAULT '0' COMMENT '動画共有オプション',
  `use_sales` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'セールス機能利用フラグ',
  `use_port_plan` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'ポート課金フラグ',
  `user_whiteboard_advertise` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'ホワイトボード広告表示',
  `clip_share_storage_size` int(10) NOT NULL DEFAULT '0' COMMENT '動画共有利用容量(MB)',
  `payment_terms` varchar(10) DEFAULT 'post' COMMENT '請求月 { pre : 前請求、 on : 当月請求、 post : 後請求 }',
  `support_info_type` varchar(64) DEFAULT NULL COMMENT 'サポートタイプ(Free：free)',
  PRIMARY KEY (`user_key`),
  KEY `intra_fms` (`intra_fms`),
  KEY `user_group` (`user_group`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `user_fms_server`
--

CREATE TABLE IF NOT EXISTS `user_fms_server` (
  `fms_key` int(11) NOT NULL AUTO_INCREMENT,
  `user_key` int(11) NOT NULL,
  `server_address` varchar(64) NOT NULL,
  `server_port` int(10) NOT NULL,
  `server_count` int(10) NOT NULL DEFAULT '0' COMMENT '利用回数',
  `server_priority` int(10) NOT NULL DEFAULT '1' COMMENT '優先度',
  `error_count` int(11) NOT NULL DEFAULT '0',
  `is_available` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0: 停止, 1: 有効',
  `registtime` datetime NOT NULL,
  `updatetime` datetime NOT NULL,
  PRIMARY KEY (`fms_key`),
  KEY `status` (`is_available`),
  KEY `user_key` (`user_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `user_group`
--

CREATE TABLE IF NOT EXISTS `user_group` (
  `group_id` varchar(64) NOT NULL COMMENT 'グループID',
  `group_name` text NOT NULL COMMENT 'グループ名',
  `basic_fee` bigint(20) NOT NULL COMMENT '基本料金',
  `running_fee` int(11) NOT NULL COMMENT '追加料金(分)',
  `limit_fee` bigint(20) NOT NULL COMMENT '上限料金',
  `free_time` int(11) NOT NULL COMMENT '無料通話時間(分)',
  `status` varchar(1) NOT NULL COMMENT '0: 無効, 1: 有効',
  `create_datetime` datetime NOT NULL COMMENT '開始日時',
  `update_datetime` datetime NOT NULL COMMENT '更新日付'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `user_plan`
--

CREATE TABLE IF NOT EXISTS `user_plan` (
  `user_plan_key` int(11) NOT NULL AUTO_INCREMENT,
  `user_key` varchar(64) NOT NULL,
  `service_key` int(11) NOT NULL DEFAULT '0',
  `discount_rate` int(11) NOT NULL DEFAULT '0' COMMENT '割引率',
  `contract_month_number` int(11) NOT NULL DEFAULT '0' COMMENT '契約期間（月数）',
  `user_plan_yearly` tinyint(4) NOT NULL DEFAULT '0',
  `user_plan_yearly_starttime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_plan_starttime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_plan_endtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'プラン終了日',
  `user_plan_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '停止中:0、利用中:1、開始待ち:2',
  `sales_plan_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'セールス部屋利用プラン',
  `user_plan_registtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_plan_updatetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_plan_key`),
  KEY `user_key` (`user_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `user_service_option`
--

CREATE TABLE IF NOT EXISTS `user_service_option` (
  `user_service_option_key` int(11) NOT NULL AUTO_INCREMENT,
  `user_key` varchar(64) NOT NULL DEFAULT '0',
  `service_option_key` int(11) NOT NULL DEFAULT '0',
  `user_service_option_status` int(11) NOT NULL DEFAULT '1' COMMENT '停止中:0、利用中:1、開始待ち:2',
  `user_service_option_starttime` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '開始日',
  `user_service_option_registtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_service_option_deletetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_service_option_expiredatetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '停止予定日',
  PRIMARY KEY (`user_service_option_key`),
  KEY `room_key` (`user_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `video_conference`
--

CREATE TABLE IF NOT EXISTS `video_conference` (
  `video_conference_key` int(11) NOT NULL AUTO_INCREMENT,
  `mcu_server_key` int(10) NOT NULL,
  `media_mixer_key` int(10) NOT NULL,
  `user_key` int(11) NOT NULL,
  `meeting_key` int(10) NOT NULL,
  `room_key` varchar(64) NOT NULL,
  `conference_id` varchar(64) NOT NULL,
  `did` varchar(20) NOT NULL,
  `document_sharing_status` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `createtime` datetime NOT NULL,
  `starttime` datetime NOT NULL,
  `updatetime` datetime NOT NULL,
  PRIMARY KEY (`video_conference_key`),
  KEY `user_key` (`user_key`),
  KEY `mcu_server_key` (`mcu_server_key`),
  KEY `media_mixer_key` (`media_mixer_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `video_conference_operation_log`
--

CREATE TABLE IF NOT EXISTS `video_conference_operation_log` (
  `operation_key` int(11) NOT NULL AUTO_INCREMENT,
  `operation_id` int(11) NOT NULL,
  `did` varchar(20) NOT NULL,
  `room_key` varchar(64) NOT NULL,
  `user_key` int(11) NOT NULL,
  `confid` varchar(64) NOT NULL,
  `result` tinyint(1) NOT NULL,
  `operator` varchar(64) NOT NULL,
  `detail` text,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`operation_key`),
  KEY `operation_id` (`operation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `video_conference_participant`
--

CREATE TABLE IF NOT EXISTS `video_conference_participant` (
  `video_participant_key` int(11) NOT NULL AUTO_INCREMENT,
  `video_conference_key` int(11) NOT NULL,
  `media_mixer_key` int(10) NOT NULL,
  `conference_id` varchar(64) NOT NULL,
  `did` varchar(20) DEFAULT NULL,
  `participant_session_id` varchar(50) NOT NULL,
  `part_id` int(10) NOT NULL,
  `participant_name` varchar(255) NOT NULL,
  `participant_ip` varchar(20) NOT NULL,
  `participant_type` varchar(30) NOT NULL,
  `participant_state` int(2) NOT NULL,
  `participant_key` int(10) NOT NULL,
  `meeting_key` int(10) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `createtime` datetime NOT NULL,
  `updatetime` datetime NOT NULL,
  PRIMARY KEY (`video_participant_key`),
  KEY `did` (`did`),
  KEY `media_mixer_key` (`media_mixer_key`),
  KEY `participant_session_id` (`participant_session_id`),
  KEY `participant_type` (`participant_type`),
  KEY `participant_state` (`participant_state`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
