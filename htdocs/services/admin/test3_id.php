<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
ini_set("memory_limit","2000M");
set_time_limit(0);
require_once ('classes/AppFrame.class.php');
require_once ('classes/mgm/dbi/user.dbi.php');
require_once("classes/N2MY_Account.class.php");
require_once ('classes/dbi/member.dbi.php');
require_once ('classes/dbi/member_group.dbi.php');
require_once ('classes/dbi/member_status.dbi.php');
require_once ('classes/dbi/user.dbi.php');
require_once ('config/config.inc.php');
require_once ('lib/EZLib/EZUtil/EZEncrypt.class.php');
require_once( "lib/EZLib/EZCore/EZLogger2.class.php" );
require_once ('lib/EZLib/EZUtil/EZCsv.class.php');
require_once ('classes/dbi/blacklist.dbi.php');
require_once ('classes/dbi/whitelist.dbi.php');


class AppMember extends AppFrame
{
    var $logger = null;
    var $memberTable = null;
    var $blacklistTable = null;
    var $whitelistTable = null;
    var $account_dsn = null;
    private $b_array = array();//変更前csvの配列を格納
    private $a_array = array();//変更後csvの配列を格納

    function init() {
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $this->memberTable = new MemberTable($this->get_dsn());
        $this->blacklistTable = new blacklistTable($this->get_dsn());
        $this->whitelistTable = new whitelistTable($this->get_dsn());  
    }

    public function action_member_id() {
        $user_info = $this->session->get("user_info");
        $user_key = $user_info["user_key"];

        $table_name = $_POST["table"];

        //チェックの内容取得
        switch($table_name) {
           case "1":
               echo " 変更前CSVのバリデーションチェック <br><br>" ;
               $b_array = $this->before();
               $this->after_vali_check($b_array , "before");
               break;
           case "4":
               echo " 変更後CSVのバリデーションチェック <br><br>" ;
               $b_array = $this->before();
               $this->after_vali_check($b_array , "after");
               break;
           case "2":
               echo "  変更前のcsvが既存のmenber tableに存在するか  " ;
               $list = $this->before();
               $this->table_check($list, "before");
               break;
           case "3":
               echo " 変更後のcsvが既存のmenber table に存在しないか" ;
               $list = $this->before();
               $this->table_check($list , "after");
               break;
           default:
               echo "例外発生";
               exit;
        }

echo " ------ end ------ ";

      }

    // before ファイル読み込み
    public function before() {

        $request["upload_file"] = $_FILES["import_file_b"];
        $data = pathinfo($request["upload_file"]["name"]);
        echo "<br><br>";
        echo "CSVファイルチェック";
        if (empty($data['filename'])) {
          echo "-> 変更前のCSVファイルに不備があります。";
          exit;
        } elseif($data['extension'] != "csv") {
          echo "-> 変更前のCSVファイルに不備がありますす、形式を確認してください。";
          exit;
        }
        echo "<br>";
        echo "-> ok ";

        //CSVファイル格納
        $filename = $this->get_work_dir()."/".tmpfile();
        move_uploaded_file($request["upload_file"]["tmp_name"], $filename);
        $request["upload_file"]["tmp_name"] = $filename;
        $output_encoding = $this->get_output_encoding();
        $csv = new EZCsv(true,$output_encoding,"UTF-8");
        $csv->open($filename, "r");
        $rows_b = array();
        while($row = $csv->getNext(true)) {
            $rows_b[] = $row["member_id"];
        }

        $csv->close();
        unlink($filename);
        return $rows_b;
        
        }

    // after ファイル読みこみ
/*
    public function after() {
        $request["upload_file"] = $_FILES["import_file_a"];
        $data = pathinfo($request["upload_file"]["name"]);
        echo "<br><br>";
        echo "CSVファイルチェック";
        if (empty($data['filename'])) {
          echo "->  変更後のCSVファイルに不備があります。";
          exit;
        } elseif($data['extension'] != "csv") {
          echo "->  変更後のCSVファイルに不備があります、形式を確認してください。";
          exit;
        }
        echo "<br>";
        echo "-> ok ";

        //CSVファイル格納
        $filename = $this->get_work_dir()."/".tmpfile();
        move_uploaded_file($request["upload_file"]["tmp_name"], $filename);
        $request["upload_file"]["tmp_name"] = $filename;
        $output_encoding = $this->get_output_encoding();
        $csv = new EZCsv(true,$output_encoding,"UTF-8");
        $csv->open($filename, "r");
        $rows_a = array();
        while($row = $csv->getNext(true)) {
            $rows_a[] = $row["member_id"];
        }
        $csv->close();
        unlink($filename);
        return $rows_a;
    }
*/
    // IDのバリデーションチェック（既存のところから取得）
    public function after_vali_check($a_array , $flg) {
        echo "vali check <br>";
        echo "<br> ------ start ------ ";
        $regex_rule = '/^[[:alnum:]!#$%&\'*+\-\/=?^_`{|}~.@]{3,255}$/';
        foreach($a_array as $member_row) {
            $message = "";
            $message_log = "";
            $valid_result = EZValidator::valid_regex($member_row, $regex_rule);
            if ($valid_result == false){
                $message .= '<li><font color="#ff0000">'."★ : ERROR! vali check : ". $member_row.'</li></font>';
                $message_log .= "★ : ERROR! vali check : ". $member_row;
            }else{
                $message .= '<li>'."ok : ". $member_row."".'</li>';
                $message_log .= "ok : ". $member_row;
            }
            echo $message;
            $config = parse_ini_file( sprintf( "%sconfig/config.ini", N2MY_APP_DIR ), true);
            $log_dir = $config["LOGGER"]["log_dir"];
            $this->_log ( $message_log , $log_dir , "vali_check_".$flg);
        }

    }
    
    // IDの重複チェック
    public function table_check($list , $flg) {
    echo "<br><br>".$flg." table check <br><br>";
    echo "<br> ------ start ------ ";
        foreach($list as $member_row) {
            $where = "member_id = '". $member_row. "'";
            $count = $this->memberTable->numRows($where);
            $message = "";
            $message_log = "";
            if($flg == "before"){
                if ($count == "1"){
                    $message .= '<li>'."ok : ". $member_row."".'</li>';
                    $message_log .= "ok : ". $member_row;
                }elseif ($count == "0"){
                    $message .= '<li><font color="#ff0000">'."★ : ERROR! not table : ". $member_row.'</li></font>';
                    $message_log .= "★ : ERROR! not table : ". $member_row;
                }else{
                    $message .= '<li><font color="#ff0000">'."★ : ERROR! duplicate : ". $member_row.'</li></font>';              
                    $message_log .= "★ : ERROR! duplicate : ". $member_row;              
                }
            }elseif($flg == "after"){
                if ($count == "0"){
                    $message .= '<li>'."ok : ". $member_row."".'</li>';
                    $message_log .="ok : ". $member_row;
                }else{
                    $message .= '<li><font color="#ff0000">'."★ : ERROR! duplicate : ". $member_row.'</li></font>';              
                    $message_log .= "★ : ERROR! duplicate : ";              
                }
            }else{
                    $message .= '<li><font color="#ff0000">'."★ : ERROR!  例外発生 : ". $member_row.'</li></font>';                            
                    $message_log .= "★ : ERROR!  例外発生 : ". $member_row;                            
            }
            echo $message;
            $config = parse_ini_file( sprintf( "%sconfig/config.ini", N2MY_APP_DIR ), true);
            $log_dir = $config["LOGGER"]["log_dir"];
            $this->_log ( $message_log , $log_dir , "table_check_".$flg);
        }
    }
    
    // log書き出し
    private function _log($string , $result_log_dir , $title){
        $log_prefix = $title."_log";
        $log_file = sprintf("%s/%s_%s.log", $result_log_dir, $log_prefix, date('Ymd', time()));
        $fp = fopen($log_file, "a");

        if($fp){
            fwrite($fp, $string."\n");
        }
        fclose($fp);
    }
}

$main =& new AppMember();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>

