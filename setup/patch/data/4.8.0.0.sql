ALTER TABLE  `meeting` ADD  `has_shared_memo` tinyint(1) DEFAULT  '0' COMMENT  '共有メモ保持フラグ' AFTER  `has_recorded_video`;
ALTER TABLE  `meeting_sequence` ADD  `has_shared_memo` tinyint(1) NOT NULL DEFAULT  '0' COMMENT  '共有メモ保持フラグ' AFTER  `has_recorded_video`;
ALTER TABLE  `room` ADD  `max_shared_memo_size` int(11) DEFAULT  '100000' COMMENT  '共有メモ最大サイズ' AFTER  `cabinet`;

CREATE TABLE `shared_file` (
  `shared_file_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'KEY',
  `meeting_sequence_key` int(11) NOT NULL,
  `meeting_key` int(11) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `file_path` text NOT NULL,
  `file_size` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: 有効, 0: 削除',
  `create_datetime` datetime NOT NULL COMMENT '作成日時',
  `update_datetime` datetime DEFAULT NULL COMMENT '更新日時',
  PRIMARY KEY (`shared_file_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE  `member` CHANGE  `lang`  `lang` VARCHAR( 10 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT  '言語';