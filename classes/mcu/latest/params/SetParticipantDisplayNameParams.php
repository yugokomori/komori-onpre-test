<?php
require_once(dirname(__FILE__)."/../params/WSDLParams.php");
 
class SetParticipantDisplayNameParams extends WSDLParams {
	protected $confId;
	protected $mosaicId;
	protected $partId;
	protected $displayText;
	protected $scriptCode;	// 0 固定 http://www.unicode.org/iso15924/iso15924-codes.html
	public function __construct($obj) {
		parent::__construct($this, $obj);
	}
	
	public function getParams() {
		return parent::getParams($this);
	}
}