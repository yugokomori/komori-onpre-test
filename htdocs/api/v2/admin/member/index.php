<?php
require_once("classes/N2MY_Api.class.php");
class N2MY_MeetingAdminMemberAPI extends N2MY_Api
{
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        // SSL対応
        header('Pragma:');
    }

    function auth()
    {
        $this->checkAuthorizationAdmin();
        // ログイン以外の場合は認証させる
    }

    /**
     * メンバー一覧
     */
    function action_get_list() {
        $sort_key  = $this->request->get("sort_key", "name");
        $sort_type = $this->request->get("sort_type", "asc");
        $page      = $this->request->get("page", 1);
        $limit     = $this->request->get("limit", 20);
        $keyword   = $this->request->get("keyword");
        $this->output();
    }

    /**
     * メンバー追加
     */
    function action_add() {
        $member_id = $this->request->get("member_id");
        $member_pw = $this->request->get("member_pw");
        $name      = $this->request->get("name");
        $name_kana = $this->request->get("name_kana");
        $email     = $this->request->get("email");
        $group     = $this->request->get("group");
        $timezone  = $this->request->get("timezone");
        $lang      = $this->request->get("lang");
        $this->output();
    }


    /**
     * メンバー追加
     */
    function action_update() {
        $member_id = $this->request->get("member_id");
        $name      = $this->request->get("name");
        $name_kana = $this->request->get("name_kana");
        $email     = $this->request->get("email");
        $member_pw = $this->request->get("member_pw");
        $group     = $this->request->get("group");
        $timezone  = $this->request->get("timezone");
        $lang      = $this->request->get("lang");
        $this->output();
    }


    /**
     * メンバー削除
     */
    function action_delete() {
        $member_id = $this->request->get("member_id");
        $this->output();
    }

    function action_get_group() {
        $this->output();
    }

    function action_add_group() {
        $group_name       = $this->request->get("group_name");
        $this->output();
    }

    function action_update_group() {
        $group_id       = $this->request->get("group_id");
        $group_name       = $this->request->get("group_name");
        $this->output();
    }

    function action_delete_group() {
        $group_id       = $this->request->get("group_id");
        $this->output();
    }

}
$main = new N2MY_MeetingAdminMemberAPI();
$main->execute();