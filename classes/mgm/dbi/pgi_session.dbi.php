<?php
/**
 * Created on 2013/10/25
 */

require_once("classes/N2MY_DBI.class.php");

class PGiSessionTable extends N2MY_DB {

    var $rules = array(
        "pgi_system_key"  => array("required" => true),
        "pgi_session_token"   => array("required" => true),
    );

    function __construct( $dsn ) {
        $this->init( $dsn, "pgi_session" );
    }

    public function add( $data )
    {
        $data["update_datetime"] = date( "Y-m-d H:i:s" );
        return parent::add( $data );
    }

    public function update( $data, $where )
    {
        $data["update_datetime"] = date( "Y-m-d H:i:s" );
        parent::update( $data, $where );
    }
}