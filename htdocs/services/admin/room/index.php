<?php
require_once('classes/AppFrame.class.php');
require_once('classes/dbi/mail_doc_conv.dbi.php');
require_once('classes/dbi/user.dbi.php');

class AppMember extends AppFrame
{

    var $_name_space = null;
    var $mailDocConvObj = null;
    var $err_obj = null;

    function init() {
        $this->_name_space = md5(__FILE__);
        $user_info = $this->session->get("user_info");
        $this->mailDocConvObj = new MailDocConvet($this->get_dsn(), $user_info["user_key"]);
        $this->userObj        = new UserTable($this->get_dsn());
    }

    function auth() {
        $this->checkAuth();
        $this->checkAdminAuth('admin/session_error.t.html');
    }

    /**
     * デフォルト処理
     */
    function default_view() {
        $this->logger->trace("test", __FILE__, __LINE__, __FUNCTION__);
        $this->render_list();
    }

    function action_show_top() {
        $this->render_list();
    }

    function render_list() {
        // 設定の有効・無効
        $user_info = $this->session->get("user_info");
        $where = "user_key = ".$user_info["user_key"];
        $user_info = $this->userObj->getRow($where);
        if ($user_info["addition"]) {
            $addition = unserialize($user_info["addition"]);
            $mail_doc_conv = isset($addition["mail_doc_conv"]) ? $addition["mail_doc_conv"] : 2;
        } else {
            $mail_doc_conv = 2;
        }
        $this->template->assign("mail_doc_conv", $mail_doc_conv);
        // 登録一覧
        $where = "is_active = 1";
        $list = $this->mailDocConvObj->getList($where);
        $this->template->assign("mail_list", $list);
        $this->display('admin/room/mail_document/list.t.html');
    }

    function action_add_form() {
        if ($this->request->get("new") == 1) {
            $this->clear_session();
        }
        $this->render_add_form();
    }

    function render_add_form() {
        $form_data = $this->get_session();
        if (EZValidator::isError($this->err_obj)) {
            $msg_format = $this->get_message("error");
            $msg = $this->get_message("ERROR");
            $err_fields = $this->err_obj->error_fields();
            foreach ($err_fields as $key) {
                $type = $this->err_obj->get_error_type($key);
                $_msg = sprintf($msg[$type], $this->err_obj->get_error_rule($key, $type));
                $error[$key] = sprintf($msg_format, $_msg);
            }
            $this->logger->info(__FUNCTION__."#error", __FILE__, __LINE__, $error);
            $this->template->assign("err", $error);
        }
        $this->template->assign("form_data", $form_data);
        $this->display('admin/room/mail_document/add_form.t.html');
    }

    function action_add_confirm() {
        $form_data = $this->request->getAll();
        $this->set_session($form_data);
        $this->logger->error($form_data);
        $this->err_obj = $this->check($form_data);
        if (EZValidator::isError($this->err_obj)) {
            return $this->render_add_form();
        }
        $this->render_add_confirm();
    }

    function render_add_confirm() {
        // 二重登録回避
        $this->set_submit_key($this->_name_space);
        $form_data = $this->get_session();
        $this->template->assign("form_data", $form_data);
        $this->display('admin/room/mail_document/add_confirm.t.html');
    }

    function action_add_complete() {
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->render_list();
        }
        $form_data = $this->get_session();
        $data = array(
            "name"             => $form_data["name"],
            "mail_address"     => $form_data["mail_address"]
            );
        $this->mailDocConvObj->add($data);
        // 操作ログ
        $this->add_operation_log('mail_wb_upload_address_add', array('mail' => $form_data["mail_address"]));
        $this->render_add_complete();
    }

    function render_add_complete() {
        $this->display('admin/room/mail_document/add_complete.t.html');
    }

    function action_edit_form() {
        if ($uniq_key = $this->request->get("uniq_key")) {
            $where = "uniq_key = ". $uniq_key;
            $form_data = $this->mailDocConvObj->getRow($where);
            $this->set_session($form_data);
        }
        $this->render_edit_form();
    }

    function render_edit_form() {
        $form_data = $this->get_session();
        $this->template->assign("form_data", $form_data);
        $this->display('admin/room/mail_document/edit_form.t.html');
    }

    function action_edit_confirm() {
        $form_data = $this->request->getAll();
        $this->set_session($form_data);
        $this->err_obj = $this->check($form_data);
        if (EZValidator::isError($this->err_obj)) {
            return $this->render_add_form();
        }
        $this->set_session($this->request->getAll());
        $this->render_edit_confirm();
    }

    function render_edit_confirm() {
        // 二重登録回避
        $this->set_submit_key($this->_name_space);
        $form_data = $this->get_session();
        $this->template->assign("form_data", $form_data);
        $this->display('admin/room/mail_document/edit_confirm.t.html');
    }

    function action_edit_complete() {
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->render_list();
        }
        $form_data = $this->get_session();
        $data = array(
            "name"             => $form_data["name"],
            "mail_address"     => $form_data["mail_address"]
            );
        $where = "uniq_key = ".$form_data["uniq_key"];
        $this->mailDocConvObj->update($data, $where);
        // 操作ログ
        $this->add_operation_log('mail_wb_upload_address_update', array('mail' => $form_data["mail_address"]));
        $this->render_edit_complete();
    }

    function render_edit_complete() {
        $this->display('admin/room/mail_document/edit_complete.t.html');
    }

    function action_delete() {
        $where = "uniq_key = ".$this->request->get("uniq_key");
        $this->mailDocConvObj->delete($where);
        // 操作ログ
        $mail = $this->mailDocConvObj->getOne($where, "mail_address");
        $this->add_operation_log('mail_wb_upload_address_delete', array('mail' => $mail));
        $this->render_list();
    }

    function action_set_active() {
        $is_active = $this->request->get("is_active");
        $user_info = $this->session->get("user_info");
        $where = "user_key = ".$user_info["user_key"];
        $user_info = $this->userObj->getRow($where);
        // 既に項目がある場合は上書き
        if ($user_info["addtion"]) {
            $addition = unserialize($user_info["addtion"]);
        }
        $addition["mail_doc_conv"] = $is_active;
        $data["addition"] = serialize($addition);
        $ret = $this->userObj->update($data, $where);
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
        }
        $this->render_list();
    }
    
    function action_set_extend_bandwith(){
        $data = array();
        $where = sprintf("room_key = '%s'", $this->request->get("room_key"));
        $data["use_extend_bandwidth"] = $this->request->get("use_extend_bandwidth");
        //max_user_bandwidth?
        if($data["use_extend_bandwidth"]) {
            $data["max_user_bandwidth"] = "384";
        } else {
          $data["max_user_bandwidth"] = "256";
        }
        $data["room_updatetime"] = date("Y-m-d H:i:s");
        $this->logger2->info($data);
        require_once 'classes/dbi/room.dbi.php';
        $obj_Room = new RoomTable($this->get_dsn());
        $this->logger2->info($where);
        $ret = $obj_Room->update($data, $where);
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
            echo json_encode(false);
            return "";
        }
        echo json_encode(true);
        return "";
    }

    function set_session($data) {
        $this->session->set("form_data", $data, $this->_name_space);
    }

    function get_session() {
        return $this->session->get("form_data", $this->_name_space);
    }

    function clear_session() {
        $this->session->remove("form_data", $this->_name_space);
    }

    function check($data) {
        return $this->mailDocConvObj->check($data);
    }

    function action_set_all() {
        $document_ext = $this->config->getAll('DOCUMENT_FILES');
        $image_ext    = $this->config->getAll('DOCUMENT_IMAGES');
        $room_info["whiteboard_filetype"] = serialize(array(
            "document"  => array_keys($document_ext),
            "image"     => array_keys($image_ext)
            ));
        // 初期化
        if (!$room_info["cabinet"])                     $room_info["cabinet"] = 1;
        if (!$room_info["cabinet_size"])                $room_info["cabinet_size"] = 20;
        if (!$room_info["mfp"])                         $room_info["mfp"] = "all";
        if (!$room_info["default_layout_4to3"])         $room_info["default_layout_4to3"] = 'normal';
        if (!$room_info["default_layout_16to9"])        $room_info["default_layout_16to9"] = 'small';
        if (!$room_info["whiteboard_page"])             $room_info["whiteboard_page"] = 100;
        if (!$room_info["whiteboard_size"])             $room_info["whiteboard_size"] = 20;
        if (!$room_info["default_microphone_mute"])     $room_info["default_microphone_mute"] = "off";
        if (!$room_info["default_camera_mute"])         $room_info["default_camera_mute"] = "off";
        if (!$room_info["default_h264_use_flg"])        $room_info["default_h264_use_flg"] = 1;
        if (!$room_info["default_agc_use_flg"])         $room_info["default_agc_use_flg"] = 1;
        if (!$room_info["is_auto_transceiver"])         $room_info["is_auto_transceiver"] = 0;
        if (!$room_info["transceiver_number"])          $room_info["transceiver_number"] = 11;
        if (!$room_info["is_auto_voice_priority"])      $room_info["is_auto_voice_priority"] = 0;
        if (!$room_info["is_netspeed_check"])           $room_info["is_netspeed_check"] = "";
        if (!$room_info["is_wb_no"])                    $room_info["is_wb_no"] = "1";
        if (!$room_info["is_device_skip"])              $room_info["is_device_skip"] = "0";
        if (!$room_info["is_participant_notifier"])     $room_info["is_participant_notifier"] = "1";
        if (!$room_info["audience_chat_flg"])           $room_info["audience_chat_flg"] = "0";
        if (!$room_info["disable_rec_flg"])             $room_info["disable_rec_flg"] = "0";
        if (!$room_info["is_personal_wb"])              $room_info["is_personal_wb"] = "1";
        if (!$room_info["is_convert_wb_to_pdf"])        $room_info["is_convert_wb_to_pdf"] = "1";
        if (!$room_info["rec_gw_convert_type"])         $room_info["rec_gw_convert_type"] = "mov";
        if (!$room_info["rec_gw_userpage_setting_flg"]) $room_info["rec_gw_userpage_setting_flg"] = "0";

        if (!$room_info["use_teleconf"])            $room_info["use_teleconf"]       = "1";
        if (!$room_info["use_pgi_dialin"])          $room_info["use_pgi_dialin"]     = "1";
        if (!$room_info["use_pgi_dialin_free"])     $room_info["use_pgi_dialin_free"]= "1";
        if (!$room_info["use_pgi_dialout"])         $room_info["use_pgi_dialout"]    = "1";

        //sales
        if (!$room_info["customer_camera_use_flg"])             $room_info["customer_camera_use_flg"]    = "1";
        if (!$room_info["customer_camera_status"])              $room_info["customer_camera_status"]    = "1";
        if (!$room_info["customer_sharing_button_hide_status"]) $room_info["customer_sharing_button_hide_status"]    = "1";
        if (!$room_info["wb_allow_flg"])                        $room_info["wb_allow_flg"]    = "0";
        if (!$room_info["chat_allow_flg"])                      $room_info["chat_allow_flg"]    = "0";
        if (!$room_info["print_allow_flg"])                     $room_info["print_allow_flg"]    = "0";
        
        if (!$room_info["prohibit_extend_meeting_flg"])         $room_info["prohibit_extend_meeting_flg"]    = "0";
        
        $user_info = $this->session->get("user_info");
        if($this->session->get("service_mode") == "sales" || $user_info["meeting_version"] == null) {
          $room_info["default_layout_16to9"] = "normal";
        }
        $this->session->set("room_info", $room_info, $this->_name_space);
        return $this->render_set_form();
    }

    function render_set_form($err = null) {
        if (EZValidator::isError($err)) {
            $msg_format = $this->get_message("error");
            $msg        = $this->get_message("ERROR");
            $err_fields = $this->err_obj->error_fields();
            foreach ($err_fields as $key) {
                $type = $this->err_obj->get_error_type($key);
                $_msg = sprintf($msg[$type], $this->err_obj->get_error_rule($key, $type));
                $error[$key] = sprintf($msg_format, $_msg);
            }
            $this->logger->info(__FUNCTION__."#error", __FILE__, __LINE__, $error);
            $this->template->assign("error_info", $error);
        }
        $this->set_submit_key();
        $room_info = $this->session->get("room_info", $this->_name_space);
        $room_list = $this->get_room_info();
        $enable_teleconf = false;
        foreach ($room_list as $room_key => $room_detail) {
            if (@$room_detail["options"]["teleconference"]) {
                $enable_teleconf = true;
            }
            // 暗号化、TLSオプション無し
            if ($room_detail["options"]["meeting_ssl"] == 0) {
                $protocol_list = array(
                    "rtmp:1935"   => "rtmp:1935",
                    "rtmp:80"     => "rtmp:80",
                    "rtmp:8080"   => "rtmp:8080",
                    "rtmps-tls:443"   => "rtmps-tls:443",
                    "rtmps:443"   => "rtmps:443",
                    "rtmpt:80"    => "rtmpt:80",
                    "rtmpt:8080"  => "rtmpt:8080",
                );
            // 暗号化オプションあり
            } else {
                $protocol_list2 = array(
                    "rtmpe:1935"  => "rtmpe:1935",
                    "rtmpe:80"    => "rtmpe:80",
                    "rtmpe:8080"  => "rtmpe:8080",
                    "rtmps-tls:443"   => "rtmps-tls:443",
                    "rtmps:443"   => "rtmps:443",
                    "rtmpte:80"   => "rtmpte:80",
                    "rtmpte:8080" => "rtmpte:8080",
                );
            }
        }
        // 通常プロトコル
        $protocol_deny_list = array();
        $protocol_allow_list = array();
        $list = array();
        if ($protocol_list) {
            if (!$room_info["rtmp_protocol"]) {
                $protocol_allow_list = $protocol_list;
            } else {
                $list = split(",", $room_info["rtmp_protocol"]);
                foreach($list as $protocol) {
                    if (array_key_exists($protocol, $protocol_list)) {
                        $protocol_allow_list[$protocol] = $protocol_list[$protocol];
                    }
                }
            }
            foreach($protocol_list as $key => $value) {
                if (!array_key_exists($key, $protocol_allow_list)) {
                    $protocol_deny_list[$key] = $value;
                }
            }
        }
        // 暗号化プロトコル
        $protocol_deny_list2 = array();
        $protocol_allow_list2 = array();
        if ($protocol_list2) {
            $list = array();
            if (!$room_info["rtmp_protocol"]) {
                $protocol_allow_list2 = $protocol_list2;
            } else {
                $list = split(",", $room_info["rtmp_protocol"]);
                foreach($list as $protocol) {
                    if (array_key_exists($protocol, $protocol_list2)) {
                        $protocol_allow_list2[$protocol] = $protocol_list2[$protocol];
                    }
                }
            }
            foreach($protocol_list2 as $key => $value) {
                if (!array_key_exists($key, $protocol_allow_list2)) {
                    $protocol_deny_list2[$key] = $value;
                }
            }
        }
        $document_ext = $this->config->getAll('DOCUMENT_FILES');
        $image_ext    = $this->config->getAll('DOCUMENT_IMAGES');
        if ($room_info["whiteboard_filetype"]) {
            $filetype = unserialize($room_info["whiteboard_filetype"]);
            $room_info["whiteboard_filetype"] = array();
            if ($filetype["document"]) {
                foreach($filetype["document"] as $val) {
                    foreach($document_ext as $type => $extensions) {
                        $_extensions = split(",", $extensions);
                        // 登録形式を、拡張子 -> タイプ に変更し、既存データとの整合性確保のため
                        if (in_array($val, $_extensions) || $val == $type) {
                            $room_info["whiteboard_filetype"][$type] = 1;
                        }
                    }
                }
            }
            if ($filetype["image"]) {
                foreach($filetype["image"] as $val) {
                    foreach($image_ext as $type => $extensions) {
                        $_extensions = split(",", $extensions);
                        // 登録形式を、拡張子 -> タイプ に変更し、既存データとの整合性確保のため
                        if (in_array($val, $_extensions) || $val == $type) {
                            $room_info["whiteboard_filetype"][$type] = 1;
                        }
                    }
                }
            }
        }
        if ($room_info["cabinet_filetype"]) {
            $room_info["cabinet_filetype"] = unserialize($room_info["cabinet_filetype"]);
        }
        $net_speed_list = $this->get_message("NET_SPEED_LIST");
        foreach($net_speed_list as $key => $val) {
            $net_speed_list[$key] = substr($val, strpos($val, ":") + 1);
        }
        $user_info = $this->session->get("user_info");
        $this->template->assign(array("image_files"           => $image_ext,
                                      "document_files"        => $document_ext,
                                      "net_speed_list"        => $net_speed_list,
                                      "room_info"             => $room_info,
                                      "is_trial"              => $user_info['user_status'] == 2,
                                      "protocol_list"         => $protocol_list,
                                      "protocol_deny_list"    => $protocol_deny_list,
                                      "protocol_allow_list"   => $protocol_allow_list,
                                      "protocol_list2"        => $protocol_list2,
                                      "protocol_deny_list2"   => $protocol_deny_list2,
                                      "protocol_allow_list2"  => $protocol_allow_list2,
                                      "is_set_all"            => true,
                                      "enable_teleconf"       => $enable_teleconf, ));
        $this->template->assign("all_edit" , 1);
        $this->display('admin/room/all.t.html');
    }


    function action_edit_all() {
        // 二重処理チェック
        if (!$this->check_submit_key()) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->render_set_form();
        }
        $user_info = $this->session->get( "user_info" );
        $request = $this->request->getAll();
        require_once 'classes/dbi/room.dbi.php';
        $obj_Room = new RoomTable($this->get_dsn());
        if ($request["cabinet_filetype"]) {
            foreach ($request["cabinet_filetype"] as $extension) {
                $extensions[] = strtolower($extension);
            }
            $request["cabinet_filetype"] = $extensions;
        }
        $data = array(
            "cabinet"                     => $request["cabinet"],
            "cabinet_size"                => $request["cabinet_size"],
            "cabinet_filetype"            => serialize($request["cabinet_filetype"]),
            "mfp"                         => $request["mfp"],
            "default_layout_4to3"         => $request["default_layout_4to3"],
            "default_layout_16to9"        => $request["default_layout_16to9"],
            "whiteboard_page"             => $request["whiteboard_page"],
            "whiteboard_size"             => $request["whiteboard_size"],
            "whiteboard_filetype"         => serialize($request["whiteboard_filetype"]),
            "default_microphone_mute"     => $request["default_microphone_mute"],
            "default_camera_mute"         => $request["default_camera_mute"],
            "default_agc_use_flg"         => $request["default_agc_use_flg"],
            "is_auto_transceiver"         => $request["is_auto_transceiver"],
            "transceiver_number"          => $request["transceiver_number"],
            "is_auto_voice_priority"      => $request["is_auto_voice_priority"],
            "is_netspeed_check"           => $request["is_netspeed_check"],
            "is_wb_no"                    => $request["is_wb_no"],
            "is_device_skip"              => $request["is_device_skip"],
            "is_participant_notifier"     => $request["is_participant_notifier"],
          "audience_chat_flg"           => $request["audience_chat_flg"],
            "disable_rec_flg"             => $request["disable_rec_flg"],
            "is_personal_wb"              => $request["is_personal_wb"],
            "is_convert_wb_to_pdf"        => $request["is_convert_wb_to_pdf"],
            "rec_gw_userpage_setting_flg" => $request["rec_gw_userpage_setting_flg"],
            "customer_camera_use_flg"     => $request["customer_camera_use_flg"],
            "customer_camera_status"      => $request["customer_camera_status"],
            "customer_sharing_button_hide_status" => $request["customer_sharing_button_hide_status"],
            "wb_allow_flg"                => $request["wb_allow_flg"],
            "chat_allow_flg"              => $request["chat_allow_flg"],
            "print_allow_flg"             => $request["print_allow_flg"],
            "prohibit_extend_meeting_flg" => $request["prohibit_extend_meeting_flg"],
            );
        if (!$this->config->get("IGNORE_MENU", "teleconference")) {
            if ($request['use_teleconf']) {
                $data['use_teleconf']        = 1;
                $data['use_pgi_dialin']      = $request['use_pgi_dialin']      ? 1: 0;
                $data['use_pgi_dialin_free'] = $request['use_pgi_dialin_free'] ? 1: 0;
                $data['use_pgi_dialout']     = $request['use_pgi_dialout']     ? 1: 0;
            } else {
                $data['use_teleconf']        = 0;
                $data['use_pgi_dialin']      = 0;
                $data['use_pgi_dialin_free'] = 0;
                $data['use_pgi_dialout']     = 0;
            }
        }
        $rules = array("cabinet" => array("required" => true,
                                          "allow"    => array("0", "1")),
                       "mfp"     => array("required" => true,
                                          "allow"    => array("all", "select", "deny")),);
        $this->err_obj = $obj_Room->check($data, $rules);
        if ($request["cabinet_filetype"]) {
            foreach($request["cabinet_filetype"] as $ext) {
                if (ereg("[^0-9a-z]", $ext)) {
                    $this->err_obj->set_error("cabinet_filetype", "alnum", $ext);
                    break;
                }
            }
        }
        if (!$this->config->get("IGNORE_MENU", "teleconference")) {
            if ($data['use_teleconf'] && !$data['use_pgi_dialin']
             && !$data['use_pgi_dialin_free'] && !$data['use_pgi_dialout']) {
                $this->err_obj->set_error("teleconf", "minitem", 1);
            }
            foreach (array('use_teleconf', 'use_pgi_dialin', 'use_pgi_dialin_free', 'use_pgi_dialout') as $col) {
               $teleconf_data[$col] = $data[$col];
               unset($data[$col]);
            }
        }
        // エラー
        if (EZValidator::isError($this->err_obj)) {
            $room_info = $this->session->get("room_info", $this->_name_space);
            $this->session->set("room_info", $data, $this->_name_space);
            return $this->render_set_form($this->err_obj);
        }
        $room_list = $this->get_room_info();

        foreach ($room_list as $room_key => $room_info) {
            if ($room_info["options"]["meeting_ssl"]) {
                $data["rtmp_protocol"] = $request["rtmp_protocol2"];
            } else {
                $data["rtmp_protocol"] = $request["rtmp_protocol"];
            }
            $where = sprintf( "room_key='%s' AND user_key='%s'", $room_key, $user_info["user_key"] );
            $this->logger2->info(array($data, $where));
            if ($room_info["options"]["teleconference"] && !$this->config->get("IGNORE_MENU", "teleconference")) {
                $obj_Room->update(array_merge($data, $teleconf_data), $where);
            } else {
                $obj_Room->update($data, $where);
            }
        }

        // セッションの部屋情報を更新
        $rooms = $this->get_room_list(true);
        // 操作ログ
        $this->add_operation_log('setup_room');
        header("Location: /services/admin/?action_room_plan&setting=1");
    }

    function action_video_conference_setting($room_key = "", $message = "") {
        $user_info = $this->session->get("user_info");
        $room_key = $room_key ? $room_key : $this->request->get("room_key");
        
        require_once("classes/dbi/room.dbi.php");
        $obj_Room = new RoomTable($this->get_dsn());
        $room_info = $obj_Room->getRow(sprintf("user_key = %s AND room_key='%s'", $user_info["user_key"], $room_key), "room_key, room_name");
        
        require_once("classes/mcu/resolve/Resolver.php");
        if (($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($room_key))) {
            $ives_setting = $videoConferenceClass->getIvesInfoByRoomKey($room_key);
            $ives_setting["video_conference_address"] = $videoConferenceClass->getRegularAddress($ives_setting["ives_did"]);
        }
        $this->logger2->info($ives_setting);
        
        $this->template->assign("room_info", $room_info);
        $this->template->assign("ives_setting", $ives_setting);
        $this->template->assign("message", $message);
        // 登録一覧
        $this->display('admin/room/video_conference/index.t.html');
    }
    
    function action_edit_video_conference_setting() {
        require_once("classes/mgm/dbi/mcu_server.dbi.php");
        require_once("classes/mcu/config/McuConfigProxy.php");
        
        $user_info = $this->session->get("user_info");
        $room_key = $this->request->get("room_key");
        
        if (!$room_key) {
            $this->action_video_conference_setting();
            exit;
        }
        require_once("classes/mcu/resolve/Resolver.php");
        if (($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($room_key))) {
            $ives_setting = $videoConferenceClass->getIvesInfoByRoomKey($room_key);
            $ives_setting["video_conference_address"] = $videoConferenceClass->getRegularAddress($ives_setting["ives_did"]);
        }
        require_once("classes/dbi/ives_setting.dbi.php");
        $obj_ivesSetting = new IvesSettingTable($this->get_dsn());
        $update_where = "ives_setting_key = " . $ives_setting["ives_setting_key"] . " AND is_deleted = 0";
        $data = array();
        //会議中、予約があった場合は変更できないようにする
        require_once 'classes/dbi/room.dbi.php';
        $obj_Room = new RoomTable($this->get_dsn());
        $where = "room_key = '".addslashes($room_key)."'";
        $room_info = $obj_Room->getRow($where, "room_key, meeting_key");
        
        require_once( "classes/dbi/meeting.dbi.php" );
        $obj_Meeting = new MeetingTable($this->get_dsn());
        $where = "meeting_ticket = '".$room_info["meeting_key"]."'".
            " AND is_active = 1";
        $rowNowMeeting = $obj_Meeting->getRow($where);
        
        require_once ("classes/dbi/reservation.dbi.php");
        $obj_Reservation = new ReservationTable($this->get_dsn());
        $where = "room_key = '".addslashes($room_key)."'".
                 " AND reservation_status = 1".
                 " AND (reservation_starttime > '".date("Y-m-d H:i:s")."' OR reservation_endtime > '".date("Y-m-d H:i:s")."')";
        $reservation_count = $obj_Reservation->numRows($where);
        if ($reservation_count > 0 || $rowNowMeeting) {
            $this->action_video_conference_setting($room_key, "reservation_error");
            exit;
        }
        if ($ives_setting["ignore_video_conference_address"] == 1) {
            $data = array("ignore_video_conference_address" => 0);
        } else {
            //Did再生成して非表示状態にする
            $mcu_db = new McuServerTable($this->get_auth_dsn());
            $mcu_host = $mcu_db->getActiveServerAddress($ives_setting["mcu_server_key"]);
            if (!$mcu_host) {
                $this->logger2->error("get mcu server host error");
                $this->action_video_conference_setting($room_key, "error");
                exit;
            }
            $vad = ($ives_setting["use_active_speaker"] == 1) ? true : false;
            require_once("classes/polycom/Polycom.class.php");
            require_once("classes/dbi/operation_log.dbi.php");
            $polycom = new PolycomClass($this->get_dsn());
            $polycom->setWsdlDomain($mcu_host);
            $polycom->removeAdhocConferenceTemplate($ives_setting["ives_did"]);
            $polycom->setWsdlDomain($mcu_host);
            $did = $polycom->createDid();
            
            require_once("classes/mgm/dbi/media_mixer.dbi.php");
            $media_db     = new MediaMixerTable($this->get_auth_dsn());
            $where   	= "media_mixer_key = ".addslashes($ives_setting["media_mixer_key"]);
            $media_mixer_info = $media_db->getRow($where);
            $media_mixer_id = $media_mixer_info["media_name"]."@".$media_mixer_info["url"];
            $polycom->createAdhocConferenceTemplate($did, $vad, $media_mixer_id);
            $this->logger2->info(array("room_key" => $room_key, "old_did" => $ives_setting["ives_did"], "new_did" => $did));
            $objOperationLog    = new OperationLogTable($this->get_dsn());
            $opration_log = array(
                "user_key"              => $user_info["user_key"],
                "remote_addr"           => $_SERVER["REMOTE_ADDR"],
                "action_name"           => 'change_ives_did',
                "operation_datetime"    => date("Y-m-d H:i:s"),
                "info"                  => serialize(array(
                    "room_key"          => $room_key,
                    "old_ives_did"      => $ives_setting["ives_did"],
                    "new_ives_did"      => $did,
                    ))
                );
            $objOperationLog->add($opration_log);
            $data = array("ives_did" => $did,
                          "ignore_video_conference_address" => 1);
        }
        $ives_setting = $obj_ivesSetting->update($data, $update_where);
        $this->action_video_conference_setting($room_key, "complete");
    }
}

$main = new AppMember();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
