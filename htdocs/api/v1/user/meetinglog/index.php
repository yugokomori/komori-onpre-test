<?php
/*
 * Created on 2008/02/06
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once("classes/N2MY_Api.class.php");
require_once("classes/N2MY_MeetingLog.class.php");
require_once("config/config.inc.php");
require_once ("lib/EZLib/EZUtil/EZDate.class.php");

class N2MY_MeetingLog_API extends N2MY_Api
{
    var $session_id = null;
    var $meetingLog = null;
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        // SSL対応
        header('Pragma:');
    }

    /**
     * 認証処理
     *
     * @param
     * @return
     */
    function auth()
    {
        $this->checkAuthorization();
        $this->meetingLog = new N2MY_MeetingLog($this->get_dsn());
    }

    function action_meeting_log(){
        $where_is_member = "";
        $member_info = $this->session->get( "member_info" );
        if(!$member_info){
            return $this->display_error("100", "NOT MEMBER");
        }
        $user_info = $this->session->get( "user_info");
        $_wk_start_limit = $this->request->get( "start_limit" );
        $_wk_end_limit = $this->request->get( "end_limit" );
        // タイムスタンプ変換
        $server_time_zone = $this->config->get("GLOBAL", "time_zone", N2MY_SERVER_TIMEZONE); // このサーバーのタイムゾーン
        $user_time_zone   = $this->session->get("time_zone"); // セッションの値から取得
        if ($_wk_start_limit) {
            if (is_numeric($_wk_start_limit)) {
                $_ts_local = $_wk_start_limit;
            } else {
                $_ts_local = EZDate::getLocateTime($_wk_start_limit, $user_time_zone, $server_time_zone);
            }
            $start_limit = EZDate::getZoneDate("Y-m-d H:i:s", $user_time_zone, $_ts_local);
        }
        $this->logger2->debug(array($_wk_start_limit, $user_time_zone, $server_time_zone, $start_limit, date("Y-m-d H:i:s", $_ts_local)));
        if ($_wk_end_limit) {
            if (is_numeric($_wk_end_limit)) {
                $_ts_local = $_wk_end_limit;
            } else {
                $_ts_local = EZDate::getLocateTime($_wk_end_limit, $user_time_zone, $server_time_zone);
            }
            $end_limit = EZDate::getZoneDate("Y-m-d H:i:s", $user_time_zone, $_ts_local);
        }

        $param = array(
            "member_key" => $member_info["member_key"],
            "user_key" => $member_info["user_key"],
            "start_time"=> $start_limit,
            "end_time"=> $end_limit,
        );
        $meeting_list = $this->meetingLog->getMyMeetingList($param);
//$this->logger2->info($this->meetingLog->getAdminmeetingLog($param));
        $log_data = array();

        foreach($meeting_list as $meeting_info){
            $log = false;
            $log = $this->meetingLog->getLog($member_info , $meeting_info);
            if($log){
                $log_data = array_merge($log_data , $log);
            }
        }

//$this->logger2->info($meeting_list);
        // ログリストを取得
        $log_count = count($log_data);
        // mcu_down_flgがある場合は除外する
        for($i = 0; $i < $log_count; $i++){
            if($log_data[$i]["mcu_down_flg"] == 1){
                unset($log_data[$i]);
            }

        }
//$this->logger2->info($log_data);

        return $this->output($log_data);
    }

    /**
     * 会議記録一覧（検索）
     *
     * @param string room_key ルームキー
     * @param string meeting_name 会議名 (デフォルト null)
     * @param string user_name 参加者名 (デフォルト null)
     * @param datetime start_limit 検索開始日 (デフォルト null)
     * @param datetime end_limit 検索終了日 (デフォルト null)
     * @param int offset 開始件数 (デフォルト null)
     * @param int limit 表示件数 (デフォルト null)
     * @param string sort_key ソート基準 (デフォルト 会議開始時間)
     * @param string sort_type 降順、昇順 (デフォルト 昇順)
     * @return $string $output 会議キー、会議名、日時、参加者、サイズ、映像有無、議事録有無
     */
    function action_get_list()
    {
        $room_key        = $this->request->get(API_ROOM_ID);
        $meeting_name    = htmlspecialchars($this->request->get("meeting_name"));
        $user_name       = htmlspecialchars($this->request->get("user_name"));
        $_wk_start_limit = $this->request->get("start_limit");
        $_wk_end_limit   = $this->request->get("end_limit");
        $page            = $this->request->get("page", 1);
        $limit           = $this->request->get("limit", N2MY_API_GET_LIST_LIMIT);
        $sort_key        = $this->request->get("sort_key");
        $sort_type       = $this->request->get("sort_type");
        $output_type     = $this->request->get("output_type");
        $is_publish      = $this->request->get("is_publish");
        $is_reserved     = $this->request->get("is_reserved");
        $is_locked       = $this->request->get("is_locked");
        $has_minutes     = $this->request->get("has_minutes");
        $has_video       = $this->request->get("has_video");

        $status = 0;

        // タイムスタンプ変換
        $server_time_zone = $this->config->get("GLOBAL", "time_zone", N2MY_SERVER_TIMEZONE); // このサーバーのタイムゾーン
        $user_time_zone   = $this->session->get("time_zone"); // セッションの値から取得
        if ($_wk_start_limit) {
            if (is_numeric($_wk_start_limit)) {
                $_ts_local = $_wk_start_limit;
            } else {
                $_ts_local = EZDate::getLocateTime($_wk_start_limit, $user_time_zone, $server_time_zone);
            }
            $start_limit = EZDate::getZoneDate("Y-m-d H:i:s", $user_time_zone, $_ts_local);
        }
        $this->logger2->debug(array($_wk_start_limit, $user_time_zone, $server_time_zone, $start_limit, date("Y-m-d H:i:s", $_ts_local)));
        if ($_wk_end_limit) {
            if (is_numeric($_wk_end_limit)) {
                $_ts_local = $_wk_end_limit;
            } else {
                $_ts_local = EZDate::getLocateTime($_wk_end_limit, $user_time_zone, $server_time_zone);
            }
            $end_limit = EZDate::getZoneDate("Y-m-d H:i:s", $user_time_zone, $_ts_local);
        }
        $this->logger2->debug(array($_wk_end_limit, $user_time_zone, $server_time_zone, $end_limit, date("Y-m-d H:i:s", $_ts_local)));
        $user_info = $this->session->get("user_info");
        $offset       = ($limit * ($page - 1));
        $room_info = $this->session->get("room_info");
        // チェックルール編集
        $rules = array(
            API_ROOM_ID => array(
                "allow" => array_keys($room_info),
                ),
//            "title"     => array(
//                "maxlen"   => 64,
//                ),
//            "participant"     => array(
//                "maxlen"   => 64,
//                ),
            "start_datetime"     => array(
                "datetime"   => true,
                ),
            "end_datetime"     => array(
                "datetime"   => true,
                ),
            "limit"     => array(
                "numeric"   => true,
                ),
            "page"     => array(
                "numeric"   => true,
                ),
            "sort_key" => array(
                "allow" => array("meeting_name","meeting_size_used","meeting_start_date","meeting_end_date","meeting_use_minute")
                ),
            "sort_type" => array(
                "allow" => array("asc", "desc")
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        // チェックデータ編集
        $chack_data = array(
            "room_key"         => $room_key,
//            "title"             => $meeting_name,
//            "participant"       => $user_name,
            "start_datetime"    => $start_limit,
            "end_datetime"      => $end_limit,
            "limit"             => $limit,
            "page"              => $page,
            "sort_key"          => $sort_key,
            "sort_type"         => $sort_type,
            "output_type"       => $output_type,
        );
        // チェック
        $err_obj = $this->error_check($chack_data, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        switch ($sort_key) {
            case "meeting_start_date":
                $sort_key = "actual_start_datetime";
                break;
            case "meeting_end_date":
                $sort_key = "actual_end_datetime";
                break;
        }
        $query = array(
            "user_key"          => $user_info["user_key"],
            "room_key"          => $room_key,
            "title"             => $meeting_name,
            "participant"       => $user_name,
            "start_datetime"    => $start_limit,
            "end_datetime"      => $end_limit,
            "limit"             => $limit,
            "offset"            => $offset,
            "sort_key"          => $sort_key,
            "sort_type"         => $sort_type,
            "is_publish"        => $is_publish,
            "is_reserved"       => $is_reserved,
            "is_locked"         => $is_locked,
            "has_minutes"       => $has_minutes,
            "has_video"         => $has_video,
        );
        $list = $this->meetingLog->getList($query);

        foreach ($list as $_key => $_val) {
            $lists[$_key] = array(
                 API_ROOM_ID                => $list[$_key]['room_key'],
                 API_MEETING_ID             => $list[$_key]['meeting_session_id'],
                 'meeting_name'             => $list[$_key]['meeting_name'],
                 'meeting_size_used'        => $list[$_key]['meeting_size_used'],
                 'meeting_log_password'     => $list[$_key]['meeting_log_password'] = $list[$_key]['meeting_log_password'] ? 1 : 0,
                 'is_locked'                => $list[$_key]['is_locked'],
                 'is_reserved'              => $list[$_key]['is_reserved'],
                 'is_publish'               => $list[$_key]['is_publish'],
                 API_MEETINGLOG_MINUTES     => $list[$_key]['has_recorded_minutes'],
                 API_MEETINGLOG_VIDEO       => $list[$_key]['has_recorded_video'],
                 'meeting_start_date'       => strtotime($list[$_key]['actual_start_datetime']),
                 'meeting_end_date'         => strtotime($list[$_key]['actual_end_datetime']),
                 'meeting_use_minute'       => $list[$_key]['meeting_use_minute'],
                 'eco_move'                 => $list[$_key]['eco_move'],
                 'eco_time'                 => $list[$_key]['eco_time'],
                 'eco_fare'                 => $list[$_key]['eco_fare'],
                 'eco_co2'                  => $list[$_key]['eco_co2'],
              );
            foreach ($_val['participants'] as $_key_p => $_val_p) {
                   $lists[$_key]['participants'][$_key_p] = array(
                           'participant_name'   => $list[$_key]['participants'][$_key_p]['participant_name'],
                           API_PARTICIPANT_ID   => $list[$_key]['participants'][$_key_p]['participant_session_id'],
                           'use_count'          => $list[$_key]['participants'][$_key_p]['use_count'],
                           'participant_type'   => $list[$_key]['participants'][$_key_p]['participant_type'] = $list[$_key]['participants'][$_key_p]['participant_type_name']
                   );

            }


            // meeting_sequences
            foreach ($_val['meeting_sequences'] as $_key_ms => $val_ms) {
                      $lists[$_key]['meeting_sequences'][$_key_ms] = array(
                            'meeting_sequence_key'    => $list[$_key]['meeting_sequences'][$_key_ms]['meeting_sequence_key'],
                            'meeting_size_used'       => $list[$_key]['meeting_sequences'][$_key_ms]['meeting_size_used'],
                            API_MEETINGLOG_MINUTES    => $list[$_key]['meeting_sequences'][$_key_ms]['has_recorded_minutes'],
                            API_MEETINGLOG_VIDEO      => $list[$_key]['meeting_sequences'][$_key_ms]['has_recorded_video']
                       );

            }
        }

        $count = $this->meetingLog->getCount($query);
        $data = array(
            "count" => $count,
            "meetinglogs"  => array("meetinglog" => $lists),
            );
        return $this->output($data);
    }

    function action_get_detail() {

        require_once("classes/dbi/file_cabinet.dbi.php");
        $obj_Meeting = new MeetingTable( $this->get_dsn() );
        $objParticipant = new DBI_Participant( $this->get_dsn() );
        $objFileCabinet = new FileCabinetTable( $this->get_dsn() );
        $objMeetingSequence = new DBI_MeetingSequence( $this->get_dsn() );

        $meeting_session_id = $this->request->get(API_MEETING_ID);
        $meeting_pw = $this->request->get("password");
        $user_info = $this->session->get("user_info");

        // チェックルール編集
        $rules = array(
            "output_type"     => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        // チェックデータ（リクエスト）編集
        $request = array(
            "output_type" => $this->request->get("output_type", null),
            );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        $where = "meeting_session_id = '".addslashes($meeting_session_id)."'";
        if (!$meetingInfo = $obj_Meeting->getRow($where)) {
            $err_obj->set_error(API_MEETING_ID, "MEETING_LOG_INVALID");
        } else {
            // ログインユーザ以外の予約
            if ($meetingInfo["user_key"] != $user_info["user_key"]) {
                $err_obj->set_error(API_MEETING_ID, "MEETING_LOG_INVALID");
            }
        }
        /* 映像、議事録再生時にパスワードが聞かれるのでもんだない？
        if ($meetingInfo["meeting_log_password"]) {
            if($meeting_pw != $meetingInfo["meeting_log_password"]) {
                $err_obj->set_error("password", "pw_equal");
            }
        }
        */
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
//        $meetingInfo["meeting_size_used"] = round($meetingInfo["meeting_size_used"] / (1024 * 1024));

        $meetingInfo["participantList"] = $objParticipant->getList($meetingInfo["meeting_key"], false);

        $_server_time_zone = $this->config->get("GLOBAL", "time_zone", N2MY_SERVER_TIMEZONE);

        $meeting_info = array(
            API_ROOM_ID                 => $meetingInfo["room_key"],
            API_MEETING_ID              => $meetingInfo["meeting_session_id"],
            'meeting_name'              => $meetingInfo["meeting_name"],
            'meeting_size_used'         => round($meetingInfo["meeting_size_used"] / (1024 * 1024)),
            'meeting_log_password'      => $meetingInfo["meeting_log_password"] ? 1 : 0 ,
            'is_locked'                 => $meetingInfo["is_locked"],
            'is_reserved'               => $meetingInfo["is_reserved"],
            'is_publish'                => $meetingInfo["is_publish"],
            API_MEETINGLOG_MINUTES      => $meetingInfo["has_recorded_minutes"],
            API_MEETINGLOG_VIDEO        => $meetingInfo["has_recorded_video"],
            'meeting_start_date'        => strtotime($meetingInfo["actual_start_datetime"]),
            'meeting_end_date'          => strtotime($meetingInfo["actual_end_datetime"]),
            'meeting_use_minute'        => $meetingInfo["meeting_use_minute"],
            'eco_move'                  => $meetingInfo["eco_move"],
            'eco_time'                  => $meetingInfo["eco_time"],
            'eco_fare'                  => $meetingInfo["eco_fare"],
            'eco_co2'                   => $meetingInfo["eco_co2"]
          );
        //アンケートダウンロードURL
        if($meetingInfo["has_quick_vote"]) {
            $meeting_info["quick_vote_url"] = "/services/meetinglog/?action_download_vote_csv=&meeting_key=".$meetingInfo["meeting_key"];
        } else {
            $meeting_info["quick_vote_url"] = "";
        }
        //共有メモダウンロード
        if($meetingInfo["has_shared_memo"] == 1){
            require_once("classes/dbi/shared_file.dbi.php");
            $objSharedFile = new sharedFileTable($this->get_dsn());
            $shared_where = sprintf("status = 1 AND meeting_key='%s'", $meetingInfo["meeting_key"]);
            $sharedFileList = $objSharedFile->getRowsAssoc($shared_where);
            $this->logger2->info($sharedFileList);
            foreach ($sharedFileList as $key => $sharedmemo) {
                $meeting_info["shared_memos"]["shared_memo"][$key]["shared_memo_url"] = "/services/meetinglog/?action_get_memo=&meeting_key=".$meetingInfo["meeting_key"]."&shared_file_key=".$sharedmemo["shared_file_key"];
            }
        }else {
            $meeting_info["shared_memo"] = "";
        }
        //$this->logger2->info($meeting_info);
          //$meetingInfo = $meeting_info;

        foreach($meetingInfo["participantList"] as $p_key => $participant) {
             $meeting_info["participants"]["participant"][$p_key] = array(
                API_PARTICIPANT_ID           => $participant["participant_session_id"],
                'participant_name'           => $participant["participant_name"],
                'participant_email'          => $participant["participant_email"],
                'participant_station'        => $participant["participant_station"],
                'use_count'                  => $participant["use_count"],
                'mode'                       => $participant["participant_mode_name"],
                'type'                       => $participant["participant_type_name"],
                API_PARTICIPANT_START        => strtotime($participant["uptime_start"]),
                API_PARTICIPANT_END          => strtotime($participant["uptime_end"])
            );
        }
//            unset($participant['participant_key']);
//            unset($participant['meeting_key']);
//            unset($participant['member_key']);
//            unset($participant['participant_mode_key']);
//            unset($participant['participant_type_key']);
//            unset($participant['participant_role_key']);
//            unset($participant['participant_skin_type']);
//            unset($participant['participant_role_name']);
//            unset($participant['participant_locale']);
//            unset($participant['is_active']);
//            unset($participant['is_narrowband']);
//            unset($participant['is_blocked']);
//            unset($participant['create_datetime']);
//            unset($participant['update_datetime']);

//            $participant['mode'] = $participant['participant_mode_name'];
//            unset($participant['participant_mode_name']);
//            $participant['type'] = $participant['participant_type_name'];
//            unset($participant['participant_type_name']);
//            $participant[API_PARTICIPANT_ID] = $participant['participant_session_id'];
//            unset($participant['participant_session_id']);
//            $participant[API_PARTICIPANT_START] = strtotime($participant['uptime_start']);
//            unset($participant['uptime_start']);
//            $participant[API_PARTICIPANT_END] = strtotime($participant['uptime_end']);
//            unset($participant['uptime_end']);


        unset($meetingInfo["participantList"]);
        //$meeting_info["participants"]["participant"] = $participant_list;


        //sequeceList

        $where = "meeting_key = ".$meetingInfo["meeting_key"];
        $sequeceList = $objMeetingSequence->getRowsAssoc($where, array("meeting_sequence_key"=>"desc") );

        foreach ($sequeceList as $i => $sequece) {
            $videoURL = NULL;
            $minuteURL = NULL;
            if ($meetingInfo["is_publish"]) {
                require_once("lib/EZLib/EZUtil/EZEncrypt.class.php");
                if ($sequeceList[$i]["has_recorded_video"]) {
                    $data = sprintf("%s,log_video,%s,%s", $meetingInfo["meeting_key"], $this->session->get("lang"), $sequece["meeting_sequence_key"]);
                    $encrypted_data = EZEncrypt::encrypt(N2MY_ENCRYPT_KEY, N2MY_ENCRYPT_IV, $data);
                    $encrypted_data = str_replace("/", "-", $encrypted_data);
                    $encrypted_data = str_replace("+", "_", $encrypted_data);
                    $videoURL = sprintf( "%sondemand/%s", N2MY_BASE_URL, $encrypted_data);
                }
                if ($sequeceList[$i]["has_recorded_minutes"]) {
                    $data = sprintf("%s,log_minute,%s,%s", $meetingInfo["meeting_key"], $this->session->get("lang"), $sequece["meeting_sequence_key"]);
                    $encrypted_data = EZEncrypt::encrypt(N2MY_ENCRYPT_KEY, N2MY_ENCRYPT_IV, $data);
                    $encrypted_data = str_replace("/", "-", $encrypted_data);
                    $encrypted_data = str_replace("+", "_", $encrypted_data);
                    $minuteURL = sprintf( "%sminute/%s", N2MY_BASE_URL, $encrypted_data);
                }
            }

            $meeting_info["meeting_sequences"]["meeting_sequence"][$i] = array(
                'meeting_sequence_key'  => $sequeceList[$i]["meeting_sequence_key"],
                'meeting_size_used'     => $sequeceList[$i]["meeting_size_used"],
                'video_url'             => $videoURL,
                'minute_url'            => $minuteURL,
                API_MEETINGLOG_MINUTES   => $sequeceList[$i]["has_recorded_minutes"],
                API_MEETINGLOG_VIDEO     => $sequeceList[$i]["has_recorded_video"]
             );

//            unset($sequeceList[$i]["server_key"]);
//            unset($sequeceList[$i]["meeting_key"]);
//            unset($sequeceList[$i]["status"]);
//            unset($sequeceList[$i]["create_datetime"]);
//            unset($sequeceList[$i]["update_datetime"]);
//            $sequeceList[$i][API_MEETINGLOG_MINUTES] = $sequeceList[$i]["has_recorded_minutes"];
//            unset($sequeceList[$i]["has_recorded_minutes"]);
//            $sequeceList[$i][API_MEETINGLOG_VIDEO] = $sequeceList[$i]["has_recorded_video"];
//            unset($sequeceList[$i]["has_recorded_video"]);
        }

        //$meeting_info["meeting_sequences"]["meeting_sequence"] = $sequece_list;

        //cabinet
        $where = "meeting_key = '".$meetingInfo["meeting_key"]."'" .
                " AND status = 1";
        $cabinets = $objFileCabinet->getRowsAssoc( $where, array( "cabinet_id"=>"asc" ) );


        foreach($cabinets as $c_key => $val) {
            $meeting_info["cabinets"]["cabinet"][$c_key] = array(
                'cabinet_id'    => $cabinets[$c_key]["cabinet_id"],
                'size'          => $cabinets[$c_key]["size"],
                'file_name'     => $cabinets[$c_key]["file_name"]
             );

//            unset($cabinets[$key]["meeting_key"]);
//            unset($cabinets[$key]["tmp_name"]);
//            unset($cabinets[$key]["type"]);
//            unset($cabinets[$key]["file_cabinet_path"]);
//            unset($cabinets[$key]["status"]);
//            unset($cabinets[$key]["registtime"]);
//            unset($cabinets[$key]["updatetime"]);

        }

        //$meeting_info["cabinets"]["cabinet"] = $cabinet;


//        unset($meetingInfo['meeting_key']);
//        unset($meetingInfo['fms_path']);
//        unset($meetingInfo['layout_key']);
//        unset($meetingInfo['server_key']);
//        unset($meetingInfo['sharing_server_key']);
//        unset($meetingInfo['user_key']);
//        unset($meetingInfo['member_keys']);
//        unset($meetingInfo['participant_names']);
//        unset($meetingInfo['meeting_ticket']);
//        unset($meetingInfo['meeting_country']);
//        unset($meetingInfo['meeting_room_name']);
//        unset($meetingInfo['meeting_tag']);
//        unset($meetingInfo['meeting_max_audience']);
//        unset($meetingInfo['meeting_max_extendable']);
//        unset($meetingInfo['meeting_max_seat']);
//        unset($meetingInfo['meeting_size_recordable']);
//        unset($meetingInfo['meeting_size_uploadable']);
//        unset($meetingInfo['meeting_log_owner']);
//        unset($meetingInfo['meeting_start_datetime']);
//        unset($meetingInfo['meeting_stop_datetime']);
//        unset($meetingInfo['meeting_extended_counter']);
//        unset($meetingInfo['is_active']);
//        unset($meetingInfo['is_extended']);
//        unset($meetingInfo['is_blocked']);
//        unset($meetingInfo['version']);
//        unset($meetingInfo['create_datetime']);
//        unset($meetingInfo['update_datetime']);
//        unset($meetingInfo['room_addition']);
//        unset($meetingInfo['is_deleted']);
//        unset($meetingInfo['use_flg']);
        //$meetingInfo['eco_info'] = ($meetingInfo['eco_info']) ? unserialize($meetingInfo['eco_info']) : null;
//        unset($meetingInfo['eco_info']);

//        $meetingInfo[API_MEETINGLOG_MINUTES] = $meetingInfo["has_recorded_minutes"];
//        unset($meetingInfo["has_recorded_minutes"]);
//        $meetingInfo[API_MEETINGLOG_VIDEO] = $meetingInfo["has_recorded_video"];
//        unset($meetingInfo["has_recorded_video"]);
//        $meetingInfo[API_ROOM_ID] = $meetingInfo["room_key"];
//        unset($meetingInfo["room_key"]);
//        $meetingInfo[API_MEETING_ID] = $meetingInfo["meeting_session_id"];
//        unset($meetingInfo["meeting_session_id"]);

        // サーバーのタイムゾーン取得
//        $_server_time_zone = $this->config->get("GLOBAL", "time_zone", N2MY_SERVER_TIMEZONE);
//        $meetingInfo['meeting_start_date'] = strtotime($meetingInfo['actual_start_datetime']);
//        unset($meetingInfo['actual_start_datetime']);
//        $meetingInfo['meeting_end_date'] = strtotime($meetingInfo['actual_end_datetime']);
//        unset($meetingInfo['actual_end_datetime']);

        return $this->output($meeting_info);
    }

    /**
     * 会議記録（ログ自体）削除
     *
     * @param string room_key ルームキー
     * @param string meeting_key 会議キー
     * @return boolean
     */
    function action_delete_meetinglog()
    {
        require_once("classes/N2MY_MeetingLog.class.php");

        // room_key、meeting_session_id取得
        $room_key           = $this->request->get(API_ROOM_ID);
        $meeting_session_id = $this->request->get(API_MEETING_ID);

        $result = array(
            API_ROOM_ID => $room_key,
            API_MEETING_ID => $meeting_session_id,
        );
        return $this->output($result);
    }

    /**
     * 会議記録パスワード設定
     *
     * @param string room_key ルームキー
     * @param string meeting_session_id 会議セッションID
     * @param string meetinglog_password パスワード
     * @return boolean
     */
    function action_set_meetinglog_password()
    {
        require_once("classes/N2MY_MeetingLog.class.php");

        // room_key、meeting_session_id取得
        $room_key            = $this->request->get(API_ROOM_ID);
        $meeting_session_id  = $this->request->get(API_MEETING_ID);
        $log_password        = $this->request->get("log_password");
        $result = array(
            API_ROOM_ID => $room_key,
            API_MEETING_ID => $meeting_session_id,
            );
        return $this->output($result);
    }

    /**
     * 会議記録パスワード削除
     *
     * @param string room_key ルームキー
     * @param string meeting_session_id 会議セッションID
     * @return boolean
     */
    function action_unset_meetinglog_password()
    {
        require_once("classes/N2MY_MeetingLog.class.php");

        // room_key、meeting_session_id取得
        $room_key           = $this->request->get(API_ROOM_ID);
        $meeting_session_id = $this->request->get(API_MEETING_ID);
        $log_password       = $this->request->get("log_password");
        $result = array(
            API_ROOM_ID => $room_key,
            API_MEETING_ID => $meeting_session_id,
            );
        return $this->output($result);
    }

    /**
     * 会議記録保護設定
     *
     * @param string room_key ルームキー
     * @param string meeting_session_id 会議セッションID
     * @return boolean
     */
    function action_set_meetinglog_protect()
    {
        require_once("classes/N2MY_MeetingLog.class.php");

        // room_key、meeting_session_id取得
        $room_key           = $this->request->get(API_ROOM_ID);
        $meeting_session_id = $this->request->get(API_MEETING_ID);
        $log_password       = $this->request->get("log_password");

        $result = array(
            API_ROOM_ID => $room_key,
            API_MEETING_ID => $meeting_session_id,
            );
        return $this->output($result);
    }

    /**
     * 会議記録保護解除
     *
     * @param string room_key ルームキー
     * @param string meeting_session_id 会議セッションID
     * @return boolean
     */
    function action_unset_meetinglog_protect()
    {
        require_once("classes/N2MY_MeetingLog.class.php");

        // room_key、meeting_session_id取得
        $room_key           = $this->request->get(API_ROOM_ID);
        $meeting_session_id = $this->request->get(API_MEETING_ID);
        $log_password       = $this->request->get("log_password");
        $result = array(
            API_ROOM_ID => $room_key,
            API_MEETING_ID => $meeting_session_id,
            );
        return $this->output($result);
    }

    function action_play()
    {
        require_once("classes/N2MY_MeetingLog.class.php");

        $meeting_id = $this->request->get(API_MEETING_ID);
        $sequence_key = $this->request->get("sequence_key");
        $log_type     = $this->request->get("log_type");
        $check_data = array(
            "meeting_id" => $meeting_id,
            "sequence_key" => $sequence_key,
            "log_type" => $log_type,
        );
        $rules = array(
            "meeting_id" => array(
                "required" => true
            ),
            "sequence_key" => array(
                "required" => true
            ),
            "log_type" => array(
                "required" => true,
                "allow" => array("video", "minutes")
            ),
        );
        $err_obj = $this->error_check($check_data, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        // 認証処理

        $request = $this->request->getAll();
        $obj_Meeting  = new DBI_Meeting( $this->get_dsn() );
        $meeting_info     = $obj_Meeting->getRow( sprintf( "meeting_session_id='%s' AND is_deleted=0", $meeting_id ) );
        $member_info = $this->session->get( "member_info" );
        $user_info = $this->session->get( "user_info");
        ( ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") &&
            $member_info["room_key"] != $meeting_info["room_key"] ) ?
            $this->session->set( "display_deleteButton", false ) :
            $this->session->set( "display_deleteButton", true );
        //set parameters into session
        if ( ! $meeting_info ) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$sequence_key);
            return $this->display_error(101, "Object not found", $this->get_error_info($err_obj));
        }
        $this->session->remove("ondemand");

        // 録画GW情報取得
        require_once 'classes/core/dbi/MeetingSequence.dbi.php';
        $objMeetingSequence = new DBI_MeetingSequence( $this->get_dsn() );
        $where = "meeting_key = ".$meeting_info["meeting_key"].
                 " AND meeting_sequence_key = '".addslashes($request["sequence_key"])."'";
        if ($log_type == "video") {
            $log_type = "log_video";
            $where .= " AND has_recorded_video = 1";
        } else {
            $log_type = "log_minutes";
            $where .= " AND has_recorded_minutes = 1";
        }
        $meeting_sequence_info = $objMeetingSequence->getRow($where);
        if ($meeting_sequence_info) {
            $this->session->set("display_deleteButton", ($meeting_sequence_info["record_status"] != "convert") ? true : false);
        } else {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$meeting_sequence_info);
            return $this->display_error(101, "Object not found", $this->get_error_info($err_obj));
        }
        $this->session->set( "log_meeting_key", $meeting_info["meeting_key"] );
        $this->session->set( "log_sequence_key", $meeting_sequence_info["meeting_sequence_key"] );
        $this->session->set( "log_meeting_name", $meeting_info["meeting_name"] );
        $this->session->set( "log_room_key", $meeting_info["room_key"] );
        $this->session->set( "log_type", $log_type );
        $url = N2MY_BASE_URL."services/core/meeting_log/play.php?action_log_display=&".N2MY_SESS_NAME."=".session_id();

        $data = array(
            "url"               => $url,
            API_MEETING_ID      => $meeting_info["meeting_session_id"],
            "meeting_sequence_key"  => $meeting_sequence_info["meeting_sequence_key"],
            "log_type"        => $log_type,
        );
        return $this->output($data);

    }

    /**
     * 会議記録再生（映像）
     *
     * @param string room_key ルームキー
     * @param string meeting_session_id 会議セッションID
     * @return boolean
     */
    function action_play_logplayer()
    {
        require_once("classes/N2MY_MeetingLog.class.php");

        $meeting_key  = $this->request->get("meeting_key");
        $sequence_key = $this->request->get("sequence_key");
        // 認証処理

        if ($meeting_key && $sequence_key) {
            $param = array(
                "action_set_param"      => "",
                "meeting_ticket"        => $meeting_key,
                "sequence_key"          => $sequence_key,
                "locale"                => $this->session->get("lang"),
                "mode"                  => 'log_video'
                );
            $this->logger->debug(__FUNCTION__."#param", __FILE__, __LINE__, $param);
            $url = $this->get_core_url("/service/meeting_log/play.php", $param);
            $session_id = $this->wget($url);
            $this->logger->trace("session_id",__FILE__,__LINE__,$session_id);
            $session_id = $session_id;
            $param = array(
                "action_play"     => "",
                "session_id"      => $session_id
                );
            $url = $this->get_core_url("/service/meeting_log/play.php", $param);
            $this->logger->info(__FUNCTION__."#url", __FILE__, __LINE__, $url);
            header("Location: ".$url."&cachebuster=".time());
        }
        $data = array(
            "meeting_key"  => $meeting_key,
            "sequence_key" => $sequence_key,
            );
        return $this->output($data);
    }

    /**
     * 会議記録再生（議事録）
     *
     * @param string room_key ルームキー
     * @param string meeting_session_id 会議セッションID
     * @return boolean
     */
    function action_play_lastlogplayer()
    {
        require_once("classes/N2MY_MeetingLog.class.php");
        $meeting_key  = $this->request->get("meeting_key");
        $sequence_key = $this->request->get("sequence_key");
        // 認証処理
        if ($meeting_key && $sequence_key) {
            $param = array(
                "action_set_param"      => "",
                "meeting_ticket"        => $meeting_key,
                "sequence_key"          => $sequence_key,
                "locale"                => $this->session->get("lang"),
                "mode"                  => 'log_minutes'
                );
            $this->logger->debug(__FUNCTION__."#param", __FILE__, __LINE__, $param);
            $url = $this->get_core_url("/service/meeting_log/play.php", $param);
            $session_id = $this->wget($url);
            $this->logger->trace("session_id",__FILE__,__LINE__,$session_id);
            $session_id = $session_id;
            $param = array(
                "action_play"     => "",
                "session_id"      => $session_id
                );
            $url = $this->get_core_url("/service/meeting_log/play.php", $param);
            $this->logger->info(__FUNCTION__."#url", __FILE__, __LINE__, $url);
            header("Location: ".$url."&cachebuster=".time());
        }
        $result = array(
            "meeting_key"  => $meeting_key,
            "sequence_key" => $sequence_key,
            );
        return $this->output($result);
    }

    function action_eco_update() {
        if ($dsn_key = $this->request->get("serverDsnKey", N2MY_DEFAULT_DB)) {
            $this->_dsn = $this->get_dsn_value($dsn_key);
        } else {
            $this->_dsn = $this->get_dsn();
        }
        $this->obj_Eco = new N2MY_Eco( $this->get_dsn(), N2MY_MDB_DSN );
        require_once('lib/EZLib/Transit.class.php');
        $uri = $this->config->get("N2MY", "transit_uri");
        $transit_log = realpath(dirname(__FILE__)."/../../../var/")."/transit.log";
        $Transit = Transit::factory( $uri, $transit_log );
        $meeting_id = $this->request->get(API_MEETING_ID);
        $station_list   = $this->request->get("station");
        $endpoint       = $this->request->get("endpoint");
        $participants   = $this->request->get("participants");
        foreach($participants as $key => $participant) {
            $participants[$key] = $participant;
            $participants[$key]["participant_station"] = $station_list[$participant["participant_key"]];
        }
        $this->request->set("participants", $participants);
        $this->request->set("end_point", $endpoint);
        // 入力チェック
        $err = array();
        foreach($station_list as $key => $station) {
            if (!$station) {
                if ($endpoint == $key) {
                    $err[$key] = true;
                }
            } else {
                $ret = $Transit->getStationList($station, false, 50);
                if (!in_array($station, $ret)) {
                    $err[$key] = true;
                }
            }
        }
        if ($err) {
            return $this->render_eco_edit_form($err);
        }
        // 参加者の拠点情報を更新
        require_once("classes/dbi/meeting.dbi.php");
        require_once("classes/core/dbi/Participant.dbi.php");
        $obj_Meeting = new MeetingTable($this->get_dsn());
        $objParticipant = new DBI_Participant($this->get_dsn());
        $where = "meeting_session_id = '".addslashes($meeting_id)."'";
        $meeting_info = $obj_Meeting->getRow($where);
        $this->logger2->info($station_list);
        if (!DB::isError($meeting_info)) {
            foreach($station_list as $participant_key => $station) {
                $where = "meeting_key = ".$meeting_info["meeting_key"].
                    " AND participant_key = '".addslashes($participant_key)."'";
                $data = array(
                    "participant_station" => $station,
                    "update_datetime"     => date("Y-m-d H:i:s"),
                );
                $objParticipant->update($data, $where);
            }
            // 経路検索結果取得
            $this->obj_Eco->createReport($meeting_info["meeting_key"], $station_list[$endpoint], false, false);
        }
        // 結果画面表示
        $this->action_meeting_log();
    }

}
$main = new N2MY_MeetingLog_API();
$main->execute();