<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_Account.class.php");
require_once('classes/core/dbi/Meeting.dbi.php');
require_once ("classes/N2MY_Storage.class.php");

class Service_LogPlayer extends AppFrame
{

    private $_obj_Storage      = null;

    function init() {
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->_name_space = md5(__FILE__);
    }

    function auth() {
        $this->checkAuth();
        $serverInfo                    = $this->session->get("server_info");
        $this->_obj_Storage  = new N2MY_Storage($this->get_dsn(), $serverInfo["host_name"]);
    }

    function default_view()
    {
        $storage_key = $this->request->get('storage_key');
        $user_info = $this->session->get("user_info");
        $file_info = $this->_obj_Storage->getStorageInfo($storage_key);
        if($user_info["user_key"] != $file_info["user_key"]){
            return;
        }

        $width  = 640;
        $height = 480;

        $file_url =  sprintf( "%s%s%s", N2MY_BASE_URL."docs/", $file_info["file_path"], $file_info["file_name"] );
        //assign values
        $this->template->assign('charset', $this->config->get( "GLOBAL", "html_output_char" ) );
        $this->template->assign('locale' , $this->get_language() );
        $this->template->assign('width' , $width );
        $this->template->assign('height' , $height );
        $this->template->assign('file_url' , $file_url );
        //$this->logger2->info($file_url);
        // service info
        $frame["service_info"] = $this->get_message("SERVICE_INFO");
        $this->template->assign("__frame", $frame);
        $this->template->assign("api_url", N2MY_BASE_URL);
        $this->display('user/storage/personal_whiteboard_as3.t.html');
    }
}

$main =& new Service_LogPlayer();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
