<?php 
require_once(dirname(__FILE__)."/../../logic/composition/CompositionManager.php");

class SalesMobileMixCompositionManager extends CompositionManager {
	public function run() {
		$this->setCompositionTypeForMobileParticipant();
	}

	protected function getMobileParticipantCompositionType() {
		return PIP1;
	}
}
