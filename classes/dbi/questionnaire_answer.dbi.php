<?php
require_once("classes/N2MY_DBI.class.php");

class QuestionnaireAnswerTable extends N2MY_DB
{

    var $table = 'questionnaire_answer';
    protected $primary_key = "questionnaire_answer_key";

    function __construct( $dsn ) {
        $this->init($dsn, $this->table );
    }
}

