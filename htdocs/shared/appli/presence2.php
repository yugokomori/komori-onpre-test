<?php
header("Content-Type: text/xml; charset=UTF-8;");
print '<?xml version="1.0" encoding="UTF-8" ?>'."\n";
$url = ($_SERVER["HTTPS"]) ? 'https://' : 'http://';
$url .= $_SERVER["HTTP_HOST"];
$version_ini = parse_ini_file('setup/version.ini', true);
$version = $version_ini["APPLI"]['presence2'];
?>
<PresenceAppli2>
   <Version><?php print $version;?></Version>
   <DownloadUrl>
       <WIN><?php print $url;?>/shared/appli/PresenceAppli2.exe</WIN>
       <MAC><?php print $url;?>/shared/appli/PresenceAppli2.dmg</MAC>
   </DownloadUrl>
</PresenceAppli2>