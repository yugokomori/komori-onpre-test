<?php
require_once("classes/dbi/reseller_info.dbi.php");
require_once("dBug.php");

class ResellerAuth {

    var $logger = "";
    var $reseller_info = "";

    function ResellerAuth($dsn)
    {
        $this->logger =& EZLogger::getInstance();
        $this->reseller_info = new ResellerInfoTable($dsn);
    }

    /**
     * ログイン処理
     *
     * @param string $login_id ログインID
     * @param string $login_pw ログインパスワード
     * @param boolean $pw_encode パスワードの暗号化（MD5ハッシュ）
     * @return mixed 成功時:リセラー情報、エラー時:false
     */
    function check($login_id, $login_pw, $pw_encode = 0, $admin_id="", $admin_pw="")
    {
        // 未入力チェック
        if(!$login_id && !$login_id){
            return false;
        }
        // リセラーアカウントでチェック
        if ($pw_encode == "md5") {
            $reseller_info = $this->reseller_info->checkLogin_MD5($login_id, $login_pw, $admin_id, $admin_pw);
        } else if ($pw_encode == "sha1") {
            $reseller_info = $this->reseller_info->checkLogin_Sha1($login_id, $login_pw, $admin_id, $admin_pw);
        } else {
            $reseller_info = $this->reseller_info->checkLogin($login_id, $login_pw, $admin_id, $admin_pw);
        }
        if (PEAR::isError($reseller_info)) {
            // ログイン情報取得時エラー
            $this->logger->error(__FUNCTION__."# Login ERROR!", __FILE__, __LINE__, $reseller_info);
            return false;
        } elseif ($reseller_info) {
            // ユーザログイン
            $this->logger->error(__FUNCTION__."# Login SUCCESS!", __FILE__, __LINE__, $reseller_info);
            return $reseller_info;
        }
        $this->logger->error(__FUNCTION__."# No Account, ERROR!", __FILE__, __LINE__, $reseller_info);
        // アカウントが存在しない
        return false;
    }
}
