<?php

require_once('classes/core/dbi/Meeting.dbi.php');
require_once('classes/core/dbi/MeetingOptions.dbi.php');
require_once('classes/core/dbi/MeetingSequence.dbi.php');
require_once('classes/core/dbi/MeetingUptime.dbi.php');
require_once('classes/core/dbi/MeetingUseLog.dbi.php');
require_once("classes/core/dbi/Document.dbi.php");
require_once('classes/core/dbi/Options.dbi.php');
require_once('classes/core/dbi/Participant.dbi.php');
require_once('classes/mgm/dbi/FmsServer.dbi.php');
require_once('classes/core/dbi/SharingServer.dbi.php');
require_once("classes/N2MY_FileUpload.class.php");
require_once("classes/dbi/intra_fms.dbi.php");
require_once("classes/dbi/room.dbi.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/dbi/member.dbi.php");
require_once("classes/dbi/ordered_service_option.dbi.php");
require_once('classes/mgm/dbi/FmsServer.dbi.php');
require_once('lib/EZLib/EZUtil/EZFile.class.php');
require_once("classes/dbi/fms_watch_status.dbi.php");


class Core_Meeting
{
    var $logger = null;
    var $logger2 = null;
    private $dsn = null;
    private $mgm_dsn = null;
    private $config;

    public function __construct( $dsn, $mgm_dsn=null )
    {
        $this->logger  = EZLogger::getInstance();
        $this->logger2 = EZLogger2::getInstance();
        $this->dsn     = $dsn;
        $this->mgm_dsn = $mgm_dsn;
        $this->config  = parse_ini_file(sprintf( "%sconfig/config.ini", N2MY_APP_DIR ), true);
    }

    /**
     * 現在の部屋の状態を取得する
     */
    public function getMeetingStatus( $rooms ) {
      $this->logger2->debug("#getMeetingStatus");
        $obj_Meeting        = new DBI_Meeting( $this->dsn );
        $obj_MeetingUptime  = new DBI_MeetingUptime( $this->dsn );
        $obj_Participant    = new DBI_Participant( $this->dsn );
        $obj_Room           = new RoomTable($this->dsn);

        foreach ($rooms as $room_key => $meeting_ticket ) {
            $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_ticket='%s'", addslashes($meeting_ticket) ) );
            $status       = $meeting_info["is_active"] ? 1 : 0;
            $is_reserved  = $meeting_info["is_reserved"] ? 1 : 0;
            $pcount       = 0;
            // データが存在する場合
            $participants = array();
            if ($meeting_info) {
                $meeting_key = $meeting_info["meeting_key"];
                $status      = $meeting_info["is_active"];
                $pcount      = $obj_Participant->getCount($meeting_key);
                // 0人
                if ($pcount == 0 && $meeting_key && $status == 1) {
                    if ($meeting_info["is_reserved"]) {

                        // 予約情報を取得
                        require_once('classes/dbi/reservation.dbi.php');
                        $obj_reservation = new ReservationTable($this->dsn );
                        $reservation_info = $obj_reservation->getRow("meeting_key='" .$meeting_info["meeting_ticket"] . "'");

                        $cur_datetime = time();
                        if ($cur_datetime > strtotime($reservation_info["reservation_extend_endtime"])) {
                            if ($meeting_info["intra_fms"]) {
                                $this->stopMeeting($meeting_key, true, false);
                            } else {
                                $this->stopMeeting($meeting_key, null, false);
                            }
                            $status = "0";
                            // 次の会議予約が開催中であれば、置き換え
                            $next_meeting_key = $obj_Meeting->getNextKey( $room_key, $meeting_key );
                            $meeting_key      = '';
                            $meeting_ticket   = '';
                            if( $next_meeting_key ){
                                $next_meeting = $obj_Meeting->getRow( sprintf( "meeting_key ='%s' AND is_deleted = 0",
                                                                               $next_meeting_key ) );
                                if (strtotime($next_meeting['meeting_start_datetime']) <= time()
                                 && time() <= strtotime($next_meeting['meeting_stop_datetime'])) {
                                    $meeting_key    = $next_meeting_key;
                                    $meeting_ticket = $next_meeting['meeting_ticket'];
                                    $this->logger2->info($meeting_ticket, '置き換え');
                                    $obj_Room->last_meeting_update($room_key, $meeting_ticket);
                                }
                            }
                            $this->logger2->info($meeting_ticket);
                        }
                    } else {
                        // 次の会議の開始時間 ＝ 終了時間が過ぎて
                        $next_meeting_key = $obj_Meeting->getNextKey( $room_key, $meeting_key );
                        if ($next_meeting_key){
                            $next_meeting = $obj_Meeting->getRow( sprintf( "meeting_key ='%s' AND is_deleted = 0", $next_meeting_key, "meeting_key, meeting_ticket, meeting_start_datetime" ) );
                            if ($next_meeting){
                                $cur_datetime = time();
                                $next_meeting_start_datetime = strtotime($next_meeting['meeting_start_datetime']);
                                if ($cur_datetime > $next_meeting_start_datetime) {
                                    // イントラFMSの場合は録画容量を取得しない
                                    if ($meeting_info["intra_fms"]) {
                                        $this->stopMeeting($meeting_key, true, false);
                                    } else {
                                        $this->stopMeeting($meeting_key, null, false);
                                    }
                                    // 次の会議予約が開催中であれば、置き換え
                                    $meeting_key = $next_meeting_key;
                                    $meeting_ticket = $next_meeting['meeting_ticket'];
                                    $this->logger2->info($next_meeting, '置き換え');
                                    $obj_Room->last_meeting_update($room_key, $meeting_ticket);
                                    $status = "0";
                                }
                            }
                        }
                    }
                // 予約会議をキャンセル等で終了させた場合の対応
                } elseif ($pcount == 0 && $meeting_key && $status == 0) {
                    // 次の会議予約が開催中であれば、置き換え
                    $next_meeting_key = $obj_Meeting->getNextKey( $room_key, '' );
                    $meeting_key      = '';
                    $meeting_ticket   = '';
                    if ( $next_meeting_key ) {
                        $next_meeting = $obj_Meeting->getRow( sprintf( "meeting_key ='%s' AND is_deleted = 0", $next_meeting_key ) );
                        if (strtotime($next_meeting['meeting_start_datetime']) <= time()
                           && time() <= strtotime($next_meeting['meeting_stop_datetime'])) {
                            $meeting_key    = $next_meeting_key;
                            $meeting_ticket = $next_meeting['meeting_ticket'];
                            $this->logger2->info($meeting_ticket, '置き換え');
                            $obj_Room->last_meeting_update($room_key, $meeting_ticket);
                        }
                        $is_reserved = $next_meeting["is_reserved"] ? 1 :0;
                    }
                    $status = "0";
                }
                if ($meeting_key) {
                    //get online_participans
                    $participants = $obj_Participant->getList($meeting_key, true);
                }
            } else {
                // 次の会議予約が開催中であれば、置き換え
                $where = "room_key = '".addslashes($room_key)."'".
                         " AND is_active = 1".
                         " AND is_deleted = 0".
                         " AND is_reserved = 1".
                         " AND meeting_start_datetime <= '".date('Y-m-d H:i:s')."'".
                         " AND meeting_stop_datetime >= '".date('Y-m-d H:i:s')."'";
                $next_meeting_key = $obj_Meeting->getOne($where, "meeting_key", array("meeting_start_datetime" => "asc"));
                $meeting_key      = '';
                $meeting_ticket   = '';
                if ($next_meeting_key) {
                    $next_meeting = $obj_Meeting->getRow( sprintf( "meeting_key ='%s' AND is_deleted = 0", $next_meeting_key ) );
                    if (strtotime($next_meeting['meeting_start_datetime']) <= time()
                     && time() <= strtotime($next_meeting['meeting_stop_datetime'])) {
                        $meeting_key    = $next_meeting_key;
                        $meeting_ticket = $next_meeting['meeting_ticket'];
                        $this->logger2->info($meeting_ticket, '置き換え');
                        $obj_Room->last_meeting_update($room_key, $meeting_ticket);
                    }
                    $is_reserved = $next_meeting["is_reserved"] ? 1 :0;
                }
                $status = "0";
            }
            $_rooms[] = array("room_key"      => $room_key,
                              "meeting_key"   => $meeting_key,
                              "meeting_ticket"=> $meeting_ticket,
                              "status"        => $status,
                              "is_reserved"   => $is_reserved,
                              "pcount"        => $pcount,
                              "participants"  => $participants);
        }
        return $_rooms;
    }

    public function checkMeetingStatus( $parameters )
    {
        $obj_Meeting        = new DBI_Meeting( $this->dsn );
        $obj_MeetingOptions = new DBI_MeetingOptions( $this->dsn );
        $obj_Options        = new DBI_Options( N2MY_MDB_DSN );

        $version = $this->getVersion();
        $params = array(
            "server_key"              => 0,
            "sharing_server_key"      => 0,
            "user_key"                => 0,
            "layout_key"              => null,
            "room_key"                => null,
            "meeting_ticket"          => null,
            "pin_cd"                  => null,
            "meeting_country"         => 'ja',
            "meeting_room_name"       => null,
            "meeting_name"            => null,
            "meeting_tag"             => null,
            "meeting_max_audience"    => 9,
            "meeting_max_whiteboard"  => 0,
            "meeting_max_extendable"  => 2160,
            "meeting_max_seat"        => 10,
            "meeting_size_recordable" => 0,
            "meeting_size_uploadable" => 20971520,
            "meeting_log_owner"       => '',
            "meeting_log_password"    => '',
            "meeting_start_datetime"  => null,
            "meeting_stop_datetime"   => null,
            "need_fms_version"        => '',
            "use_sales_option"        => 0,
            "inbound_flg"             => 0,
            "inbound_id"              => null,
            "prohibit_extend_meeting_flg" => 0,
            "is_reserved"             => 0,
            "version"                 => $version,
            "ssl"                     => 0,
            "tls"                     => 0,
            "hispec"                  => 0,
            "mobile"                  => 0,
            "h323"                    => 0,
            "chat"                    => 0,
            "sharing"                 => 0,
            "translator"              => 0,
            "intra_fms"               => 0,
            "multicamera"             => 0,
            "telephone"               => 0,
            "smartphone"              => 0,
            "record_gw"               => 0,
            "whiteboard_video"        => 0,
            "video_conference"        => 0,
            "minimumBandwidth80"      => 0,
            "h264"                    => 0,
            "ChinaFastLine"           => 0,
            "ChinaFastLine2"          => 0,
            "GlobalLink"              => 0
        );

        $data = array();
        foreach ($params as $idx => $val) {
            $data[$idx] = isset($parameters[$idx]) ? $parameters[$idx] : "";
            if ($data[$idx] == "") {

                $data[$idx] = $val;
            }
        }

        // ユーザー情報取得
        require_once 'classes/dbi/user.dbi.php';
        $objUser   = new UserTable($this->dsn);
        $user_info = $objUser->getRow("user_key = '".addslashes($data["user_key"])."'", "user_id,is_compulsion_pw,compulsion_pw,meeting_version");
        if (!$user_info["meeting_version"]) {
            $data["version"] = "4.6.5.0";
        }
        // 強制パスワード設定
        if (!$data["meeting_log_password"] && $user_info["is_compulsion_pw"] == "1") {
            $this->logger2->info($user_info, "パスワード強制設定");
            $data["meeting_log_password"] = $user_info["compulsion_pw"];
        }
        //add meeting_option
        $optionlist = array(
            "ssl"                   => $data["ssl"],
            "tls"                   => $data["tls"],
            "hispec"                => $data["hispec"],
            "mobile"                => $data["mobile"],      // マルチカメラ時排他処理はconfig.phpで
            "h323"                  => $data["h323"],        // マルチカメラ時排他処理はconfig.phpで
            "chat"                  => $data["chat"],
            "sharing"               => $data["sharing"],
            "translator"            => $data["translator"],
            "intra_fms"             => $data["intra_fms"],
            "multicamera"           => $data["multicamera"],
            "telephone"             => $data["telephone"],   // マルチカメラ時排他処理はconfig.phpで
            "smartphone"            => $data["smartphone"],   // マルチカメラ時排他処理はconfig.phpで
            "record_gw"             => $data["record_gw"],
            "whiteboard_video"      => $data["whiteboard_video"],
            "video_conference"      => $data["video_conference"],
            "minimumBandwidth80"    => $data["minimumBandwidth80"],
            "h264"                  => $data["h264"],
            "ChinaFastLine"         => $data["ChinaFastLine"],
            "ChinaFastLine2"        => $data["ChinaFastLine2"],
            "GlobalLink"            => $data["GlobalLink"]
        );
        $this->logger->debug("MeetingCreate",__FILE__,__LINE__,$data);
        $this->logger2->debug($optionlist);

        //---------------------------------------------------------------------
        //get meeting_key
        if( $data["meeting_ticket"] )
            $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_ticket='%s' AND is_deleted=0", $data["meeting_ticket"] ) );

        $data['meeting_key'] = $meeting_info["meeting_key"];
        if ( $meeting_info ) {
            //check status
            if ($meeting_info['is_deleted'] == '1' || $meeting_info['is_active']  == '0') {
                $this->_error = 'MEETING_EXPIRED_ERROR';
            } else {
                require_once "classes/core/dbi/Participant.dbi.php";
                $obj_Participant = new DBI_Participant( $this->dsn );
                $where = "meeting_key = ".$data['meeting_key'].
                         " AND use_count > 0";
                // 会議が始まっていない場合のみ反映される
                if ($obj_Participant->numRows($where) == 0) {
                    if (!$meeting_info['is_reserved'] && !$this->config["IGNORE_MENU"]["teleconference"]) {
                        $this->updateMeetingTeleconf($data['meeting_key'], $meeting_info['room_key']);
                    }
                    $this->logger2->debug($data);
                    // オプション情報更新
                    $this->logger2->debug($data, "Update Option");
                    $where = "meeting_key = ".$data['meeting_key'];
                    $obj_MeetingOptions->remove($where);
                    foreach ($optionlist as $key => $value) {
                        $optiondata = array();
                        if ($value > 0) {
                            $option = $obj_Options->getRow( sprintf( "option_name='%s'", $key ) );
                            $modata = array(
                                "meeting_key"              => $data["meeting_key"],
                                "option_key"               => $option["option_key"],
                                "meeting_option_quantity"  => $value
                            );
                            $result = $obj_MeetingOptions->add( $modata );
                            if (!$result) {
                                $this->_error = 'DB_INSERT_ERROR(MeetingOptions_Add)';
                            }
                        }
                    }
                    // 参加人数と会議記録容量も変更する
                    $wehere = "meeting_key = ".$data['meeting_key'];
                    $meetingdata = array(
                       "meeting_country"         => $data["meeting_country"],
                       "meeting_room_name"       => $data["meeting_room_name"],
                       "meeting_name"            => $parameters["reservation_modify"]?$data["meeting_name"]:($meeting_info["meeting_name"]? $meeting_info["meeting_name"]:$data["meeting_name"]),
                       "meeting_tag"             => $data["meeting_tag"],
                       "meeting_max_audience"    => $data["meeting_max_audience"],
                       "meeting_max_whiteboard"  => $data["meeting_max_whiteboard"],
                       "meeting_max_extendable"  => $data["meeting_max_extendable"],
                       "meeting_max_seat"        => $data["meeting_max_seat"],
                       "meeting_size_recordable" => $data["meeting_size_recordable"],
                       "meeting_size_uploadable" => $data["meeting_size_uploadable"],
                       "meeting_log_password"    => $parameters["reservation_modify"]?$data["meeting_log_password"]:($meeting_info["meeting_log_password"]? $meeting_info["meeting_log_password"]:$data["meeting_log_password"]),
                       "meeting_start_datetime"  => $data["meeting_start_datetime"],
                       "meeting_stop_datetime"   => $data["meeting_stop_datetime"],
                       "need_fms_version"        => $data["need_fms_version"],
                       "version"                 => $data["version"],
                       "use_sales_option"        => $data["use_sales_option"],
                       "inbound_flg"             => $data["inbound_flg"],
                       "inbound_id"              => $data["inbound_id"],
                       "prohibit_extend_meeting_flg" => $data["prohibit_extend_meeting_flg"],
                    );
                    $this->logger2->debug($meetingdata);
                    // fix me
                    $service_option  = new OrderedServiceOptionTable($this->dsn);
                    $enable_teleconf = $service_option->enableOnRoom(23, $meeting_info["room_key"]); // 23 = teleconf
                    if (!$this->config["IGNORE_MENU"]["teleconference"] && $enable_teleconf) {
                        $pgi_columns  = array("tc_type", "tc_teleconf_note", "use_pgi_dialin", "use_pgi_dialin_free",
                                              "use_pgi_dialout", "pgi_api_status");
                        foreach ($pgi_columns as $pgi_col) {
                            if (isset($parameters[$pgi_col])) {
                                $meetingdata[$pgi_col] = $parameters[$pgi_col];
                            }
                        }
                        $pgi_api_status = $this->getExecutePGiAPiStatus($meeting_info, $meetingdata, $data);
                        if ($pgi_api_status) {
                            $meetingdata["pgi_api_status"] = $pgi_api_status;
                        }
                    }
                    $obj_Meeting->update($meetingdata, $where);
                }
            }
            //会議作成フラグ変更
            $obj_Room = new RoomTable($this->dsn);
            $where_room = "room_key = '".$meeting_info["room_key"]."'";
            $room_update = array("creating_meeting_flg" => 0);
            $obj_Room->update($room_update, $where_room);
        } else {
            $meeting_session_id = md5(uniqid(rand(), 1));
            //set is_reserved
            if ($data['meeting_start_datetime'] && $data['meeting_stop_datetime']) {
                $data['is_reserved'] = 1;
            }
            // 携帯連携用のPINコード
            $pin_cd = $this->_getPinCd($data["user_key"]);
            $this->logger2->info(array(
                "meeting_session_id" => $meeting_session_id,
                "pin_cd"             => $pin_cd), "PINコードを発行しました。");
            //add record
            $meetingdata = array(
               "layout_key"              => $data["layout_key"],
               "server_key"              => $data["server_key"],
               "sharing_server_key"      => $data["sharing_server_key"],
               "user_key"                => $data["user_key"],
               "room_key"                => $data["room_key"],
               "meeting_ticket"          => $data["meeting_ticket"],
               "meeting_session_id"      => $meeting_session_id,
               "pin_cd"                  => $pin_cd,
               "meeting_country"         => $data["meeting_country"],
               "meeting_room_name"       => $data["meeting_room_name"],
               "meeting_name"            => $data["meeting_name"],
               "meeting_tag"             => $data["meeting_tag"],
               "meeting_max_audience"    => $data["meeting_max_audience"],
               "meeting_max_whiteboard"  => $data["meeting_max_whiteboard"],
               "meeting_max_extendable"  => $data["meeting_max_extendable"],
               "meeting_max_seat"        => $data["meeting_max_seat"],
               "meeting_size_recordable" => $data["meeting_size_recordable"],
               "meeting_size_uploadable" => $data["meeting_size_uploadable"],
               "meeting_log_owner"       => $data["meeting_log_owner"],
               "meeting_log_password"    => $data["meeting_log_password"],
               "meeting_start_datetime"  => $data["meeting_start_datetime"],
               "meeting_stop_datetime"   => $data["meeting_stop_datetime"],
               "need_fms_version"        => $data["need_fms_version"],
               "is_reserved"             => $data["is_reserved"],
               "intra_fms"               => $data["intra_fms"],
               "version"                 => $data["version"],
               "use_sales_option"        => $data["use_sales_option"],
               "inbound_flg"             => $data["inbound_flg"],
               "inbound_id"              => $data["inbound_id"],
               "prohibit_extend_meeting_flg" => $data["prohibit_extend_meeting_flg"],
           );

            $service_option  = new OrderedServiceOptionTable($this->dsn);
            $enable_teleconf = $service_option->enableOnRoom(23, $data["room_key"]); // 23 = teleconf
            if (!$this->config["IGNORE_MENU"]["teleconference"] && $enable_teleconf) {
                $meetingdata["tc_type"]          = $parameters['tc_type'] ? $parameters['tc_type'] : 'voip';
                $meetingdata["pgi_api_status"]   = 'create';
                $meetingdata["tc_teleconf_note"] = $parameters["tc_teleconf_note"] ? $parameters["tc_teleconf_note"] : '';
                if ($data['is_reserved']) {
                    $meetingdata["use_pgi_dialin"]      = $parameters["use_pgi_dialin"]      ? 1 : 0;
                    $meetingdata["use_pgi_dialin_free"] = $parameters["use_pgi_dialin_free"] ? 1 : 0;
                    $meetingdata["use_pgi_dialout"]     = $parameters["use_pgi_dialout"]     ? 1 : 0;
                } else {
                    require_once ("classes/dbi/room.dbi.php");
                    $room_table = new RoomTable($this->dsn);
                    $room       = $room_table->findByKey($data['room_key']);
                    if ($room['use_teleconf']){
                        $meetingdata["use_pgi_dialin"]      = $room["use_pgi_dialin"];
                        $meetingdata["use_pgi_dialin_free"] = $room["use_pgi_dialin_free"];
                        $meetingdata["use_pgi_dialout"]     = $room["use_pgi_dialout"];
                    }  else {
                        $meetingdata["use_pgi_dialin"]      = 0;
                        $meetingdata["use_pgi_dialin_free"] = 0;
                        $meetingdata["use_pgi_dialout"]     = 0;
                    }
                }
            }

            require_once("classes/mcu/resolve/Resolver.php");
            if (($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($meetingdata["room_key"]))) {
              if (($ivesInfo = $videoConferenceClass->getIvesInfoByRoomKey($meetingdata["room_key"]))) {
                $temporaryDid = $meetingdata["is_reserved"]? $videoConferenceClass->getReservationGuestDidByParmanentDid($ivesInfo["ives_did"]): $videoConferenceClass->getInvitationGuestDidByParmanentDid($ivesInfo["ives_did"]);
                $meetingdata["temporary_did"] = $temporaryDid;
              }
            }

            $data['meeting_key'] = $obj_Meeting->add( $meetingdata );
            //会議作成フラグ変更
            $obj_Room = new RoomTable($this->dsn);
            $where_room = "room_key = '".$meetingdata["room_key"]."'";
            $room_update = array("creating_meeting_flg" => 0);
            $obj_Room->update($room_update, $where_room);
            //会議内からの招待メールのため会議作成毎にmgm_relationにmeeting_session_idを追加
            require_once "classes/mgm/dbi/relation.dbi.php";
            require_once "classes/dbi/user.dbi.php";
            $objMgmRelationTable = new MgmRelationTable(N2MY_MDB_DSN);
            $objUser             = new UserTable($this->dsn);
            $user_id             = $objUser->getOne("user_key = '".addslashes($data["user_key"])."'", "user_id");
            if ($user_id) {
                $relation_data = array(
                            "relation_key"  => $meetingdata["meeting_session_id"],
                            "user_key"      => $user_id,
                            "relation_type" => "meeting",
                            "status"        => 1,
                        );
                $relation_ret = $objMgmRelationTable->add($relation_data);
                if (DB::isError($relation_ret)) {
                    $this->logger2->info($relation_ret->getUserInfo());
                }
            }
            if (!$data['meeting_key']) {
                $this->_error = 'DB_INSERT_ERROR(Meeting)';
            } else {
                foreach( $optionlist as $key => $value ){
                    $optiondata = array();
                    if( $value > 0 ){
                        $option = $obj_Options->getRow( sprintf( "option_name='%s'", $key ) );
                        $modata = array(
                            "meeting_key"             => $data["meeting_key"],
                            "option_key"              => $option["option_key"],
                            "meeting_option_quantity" => $value
                            );
                        $result = $obj_MeetingOptions->add( $modata );
                        if (!$result) {
                            $this->_error = 'DB_INSERT_ERROR(MeetingOptions_Add)';
                        }
                    }
                }
            }
            //get created meeting data
            $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND is_deleted=0", $data['meeting_key'] ) );
        }

        //set meeting_session_id
        if ( isset( $meeting_info['meeting_session_id'] ) && $meeting_info['meeting_session_id'] ) {
            return $meeting_info['meeting_session_id'];
        }
    }

    public function checkReservationMeetingStatus( $rooms ) {
        $obj_Meeting        = new DBI_Meeting( $this->dsn );
        $obj_Participant    = new DBI_Participant( $this->dsn );

        foreach ($rooms as $room_key => $meeting_ticket ) {
            $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_ticket='%s'", addslashes($meeting_ticket), "meeting_key, is_active, is_reserved, meeting_stop_datetime, intra_fms , meeting_ticket" ) );
            $status       = $meeting_info["is_active"] ? 1 : 0;
            $is_reserved  = $meeting_info["is_reserved"] ? 1 : 0;
            $pcount       = 0;
            // データが存在する場合
            $participants = array();
            if ($meeting_info) {
                $meeting_key = $meeting_info["meeting_key"];
                $status      = $meeting_info["is_active"];
                $pcount      = $obj_Participant->getCount($meeting_key);
                // 0人
                if ( $meeting_key && $status == 1) {
                    if ($meeting_info["is_reserved"]) {
                        require_once("classes/dbi/reservation.dbi.php");
                        $obj_Reservation = new ReservationTable($this->dsn);
                        $where = "meeting_key = '".addslashes($meeting_info["meeting_ticket"])."'" .
                                " AND reservation_status = 1";
                        $reservation_info = $obj_Reservation->getRow($where);
                        $cur_datetime = time();
                        if (($cur_datetime > strtotime($reservation_info["reservation_extend_endtime"]))) {
                            $this->logger2->info($meeting_info);
                            if ($meeting_info["intra_fms"]) {
                                $this->stopMeeting($meeting_key, true, false);
                            } else {
                                $this->stopMeeting($meeting_key, null, false);
                            }
                        }

                    }
                }
            }
        }
        return true;
    }

    // 終了している会議のConferenceを確認に終了させる
    public function checkConference(){
        require_once("classes/dbi/reservation.dbi.php");
        require_once("classes/dbi/video_conference.dbi.php");
        $obj_VideoConference = new VideoConferenceTable($this->dsn);
        $obj_Meeting        = new DBI_Meeting( $this->dsn );
        $obj_Reservation = new ReservationTable($this->dsn);
        $where = "is_active = 1";
        $active_Conference = $obj_VideoConference->getRowsAssoc($where);
//$this->logger2->info($active_Conference);
        // アクティブなConferenceがなければ終了
        if(!$active_Conference){
            $this->logger2->info("no active conference");
            return;
        }
        // アクティブなカンファレンスあれば予約情報と照らし合わせる
        foreach ($active_Conference as $conference){
            $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_key='%s'", addslashes($conference["meeting_key"])));
            $where = "meeting_key = '".addslashes($meeting_info["meeting_ticket"])."'" .
                    " AND reservation_status = 1";
            $reservation_info = $obj_Reservation->getRow($where);
            $cur_datetime = time();
            if (($cur_datetime > strtotime($reservation_info["reservation_extend_endtime"]))) {
                // カンファレンス終了
                $this->logger2->info($reservation_info);
                $this->logger2->info($conference);
                require_once("classes/mcu/resolve/Resolver.php");

                try {
                    if ( $videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($meeting_info["room_key"], true) ) {
                        if ($videoInfo = $videoConferenceClass->getActiveVideoConferenceByMeetingKey($conference["meeting_key"])) {
                            $videoConferenceClass->cancelReservation( $videoInfo["conference_id"] );
                        }
                    }
                } catch (Exception $e){
                    $this->logger2->warn($e->getMessage());
                    $this->logger2->warn($conference);
                }

            }
        }


    }

    // 電話連携
    private function _getPinCd($user_key) {
        require_once 'classes/mgm/dbi/relation.dbi.php';
        require_once 'classes/dbi/user.dbi.php';
        $objMgmRelationTable = new MgmRelationTable(N2MY_MDB_DSN);
        $objUser             = new UserTable($this->dsn);
        // 1億分のn の リトライ回数乗 の確率で失敗する。
        for ($i = 0; $i < 5; $i++) {
            // 8桁数値の乱数取得
            $pin_cd = str_pad(mt_rand(0, 99999999), 8, "0", STR_PAD_LEFT);
            $where = "relation_type = 'telephone'" .
                     " AND relation_key = '".addslashes($pin_cd)."'";
            if ($objMgmRelationTable->numRows($where) == 0) {
                if ($user_id = $objUser->getOne("user_key = '".addslashes($user_key)."'", "user_id")) {
                    $relation_data = array(
                            "relation_key"  => $pin_cd,
                            "user_key"      => $user_id,
                            "relation_type" => "telephone",
                            "status"        => 1,
                        );
                    $relation_ret = $objMgmRelationTable->add($relation_data);
                    if (DB::isError($relation_ret)) {
                        $this->logger2->error($relation_ret->getUserInfo(), "PINコード発行エラー");
                        return false;
                    } else {
                       return $pin_cd;
                    }
                } else {
                    $this->logger2->warn($user_key, "ユーザーが存在しない");
                    return false;
                }
            } else {
                $this->logger2->warn($pin_cd, "PINコードが重複している");
            }
        }
        $this->logger2->error("リトライ上限数を超え、有効なPINコードを発行できなかった。");
        return false;
    }

    public function getMeetingDetails( $parameters )
    {
        $obj_Meeting         = new DBI_Meeting( $this->dsn );
        $obj_MeetingOptions  = new DBI_MeetingOptions( $this->dsn );
        $obj_Participant     = new DBI_Participant( $this->dsn );
        $obj_FmsServer       = new DBI_FmsServer( N2MY_MDB_DSN );
        $objUserFmsServer    = new DBI_IntraFmsServer($this->dsn);
        $obj_MeetingSequence = new DBI_MeetingSequence( $this->dsn );
        $obj_SharingServer   = new DBI_SharingServer( $this->dsn );
        $params = array(
                        "user_key"              => null,
                        "meeting_session_id"    => null,
                        "member_key"            => null,
                        "participant_id"        => null,
                        "participant_name"      => null,
                        "participant_email"     => null,
                        "participant_station"   => null,
                        "participant_locale"    => 'ja',
                        "participant_country"   => 'jp',
                        "participant_mode_name" => 'general',
                        "participant_type_name" => 'default',
                        "participant_role_name" => 'default',
                        "participant_skin_type" => null,
                        "is_narrowband"         => 0,
                        "is_mobile"         => 0,
                        "mail_upload_address"   => '',
                        "account_model"         => '',
                        "inbound_id"       => null,
                        "inbound_log_key"       => null,
                       );

        $data = array();
        foreach ($params as $idx => $val) {
            $data[$idx] = $parameters[$idx];
            if ($data[$idx] == "") {
                $data[$idx] = $val;
            }
        }

        //---------------------------------------------------------------------
        //get meeting_key
        $where = "meeting_session_id = '".addslashes($data['meeting_session_id'])."'" .
                " AND is_deleted = 0";
        $meeting_info = $obj_Meeting->getRow($where);
        $meeting_key  = $meeting_info["meeting_key"];
        if (!$meeting_info) {
            $this->logger->error(__FUNCTION__."#meeting not found",__FILE__,__LINE__,$meeting_info);
            return $this->error();
        }
        // SSLオプション
        $options = $obj_MeetingOptions->getList($meeting_key);
        $ssl     = 0;
        $use_chinaline_status = 0;
        foreach($options as $option) {
            if ($option["option_name"] == "ssl") {
                $ssl = 1;
            }
            if ($option["option_name"] == "ChinaFastLine") {
                $use_chinaline_status = 1;
            }
            if ($option["option_name"] == "ChinaFastLine" && $option["option_name"] == "ChinaFastLine2") {
                $use_chinaline_status = 2;
            }
            if ($option["option_name"] == "GlobalLink") {
                $use_global_link = 1;
            }
        }
        //利用しない(利用停止中の)FMSを取得
        require_once("classes/dbi/fms_deny.dbi.php");
        try {
            $objFmsDeny = new FmsDenyTable( $this->dsn );
            $_fms_deny_keys = $objFmsDeny->findByUserKey($meeting_info["user_key"], null, null, 0, "fms_server_key");
        } catch (Exception $e) {
            $this->logger2->error($e->getMessage());
            return false;
        }
        $fms_deny_keys = array();
        if ($_fms_deny_keys) {
            foreach ($_fms_deny_keys as $fms_key) {
              $fms_deny_keys[] = $fms_key["fms_server_key"];
            }
        }

        //除外DC
        require_once("classes/dbi/datacenter_ignore.dbi.php");
        $objDatacenterIgnore = new DatacenterIgnoreTable($this->dsn);
        $ignore_dc = $objDatacenterIgnore->get_datacenter_ignore($meeting_info["user_key"]);
        $this->logger2->debug($ignore_dc, "ignore datacenter");

        // FMSサーバ指定無し
        if ( !$meeting_info['server_key'] ) {
            /**
             * FMSサーバ取得
             * intra
             */
            if (1 == $meeting_info["intra_fms"]) {
                $server_key = $objUserFmsServer->getOne(sprintf("user_key='%s'", $meeting_info["user_key"]), "fms_key");
            } else if (0 == $meeting_info["intra_fms"] && $use_global_link) {
                $fms_country_priority = $this->config['FMS_COUNTRY_PRIORITY'][$meeting_info["meeting_country"]];
                if (!$fms_country_priority) {
                    $fms_country_priority = $this->config['FMS_COUNTRY_PRIORITY']["etc"];
                }
                $datacenter_country_list = split(',', $fms_country_priority);
                $server_key = $obj_FmsServer->getGlobalLinkFmsList($ssl, $fms_deny_keys, $datacenter_country_list ,$meeting_info["need_fms_version"]);
            } else if (0 == $meeting_info["intra_fms"]) {
                //優先DC
                require_once("classes/dbi/datacenter_priority.dbi.php");
                $objDatacenterPriority = new DatacenterPriorityTable($this->dsn);
                require_once("classes/dbi/datacenter_priority.dbi.php");
                require_once("classes/core/dbi/DataCenter.dbi.php");
                $objDatacenterPriority = new DatacenterPriorityTable($this->dsn);
                $objDatacenter = new DBI_DataCenter(N2MY_MDB_DSN);
                $priority_dc =$objDatacenterPriority->get_datacenter_priority($meeting_info["user_key"],$meeting_info["meeting_country"]);
                $priority_dc_all =$objDatacenterPriority->get_datacenter_priority($meeting_info["user_key"],"all");
                $priority_dc = array_merge($priority_dc, $priority_dc_all);
                //所在地以外のDCリスト取得
                $fms_country_priority = $this->config['FMS_COUNTRY_PRIORITY'][$meeting_info["meeting_country"]];
                if (!$fms_country_priority) {
                    $fms_country_priority = $this->config['FMS_COUNTRY_PRIORITY']["etc"];
                }
                $datacenter_country_list = split(',', $fms_country_priority);
                $datacenter_list = array();
                foreach ( $datacenter_country_list as $datacenter_country) {
                    if ($meeting_info["meeting_country"] != $datacenter_country_list) {
                        $country_priority_dc_list =
                        $objDatacenterPriority->get_datacenter_priority($meeting_info["user_key"],$datacenter_country);
                    }
                    if ($country_priority_dc_list) {
                        if ($ignore_dc) {
                            $ignore_dc = array_merge($ignore_dc, $country_priority_dc_list);
                        }
                    }
                    $country_dc_list =
                        $objDatacenter->getKeyByCountry($datacenter_country, $ignore_dc,$data["account_model"],$use_chinaline_status);
                    if ($country_dc_list) {
                        foreach( $country_dc_list as $country_dc ){
                            $datacenter_list[] = $country_dc;
                        }
                    }
                    $datacenter_list =
                        array_merge($country_priority_dc_list, $datacenter_list);
                }
                $datacenter_list = array_merge($priority_dc,$datacenter_list);
                $datacenter_list = array_map('serialize', $datacenter_list);
                $datacenter_list = array_unique($datacenter_list);
                $datacenter_list = array_map('unserialize', $datacenter_list);
                $this->logger2->debug($datacenter_list, "datacenter_list");
                if ($datacenter_list) {
                    $server_key = $obj_FmsServer->getKeyByDataCenterList($ssl, $fms_deny_keys, $datacenter_list, $meeting_info["need_fms_version"]);
                } else {
                    $this->logger2->error(array("intra_fms"       => $meeting_info["intra_fms"],
                                                "meeting_country" => $meeting_info['meeting_country'] ,
                                                 $ssl),
                                                "選択可能なデータセンターが存在しませんでした。");
                    return false;
                }
            }

            if (!$server_key) {
                $this->logger2->error(array("intra_fms"       => $meeting_info["intra_fms"],
                                            "meeting_country" => $meeting_info['meeting_country'] ,
                                             $ssl),
                                            "選択可能なFMSが存在しませんでした。");
                return false;
            }

            // FMSシェアリングサーバ取得
            $sharing_server_key = $obj_SharingServer->getKeyByPriority($meeting_info['meeting_country'], $ssl);
            $this->logger2->info(array(
                'meeting_key'        => $meeting_key,
                'server_key'         => $server_key,
                'sharing_server_key' => $sharing_server_key,
                )
            );
            //再度情報取得
            //get meeting_key
            $where = "meeting_session_id = '".addslashes($data['meeting_session_id'])."'" .
                    " AND is_deleted = 0";
            $meeting_info = $obj_Meeting->getRow($where);
            if (!$meeting_info['server_key']) {
                // 更新
                $where = "meeting_key = ".addslashes($meeting_info['meeting_key']);
                // バージョン情報を上書き
                require_once 'classes/dbi/user.dbi.php';
                $objUser   = new UserTable($this->dsn);
                $user_info = $objUser->getRow("user_key = '".addslashes($meeting_info["user_key"])."'", "user_id,is_compulsion_pw,compulsion_pw,meeting_version");
                if (!$user_info["meeting_version"]) {
                    $version = "4.6.5.0";
                } else {
                    $version = $this->getVersion();
                }
                $server_data = array(
                    "version"            => $version,
                    "fms_path"           => "mtg".date("Y/m/d/"),
                    "server_key"         => $server_key,
                    "sharing_server_key" => $sharing_server_key,
                    );
                $ret = $obj_Meeting->update($server_data, $where);
                if (DB::isError($ret)) {
                    $this->logger->error(__FUNCTION__."#update error!",__FILE__,__LINE__,$ret->getUserInfo());
                }
                $server_sequence_data = array("meeting_key" => $meeting_key,
                                              "server_key"  => $server_key,);
                //add server seaquence
                $meeting_sequence_key = $obj_MeetingSequence->add($server_sequence_data);
                if (DB::isError($meeting_sequence_key)) {
                    $this->logger->error(__FUNCTION__."#update error!",__FILE__,__LINE__,$meeting_sequence_key->getUserInfo());
                }
            }
            // 予約なし会議で、名前なしで退出した時の古い会議でバージョンなしを防ぐため
        } elseif (!$meeting_info['version']) {
            $this->logger->info(__FUNCTION__."#versionなし会議",__FILE__,__LINE__,$meeting_info['meeting_key']);
            require_once 'classes/dbi/user.dbi.php';
            $objUser   = new UserTable($this->dsn);
            $user_info = $objUser->getRow("user_key = '".addslashes($data["user_key"])."'", "user_id,is_compulsion_pw,compulsion_pw,meeting_version");
            if (!$user_info["meeting_version"]) {
                $version = "4.6.5.0";
            } else {
                $version     = $this->getVersion();
            }
            $server_data = array( "version" => $version,);
            $where       = "meeting_key = ".addslashes($meeting_info['meeting_key']);
            $ret         = $obj_Meeting->update( $server_data, $where );
        } else {
            if (0 == $meeting_info["intra_fms"]) {
            }
        }

        /**
         * add server seaquence
         * 有効なシーケンスデータがない場合は追加
         */
        $where                = sprintf( "meeting_key='%s'", $meeting_key );
        $sort                 = array( "meeting_sequence_key" => "desc");
        $sequeceInfo          = $obj_MeetingSequence->getRow( $where, null, $sort, 1, 0 );
        $meeting_sequence_key = $sequeceInfo['meeting_sequence_key'];

        if (!$sequeceInfo || ! $sequeceInfo["status"]){
            // 不要なデータを破棄
            $meeting_sequence_data = array("status" => '0');
            $where = "meeting_key='".$meeting_key."'" .
                     " AND status = 1";
            $obj_MeetingSequence->update($meeting_sequence_data, $where);
            $server_sequence_data = array(
                       "meeting_key" => $meeting_key,
                       "server_key"  => $meeting_info["server_key"],
                      );
            //add server seaquence
            $meeting_sequence_key = $obj_MeetingSequence->add($server_sequence_data);
        }
        $this->logger2->debug($meeting_sequence_key, 'meeting_sequence_key');
        //check status
        if (!$meeting_info["is_active"] || $meeting_info["is_active"] == '0' ) {
            return $this->error();
        }
        //add participant information
        $data['meeting_key']            = $meeting_key;
        $data['participant_session_id'] = session_id();
        $data["storage_login_id"] = null;
        if($data["member_key"] != null){
            require_once("classes/dbi/member.dbi.php");
            $obj_member = new MemberTable($this->dsn);
            $data["storage_login_id"] = $obj_member->getOne("member_key = " . $data["member_key"], "member_id");
        }else{
            if($data["user_key"] != null){
                $obj_user = new UserTable($this->dsn);
                $data["storage_login_id"] = $obj_user->getOne("user_key = " . $data["user_key"], "user_id");
            }
        }
        $participant_key                = $obj_Participant->add($data);
        $participant_info               = $obj_Participant->getRow( sprintf( "participant_key='%s'", $participant_key ) );
        $participant_session_id         = $participant_info['participant_session_id'];

        $return = array(
                "user_key"               => $meeting_info["user_key"],
                "room_key"               => $meeting_info["room_key"],
                    "meeting_key"            => $meeting_key,
                    "meeting_ticket"         => $meeting_info["meeting_ticket"],
                    "meeting_name"           => $meeting_info["meeting_name"],
                    "participant_key"        => $participant_key,
                    "lang"                   => $data["participant_locale"],
                    "mail_upload_address"    => $data["mail_upload_address"],
                    "participant_session_id" => $participant_session_id,
                    "participant_type_name"  => $data['participant_type_name'],
                    "meeting_sequence_key"   => $meeting_sequence_key,
                    "ives_conference_id"     => $meeting_info["ives_conference_id"],
                    "ives_did" 			     => $meeting_info["ives_did"]
        );

        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$data);
        switch( $data['participant_type_name'] ) {
            case "compact":
                $return["participant_type_name"]    = "compact";
                break;

            case "telepresence":
                $return["participant_type_name"]    = "telepresence";
                break;

            default:
                $return["participant_type_name"]    = "meeting";
                break;

        }
        return $return;
    }

    /*
     * core/htdocs/api/meeting/MeetingInfoList.php
     */
    public function getMeetingInfoList( $param )
    {
        $obj_Meeting          = new DBI_Meeting( $this->dsn );
        $obj_Participant      = new DBI_Participant( $this->dsn );
        $obj_MeetingSequence  = new DBI_MeetingSequence( $this->dsn );
        $meeting_list         = array();
        $param["is_deleted"]  = 0;
        $where                = $this->getWhereStr($param);
        $sort                 = $param["sort_key"] ? array($param["sort_key"] => $param["sort_type"]) : "";
        $this->logger2->trace($param);

        if (!$where) {
           return $meeting_list;
        }

        $meeting_list = $obj_Meeting->getRowsAssoc($where, $sort, $param["limit"], $param["offset"]);
        if (DB::isError($meeting_list)) {
            $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, $rs->getUserInfo());
            return $meeting_list;
        }

        foreach ($meeting_list as $key => $meeting_info) {
            $_ = array();
            // 利用時間
            $meeting_list[$key]["meeting_use_time"] = $meeting_info["meeting_use_minute"];
            // 参加者一覧
            $meeting_list[$key]["participants"] = $obj_Participant->getList($meeting_info["meeting_key"], false);
            if ($meeting_info["version"]) {
                $meeting_list[$key]["meeting_sequences"] = $obj_MeetingSequence->getRowsAssoc( sprintf( "meeting_key='%s'", $meeting_info["meeting_key"] ), array( "meeting_sequence_key"=>"asc" ) );
            } else {
                $_ = $obj_MeetingSequence->getRow( sprintf( "meeting_key='%s'", $meeting_info["meeting_key"] ), "*", array( "meeting_sequence_key"=>"desc" ), 1 );
                $_["has_recorded_minutes"] = $meeting_info["has_recorded_minutes"];
                $_["has_recorded_video"]   = $meeting_info["has_recorded_video"];
                $meeting_list[$key]["meeting_sequences"] = array($_);
            }
        }
//        $this->logger2->debug(array($param, $meeting_list));
        return $meeting_list;
    }

    function getWhereStr($param) {
        $obj_Participant     = new DBI_Participant( $this->dsn );
        $where_str = "";
        // 終了した会議のみ
        $cond[] = "is_active = 0";
        $cond[] = "meeting_use_minute > 0";
        // participantがあったら情報取得
        if ( $param["participant"] ) {
            $where = "participant_name like '%".addslashes($param['participant'])."%'" .
                    "AND  (participant_type_key = 1 OR participant_type_key = 4 OR participant_type_key = 6 OR participant_type_key = 8)";
            $meeting_keys = $obj_Participant->getCol( $where, "meeting_key" );
            if (DB::isError($meeting_keys)) {
                $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, $rows->getUserInfo());
                $cond[] = "meeting.meeting_key = ''";
            } elseif (!$meeting_keys) {
                $cond[] = "meeting.meeting_key = ''";
            } else {
                $meeting_keys = array_unique($meeting_keys);
                $cond[] = "meeting.meeting_key IN (".join($meeting_keys, ",").")";
            }
        }
        // ユーザーキー
        if ( $param["user_key"] ) {
            $cond[] = "meeting.user_key = '".addslashes( $param["user_key"] )."'";
        }
        // タグ（現在は利用されていない）
        if (isset($param["meeting_tag"]) && $param["meeting_tag"]) {
            $cond[] = "meeting.meeting_tag = '".addslashes( $param["meeting_tag"] )."'";
        }
        // 会議名
        if ( $param["title"] ) {
            $cond[] = "meeting.meeting_name like '%".addslashes( $param["title"] )."%'";
        }
        // 利用日時
        if ($param["start_datetime"] || $param["end_datetime"]) {
            $starttime =  $param["start_datetime"]  ? $param["start_datetime"] : "0000-00-00 00:00:00";
            $endtime = $param["end_datetime"] ? $param["end_datetime"] : "9999-12-31 23:59:59";
            $cond[] = "(" .
                    "( meeting.actual_start_datetime between '".addslashes($starttime)."' AND '".addslashes($endtime)."' )" .
                " OR ( meeting.actual_end_datetime between '".addslashes($starttime)."' AND '".addslashes($endtime)."' )" .
                " OR ( '".addslashes($starttime)."' BETWEEN meeting.actual_start_datetime AND meeting.actual_end_datetime )" .
                " OR ( '".addslashes($endtime)."' BETWEEN meeting.actual_start_datetime AND meeting.actual_end_datetime )" .
                ")";
        }
        // MByte指定
        if ($param["meeting_size_used"]) {
            $cond[] = sprintf( "meeting_size_used >= %d", ($param["meeting_size_used"] - 0.005) * 1024 * 1024 );
        }
        // 利用時間（分）
        if ($param["meeting_use_minute"]) {
            $cond[] = sprintf("meeting_use_minute > %d", $param["meeting_use_minute"]);
        }
        // 議事録フラグ
        if (isset($param["has_recorded_minutes"])) {
            $cond[] = "has_recorded_minutes = ".($param["has_recorded_minutes"] ? 1 : 0);
        }
        // 録画フラグ
        if (isset($param["has_recorded_video"])) {
            $cond[] = "has_recorded_video = ".($param["has_recorded_video"] ? 1 : 0);
        }
        // 予約
        if (isset($param["is_reserved"])) {
            $cond[] = "is_reserved = ".($param["is_reserved"] ? 1 : 0);
        }
        // 保護
        if (isset($param["is_locked"])) {
            $cond[] = "is_locked = ".($param["is_locked"] ? 1 : 0);
        }
        // オンデマンド
        if (isset($param["is_publish"])) {
            $cond[] = "is_publish = ".($param["is_publish"] ? 1 : 0);
        }
        // 削除フラグ
        if (isset($param["is_deleted"])) {
            $cond[] = "is_deleted = ".($param["is_deleted"] ? 1 : 0);
        }
        $where_str = join(" AND ", $cond);

        // ユーザ全件
        if (!$param["room_key"] && !$param["member_key"]) {
          $session   = EZSession::getInstance();
          if($session->get("service_mode") == "sales") {
            $where_str .= " AND meeting.room_key IN (SELECT room.room_key FROM room WHERE room.use_sales_option=1 AND room.user_key ='".$param["user_key"]."')";
          } elseif ($session->get("service_mode") == "meeting") {
            $where_str .= " AND meeting.room_key IN (SELECT room.room_key FROM room WHERE room.use_sales_option!=1 AND room.user_key ='".$param["user_key"]."')";
          }
        // 指定部屋のみ
        } elseif ($param["room_key"] && !$param["member_key"]) {
            if (is_array($param["room_key"])) {
                $where_str .= " AND meeting.room_key = '". join("','", $param["room_key"]) ."'";
            } else {
                $where_str .= " AND meeting.room_key = '".addslashes( $param["room_key"] )."'";
            }
        // 招待された予約のみ
        } elseif (!$param["room_key"] && $param["member_key"]) {
            $where_str .= " AND meeting_key IN (SELECT participant.meeting_key ".
                           "FROM participant WHERE participant.member_key = '".addslashes($param["member_key"])."' ".
                           "GROUP BY participant.meeting_key)";
        } elseif ($param["room_key"] && $param["member_key"]) {
            if (is_array($param["room_key"])) {
                $where_str .= " AND meeting.room_key = '". join("','", $param["room_key"]) ."'" .
                              " OR (".$where_str." ".
                               "AND meeting_key IN (SELECT participant.meeting_key ".
                               "FROM participant WHERE participant.member_key = '".addslashes($param["member_key"])."' ".
                               "GROUP BY participant.meeting_key))";
            } else {
                $where_str .= " AND meeting.room_key = '".addslashes( $param["room_key"] )."'" .
                              " OR (".$where_str." AND meeting_key IN (SELECT participant.meeting_key ".
                               "FROM participant WHERE participant.member_key = '".addslashes($param["member_key"])."' ".
                               "GROUP BY participant.meeting_key))";
            }
        }

        $this->logger2->debug($where_str);
        return $where_str;
    }

    /**
     * core/htdocs/api/meeting/MeetingInfoListAll.php
     */
    public function getMeetingInfoListAll( $parameters)
    {
        $obj_Participant     = new DBI_Participant( $this->dsn );
        $obj_N2MY_DB        = new N2MY_DB( $this->dsn );

        $params = array(
                        "room_key"          => null,
                        "meeting_tag"       => null,
                        "title"             => null,
                        "start_datetime"    => null,
                        "end_datetime"      => null,
                        "member_key"        => null,
                        "limit"             => null,
                        "offset"            => null,
                        "sort_key"          => "meeting_start_datetime",
                        "sort_type"         => "desc"
                       );

        $data = array();
        foreach ($params as $idx => $val) {
            $data[$idx] = $parameters[$idx];
            if ($data[$idx] == "") {
                $data[$idx] = $val;
            }
        }

        //---------------------------------------------------------------------
        //get meeting information
        $sql = "SELECT * FROM meeting" .
                " WHERE meeting.is_active = 0" .
                " AND meeting_use_minute > 0";
        // 部屋IDは必ずいる
        if ($data["room_key"]) {
            $room_keys = split( ",", $data["room_key"] );
            $room_key_list = "";
            foreach($room_keys as $_key => $_room_key) {
                if ($room_key_list) {
                    $room_key_list .= ",";
                }
                $room_key_list .= "'".addslashes($_room_key)."'";
            }
            $sql .= " AND meeting.room_key in (".$room_key_list.")";
        } else {
            $sql .= " AND meeting.room_key = ''";
        }
        if ($data["meeting_tag"]) {
            $sql .= " AND meeting.meeting_tag = '".addslashes( $data["meeting_tag"] )."'";
        }
        if ($data["title"]) {
            $sql .= " AND meeting.meeting_name like '%".addslashes($data["title"])."%'";
        }
        if ($data["start_datetime"] || $data["end_datetime"]) {
            $starttime = ($data["start_datetime"]) ? $data["start_datetime"] : "0000-00-00 00:00:00";
            $endtime = ($data["end_datetime"]) ? $data["end_datetime"] : "9999-12-31 23:59:59";
            $sql .= " AND (" .
                    "( meeting.actual_start_datetime between '".$starttime."' AND '".$endtime."' )" .
                " OR ( meeting.actual_end_datetime between '".$starttime."' AND '".$endtime."' )" .
                " OR ( '".$starttime."' BETWEEN meeting.actual_start_datetime AND meeting.actual_end_datetime )" .
                " OR ( '".$endtime."' BETWEEN meeting.actual_start_datetime AND meeting.actual_end_datetime )" .
                ")";
        }
        if ($data["member_key"]) {
            $sql = " AND meeting_key IN (SELECT participant.meeting_key FROM participant WHERE participant.member_key = '".addslashes($data["member_key"])."' GROUP BY participant.meeting_key)";
        }
        // クエリ生成
        if ($data["sort_key"]) {
            $sql .= " ORDER BY " . $data["sort_key"];
            if (strtolower($data["sort_type"]) == "desc") {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }
        }

        if ( ($data["limit"] !== null) && ($data["limit"] !== "") && is_numeric($data["limit"])) {
            if (!$data["offset"]) {
                $offset = 0;
            }
            $rs = $obj_N2MY_DB->_conn->limitQuery($sql, $offset, $data["limit"]);
        } else {
            $rs = $obj_N2MY_DB->_conn->query($sql);
        }
        if (DB::isError($rs)) {
            $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, $rs->getUserInfo());
        }
        $return = array();
        while ($_row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            // 利用時間
            $_row["meeting_use_time"] = $_row["meeting_use_minute"];
            // 参加者一覧
            $participant_list = $obj_Participant->getList($_row["meeting_key"], false);
            $_row["participants"] = $participant_list;
            $return[] = $_row;
        }
        return $return;
    }

    //api/meeting/MeetingStop.php
    public function stopMeeting($meeting_key, $size = null, $sleep = true ) {
        $obj_Meeting            = new DBI_Meeting( $this->dsn );
        $obj_MeetingUseLog      = new DBI_MeetingUseLog( $this->dsn );
        $obj_Participant        = new DBI_Participant( $this->dsn );
        $obj_MeetingSequence    = new DBI_MeetingSequence( $this->dsn );
        $obj_MeetingUptime      = new DBI_MeetingUptime( $this->dsn );

        $where = "meeting_key = ".$meeting_key.
            " AND is_deleted = 0";
        $meeting_info = $obj_Meeting->getRow($where);

        // 参加者を全員退出
        $obj_Participant->updateList($meeting_key, array());
        // 会議利用時間テーブル終了（会議以外からの終了も含め）
        $obj_MeetingUptime->stop($meeting_key);
        // 延長
        $is_active = false;
        if ($meeting_info['is_reserved']) {
            require_once("classes/dbi/reservation.dbi.php");
            $obj_Reservation = new ReservationTable($this->dsn);
            $where = "meeting_key = '".addslashes($meeting_info["meeting_ticket"])."'" .
                    " AND reservation_status = 1".
                    " AND reservation_extend_endtime > '".addslashes(date("Y-m-d H:i:s"))."'";
            $reservation_info = $obj_Reservation->getRow($where);
            if (DB::isError($reservation_info)) {
                $this->logger2->error($reservation_info->getUserInfo());
                return false;
            } elseif ($reservation_info) {
                $is_active = true;
            }
        }
        if (!$is_active) {
            $obj_Meeting->stop($meeting_key, array());
        }
        //----------------------
        // 利用時間,録画容量
        //----------------------
        $where = "meeting_key = ".$meeting_key .
                 " AND type IS NULL ";
        $usetime = $obj_MeetingUseLog->numRows($where);

        /**
         * polycom conferenceが開催された場合 meeting_uptime tableの数値を参照する
         */

        require_once("classes/mcu/resolve/Resolver.php");
        if (($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($meeting_info["room_key"]))) {
      if ($videoInfo = $videoConferenceClass->getVideoConferenceByMeetingKey($meeting_key)) {
              $usetime = $videoConferenceClass->getUsageTimeFromMeetingUpTime($meeting_key);
      }
        }

        if ($usetime) {
            // 全FMSのステータス変更
            // 4.4.1追記　intra利用時(services/api.php?action_stop_meeting_by_fmswebより叩かれた場合)はfmsに取得しにいかない
            if ($size === null) {
                $where = "meeting_key = ".$meeting_key.
                         " AND (has_recorded_minutes = 1 OR has_recorded_video = 1)";
                $meeting_sequence_list = $obj_MeetingSequence->getCol($where, "meeting_sequence_key");
                foreach ($meeting_sequence_list as $meeting_sequence_key) {
                    $where = "meeting_sequence_key = ".$meeting_sequence_key;
                    $data  = array("status" => 0);
                    $obj_MeetingSequence->update($data, $where);
                }
            }
            $where = "meeting_key = ".$meeting_key;
            // 現在の状態を取得(各FMSの容量および、利用フラグを取得)
            $meeting_sequence_status = $obj_MeetingSequence->getStatus($meeting_key);
            // 終了時間を取得
            $actual_end_datetime = $obj_MeetingUptime->getOne($where, "meeting_uptime_stop", array("meeting_uptime_key" => "desc"));
        } else {
            $user_table = new UserTable($this->dsn);
            $user       = $user_table->find($meeting_info['user_key']);
            if (!$user) {
                $this->logger->warn('user not found'.print_r($user , true));
                return false;
            }
            if (!$is_active) {
                require_once ("classes/dbi/meeting_clip.dbi.php");
                $meeting_clip_table = new MeetingClipTable($this->dsn);
                try {
                    $meeting_clip_table->deleteByMeetingKey($meeting_info['meeting_key']);
                } catch (Exception $e) {
                    $this->logger2->error('deleteClipRelations meeting_key:'.$meeting_info['meeting_key']);
                    return false;
                }
            }
        }
        $meeting_sequence_status["meeting_use_minute"]  = $usetime;
        $meeting_sequence_status["actual_end_datetime"] = $actual_end_datetime;
        //action_set_fmssize_by_fms処理した後で会議中止
        $where = "meeting_key = ".$meeting_key;
        $last_meeting_sequence_key = $obj_MeetingSequence->getOne($where, "meeting_sequence_key",array("meeting_sequence_key" => "desc"));
        $objFmsWatch = new FmsWatchTable($this->dsn);
        $where = sprintf("meeting_sequence_key= %d AND status = 2", $last_meeting_sequence_key);
        $fmsWatchInfo = $objFmsWatch->getRow($where);
        if($fmsWatchInfo) {
            if ($meeting_sequence_status["has_recorded_minutes"]) {
                $document_size = $this->getDocumentSize($meeting_key);
                $meeting_sequence_status["meeting_size_used"] += $document_size;
            }
        }
        $this->logger2->debug(array("meeting_key"   => $meeting_key,
                                   "meeting_info"  => $meeting_sequence_status));
        $where = "meeting_key = ".$meeting_key;
        $obj_Meeting->update($meeting_sequence_status, $where);


        //インバウンド用会議優先再入室フラグ
        $this->logger2->info($meeting_info);
        if ($meeting_info["inbound_flg"]) {
            $use_seconds = strtotime($meeting_sequence_status["actual_end_datetime"]) - strtotime($meeting_info["actual_start_datetime"]);
            require_once("classes/dbi/inbound.dbi.php");
            $obj_Inbound = new InboundTable ($this->dsn);
            $where = "user_key = ".$meeting_info["user_key"].
                 " AND inbound_id = '".$meeting_info["inbound_id"]."'";
            $inbound_info = $obj_Inbound->getRow($where);
            if ($inbound_info["reenter_time"] > $use_seconds) {
                //再入室時の優先フラグを付ける
                $this->logger->debug("reenter",__FILE__,__LINE__,$use_seconds);
                $room_update = array("reenter_flg" => "1");
            } else {
                $room_update = array("reenter_flg" => "0");
            }
        } else if ($meeting_info["use_flg"]) {
            $room_update = array("reenter_flg" => "0");
        }
        require_once("classes/dbi/room.dbi.php");
        $obj_Room = new RoomTable($this->dsn);
        $where_room = "room_key = '".$meeting_info["room_key"]."'";
        $obj_Room->update($room_update, $where_room);

        // ファイルキャビネット削除
        if( 0 == $meeting_info['use_flg']) {
            $objFileUpload = new N2MY_FileUpload();
            $dir           = $objFileUpload->getDir( $meeting_key );
            $destDir       = $objFileUpload->getFullPath( $dir );
            $objFileUpload->deleteDir( $destDir );

            require_once( "classes/dbi/file_cabinet.dbi.php" );
            $objFileCabinet = new FileCabinetTable( $this->dsn );
            $fileData       = array("status"=> 0,
                                    "updatetime" => date( "Y-m-d H:i:s" ));
            $where = sprintf( "meeting_key='%s'", $meeting_key );
            $objFileCabinet->update( $fileData, $where );
        }
        // 会議終了
        if (!$is_active) {
            //----------------------
            // 参加者情報を索引として登録
            //----------------------
            $where = "meeting_key = ".$meeting_key.
                     " AND use_count > 0".
                     " AND is_active = 0";
            $participant_list = $obj_Participant->getRowsAssoc($where, null, null, null, "member_key,participant_name");
            $participant_ids   = array();
            $participant_names = array();
            $member_keys       = array();
            foreach($participant_list as $_key => $participant) {
                if ($participant["member_key"]) {
                    $member_keys[] = $participant["member_key"];
                }
                $participant_names[] = $participant["participant_name"];
            }
            $participant_ids = array_unique($participant_ids);
            // 改行コードできって検索しやすくする
            $participant_info["member_keys"]       = join($member_keys,"\n");
            $participant_info["participant_names"] = join($participant_names,"\n");
            $where = "meeting_key = ".$meeting_key;
            $obj_Meeting->update($participant_info, $where);
            //----------------------
            // エコメーター
            //----------------------
            if ($usetime) {
                // ECO情報の更新
                require_once("classes/N2MY_Eco.class.php");
                $obj_Eco = new N2MY_Eco($this->dsn, N2MY_MDB_DSN);
                $obj_Eco->createReport($meeting_key, "", 1);// ECO情報の更新: 最短ルート算出、メール送信有り
            }
            //----------------------
            // 会議内招待メールの非アクティブ化
            //----------------------
            require_once "classes/dbi/meeting_invitation.dbi.php";
            $objMeetingInvitationTable = new MeetingInvitationTable($this->dsn);
            $invitationData            = array("status"=>9);
            $objMeetingInvitationTable->update($invitationData, sprintf("meeting_key='%s'", $meeting_key));
            $this->logger2->debug($invitationData);
            //----------------------
            // PINコード削除
            //----------------------
            if (!$meeting_info["pin_cd"]) {
                return;
            }
            require_once "classes/mgm/dbi/relation.dbi.php";
            $objMgmRelationTable = new MgmRelationTable(N2MY_MDB_DSN);
            $where = "relation_type = 'telephone'" .
                     " AND relation_key = '".addslashes($meeting_info["pin_cd"])."'";
            $ret = $objMgmRelationTable->remove($where);
            if (DB::isError($ret)) {
                $this->logger2->error($ret->getUserInfo());
                return;
            }
            $this->logger2->info(array("meeting_session_id" => $meeting_info["meeting_session_id"],
                                 "pin_cd"                   => $meeting_info["pin_cd"]),
                                 "PINコードを破棄しました。");

            // polycom
        require_once("classes/mcu/resolve/Resolver.php");
        if (($videoConferenceClass = Resolver::getConferenceOptionClassByRoomKey($meeting_info["room_key"]))) {
              $videoConferenceClass->deleteTemplateGuestDid($meeting_info["temporary_did"]);
          }
        }
    }

    function changeFMS($meeting_key, $server_key) {
        $this->logger2->info(array(
            "meeting_key"  => $meeting_key,
            "server_key"   => $server_key
            ));
        $obj_Meeting         = new DBI_Meeting( $this->dsn );
        $obj_MeetingSequence = new DBI_MeetingSequence( $this->dsn );
        $meeting_info        = $obj_Meeting->getRow(sprintf( "meeting_key='%s' AND  is_deleted = 0" , $meeting_key));
        if (!$meeting_key || !$server_key) {
            return false;
        }

        if ($meeting_info["server_key"] == $server_key) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,"同じFMSに変更不可");
            return false;
        }
        // データセンタ切り替え
        $where  = "meeting_key = ".$meeting_key;
        $data   = array("update_datetime" => date("Y-m-d H:i:s"),
                        "server_key" => $server_key,);
        $result = $obj_Meeting->update($data, $where);
        if (DB::isError($result)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$result->getUserInfo());
            return false;
        }
        // ミーティングシーケンス追加
        $result = $obj_MeetingSequence->chagenSequenceData($meeting_key, $server_key);
        if (DB::isError($result)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$result->getUserInfo());
            return false;
        }
        return true;
    }


    /**
     * データセンター切り替え
     *
     */
    function changeDataCenter($meeting_sequence_key, $has_recorded_minutes, $has_recorded_video, $rec_time = array())
    {
        require_once("classes/core/Core_Meeting.class.php");
        require_once("classes/core/dbi/MeetingSequence.dbi.php");
        $obj_MeetingSequence = new DBI_MeetingSequence( $this->dsn );
        $obj_CoreMeeting     = new Core_Meeting( $this->dsn, N2MY_MDB_DSN );
        $where               = "meeting_sequence_key = ".$meeting_sequence_key;
        $meeting_key         = $obj_MeetingSequence->getOne($where, "meeting_key");
        $this->logger2->info(array(
            "meeting_key"           => $meeting_key,
            "meeting_sequence_key"  => $meeting_sequence_key,
            "has_recorded_minutes"  => $has_recorded_minutes,
            "has_recorded_video"    => $has_recorded_video,
            "record_second"         => $rec_time
            ));
        //サイズは別途FlashGatway側のgetFmsSizeで取得するように変更
        $data = array(
            "has_recorded_minutes" => $has_recorded_minutes,
            "has_recorded_video"   => $has_recorded_video,
            "meeting_size_used"    => 0,
            "status"               => 0,
            "record_second"        => (array_sum($rec_time) / 1000)
            );
        $result = $obj_MeetingSequence->update($data, $where);
        if (DB::isError($result)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$result->getUserInfo());
            return false;
        }
        return true;
    }

    //api/meeting/MeetingReservationCancel.php
    public function cancelReservation( $param )
    {
        $obj_Meeting         = new DBI_Meeting( $this->dsn );
        $obj_Participant     = new DBI_Participant( $this->dsn );
        $obj_MeetingSequence = new DBI_MeetingSequence( $this->dsn );
        $obj_MeetingUptime   = new DBI_MeetingUptime( $this->dsn );

        $meeting_info        = $obj_Meeting->getRow( sprintf( "meeting_ticket='%s' AND is_deleted=0", $param["meeting_ticket"] ) );
        $next_meeting_key    = $obj_Meeting->getNextKey( $param['room_key'], $meeting_info["meeting_key"] );
        $result  = false;
        if ( $meeting_info ) {
            $data = array(
                "is_deleted"    => 1,
                "is_active"     => 0,
                "pin_cd"        => "",
                );
            $where = sprintf( "meeting_key=%s", $meeting_info["meeting_key"] );
            $result = $obj_Meeting->update( $data, $where );
            //----------------------
            // PINコード削除
            //----------------------
            if ($meeting_info["pin_cd"]) {
                $objMgmRelationTable = new MgmRelationTable(N2MY_MDB_DSN);
                $where = "relation_type = 'telephone'" .
                    " AND relation_key = '".addslashes($meeting_info["pin_cd"])."'";
                $ret = $objMgmRelationTable->remove($where);
                if (DB::isError($ret)) {
                    $this->logger2->error($ret->getUserInfo());
                } else {
                    $this->logger2->info(array(
                        "meeting_session_id" => $meeting_info["meeting_session_id"],
                        "pin_cd"             => $meeting_info["pin_cd"]
                        ), "PINコードを破棄しました。");
                }
            }
        }
        if (DB::isError($result)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$result->getUserInfo());
        }
    }

    function getCount( $param ) {
        //api/meeting/MeetingCountList.php
        $param = array(
            "user_key"      => $param["user_key"]       ? $param["user_key"]       : null,
            "room_key"      => $param["room_key"]       ? $param["room_key"]       : null,
            "member_key"    => $param["member_key"]     ? $param["member_key"]     : null,
            "title"         => $param["title"]          ? $param["title"]          : null,
            "meeting_tag"   => $param["meeting_tab"]    ? $param["meeting_tab"]    : null,
            "participant"   => $param["participant"]    ? $param["participant"]    : null,
            "delete"        => $param["delete"]         ? $param["delete"]         : 0,
            "start_datetime"=> $param["start_datetime"] ? $param["start_datetime"] : null,
            "end_datetime"  => $param["end_datetime"]   ? $param["end_datetime"]   : null,
        );
        $count = 0;
        //participantがあったら情報取得
        if ($param['participant']) {
            $obj_Participant  = new DBI_Participant( $this->dsn );
            $sql = "participant_name like '%".addslashes($param['participant'])."%'" .
                   "AND  (participant_type_key = 1 OR participant_type_key = 4)";
            $rows =$obj_Participant->getRowsAssoc($sql, null, null, null, null, "meeting_key");
        }
        if (DB::isError($rows)) {
            print $rows->getUserInfo();
        }
        // 参加者の検索結果が0だったら検索終了
        if ($param["participant"] && !$rows) {
            return $count;
        }
        //get meeting information
        $sql = "SELECT COUNT(meeting_key) as meeting_count" .
                ",SUM(meeting_use_minute) as total_use_time" .
                " FROM meeting" .
                " WHERE is_active = 0" .
                " AND is_deleted = ".addslashes( $param["delete"] ).
                " AND meeting_use_minute > 0";
        // ユーザーキー
        if ( $param["user_key"] ) {
            $sql .= " AND meeting.user_key = '".addslashes( $param["user_key"] )."'";
        }
        // 部屋キー
        if ( $param["room_key"] ) {
            if (is_array($param["room_key"])) {
                $sql .= " AND meeting.room_key = '". join("','", $param["room_key"]) ."'";
            } else {
                $sql .= " AND meeting.room_key = '".addslashes( $param["room_key"] )."'";
            }
        }

        // participantの情報があったら読み込む
        if ($rows) {
            $sql .= " AND (";
            foreach ($rows as $_key => $value) {
                if ($_key != "0") {
                    $sql .= " OR ";
                }
                $sql .= "meeting_key = '".addslashes($value['meeting_key'])."'";
            }
            $sql .= ")";
        }
        if ( $param["meeting_tag"] ) {
            $sql .= " AND meeting_tag = '".addslashes( $param["meeting_tag"] )."'";
        }
        if ( $param["title"] ) {
            $sql .= " AND meeting_name like '%".addslashes( $param["title"] )."%'";
        }
        if ( $param["start_datetime"] || $param["end_datetime"] ) {
            $starttime = ($param["start_datetime"]) ? $param["start_datetime"] : "0000-00-00 00:00:00";
            $endtime = ($param["end_datetime"]) ? $param["end_datetime"] : "9999-12-31 23:59:59";
            $sql .= " AND (" .
                    "( actual_start_datetime between '".$starttime."' AND '".$endtime."' )" .
                " OR ( actual_end_datetime between '".$starttime."' AND '".$endtime."' )" .
                " OR ( '".$starttime."' BETWEEN actual_start_datetime AND actual_end_datetime )" .
                " OR ( '".$endtime."' BETWEEN actual_start_datetime AND actual_end_datetime )" .
                ")";
        }
        if ($param["member_key"]) {
            $sql .= " AND meeting_key IN (SELECT participant.meeting_key FROM participant WHERE participant.member_key = '".addslashes($param["member_key"])."' GROUP BY participant.meeting_key)";
        }
        $obj_Meeting = new DBI_Meeting( $this->dsn );
        $rs          = $obj_Meeting->_conn->query( $sql );
        $ret         = array();
        if (DB::isError($rs)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$rs->getUserInfo());
            return $count;
        }
        $result = array();
        $row    = $rs->fetchRow(DB_FETCHMODE_ASSOC);
        $this->logger2->debug(array($param, $row));
        return $row["meeting_count"];
    }

    public function getSummary($param) {
        $obj_Participant     = new DBI_Participant( $this->dsn );
        $count               = 0;
        $param["is_deleted"] = isset($param["delete"]) ? $param["delete"] : 0;
        //get meeting information
        $sql = "SELECT COUNT(meeting_key) as meeting_count" .
                ",SUM(meeting_use_minute) as total_use_time" .
                ",SUM(meeting_size_used) as total_size_used" .
                " FROM meeting";
        if (!$where = $this->getWhereStr($param)) {
            return array();
        }

        $sql        .= " WHERE ".$where;
        $obj_Meeting = new DBI_Meeting( $this->dsn );
        $rs          = $obj_Meeting->_conn->query( $sql );
        $ret         = array();

        if (DB::isError($rs)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$rs->getUserInfo());
            return $count;
        }
        $result = array();
        $row    = $rs->fetchRow(DB_FETCHMODE_ASSOC);
        $this->logger2->debug(array($param, $row));

        return $row;
    }

    /**
     *
     */
    public function getParticipatedMeetingList( $param )
    {
        require_once('classes/N2MY_DBI.class.php');
        $obj_MeetingSequence  = new DBI_MeetingSequence( $this->dsn );

        $sql = sprintf(
                   "SELECT
                        meeting.*
                    FROM
                        meeting,
                        (
                            SELECT
                                DISTINCT( meeting_key ) as meeting_key
                                FROM
                                    participant
                                WHERE
                                    member_key = %d
                        ) participant
                    WHERE
                        meeting.meeting_key = participant.meeting_key AND
                        meeting.use_flg = 1 AND
                        meeting.is_active = 0 AND
                        meeting.is_deleted = 0
                    ORDER BY meeting_key DESC
                    ", $param["member_key"] );
        $obj_N2MYDB = new N2MY_DB( $this->dsn );
        $rs         = $obj_N2MYDB->_conn->limitQuery( $sql, $param["offset"], $param["limit"] );
        $ret        = array();
        if (DB::isError($rs)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$rs->getUserInfo());
            return false;
        }
        $obj_Participant = new DBI_Participant( $this->dsn );
        $result          = array();
        while ( $ret = $rs->fetchRow(DB_FETCHMODE_ASSOC) ) {
            $ret["participants"] = $obj_Participant->getList($ret["meeting_key"], false);

            if( $ret["version"] ){
                $ret["meeting_sequences"] = $obj_MeetingSequence->getRowsAssoc( sprintf( "meeting_key='%s'", $ret["meeting_key"] ), array( "meeting_sequence_key"=>"asc" ) );
            } else {
                $_[] = $obj_MeetingSequence->getRow( sprintf( "meeting_key='%s'", $ret["meeting_key"] ), array( "meeting_sequence_key"=>"desc" ), 1 );
                $_[] = $obj_MeetingSequence->getLatestData($ret["meeting_key"]);
                $ret["meeting_sequences"] = $_;
            }
            $result[] = $ret;
        }
        return $result;
    }

    public function getParticipatedInfo( $member_key)
    {
        require_once('classes/N2MY_DBI.class.php');
        $obj_MeetingSequence  = new DBI_MeetingSequence( $this->dsn );

        $sql = sprintf(
                   "SELECT
                        meeting.*
                    FROM
                        meeting,
                        (
                            SELECT
                                DISTINCT( meeting_key ) as meeting_key
                                FROM
                                    participant
                                WHERE
                                    member_key = %d
                        ) participant
                    WHERE
                        meeting.meeting_key = participant.meeting_key AND
                        meeting.use_flg = 1 AND
                        meeting.is_active = 0 AND
                        meeting.is_deleted = 0
                    ORDER BY meeting_key DESC;
                    ", $member_key );
        $obj_N2MYDB = new N2MY_DB( $this->dsn );
        $rs         = $obj_N2MYDB->_conn->query( $sql );
        $result     = array();
        if (DB::isError($rs)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$rs->getUserInfo());
            return false;
        }
        $total_size = 0;
        while ($ret = $rs->fetchRow( DB_FETCHMODE_ASSOC ) )
        {
            $total_size += $ret["meeting_size_used"];
            $result[] = $ret;
        }
        $result["count"]          = count( $result );
        $result["total_log_size"] = $total_size;
        return $result;
    }

    /**
     * MeetingSequenceより移動
     */
    public function deleteMinute($meeting_sequence_key)
    {
        // FMS PATH取得
        $obj_Meeting           = new DBI_Meeting($this->dsn);
        $where                 = "meeting_sequence_key = '".mysql_real_escape_string($meeting_sequence_key)."'";
        $obj_MeetingSequence   = new DBI_MeetingSequence($this->dsn);
        $meeting_sequence_data = $obj_MeetingSequence->getRow($where);

        $obj_Meeting  = new DBI_Meeting($this->dsn);
        $where        = sprintf("meeting_key='%s'", $meeting_sequence_data['meeting_key']);
        $meeting_info = $obj_Meeting->getRow($where);

        /**
         * intrafmsを利用していない会議のみ
         */
        if (0 == $meeting_info["intra_fms"] &&
            "1" == $meeting_sequence_data["has_recorded_minutes"]) {

            require_once("classes/mgm/dbi/FmsServer.dbi.php");
            $obj_FmsServer = new DBI_FmsServer(N2MY_MDB_DSN);
            $server        = $obj_FmsServer->getRow(sprintf("server_key=%d", $meeting_sequence_data["server_key"]));

            $fms_address  = $server["local_address"] ? $server["local_address"] : $server["server_address"];
            $fms_app_name = $this->config["CORE"]["app_name"];
            $meeting_id   = $obj_Meeting->getMeetingID($meeting_sequence_data['meeting_key']);
            $url = "http://".$fms_address.":".$server["server_port"]."/fms_delete.php" .
                   "?mode=minutes&app_name=".$fms_app_name.
                   "&key=".$meeting_info["fms_path"].$meeting_id.
                   "&sequence_key=".$meeting_sequence_key;
            $this->logger2->debug($url);
            $ch = curl_init();
            $option = array(
                CURLOPT_URL            => $url ,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_CONNECTTIMEOUT => 10,
                CURLOPT_TIMEOUT        => 10,
                );
            curl_setopt_array($ch, $option);
            $remaining_size = curl_exec($ch);
            $this->logger2->info(array($url, $remaining_size));
        }
        // 共有メモサイズ取得
        require_once("classes/dbi/shared_file.dbi.php");
        $objSharedFile = new SharedFileTable($this->dsn);
        $sharedFileSize = $objSharedFile->getSequenceFileSize($meeting_sequence_key);
    // DB更新 meeting_sequenceテーブル更新
        $where = "meeting_sequence_key = ".$meeting_sequence_key;
        $data = array(
                "meeting_size_used"    => $remaining_size + $sharedFileSize,
                "has_recorded_minutes" => 0
                );
        $where = sprintf("meeting_sequence_key = '%s'", $meeting_sequence_key);
        $ret = $obj_MeetingSequence->update($data, $where);
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
            return false;
        }
        return true;
    }

    /**
     * meetingテーブルの議事録を削除(MeetingSequenceの議事録が全て削除されていたら)
     * @param $meeting_key
     * @return boolean
     */
    public function deleteMeetingTblMinute($meeting_key){

      $obj_Meeting           = new DBI_Meeting($this->dsn);
      $obj_MeetingSequence   = new DBI_MeetingSequence($this->dsn);
      $where =  sprintf( "meeting_key='%s' AND has_recorded_minutes=1", $meeting_key );
      $sequenceList = $obj_MeetingSequence -> getRowsAssoc( $where );

      if(count($sequenceList) == 0){
             // DB更新 meetingテーブル更新
          $meeting_data = array(
              "has_recorded_minutes" => 0,
              );
          $meeting_where = sprintf("meeting_key='%s'", $meeting_key);
          $meeting_ret = $obj_Meeting->update($meeting_data, $meeting_where);
          if (DB::isError($meeting_ret)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$meeting_ret->getUserInfo());
            return false;
          }
      }
      return true;

    }

    /**
     * MeetingSequenceより移動
     */
    public function deleteVideo($meeting_sequence_key)
    {
        // FMS PATH取得
        $obj_Meeting           = new DBI_Meeting($this->dsn);
        $where                 = "meeting_sequence_key = '".mysql_real_escape_string($meeting_sequence_key)."'";
        $obj_MeetingSequence   = new DBI_MeetingSequence($this->dsn);
        $meeting_sequence_data = $obj_MeetingSequence->getRow($where);

        $obj_Meeting  = new DBI_Meeting($this->dsn);
        $where        = sprintf("meeting_key='%s'", $meeting_sequence_data['meeting_key']);
        $meeting_info = $obj_Meeting->getRow($where);

        /**
         * intrafmsを利用していない会議のみ
         */
        if (0 == $meeting_info["intra_fms"] &&
            "1" == $meeting_sequence_data["has_recorded_video"]) {

            require_once("classes/mgm/dbi/FmsServer.dbi.php");
            $obj_FmsServer = new DBI_FmsServer( N2MY_MDB_DSN );
            $server        = $obj_FmsServer->getRow(sprintf("server_key=%d", $meeting_sequence_data["server_key"]));

            $fms_address   = $server["local_address"] ? $server["local_address"] : $server["server_address"];
            $fms_app_name  = $this->config["CORE"]["app_name"];
            $meeting_id    = $obj_Meeting->getMeetingID($meeting_sequence_data['meeting_key']);
            $url = "http://".$fms_address.":".$server["server_port"]."/fms_delete.php" .
                   "?mode=video&app_name=".$fms_app_name.
                   "&key=".$meeting_info["fms_path"].$meeting_id.
                   "&sequence_key=".$meeting_sequence_key;
            $this->logger2->debug($url);
            $ch = curl_init();
            $option = array(
                CURLOPT_URL            => $url ,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_CONNECTTIMEOUT => 10,
                CURLOPT_TIMEOUT        => 10,
                );
            curl_setopt_array($ch, $option);
            $remaining_size = curl_exec($ch);
            $this->logger2->info(array($url, $remaining_size));
        }
        // 共有メモサイズ取得
        require_once("classes/dbi/shared_file.dbi.php");
        $objSharedFile = new SharedFileTable($this->dsn);
        $sharedFileSize = $objSharedFile->getSequenceFileSize($meeting_sequence_key);

        // DB更新 meeting_sequenceテーブル更新
        $where = sprintf("meeting_sequence_key = '%s'", $meeting_sequence_key);
        $data = array(
                "meeting_size_used"    => $remaining_size + $sharedFileSize,
                "has_recorded_video"   => 0
                );
        $ret = $obj_MeetingSequence->update($data, $where);
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
            return false;
        }
        return true;
    }

    /**
     * meetingテーブルのビデオを削除(MeetingSequenceのビデオが全て削除されていたら)
     * @param $meeting_key
     * @return boolean
     */
    public function deleteMeetingTblVideo($meeting_key){

      $obj_Meeting           = new DBI_Meeting($this->dsn);
      $obj_MeetingSequence   = new DBI_MeetingSequence($this->dsn);
    $where =  sprintf( "meeting_key='%s' AND has_recorded_video=1", $meeting_key );
    $sequenceList = $obj_MeetingSequence -> getRowsAssoc( $where );

    if(count($sequenceList) == 0){
      // DB更新 meetingテーブル更新
      $meeting_data = array(
          "has_recorded_video" => 0,
      );
      $meeting_where = sprintf("meeting_key='%s'", $meeting_key);
      $meeting_ret = $obj_Meeting->update($meeting_data, $meeting_where);
      if (DB::isError($meeting_ret)) {
        $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$meeting_ret->getUserInfo());
        return false;
      }
    }
    return true;

    }

    /**
     * キャビネットファイルの削除
     */
    public function deleteCabinet($meeting_key, $cabinet_key = null) {
        $objFileUpload = new N2MY_FileUpload();
        $dir           = $objFileUpload->getDir( $meeting_key );
        $destFile      = $objFileUpload->getFullPath($dir);
        $where         = "meeting_key = '$meeting_key'" .
                         " AND status = 1";
        // ファイル単位で削除
        if ($cabinet_key) {
            $destFile = $destFile.'/'.$cabinet_key;
            $where   .= " AND tmp_name = '".addslashes($cabinet_key)."'";
        }
        if (file_exists($destFile)) {
            $objFileUpload->deleteDir( $destFile );
            require_once( "classes/dbi/file_cabinet.dbi.php" );
            $objFileCabinet = new FileCabinetTable($this->dsn);
            $data = array(
                "status"        => 0,
                "updatetime"    => date("Y-m-d H:i:s")
                );
            $objFileCabinet->update($data, $where);
        }
    }

    public function getFmsSize($meeting_key, $meeting_sequence_key = null) {
        // FMSサーバ取得
        $obj_Meeting  = new DBI_Meeting($this->dsn);
        $where        = "meeting_key = '".$meeting_key."'";
        $meeting_info = $obj_Meeting->getRow($where, "fms_path,server_key");
        if (!$meeting_sequence_key) {
            $fms_path   = $meeting_info["fms_path"];
            $server_key = $meeting_info["server_key"];
        } else {
            // FMS PATH取得
            $where    = "meeting_key = '".$meeting_key."'";
            $fms_path = $obj_Meeting->getOne($where, "fms_path");
            $where    = "meeting_key = '".$meeting_key."'" .
                        " AND meeting_sequence_key = '".$meeting_sequence_key."'";
            // FMS SERVER取得
            $obj_MeetingSequence = new DBI_MeetingSequence($this->dsn);
            $server_key          = $obj_MeetingSequence->getOne($where, "server_key");
        }
        if (!$meeting_info["intra_fms"]) {
            $obj_FmsService = new DBI_FmsServer(N2MY_MDB_DSN);
            $where          = "server_key = ".$server_key;
            $this->logger2->debug(array($meeting_key, $meeting_sequence_key, $server_key, $where));
            $fms_server_info = $obj_FmsService->getRow($where);
        } else {
            require_once("classes/dbi/user_fms_server.dbi.php");
            $objUserFmsServer = new UserFmsServerTable($this->dsn);
            $fms_server_info  = $objUserFmsServer->getRow(sprintf("fms_key='%s'", $server_key));
        }
        $fms_app_name = $this->config["CORE"]["app_name"];
        $fms_address  = $fms_server_info["local_address"] ? $fms_server_info["local_address"] : $fms_server_info["server_address"];
        $meeting_id   = $obj_Meeting->getMeetingID($meeting_key);
        $url          = "http://".$fms_address.":".$fms_server_info["server_port"]."/fms_size.php" .
                        "?app_name=" .urlencode($fms_app_name).
                        "&meeting_key=".$fms_path.$meeting_id;
        if ($meeting_sequence_key) {
            $url .= "&sequence_key=".$meeting_sequence_key;
        }
        $ch = curl_init();
        $option = array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT        => 10,
            );
        curl_setopt_array($ch, $option);
        $ret = curl_exec($ch);
        $this->logger2->info(array("url"  => $url,
                                   "size" => $ret));
        return $ret;
    }

    /**
     * 資料の容量を取得
     */
    public function getDocumentSize($meeting_key) {
        $obj_Document = new DBI_Document($this->dsn);
        $where        = "meeting_key = ".$meeting_key. " AND document_status = ".DOCUMENT_STATUS_SUCCESS;
        $documents    = $obj_Document->getRowsAssoc($where);
        $size         = 0;

        foreach ($documents as $document) {
            $size   += $document["document_size"];
            $files[] = array('file' => $document["document_path"].'/'.$document["document_id"],
                             'size' => $document["document_size"]);
        }
        $this->logger2->debug(array($files, $size));
        return $size;
    }

    public function getFileCabinetSize($meeting_key) {
        require_once( "classes/dbi/file_cabinet.dbi.php" );
        $objFileCabinet = new FileCabinetTable( $this->dsn );
        $where          = "meeting_key = ".$meeting_key.
                          " AND status = 1";
        $documents      = $objFileCabinet->getRowsAssoc($where);
        $size           = 0;
        foreach($documents as $document) {
            $size += $document["size"];
            $files[] = array('file' => $document["file_cabinet_path"],
                             'size' => $document["size"]
                );
        }
        $this->logger2->info(array($files, $size));
        return $size;
    }

    /**
     * DBに記録済みのFMSの容量を取得する。
     */
    public function getMeetingDataSize($meeting_key) {
        require_once( "classes/core/dbi/MeetingSequence.dbi.php" );
        $objMeetingSeqence    = new DBI_MeetingSequence( $this->dsn );
        $where                = "meeting_key = ".$meeting_key;
        $meeting_seqence_list = $objMeetingSeqence->getRowsAssoc($where, null, null, null,
                                                                 'meeting_key, meeting_sequence_key, server_key, meeting_size_used, status');
        $size = 0;
        foreach ($meeting_seqence_list as $meeting_seqence) {
            $size += $meeting_seqence["meeting_size_used"];
            $meeting_seqences[] = array(
                'meeting_sequence_key' => $meeting_seqence["meeting_sequence_key"],
                'size'                 => $meeting_seqence["meeting_size_used"]
                );
        }
        $this->logger2->info(array($meeting_seqences, $size));
        return $size;
    }

    public function getRecordGWSize($meeting_key) {
        require_once( "classes/core/dbi/MeetingSequence.dbi.php" );
        $objMeetingSeqence = new DBI_MeetingSequence( $this->dsn );
        $where = "meeting_key = ".$meeting_key.
                 " AND record_status = 'complete'";
        $meeting_seqence_list = $objMeetingSeqence->getRowsAssoc($where, null, null, null,
                                                                 'meeting_key, meeting_sequence_key, record_status, record_filepath');
        $size = 0;
        foreach ($meeting_seqence_list as $meeting_seqence) {
            $_size = filesize(N2MY_MOVIES_DIR.$meeting_seqence["record_filepath"]);
            $size += $_size;
            $files[] = array(
                'file' => N2MY_MOVIES_DIR.$meeting_seqence["record_filepath"],
                'size' => $_size
                );
        }
        $this->logger2->info(array($files, $size));
        return $size;
    }

    public function getMeetingSize($meeting_key) {
        // アップロード資料の容量
        $document_size     = $this->getDocumentSize($meeting_key);
        $file_cabinet_size = $this->getFileCabinetSize($meeting_key); // ファイルキャビネットの容量
        $meeting_data_size = $this->getMeetingDataSize($meeting_key);// FMSの容量
        $record_gw_size    = $this->getRecordGWSize($meeting_key);// 録画GWの容量取得

        $size              = $document_size + $file_cabinet_size + $meeting_data_size + $record_gw_size;
        $this->logger2->info(array(
            'detail' => array(
                'document_size'     => $document_size,
                'file_cabinet_size' => $file_cabinet_size,
                'meeting_data_size' => $meeting_data_size,
                'record_gw_size'    => $record_gw_size),
                'total'             => $size)
            );
        return $size;
    }

    public function getMeetingStatusInbound( $user_id, $inbound_id, $old_flg = null ) {
      $obj_Meeting = new DBI_Meeting( $this->dsn );
      $obj_MeetingUptime = new DBI_MeetingUptime( $this->dsn );
      $obj_Participant = new DBI_Participant( $this->dsn );
      //$columns = "meeting_key, room_key, meeting_ticket, meeting_type, is_active, is_reserved ,meeting_stop_datetime, reenter_flg";
      $obj_User           = new UserTable($this->dsn);
      $where = "user_id = '".addslashes($user_id)."'".
                   " AND use_sales = 1 AND user_delete_status = 0";
      $user_info = $obj_User->getRow($where);
      $columns = "meeting_key, room_key, meeting_session_id, meeting_ticket, is_active, is_reserved ,meeting_stop_datetime";
      $where = "user_key = ".$user_info["user_key"];
      if (!$old_flg) {
        $where .= " AND inbound_flg = 1".
            " AND inbound_id = '".$inbound_id."'";
      }
      $where .= " AND is_active = 1 AND meeting_use_minute = 0";
      $this->logger2->debug(array("where"=>$where,"column"=>$columns),"select");

      $meetings = $obj_Meeting->getRowsAssoc($where, array("meeting_start_datetime" => "asc"), null, null, $columns);
      $this->logger2->debug($meetings);
      if (!$meetings) {
          $meetings = $obj_Meeting->getRowsAssoc($where, array("meeting_start_datetime" => "asc"), null, null, $columns);
      }

      $this->logger->trace("meetings",__FILE__,__LINE__,$meetings);
      foreach( $meetings as $meeting_info ) {
        $status = $meeting_info["is_active"] ? 1 : 0;
        $pcount = 0;
        // データが存在する場合
        $participants = array();
        if ($meeting_info) {
          $meeting_key = $meeting_info["meeting_key"];
          $meeting_type = $meeting_info["meeting_type"];
          $pcount = $obj_Participant->getCount($meeting_key);
          //get online_participans
          $participants = $obj_Participant->getList($meeting_key, true);
        }
        if ($pcount > 0) {
          $_rooms[] = array(
              "room_key" => $meeting_info["room_key"],
              "meeting_key" => $meeting_key,
              "meeting_session_id" => $meeting_info["meeting_session_id"],
              "meeting_type" => $meeting_type,
              "reenter_flg" => $meeting_info["reenter_flg"],
              "meeting_ticket" => $meeting_info["meeting_ticket"],
              "is_reserved" => $meeting_info["is_reserved"],
              "status" => $status,
              "pcount" => $pcount,
              "participants" => $participants
          );
        }
      }
      return $_rooms;
    }

    private function error()
    {
        return false;
    }

    function getVersion() {
        static $version;
        if ($version) {
            return $version;
        }
        $version_file = N2MY_APP_DIR."version.dat";
        if ($fp = fopen($version_file, "r")) {
            $version = trim(fgets($fp));
        }
        return $version;
    }
    private function getExecutePGiAPiStatus($meeting_info, $meetingdata, $data){

        if (!$meeting_info["is_active"]) {
            return '';
        }

        $isCancelMeeting = !@$meetingdata["meeting_start_datetime"] || !@$meetingdata["meeting_stop_datetime"];
        if ($isCancelMeeting) {
            return '';
        }

        $isCreatePGi    = !$meeting_info["pgi_conference_id"] && !$meeting_info["pgi_api_status"];
        if ($isCreatePGi) {
            return 'create';
        }

        $isEditReserve  = $meeting_info["is_reserved"] && ($meetingdata["pgi_api_status"] != 'error');
        if (!$isEditReserve) {
            return '';
        }

        if (($meeting_info["meeting_start_datetime"] != $data["meeting_start_datetime"])
         || ($meeting_info["meeting_stop_datetime"]  != $data["meeting_stop_datetime"])) {
             return 'edit';
        }

        return '';
    }
    private function updateMeetingTeleconf($meeting_key, $room_key)
    {
        if (!$meeting_key || !$room_key ) {
            $this->logger2->error("meeting_key or room_key is empty meeting_key:$meeting_key /  room_key:$room_key");
            return;
        }

        $service_option = new OrderedServiceOptionTable($this->dsn);
        if (!$service_option->enableOnRoom(23, $room_key)) { // 23 = teleconference
            return;
        }
        $room_table = new RoomTable($this->dsn);
        $room = $room_table->findByKey($room_key);

        if ($room['use_teleconf']) {
            $data = array('use_pgi_dialout'     => $room['use_pgi_dialout'],
                          'use_pgi_dialin'      => $room['use_pgi_dialin'],
                          'use_pgi_dialin_free' => $room['use_pgi_dialin_free']);
        } else {
            $data = array('tc_type'             => 'voip',
                          'use_pgi_dialout'     => 0,
                          'use_pgi_dialin'      => 0,
                          'use_pgi_dialin_free' => 0);
        }
        $meeting = new DBI_Meeting($this->dsn);
        $meeting->update($data, "meeting_key='$meeting_key'");
    }
}
