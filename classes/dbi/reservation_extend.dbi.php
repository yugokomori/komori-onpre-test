<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/N2MY_DBI.class.php");
require_once("lib/EZLib/EZUtil/EZDate.class.php");

class ReservationExtendTable extends N2MY_DB {

    var $serverTimeZone = N2MY_SERVER_TIMEZONE;
    var $time_zone = N2MY_SERVER_TIMEZONE;
    protected $primary_key = "reservation_extend_key";

    function ReservationExtendTable($dsn) {
        $this->init($dsn, "reservation_extend");
    }

    function add($data) {
        $data["create_datetime"] = date("Y-m-d H:i:s");
        return parent::add($data);
    }
}


/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
