<?php
$start = time();
echo "start => ".time()."<br />";
require_once( "Statistics.class.php" );

$st = new Statistics( Statistics::MATRIX1 );

$search = null;

if($_SERVER["REQUEST_METHOD"] == "GET"){
  $search = $_GET;
  $query_string = urldecode($_SERVER["QUERY_STRING"]);

  //日付
}

$headersCnf = $st->getHeadersConfig();

$headers = $st->getHeaders();

if(count($search) != 0){
  $search = $st->checkData($search);
  $rows = $st->getDataArray($search);
}else{
  $rows = array();
}

$fmsServers = $st->getFmsInfo();
?>

<html>
  <head>
    <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
    <link rel="stylesheet" href="./css/api.css" type="text/css">
    <link rel="stylesheet" href="./css/calendar.css" type="text/css">
    <script type="text/javascript" src="./js/mtg.js"></script>
    <script type="text/javascript" src="./js/prototype/prototype.js" charset="UTF-8"></script>
    <script type="text/javascript" src="./js/protocalendar.js" charset="UTF-8"></script>
    <script type="text/javascript" src="./js/lang_ja.js" charset="UTF-8"></script>
    
  </head>
  <body>
  
  <form name="form1" id="form1" method="GET" action="./statistics.php" >
    <table class="data">
      <tr>
        <th>fms</th>
        <td>
          <?php
            foreach($fmsServers as $i => $fmsServer){
              $chk = "";
              if(is_array($search["fms"]) && in_array($fmsServer["server_key"], $search["fms"])){
                $chk = "checked = checked";
              }elseif(!is_array($search["fms"])){
                $chk = "checked = checked"; 
              }
              
              echo sprintf('<input type="checkbox" name="fms[]" value="%s" id="check%s" %s/><label for="check%s" >%s</label>', $fmsServer["server_key"], $i, $chk, $i, $fmsServer["server_address"]);
              if($i != 0 && ($i % 3) == 2){
                echo '<br />';
              }
            }
          ?>
          <br /><input type="button" value="All" onclick="allCheck('fms[]')" /><input type="button" value="Clear" onclick="allClear('fms[]')" />
        </td>
      </tr>
      <tr>
        <th>Time</th>
        <td>
            <input type="text" name="from" class="input-date calendar" value="<?php echo @$search['from'] ? @$search['from']: date('Y-m-d', strtotime('-30 day',time())); ?>">
            <select name="from_h">
              <?php
                for($i=0; $i<=23; $i++){
                  $s = "";
                  if($i == @$search["from_h"]){
                    $s = "selected='selected'";
                  }
                 echo sprintf("<option value='%s' %s >%s</option>",  $i, $s, $i);
                }
              ?>
            </select>：
            <select name="from_m">
              <?php
                $ms = array("00","05","10","15","20","25","30","35","40","45","50","55");
                foreach($ms as $m){
                  $s = "";
                  if($m == @$search["from_m"]){
                    $s = "selected='selected'";
                  }
                 echo sprintf("<option value='%s' %s >%s</option>",  $m, $s, $m);
                }
              ?>
            </select>
            &nbsp;～&nbsp;
            <input type="text" name="to" class="input-date calendar" value="<?php echo @$search['to'] ? @$search['to']: date('Y-m-d', time()); ?>" id="cal2">
            <select name="to_h">
              <?php
                for($i=0; $i<=23; $i++){
                  $s = "";
                  if($i == @$search["to_h"]){
                    $s = "selected='selected'";
                  }
                 echo sprintf("<option value='%s' %s >%s</option>",  $i, $s, $i);
                }
              ?>
            </select>：
            <select name="to_m">
              <?php
                $ms = array("00","05","10","15","20","25","30","35","40","45","50","55");
                foreach($ms as $m){
                  $s = "";
                  if($m == @$search["to_m"]){
                    $s = "selected='selected'";
                  }
                 echo sprintf("<option value='%s' %s >%s</option>",  $m, $s, $m);
                }
              ?>
            </select>
          </td>
      </tr>
      <tr>
        <th>User Account</th>
        <td>
          <input type="text" name="user" value="<?php echo @$search['user'] ?>">
        </td>
      </tr>
      <tr>
        <th>Meeting Restart</th>
        <td>
          <input type="radio" name="reStart" value="0" id="radio1" <?php echo (empty($search['reStart']) || @$search['reStart'] == "0" )? "checked=checked": ""; ?> /><label for="radio1" >すべて</label>
          <input type="radio" name="reStart" value="1" id="radio2" <?php echo (@$search['reStart'] == "1")? "checked=checked": ""; ?> /><label for="radio2" >再入室のみ</label>
        </td>
      </tr>
      <tr>
        <td colspan ="2" ><input type="submit" value="Search"></td>
      </tr>
      
    </table?>
    
  </form>
  
  <br /><br />
  
  <table class="data">

    <tr>
      <th>&emsp;</th>
    <?php
      foreach ( $headers as $header_id ) {
        printf( "<th>%s</th>", $headersCnf[$header_id]["view"] );
      }
    ?>
    </tr>

    <?php
      //foreach ( $headers as $header_id ) {
      //  printf( "<tr><td>%s</td>", $headersCnf[$header_id]["view"] );
      //  foreach($rows as $c => $row){
      //    if ( $c == $header_id )
      //      print "<td>&emsp;</td>";
      //    else
      //      //printf( "<td>%s</td>", $v->$($headersCnf[$k]["name"]) );
      //      var_dump($row->$headersCnf[0]["name"]);
      //      printf( "<td>%s</td>", $row->$headersCnf[$c]["name"] );
      //  }
      //  print "</tr>";
      //}
      $total  = array();
      foreach ( $rows as $r => $row ) {
        printf( "<tr><td>%s</td>", $headersCnf[$r]["view"] );
        foreach($headers as  $i  => $header_id){
            $tmpV = ($row->$headersCnf[$header_id]["name"])? $row->$headersCnf[$header_id]["name"] : 0 ;
            if($tmpV != 0){
              printf( "<td><a href='./detail.php?%s&header=%s&rHeader=%s' target='_blank'>%s</a>&nbsp;</td>", $query_string, $header_id, $r, $tmpV );
            }else{
              printf( "<td>%s&nbsp;</td>", $tmpV );
            }
            $total[$i] += $row->$headersCnf[$header_id]["name"];
        }
        print "</tr>";
      }
      
      //計
      printf( "<tr><td>%s</td>", "Total?" );
      foreach ( $total as $t => $value ) {
        printf( "<td>%s</td>", $value );
      }
      print "</tr>";
      
      
    ?>

  </table>
  <br /><br />
<?php

$st2 = new Statistics( Statistics::MATRIX2 );


$search2 = null;

if($_SERVER["REQUEST_METHOD"] == "POST"){
  $search2 = $_POST;
}

$headers2 = $st2->getHeaders();

if(count($search) != 0){
  $rows2 = $st2->getDataArray($search);
}else{
  $rows2 = array();
}
?>
  
  <table class="data">

    <tr>
      <th>&emsp;</th>
    <?php
      foreach ( $headers2 as $header_id ) {
        printf( "<th>%s</th>", $headersCnf[$header_id]["view"] );
      }
    ?>
    </tr>

    <?php
      $total  = array();
      foreach ( $rows2 as $r => $row ) {
        printf( "<tr><td>%s</td>", $headersCnf[$r]["view"] );
        foreach($headers2 as  $i  => $header_id){
            //printf( "<td>%s&nbsp;</td>", ($row->$headersCnf[$header_id]["name"])? $row->$headersCnf[$header_id]["name"] : 0 );
            
            $tmpV = ($row->$headersCnf[$header_id]["name"])? $row->$headersCnf[$header_id]["name"] : 0 ;
            if($tmpV != 0){
              printf( "<td><a href='./detail.php?%s&header=%s&rHeader=%s' target='_blank'>%s</a>&nbsp;</td>", $query_string, $header_id, $r, $tmpV );
            }else{
              printf( "<td>%s&nbsp;</td>", $tmpV );
            }
            
            $total[$i] += $row->$headersCnf[$header_id]["name"];
        }
        print "</tr>";
      }
      
      //計
      printf( "<tr><td>%s</td>", "Total?" );
      foreach ( $total as $t => $value ) {
        printf( "<td>%s</td>", $value );
      }
      print "</tr>";
      
      
    ?>

  </table>
  
  </body>
</html>
<?php
  $end = time();
  echo "end => ", $end."<br />";
  
  $aaa  = $end - $start;
  
  echo "time => ".$aaa."<br />";
  
?>


