<?php

require_once("classes/N2MY_DBI.class.php");

class QuickVoteLogTable extends N2MY_DB {

    var $table = "quick_vote_log";
    var $logger = null;
    protected $primary_key = "quick_vote_log_key";

    public function __construct( $dsn )
    {
        $this->init($dsn, $this->table);
    }
    
    public function countVoteNumber($meeting_key) {
    	$where = "meeting_key =".$meeting_key;
     	$db_res = $this->getOne($where, "number",array("number" => "desc"));
    	$this->logger->info("meeting key",__FUNCTION__,__LINE__,$db_res);
        if (empty($db_res)) {
        	$this->logger->error(__FUNCTION__,__FILE__,__LINE__,"no data");
            return 1;
        } else {
        	return $db_res + 1;
        }
    }
    
    public function countResult($meeting_key,$number,$item) {
    	$where = "meeting_key =".$meeting_key;
    	$where .= " and number =".$number;
    	$where .= " and result ='".$item."'";
    	$db_res = $this->numRows($where);
    	if (empty($db_res)) {
    		$this->logger->error(__FUNCTION__,__FILE__,__LINE__,"no data");
    		return 0;
    	} else {
    		return $db_res;
    	}
    }
    
    public function check_meeting_vote_flag() {
    	
    }
}