<?php
require_once(dirname(__FILE__)."/../params/WSDLParams.php"); 

class CreateConferenceExtParams extends WSDLParams {
	protected $name;
	protected $did;
	protected $mixerId;
	protected $size;
	protected $compType;
	protected $vad;
	protected $profileId;
	protected $audioCodecs;
	protected $videoCodecs;
	protected $textCodecs;
	protected $autoAccept;
	protected $callbackURL;
	public function __construct($obj) {
		parent::__construct($this, $obj);
	}
	
	public function getParams() {
		return parent::getParams($this);
	}
}