<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/N2MY_DBI.class.php");

class DBI_MeetingUptime extends N2MY_DB
{
    public $table = "meeting_uptime";
    public $logger = null;
    public $rules = array();
    protected $primary_key = "meeting_uptime_key";

    public function __construct( $dsn ) {
        $this->init( $dsn, $this->table );
    }

    /**
     * 会議中のデータ取得
     * @access protected
     */
    function getActiveInfo($meeting_key) {
        $where = "meeting_uptime_stop IS NULL" .
                " AND meeting_key = ".$meeting_key;
        return $this->getRow($where);
    }

    /**
     * 会議利用開始
     */
    function start($meeting_key) {
        $meeting_uptime_key = $this->getActiveInfo($meeting_key);
        // 利用中
        if (!$meeting_uptime_key) {
            $datetime = date("Y-m-d H:i:s");
            $data = array(
                "meeting_key" => $meeting_key,
                "meeting_uptime_start" => date("Y-m-d H:i:s"),
                "create_datetime" => $datetime,
            );
            $this->logger2->debug($data);
            $this->add($data);
        }
    }

    /**
     * 会議利用中断
     */
    function stop($meeting_key) {
        $meeting_uptime_info = $this->getActiveInfo($meeting_key);
        // 存在する
        if ($meeting_uptime_info) {
            $datetime = date("Y-m-d H:i:s");
            $where = "meeting_uptime_key = " . $meeting_uptime_info["meeting_uptime_key"];
            // 終了時間を記録
            $data = array (
                "meeting_uptime_stop" => date("Y-m-d H:i:s"),
                "update_datetime" => $datetime
            );
            $this->logger2->info($data);
            $result = $this->update($data, $where);
        }
    }
}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */