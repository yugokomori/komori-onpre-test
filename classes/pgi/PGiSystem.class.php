<?php
class PGiSystem
{
    static function findConfigAll()
    {
        $rate_config = simplexml_load_file(N2MY_APP_DIR.'/config/pgi/systems.xml');
        if (!$rate_config) {
            throw('systems.xml file not found');
        }
        return $rate_config;
    }
    static function findDefault()
    {
        $config  = EZConfig::getInstance(N2MY_CONFIG_FILE)->getAll("PGI");
        $system_key = $config['system_key'];
        if (!$system_key) {
            throw new Exception("pgi system_key not defined ");
        }
        return self::findBySystemKey($system_key);
    }
    static function findBySystemKey($system_key)
    {
        $file = N2MY_APP_DIR.'config/pgi/systems.xml';
        $xml  = simplexml_load_file($file);
        if (!$xml) {
            throw new Exception("$file  not exist or parse failure");
        }

        if (count($xml) == 0) {
            throw new Exception("pgi systems not defined (length : 0)");
        }

        $pgi_system = null;
        foreach ($xml as $v) {
            if ((string)$v->key == $system_key) {
                $pgi_system = $v;
            }
        }
        return $pgi_system;
    }
}
