<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
set_time_limit(0);
ini_set("memory_limit","2048M");

require_once ('classes/AppFrame.class.php');
require_once ('classes/mgm/dbi/user.dbi.php');
require_once("classes/N2MY_Account.class.php");
require_once ('classes/dbi/member.dbi.php');
require_once ('classes/dbi/member_group.dbi.php');
require_once ('classes/dbi/member_status.dbi.php');
require_once ('classes/dbi/user.dbi.php');
require_once ('config/config.inc.php');
require_once ('lib/EZLib/EZUtil/EZEncrypt.class.php');
require_once( "lib/EZLib/EZCore/EZLogger2.class.php" );
require_once ('lib/EZLib/EZUtil/EZCsv.class.php');
require_once ('classes/dbi/whitelist.dbi.php');

class AppMember extends AppFrame
{
    var $logger = null;
    var $_name_space = null;
    var $memberTable = null;
    var $adminTable  = null;
    var $groupTable = null;
    var $statusTable = null;
    var $account_dsn = null;
    var $max_address_cnt = null;

    function init() {
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->memberTable = new WhitelistTable($this->get_dsn());
        $this->adminTable = new MemberTable($this->get_dsn());
        $this->groupTable = new MemberGroupTable($this->get_dsn());
        $this->_name_space = md5(__FILE__);
        $this->max_address_cnt  = 50000;

        //個別編集、削除用
        $this->whitelistTable = new WhitelistTable($this->get_dsn());

    }

    /**
     * ログイン認証
     */
    function auth() {
        $this->checkAuth();
        $this->checkAdminAuth('admin/session_error.t.html');
    }
    /**
     * デフォルト処理
     */
    function default_view() {
        $this->logger->trace("test", __FILE__, __LINE__, __FUNCTION__);
        $this->action_show_top();
    }

    function action_show_top()
    {
        $this->display('admin/member/index.t.html');
    }

    function action_admin_member($message = ""){
        $this->session->remove("conditions");
        $this->render_admin_member();
    }

    function action_search_member() {
        $this->setCond();
        $this->render_admin_member();
    }

    function action_pager() {
        $this->render_admin_member();
    }

    function render_admin_member($message = ""){

        if($message){
            $this->template->assign('message', $message);
        }
        //user_key取得
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];

        //user_keyからメンバーを取得
        $conditions = $this->getCond();
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$conditions);
        $sort_key  = ($conditions["sort_key"]) ? $conditions["sort_key"] : "member_id";
        $sort_type = ($conditions["sort_type"]) ? $conditions["sort_type"] : "asc";
        $sort_key_rules = array("member_name","member_id","accounting_unit_code","expense_department_code");
        if (!EZValidator::valid_allow($sort_key, $sort_key_rules)) {
            $sort_key = "member_name";
        }
        $sort_type_rules = array("asc","desc");
        if (!EZValidator::valid_allow($sort_type, $sort_type_rules)) {
            $sort_type = "desc";
        }
        $limit = $this->request->get("page_cnt");
        if (!$limit) {
            $limit = 10;
        }
        $page      = $this->request->get("page", 1);
        $offset    = ($page - 1) * $limit;
        // 条件
        $where = "user_key = ".addslashes($user_key).
            " AND whitelist_status > -1";
        if ($conditions["member_name"]) {
            $where .= " AND (member_name like '%".addslashes($conditions["member_name"])."%'";
            $where .= " OR member_id like '%".addslashes($conditions["member_name"])."%'";
            $where .= " OR expense_department_code like '%".addslashes($conditions["member_name"])."%'";
            $where .= " OR accounting_unit_code like '%".addslashes($conditions["member_name"])."%')";
        }
        $member_list = $this->memberTable->getRowsAssoc($where, array($sort_key => $sort_type), $limit, $offset);
        $count = $this->memberTable->numRows($where);
        $obj_MgmUserTable = new MgmUserTable( $this->get_auth_dsn() );
        $member_info = array();

        //個別編集、削除用
        $whitelist_info = array();
        foreach( $whitelist_info as $whitelist ){
            $whitelist_info[] = array(
                    "auth_user_key" => $obj_MgmUserTable->getOne( sprintf( "user_id='%s'", $whitelist["member_id"] ), "user_key" ),
                    "status"        => $this->_getMemberStatusName( $whitelist['whitelist_status'] ),
                    "key"           => $whitelist['whitelist_key'],
                    "id"            => $whitelist['member_id'],
                    "name"          => $whitelist['member_name'],
                    "accounting_unit_code" => $whitelist['accounting_unit_code'],
                    "expense_department_code" => $whitelist['expense_department_code'],
            );
        }

        foreach( $member_list as $member ){
            $member_info[] = array(
                            "auth_user_key" => $obj_MgmUserTable->getOne( sprintf( "user_id='%s'", $member["member_id"] ), "user_key" ),
                            "status"        => $this->_getMemberStatusName( $member['whitelist_status'] ),
                            "key"           => $member['whitelist_key'],
                            "id"            => $member['member_id'],
                            "accounting_unit_code" => $member['accounting_unit_code'],
                            "expense_department_code" => $member['expense_department_code'],
                            "name"          => $member['member_name'],
                            );
        }
        $add_flg = ( ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") &&
            $user_info["max_member_count"] <= count( $member_info ) ) ? false : true ;
        $this->template->assign("add_flg", $add_flg );
        $this->template->assign('info', $member_info);
        $this->template->assign('vcubeid_add_use_flg', $user_info["vcubeid_add_use_flg"]);
        // ページング
        $this->template->assign('conditions', $conditions);
        $pager = $this->setPager($limit, $page, $count);
        $this->template->assign('pager', $pager);
        $this->display('admin/member/whitelist/index.t.html');
    }

    function setCond() {
        $conditions  = $this->request->get("conditions");
        if ($conditions) {
            $this->session->set("conditions", $conditions);
        }
    }

    function getCond() {
        $condition = $this->session->get("conditions");
        return $condition;
    }

    function action_change_vcubeid_flg() {
        $user_info = $this->session->get("user_info");
        $vcubeid_use_flg = $this->request->get("vcubeid_add_use_flg");
        $user_info["vcubeid_add_use_flg"] = $vcubeid_use_flg;
        $objUser = new UserTable($this->get_dsn());
        $where = "user_key = ". $user_info["user_key"];
        $data = array("vcubeid_add_use_flg" => $vcubeid_use_flg );
        $objUser->update($data, $where);
        $this->session->set("user_info", $user_info);
        $this->action_admin_member();
    }

    function action_add_member($message = "")
    {

        $member_info = ( 1 == $this->request->get( "action_add_member") ) ? "" : $this->session->get('new_member_info');
        //user_key取得
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];

        //エラー時にユーザインプットの再表示
        if( $member_info ){
            foreach($member_info as $key => $value){
                $this->template->assign($key, $value);
            }
        }
        if($message){
            $this->template->assign('message', $message);
        }

        $this->template->assign('user_info', $user_info);
        $this->display('admin/member/whitelist/add/index.t.html');

    }

    function action_add_member_confirm()
    {

        $this->set_submit_key($this->_name_space);
        // 入力値を保持
        $request = $this->request->getAll();
        $this->session->set('new_member_info', $request);
        $user_info = $this->session->get( "user_info");
        $message = null;
        $message = $this->check_member_info($request, $user_info);
        if( $message ){
            $this->action_add_member($message);
        } else {
            foreach($request as $key => $value){
                $this->template->assign ( $key, $value );
            }
            $this->display ( 'admin/member/whitelist/add/confirm.t.html' );
        }

    }
    public function action_add_member_csv() {
        $this->template->assign('max_address_cnt',  $this->max_address_cnt);
        $this->display('admin/member/whitelist/csv_index.t.html');
    }

    function action_format_download() {
        $output_encoding = $this->get_output_encoding();
        $csv = new EZCsv(true,$output_encoding,"UTF-8");
        $dir = $this->get_work_dir();
        $tmpfile = tempnam($dir, "csv_");
        $csv->open($tmpfile, "w");

        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="sample_whiteid.csv"');

        // ヘッダ
        $header = array("member_id","name","accounting_unit_code","expense_department_code","flag","error");
        $sample = array("21921","太郎","1qa","2ws","A","This is a sample data. Please delete.");
        $this->logger2->debug(__FUNCTION__, __FILE__, __LINE__, $header);
        $csv->setHeader($header);
        $csv->write($header);
        $csv->write($sample);
        $csv->close();
        $fp = fopen($tmpfile, "r");
        $contents = fread($fp, filesize($tmpfile));
        fclose($fp);
        print $contents;
        unlink($tmpfile);
    }

    public function action_import_member_csv() {
        $request["upload_file"] = $_FILES["import_file"];

        $data = pathinfo($request["upload_file"]["name"]);
        //CSVファイルチェック
        if (empty($data['filename'])) {
            $message .= CSV_NO_FILE_ERROR;
            return $this->action_import_complete($message);
        } elseif($data['extension'] != "csv") {
            $message .= CSV_FILE_FORMAT_ERROR ;
            return $this->action_import_complete($message);
        }
        $filename = $this->get_work_dir()."/".tmpfile();
        move_uploaded_file($request["upload_file"]["tmp_name"], $filename);
        $request["upload_file"]["tmp_name"] = $filename;
        $output_encoding = $this->get_output_encoding();
        $csv = new EZCsv(true,$output_encoding,"UTF-8");
        $csv->open($filename, "r");
        //取り込んだファイルのヘッダチェック
        $head_ex = "member_id,name,accounting_unit_code,expense_department_code,flag,error";
        $h_ar = implode(",", $csv->getHeader());
        if($head_ex !== $h_ar) {
            $message .= CSV_FILE_FORMAT_ERROR;
            return $this->action_import_complete($message);
        }
        $rows = array();
        $count = 0;
        while($row = $csv->getNext(true)) {
            $rows[] = $row;
            $count++;
        }
        $csv->close();
        unlink($filename);
        //記録メンバー数チェック
        if(empty($rows)) {
            $message .= CSV_NO_ITEM_ERROR;
            return $this->action_import_complete($message);
        } elseif($count > $this->max_address_cnt) {
            $message .= CSV_LIMIT_ERROR;
            return $this->action_import_complete($message);
        }

        //追加・編集・削除処理
        $user_info = $this->session->get( "user_info" );
        $result_array = array();
        $error_flg = 0;
        foreach($rows as $member_row) {
            //$member_row["storage_flg"] = "Y";
            switch($member_row["flag"]) {
                case "X":
                    break;
                case "A":
                    if(!$this->_isExist($member_row["member_id"], $user_info['user_key'])){
                        //追加こちから
                        if ($error_message = $this->_isMemberInfo($member_row, $user_info)){
                            //入力情報をバリデーションエラー
                            $error_msg = "";
                            foreach($error_message as $msg) {
                                $error_msg .= $msg." ";
                            }
                            $member_row[error] = $error_msg;
                            $error_flg = 1;
                        } elseif ($user_info["max_member_count"] != 0 && $this->_getMemberCount($user_info["user_key"]) >= $user_info["max_member_count"]) {
                            //契約上限値越えた
                            $member_row[error] = MEMBER_ERROR_OVER_COUNT;
                            $error_flg = 1;
                        } else {
                            //追加処理
                            if($member_row["group_name"]) {
                                $group_key = $this->_isGroupExist($member_row["group_name"],$user_info["user_key"],true);
                                $member_row["member_group"] = $group_key;
                            }
                            $add_data = array(
                                "user_key"           => $user_info["user_key"],
                                "member_id"          => $member_row['member_id'],
                                "accounting_unit_code"    => $member_row['accounting_unit_code'],
                                "expense_department_code" => $member_row['expense_department_code'],
                                "member_name"        => $member_row['name'],
                                "whitelist_status"      => "0",
                            );
                            $this->logger2->info($add_data);
                            $ret = $this->memberTable->add( $add_data );
                            if (DB::isError($ret)) {
                                $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                            } else {
                                $this->logger->info(__FUNCTION__."#add whitelist successful!",__FILE__,__LINE__,$temp);
                            }
                        }
                    } else {
                        //編集こちから
                        $whitelist_key = $this->_isMemberExist($member_row["member_id"],$user_info["user_key"]);
                        if(!$whitelist_key) {
                            //他ユーザーのメンバーがこのメンバーIDを使っています
                            $member_row[error] = MEMBER_ERROR_ID_EXIST;
                            $error_flg = 1;
                        } elseif ($error_message = $this->_isMemberInfo($member_row, $user_info, $whitelist_key)) {
                            //入力情報をバリデーションエラー
                            $error_msg = "";
                            foreach($error_message as $msg) {
                                $error_msg .= $msg." ";
                            }
                            $member_row[error] = $error_msg;
                            $error_flg = 1;
                        } else {
                            //更新処理
                            if($member_row["group_name"]) {
                                $group_key = $this->_isGroupExist($member_row["group_name"],$user_info["user_key"],true);
                                $member_row["member_group"] = $group_key;
                            }
                            $edit_data = array(
                                "accounting_unit_code"    => $member_row['accounting_unit_code'],
                                "expense_department_code" => $member_row['expense_department_code'],
                                "member_name"        => $member_row['name'],
                                "whitelist_status"      => "0",
                            );
                            if ($member_row['member_pass']) {
                                $edit_data['member_pass'] = EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $member_row['member_pass']);
                            }
                            $where =  sprintf( "whitelist_key='%s'", $whitelist_key );
                            $this->logger2->info($edit_data);
                            $result = $this->memberTable->update($edit_data, $where);
                            if (DB::isError($result)) {
                                $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$result->getUserInfo());
                            } else {
                                $this->logger->info(__FUNCTION__."#edit whitelist successful!",__FILE__,__LINE__,$auth_data);
                            }
                        }
                    }
                    break;
                case "D":
                    //削除こちから
                    $whitelist_key = $this->_isMemberExist($member_row["member_id"],$user_info["user_key"]);
                    if(!$whitelist_key) {
                        //メンバーIDが使っていません
                        $member_row[error] = MEMBER_ERROR_DELETE;
                        $error_flg = 1;
                    } else {
                        //削除処理
                        $delete_data['whitelist_status']=-1;
                        $ret = $this->memberTable->update($delete_data, sprintf( "whitelist_key = '%s'", $whitelist_key ) );
                        if (DB::isError($ret)) {
                            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
                        } else {
                            $this->logger->info(__FUNCTION__."#delete whitelist successful!",__FILE__,__LINE__,array(
                                "whitelist_key" => $whitelist_key)
                            );
                        }
                    }
                    break;
                default:
                    $member_row[error] = MEMBER_ERROR_CSV_FLAG;
                    $error_row[] = $member_row;
                    $error_flg = 1;
                    break;
            }
            $result_array[] = $member_row;
        }
        $this->logger2->info($result_array);
        if($error_flg) {
            $message = "一部のデータの登録ができませんでした。";
        }
        return $this->action_import_complete($message, $result_array,$error_flg);
    }

    public function action_import_complete($message="",$result_array = "",$error_flg = 0) {
        if ($message) {
            $this->template->assign('message', $message);
        }
        if ($error_flg) {
            $this->session->set("result_array",$result_array);
            $this->template->assign('is_error', true);
        }
        $this->display('admin/member/whitelist/csv_done.t.html');
    }

    public function action_export_member_csv() {
        $user_info = $this->session->get("user_info");
        $user_key = $user_info["user_key"];

        // CSV出力
        $output_encoding = $this->get_output_encoding();
        $csv = new EZCsv(true,$output_encoding,"UTF-8");
        $dir = $this->get_work_dir();
        $tmpfile = tempnam($dir, "csv_");
        $csv->open($tmpfile, "w");
        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="Whiteid_'.date("Ymd").'.csv"');
        $header = array(
            "member_id"   => "member_id",
            "name"        => "name",
            "accounting_unit_code"   => "accounting_unit_code",
            "expense_department_code"   => "expense_department_code",
            "flag"        => "flag",
            "error"       => "error",
        );
        $csv->setHeader($header);
        $csv->write($header);
        $where =  sprintf( "user_key='%s' and whitelist_status ='0'", $user_key );
        $_list = $this->memberTable->getRowsAssoc($where);
        foreach($_list as $_key => $row) {
            $line = array();
            $line["member_id"]      = $row["member_id"];
            $line["accounting_unit_code"]      = $row["accounting_unit_code"];
            $line["expense_department_code"]   = $row["expense_department_code"];
            $line["name"]           = $row["member_name"];
            $line["flag"]           = "X";
            $line["error"]           = "";

            $this->logger2->info($line);
            $csv->write($line);
        }

        // CSV出力
        $csv->close();
        $fp = fopen($tmpfile, "r");
        $contents = fread($fp, filesize($tmpfile));
        fclose($fp);

        print $contents;
        unlink($tmpfile);
    }

    public function action_err_csv_download() {
        $rows = $this->session->get("result_array");
        if(!empty($rows)) {
            // エラーの出力
            $output_encoding = $this->get_output_encoding();
            $csv = new EZCsv(true,$output_encoding,"UTF-8");
            $dirName = "/member";
            $dir = $this->get_work_dir().$dirName;
            if( !is_dir( $dir ) ){
                mkdir( $dir );
                chmod( $dir ,0777 );
            }
            $tmpfile = tempnam($dir, "csv_");
            $csv->open($tmpfile, "w");

            header('Content-Type: application/octet-stream;');
            header('Content-Disposition: attachment; filename="Whiteid_err.csv"');

            $header = array(
                "member_id"   => "member_id",
                "name"        => "name",
                "accounting_unit_code"   => "accounting_unit_code",
                "expense_department_code"   => "expense_department_code",
                "flag"        => "flag",
                "error"       => "error"
            );
            $csv->setHeader($header);
            $csv->write($header);

            foreach($rows as $a_key => $row) {
                $err_list["member_id"]      = $row["member_id"];
                $err_list["accounting_unit_code"]      = $row["accounting_unit_code"];
                $err_list["expense_department_code"]      = $row["expense_department_code"];
                $err_list["name"]           = $row["name"];
                $err_list["flag"]           = $row["flag"];
                $err_list["error"]          = $row["error"];

                $this->logger2->info($err_list);
                $csv->write($err_list);
            }
            $csv->close();
            $fp = fopen($tmpfile, "r");
            $contents = fread($fp, filesize($tmpfile));
            fclose($fp);
            chmod($tmpfile, 0777);
            print $contents;

            unlink($tmpfile);
        }
        unset($_SESSION['tmp_file']);
    }

    private function _isMemberExist($member_id,$user_key) {
        $where = sprintf( "user_key = '%s' AND member_id='%s' AND whitelist_status = 0", $user_key, $member_id );
        $whitelist_key = $this->memberTable->getOne($where, "whitelist_key");
        return $whitelist_key;
    }

    private function _getMemberCount($user_key) {
        $where = sprintf( "user_key = '%s' AND whitelist_status >='0'", $user_key );
        $member_count = $this->memberTable->numRows($where);
        return $member_count;
    }

    private function _isMemberInfo($request, $user_info, $whitelist_key = null) {
        $message = array();

        /*
         * CSV バリデーションチェック
         */
        if ($whitelist_key) {
            // whitelist_keyが存在する場合はCSV 編集の処理
            $idVali = $this->idValidationCheck ( $request ['member_id'], $whitelist_key, $user_key );
            if ($idVali) {
                $message [] = $idVali;
            }
        } else {
            // whitelist_keyが存在しない場合はCSV 新規の処理
            $idCsvVali = $this->idCsvValidationCheck ( $request ['member_id'], $user_key );
            if ($idCsvVali) {
                $message [] = $idCsvVali;
            }
        }

        // 名前
        $nameVali = $this->nameValidationCheck($request['name']);
        if ($nameVali) {
            $message [] = $nameVali ;
        }

        // 経理単位コード
        $accVali = $this->accValidationCheck ( $request ['accounting_unit_code'] );
        if ($accVali) {
            $message [] = $accVali ;
        }

        // 費用部課コード
        $expVali = $this->expValidationCheck ( $request ['expense_department_code'] );
        if ($expVali) {
            $message [] = $expVali;
        }

        return $message;
    }
    private function _isGroupExist($group_name,$user_key, $isCreate = false) {
        $group_key = $this->groupTable->getOne( sprintf( "member_group_name='%s' AND user_key = '%s'", $group_name, $user_key),"member_group_key");
        if($group_key){
            return $group_key;
        } elseif ($isCreate) {
            //add group
            $data = array();
            $data["member_group_name"] = trim($group_name);
            $data["user_key"] = $user_key;
            $key = $this->groupTable->add($data);
            return $key;
        } else {
            return 0;
        }
    }
    //member_group_key からそのmember_group_nameの取得
    private function _getMemberGroupName($key)
    {
        if ($key == 0) {
            return false;
        } else {
            $group = $this->groupTable->getRow( sprintf( "member_group_key='%s' AND user_key != 0", $key ) );
            return $group['member_group_name'];
        }
    }

    //member_status_key からそのmember_status_nameの取得
    private function _getMemberStatusName($key){
        if ($key == 0) {
            return false;
        } else {
            $obj_MemberStatus = new MemberStatusTable( $this->get_auth_dsn() );
            $ret = $obj_MemberStatus->select('member_status_key = '.$key);
            $status = $ret->fetchRow(DB_FETCHMODE_ASSOC);
            return $status['member_status_name'];
        }
    }

    /**
     * id からそのユーザーが存在するかチェック
     */
    private function _isExist($id, $user_key){
        //member, user, 認証DBのuser のテーブルをチェック
        $obj_UserTable = new UserTable( $this->get_dsn() );
        $obj_MgmUserTable = new MgmUserTable( $this->get_auth_dsn() );

        if ( $this->memberTable->numRows( sprintf("user_key = '%s' AND member_id='%s' AND whitelist_status = 0" ,$user_key,$id ) )  > 0 ){
            return true;
        } else {
            return false;
        }
    }

    private function check_member_info( $request, $user_info ){
        $message = null;

        /*
         * 新規追加 バリデーション
         */

        //ホワイト会社ID
        $idVali = $this->idValidationCheck(
                $request['member_id'],
                $request['whitelist_key'],
                $user_info['user_key']
        );
            if ($idVali) {
            $message .= '<li>'. $idVali . '</li>';
        }

        // 名前
        $nameVali = $this->nameValidationCheck($request['member_name']);
        if ($nameVali) {
            $message .= '<li>'. $nameVali . '</li>';
        }

        // 経理単位コード
        $accVali = $this->accValidationCheck ( $request ['accounting_unit_code'] );
        if ($accVali) {
            $message  .= '<li>' . $accVali . '</li>';
        }

        // 費用部課コード
        $expVali = $this->expValidationCheck ( $request ['expense_department_code'] );
        if ($expVali) {
            $message  .= '<li>' . $expVali . '</li>';
        }

        return $message;
    }

    /**
     * ＤＢにメンバー情報の登録
     */
    function action_add_member_complete() {
        if (! $this->check_submit_key ( $this->_name_space )) {
            $this->logger->warn ( __FUNCTION__ . "#duplicate", __FILE__, __LINE__ );
            header ( "Location: ?action_add_member=1" );
            exit ();
        }
        // 部屋追加
        $user_info = $this->session->get ( "user_info" );
        $request = $this->session->get ( 'new_member_info' );

        $obj_Room = new RoomTable ( $this->get_dsn () );
        require_once ("classes/dbi/ordered_service_option.dbi.php");
        $ordered_service_option = new OrderedServiceOptionTable ( $this->get_dsn () );

        // member追加
        $obj_UserTable = new UserTable ( $this->get_dsn () );

        $user_key = $user_info ['user_key'];
        $temp = array (
                "user_key" => $user_key,
                "member_id" => $request ['member_id'],
                "accounting_unit_code" => $request ['accounting_unit_code'],
                "expense_department_code" => $request ['expense_department_code'],
                "member_name" => $request ['member_name'],
                "whitelist_status"      => (($request['member_status_key']) ? $request['member_status_key'] : 0)

        );
        $ret = $this->memberTable->add ( $temp );
        if (DB::isError ( $ret )) {
            $this->logger->error ( __FUNCTION__ . "#DB ERROR!", __FILE__, __LINE__, $ret->getUserInfo () );
        } else {
            $this->logger->info ( __FUNCTION__ . "#add member successful!", __FILE__, __LINE__, $temp );
        }

        // 操作ログ
        $this->add_operation_log ( 'whitelist_add', array (
                'member_id' => $request ['member_id']
        ) );
        $this->display ( 'admin/member/whitelist/add/done.t.html' );
    }

    function action_edit_whitelist($message = ""){
        // 個別編集
        if($message){
            $this->template->assign('message', $message);
        }
        //user_key取得
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];

        if($this->request->get('mode') == "edit_whitelist"){
            //member_keyからメンバーを取得
            $key = $this->request->get('key');
            $info = $this->whitelistTable->getRow( sprintf( "user_key='%s' AND whitelist_key='%s'", $user_key , $key ) );
            // 不正アクセス対策

            if(!$info){
                $this->logger2->warn(__FUNCTION__, __FILE__, __LINE__,$user_info["user_key"]);
                return $this->action_admin_whitelist();
            }

            $whitelist_info['whitelist_key'] = $key;
            $whitelist_info['member_id'] = $info['member_id'];
            $whitelist_info['member_name'] = $info['member_name'];
            $whitelist_info['accounting_unit_code'] = $info['accounting_unit_code'];
            $whitelist_info['expense_department_code'] = $info['expense_department_code'];

        } else {
            $whitelist_info = $this->session->get('m_whitelist_edit_info');
        }

        foreach($whitelist_info as $key => $value){
            $this->template->assign($key, $value);
        }
        $this->display('admin/member/whitelist/edit/index.t.html');
    }

    function action_edit_whitelist_confirm(){

        // 入力値を保持
        $request = $this->request->getAll();
        $this->session->set('m_whitelist_edit_info', $request);
        $user_info = $this->session->get("user_info");

        $message = null;

        /*
         * 個別編集 バリデーション
         */

        //ID
        $idVali = $this->idValidationCheck(
                $request['member_id'],
                $request['whitelist_key'],
                $user_info['user_key']
        );
        if ($idVali) {
            $message .= '<li>'. $idVali . '</li>';
        }

        // 名前
        $nameVali = $this->nameValidationCheck($request['member_name']);
        if ($nameVali) {
            $message .= '<li>'. $nameVali . '</li>';
        }

        // 経理単位コード
        $accVali = $this->accValidationCheck ( $request ['accounting_unit_code'] );
        if ($accVali) {
            $message .= '<li>' . $accVali . '</li>';
        }

        // 費用部課コード
        $expVali = $this->expValidationCheck ( $request ['expense_department_code'] );
        if ($expVali) {
            $message .= '<li>' . $expVali . '</li>';
        }

        if ($message) {
            $this->action_edit_whitelist($message);
        } else {
            foreach($request as $key => $value){
                $this->template->assign($key, $value);
            }
            $this->display('admin/member/whitelist/edit/confirm.t.html');
        }
    }

    function action_edit_whitelist_complete()
    {

        $request = $this->session->get('m_whitelist_edit_info');

        $temp = array(
            "whitelist_key"          => $request['whitelist_key'],
            "member_id"          => $request['member_id'],
            "member_name"        => $request['member_name'],
            "accounting_unit_code"        => $request['accounting_unit_code'],
            "expense_department_code"        => $request['expense_department_code'],
            "whitelist_status"      => (($request['whitelist_status']) ? $request['whitelist_status'] : 0)
        );

        $user_info = $this->session->get('user_info');

        // 更新処理
        $ret = $this->whitelistTable->update($temp, sprintf( "whitelist_key='%s'", $request['whitelist_key'] ) );

        // エラー処理
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
        } else {
            $this->logger->info(__FUNCTION__."#edit whitelist successful!",__FILE__,__LINE__,$temp);
        }

        // 操作ログ
        $this->add_operation_log('whitelist_list_update', array('whitelist_key' => $request['whitelist_key']));
        $this->display('admin/member/whitelist/edit/done.t.html');
    }

    function action_delete_whitelist(){

        $user_info = $this->session->get( "user_info");
        $whitelist_key = $this->request->get('whitelist_key');
        $whitelistInfo = $this->whitelistTable->getRow( sprintf( "user_key='%s' AND whitelist_key='%s'",$user_info["user_key"] , $whitelist_key ) );
        // 不正アクセス対策
        if(!$whitelistInfo){
            $this->logger2->warn(__FUNCTION__, __FILE__, __LINE__,$user_info["user_key"]);
            return $this->action_admin_whitelist();
        }
        $key = $this->request->get('whitelist_key');
        $temp['whitelist_status']=-1;
        $ret = $this->whitelistTable->update($temp, sprintf( "whitelist_key = '%s'", $key ) );
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
        } else {
            $this->logger->info(__FUNCTION__."#delete whitelist successful!",__FILE__,__LINE__,array(
                "whitelist_key" => $key)
                );
            $data["user_id"] = $temp["member_id"];
        }
        // 操作ログ
        $this->add_operation_log('whitelist_delete', array('member_id' => $whitelistInfo['member_id']));
        $message .= "<li>".WHITE_DELETE_USER . "</li>";
        $this->render_admin_member($message);
    }

    /*
     * バリデーションチェック
     */

    //ID
    function idValidationCheck
    ($member_id, $whitelist_key, $user_key){

        $regex_rule = '/^[[:alnum:]._-]{3,20}$/';
        $valid_result = EZValidator::valid_regex($member_id, $regex_rule);
        if ($valid_result == false){
            $message = MEMBER_ERROR_ID_LENGTH;
        }else{
            $idCheck = $this->whitelistTable->numRows(sprintf(" user_key = '%s' AND member_id='%s' AND whitelist_status = 0 AND whitelist_key!='%s'",
                     $user_key, $member_id, $whitelist_key));
            if ($idCheck > 0) {
                // 既に同レコードがあるものは重複エラー出力
                $message = MEMBER_ERROR_ID_EXIST;
            }
        }
        return $message;
    }

    //ID CSV追加時専用
    function idCsvValidationCheck
    ($member_id, $user_key){

        $regex_rule = '/^[[:alnum:]._-]{3,20}$/';
        $valid_result = EZValidator::valid_regex($member_id, $regex_rule);
        if ($valid_result == false){
            $message = MEMBER_ERROR_ID_LENGTH;
        }else{
            $idCheck = $this->whitelistTable->numRows(sprintf(" user_key = '%s' AND member_id='%s' AND whitelist_status = 0", $user_key, $member_id));
           if ($idCheck > 0) {
                // 既に同レコードがあるものは重複エラー出力
                $message = MEMBER_ERROR_ID_EXIST;
            }
        }
        return $message;
    }

    // 名前
    function nameValidationCheck($name){

        if (! $name ) {
            $message = MEMBER_ERROR_NAME ;
        } elseif (mb_strlen ( $name ) > 50) {
            $message = MEMBER_ERROR_NAME_LENGTH ;
        }
        return $message;
    }

    // 経理単位コード
    function accValidationCheck($accounting_unit_code) {
        $regex_rule = '/^[[:alnum:]]{1,8}$/';
        $valid_result = EZValidator::valid_regex ( $accounting_unit_code, $regex_rule );
        if ($valid_result == false) {
            $message = ACCOUNTING_ERROR_ID_LENGTH;
        }
        return $message;
    }

    // 費用部課コード
    function expValidationCheck($expense_department_code) {
        $regex_rule = '/^[[:alnum:]]{1,5}$/';
        $valid_result = EZValidator::valid_regex ( $expense_department_code, $regex_rule );
        if ($valid_result == false) {
            $message = DEPARTMENT_ERROR_ID_LENGTH;
        }
        return $message;
    }

    function showAdminLogin($message = ""){

        if ($message) {
            $this->template->assign('message', $message);
        }
        $this->display('user/admin_login.tmpl');
    }

    function showAdminTop(){
        $main = new Main();
        if ($message = $main->checkAdminLoginInfo()) {
            showAdminLogin($message);
            exit;
        }
        header("Location: /service/admin.php");
    }
}



$main =& new AppMember();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>

