<?php 

require_once("classes/mcu/config/McuConfigProxy.php");
require_once(dirname(__FILE__)."/../../../../logic/participant/ParticipantCreatedLogic.php");
require_once(dirname(__FILE__)."/../../../../controller/participant/create/client/interface/IMCU.php");

abstract class ACreatedParticipant implements IMCU {
	protected $logger;
	protected $logic;
	
	abstract public function execute();
	
	public function __construct($model) {
		$this->logger = EZLogger2::getInstance();

		$this->initialize($model);
	}
	
	protected function initialize($model) {
		$this->setLogic($model);
	}
	
	protected function setLogic($model) {
		$this->logic = new ParticipantCreatedLogic($model);
	}
	
	protected function reject() {
		$this->logic->rejectParticipant();
	}
	
	protected function remove() {
		$this->logic->removeParticipant();
	}
}
