<?php
require_once("config/config.inc.php");
require_once("classes/AppFrame.class.php");
require_once("classes/dbi/meeting.dbi.php");
require_once("classes/N2MY_FileUpload.class.php");
require_once("lib/EZLib/EZUtil/EZFile.class.php");

class AppFileUpload extends AppFrame
{
    var $logger = null;
    var $meetingLog = null;

    function init()
    {
        $this->objFileUpload = new N2MY_FileUpload();
    }

    public function default_view()
    {
        $this->logger2->debug( array( $_FILES, $_REQUEST ) );
        $request = $_REQUEST;
        // 会議キーに変換
        require_once("classes/mgm/MGM_Auth.class.php");
        $obj_MGMClass = new MGM_AuthClass(N2MY_MDB_DSN);
        $meeting_key = $obj_MGMClass->getMeetingID($request["meeting_key"]);
        $this->makeDir($meeting_key);
        $dir = $this->objFileUpload->getDir( $meeting_key );
        $destDir = $this->objFileUpload->getFullPath( $dir );
        $this->logger2->info(array($meeting_key, $dir, $destDir));

        if( false !== $destDir && $request["name"] ){
            $destFile = sprintf( "%s/%s", $destDir, $request["name"] );
            $this->moveFile( $destFile, $_FILES['Filedata']['tmp_name'] );
            require_once( "classes/dbi/file_cabinet.dbi.php" );
            $serverList = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true );
            list( $key, $dsn ) = each( $serverList["SERVER_LIST"] );
            $objFileCabinet = new FileCabinetTable( $dsn );
            $data = array(
                        "meeting_key"       => $meeting_key,
                        "tmp_name"          => $request["name"],
                        "type"              => $_FILES["Filedata"]["type"],
                        "size"              => $_FILES["Filedata"]["size"],
                        "file_name"         => $_FILES["Filedata"]["name"],
                        "file_cabinet_path" => $dir,
                        "status"            => 1,
                        "registtime"        => date( "Y-m-d H:i:s" )
                        );
            $objFileCabinet->add( $data );
            // 操作ログ
            require_once("classes/dbi/meeting.dbi.php");
            require_once("classes/core/dbi/Participant.dbi.php");
            $objMeeting     = new MeetingTable($this->get_dsn());
            $objParticipant = new DBI_Participant($this->get_dsn());
            $participant_id = $request['participant_id'];
            $meeting_info = $objMeeting->getRow("meeting_key = '".$meeting_key."'");
            $participant_info = $objParticipant->getRow("meeting_key = '".$meeting_key."' AND participant_key = '".$participant_id."'");
            if ($participant_info["member_key"]) {
                require_once("classes/dbi/member.dbi.php");
                $objMember = new MemberTable($this->get_dsn());
                $member_id = $objMember->getOne("member_key = ".$participant_info["member_key"], 'member_id');
            }
            //操作ログ追加
            require_once ("classes/dbi/operation_log.dbi.php");
            $objOperationLog = new OperationLogTable($this->get_dsn());
            $data = array(
                    "user_key"           => $meeting_info["user_key"],
                    "member_key"         => $participant_info["member_key"],
                    "member_id"          => $member_id,
                    "session_id"         => $participant_info['participant_session_id'],
                    "action_name"        => 'cabinet_upload',
                    "remote_addr"        => $_SERVER["REMOTE_ADDR"],
                    "info"               => serialize(array(
                        'room_key'          => $meeting_info['room_key'],
                        'meeting_name'      => $meeting_info['meeting_name'],
                        'participant_name'  => $participant_info['participant_name'],
                        'file_name'         => $_FILES["Filedata"]["name"],
                        )),
                    "operation_datetime" => date( "Y-m-d H:i:s" ),
            );
            $this->logger2->info($data);
            $objOperationLog->add($data);
            header("HTTP/1.0 200 OK");
            print "OK";
        } else
            $this->render_400();
    }

    private function moveFile( $destFile, $targetFile )
    {
        if( ! $this->objFileUpload->moveFile( $destFile, $targetFile ) ) $this->render_400();
    }

    private function makeDir( $meeting_key )
    {
        if( ! $this->objFileUpload->makeDir( $meeting_key ) ) {$this->render_400();}
    }

    private function render_400()
    {
        $this->logger2->info( array( $_FILES, $_REQUEST ) );
        header("HTTP/1.0 400 Bad Request");
        print "Bad Request";
        exit;
    }
}

$main = new AppFileUpload();
$main->execute();
