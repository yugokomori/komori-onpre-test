<?php
require_once("set_env.php");
require_once("classes/AppFrame.class.php");
require_once("classes/core/Core_Meeting.class.php");

class AppCheckReservationMeetingStatus extends AppFrame {

    function default_view() {
        //予約会議終了チェック
        $user_key = ADMIN_USER_KEY;
        require_once ("classes/dbi/room.dbi.php");
        $obj_Room = new RoomTable( $this->get_dsn() );
        $where = "user_key = ".mysql_real_escape_string($user_key);
        $rooms = $obj_Room->getRowsAssoc($where, null, null, null, "room_key, meeting_key");
        foreach( $rooms as $key => $val ){
            $query[$val["room_key"]] = $val["meeting_key"];
        }
        require_once("classes/core/Core_Meeting.class.php");
        $obj_CoreMeeting = new Core_Meeting($this->get_dsn());
        $obj_CoreMeeting->checkReservationMeetingStatus($query);


        // 終了した会議のConferenceも落とすようにする
        $obj_CoreMeeting->checkConference();
    }

}
$main = new AppCheckReservationMeetingStatus();
$main->execute();