<?php

require_once("classes/N2MY_DBI.class.php");

class VideoConferenceParticipantTable extends N2MY_DB {

    var $table = "video_conference_participant";
    var $logger = null;
    protected $primary_key = "video_participant_key";

    public function __construct( $dsn )
    {
        $this->init($dsn, $this->table);
    }
    
    public function getActiveParticipantCountByMixerKey($mixerKey) {
    	$where = sprintf("media_mixer_key=%d", $mixerKey);
    	$result = $this->numRows($where);
    	return $result;
    }
    
    public function getCallingParticipantByMeetingKeyDid($meetingKey, $did) {
		$where = sprintf("meeting_key=%d AND did ='%s' AND is_active = 0", $meetingKey, $did);
		
		$db_res = $this->select($where , array('video_participant_key' => 'desc'));
		if (!$db_res) {
			return false;
		}
		return $db_res->fetchRow(DB_FETCHMODE_ASSOC);
    	
    }
}
