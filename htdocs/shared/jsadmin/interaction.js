var Interaction = Class.create();


Interaction.prototype.initialize = function(nodes, isFocussing) {
  if(document.isIE) {
    var hoverElems = document.getElementsByTagNames(nodes);

    if(!document.isIE7) {
      this.setActiveClass(hoverElems, 'mouseover', 'mouseout', 'over' );
	}

    if(isFocussing) {
      var focusElems = new Array();
      for(var i=0; i<hoverElems.length; i++) {
        if(hoverElems[i].type && hoverElems[i].type.match(/radio|checkbox|button|submit|reset|file/)) { continue; }
        focusElems.push(hoverElems[i]);
      }	
      this.setActiveClass(focusElems, 'focus', 'blur', 'focus');
	}
  }
}


Interaction.prototype.setActiveClass = function(nodes, evtOn, evtOff, cname) {
  for(var i=0; i<nodes.length; i++) {
    var node = nodes[i];

    node['on' + evtOn] = function() {
      Element.addClassName(this, cname);
    }
    node['on' + evtOff] = function() {
      Element.removeClassName(this, cname);
    }

    /*
    // マウスイベントの重複を防ぐ場合は、以下を使用（処理速度はやや劣る）
    Event.observe(
      node,
      evtOn,
      this.addClassName.bind(node, cname),
      false
    );

    Event.observe(
      node,
      evtOff,
      this.removeClassName.bind(node, cname),
      false
    );
    */
  }
}

/*
// 上記で Event.ovserve & Bind を使用するとき呼び出される
Interaction.prototype.addClassName = function(_className) {
  Element.addClassName(this, _className);
}

Interaction.prototype.removeClassName = function(_className) {
  Element.removeClassName(this, _className);
}
*/

Event.observe(
  window,
  'load',
  function() {
    var $_tag_array = ['input', 'textarea', 'select'];
    var interaction = new Interaction($_tag_array, true);
  },
  false
);





