<?php

require_once("classes/AppFrame.class.php");

class EnqueteList extends AppFrame {

    function default_view() {
        $this->action_enquete_list();
    }

    function action_enquete_list() {

        //ディレクトリ・ハンドルをオープン
        $res_dir = opendir( N2MY_DOCUMENT_ROOT."admin_tool/enqueteLog/" );

        //ディレクトリ内のファイル名を１つずつを取得
        while( $file_name = readdir( $res_dir ) ){
            //取得したファイル名を表示
            $list[] = $file_name;
            rsort($list);
        }

        array_splice($list,  -2);

        //ディレクトリ・ハンドルをクローズ
        closedir( $res_dir );

        $datetime = time();
        $this->template->assign('datetime', $datetime);
        $this->template->assign('rows', $list);
        $this->display('admin_tool/enquete_list/enquete_list.t.html');

    }


}

$main = new EnqueteList();
$main->execute();
?>
