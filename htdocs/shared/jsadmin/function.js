//rollover images & current images
function ButtonImage(img) {
    this.img = img;
    this.mouseoverImg = new Image();
    this.mouseoverImg.src = this.img.src.replace('.gif','_on.gif');
}

ButtonImage.prototype.setMouseOver = function() {
    var overImageSrc = this.mouseoverImg.src
    var outImageSrc  = this.img.src;
    this.img.onmouseover = function() { this.src = overImageSrc };
    this.img.onmouseout  = function() { this.src = outImageSrc };
}

ButtonImage.prototype.setCurrentMouseOver = function() {
    var overImageSrc = this.mouseoverImg.src.replace('_on.gif','_cr_on.gif');
    var outImageSrc  = this.img.src;
    this.img.onmouseover = function() { this.src = overImageSrc };
    this.img.onmouseout  = function() { this.src = outImageSrc };
}

ButtonImage.prototype.setDefaultOn = function() {
    this.img.src = this.img.src.replace('.gif','_cr.gif');
}

function setButtonImage(img,def) {
    var buttonImage = new ButtonImage(img);
    if (def == 1) {
        buttonImage.setDefaultOn();
        buttonImage.setCurrentMouseOver();
    }
    else {
        buttonImage.setMouseOver();
    }
    //return buttonImage;
}

Event.observe(
window,'load', function() {
    var buttons = document.getElementsByClassName('buttons');
    for (var i=0; i<buttons.length; i++) {
        if (window.defaultOn && defaultOn.indexOf('|'+buttons[i].id+'|')>=0) {
            setButtonImage(buttons[i],1);
        }
        else {
            setButtonImage(buttons[i]);
        }
    }
},
false
);

//form color changer
function focusColor(i){
    i.style.backgroundColor='#FFFFFF';
    i.style.border='1px solid #FF9900';
}
function blurColor(i){
    i.style.backgroundColor='#F3F3F3';
    i.style.border='1px solid #7f9db9';
}


function str_escape(str) {

    if (!str) return "";
    return str
        .replace(/&/g   ,"&amp;")
        .replace(/</g   ,"&lt;")
        .replace(/>/g   ,"&gt;")
        .replace(/\x22/g,"&quot;")
        .replace(/\x27/g,"&#39;")
    ;
    return str;

}

function get_flash_version() {
 var version='0.0.0';
 if(navigator.plugins && navigator.mimeTypes['application/x-shockwave-flash']){
  var plugin=navigator.mimeTypes['application/x-shockwave-flash'].enabledPlugin;
  // the code below is used in SWFObject
  //var plugin=navigator.plugins['Shockwave Flash'];
  if (plugin && plugin.description) {
   // convert the description like 'Shockwave Flash 9.0 r28' into version string like '9.0.8';
   // regex is provided by SWFObject
   version=plugin.description.replace(/^[A-Za-z\s]+/, '').replace(/(\s+|\s+b[0-9]+)/, ".");
   var tempArray = version.split( "." );
   if (version.match("b")) {
       version=version.replace(/\.[b][0-9]+$/, ".0");
   } else if (tempArray[3]) {
       version=version.replace("."+tempArray[3], "");
   } else {
       version=version.replace(/[A-Za-z]/, "");
   };
  }

 } else { // in the case of Win IE
  var x='';
  try {
   // for ver.7 and later
   var axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7");
   x=axo.GetVariable("$version");
  } catch(e) {
   try {
    // for ver.6
    axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");
    x="WIN 6,0,21,0";
    /*
     * GetVariable() crashes player version 6.0.22-29, and
     * players which have those versions throws when access
     * to AllowScriptAccess
     */
    axo.AllowScriptAccess="always";
    x=axo.GetVariable("$version");
   } catch(e) {
    if (!x.match(/^WIN/)) {
     try {
      // for 4.x,5.x
      axo=null;
      axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.3");
      // version 3 player throws when you call GetVariale().
      x=axo.GetVariable("$version");
     } catch(e) {
      if (axo) {
       // for 3.x
       x="WIN 3,0,18,0";
      } else {
       try {
        // for 2.x
        axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash");
        x="WIN 2,0,0,11";
       } catch(e) {
        x="WIN 0,0,0,0";
       }
      }
     }
    }
   }
  }
  // convert ActiveX version string to our version string like '9.0.28'
  version=x.replace(/^WIN /,'').replace(/,[0-9]+$/,'').replace(/,/g,'.');
 }
 // check version string format
 // Quicktime enabled Safari returns a description in natural language
 if (version.match(/^[0-9]+\.[0-9]+\.[0-9]+$/)) {
  return version;
 } else {
  return '0.0.0';
 }
}