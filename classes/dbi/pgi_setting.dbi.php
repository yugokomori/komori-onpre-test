<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("classes/N2MY_DBI.class.php");

/*
 * ビジネスロジックを含まない純粋なClipテーブルのDBI
 * */

class PGiSettingTable extends N2MY_DB {

    var $table = "pgi_setting";
    var $logger = null;
    protected $primary_key = 'pgi_setting_key';

    public function __construct( $dsn ) 
    {
        $this->init($dsn, $this->table );
    }
    public function add($data)
    {
        $data["registtime"] = date("Y-m-d H:i:s");
        $pgi_setting_key = parent::add($data);
        if (PEAR::isError($pgi_setting_key)) {
            throw new Exception("db errror where:$where , data:".print_r($data,true)." mysql said".mysql_error());
        }


        return $pgi_setting_key;
    }
    public function update($data, $where)
    {
        $data["updatetime"] = date("Y-m-d H:i:s");
        $res = parent::update($data, $where);
        if (PEAR::isError($res)) {
            throw new Exception("db errror where:$where , data:".print_r($data,true).' mysql_error:'.mysql_error());
        }
    }
    //}}}
    public function findByKey($pgi_setting_key)
    {
        if (!$pgi_setting_key) {
            throw new Exception("pgi_setting_key: $pgi_setting_key is empty ");
        }

        $where  = "pgi_setting_key = '".mysql_real_escape_string($pgi_setting_key)."'"
                  ." AND is_deleted = 0";

        $db_res = $this->select($where , array(),1);
        if ($db_res === false || PEAR::isError($db_res)) {
            throw new Exception(__FILE__.' '.__LINE__." where : $where");
        }

        if (!$db_res) {
            return array();
        }

        return $db_res->fetchRow(DB_FETCHMODE_ASSOC);
    }
    public function findByRoomKey($room_key)
    {
        if (!$room_key) {
            throw new Exception("room_key: $room_key is empty ");
        }

        $where  = "room_key = '".mysql_real_escape_string($room_key)."'"
                 ." AND is_deleted = 0";

        $db_res = $this->select($where , array('startdate' => 'desc'));

        if ($db_res === false || PEAR::isError($db_res)) {
            throw new Exception(__FILE__.' '.__LINE__." where : $where");
        }

        if (!$db_res) {
            return array();
        }
        $res = array(); 
        while ($row  =$db_res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $res[] = $row;
        }
        return $res;
    }
    public function findEnablleAtYM($room_key, $ym)
    {
        if (!$room_key) {
            throw new Exception("room_key: $room_key is empty ");
        }

        $where  = "room_key = '".mysql_real_escape_string($room_key)."'"
                 ." AND is_deleted = 0"
                 ." AND startdate <= '".$ym.'-01'."'";

        $db_res = $this->select($where ,array('startdate' => 'desc'), $limit = 1 ,$offset=0, $columns="*");
        if ($db_res === false || PEAR::isError($db_res)) {
            throw new Exception(__FILE__.' '.__LINE__." where : $where");
        }

        return $db_res ? $db_res->fetchRow(DB_FETCHMODE_ASSOC) : array();
    }
    public function findEnablleNow($room_key)
    {
        if (!$room_key) {
            throw new Exception("room_key: $room_key is empty ");
        }
        return $this->findEnablleAtYM($room_key, date('Y-m')); 
    }
}
