<?php

require_once("classes/N2MY_DBI.class.php");

class VideoConferenceIpListTable extends N2MY_DB {

    var $table = "video_conference_ip_list";
    var $logger = null;
    protected $primary_key = "video_conference_ip_list_key";

    var $status_enable = 1;
    var $status_disable = 0;

    public function __construct( $dsn )
    {
        $this->init($dsn, $this->table);
        $this->logger = EZLogger2::getInstance();
    }

    public function getThisOperationId() {
        $result = $this->getRow(null, "max(operation_id) as operation_id");
        return ++$result["operation_id"];
    }


    /**
     * 会議室番号を元に優先IPリストを取得する
     * @param int $room_num 会議室番号
     * @param int $user_key ユーザーキー
     * @return array $reslut 接続IP 該当データがない場合はNULL
     */
    function getConnectIpList($room_num, $user_key){

        $room_name_array = str_split($room_num);
        $room_name_count = count($room_name_array);
        $where_list = array();
        for($i = 0; $i < $room_name_count; $i++){
            $tmp_name= "";
            for($z = 0; $z <= $i; $z++){
                $tmp_name = $tmp_name.$room_name_array[$z];
            }
            $tmp_name = $tmp_name . "*";
            $where_list[] = $tmp_name;
        }

        $where = sprintf("( room_num = '%s'" , $room_num);

        foreach($where_list as $str){
            $where =sprintf($where . " OR room_num = '%s'" , $str);
        }

        $where = $where . ") AND status = " . $this->status_enable . " AND user_key = ".$user_key;
        $sort = array(
                "priority" => "ASC"
        );

        $reslut = $this->getRowsAssoc($where  , $sort);

        return $reslut;

    }

    /**
     * 会議室番号を元に優先IPを取得する
     * @param int $room_num 会議室番号
     * @param int $user_key ユーザーキー
     * @return array $IP 接続IP 該当データがない場合はNULL
     */
    function getConnectIp($room_num, $user_key){
        $ip_list = $this->getConnectIpList($room_num, $user_key);
        return $ip_list[0]["connect_ip"];

    }

    public function add( $data )
    {
    	$data["create_datetime"] = date("Y-m-d H:i:s");
    	$data["update_datetime"] = date("Y-m-d H:i:s");
    	return parent::add($data);
    }

    public function update( $data, $where )
    {
    	$data["update_datetime"] = date("Y-m-d H:i:s");
    	return parent::update($data, $where);
    }

}
