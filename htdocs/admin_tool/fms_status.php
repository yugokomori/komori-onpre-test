<?php
# 以下は環境によって書き換えてください
$config_file = realpath(dirname(__FILE__)."/../../config")."/config.ini";
$config = parse_ini_file($config_file, true);
$dsn_str = $config["GLOBAL"]["auth_dsn"];

$dsn  = parse_url($dsn_str);
$host = ($dsn["port"]) ? $dsn["host"].":".$dsn["port"] : $dsn["host"];
$user = $dsn["user"];
$pass = $dsn["pass"];
$db   = substr($dsn["path"],1);

$conn = mysql_connect($host, $user, $pass);
$db_selected = mysql_select_db($db, $conn);
if (!$db_selected){
    die("DB connect error :".mysql_error());
}

$fms = $_REQUEST["fms"];
$status = $_REQUEST["status"];

if ($status == "stop") {
    $sql = "UPDATE `fms_server` SET `is_available` = '0' WHERE `server_address` ='".addslashes($fms)."'";
    $result = mysql_query($sql);
    if (!$result) {
        print "Not stop ".$fms." : " .mysql_error();
        exit;
    }
    print "stop : ".$fms;
    exit;
} else if ($status == "start") {
    $sql = "UPDATE `fms_server` SET `is_available` = '1' WHERE `server_address` ='".addslashes($fms)."'";
    $result = mysql_query($sql);
    if (!$result) {
        print "Not start ".$fms." : " .mysql_error();
        exit;
    }
    print "start : ".$fms;
    exit;
}
?>