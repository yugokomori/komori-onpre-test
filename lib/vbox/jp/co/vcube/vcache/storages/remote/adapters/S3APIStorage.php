<?php

/**
 * @author Yuki Asano <y-asano@vcube.co.jp>
 * @copyright Vcube, Inc. All Rights Reserved.
 * @version 0.1
 */

require_once( "jp/co/vcube/vcache/storages/remote/RemoteStorage.php" );
require_once( "jp/co/vcube/vcache/core/ResultArrayObject.php" );
require_once( "jp/co/vcube/util/ExecCommand.php" );
require_once( "jp/co/vcube/vcache/util/TransferOption.php" );

class S3APIStorage extends RemoteStorage
{
  /**
   * @private $result ResultArrayObject - A result value from copyTo method.
   */
  private $result;

  /**
   * The abstract method for each storage config.
   */
  public function getStorageConfig(){
  }

  /** @public $transfer_option TransferOption - Transfer option object. */
  public $transfer_option;

  /** @public $default_transfer_mode Integer - Default transfer mode. Default value is MountStorage::TRANSFER_MODE_SCP */
  public $default_transfer_mode = RemoteStorage::TRANSFER_MODE_S3CMD;

  /**
   * $this filepath copy to $target filepath.
   * @param $target Object - Storage instance that inherited AbstractStorage.
   * @throws Exception - Invalid return value. Allow only inherited ResultArrayObject class.
   * @return void
   */
  final public function copyTo( $target )
  {

    if ( $this->isRemoteStorage() && $target->isMountStorage() ) {
      $result = $this->downloadTo( $target );

    } elseif ( $this->isMountStorage() && $target->isRemoteStorage() ) {
      $result = $this->uploadTo( $target );

    } elseif ( $this->isMountStorage() && $target->isMountStorage() ) {
      throw new Exception( "LocalStorage can not allowed with LocalStorage." );

    } elseif ( $this->isRemoteStorage() && $target->isRemoteStorage() ) {
      throw new Exception( "RemoteStorage can not allowed with RemoteStorage." );
    }

    if ( !($result instanceof ResultArrayObject) && !is_subclass_of($result, 'ResultArrayObject') )
      throw new Exception( "Invalid return value. Allow only inherited ResultArrayObject class. - ". ($result ? get_class($result) : "void") );

    $this->result = $result;
    return $result;

  }

  /**
   * A result value from copyTo method.
   * @return ResultArrayObject - Return value must ResultArrayObject or inherited it.
   */
  final public function uploadTo( $target)
  {
    throw new Exception( "This method is not available" );
  }

  /**
  * upload from the target method.
  * @return ResultArrayObject - Return value must ResultArrayObject or inherited it.
  */
  final public function upload( $target)
  {
      if ( !$target->transfer_option )
        $target->transfer_option = new TransferOption( $target->default_transfer_mode );

      $result = null;
      switch( $target->transfer_option->getTransferMode() ) {

        case AbstractStorage::TRANSFER_MODE_S3CMD:
          $result = $this->uploadByS3CMD( $target );
          break;

        case AbstractStorage::TRANSFER_MODE_S3SYNC:
          $result = $this->uploadByS3SYNC( $target );
          break;

        default:
          throw new Exception( "Invalid transfer_mode. - ".$this->transfer_mode );
      }

      return $result;

  }
  /**
  * download from the target method.
  * @return ResultArrayObject - Return value must ResultArrayObject or inherited it.
  */
  final public function download( $target)
  {
    throw new Exception( "This method is not available" );
  }

  final private function uploadByS3CMD( $target )
  {
    if ( !$target->transfer_option )
      $target->transfer_option = new TransferOption( $target->default_transfer_mode );

    $config = $this->getStorageConfig();

    $cmd = new ExecCommand( $target->transfer_option->timeout, $target->transfer_option->expect_return );

    $cmd_path = sprintf( "%s put  ", $config->s3cmd);

    $cmd->setTrustedArgument( $cmd_path );
    $cmd->setTrustedArgument( $target->transfer_option->first_argument );

    $target_path = sprintf( " %s:%s ", $config->s3bucket, $this->filepath);

    $cmd->setTrustedArgument( $target_path );

    if ( $this->unescape )
      $cmd->setTrustedArgument( $target->filepath );
    else
      $cmd->setArgument( $target->filepath );

    $cmd->setTrustedArgument( $target->transfer_option->end_argument );

    if ( $target->transfer_option->async_transfer )
      $cmd->setTrustedArgument( " & " );

    $cmd->execute( $config->ruby_path );

    return new ResultArrayObject( $cmd->getResults() );
  }

  /**
   * $this filepath upload to $target filepath.
   * @param $target Object - Storage instance that inherited AbstractStorage.
   * @throws Exception - Invalid return value. Allow only inherited ResultArrayObject class.
   * @return void
   */
  final private function uploadByS3SYNC( $target)
  {
    $config = $this->getStorageConfig();

    $cmd = new ExecCommand( $target->transfer_option->timeout, $target->transfer_option->expect_return );

    $cmd->setTrustedArgument( $config->s3sync );
    $cmd->setTrustedArgument( $target->transfer_option->first_argument );

    if ( $this->unescape )
      $cmd->setTrustedArgument( $target->filepath );
    else
      $cmd->setArgument( $target->filepath );

    $target_path = sprintf( "%s:%s", $config->s3bucket, $this->filepath);

    if ( $target->unescape )
      $cmd->setTrustedArgument( $target_path );
    else
      $cmd->setArgument( $target_path );

    $cmd->execute( $config->ruby_path );

    return new ResultArrayObject( $cmd->getResults() );
  }

  /**
   * $this filepath download to $target filepath.
   * @param $target Object - Storage instance that inherited AbstractStorage.
   * @throws Exception - Invalid return value. Allow only inherited ResultArrayObject class.
   * @return void
   */
  final public function downloadTo( $target )
  {
    /* Do not remove below line when you override this method. */
    $this->validateDownloadTo( $target );

    if ( !$this->transfer_option )
      $this->transfer_option = new TransferOption( $this->default_transfer_mode );

    $result = null;
    switch( $this->transfer_option->getTransferMode() ) {

      case AbstractStorage::TRANSFER_MODE_S3CMD:
        $result = $this->downloadByS3CMD( $target );
        break;

      case AbstractStorage::TRANSFER_MODE_S3SYNC:
        $result = $this->downloadByS3SYNC( $target );
        break;

      default:
        throw new Exception( "Invalid transfer_mode. - ".$this->transfer_mode );
    }

    $this->result = $result;

     return $result;

  }

  /**
   * A result value from copyTo method.
   * @return ResultArrayObject - Return value must ResultArrayObject or inherited it.
   */
  public function getResultArrayObject()
  {
    return $this->result;
  }

  /**
   * A transfer method for SCP.
   * @param $target Object - Storage instance that inherited AbstractStorage.
   * @throws Exception - ExecCommand exceptions directly.
   * @return ResultArrayObject - Return value must ResultArrayObject or inherited it.
   */
  final private function transferBySCP( $target )
  {
    throw new Exception( "This method is not available" );
  }

  /**
   * A transfer method for SFTP.
   * @param $target Object - Storage instance that inherited AbstractStorage.
   * @return void
   */
  final private function transferBySFTP( $target )
  {
    throw new Exception( "This method is not available" );
  }

  /**
   * A transfer method for RSYNC.
   * @param $target Object - Storage instance that inherited AbstractStorage.
   * @return $cmd ExecCommand instance.
   */
  final private function transferByRSYNC( $target )
  {
    throw new Exception( "This method is not available" );
  }

  /**
   * A transfer method for S3CMD.
   * @param $target Object - Storage instance that inherited AbstractStorage.
   * @return void
   */
  final private function downloadByS3CMD( $target )
  {
    $config = $this->getStorageConfig();

    $cmd = new ExecCommand( $this->transfer_option->timeout, $this->transfer_option->expect_return );

    $cmd_path = sprintf( "%s get  ", $config->s3cmd);

    $cmd->setTrustedArgument( $cmd_path );
    $cmd->setTrustedArgument( $this->transfer_option->first_argument );

    $target_path =  sprintf( " %s:%s ", $config->s3bucket, $this->filepath);

    $cmd->setTrustedArgument( $target_path );

    if ( $this->unescape )
      $cmd->setTrustedArgument( $target->filepath );
    else
      $cmd->setArgument( $target->filepath );

    $cmd->setTrustedArgument( $this->transfer_option->end_argument );

    if ( $this->transfer_option->async_transfer )
      $cmd->setTrustedArgument( " & " );

    $cmd->execute( $config->ruby_path );

    return new ResultArrayObject( $cmd->getResults() );
  }

  /**
   * A transfer method for S3SYNC.
   * @param $target Object - Storage instance that inherited AbstractStorage.
   * @return void
   */
  final private function downloadByS3SYNC( $target )
  {
    $target_config = $target->getStorageConfig();

    $cmd = new ExecCommand( $this->transfer_option->timeout, $this->transfer_option->expect_return );

    $cmd->setTrustedArgument( $target_config->s3sync );
    $cmd->setTrustedArgument( $this->transfer_option->first_argument );

    $target_path = sprintf( "%s:%s", $target_config->s3bucket, $target->filepath);

    if ( $target->unescape )
      $cmd->setTrustedArgument( $target_path );
    else
      $cmd->setArgument( $target_path );

    if ( $this->unescape )
      $cmd->setTrustedArgument( $this->filepath );
    else
      $cmd->setArgument( $this->filepath );

    $cmd->execute( $target_config->ruby_path );

    return new ResultArrayObject( $cmd->getResults() );
  }

  /**
   * $this filepath copy to $target filepath, before execute delete method for $this filepath.
   * @param $target Object - Storage instance that inherited AbstractStorage.
   * @throws Exception - The rename() method was failed.
   * @return void
   */
  public function moveTo( $target )
  {
    throw new Exception( "This method is not available" );
  }

  /**
   * Create specified directory.
   * @throws Exception - Could not create a directory. The mkdir() method was failed.
   * @return void
   */
  public function makeDir()
  {
    throw new Exception( "This method is not available" );
  }

  /**
   * Return the exists filelist array without dot-files(`.` and `..`).
   * @throws Exception - It is not a directory.
   * @return $list Array - A exists filelist array.
   */
  public function filelist()
  {
    $config = $this->getStorageConfig();

    $cmd = new ExecCommand( $this->transfer_option->timeout, $this->transfer_option->expect_return );

    $cmd_path = sprintf( "%s list  ", $config->s3cmd);

    $cmd->setTrustedArgument( $cmd_path );
    $cmd->setTrustedArgument( $this->transfer_option->first_argument );

    $target_path =  sprintf( " %s:%s ", $config->s3bucket, $this->filepath);

    $cmd->setTrustedArgument( $target_path );

    $cmd->setTrustedArgument( $this->transfer_option->end_argument );

    $cmd->execute( $config->ruby_path );
    $result = $cmd->getResults();

    if($result['output'])
      unset($result['output'][0]);

    return new ResultArrayObject( array_values( $result['output'] ) );
  }

  /**
   * Delete a file.
   * @throws Exception - The unlink() method was failed.
   * @return void
   *
   */
  public function delete()
  {
    $config = $this->getStorageConfig();

    $cmd = new ExecCommand( $this->transfer_option->timeout, $this->transfer_option->expect_return );

    $cmd_path = sprintf( "%s delete  ", $config->s3cmd);

    $cmd->setTrustedArgument( $cmd_path );
    $cmd->setTrustedArgument( $this->transfer_option->first_argument );

    $target_path =  sprintf( " %s:%s ", $config->s3bucket, $this->filepath);

    $cmd->setTrustedArgument( $target_path );

    $cmd->setTrustedArgument( $this->transfer_option->end_argument );

    if ( $this->transfer_option->async_transfer )
      $cmd->setTrustedArgument( " & " );

    $cmd->execute( $config->ruby_path );
    $this->result = new ResultArrayObject( $cmd->getResults() );
  }

  /**
   * Verify a file exists.
   * @return Boolean
   */
  public function exists()
  {
    $list = $this->filelist();

    return in_array($this->filepath, (array)$list);
  }
}
