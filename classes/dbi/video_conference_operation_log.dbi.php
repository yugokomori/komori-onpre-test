<?php

require_once("classes/N2MY_DBI.class.php");

class VideoConferenceOperationLogTable extends N2MY_DB {

    var $table = "video_conference_operation_log";
    var $logger = null;
    protected $primary_key = null;

    public function __construct( $dsn )
    {
        $this->init($dsn, $this->table);
    }
    
    public function getThisOperationId() {
    	$result = $this->getRow(null, "max(operation_id) as operation_id");
    	return ++$result["operation_id"];
    }
}
