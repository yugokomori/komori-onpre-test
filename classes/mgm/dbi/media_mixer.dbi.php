<?php
require_once("classes/N2MY_DBI.class.php");

class MediaMixerTable extends N2MY_DB {

    var $table = 'media_mixer';
    protected $primary_key = "media_mixer_key";

    function __construct($dsn)
    {
        $this->logger =& EZLogger::getInstance();
        $this->init($dsn, $this->table);
    }
    
    public function getMobileMixerRecord($server_key = null, $media_mixer_key = null) {
        $where = "is_available = 1 AND mobile_mix = 1";
        if($server_key !== null) {
            $_sk = $server_key * 1;
            if(is_int($_sk)) {
                $where .= " AND mcu_server_key = " . addslashes($_sk);
            }
            else {
                return array();
            }
        }
        if ($media_mixer_key) {
            $where .= " AND media_mixer_key = " . addslashes($media_mixer_key);
        }
        
        $rows = $this->getRowsAssoc($where);
        if(DB::isError($rows)) {
            return array();
        }
        else {
            return $rows;
        }
    }
    
    public function getConferenceMixerRecord($server_key = null, $media_mixer_key = null) {
        $where = "is_available = 1 AND mobile_mix = 1";
        
        if($server_key !== null) {
            $_sk = $server_key * 1;
            if(is_int($_sk)) {
                $where .= " AND mcu_server_key = " . addslashes($_sk);
            }
            else {
                return array();
            }
        }
        if ($media_mixer_key) {
            $where .= " AND media_mixer_key = " . addslashes($media_mixer_key);
        }
        
        $rows = $this->getRowsAssoc($where);
        
        if(DB::isError($rows)) {
            return array();
        }
        else {
            return $rows;
        }
    }
}
