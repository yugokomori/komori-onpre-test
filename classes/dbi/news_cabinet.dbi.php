<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/N2MY_DBI.class.php");

class NewsCabinetTable extends N2MY_DB {

    var $maintenance = "";
    var $lang = "";

    /**
     * コンストラクタ
     *
     */
    function NewsCabinetTable($dsn) {
        $this->rules = array(
            "news_cabinet_key"  => array(
                "required" => true),
            "news_key"          => array(
                "required" => true),
            "type"              => array(
                "required" => true),
            "size"              => array(
                "required" => true),
            "extension"         => array(
                "required" => true),
            "file_name"         => array(
                "required" => true),
            "file_path"         => array(
                "required" => true),
            "create_datetime"   => array(
                "required" => true),
        );
        $this->init($dsn, "news_cabinet");
    }

    function update_news_cabinet($news_cabinet) {
        $where = "news_cabinet_key ='".$news_cabinet["news_cabinet_key"]."'";
        if($news_cabinet["delete_flg"] && !$news_cabinet["file_name"]){
        	$config = EZConfig::getInstance(N2MY_CONFIG_FILE);
        	$directory = $config->get("INFORMATION", "scp_dir");
        	$old_file = $this->getOne($where, "file_path");
        	//$this->logger2->info($directory.$old_file);
        	if($old_file) {
        		if(!$config->get("INFORMATION", "use_scp")) {
        			unlink($directory.$old_file);
        		} else {
        			//リモートサーバにファイル削除
        			$scp_server1 = $config->get("INFORMATION", "scp_server1");
        			$scp_server2 = $config->get("INFORMATION", "scp_server2");
        			$scp_user = $config->get("INFORMATION", "scp_user");
        			if($directory) {
        				$cmd = sprintf('ssh %s@%s "rm -f %s"' , $scp_user,$scp_server1,$directory.$old_file);
        				exec($cmd, $arr, $res);
        				if($res !== 0){
        					$this->logger2->info($arr );
        					$this->logger2->info($cmd );
        					$this->logger2->warn($directory.$new_path.":インフォメーションファイルがサーバー1に削除失敗。");
        					$cmd = sprintf('ssh %s@%s "rm -f %s"' , $scp_user,$scp_server2,$directory.$old_file);
        					exec($cmd, $arr, $res);
        					if($res !== 0){
        						$this->logger2->info($arr );
        						$this->logger2->info($cmd );
        						$this->logger2->warn($directory.$new_path.":インフォメーションファイルがサーバー2に削除失敗。");
        					}
        				}
        			}
        		}
        	}
        	$this->remove($where);
        } else if ($news_cabinet["update_flg"]) {
            $data = array(
                "tmp_name" => $news_cabinet["tmp_name"],
                "type" => $news_cabinet["type"],
                "size" => $news_cabinet["size"],
                "file_name" => $news_cabinet["file_name"],
                );
            $config = EZConfig::getInstance(N2MY_CONFIG_FILE);
            $directory = $config->get("INFORMATION", "scp_dir");
            $old_file = $this->getOne($where, "file_path");
            if($old_file) {
            	//元ファイルを削除
            	if(!$config->get("INFORMATION", "use_scp")) {
            		unlink($directory.$old_file);
            	} else {
            		//リモートサーバにファイル削除
            		$scp_server1 = $config->get("INFORMATION", "scp_server1");
            		$scp_server2 = $config->get("INFORMATION", "scp_server2");
            		$scp_user = $config->get("INFORMATION", "scp_user");
            		if($old_file && $directory) {
            			$cmd = sprintf('ssh %s@%s "rm -f %s"' , $scp_user,$scp_server1,$directory.$old_file);
            			exec($cmd, $arr, $res);
            			$this->logger2->info($res );
            			if($res !== 0){
            				$this->logger2->info($arr );
            				$this->logger2->info($cmd );
            				$this->logger2->warn($directory.$new_path.":インフォメーションファイルがサーバー1に削除失敗。");
            				$cmd = sprintf('ssh %s@%s "rm -f %s"' , $scp_user,$scp_server2,$directory.$old_file);
            				exec($cmd, $arr, $res);
            				if($res !== 0){
            					$this->logger2->info($arr );
            					$this->logger2->info($cmd );
            					$this->logger2->warn($directory.$new_path.":インフォメーションファイルがサーバー2に削除失敗。");
            				}
            			}
            		}
            	}
            }
            $data["extension"] = pathinfo($news_cabinet["file_path"], PATHINFO_EXTENSION);
            $data["session_id"] = uniqid();
            $new_path = $data["session_id"].".".$data["extension"];
            //$this->logger2->info($new_path);
	        if(!$config->get("INFORMATION", "use_scp")) {
	        	rename(N2MY_DOCUMENT_ROOT.$news_cabinet["file_path"],$directory.$new_path);
	        } else {
	        	//scp対応
	        	$scp_server1 = $config->get("INFORMATION", "scp_server1");
        		$scp_server2 = $config->get("INFORMATION", "scp_server2");
        		$scp_user = $config->get("INFORMATION", "scp_user");
				$cmd = sprintf('scp "%s" %s@%s:"%s"' , N2MY_DOCUMENT_ROOT.$news_cabinet["file_path"],$scp_user,$scp_server1,$directory.$new_path);
				exec($cmd, $arr, $res);
				if($res !== 0){
					$this->logger2->info($arr );
					$this->logger2->info($cmd );
					$this->logger2->warn($directory.$new_path.":インフォメーションファイルサーバー1に移行失敗。");
					$cmd = sprintf('scp "%s" %s@%s:"%s"' , N2MY_DOCUMENT_ROOT.$news_cabinet["file_path"],$scp_user,$scp_server2,$directory.$new_path);
					exec($cmd, $arr, $res);
					if($res !== 0){
						$this->logger2->info($arr );
						$this->logger2->info($cmd );
						$this->logger2->warn($directory.$new_path.":インフォメーションファイルサーバー2に移行失敗。");
					}
				}
	        }
            $data["file_path"] = $new_path;
            $this->update($data, $where);
        } else {
            $data = array(
                //"tmp_name" => $news_cabinet["tmp_name"],
                "file_name" => $news_cabinet["file_name"],
                );
            if(!$news_cabinet["tmp_name"]) {
            	//ファイル削除、名前だけ残す
            	$data["extension"] = "";
            	$data["tmp_name"] = "";
            	$data["size"] = "";
            	$data["type"] = "";
            	$data["file_path"] = "";
            }
            $this->update($data, $where);
        }
    }

    function add_news_cabinet($news_cabinet, $news_key) {
    	//$this->logger2->info($news_cabinet);
        $data = array(
            "news_key" => $news_key,
            "tmp_name" => $news_cabinet["tmp_name"],
            "type" => $news_cabinet["type"],
            "size" => $news_cabinet["size"]?$news_cabinet["size"]:"0",
            "file_name" => $news_cabinet["file_name"],
            "create_datetime" => date('Y-m-d H:i:s'),
            );
        if($news_cabinet["tmp_name"]) {
        	$config = EZConfig::getInstance(N2MY_CONFIG_FILE);
        	$directory = $config->get("INFORMATION", "scp_dir");
        	$data["extension"] = pathinfo($news_cabinet["file_path"], PATHINFO_EXTENSION);
        	$data["session_id"] = uniqid();
        	$new_path = $data["session_id"].".".$data["extension"];
        	if(!$config->get("INFORMATION", "use_scp")) {
        		if(!is_dir($directory)) {
        			mkdir($directory, 0777);
        		}
        		rename(N2MY_DOCUMENT_ROOT.$news_cabinet["file_path"],$directory.$new_path);
        	} else {
        		//scp対応
        		$scp_server1 = $config->get("INFORMATION", "scp_server1");
        		$scp_server2 = $config->get("INFORMATION", "scp_server2");
        		$scp_user = $config->get("INFORMATION", "scp_user");
        		$cmd = sprintf('scp "%s" %s@%s:"%s"' , N2MY_DOCUMENT_ROOT.$news_cabinet["file_path"],$scp_user,$scp_server1,$directory.$new_path);
        		exec($cmd, $arr, $res);
        		if($res !== 0){
        			$this->logger2->info($arr );
        			$this->logger2->info($cmd );
        			$this->logger2->warn($directory.$new_path.":インフォメーションファイルサーバー1に移行失敗。");
        			$cmd = sprintf('scp "%s" %s@%s:"%s"' , N2MY_DOCUMENT_ROOT.$news_cabinet["file_path"],$scp_user,$scp_server2,$directory.$new_path);
        			exec($cmd, $arr, $res);
        			if($res !== 0){
        				$this->logger2->info($arr );
        				$this->logger2->info($cmd );
        				$this->logger2->warn($directory.$new_path.":インフォメーションファイルサーバー2に移行失敗。");
        			}
        		}
        	}
        	$data["file_path"] = $new_path;
        }   
        $this->add($data);
    }

    function get_news_cabinet_list($news_key) {
        $where = "news_key = '".$news_key."'";
        $files = $this->getRowsAssoc($where, array("news_cabinet_key" => "asc"));
        for($i = 0; $i < 5; $i++) {
            if($files[$i]) {
                $news_cabinet[$i] = $files[$i];
                //$news_cabinet[$i]["tmp_name"] = $files[$i]["file_name"].".".$files[$i]["extension"];
            } else {
                $news_cabinet[$i] = array();
            }
        }
        return $news_cabinet;
    }

    function download_news_cabinet($session_id) {
        $where = "session_id ='".$session_id."'";
        $file_info = $this->getRow($where);
        //$this->logger2->info($file_info);
        if(!$file_info) {
            header("location:404.php");
        }
        header("Content-Type: application/jpeg");
        header("Content-Length: " . filesize(N2MY_DOCUMENT_ROOT.$file_info["file_path"]));
        header('Content-Disposition: attachment; filename="' . urlencode($file_info["tmp_name"]) . '"');
        readfile(N2MY_DOCUMENT_ROOT.$file_info["file_path"]);
        exit;
    }
}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
