ALTER TABLE  reservation ADD  `payment_type` TINYINT( 1 ) NOT NULL DEFAULT  '1' COMMENT '1:主催者負担、2:利用者負担' AFTER  `organizer_email`;
ALTER TABLE  reservation ADD  `is_publish` TINYINT( 1 ) NOT NULL DEFAULT  '1' COMMENT '0: 非公開、1: 公開中' AFTER  `payment_type`;

/* 延長機能 */
CREATE TABLE `reservation_extend` (
`reservation_extend_key` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '予約延長情報キー',
`reservation_session_id` varchar(64) NULL COMMENT  '予約セッションID',
`member_key` int(11)  COMMENT  'メンバーKEY',
`extend_minute` int(11) NOT NULL DEFAULT 0 COMMENT  '延長時間（分）',
`extend_seat` int(11) NOT NULL DEFAULT 0 COMMENT '拡張人数',
`extend_starttime` datetime NOT NULL COMMENT '延長開始時間',
`extend_endtime` datetime NOT NULL COMMENT '延長終了時間',
`create_datetime` datetime NOT NULL,
PRIMARY KEY (`reservation_extend_key`)
)ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
ALTER TABLE  `reservation_extend` ADD INDEX (  `reservation_session_id` );

ALTER TABLE  reservation ADD  `reservation_extend_flg` TINYINT( 4 ) NOT NULL DEFAULT  '0' COMMENT '会議延長フラグ' AFTER  `organizer_email`;
ALTER TABLE  reservation ADD  `reservation_extend_endtime` datetime	 NOT NULL DEFAULT  '0000-00-00 00:00:00' COMMENT '会議延長時終了時間' AFTER  `reservation_endtime`;
update reservation set reservation_extend_endtime = reservation_endtime;

ALTER TABLE `member` ADD `is_admin` TINYINT( 1 ) NOT NULL DEFAULT  '0' COMMENT '管理者IDフラグ' AFTER  `member_id`;
ALTER TABLE `member` CHANGE COLUMN `member_email` `member_email` TEXT NULL DEFAULT NULL;

CREATE TABLE IF NOT EXISTS `whitelist` (
  `whitelist_key` int(11) NOT NULL AUTO_INCREMENT,
  `user_key` int(11) NOT NULL DEFAULT '0',
  `member_id` text NOT NULL,
  `member_name` text NOT NULL,
  `accounting_unit_code` VARCHAR( 10 ) NULL,
  `expense_department_code` VARCHAR( 10 ) NULL,
  `whitelist_status` int(11) NOT NULL DEFAULT '0' COMMENT '-1:削除、0:有効',
  `create_datetime` datetime DEFAULT NULL COMMENT '作成日時',
  `update_datetime` datetime DEFAULT NULL COMMENT '更新日時',
  PRIMARY KEY (`whitelist_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
ALTER TABLE  `whitelist` ADD INDEX (  `accounting_unit_code` );
ALTER TABLE  `whitelist` ADD INDEX (  `expense_department_code` );

ALTER TABLE  `reservation` CHANGE  `reservation_pw`  `reservation_pw` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

ALTER TABLE  `member` ADD  `member_pin_cd` VARCHAR( 128 ) NULL COMMENT  'モバイル入室紐付け用コード' AFTER  `outbound_number_id` ,ADD INDEX ( member_pin_cd );