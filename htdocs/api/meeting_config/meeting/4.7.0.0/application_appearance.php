<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/AppFrame.class.php");
class API_ApplicationAppearance extends AppFrame
{
    function init() {
        $this->dsn = $this->get_dsn();
    }

    function default_view() {
        $this->render_default();
    }

    function render_default() {
        $this->display( "core/api/meeting_config/meeting/resources/core_appearance.t.xml" );
    }

}

$main = new API_ApplicationAppearance();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */