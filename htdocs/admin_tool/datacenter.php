<?php
require_once("classes/AppFrame.class.php");
require_once("classes/mgm/MGM_Auth.class.php");
require_once("classes/mgm/dbi/user.dbi.php");
require_once("classes/dbi/user.dbi.php");
require_once("lib/EZLib/EZUtil/EZCsv.class.php");

class AppDataCenterEgnore extends AppFrame {

    var $_sess_page;
    var $account_dsn = null;
    var $objUser = null;

    /**
     * 初期化
     */
    function init() {
        $_COOKIE["lang"] = "en";
        $this->logger->trace(__FUNCTION__,__FILE__,__LINE__,$this->_get_passage_time());
        $this->_sess_page = "_adminauthsession";
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $this->_name_space = md5(__FILE__);
        if ($this->get_dsn()) {
            $this->objUser = new UserTable($this->get_dsn());
        }
    }

    function default_view() {
        $this->display_form();
    }

   /**
     * フォーム・一覧取得
     */
    function display_form() {
        $this->logger->debug("request",__FILE__,__LINE__,$_REQUEST);
        $this->set_submit_key($this->_name_space);

        $request  = $this->request->getAll();
        $this->logger->trace("request",__FILE__,__LINE__,$request);
        $required = $this->session->get("required");
        $this->logger->trace("required",__FILE__,__LINE__,$required);
        if ($required) {
            $message = $this->session->get("message");
            $this->template->assign("message",$message);
            $this->template->assign("required",$required);
        }
        $before_user_id = $this->request->get("before_user_id");
        if ($before_user_id) {
            $this->template->assign("before_user_id",$before_user_id);
        }
        $before_datacenter_key = $this->request->get("before_datacenter_key");
        if ($before_datacenter_key) {
            $this->template->assign("before_datacenter_key",$before_datacenter_key);
        }
        $dsn = $this->get_dsn();

        require_once("classes/dbi/datacenter_ignore.dbi.php");
        $datacenter_ignore_db = new DatacenterIgnoreTable($dsn);
        $action_name = __FUNCTION__;
        $columns     = $datacenter_ignore_db->getTableInfo("data");
        $columns     = $this->_get_relation_data($dsn, $columns);
        $this->logger->trace("columns",__FILE__,__LINE__,$columns);

        $form_data = $this->session->get("form_data");
        if (!isset($form_data)) {
            $form_data = $this->set_form($action_name, "datacenter_ignore_key", "asc");
            $this->session->set("form_data", $form_data);
        }
        $session_user_data = $this->session->get("user_data");
        if (!empty($before_user_id)) {
            $user_data = array("user_id"=> $before_user_id, "user_key" => $this->request->get("before_user_key"));
        } else {
            $user_data = $session_user_data;
        }
        $page      = $user_data['page'];
        $limit     = $user_data['page_cnt'];
        $offset    = ($limit * ($page - 1));
        $where     = $datacenter_ignore_db->getWhere($columns, $user_data);
        $this->logger->debug("where",__FILE__,__LINE__,$where);

        // データ取得
        $rows        = $datacenter_ignore_db->getRowsAssoc(
            $where, array($form_data['sort_key'] => $form_data['sort_type']), $limit, $offset);
        $total_count = $datacenter_ignore_db->numRows($where);

        $pager_info = $this->setPager($limit, $page, $total_count);
        $this->template->assign("page", array(
            "pager"        => $pager_info,
            "action"       => $action_name,
            "after_action" => $this->request->get("after_action"))
            );
        $add_form = $this->session->get("temp", $action_name);
        $this->template->assign("add_form", array(
            "data"      => $add_form,
            "ok_action" => $action_name,
            "ng_action" => $action_name
        ));
        foreach ($columns as $key => $column) {
            if ($column["item"]["relation_data"]) {
                $datacenterkeys = $column["item"]["relation_data"];
                $this->template->assign("datacenterkeys",$datacenterkeys);
                break;
            }
        }
        $user_id = $session_user_data["user_id"];
        if (isset($user_id)) {
            $this->template->assign("user_id",$user_id);
        }
        $table = "datacenter_ignore";
        $this->template->assign("rows",$rows);
        $this->template->assign("columns",$columns);
        $this->template->assign("table",$table);

        $template = "admin_tool/".$table."/add_datacenter.t.html";
        $this->_display($template);
    }

    function _display ($template) {
        $staff_auth = $_SESSION["_authsession"]["data"]["authority"];
        $this->template->assign("authority", $staff_auth);
        $this->display($template);
    }

     /**
     * 除外DC DB登録
     */
    function action_datacenter_ignore_add($message = "") {
        $after_action = $this->request->get("after_action");
        $error_action = $this->request->get("error_action");
        if ($after_action) {
            $error_action = $error_action."&after_action=".$after_action;
        }
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            header("Location: datacenter.php?".$error_action."#fragment1");
        }
        if ($message) {
            $this->template->assign("message",$message);
        }
        $dsn = $this->get_dsn();

        require_once("classes/dbi/user.dbi.php");
        $user_db   = new UserTable($dsn);
        $form_data = $this->request->get("add_form");
        $user_data = $this->session->get("user_data");
        if (!$user_data["user_id"]) {
            $user_id = $form_data["user_id"];
        } else {
            $user_id = $user_data["user_id"];
        }
        $where = "user_id = '".addslashes($user_id)."'";
        $this->logger->trace("where",__FILE__,__LINE__,$where);
        $msg_format = $this->get_message("error");
        $message  = array();
        $required = array();
        if (!$user_data["user_id"]) {
            $total_count = $user_db->numRows($where);
            if (empty($user_id)) {
                $message["required"] = "Please input User ID.";
                $required["user_id"] = "1";
            } else if (!$total_count) {
                $message["required"] = "This ID does not exist.";
                $required["user_id"] = "1";
            }
        }
        $rows = $user_db->getRowsAssoc($where);
        $user_key = 0;
        foreach ($rows as $columns) {
            foreach ($columns as $key => $column) {
              if ($key == "user_key") {
                  $user_key = $column;
                  break;
              }
            }
        }
        if (empty($required)) {
            require_once("classes/dbi/datacenter_priority.dbi.php");
            $datacenter_priority_db = new DatacenterPriorityTable($dsn);
            $datacenter_key         = $form_data["datacenter_key"];
            $where = "user_key = '".addslashes($user_key)."'".
                     " AND datacenter_key = '".addslashes($datacenter_key)."'";
            $this->logger->trace("where",__FILE__,__LINE__,$where);
            $total_count = $datacenter_priority_db->numRows($where);

            require_once("classes/dbi/datacenter_ignore.dbi.php");
            $datacenter_ignore_db = new DatacenterIgnoreTable($dsn);
            if (0 < $total_count) {
                    $message["required"]        = "It has been set as priority datacenter.";
                    $required["datacenter_key"] = "1";
            } else {
                $where = "user_key = '".addslashes($user_key)."'";
                $rows  = $datacenter_ignore_db->getRowsAssoc($where);
                foreach ($rows as $columns) {
                    foreach ($columns as $key => $column) {
                       if ($key == "datacenter_key") {
                           $add_datacenter_key[$column] = $column;
                      }
                    }
                }
                if (!empty($add_datacenter_key)) {
                    require_once("classes/core/dbi/DataCenter.dbi.php");
                    $datacenter_db = new N2MY_DB($this->account_dsn, "datacenter");
                    $where         = " datacenter_key NOT IN (". implode(",",$add_datacenter_key) .")";
                    if (!$datacenter_db->numRows($where)) {
                        $message["required"]        = "All datacenters have been set.";
                        $required["datacenter_key"] = "1";
                    }
                }
                if (empty($required)) {
                    foreach ($add_datacenter_key as $add_key => $add_column) {
                        if ($add_key == $datacenter_key) {
                            $message["required"]        = "Dataceter has already been set.";
                            $required["datacenter_key"] = "1";
                            break;
                        }
                    }
                }
            }
        }
        if (!empty($required)) {
            $this->session->set("message", $message);
            $this->session->set("required", $required);
            $error_action = $error_action."&before_user_id=".$user_id;
            $error_action = $error_action."&before_user_key=".$user_key;
            $error_action = $error_action."&before_datacenter_key=".$form_data["datacenter_key"];
            header("Location: datacenter.php?".$error_action);
            exit;
        } else {
            $add_data = array(
                "user_key"         => $user_key,
                "datacenter_key"   => $form_data["datacenter_key"],
                "create_datetime" => date("Y-m-d H:i:s"),
            );
            $datacenter_ignore_db->add($add_data);
            $this->logger->trace("add_data",__FILE__,__LINE__,$add_data);
            //操作ログ登録
            $operation_data = array (
                "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
                "action_name"        => __FUNCTION__,
                "table_name"         => "datacenter_ignore",
                "keyword"            => "add data",
                "info"               => serialize($form_data),
                "operation_datetime" => date("Y-m-d H:i:s"),
            );
            $this->add_operation_log($operation_data);

            $this->session->remove("message");
            $this->session->remove("required");
            $after_action = $after_action."&before_user_id=".$user_id;
            $after_action = $after_action."&before_user_key=".$user_key;
            $after_action = $after_action."&select_row_user_id=".$form_data["select_row_user_id"];
            header("Location: datacenter.php?".$after_action);
        }
    }

     /**
     * DC停止
     */
    function action_delete_datacenter() {
        $column_key   = $this->request->get("key");
        $where = $column_key." = '".addslashes($this->request->get("value"))."'";

        require_once("classes/dbi/datacenter_ignore.dbi.php");
        $datacenter_ignore_db = new DatacenterIgnoreTable($this->get_dsn());
        $datacenter_ignore_db->remove($where);
        //操作ログ登録
        $operation_data = array (
            "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
            "action_name"        => __FUNCTION__,
            "table_name"         => "$table",
            "keyword"            => "datacenter_ignore_key",
            "info"               => $column_key,
            "operation_datetime" => date("Y-m-d H:i:s"),
        );
        $this->add_operation_log($operation_data);

        $this->session->remove("message");
        $this->session->remove("required");
        header("Location: datacenter.php");
    }

     function _get_relation_data($dsn, $columns) {
        foreach ($columns as $key => $value) {
            if (isset($value["item"]["relation"])) {
                $relation = $value["item"]["relation"];
                if ($relation["db_type"] == "account") {
                    $relation_db = new N2MY_DB($this->account_dsn, $relation["table"]);
                } else {
                    $relation_db = new N2MY_DB($dsn, $relation["table"]);
                }
                if ($relation["status_name"]) {
                    $where = $relation["status_name"]."='".$relation["status"]."'";
                    $relation_data = $relation_db->getRowsAssoc($where);
                } else {
                    $relation_data = $relation_db->getRowsAssoc();
                }
                if ($relation["default_key"] || $relation["default_value"]) {
                    $columns[$key]["item"]["relation_data"][$relation["default_key"]] = $relation["default_value"];
                }
                foreach ($relation_data as $_data) {
                    $columns[$key]["item"]["relation_data"][$_data[$relation["key"]]] = $_data[$relation["value"]];
                }
            }
        }
        return $columns;
    }
}

$main =& new AppDataCenterEgnore();
$main->execute();