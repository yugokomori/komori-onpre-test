※このメールは送信専用のため、ご返信いただけません。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
V-CUBEセールス＆サポートからのお知らせ
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Web会議システムのご利用ありがとうございます。
現在開催している下記の会議に招待されました。
「招待URL」をクリックすると、Web会議に参加することができます。

■招待URL
{if $info.guest_url_format ==1}
{$invitation_url}
{else}
<{$invitation_url}>
{/if}
{if $reservationPassword !== ""}

■パスワード
{if $reservationPassword}{$reservationPassword}{else}なし{/if}{/if}

------------------------------
会議の詳細情報
------------------------------
{if $meeting_info.pin_cd && !$contact_name}
■V-CUBEモバイル用 暗証番号
{$meeting_info.pin_cd}
※iPad/AndroidからV-CUBEモバイルのアプリを起動し、
「暗証番号でログイン」から暗証番号を入力することでも、
Web会議への参加が可能です。

※V-CUBEモバイルアプリの入手方法は、下部のご利用案内をご確認ください。
{/if}
{if $temporarySipAddress && !$reservationPassword}

■テレビ会議システム専用ゲストアドレス
・SIP
{$temporarySipNumberAddress} 
{$temporarySipAddress} 
 ・H.323
{$temporaryH323NumberAddress}
{$temporaryH323Address}
※Polycom等のテレビ会議端末からV-CUBEミーティングに接続する場合はこちらのアドレスを入力して入室してください。
{/if}
{if $room_info.options.telephone == "1" && $meeting_info.pin_cd}

■電話番号一覧
固定電話・携帯電話から接続する国の電話番号を入力し、
音声ガイダンスに従って下記の暗証番号を入力することで、
Web会議にご参加頂けます。
{foreach from=$telephone_data item=location}{$location.name} :
{foreach from=$location.list item=tel_no}{$tel_no}
{/foreach}
{/foreach}

■暗証番号
{$meeting_info.pin_cd}
{/if}

------------------------------
ご利用案内
------------------------------
■ご利用方法
1. Webカメラとヘッドセットを、パソコンに接続します。
2. 環境確認ページにアクセスしてください。
   ネットワークや接続機器の状況が簡易診断できます。
{if $info.guest_url_format ==1}
   {$base_url}services/tools/checker/
{else}
   <{$base_url}services/tools/checker/>
{/if}
3. 上記の「招待URL」から会議室に入室します。

※会議室に入室した後、カメラやヘッドセットを接続した場合、
　一度会議室から退室し、再入室をすれば映像・音声を認識します。
※Webカメラのご用意がない場合、音声のみのご参加も可能です。

■推奨動作環境
動作環境ページにてご確認ください。
{if $info.guest_url_format ==1}
http://www.nice2meet.us/ja/requirements/meeting.html
{else}
<http://www.nice2meet.us/ja/requirements/meeting.html>
{/if}
■V-CUBEモバイル　アプリケーションの入手方法
スマートフォンやタブレット端末（iPad/Android）からは、
アプリをダウンロードいただくことでご利用いただけます。
・iOS用アプリケーション
{if $info.guest_url_format ==1}
http://itunes.apple.com/jp/app/id431070449?mt=
{else}
<http://itunes.apple.com/jp/app/id431070449?mt=>
{/if}
・Android用アプリケーション
{if $info.guest_url_format ==1}
https://play.google.com/store/apps/details?id=air.jp.co.vcube.mobile.vrms4
{else}
<https://play.google.com/store/apps/details?id=air.jp.co.vcube.mobile.vrms4>
{/if}

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　  "V-CUBE　セールス＆サポート" とは？
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■

　　" V-CUBE　セールス＆サポート" は、指定したアドレスにアクセスする
　　だけで、遠く離れた場所にいてもお互いの顔を見ながら映像と音声による
　　コミュニケーションが可能です。
　　また、共有ホワイトボード機能、録画機能、PC画面共有機能等、
　　コミュニケーションを円滑に取るための機能が満載です。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE　セールス＆サポート
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■開発・運営:
ブイキューブグループ {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
〒153-0051 東京都目黒区上目黒2-1-1 中目黒GTタワー20F
■カスタマーサポートセンター（日本語専用）
電話番号: 0570-002192（24時間365日電話受付）
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if} 
■カスタマーサポートセンター (英語その他)
電話番号：
　東京　+81-3-4560-1287
　マレーシア　+60-3-7724-9693
　シンガポール　+65-3158-2832
　中国　 +86-4006-618-2360
E-mail：{if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if} 
対応時間：平日 9:00-18:00(GMT+8)
土日及びマレーシアの祝日は休業となります。
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
