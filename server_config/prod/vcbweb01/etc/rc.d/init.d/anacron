#!/bin/sh
# Startup script for anacron
#
# chkconfig: 2345 95 05
# description: Run cron jobs that were left out due to downtime
# pidfile: /var/run/anacron.pid
#
# Source function library.
. /etc/rc.d/init.d/functions

[ -f /usr/sbin/anacron ] || exit 0

prog="anacron"
PIDFILE=/var/run/${prog}.pid
LOCKFILE=/var/lock/subsys/$prog
#
#  NOTE: anacron exits after it has determined it has no more work to do.
#        Hence, its initscript cannot do normal lock file management.
#        The anacron binary now creates its own /var/run/anacron.pid pid file
#        and /var/lock/subsys lock files, and removes them automatically at exit,
#        so at least there will be no more "anacron is dead but subsys locked" 
#        messages.
#

start() {
    echo -n $"Starting $prog: " 
    daemon +19 anacron -s
    RETVAL=$?
    if [ $RETVAL -ne 0 ]; then
	failure;
    fi;
    echo
    return $RETVAL
}

stop() {
    echo -n $"Stopping $prog: "
    if [ -f $PIDFILE ]; then
	killproc anacron
	RETVAL=$?
	if [ $RETVAL -ne 0 ]; then 
	    failure;
	fi;
    else
	RETVAL=1
	failure;
    fi
    echo
    return $RETVAL
}

case "$1" in
	start)
	    start
	    ;;
	
	stop)
	    stop
	    ;;
	
	status)
	    status anacron
	    ;;

	restart)
	    stop
	    start
	    ;;

	condrestart)
	    if [ -f $LOCKFILE ]; then
		stop
		start
	    fi
	    ;;
	
	*)
	    echo $"Usage: $0 {start|stop|restart|condrestart|status}"
	    exit 1

esac

exit $RETVAL
