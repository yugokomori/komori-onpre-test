<?php
$dir = dirname(__FILE__)."/../";
$dir = realpath($dir);
$file = $dir."/htdocs/.htaccess";
$fp   = fopen($file,"r");
while($line = fgets($fp,1024)){
    $line = trim($line);
    $params = preg_split("/[\s]+/",$line);
    if(count($params) > 2 ){
        switch ($params[0]) {
        case "php_value" :
            ini_set($params[1], $params[2]);
            break;
        case "SetEnv":
            $_SERVER[$params[1]] = $params[2];
            break;
        }
    }
}
fclose($fp);
?>
