<?php

require_once("classes/mcu/model/McuModel.php");
require_once("classes/mcu/model/gateway/McuGatewayProxy.php");

require_once(dirname(__FILE__)."/../../controller/ConferenceController.php");
require_once(dirname(__FILE__)."/../../logic/conference/MobileConferenceCreatedLogic.php");
require_once(dirname(__FILE__)."/../../params/CreateConferenceExtParams.php");

class MobileConferenceController extends ConferenceController {

	public function __construct() {
		parent::__construct();

		$this->mcuGatewayProxy		= new McuGatewayProxy();
	}

	/**
	 * <p></p>
	 * @return McuModel Object
	 */
	public function createMcuGatewayForMobile($roomKey ,$type=null ) {
		$did		= $this->meetingProxy->createDid();
		$this->mcuGatewayProxy->setActiveMcuServerThrowable($roomKey);
		$info = array(
				"status"			=> true,
				"contract"			=> false,
				"sipServer"			=> $this->mcuGatewayProxy->getMCUControllerServer(),
				"sipServerDomain"	=> $this->mcuGatewayProxy->getServerDomain(),
				"sipProxyIp"		=> $this->mcuGatewayProxy->getSipProxy(),
				"sipProxyPort"		=> $this->mcuGatewayProxy->getSipProxyPort(),
				"socketServer"		=> $this->mcuGatewayProxy->getSocketServer(),
				"socketServerPort"	=> $this->mcuGatewayProxy->getSocketPort(),
				"defaultId"			=> $this->configProxy->get("defaultId"),
				"defaultPw"			=> $this->configProxy->get("defaultPw"),
				"defaultViewId"		=> $this->configProxy->get("defaultViewId"),
				"defaultViewPw"		=> $this->configProxy->get("defaultViewPw"),
				"smallId"			=> $this->configProxy->get("smallId"),
				"smallPw"			=> $this->configProxy->get("smallPw"),
				"smallViewId"		=> $this->configProxy->get("smallViewId"),
				"smallViewPw"		=> $this->configProxy->get("smallViewPw"),
				"telephoneId"		=> $this->configProxy->get("telephoneId"),
				"telephonePw"		=> $this->configProxy->get("telephonePw"),
				"did"				=> $did);
		$model = new McuModel($info);
		return $model->getContent();
	}

	/**
	 *
	 * @param string $did
	 * @return string conference id.
	 */
	public function createConference($meeting_key, $did, $key, $media_mixer_key) {
		$logic = new MobileConferenceCreatedLogic();
		$mcuModel	= $this->mcuGatewayProxy->getMcuServerActiveMobileMix($key, $media_mixer_key);

		$this->mcuGatewayProxy->setMcuServerModelByMeetingKey($meeting_key);
		$mediaMixer = $logic->fixMediaMixerByMcuKey($this->mcuGatewayProxy->mcuServerModel["server_key"], $mcuModel["media_mixer_key"]);

		if ($confId = $logic->getConferenceByDID($did))
			$logic->removeConferenceByConfId($confId);

		$obj = new CreateConferenceExtParams(array(
                "name"		=> $this->configProxy->get("conference_name"),
                "did"		=> $did,
				"mixerId"	=> $mediaMixer["media_name"]."@".$mediaMixer["url"],
                "size"		=> $this->configProxy->get("size"),
                "compType"	=> 0,
				"vad"		=> true,
                "profileId"	=> $this->configProxy->get("profileId"),
                "audioCodecs"=> "",
                "videoCodecs"=> "",
                "textCodecs"	=> "",
                "autoAccept"	=> false,
                "callbackURL"	=> sprintf($this->configProxy->get("callback_conference"), $this->configProxy->get("soap_domain"))
				));
		$logic->createConferenceForMobile($obj->getParams());

		$logic->createConferenceRecord($meeting_key, $did, $mediaMixer["media_mixer_key"]);

		$params = $logic->getCreateFlashMosaic();
		$logic->createMosaic($params);

		$params = $logic->getCreateMobileMosaic();
		$logic->createMosaic($params);

		$params = $logic->getCreatePgiMosaic();
		$logic->createMosaic($params);

		$params = $logic->getCreateSidebarParams();
		$logic->createSidebar($params);

		$logic->setConferenceMosaicSlot();
		$logic->setFlashMosaicSlot();
		$logic->setMobileMosaicSlot();
	}

	public function removeConference($meetingKey) {
		$logic = new MobileConferenceCreatedLogic();
		$conferenceRecordList = $logic->getConferenceRecordByMeetingKey($meetingKey);
		for ($i = 0; $i < count($conferenceRecordList); $i++) {
			$conference = $conferenceRecordList[$i];

			$logic->removeConferenceByConfId($conference["conference_id"]);
			if ($conference["is_active"])
				$logic->removeConferenceRecord($conference["conference_id"]);
		}
	}

	/**
	 * mobile conferenceの状況取得
	 * video_conference_optionが付与されている部屋の会議の場合は
	 * mobile conferenceではなく通常のvideo conferenceとする
	 *
	 * @param int $meeting_key
	 * @param int $meeting_key
	 * @return boolean
	 */
	public function getMobileConferenceStatus($meeting_key) {
		$meetingRecord = $this->meetingProxy->getMeetingByMeetingKey($meeting_key);
		if (!$meetingRecord)
			return false;
		$option = $this->meetingProxy->getSettingRecordByRoomKey($meetingRecord["room_key"]);
		if ($option)
			return false;

		$conference = $this->conferenceProxy->getActiveConferenceByMeetingKey($meetingRecord["meeting_key"]);
		return ($conference) ? true : false;
	}

	public function setVideoMute($meetingKey, $participantKey, $flag, $media_mixer_key) {
		$conferenceRecord = $this->conferenceProxy->getActiveConferenceByMeetingKey($meetingKey);
		if (!$conferenceRecord)
			return;
		$mcuServerRecord = $this->mcuGatewayProxy->getMcuServerByServerKey($conferenceRecord["mcu_server_key"], $media_mixer_key);
		if (!$mcuServerRecord)
			return;

		$wsdl = sprintf("%s:%d", $mcuServerRecord["server_address"], $mcuServerRecord["controller_port"]);
		$this->mcuControllerProxy->setWSDL($wsdl);
		parent::setVideoMute($meetingKey, $participantKey, $flag);
	}

	public function changeDataCenter($meetingRecord) {
		$fmsController= new FMSNetConnectionController($meetingRecord);
		$fmsController->connect();
		$fmsController->bootFMSInstance();

		$list = $this->conferenceProxy->getActiveConferenceSipTypeParticipantListByMeetingKey($meetingRecord["meeting_key"]);
		$data = array("participantList"=>$list,"callingStatus"=>"");
		$fmsController->noticeParticipantListChanged($data);
	}
}
