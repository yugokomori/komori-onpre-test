-- phpMyAdmin SQL Dump
-- version 3.0.1.1
-- http://www.phpmyadmin.net
--
-- 生成時間: 2013 年 11 月 21 日 20:44
-- サーバのバージョン: 5.5.23
-- PHP のバージョン: 5.2.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";-- --------------------------------------------------------

--
-- テーブルの構造 `action`
--

CREATE TABLE IF NOT EXISTS `action` (
  `action_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `action_name` text NOT NULL,
  `action_detail` text,
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`action_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- テーブルのダンプデータ `action`
--

INSERT INTO `action` VALUES (1, 'upload', NULL, '2006-06-19 17:35:32', NULL);
INSERT INTO `action` VALUES (2, 'filecabinet', NULL, '2006-06-19 17:35:32', NULL);
INSERT INTO `action` VALUES (3, 'chat', NULL, '2006-06-19 17:35:32', NULL);
INSERT INTO `action` VALUES (4, 'wboard', NULL, '2006-06-19 17:35:32', NULL);
INSERT INTO `action` VALUES (5, 'sharing', NULL, '2006-06-19 17:35:32', NULL);
INSERT INTO `action` VALUES (6, 'rec', NULL, '2006-06-19 17:35:32', NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `agency`
--

CREATE TABLE IF NOT EXISTS `agency` (
  `agency_id` int(11) NOT NULL AUTO_INCREMENT,
  `agency_name` varchar(150) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `create_datetime` varchar(45) DEFAULT NULL,
  `delete_datetime` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`agency_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `api_auth`
--

CREATE TABLE IF NOT EXISTS `api_auth` (
  `api_no` int(11) NOT NULL AUTO_INCREMENT COMMENT '連番',
  `appli_name` varchar(100) DEFAULT NULL COMMENT 'アプリケーション名',
  `appli_info` text NOT NULL,
  `api_key` varchar(64) NOT NULL COMMENT 'API_KEY',
  `secret` varchar(64) NOT NULL COMMENT '秘密鍵',
  `status` varchar(1) NOT NULL COMMENT '0:無効, 1:有効',
  `create_datetime` datetime NOT NULL COMMENT '作成日時',
  `update_datetime` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`api_no`),
  UNIQUE KEY `api_key` (`api_key`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='API認証' AUTO_INCREMENT=3 ;

--
-- テーブルのダンプデータ `api_auth`
--
INSERT INTO `api_auth` (`api_no`, `appli_name`, `appli_info`, `api_key`, `secret`, `status`, `create_datetime`, `update_datetime`) VALUES
(1, 'Centre', 'Centre', '2dec10f4df9bf6567c89bc75567842dc52c43d97873b4c337aa0295990d73695', '41a62e1dacf86088c7e2b82f136cd590bd9254455502044c34cd70fd0d8ead88', '1', '2010-11-22 12:01:09', '2010-11-22 12:01:09'),
(2, 'PresenceAppli2', 'プレゼンスアプリ用', '8f46a64ca94d1e3e6827162b403b4de97bf7b9f32a02a74b245f0edca8fa1ce8', '57f8a550faf179c172312f11e554285cdd88ea765c5cff719114bf587f1d9edc', '1', '2011-02-18 14:22:23', '2011-02-18 14:22:23');


-- --------------------------------------------------------

--
-- テーブルの構造 `convert_file`
--

CREATE TABLE IF NOT EXISTS `convert_file` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `db_host` varchar(100) NOT NULL COMMENT 'DB名',
  `document_path` text NOT NULL COMMENT 'パス',
  `document_id` varchar(255) DEFAULT NULL,
  `document_format` varchar(64) DEFAULT NULL COMMENT '変換後のフォーマット',
  `document_version` varchar(64) DEFAULT NULL COMMENT 'アニメーションのバージョン',
  `extension` varchar(255) NOT NULL DEFAULT '',
  `status` varchar(255) NOT NULL DEFAULT '',
  `priority` int(11) DEFAULT NULL COMMENT '優先度',
  `addition` text NOT NULL COMMENT '拡張',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`no`),
  KEY `document_id` (`document_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `convert_process`
--

CREATE TABLE IF NOT EXISTS `convert_process` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) NOT NULL DEFAULT '',
  `status` varchar(255) NOT NULL DEFAULT '',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`no`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `country_key` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` text NOT NULL,
  `country_consumption_tax` double NOT NULL DEFAULT '0',
  `country_registtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`country_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- テーブルのダンプデータ `country`
--

INSERT INTO `country` VALUES (1, 'jp', 5, '2003-08-17 13:13:38');
INSERT INTO `country` VALUES (2, 'us', 0, '2003-08-17 13:13:38');
INSERT INTO `country` VALUES (3, 'sg', 5, '2004-01-31 06:51:32');

-- --------------------------------------------------------

--
-- テーブルの構造 `custom_domain_service`
--

CREATE TABLE IF NOT EXISTS `custom_domain_service` (
  `custom_domain_service_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'カスタムドメインキー',
  `service_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '提供サービス名',
  `domain` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ドメイン名',
  `status` int(11) DEFAULT '1' COMMENT 'ステータス',
  `create_datetime` datetime DEFAULT NULL COMMENT '作成日',
  `update_datetime` datetime DEFAULT NULL COMMENT '更新日',
  PRIMARY KEY (`custom_domain_service_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='カスタムドメイン用' ;


--
-- テーブルの構造 `datacenter`
--

CREATE TABLE IF NOT EXISTS `datacenter` (
  `datacenter_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datacenter_name_ja` text NOT NULL,
  `datacenter_name_en` text NOT NULL,
  `datacenter_name_cn` text NOT NULL,
  `datacenter_name_tw` text NOT NULL,
  `datacenter_name_fr` text NOT NULL,
  `datacenter_country` varchar(32) NOT NULL DEFAULT 'jp',
  `free_use_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'フリーで利用する場合は1',
  `status` int(4) NOT NULL DEFAULT '1' COMMENT 'ステータス',
  `sort` int(11) NOT NULL DEFAULT '1' COMMENT '優先順位',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`datacenter_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `db_server`
--

CREATE TABLE IF NOT EXISTS `db_server` (
  `server_key` int(11) NOT NULL AUTO_INCREMENT,
  `host_name` varchar(100) NOT NULL,
  `dsn` text NOT NULL,
  `server_status` tinyint(1) NOT NULL DEFAULT '1',
  `server_registtime` datetime NOT NULL,
  `server_updatetime` datetime DEFAULT NULL,
  PRIMARY KEY (`server_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `eco_simulator_log`
--

CREATE TABLE IF NOT EXISTS `eco_simulator_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `endpoint` text NOT NULL,
  `startpoint` text NOT NULL,
  `move` double NOT NULL,
  `time` bigint(20) NOT NULL,
  `fare` bigint(20) NOT NULL,
  `co2` double NOT NULL,
  `num_trips` int(11) NOT NULL,
  `num_expances` int(11) NOT NULL DEFAULT '0',
  `entrytime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `faq`
--

CREATE TABLE IF NOT EXISTS `faq` (
  `faq_key` int(11) NOT NULL AUTO_INCREMENT,
  `faq_category` int(11) NOT NULL DEFAULT '0',
  `faq_subcategory` int(11) NOT NULL DEFAULT '0',
  `faq_order` int(11) NOT NULL DEFAULT '0',
  `faq_question` text NOT NULL,
  `faq_answer` text NOT NULL,
  `faq_status` int(11) NOT NULL DEFAULT '0',
  `faq_lang` varchar(255) NOT NULL DEFAULT '0',
  `faq_type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1: すべてのサービス, 2: 通常, 3: メンバー課金モデル',
  PRIMARY KEY (`faq_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `faq_data`
--

CREATE TABLE IF NOT EXISTS `faq_data` (
  `faq_key` int(11) NOT NULL,
  `lang` varchar(10) NOT NULL,
  `question` text,
  `answer` text,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  UNIQUE KEY `faq_key` (`faq_key`,`lang`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='FAQデータ';

-- --------------------------------------------------------

--
-- テーブルの構造 `faq_list`
--

CREATE TABLE IF NOT EXISTS `faq_list` (
  `faq_key` int(11) NOT NULL AUTO_INCREMENT,
  `faq_category` int(11) NOT NULL DEFAULT '0',
  `faq_subcategory` int(11) NOT NULL DEFAULT '0',
  `faq_order` int(11) NOT NULL DEFAULT '0',
  `faq_question` text NOT NULL,
  `faq_answer` text NOT NULL,
  `faq_status` int(11) NOT NULL DEFAULT '0',
  `faq_lang` varchar(255) NOT NULL DEFAULT '0',
  `faq_type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1: すべてのサービス, 2: 通常, 3: メンバー課金モデル',
  PRIMARY KEY (`faq_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=152 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `fms_server`
--

CREATE TABLE IF NOT EXISTS `fms_server` (
  `server_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datacenter_key` int(10) unsigned NOT NULL DEFAULT '0',
  `server_address` varchar(255) NOT NULL,
  `local_address` varchar(255) DEFAULT NULL COMMENT 'ローカルアドレス',
  `server_port` int(10) DEFAULT NULL,
  `server_country` varchar(32) DEFAULT 'jp',
  `server_count` int(10) unsigned DEFAULT '0',
  `server_priority` int(10) unsigned NOT NULL DEFAULT '0',
  `protocol_port` varchar(255) DEFAULT 'rtmp:1935,rtmp:80,rtmp:8080,rtmps:443,rtmps-tls:443' COMMENT 'FMS利用プロトコルポート',
  `server_version` varchar(20) NOT NULL COMMENT 'FMSバージョン',
  `is_ssl` tinyint(1) DEFAULT '0' COMMENT '暗号通信用',
  `use_global_link` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'GlobalLinkで利用するか',
  `maintenance_time` varchar(8) DEFAULT NULL COMMENT 'リブート時間',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT NULL,
  `is_available` tinyint(4) NOT NULL DEFAULT '1',
  `error_count` int(11) DEFAULT '0',
  PRIMARY KEY (`server_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `ip_whitelist`
--

CREATE TABLE IF NOT EXISTS `ip_whitelist` (
  `ip_whitelist_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'KEY',
  `user_key` int(11) NOT NULL,
  `ip` varchar(100) NOT NULL,
  `comment` varchar(50) DEFAULT NULL COMMENT 'IP制限用コメント',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: 有効, 9: 削除',
  `create_datetime` datetime NOT NULL COMMENT '作成日時',
  `update_datetime` datetime DEFAULT NULL COMMENT '更新日時',
  PRIMARY KEY (`ip_whitelist_key`),
  KEY `user_key` (`user_key`),
  KEY `ip` (`ip`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `layout`
--

CREATE TABLE IF NOT EXISTS `layout` (
  `layout_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `layout_id` int(10) unsigned DEFAULT NULL,
  `layout_name` varchar(255) DEFAULT NULL,
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`layout_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `mcu_server`
--

CREATE TABLE IF NOT EXISTS `mcu_server` (
  `server_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `server_country` varchar(32) DEFAULT 'jp',
  `server_address` varchar(255) NOT NULL,
  `number_domain` varchar(255) NOT NULL,
  `guest_domain` varchar(255) NOT NULL,
  `guest_number_domain` varchar(255) NOT NULL,
  `guest_h323_domain` varchar(255) NOT NULL,
  `guest_h323_number_domain` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `controller_port` int(10) NOT NULL DEFAULT '8080',
  `socket_port` int(10) NOT NULL DEFAULT '1500',
  `sip_proxy_port` int(10) NOT NULL DEFAULT '5060',
  `is_available` tinyint(4) NOT NULL DEFAULT '1',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`server_key`),
  KEY `ip` (`ip`),
  KEY `ip_2` (`ip`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `media_mixer`
--

CREATE TABLE IF NOT EXISTS `media_mixer` (
  `media_mixer_key` int(11) NOT NULL AUTO_INCREMENT,
  `mcu_server_key` int(10) NOT NULL,
  `media_name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `media_ip` varchar(255) NOT NULL,
  `local_net` varchar(255) NOT NULL,
  `public_ip` varchar(255) NOT NULL,
  `mobile_mix` int(1) NOT NULL,
  `is_available` int(1) NOT NULL,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  PRIMARY KEY (`media_mixer_key`),
  KEY `mcu_server_key` (`mcu_server_key`,`is_available`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- テーブルの構造 `meeting_date_log`
--

CREATE TABLE IF NOT EXISTS `meeting_date_log` (
  `log_no` int(11) NOT NULL AUTO_INCREMENT,
  `log_date` date NOT NULL DEFAULT '0000-00-00',
  `user_id` varchar(64) DEFAULT NULL COMMENT 'ユーザーID',
  `staff_id` varchar(64) DEFAULT NULL COMMENT '営業担当ID',
  `room_key` varchar(64) NOT NULL DEFAULT '',
  `use_time` int(11) NOT NULL DEFAULT '0',
  `use_time_mobile_normal` int(11) NOT NULL DEFAULT '0',
  `use_time_mobile_special` int(11) NOT NULL DEFAULT '0',
  `use_time_hispec` int(11) NOT NULL DEFAULT '0',
  `use_time_audience` int(11) NOT NULL DEFAULT '0',
  `use_time_h323` int(11) NOT NULL DEFAULT '0',
  `use_time_h323ins` int(11) NOT NULL DEFAULT '0',
  `account_time_mobile_normal` int(11) NOT NULL DEFAULT '0',
  `account_time_mobile_special` int(11) NOT NULL DEFAULT '0',
  `account_time_hispec` int(11) NOT NULL DEFAULT '0',
  `account_time_audience` int(11) NOT NULL DEFAULT '0',
  `account_time_h323` int(11) NOT NULL DEFAULT '0',
  `account_time_h323ins` int(11) NOT NULL DEFAULT '0',
  `meeting_cnt` int(11) NOT NULL DEFAULT '0',
  `participant_cnt` int(11) NOT NULL DEFAULT '0',
  `eco_move` double NOT NULL DEFAULT '0' COMMENT '移動量(km)',
  `eco_time` bigint(20) NOT NULL DEFAULT '0' COMMENT '時間(分)',
  `eco_fare` bigint(20) NOT NULL DEFAULT '0' COMMENT '料金(円)',
  `eco_co2` double NOT NULL DEFAULT '0' COMMENT 'CO2(kg)',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`log_no`),
  KEY `log_date` (`log_date`),
  KEY `room_key` (`room_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `member_status`
--

CREATE TABLE IF NOT EXISTS `member_status` (
  `member_status_key` int(11) NOT NULL AUTO_INCREMENT,
  `member_status_name` text NOT NULL,
  PRIMARY KEY (`member_status_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- テーブルのダンプデータ `member_status`
--

INSERT INTO `member_status` VALUES (1, 'Not Active');
INSERT INTO `member_status` VALUES (2, 'Audience User');
INSERT INTO `member_status` VALUES (3, 'Default User');
INSERT INTO `member_status` VALUES (4, 'メンバー解除');
INSERT INTO `member_status` VALUES (5, 'メンバー休止');
INSERT INTO `member_status` VALUES (6, 'オーディエンス');
INSERT INTO `member_status` VALUES (7, '通常ユーザー');
INSERT INTO `member_status` VALUES (8, 'アドミンユーザー');

-- --------------------------------------------------------

--
-- テーブルの構造 `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `news_key` int(11) NOT NULL AUTO_INCREMENT,
  `news_date` date NOT NULL DEFAULT '0000-00-00',
  `news_title` text NOT NULL,
  `news_contents` text NOT NULL,
  `news_show` int(11) NOT NULL DEFAULT '0',
  `news_delete` int(11) NOT NULL DEFAULT '0',
  `news_maintenance` int(11) NOT NULL DEFAULT '0',
  `news_lang` varchar(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`news_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'キー',
  `start_datetime` datetime NOT NULL COMMENT 'メンテナンス開始日時',
  `end_datetime` datetime NOT NULL COMMENT 'メンテナンス終了日時',
  `info` text NOT NULL COMMENT '情報',
  `url_ja` text NOT NULL COMMENT 'メンテナンスページ（日本語）',
  `url_en` text NOT NULL COMMENT 'メンテナンスページ（英語）',
  `url_zh` text NOT NULL COMMENT 'メンテナンスページ（中国語）',
  `default_lang` varchar(10) NOT NULL COMMENT '設定がない場合のデフォルト言語',
  `status` varchar(1) NOT NULL COMMENT 'ステータス',
  `create_datetime` datetime NOT NULL COMMENT '作成日時',
  `update_datetime` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='告知機能' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `operation_log`
--

CREATE TABLE IF NOT EXISTS `operation_log` (
  `operation_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'キー',
  `staff_key` int(11) NOT NULL COMMENT 'スタッフキー',
  `action_name` varchar(255) NOT NULL COMMENT 'アクション',
  `operation_datetime` datetime NOT NULL COMMENT '操作日付',
  `table_name` varchar(255) NOT NULL COMMENT '操作テーブル',
  `keyword` varchar(255) NOT NULL COMMENT '検索用のインデックス',
  `info` text NOT NULL COMMENT '操作内容',
  `create_datetime` datetime NOT NULL COMMENT '作成日時',
  PRIMARY KEY (`operation_key`),
  KEY `staff_key` (`staff_key`,`action_name`,`operation_datetime`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='操作履歴' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `option_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(64) NOT NULL DEFAULT '',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`option_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- テーブルのダンプデータ `options`
--

INSERT INTO `options` VALUES (1, 'ssl', '2006-06-19 17:35:32', NULL);
INSERT INTO `options` VALUES (2, 'hispec', '2006-06-19 17:35:32', NULL);
INSERT INTO `options` VALUES (3, 'sharing', '2006-06-19 17:35:32', NULL);
INSERT INTO `options` VALUES (4, 'mobile', '2006-06-19 17:35:32', NULL);
INSERT INTO `options` VALUES (5, 'h323', '2006-06-19 17:35:32', NULL);
INSERT INTO `options` VALUES (6, 'audience', '2006-06-19 17:35:32', NULL);
INSERT INTO `options` VALUES (7, 'messenger', '2006-06-19 17:35:32', NULL);
INSERT INTO `options` VALUES (8, 'audience_max', '2006-06-19 17:35:32', NULL);
INSERT INTO `options` VALUES (9, 'seat_max', '2006-06-19 17:35:32', NULL);
INSERT INTO `options` VALUES (10, 'narrow', '2006-06-19 17:35:32', NULL);
INSERT INTO `options` VALUES (11, 'upload', '2006-06-19 17:35:32', NULL);
INSERT INTO `options` VALUES (12, 'rec', '2006-06-19 17:35:32', NULL);
INSERT INTO `options` VALUES (13, 'translator', '2006-06-19 17:35:32', NULL);
INSERT INTO `options` VALUES (14, '', '0000-00-00 00:00:00', NULL);
INSERT INTO `options` VALUES (15, 'intra_fms', '0000-00-00 00:00:00', NULL);
INSERT INTO `options` VALUES (16, 'multicamera', '0000-00-00 00:00:00', NULL);
INSERT INTO `options` VALUES (17, 'telephone', '0000-00-00 00:00:00', NULL);
INSERT INTO `options` VALUES (18, 'record_gw', '0000-00-00 00:00:00', NULL);
INSERT INTO `options` VALUES (19, 'smartphone', '0000-00-00 00:00:00', NULL);
INSERT INTO `options` VALUES (20, 'whiteboard_video', '0000-00-00 00:00:00', NULL);
INSERT INTO `options` VALUES (21, 'teleconference', '2012-05-16 04:46:10', NULL);
INSERT INTO `options` VALUES (22, 'video_conference', '2012-05-16 04:46:10', NULL);
INSERT INTO `options` VALUES (23, 'minimumBandwidth80', '2012-05-16 04:46:10', NULL);
INSERT INTO `options` VALUES (24, 'h264', '2012-05-16 04:46:10', NULL);
INSERT INTO `options` VALUES (25, 'tls', '2012-07-19 11:52:49', NULL);
INSERT INTO `options` VALUES (28, 'GlobalLink', '2013-03-04 10:58:31', NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `participant_mode`
--

CREATE TABLE IF NOT EXISTS `participant_mode` (
  `participant_mode_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `participant_mode_name` varchar(64) NOT NULL DEFAULT '',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`participant_mode_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- テーブルのダンプデータ `participant_mode`
--

INSERT INTO `participant_mode` VALUES (1, 'general', '2006-06-19 17:35:32', NULL);
INSERT INTO `participant_mode` VALUES (2, 'messenger', '2006-06-19 17:35:32', NULL);
INSERT INTO `participant_mode` VALUES (3, 'mobile', '2006-06-19 17:35:32', NULL);
INSERT INTO `participant_mode` VALUES (4, 'h323', '2006-06-19 17:35:32', NULL);
INSERT INTO `participant_mode` VALUES (5, 'log_player', '2006-06-19 17:35:32', NULL);
INSERT INTO `participant_mode` VALUES (8, 'test_user', '2006-06-19 17:35:32', NULL);
INSERT INTO `participant_mode` VALUES (9, 'trial', '2006-11-05 16:50:09', NULL);
INSERT INTO `participant_mode` VALUES (10, 'h323ins', '0000-00-00 00:00:00', NULL);
INSERT INTO `participant_mode` VALUES (11, 'unknown', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `participant_role`
--

CREATE TABLE IF NOT EXISTS `participant_role` (
  `participant_role_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `participant_role_name` varchar(64) NOT NULL DEFAULT '',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`participant_role_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- テーブルのダンプデータ `participant_role`
--

INSERT INTO `participant_role` VALUES (1, 'default', '2006-11-05 16:09:50', '0000-00-00 00:00:00');
INSERT INTO `participant_role` VALUES (2, 'invite', '2006-11-05 18:42:20', '0000-00-00 00:00:00');
INSERT INTO `participant_role` VALUES (3, 'unknown', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- テーブルの構造 `participant_type`
--

CREATE TABLE IF NOT EXISTS `participant_type` (
  `participant_type_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `participant_type_name` varchar(64) NOT NULL DEFAULT '',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`participant_type_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- テーブルのダンプデータ `participant_type`
--

INSERT INTO `participant_type` VALUES(1, 'normal', '2006-06-19 17:35:32', NULL);
INSERT INTO `participant_type` VALUES(2, 'audience', '2006-06-19 17:35:32', NULL);
INSERT INTO `participant_type` VALUES(3, 'document', '2006-06-19 17:35:32', NULL);
INSERT INTO `participant_type` VALUES(4, 'compact', '2007-05-10 00:00:00', NULL);
INSERT INTO `participant_type` VALUES(5, 'telepresence', '2007-05-10 00:00:00', NULL);
INSERT INTO `participant_type` VALUES(6, 'whiteboard', '0000-00-00 00:00:00', NULL);
INSERT INTO `participant_type` VALUES(7, 'whiteboard_audience', '0000-00-00 00:00:00', NULL);
INSERT INTO `participant_type` VALUES(8, 'multicamera', '0000-00-00 00:00:00', NULL);
INSERT INTO `participant_type` VALUES(9, 'multicamera_audience', '0000-00-00 00:00:00', NULL);
INSERT INTO `participant_type` VALUES(10, 'h323', '0000-00-00 00:00:00', NULL);
INSERT INTO `participant_type` VALUES(11, 'h323ins', '0000-00-00 00:00:00', NULL);
INSERT INTO `participant_type` VALUES(12, 'phone', '0000-00-00 00:00:00', NULL);
INSERT INTO `participant_type` VALUES(13, 'unknown', '0000-00-00 00:00:00', NULL);
INSERT INTO `participant_type` VALUES(14, 'invisible_wb', '2010-08-30 22:39:28', NULL);
INSERT INTO `participant_type` VALUES(15, 'invisible_wb_audience', '2010-09-21 22:04:05', NULL);
INSERT INTO `participant_type` VALUES(16, 'staff', '2013-06-14 19:09:21', NULL);
INSERT INTO `participant_type` VALUES(17, 'customer', '2013-06-14 19:09:21', NULL);
INSERT INTO `participant_type` VALUES(18, 'whiteboard_staff', '2013-05-16 12:34:22', NULL);
INSERT INTO `participant_type` VALUES(19, 'whiteboard_customer', '2013-05-16 12:34:22', NULL);
INSERT INTO `participant_type` VALUES(20, 'invisible_wb_staff', '2013-05-16 12:34:22', NULL);
INSERT INTO `participant_type` VALUES(21, 'invisible_wb_customer', '2013-05-16 12:34:22', NULL);
INSERT INTO `participant_type` VALUES(22, 'teleconference', '2013-12-04 21:42:01', NULL);

-- --------------------------------------------------------

CREATE TABLE `pgi_session` (
  `pgi_session_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'プライマリキー',
  `system_key` varchar(32) DEFAULT NULL COMMENT 'pgiのシステムキー',
  `company_id` varchar(64) DEFAULT NULL COMMENT 'CompanyID',
  `pgi_session_token` varchar(255) DEFAULT NULL COMMENT 'Token',
  `update_datetime` datetime NOT NULL COMMENT 'Login Time',
  PRIMARY KEY (`pgi_session_key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


--
-- テーブルの構造 `php_session`
--

CREATE TABLE IF NOT EXISTS `php_session` (
  `id` varchar(32) NOT NULL DEFAULT '',
  `data` blob NOT NULL,
  `t` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `question_id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaire_id` int(11) NOT NULL COMMENT 'questionnaireのID',
  `lang1` text CHARACTER SET utf8,
  `lang2` text CHARACTER SET utf8,
  `lang3` text CHARACTER SET utf8,
  `lang4` text CHARACTER SET utf8,
  `lang5` text CHARACTER SET utf8,
  `lang6` text CHARACTER SET utf8 COMMENT 'タイ語',
  `lang7` text CHARACTER SET utf8 COMMENT 'インドネシア語',
  `type` int(4) NOT NULL COMMENT '表示タイプ?1:radio,2:checkbox,3:radio+comment,4:checkbox+comment,5:comment',
  `sort` int(11) NOT NULL,
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`question_id`),
  KEY `questionnaire_id` (`questionnaire_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- テーブルのダンプデータ `question`
--

INSERT INTO `question` VALUES (1, 1, '今回のWeb会議利用に満足されていますか？', 'How satisfied are you with this Web Conferencing service?', '您对本次网络视频会议的使用满意吗？', NULL, NULL, NULL, NULL, 1, 1, '2011-09-27 17:24:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES (2, 1, '映像について（複数選択可）', 'About Video (multiple selections)', '关于图像（可以多选）', NULL, NULL, NULL, NULL, 2, 2, '2011-09-28 12:35:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES (3, 1, '音声について（複数選択可）', 'About Audio (multiple selections)', '关于语音（可以多选）', NULL, NULL, NULL, NULL, 2, 3, '2011-09-29 20:06:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES (4, 1, 'デザインについて（複数選択可）', 'About Design  (multiple selections)', '关于设计（可以多选）', NULL, NULL, NULL, NULL, 4, 4, '2011-09-29 20:07:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES (5, 1, 'その他（複数選択可）', 'Others  (multiple selections)', '其他（可以多选）', NULL, NULL, NULL, NULL, 4, 5, '2011-09-29 20:06:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES (6, 1, 'Web会議の利用頻度をお教え下さい。', 'How often do you use this Web Conferencing service?', '请选择您使用网络视频会议的频率。', NULL, NULL, NULL, NULL, 1, 6, '2011-09-29 20:07:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES (7, 1, '今回のWeb会議のタイプはどのようなものですか？', 'What type of meeting was this?', '您这次的网络视频会议属于什么类型？', NULL, NULL, NULL, NULL, 1, 7, '2011-10-03 15:41:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES (8, 1, '今回の会議に参加した方はどのような方ですか？（複数選択可）', 'Who participated in this meeting? (multiple selections)', '这次会议的参会者身份是什么？（可以多选）', NULL, NULL, NULL, NULL, 1, 8, '2011-10-03 15:41:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES (9, 1, 'その他ご意見・ご要望があればお願いいたします（自由記述）', 'Please give us your comments and suggestions (Free description)', '请您填写其他的意见或建议。（自由阐述）', NULL, NULL, NULL, NULL, 5, 9, '2011-10-03 15:42:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES (10, 1, '今回はどの方法でWeb会議室に入室しましたか', 'How did you enter the meeting room?', '此次是通过什么方式进入会议室？', NULL, NULL, NULL, NULL, 1, 10, '2012-03-23 00:33:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES (11, 1, 'お名前', 'Name', '姓名', NULL, NULL, NULL, NULL, 5, 11, '2012-03-23 00:33:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES (12, 1, 'フリガナ', 'Name', '假名', NULL, NULL, NULL, NULL, 5, 12, '2012-03-23 00:33:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES (13, 1, '会社名・所属先', 'Company', '公司名・所属部门', NULL, NULL, NULL, NULL, 5, 13, '2012-03-23 00:33:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES (14, 1, 'メールアドレス', 'Email Address', '邮箱地址', NULL, NULL, NULL, NULL, 5, 14, '2012-03-23 00:33:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES (15, 1, '電話番号', 'Phone Number', '电话号码', NULL, NULL, NULL, NULL, 5, 15, '2012-03-23 00:33:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES (16, 2, '今回のWeb会議利用に満足されていますか？', 'How satisfied are you with this Web Conferencing service?', '您对本次网络视频会议的使用满意吗？', '您對本次網絡視频會議的使用滿意嗎？', 'Quel est votre degré de satisfaction concernant l''utilisation de ce service de Web conférence?', 'คุณได้รับความพึงพอใจในการใช้การประชุมเว็บนี้?', 'Seberapa puas anda dengan web konferensi service ini?', 1, 1, '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES (17, 2, '映像について（複数選択可）', 'About Video (multiple selections)', '关于图像（可以多选）', '關於圖像（可以多選）', 'A propos de la vidéo (choix multiples)', 'สำหรับวิดีโอ (เลือกได้หลายแบบ)', 'tentang video ( beberapa seleksi)', 2, 2, '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES (18, 2, '音声について（複数選択可）', 'About Audio (multiple selections)', '关于语音（可以多选）', '關於語音（可以多選）', 'A propos de l''audio (choix multiples)', 'สำหรับเสียง (เลือกได้หลายแบบ)', 'tentang audio (beberapa seleksi)', 2, 3, '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES (19, 2, 'その他問題があったと感じた点・ご意見・ご要望があればお願いいたします（自由記述）', 'Please give us your comments and suggestions (Free description)', '请您填写其他的意见或建议。（自由阐述）', '請您填寫其他的意見或建議。（自由闡述）', 'N''hésitez pas à nous faire des suggestions et des commentaires', 'โปรดให้ความคิดเห็นและข้อเสนอแนะของคุณ (คำอธิบาย)', 'tolong berikan kepada kami masukan dan pendapat kamu (bebas deskripsi)', 5, 4, '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES (20, 2, 'お名前', 'Name', '姓名', '姓名', 'Nom', 'ชื่อ', 'nama', 5, 5, '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES (21, 2, 'フリガナ', 'Name', '假名', '假名', 'Nom', 'นามแฝง', 'nama', 5, 6, '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES (22, 2, '会社名・所属先', 'Company', '公司名・所属部门', '公司名·所屬部門', 'Société', 'ชื่อบริษัทหรือหน่วยงาน', 'company', 5, 7, '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES (23, 2, 'メールアドレス', 'Email Address', '邮箱地址', '郵箱地址', 'Email', 'ที่อยู่อีเมล์', 'alamat email', 5, 8, '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question` VALUES (24, 2, '電話番号', 'Phone Number', '电话号码', '電話號碼', 'Numéro de téléphone', 'หมายเลขโทรศัพท์', 'nomor telepon', 5, 9, '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- テーブルの構造 `question_branch`
--

CREATE TABLE IF NOT EXISTS `question_branch` (
  `question_branch_id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL COMMENT '紐付くquestion_id',
  `lang1` text NOT NULL,
  `lang2` text NOT NULL,
  `lang3` text NOT NULL,
  `lang4` text NOT NULL,
  `lang5` text NOT NULL,
  `lang6` text COMMENT 'タイ語',
  `lang7` text COMMENT 'インドネシア語',
  `sort` int(11) NOT NULL,
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`question_branch_id`),
  KEY `question_id` (`question_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=58 ;

--
-- テーブルのダンプデータ `question_branch`
--

INSERT INTO `question_branch` VALUES (1, 1, '大変満足', 'Very satisfied', '非常满意', '', '', NULL, NULL, 1, '2011-09-27 21:14:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (2, 1, 'やや満足', 'Somewhat satisfied', '比较满意', '', '', NULL, NULL, 3, '2011-09-27 21:15:07', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (3, 1, '満足', 'Satisfied', '满意', '', '', NULL, NULL, 2, '2011-09-27 21:15:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (4, 1, 'どちらとも言えない', 'Unsure', '都不是', '', '', NULL, NULL, 4, '2011-09-28 12:50:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (5, 1, 'やや不満', 'Somewhat dissatisfied', '比较不满意', '', '', NULL, NULL, 5, '2011-09-28 12:50:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (6, 1, '不満', 'Dissatisfied', '不满意', '', '', NULL, NULL, 6, '2011-09-28 12:51:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (7, 1, '非常に不満', 'Very dissatisfied', '非常不满意', '', '', NULL, NULL, 7, '2011-09-28 12:51:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (8, 2, '相手の映像が表示されない', 'Cannot see others'' video', '未显示对方图像', '', '', NULL, NULL, 1, '2011-09-29 19:46:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (9, 2, '自分の映像が表示されない', 'Cannot see your own video', '未显示我方图像', '', '', NULL, NULL, 2, '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (10, 2, '映像の品質が悪い', 'Video quality is not good', '图像质量差', '', '', NULL, NULL, 3, '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (11, 3, '自分の音声が取得できない', 'Cannot hear your own voice', '无法接通我方语音', '', '', NULL, NULL, 1, '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (12, 3, '相手の音声が聞こえない', 'Cannot hear others'' voice', '无法听到对方语音', '', '', NULL, NULL, 2, '2011-09-28 12:52:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (13, 3, '音声が遅延する', 'Cause delay', '语音延迟', '', '', NULL, NULL, 4, '2011-09-28 21:24:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (14, 3, '音声がエコーする（やまびこのように聞こえる）', 'Cause echo', '语音回响（听起来有回音）', '', '', NULL, NULL, 5, '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (15, 3, '音声がハウリングする（キーンという耳障りな大きな音がする）', 'Cause howling noise', '语音啸响（声音尖锐刺耳、大声作响）', '', '', NULL, NULL, 6, '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (16, 4, '機能（ボタン）が多く操作に迷う', 'Too many features (buttons) and difficult to operate', '功能（按钮）太多令操作困惑', '', '', NULL, NULL, 1, '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (17, 4, 'デザインがわかりにくい（使いにくい）箇所がある。（お手数ですが詳細をご記入ください）', 'Design is not user-friendly (please enter details)', '有些部分设计难懂（不易使用）。（麻烦您填写详细内容）', '', '', NULL, NULL, 2, '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (18, 5, '突然切断される', 'Suddenly disconnect', '突然中断', '', '', NULL, NULL, 1, '2011-09-28 12:52:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (19, 5, 'デスクトップ共有が開始出来ない', 'Cannot start Desktop Sharing', '桌面共享无法开始', '', '', NULL, NULL, 2, '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (20, 5, 'デスクトップ共有が閲覧出来ない', 'Cannot view Desktop Sharing', '桌面共享无法阅览', '', '', NULL, NULL, 3, '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (21, 5, '全体的に動作が遅い', 'Slow in performance', '整体反应慢', '', '', NULL, NULL, 4, '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (22, 5, 'ホワイトボード操作に不具合があった（機能が多くあるため、お手数ですが詳細をご記入ください）', 'Trouble with Whiteboard operation (Since there are many features, please enter details)', '白板操作有些不便（因为功能较多、麻烦您填写详细内容）', '', '', NULL, NULL, 5, '2011-09-29 19:57:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (23, 6, '月1回', 'Monthly', '每月1次', '', '', NULL, NULL, 1, '2011-09-29 20:12:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (24, 6, '月2～3回程度', '2-3 times per month', '每月2~3次', '', '', NULL, NULL, 2, '2011-09-29 20:12:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (25, 6, '月4回程度（週1回）', '4 times per month (weekly)', '每月4次（每周一次）', '', '', NULL, NULL, 3, '2011-09-29 20:12:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (26, 6, '月4回以上', 'more than 4 times per month', '每月4次以上', '', '', NULL, NULL, 4, '2011-09-29 20:12:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (27, 6, '不定期', 'Irregular', '不定期', '', '', NULL, NULL, 5, '2011-09-29 20:12:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (28, 7, '営業会議', 'Sales meeting', '营业会议', '', '', NULL, NULL, 1, '2011-09-29 20:13:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (29, 7, '企画会議', 'Planning meeting', '企划会议', '', '', NULL, NULL, 2, '2011-09-29 20:13:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (30, 7, '報告会議', 'Debrief meeting', '报告会议', '', '', NULL, NULL, 3, '2011-09-29 20:13:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (31, 7, '商談', 'Business negotiation', '商业谈判', '', '', NULL, NULL, 4, '2011-09-29 20:13:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (32, 7, '研修・教育', 'Training', '培训•教育', '', '', NULL, NULL, 5, '2011-09-29 20:13:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (33, 7, '経営会議・取締役会議', 'Management meeting/Board meeting', '经营会议•董事会议', '', '', NULL, NULL, 6, '2011-09-29 20:13:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (34, 7, 'その他', 'Others', '其他', '', '', NULL, NULL, 7, '2011-09-29 20:13:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (35, 8, '経営者', 'Executive', '经营者', '', '', NULL, NULL, 1, '2011-09-29 20:14:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (36, 8, '役職者', 'Manager', '管理者', '', '', NULL, NULL, 2, '2011-09-29 20:14:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (37, 8, '担当者', 'Person in charge', '负责人', '', '', NULL, NULL, 3, '2011-09-29 20:14:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (38, 8, '取引先', 'Business partner', '交易户', '', '', NULL, NULL, 4, '2011-09-29 20:14:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (39, 8, '顧客', 'Customer', '顾客', '', '', NULL, NULL, 5, '2011-09-29 20:14:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (40, 8, '在宅勤務者', 'Homeworker', '在家办公者', '', '', NULL, NULL, 6, '2011-09-29 20:14:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (41, 8, 'その他', 'Others', '其他', '', '', NULL, NULL, 7, '2011-09-29 20:14:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (42, 3, '音が頻繁に途切れる', 'Lose sound very often', '经常听不到声音', '', '', NULL, NULL, 3, '2011-10-11 21:30:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (44, 10, 'V-CUBEミーティングのID/パスワードを使ってログイン', 'With User ID and Password', '使用V-CUBE Meeting的ID/密码登陆 ', '', '', NULL, NULL, 1, '2012-03-23 00:33:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (45, 10, '送られてきた招待メール内のURLをクリック', 'From Invitation URL', '点击收到的邀请邮件内的URL', '', '', NULL, NULL, 2, '2012-03-23 00:33:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (46, 16, '満足', 'Satisfied', '满意', '滿意', 'Satisfait', 'พึงพอใจ?', 'Puas', 1, '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (47, 16, '普通', 'So so', '一般', '一般', 'Couci-couça', 'ดังนั้น', 'Cukup Memuaskan', 2, '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (48, 16, '不満', 'Dissatisfied', '不满意', '不滿意', 'Mécontent', 'ไม่พอใจ', 'Tidak Puas', 3, '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (49, 17, '相手の映像が表示されない', 'Cannot see others'' video', '未显示对方图像', '未顯示對方圖像', 'Impossible de voir la vidéo des autres', 'ไม่ปรากฏภาพบุคคลอื่น', 'Tidak dapat melihat video yang lainnya', 1, '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (50, 17, '自分の映像が表示されない', 'Cannot see your own video', '未显示我方图像', '未顯示我方圖像', 'Impossible de voir ma vidéo', 'ไม่ปรากฎภาพคุณเอง', 'Tidak dapa melihat video saya', 2, '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (51, 17, '映像の品質が悪い', 'Video quality is not good', '图像质量差', '圖像品質差', 'La qualité vidéo n''est pas bonne', 'คุณภาพของวิดีโอไม่ดี', 'Kualitas video tidak bagus', 3, '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (52, 18, '自分の音声が取得できない', 'Cannot hear your own voice', '无法接通我方语音', '無法接通我方語音', 'Impossible d''entendre ma voix', 'ไม่ได้ยินเสียงคุณเอง', 'Tidak dapat mendengar suara saya sendiri', 1, '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (53, 18, '相手の音声が聞こえない', 'Cannot hear others'' voice', '无法听到对方语音', '無法聽到對方語音', 'Impossible d''entendre la voix des autres', 'ไม่ได้ยินเสียงบุคคลอื่น', 'Tidak dapat mendengar suara lainnya', 2, '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (54, 18, '音が頻繁に途切れる', 'Lose sound very often', '经常听不到声音', '經常聽不到聲音', 'Le son coupe régulièrement', 'เสียงถูกขัดจังหวะบ่อย', 'Suara sering hilang', 3, '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (55, 18, '音声が遅延する', 'Cause delay', '语音延迟', '語音延遲', 'Il y a de la latence', 'เกิดความล่าช้า', 'Karena delay', 4, '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (56, 18, '音声がエコーする（やまびこのように聞こえる）', 'Cause echo', '语音回响（听起来有回音）', '語音迴響（聽起來有回音）', 'Il y a de l''echo', 'เกิดเสียงสะท้อน', 'Karena echo', 5, '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `question_branch` VALUES (57, 18, '音声がハウリングする（キーンという耳障りな大きな音がする）', 'Cause howling noise', '语音啸响（声音尖锐刺耳、大声作响）', '語音嘯響（聲音尖銳刺耳、大聲作響）', 'Il y a du bruit', 'เกิดเสียงหอน', 'Karena lolongan', 6, '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- テーブルの構造 `questionnaire`
--

CREATE TABLE `questionnaire` (
  `questionnaire_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(64) CHARACTER SET utf8 NOT NULL COMMENT '表示箇所タイプ',
  `is_active` tinyint(4) NOT NULL DEFAULT '1' COMMENT '公開:1、非公開:0',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`questionnaire_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- テーブルのダンプデータ `questionnaire`
--

INSERT INTO `questionnaire` VALUES (1, 'meeting_end', 0, 0, '2011-09-27 17:20:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `questionnaire` VALUES (2, 'meeting_end', 1, 0, '2013-03-04 10:58:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- テーブルの構造 `relation`
--

CREATE TABLE IF NOT EXISTS `relation` (
  `relation_key` varchar(64) NOT NULL,
  `relation_type` varchar(64) NOT NULL,
  `user_key` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `create_datetime` datetime NOT NULL,
  KEY `relation_key` (`relation_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `service`
--

CREATE TABLE IF NOT EXISTS `service` (
  `service_key` int(11) NOT NULL AUTO_INCREMENT,
  `country_key` int(11) NOT NULL DEFAULT '0',
  `service_name` text NOT NULL,
  `max_seat` int(11) NOT NULL COMMENT '最大参加者数',
  `max_audience_seat` int(11) NOT NULL COMMENT '最大オーディエンス数',
  `max_whiteboard_seat` int(11) NOT NULL DEFAULT '0' COMMENT '資料ユーザー数',
  `max_guest_seat` int(11) NOT NULL,
  `max_guest_seat_flg` int(11) NOT NULL,
  `extend_max_seat` int(11) NOT NULL,
  `extend_max_audience_seat` int(11) NOT NULL,
  `extend_seat_flg` int(11) NOT NULL,
  `meeting_limit_time` int(11) NOT NULL DEFAULT '0' COMMENT 'デフォルト会議利用制限時間',
  `max_room_bandwidth` int(11) NOT NULL DEFAULT '0' COMMENT '部屋上限帯域',
  `max_user_bandwidth` int(11) NOT NULL DEFAULT '0' COMMENT '拠点上限帯域',
  `min_user_bandwidth` int(11) NOT NULL DEFAULT '0' COMMENT '拠点下限限帯域',
  `default_camera_size` varchar(64) DEFAULT NULL COMMENT 'デフォルトカメラサイズ',
  `disable_rec_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT '録画不可フラグ',
  `whiteboard_video` int(1) DEFAULT '0' COMMENT '資料共有ビデオ付き',
  `meeting_ssl` int(11) NOT NULL COMMENT '暗号化',
  `desktop_share` int(11) NOT NULL COMMENT 'デスクトップ共有',
  `hdd_extention` int(11) NOT NULL COMMENT '容量',
  `high_quality` int(11) NOT NULL COMMENT '高画質',
  `mobile_phone` int(11) NOT NULL COMMENT '携帯',
  `h323_client` int(11) NOT NULL COMMENT 'H323',
  `whiteboard` int(11) NOT NULL COMMENT '資料共有',
  `multicamera` int(11) NOT NULL COMMENT 'マルチカメラ',
  `telephone` int(11) NOT NULL COMMENT '電話連携',
  `smartphone` int(11) NOT NULL COMMENT 'スマートフォン連携',
  `record_gw` int(11) NOT NULL COMMENT '録画GW',
  `h264` int(11) NOT NULL DEFAULT '0' COMMENT 'H.264オプション',
  `global_link` int(11) DEFAULT '0' COMMENT 'グローバルリンク',
  `teleconference` int(11) NOT NULL DEFAULT '0' COMMENT '電話会議オプション',
  `hd_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'HDプランフラグ',
  `bandwidth_up_flg` tinyint(4) DEFAULT '0' COMMENT '帯域追加フラグ',
  `sales_flg` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'セールスオプション',
  `use_room_plan` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'ルームプラン利用フラグ',
  `use_member_plan` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'ID制利用フラグ',
  `use_sales_plan` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'セールスプラン利用フラグ',
  `whiteboard_page` int(11) NOT NULL DEFAULT '0' COMMENT 'アップロード可能ページ数',
  `whiteboard_size` int(11) NOT NULL DEFAULT '0' COMMENT 'アップロード可能ファイルサイズ',
  `document_filetype` varchar(255) DEFAULT NULL COMMENT 'ップロード可能ドキュメント（拡張子をカンマ区切りで登録）',
  `image_filetype` varchar(255) DEFAULT NULL COMMENT 'ップロード可能画像（拡張子をカンマ区切り）',
  `cabinet_filetype` varchar(255) DEFAULT NULL COMMENT '利用キャビネットファイルタイプ（カンマ区切り）',
  `ignore_staff_reenter_alert` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'スタッフ再入室アラート非表示フラグ',
  `service_initial_charge` int(11) NOT NULL DEFAULT '0',
  `service_charge` int(11) NOT NULL DEFAULT '0',
  `service_additional_charge` double NOT NULL DEFAULT '0',
  `service_freetime` int(11) NOT NULL DEFAULT '0',
  `service_registtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `service_status` tinyint(4) NOT NULL,
  `service_expire_day` int(11) NOT NULL DEFAULT '0' COMMENT '自動利用停止日数',
  `member_storage_size` int(11) NOT NULL DEFAULT '0' COMMENT '1メンバーのストレージ容量(Mbyte)',
  `add_storage_size` int(11) DEFAULT '1000' COMMENT 'room制用、1部屋毎ストレージ容量',
  `lang_allow` varchar(100) DEFAULT NULL COMMENT '言語許可リスト(「,」区切りで指定nullの場合は全許可)',
  PRIMARY KEY (`service_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=111 ;

--
-- テーブルのデータをダンプしています `service`
--

INSERT INTO `service` VALUES(1, 1, 'STOP', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2003-08-17 14:07:33', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(2, 2, 'STOP', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2003-08-17 14:07:33', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(3, 1, 'Vstaff', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2003-08-17 14:07:33', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(4, 1, 'Trial', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2003-08-17 14:07:33', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(5, 1, 'Temporary', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 100, 0, '2003-08-17 14:07:33', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(6, 1, 'Diamond', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 29000, 79900, 0, 60000, '2003-08-17 14:07:33', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(7, 2, 'Diamond', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 299, 799, 0, 60000, '2003-08-17 14:07:33', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(8, 1, 'Platinum', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 29000, 39900, 20, 2000, '2003-08-17 14:07:33', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(9, 2, 'Platinum', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 299, 399, 0.2, 2000, '2003-08-17 14:07:33', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(10, 1, 'Gold', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 29000, 29900, 25, 1200, '2003-08-17 14:07:33', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(11, 2, 'Gold', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 299, 299, 0.25, 1200, '2003-08-17 14:07:33', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(12, 1, 'Silver', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 29000, 19900, 30, 600, '2003-08-17 14:07:33', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(13, 2, 'Silver', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 299, 199, 0.3, 600, '2003-08-17 14:07:33', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(14, 1, 'Standard(旧)', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 29000, 9900, 35, 120, '2003-08-17 14:07:33', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(15, 2, 'Standard', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 299, 99, 0.35, 120, '2003-08-17 14:07:33', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(17, 2, 'Personal', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 10, 0.02, 300, '2003-08-17 14:07:33', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(18, 2, 'Silver(Reseller)', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 299, 199, 0, 40000, '2003-10-13 00:52:12', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(19, 2, 'Trial', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2003-11-26 17:23:43', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(20, 1, 'Personal', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 980, 2, 300, '2004-01-26 02:43:47', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(21, 3, 'Standard', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 299, 99, 0.35, 120, '2004-01-31 07:54:50', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(22, 3, 'Silver', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 299, 199, 0.3, 600, '2004-01-31 07:55:19', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(23, 3, 'Gold', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 299, 299, 0.25, 1200, '2004-01-31 07:55:42', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(24, 3, 'Platinum', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 299, 399, 0.2, 2000, '2004-01-31 07:56:30', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(25, 3, 'Platinum', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 299, 799, 0, 60000, '2004-01-31 07:56:59', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(26, 1, 'Lite（旧）', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 29000, 9900, 35, 120, '2004-10-20 14:52:32', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(27, 1, 'Basic', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 29000, 29900, 25, 1200, '0000-00-00 00:00:00', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(28, 1, 'Professional', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 29000, 79900, 0, 60000, '2004-10-20 14:58:47', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(29, 3, 'Trial', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2004-11-27 00:28:38', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(30, 1, 'VismeeProfessional', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 35000, 96000, 0, 60000, '2005-01-13 15:47:14', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(31, 1, 'VismeeBasic', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 35000, 36000, 30, 1200, '2005-01-13 15:52:53', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(32, 1, 'VismeeLite', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 35000, 12000, 42, 120, '2005-01-13 15:49:52', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(33, 1, 'Lite', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 29000, 9900, 95, 0, '2005-04-12 04:48:15', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(34, 1, 'FORSI', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 60000, '2005-08-02 11:17:37', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(35, 1, 'Seminar_Spot', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 10000, 0, 200, 0, '2005-11-15 05:29:57', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(36, 1, 'Seminar_Lite', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 29000, 29000, 120, 120, '2005-11-15 05:29:57', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(37, 1, 'Seminar_Basic', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 29000, 49900, 100, 120, '2005-11-15 05:29:57', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(38, 1, 'Seminar_Professional', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 29000, 99000, 80, 120, '2005-11-15 05:29:57', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(39, 1, 'Seminar_Event', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 190000, 179000, 60, 120, '2005-11-15 05:29:57', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(40, 1, 'Seminar_Trial', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2005-11-15 05:29:57', 0, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(41, 1, 'New_Trial', 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 9900, 190, 0, '2006-03-28 19:05:47', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(42, 1, 'Entry', 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 44900, 25, 1200, '2006-03-28 19:10:47', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(43, 1, 'Standard', 10, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 79900, 0, 60000, '2006-03-28 19:10:47', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(44, 1, 'High_Quality', 10, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 119900, 0, 60000, '2006-03-28 19:14:04', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(45, 1, 'Premium20', 9, 11, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 119900, 0, 60000, '2006-03-28 19:14:04', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(46, 1, 'Premium30', 9, 21, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 149900, 0, 60000, '2006-03-28 19:19:15', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(47, 1, 'Premium40', 9, 31, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 179900, 0, 60000, '2006-03-28 19:19:15', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(48, 1, 'おてがるプラン', 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 0, 10000, 2000, 10, 60, '2009-02-10 00:00:00', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(49, 1, '使い放題プラン', 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 0, 10000, 7000, 0, 60000, '2009-02-10 00:00:00', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(50, 1, 'Premium50', 9, 41, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 209900, 0, 60000, '2009-12-11 22:24:22', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(51, 1, 'Premium60', 9, 51, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 239900, 0, 60000, '0000-00-00 00:00:00', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(52, 1, 'Premium70', 9, 61, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 269900, 0, 60000, '0000-00-00 00:00:00', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(53, 1, 'Premium80', 9, 71, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 299900, 0, 60000, '0000-00-00 00:00:00', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(54, 1, 'Premium90', 9, 81, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 329900, 0, 60000, '0000-00-00 00:00:00', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(55, 1, 'Premium100', 9, 91, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 359900, 0, 60000, '0000-00-00 00:00:00', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(56, 1, 'ペーパーレス', 20, 0, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 30000, 30000, 0, 60000, '2009-12-21 22:32:11', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(57, 1, 'Entry_', 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 34900, 25, 1200, '2010-01-28 15:23:45', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(58, 1, '同時2拠点プラン', 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 20000, 20000, 0, 60000, '2010-03-12 14:31:36', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(59, 1, '同時3拠点プラン', 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 30000, 30000, 0, 60000, '0000-00-00 00:00:00', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(60, 1, '同時5拠点プラン', 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 0, 50000, 50000, 0, 60000, '0000-00-00 00:00:00', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(61, 1, '同時2拠点（大塚商会）', 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 20000, 20000, 0, 60000, '0000-00-00 00:00:00', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(62, 1, 'スマート（大塚商会）', 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 30000, 30000, 0, 60000, '0000-00-00 00:00:00', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(63, 1, 'スマートプラス（大塚商会', 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 50000, 50000, 0, 60000, '0000-00-00 00:00:00', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(64, 1, '一括登録用', 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 5000, '2010-04-01 15:34:14', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(65, 1, 'Premium20プラス', 20, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 129900, 0, 10000, '2010-04-28 10:30:54', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(66, 1, 'Premium30プラス', 19, 11, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 159900, 0, 60000, '2010-09-14 11:57:44', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(68, 1, '20110312', 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2011-03-12 13:29:37', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(67, 1, 'V-CUBEセンター', 9, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2010-11-25 23:08:14', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(69, 1, 'IT_Hinan', 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2011-07-07 11:44:48', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(70, 1, 'V-CUBE ミーティング on cybozu.com', 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 50000, 0, 10000, '2011-11-24 15:45:09', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(71, 1, 'Paperless_Free', 5, 0, 5, 0, 0, 0, 0, 0, 30, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(72, 1, 'Meeting_Free', 3, 0, 0, 0, 0, 0, 0, 0, 30, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2012-04-16 21:06:21', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(73, 1, 'HD_3-5-10', 10, 0, 10, 0, 0, 0, 0, 0, 0, 10240, 1024, 150, '640x480', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 100000, 100000, 0, '2012-05-08 11:11:24', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(74, 1, '新ハイクオリティ', 10, 0, 10, 0, 0, 0, 0, 0, 0, 6144, 512, 100, '640x480', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 100000, 100000, 0, '2012-05-08 11:51:30', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(75, 1, '新スタンダード', 10, 0, 10, 0, 0, 0, 0, 0, 0, 3072, 384, 80, '320x240', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 100000, 100000, 0, '0000-00-00 00:00:00', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(76, 1, 'V-CUBE MTG ON cybozu.comトライアル', 5, 0, 0, 0, 0, 0, 0, 0, 45, 2048, 384, 30, '320x240', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2012-05-22 19:53:26', 1, 35, 0, 1000, NULL);
INSERT INTO `service` VALUES(77, 1, 'Panasonic Malaysia', 3, 0, 0, 0, 0, 0, 0, 0, 60, 2048, 384, 30, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 1, 90, 0, 1000, NULL);
INSERT INTO `service` VALUES(78, 1, 'PGi_Paperless_Free', 5, 0, 5, 0, 0, 0, 0, 0, 45, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(79, 1, 'VCUBE_Doctor_Standard', 5, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2012-08-20 16:07:02', 1, 0, 500, 1000, NULL);
INSERT INTO `service` VALUES(80, 1, '新エントリー', 10, 0, 10, 0, 0, 0, 0, 0, 0, 2048, 384, 80, '320x240', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 34900, 25, 1200, '2012-08-30 11:51:30', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(81, 1, '新Premium20', 9, 11, 10, 0, 0, 0, 0, 0, 0, 4096, 384, 80, '320x240', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 119900, 0, 60000, '2012-08-30 11:51:30', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(82, 1, '新Premium30', 9, 21, 10, 0, 0, 0, 0, 0, 0, 6144, 384, 80, '320x240', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 149900, 0, 60000, '2012-08-30 11:51:30', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(83, 1, '新Premium40', 9, 31, 10, 0, 0, 0, 0, 0, 0, 8192, 384, 80, '320x240', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 179900, 0, 60000, '2012-08-30 11:51:30', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(84, 1, '新Premium50', 9, 41, 10, 0, 0, 0, 0, 0, 0, 10240, 384, 80, '320x240', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 209900, 0, 60000, '2012-08-30 11:51:30', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(85, 1, '新同時2拠点（大塚商会）', 2, 0, 0, 0, 0, 0, 0, 0, 0, 2048, 384, 80, '320x240', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 20000, 20000, 0, 60000, '2012-08-30 11:51:30', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(86, 1, '新スマート（大塚商会）', 3, 0, 0, 0, 0, 0, 0, 0, 0, 2048, 384, 80, '320x240', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 30000, 30000, 0, 60000, '2012-08-30 11:51:30', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(87, 1, '新スマートプラス（大塚商会）', 5, 0, 0, 0, 0, 0, 0, 0, 0, 2048, 384, 80, '320x240', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 50000, 50000, 0, 60000, '2012-08-30 11:51:30', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(88, 1, '新Premium20プラス', 20, 0, 10, 0, 0, 0, 0, 0, 0, 4096, 384, 80, '320x240', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 129900, 0, 10000, '2012-08-30 11:51:30', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(89, 1, 'セールスプラン', 2, 0, 0, 0, 0, 0, 0, 0, 0, 2048, 384, 80, '320x240', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, NULL, NULL, NULL, 0, 45000, 34900, 25, 1200, '2012-10-25 21:21:38', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(91, 1, 'Trial_withH264', 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 9900, 190, 0, '2013-03-18 11:13:40', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(92, 1, 'cybozu_new_trial', 3, 0, 0, 0, 0, 0, 0, 0, 30, 2048, 384, 80, '320x240', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2013-03-19 13:12:39', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(93, 1, 'Cloud_Mcu', 10, 0, 10, 0, 0, 0, 0, 0, 0, 3072, 384, 80, '320x240', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 30000, 0, 0, 0, '2013-04-26 17:55:00', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(94, 1, 'セールス資料共有', 2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, '320x240', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2013-05-15 10:25:36', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(95, 1, 'Docomo_Free', 3, 0, 0, 0, 0, 0, 0, 0, 60, 2048, 384, 80, '320x240', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2013-05-15 10:25:36', 1, 0, 0, 0, NULL);
INSERT INTO `service` VALUES(96, 1, 'ブルーIDプラン', 9, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 0, 69900, 3000, 0, 0, '2013-07-12 12:42:32', 1, 0, 200, 1000, NULL);
INSERT INTO `service` VALUES(97, 1, 'グリーンIDホストプラン', 4, 21, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 10000, 5000, 0, 0, '2013-07-12 12:42:32', 1, 0, 200, 200, NULL);
INSERT INTO `service` VALUES(98, 1, 'グリーンIDホストプランライト', 4, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 5000, 2500, 0, 0, '2013-07-12 12:42:32', 1, 0, 200, 200, NULL);
INSERT INTO `service` VALUES(99, 1, 'グレーID従量プラン', 4, 21, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 0, 10000, 5000, 10, 60, '2013-07-12 12:42:32', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(100, 1, 'Docomo_Document_Free', 5, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, '320x240', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2013-07-12 12:42:32', 1, 0, 0, 0, NULL);
INSERT INTO `service` VALUES(101, 1, 'SFDCセールスプラン', 2, 0, 0, 0, 0, 0, 0, 0, 0, 2048, 384, 80, '320x240', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, NULL, NULL, NULL, 1, 45000, 34900, 25, 1200, '2013-07-23 11:01:01', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(102, 1, 'SFDCセールス資料共有', 2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, '320x240', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, NULL, NULL, NULL, 1, 0, 0, 0, 0, '2013-07-23 11:01:01', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(103, 1, 'SFDCセールス画面共有', 2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, '320x240', 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, NULL, NULL, NULL, 1, 0, 0, 0, 0, '2013-07-23 11:01:01', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(104, 1, 'イプロス2拠点プラン', 2, 0, 0, 0, 0, 0, 0, 0, 0, 3072, 384, 80, '', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 30000, 100000, 0, '0000-00-00 00:00:00', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(105, 1, 'イプロス3拠点プラン', 3, 0, 0, 0, 0, 0, 0, 0, 0, 3072, 384, 80, '', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 45000, 50000, 100000, 0, '0000-00-00 00:00:00', 1, 0, 0, 1000, NULL);
INSERT INTO `service` VALUES(106, 1, 'イプロス2拠点トライアルプラン', 2, 0, 0, 0, 0, 0, 0, 0, 30, 3072, 384, 80, '', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 100000, 0, '0000-00-00 00:00:00', 1, 14, 0, 1000, NULL);
INSERT INTO `service` VALUES(107, 1, 'イプロス3拠点トライアルプラン', 3, 0, 0, 0, 0, 0, 0, 0, 30, 3072, 384, 80, '', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 100000, 0, '0000-00-00 00:00:00', 1, 14, 0, 1000, NULL);
INSERT INTO `service` VALUES(108, 1, 'BIZ＋', 5, 0, 0, 0, 0, 0, 0, 0, 0, 3072, 384, 80, '320x240', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 1, 0, 50, 1000, 'ja,en');
INSERT INTO `service` VALUES(109, 1, 'プロスパート(プロスパID)', 10, 0, 10, 0, 0, 0, 0, 0, 0, 3072, 384, 80, '320x240', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 1, 0, 200, 1000, 'ja,en');
INSERT INTO `service` VALUES(110, 1, 'プロスパート(プロスパルーム2)', 5, 0, 5, 5, 1, 0, 0, 0, 0, 3072, 384, 80, '320x240', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 1, 0, 0, 500, NULL);


-- --------------------------------------------------------

--
-- テーブルの構造 `service_option`
--

CREATE TABLE IF NOT EXISTS `service_option` (
  `service_option_key` int(11) NOT NULL AUTO_INCREMENT,
  `country_key` int(11) NOT NULL DEFAULT '0',
  `service_option_name` text NOT NULL,
  `service_option_price` int(11) NOT NULL DEFAULT '0',
  `service_option_init_price` int(11) NOT NULL DEFAULT '0',
  `service_option_running_price` int(11) NOT NULL DEFAULT '0',
  `option_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'オプションステータス',
  `use_user_option` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'ユーザーオプションフラグ',
  PRIMARY KEY (`service_option_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

INSERT INTO `service_option` VALUES(1, 1, 'messenger', 100, 0, 0, 1, 0);
INSERT INTO `service_option` VALUES(2, 1, 'meeting_ssl', 8000, 0, 0, 1, 0);
INSERT INTO `service_option` VALUES(3, 1, 'desktop_share', 10000, 0, 0, 1, 0);
INSERT INTO `service_option` VALUES(4, 1, 'hdd_extention', 10000, 0, 0, 1, 0);
INSERT INTO `service_option` VALUES(5, 1, 'high_quality', 10000, 0, 30, 1, 0);
INSERT INTO `service_option` VALUES(6, 1, 'mobile_phone', 5000, 0, 50, 1, 0);
INSERT INTO `service_option` VALUES(7, 1, 'audience', 10000, 10000, 10, 1, 0);
INSERT INTO `service_option` VALUES(8, 1, 'h323_client', 5000, 0, 10, 1, 0);
INSERT INTO `service_option` VALUES(9, 1, 'on_demand', 30000, 0, 0, 1, 0);
INSERT INTO `service_option` VALUES(10, 1, 'on_demand_hdd_extention', 10000, 0, 0, 1, 0);
INSERT INTO `service_option` VALUES(11, 1, 'on_demand_user_extention', 30000, 0, 0, 1, 0);
INSERT INTO `service_option` VALUES(12, 1, 'seminar_user_extention', 10000, 0, 0, 1, 0);
INSERT INTO `service_option` VALUES(13, 1, 'compact', 10000, 0, 0, 1, 0);
INSERT INTO `service_option` VALUES(14, 1, '導入サポート', 0, 0, 0, 1, 0);
INSERT INTO `service_option` VALUES(15, 1, 'intra_fms', 0, 0, 0, 1, 0);
INSERT INTO `service_option` VALUES(16, 1, 'whiteboard', 0, 0, 0, 1, 0);
INSERT INTO `service_option` VALUES(18, 1, 'multicamera', 10000, 0, 0, 1, 0);
INSERT INTO `service_option` VALUES(19, 1, 'telephone', 10000, 0, 0, 1, 0);
INSERT INTO `service_option` VALUES(20, 1, 'record_gw', 0, 0, 0, 1, 0);
INSERT INTO `service_option` VALUES(21, 1, 'smartphone', 5000, 0, 0, 1, 0);
INSERT INTO `service_option` VALUES(22, 0, 'whiteboard_video', 5000, 0, 0, 1, 0);
INSERT INTO `service_option` VALUES(23, 0, 'teleconference', 0, 0, 0, 1, 0);
INSERT INTO `service_option` VALUES(24, 1, 'video_conference', 0, 0, 0, 1, 0);
INSERT INTO `service_option` VALUES(25, 1, 'minimumBandwidth80', 0, 0, 0, 1, 0);
INSERT INTO `service_option` VALUES(26, 1, 'h264', 0, 0, 0, 1, 0);
INSERT INTO `service_option` VALUES(27, 1, 'meeting_tls', 0, 0, 0, 1, 0);
INSERT INTO `service_option` VALUES(30, 1, 'GlobalLink', 0, 0, 0, 1, 0);
INSERT INTO `service_option` VALUES(31, 1, 'user_max_storage_size_extention_1G', 10000, 0, 0, 1, 1);


-- --------------------------------------------------------

--
-- テーブルの構造 `sharing_server`
--

CREATE TABLE IF NOT EXISTS `sharing_server` (
  `server_key` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datacenter_key` int(10) unsigned NOT NULL DEFAULT '0',
  `server_address` varchar(64) NOT NULL DEFAULT '',
  `server_country` varchar(32) DEFAULT 'jp',
  `server_count` int(10) unsigned DEFAULT '0',
  `server_priority` int(10) unsigned NOT NULL DEFAULT '0',
  `name_ja` text,
  `name_en` text,
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`server_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `staff_key` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '担当キー',
  `login_id` varchar(64) DEFAULT NULL COMMENT 'ログインID',
  `login_password` varchar(64) DEFAULT NULL COMMENT 'ログインパスワード',
  `staff_id` varchar(4) DEFAULT NULL COMMENT '社員管理番号',
  `name` text COMMENT '担当者名',
  `section_id` varchar(256) DEFAULT NULL COMMENT '事業部ID',
  `status` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'ステータス',
  `authority` int(11) NOT NULL DEFAULT '0' COMMENT '権限',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '作成日',
  `update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新日',
  PRIMARY KEY (`staff_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='担当者マスタ' AUTO_INCREMENT=2 ;

INSERT INTO `staff` VALUES (1, 'n2my_admin', 'df6f4c0750202b4485751787b3a6de1baa2b0aad', '1', 'システム管理者', '1', 1, 2, '2008-12-06 22:01:28', '2011-08-11 14:42:27');


-- --------------------------------------------------------

--
-- テーブルの構造 `trial`
--

CREATE TABLE IF NOT EXISTS `trial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(54) DEFAULT NULL,
  `session` varchar(200) NOT NULL,
  `q1` text,
  `q2` varchar(100) DEFAULT NULL,
  `q3` varchar(100) DEFAULT NULL,
  `create_datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_key` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` text NOT NULL,
  `server_key` int(10) NOT NULL,
  `user_registtime` datetime NOT NULL,
  `user_updatetime` datetime DEFAULT NULL,
  PRIMARY KEY (`user_key`),
  KEY `user_key` (`user_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `user_agent`
--

CREATE TABLE IF NOT EXISTS `user_agent` (
  `user_agent_key` int(11) NOT NULL AUTO_INCREMENT,
  `db_key` varchar(100) NOT NULL COMMENT 'DB名',
  `user_id` varchar(64) DEFAULT NULL COMMENT 'ユーザーID',
  `invoice_flg` int(11) DEFAULT NULL COMMENT '請求フラグ',
  `meeting_key` int(11) NOT NULL COMMENT '会議キー',
  `participant_key` int(11) DEFAULT NULL COMMENT '参加者ID',
  `appli_type` varchar(64) DEFAULT NULL COMMENT 'タイプ(normal, audience...)',
  `appli_mode` varchar(64) DEFAULT NULL COMMENT 'モード(general, h323, mobile...)',
  `appli_role` varchar(64) DEFAULT NULL COMMENT 'ロール(default, invite...)',
  `appli_version` varchar(50) DEFAULT NULL COMMENT 'as2/as3',
  `user_agent` text COMMENT '生UserAgent',
  `os` varchar(100) DEFAULT NULL COMMENT 'OS',
  `browser` varchar(100) DEFAULT NULL COMMENT 'ブラウザ',
  `flash` varchar(50) DEFAULT NULL COMMENT 'Flashバージョン',
  `protcol` varchar(20) DEFAULT NULL COMMENT 'FMS接続プロトコル',
  `port` varchar(20) DEFAULT NULL COMMENT 'FMS接続ポート',
  `ip` varchar(100) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `monitor_size` varchar(100) DEFAULT NULL COMMENT 'モニターサイズ',
  `monitor_color` varchar(20) DEFAULT NULL COMMENT 'ビット',
  `camera` text COMMENT 'カメラデバイス名',
  `mic` text COMMENT 'マイクデバイス名',
  `speed` varchar(20) DEFAULT NULL COMMENT '回線速度(bps)',
  `create_datetime` datetime DEFAULT NULL COMMENT '作成日',
  `update_datetime` datetime DEFAULT NULL COMMENT '更新日',
  PRIMARY KEY (`user_agent_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
