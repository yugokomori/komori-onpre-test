<?php
require_once("classes/mcu/model/Proxy.php");
require_once(dirname(__FILE__)."/../model/vo/ConferenceCreatedRequestObject.php");

class ConferenceCreatedRequestProxy extends Proxy {
	private $dataObject;
	
	function __construct($request) {
		parent::__construct();
		
		$this->dataObject = new ConferenceCreatedRequestObject($request);
	}
	
	public function __get($key) {
		if (property_exists($this, $key)) {
			return $this->$key;
		}
		else {
			return $this->dataObject->$key;
		}
	}
}
