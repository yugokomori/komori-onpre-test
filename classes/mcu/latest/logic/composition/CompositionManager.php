<?php
require_once(dirname(__FILE__)."/../../logic/MCUConferenceController.php");
require_once(dirname(__FILE__)."/../../params/SetCompositionTypeParams.php");

class CompositionManager extends MCUConferenceController {
	protected $totalCount;
	protected $countHardwareParticipant;
	protected $confId;
	protected $activeSpeaker;
	protected $mobile4x4;
	protected $bfcpStatus;
	protected $stbStatus;
	protected $hdFlag;
	protected $reservationCompositionType;

	public function __construct($confId, $totalCount, $countHardwareParticipant, $activeSpeaker=true, $mobile4x4=false, $bfcpStatus=false, $stbStatus=false, $hdFlag=false) {
		parent::__construct();

		$this->count = $totalCount;
		$this->countHardwareParticipant	= $countHardwareParticipant;
		$this->confId = $confId;
		$this->activeSpeaker = $activeSpeaker;
		$this->mobile4x4 = $mobile4x4;
		$this->bfcpStatus = $bfcpStatus;
		$this->stbStatus = $stbStatus;
		$this->hdFlag = $hdFlag;
		$this->setReservationCompositionType();
	}

	public function run() {
		// polycom側　composition
		$this->setCompositionTypeForConferenceParticipant();

		// mobile側　composition
		if ($this->bfcpStatus) {
        	$this->setBfcpCompositionTypeForMobileParticipant();
		} else if ($this->stbStatus) {
			$this->setCompositionTypeForStbMobileParticipant();
		} else if ($this->hdFlag) {
			$this->setCompositionTypeForHd720pMobileParticipant();
		} else {
			$this->setCompositionTypeForMobileParticipant();
		}

		// mobile側　composition
		if($this->stbStatus) {
			$this->setCompositionTypeForStbFlashParticipant();
		}
		else if($this->hdFlag) {
			$this->setCompositionTypeForHd720pFlashParticipant();
		} else {
			$this->setCompositionTypeForFlashParticipant();
		}

	}

	public function runHardwareComposition() {
		$this->setCompositionTypeForConferenceParticipant();
	}

	protected function setCompositionTypeForConferenceParticipant() {
		if ($this->stbStatus) {
			$size = $this->configProxy->get("hd_size");
			$profile = $this->configProxy->get("stbHDProfileId");
		} else if ($this->hdFlag) { //HD720p  オプション
			$size = $this->configProxy->get("hd_size");
			$profile = $this->configProxy->get("polycomHD720ProfileId");
		}
		else {
			$size = $this->configProxy->get("size");
			$profile = $this->configProxy->get("polycomProfileId");
		}

		//$compositionType = $this->getVideoParticipantCompositionType();
		$compositionType = $this->reservationCompositionType;
		$obj = new SetCompositionTypeParams(array(
				"confId"	=> $this->confId,
				"mosaicId"	=> VIDEO_FOR_CONFERENCE,
				"compType"	=> $compositionType,
				"size"		=> $size,
				"profileId"	=> $profile));
		$this->mcuControllerProxy->setCompositionType($obj->getParams());
	}

	protected function setCompositionTypeForStbMobileParticipant() {
		$compositionType = $this->getMobileParticipantCompositionType();
		$obj = new SetCompositionTypeParams(array(
				"confId"	=> $this->confId,
				"mosaicId"	=> VIDEO_FOR_MOBILE,
				"compType"	=> $compositionType,
				"size"		=> $this->configProxy->get("hd_size"),
				"profileId"	=> $this->configProxy->get("stbHDProfileId")));
		$this->mcuControllerProxy->setCompositionType($obj->getParams());
	}

	protected function setCompositionTypeForHd720pMobileParticipant() {
		$compositionType = $this->getMobileParticipantCompositionType();
		$obj = new SetCompositionTypeParams(array(
				"confId"	=> $this->confId,
				"mosaicId"	=> VIDEO_FOR_MOBILE,
				"compType"	=> $compositionType,
				"size"		=> $this->configProxy->get("hd_size"),
				"profileId"	=> $this->configProxy->get("polycomHD720ProfileId")));
		$this->mcuControllerProxy->setCompositionType($obj->getParams());
	}

	protected function setCompositionTypeForMobileParticipant() {
		$compositionType = $this->getMobileParticipantCompositionType();
		$obj = new SetCompositionTypeParams(array(
				"confId"	=> $this->confId,
				"mosaicId"	=> VIDEO_FOR_MOBILE,
				"compType"	=> $compositionType,
				"size"		=> $this->configProxy->get("size"),
				"profileId"	=> $this->configProxy->get("mobileProfileId")));
		$this->mcuControllerProxy->setCompositionType($obj->getParams());
	}

	protected function setBfcpCompositionTypeForMobileParticipant() {
		$compositionType = $this->getMobileParticipantCompositionType();
		$obj = new SetCompositionTypeParams(array(
				"confId"	=> $this->confId,
				"mosaicId"	=> VIDEO_FOR_MOBILE,
				"compType"	=> ONE_X_ONE,
				"size"		=> $this->configProxy->get("size"),
				"profileId"	=> $this->configProxy->get("mobileProfileId")));
		$this->mcuControllerProxy->setCompositionType($obj->getParams());
	}
	protected function setCompositionTypeForFlashParticipant() {
		$compositionType = $this->getFlashParticipantCompositionType();
		$obj = new SetCompositionTypeParams(array(
				"confId"	=> $this->confId,
				"mosaicId"	=> VIDEO_FOR_FLASH,
				"compType"	=> $compositionType,
				"size"		=> $this->configProxy->get("size"),
				"profileId"	=> $this->configProxy->get("flashProfileId")));
		$this->mcuControllerProxy->setCompositionType($obj->getParams());
	}
	protected function setCompositionTypeForStbFlashParticipant() {
		$compositionType = $this->getFlashParticipantCompositionType();
		$obj = new SetCompositionTypeParams(array(
				"confId"	=> $this->confId,
				"mosaicId"	=> VIDEO_FOR_FLASH,
				"compType"	=> $compositionType,
				"size"		=> $this->configProxy->get("hd_size"),
				"profileId"	=> $this->configProxy->get("stbflashProfileId")));
		$this->mcuControllerProxy->setCompositionType($obj->getParams());
	}

	protected function setCompositionTypeForHd720pFlashParticipant() {
		$compositionType = $this->getFlashParticipantCompositionType();
		$obj = new SetCompositionTypeParams(array(
				"confId"	=> $this->confId,
				"mosaicId"	=> VIDEO_FOR_FLASH,
				"compType"	=> $compositionType,
				"size"		=> $this->configProxy->get("hd_size"),
				"profileId"	=> $this->configProxy->get("polycomHD720ProfileId")));
		$this->mcuControllerProxy->setCompositionType($obj->getParams());
	}

	private function getFlashParticipantCompositionType() {
		if ($this->activeSpeaker) {
			$compositionType = ONE_X_ONE;
		}
		else {
			if (1 == $this->countHardwareParticipant) {
				$compositionType = ONE_X_ONE;
			}
			else if (1 < $this->countHardwareParticipant && $this->countHardwareParticipant <= 4) {
				$compositionType = TWO_X_TWO;
			}
			else if (4 < $this->countHardwareParticipant) {
				$compositionType = THREE_X_THREE;
			}
			else {
				// 				return false;
// 				throw new Exception("check participant number.");
				$compositionType = ONE_X_ONE;
			}
		}
		return $compositionType;
	}

	protected function getMobileParticipantCompositionType() {
	    // 会議参加者を確認
	    $this->conferenceProxy->setActiveConferenceByConfIdThrowable($this->confId);
	    // participantList.
	    $participantList =  $this->meetingProxy->getParticipantByMeetingKey($this->conferenceProxy->activeConferenceRecord["meeting_key"]);
        // テレビ会議接続があったら1×1をにする
        foreach($participantList as $participant){
            if($participant["participant_protocol"] == "sip"){
                return ONE_X_ONE;
            }
        }

		if ($this->mobile4x4) {
			if (1 == $this->count) {
				$compositionType = ONE_X_ONE;
			}
			else if (1 < $this->count && $this->count <= 4) {
				$compositionType = TWO_X_TWO;
			}
			else if (4 < $this->count && $this->count <= 9) {
				$compositionType = THREE_X_THREE;
			}
			else if ($this->count > 9) {
				$compositionType = FOUR_X_FOUR;
			}
		}
		else if ($this->stbStatus)
		{
			if (1 == $this->count) {
				$compositionType = ONE_X_ONE;
			}
			else if (1 < $this->count && $this->count <= 2) {
				$compositionType = ONE_PLUS_ONE;
			}
			else if (2 < $this->count && $this->count <= 6) {
				$compositionType = ONE_PLUS_FIVE;
			}
			else {
				$compositionType = ONE_PLUS_SEVEN;
			}
		}
		else {
			if (1 == $this->count) {
				$compositionType = ONE_X_ONE;
			}
			else {
				$compositionType = TWO_X_TWO;
			}
		}
		return $compositionType;
	}

	private function setReservationCompositionType(){
	    // 会議情報取得
	    $this->conferenceProxy->setActiveConferenceByConfIdThrowable($this->confId);
	    // meeting record.
	    $this->meetingProxy->setActiveMeetingRecordByMeetingKeyThrowable($this->conferenceProxy->activeConferenceRecord["meeting_key"]);
	    $this->logger->info($this->meetingProxy->activeMeetingRecord);
	    // 予約情報を取得し、レイアウトをセットする
	    $reservationInfo = $this->meetingProxy->getReservationRecordByMeetingKey($this->meetingProxy->activeMeetingRecord["meeting_ticket"]);
	    $this->logger->info($reservationInfo);
	    $videoconferenceLayout = $reservationInfo["videoconference_layout"];
	    switch($videoconferenceLayout){
	        case '1x1':
	            $this->reservationCompositionType = ONE_X_ONE;
	            break;
	        case '2x2':
	            $this->reservationCompositionType = TWO_X_TWO;
	            break;
	        case '3x3':
	            $this->reservationCompositionType = THREE_X_THREE;
	            break;
	        case '1+1':
	            $this->reservationCompositionType = ONE_PLUS_ONE;
	            break;
	        case '1+5':
	            $this->reservationCompositionType = ONE_PLUS_FIVE;
	            break;
	        case '1+7':
	            $this->reservationCompositionType = ONE_PLUS_SEVEN;
	            break;
	    }
	    $this->logger->info($this->reservationCompositionType);
	}

	public function getVideoParticipantCompositionType() {
		if ($this->activeSpeaker) {
			if (1 == $this->count) {
				$compositionType = ONE_X_ONE;
			}
			else if (2 == $this->count) {
				$compositionType = ONE_PLUS_ONE;
			}
			else if (3 <= $this->count && $this->count <= 6) {
				$compositionType = ONE_PLUS_FIVE;
			}
			else {
				$compositionType = ONE_PLUS_SEVEN;
			}
		}
		else {
			if (1 == $this->count) {
				$compositionType = ONE_X_ONE;
			}
			else if (1 < $this->count && $this->count <= 4) {
				$compositionType = TWO_X_TWO;
			}
			else if (4 < $this->count) {
				$compositionType = THREE_X_THREE;
			}
		}
		return $compositionType;
	}
}
