<?php
require_once("classes/N2MY_Api.class.php");
class N2MY_Meeting_Auth_API extends N2MY_Api
{
    var $session_id = null;
    var $api_version = null;
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        // SSL対応
        header('Pragma:');
        //$this->getApiInterface();
    }

    function auth()
    {
        // ログイン以外の場合は認証させる
    }

    function action_pin_login() {
        require_once("classes/mgm/MGM_Auth.class.php");
        require_once("classes/N2MY_Auth.class.php");
        require_once("classes/N2MY_Account.class.php");
        require_once 'classes/dbi/meeting.dbi.php';
        require_once 'classes/dbi/user.dbi.php';

        if (!defined("N2MY_SERVER_TIMEZONE")) {
            date_default_timezone_set($this->config->get('GLOBAL', 'time_zone_name', date_default_timezone_get()));
            $_tz = (int)(substr( date( 'O' ), 0, 3));
            // サーバータイムゾーン設定
            define("N2MY_SERVER_TIMEZONE", $_tz);
        }
        // DB選択
        $request = $this->request->getAll("pin_cd");
        $data  = explode("-", $request["pin_cd"]);
        $this->logger2->info($data);
        $pin_cd  = $data[0];
        $member_key  = $data[1];
        $member_pin_cd  = $request["pin_cd"];
        $request["pin_cd"] = $pin_cd;
        $device  = $request["device"];
        // パラメタチェック
        $rules = array(
            "pin_cd" => array(
                "required" => true,
                "valid_integer" => true,
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        if (!$dsn_info = $obj_MGMClass->getRelationDsn($pin_cd, "telephone")) {
            $err_msg["errors"][] = array(
                "field"   => "pin_cd",
                "err_cd"  => "invalid",
                "err_msg" => "PIN CDが不正です",
                );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        $server_info = $obj_MGMClass->getServerInfo($dsn_info["server_key"]);
        if (!$dsn_info) {
            $err_msg["errors"][] = array(
                "field"   => "pin_cd",
                "err_cd"  => "invalid",
                "err_msg" => "PIN CDが不正です",
                );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        // 会議情報
        $objMeeting = new MeetingTable($server_info["dsn"]);
        $where = "pin_cd = '".$pin_cd."'";
        $meeting_info = $objMeeting->getRow($where);
        if (!$meeting_info) {
            $err_msg["errors"][] = array(
                "field"   => "pin_cd",
                "err_cd"  => "invalid",
                "err_msg" => "PIN CDが不正です",
                );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        if ($meeting_info["is_reserved"]) {

            // 予約情報を取得
            require_once('classes/dbi/reservation.dbi.php');
            $obj_reservation = new ReservationTable($server_info["dsn"] );
            $reservation_info = $obj_reservation->getRow("meeting_key='" .$meeting_info["meeting_ticket"] . "'");
            if(((strtotime($meeting_info['meeting_start_datetime']) > time() || (strtotime($reservation_info['reservation_extend_endtime'])) < time()))){
                $err_msg["errors"][] = array(
                    "field"   => "pin_cd",
                    "err_cd"  => "reservation",
                    "err_msg" => "予約時間内ではありません",
                    );
                return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
            }
        }
        // ユーザー
        $objUser = new UserTable($server_info["dsn"]);
        $where = "user_key = ".$meeting_info["user_key"];
        $user_info = $objUser->getRow($where);
        if (!$user_info) {
            return $this->display_error("101", "User not found", $this->get_error_info($err_obj));
        }

        $lang = $this->get_language();
        $lang_conf_file = N2MY_APP_DIR."config/lang/".$lang."/message.ini";
        $this->_message = parse_ini_file($lang_conf_file, true);
        // パラメタ取得
        $id          = $user_info["user_id"];
        $pw          = $user_info["user_password"];
        $lang        = $this->request->get("lang", "ja");
        $country     = $this->request->get("country", "jp");
        $time_zone   = $this->request->get(API_TIME_ZONE, "9");
        $enc         = $this->request->get("enc", null);
        $output_type = $this->request->get("output_type");
        //国リストにない場合は、日本を指定
        $rules = array(
            "country"     => array(
                "allow" => array_keys($this->get_country_list()),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            $country = "jp";
        }
        $rules = array(
            "lang"     => array(
                "allow" => array_keys($this->get_language_list()),
                ),
            API_TIME_ZONE     => array(
                "allow" => array_keys($this->get_timezone_list()),
                ),
            "enc" => array(
                "allow" => array("md5", "sha1"),
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $request = $this->request->getAll();
        //古いAPI利用者用対応(zhの場合はzh-cnに変更)
        if ($request["lang"] == "zh") {
            $request["lang"] = "zh-cn";
        }
        $err_obj = $this->error_check($request, $rules);
        if (!$user_info = $obj_MGMClass->getUserInfoById( $id ) ){
            $err_obj->set_error("id", "SELECT_USER_ERROR", $id);
        } else {
            if (!$server_info = $obj_MGMClass->getServerInfo( $user_info["server_key"] )){
                $err_obj->set_error("", "SELECT_USER_ERROR", $id);
                return $this->display_error("1", "SELECT_SERVER_ERROR");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            $login = new N2MY_Auth($server_info["dsn"]);
            $user_type = "";
            // ユーザ確認
            require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
            $pw = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $pw);
            $login_info = $login->check($id, $pw, $enc);
            $obj_N2MY_Account = new N2MY_Account($server_info["dsn"]);
            if (!$login_info) {
                return $this->display_error("1", "USER_ID_PASS_INVALID");
            } else if (false === $obj_N2MY_Account->checkRemoteAddress($login_info["user_info"]["user_key"], $_SERVER["REMOTE_ADDR"])){
                    $session = EZSession::getInstance();
                    $session->removeAll();
                    $err_obj->set_error("ip_white_list", "200105", $id);
                    return $this->display_error("1", "Login failed / Invalid auth token", $this->get_error_info($err_obj));
            } else {
                // セッションスタート
                $session = EZSession::getInstance();
                // ID を取得
                $session_id = session_id();
                // ユーザ情報配置
                if  ($output_type) {
                    $session->set("output_type", $output_type);
                }
                $session->set("login", "1");
                //
                /** ミーティングデータを取得するまで予約と、現状会議のステータス確認を区別するため、invitedGuestを利用
                    会議開始処理でparticipantに登録する際inviteに戻す**/
                $session->set("login_type", 'invitedGuest');
                $session->set("lang", $lang);
                $session->set("country_key", $country);
                $session->set("time_zone", $time_zone);
                $session->set("dsn_key", $server_info["host_name"]);
                $session->set("server_info", $server_info);
                $session->set("login_device", $device);
                // ユーザー情報
                $user_info = $login_info["user_info"];
                $session->set("user_info", $login_info["user_info"]);
                //メンバー情報取得
                if ($member_pin_cd) {
                    require_once("classes/dbi/member.dbi.php");
                    $obj_Member = new MemberTable( $server_info["dsn"] );
                    $where = "member_pin_cd = '".mysql_real_escape_string($member_pin_cd)."'".
                             " AND user_key = ".$user_info["user_key"];
                    $member_info = $obj_Member->getRow($where);
                    $session->set("member_info", $member_info);
                }
                $type = "user";
                // 使用可能な部屋
                $rooms = $obj_N2MY_Account->getOwnRoomByRoomKey( $meeting_info["room_key"], $user_info["user_key"]);
                // 既存のセッションデータにも書き込む
                $session->set('room_info', $rooms);
                $session->set('invited_meeting_ticket', $meeting_info['meeting_ticket']);
                // 戻り値
                $data = array(
                    "session"      => $session_id,
                    API_ROOM_ID    => $meeting_info["room_key"],
                    API_MEETING_ID => $meeting_info["meeting_session_id"],
                    "member_info" => array(
                        'member_id'         => $member_info['member_id'],
                        'member_email'      => $member_info['member_email'],
                        'member_name'       => $member_info['member_name'],
                        'member_name_kana'  => $member_info['member_name_kana'],
                        'timezone'          => $member_info['timezone'],
                        'lang'              => $member_info['lang'],
                        ),
                    );
                $this->session = $session;
                $this->add_operation_log("login");
                return $this->output($data);
            }
        }
    }

    function action_outbound_login()
    {
        require_once("classes/mgm/MGM_Auth.class.php");
        $request = $this->request->getAll();
        $rules = array(
            "outbound_id" => array(
                "required" => true,
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $outbound_id = $request["outbound_id"];
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        if ( ! $server_info = $obj_MGMClass->getRelationDsn( $outbound_id, "outbound_id" ) ){
            return false;
        }

        $obj_Room = new RoomTable( $this->get_dsn() );
        $where = "outbound_id = '".$outbound_id."'".
                 " AND room_status != -1";
        $room_info = $obj_Room->getRow($where);
        //memberとのリレーションチェック
        require_once("classes/dbi/member_room_relation.dbi.php");
        $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
        $where = "room_key = '".addslashes($room_info["room_key"])."'";
        $room_relation = $objRoomRelation->getRow($where);
        if ($room_info && $room_relation) {
            $obj_Member = new MemberTable( $this->get_dsn() );
            $where = "outbound_id = '".addslashes($outbound_id)."'".
                     " AND member_key = ".$room_relation["member_key"];
            $member_info = $obj_Member->getRow($where);
            $this->logger->debug("room_info",__FILE__,__LINE__,$room_info);

            if($member_info){
                $addition = unserialize($room_info["addition"]);
                $room_info["email"] = $addition["email"];
                $room_info["title"] = $addition["title"];
                $room_info["room_image"] = $addition["room_image"];
                $room_info["outbound"] = $member_info["outbound_id"];
                return $room_info;
            }else{
                $err_msg["errors"][] = array(
                    "field"   => "inbound_id",
                    "err_cd"  => "invalid",
                    "err_msg" => "NOT Inbound ID",
                );
                return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
            }
        }else{
            $err_msg["errors"][] = array(
                "field"   => "outbound_id",
                "err_cd"  => "invalid",
                "err_msg" => "NOT Outbound ID",
                );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }

        // セッションスタート
        $session = EZSession::getInstance();
        // ID を取得
        $session_id = session_id();
        $session->set("login", "1");
        // 戻り値
        $data = array(
            "session" => $session_id,
            API_MEETING_ID  => $room_list["meeting_session_id"],
            //"invite_url" => $room_status[0]["invitation_url"],
            );
        return $this->output($data);
    }

    function action_inbound_login()
    {
        require_once("classes/mgm/MGM_Auth.class.php");
        $request = $this->request->getAll();
        $rules = array(
            "inbound_id" => array(
                "required" => true,
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $inbound_id = $request["inbound_id"];
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        if ( ! $server_info = $obj_MGMClass->getRelationDsn( $inbound_id, "inbound_id" ) ){
            return false;
        }
        require_once("classes/dbi/inbound.dbi.php");
        $obj_Inbound = new InboundTable( $server_info["dsn"] );
        $where = "inbound_id = '".addslashes($inbound_id)."'";
        $inbound_info = $obj_Inbound->getRow($where);
        if (!$inbound_info) {
            $err_msg["errors"][] = array(
                "field"   => "inbound_id",
                "err_cd"  => "invalid",
                "err_msg" => "NOT Inbound ID",
                );
            return $this->display_error("100", "PARAMETER_ERROR", $err_msg);
        }
        $this->logger->debug("inbound_info",__FILE__,__LINE__,$inbound_info);

        // ユーザー
        require_once("classes/dbi/user.dbi.php");
        $objUser = new UserTable($server_info["dsn"]);
        $where = "user_key = ".$inbound_info["user_key"];
        $user_info = $objUser->getRow($where);

        require_once("classes/N2MY_Inbound.class.php");
        $obj_Inbound = new N2MY_Inbound($server_info["dsn"]);
        $room_status = $obj_Inbound->getInboundStatus($user_info["user_id"], $inbound_info["inbound_id"]);
        $this->logger2->info($room_status);
        $room_list = array();
        if ($room_status) {
            foreach($room_status as $room) {
                if ("1" == $room["status"] && "1" == $room["pcount"] && "1" == $room["staff"] && "0" == $room["customer"]) {
                    $this->logger->trace("room_list",__FILE__,__LINE__,$room);
                    $room_list["room_key"] = $room["room_key"];
                    $room_list["meeting_session_id"] = $room["meeting_session_id"];
                    break;
                }
            }
        }
        if (!$room_list) {
            $err_obj->set_error($inbound_id, "MEETING_INVALID");
            return $this->display_error("101", "Not Meeting Data", $this->get_error_info($err_obj));
        }
        // セッションスタート
        $session = EZSession::getInstance();
        // ID を取得
        $session_id = session_id();
        $session->set("login", "1");
        // 戻り値
        $data = array(
            "session" => $session_id,
            API_MEETING_ID  => $room_list["meeting_session_id"],
            //"invite_url" => $room_status[0]["invitation_url"],
            );
        return $this->output($data);
    }

    /**
     * ログイン処理
     *
     * @param string id ユーザーID
     * @param string pw ユーザーパスワード
     * @return string $output テンプレートを表示
     */
    function action_login()
    {
        require_once("classes/mgm/MGM_Auth.class.php");
        require_once("classes/N2MY_Auth.class.php");
        require_once("classes/N2MY_Account.class.php");
        $lang = $this->get_language();
        $lang_conf_file = N2MY_APP_DIR."config/lang/".$lang."/message.ini";
        $this->_message = parse_ini_file($lang_conf_file, true);
        // パラメタ取得
        $id          = $this->request->get("id");
        $pw          = $this->request->get("pw");
        $lang        = $this->request->get("lang", "ja");
        $country     = $this->request->get("country", "jp");
        $time_zone   = $this->request->get(API_TIME_ZONE, "9");
        $enc         = $this->request->get("enc", null);
        $output_type = $this->request->get("output_type");
        $login_type  = $this->request->get("login_type", null);

        $this->logger->info(sprintf("api v1 action_login id:%s enc:%s login_type:%s",
                                    $id, $enc, $login_type));
        // DB選択
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        $rules = array(
            "id"     => array(
                "required" => true
                ),
            "pw"     => array(
                "required" => true
                ),
            "lang"     => array(
                "allow" => array_keys($this->get_language_list()),
                ),
            "country"     => array(
                "allow" => array_keys($this->get_country_list()),
                ),
            API_TIME_ZONE     => array(
                "allow" => array_keys($this->get_timezone_list()),
                ),
            "enc" => array(
                "allow" => array("md5", "sha1"),
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $request = $this->request->getAll();
        //古いAPI利用者用対応(zhの場合はzh-cnに変更)
        if ($request["lang"] == "zh") {
            $request["lang"] = "zh-cn";
        }
        $err_obj = $this->error_check($request, $rules);
        //モバイル判別
        if (strpos($_SERVER['HTTP_USER_AGENT'], "Android") || strpos($_SERVER['HTTP_USER_AGENT'],"iOS")) {
            $err_obj->set_error("id", "SELECT_USER_ERROR", $id);
        }
        if (!$user_info = $obj_MGMClass->getUserInfoById( $id ) ){
            $err_obj->set_error("id", "SELECT_USER_ERROR", $id);
        } else {
            if (!$server_info = $obj_MGMClass->getServerInfo( $user_info["server_key"] )){
                $err_obj->set_error("", "SELECT_USER_ERROR", $id);
                return $this->display_error("1", "SELECT_SERVER_ERROR");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            $login = new N2MY_Auth($server_info["dsn"]);
            $user_type = "";
            // ユーザ確認
            $login_info = $login->check($id, $pw, $enc);
            $obj_N2MY_Account = new N2MY_Account($server_info["dsn"]);
            if (!$login_info) {
                return $this->display_error("1", "USER_ID_PASS_INVALID");
            } else if (false === $obj_N2MY_Account->checkRemoteAddress($login_info["user_info"]["user_key"], $_SERVER["REMOTE_ADDR"])){
                    $session = EZSession::getInstance();
                    $session->removeAll();
                    $err_obj->set_error("ip_white_list", "200105", $id);
                    return $this->display_error("1", "Login failed / Invalid auth token", $this->get_error_info($err_obj));
            } else if ($login_info["member_info"]["member_type"] == "terminal" || $login_info["member_info"]["member_type"] == "centre") {
                    return $this->display_error("1", "Login failed / Invalid member type");
            } else {
                // セッションスタート
                $session = EZSession::getInstance();
                // ID を取得
                $session_id = session_id();
                // ユーザ情報配置
                if  ($output_type) {
                    $session->set("output_type", $output_type);
                }
//$this->logger2->info(array($login_info,$country));
                if ($country == "auto") {
                  $result_country_key = $this->_get_country_key($country);
                  $country = $result_country_key;
                }
//$this->logger2->info(array($login_info,$country));
                $this->logger2->debug($login_info);
                $session->set("login", "1");
                $session->set("lang", $lang);
                $session->set("country_key", $country);
                $session->set("time_zone", $time_zone);
                $session->set("dsn_key", $server_info["host_name"]);
                $session->set("server_info", $server_info);
                $session->set("login_device", $this->request->get("device"));
                // ユーザー情報
                $user_info = $login_info["user_info"];
                $session->set("user_info", $login_info["user_info"]);
                //メンバー課金の場合は会議内招待ができないようにguest扱いにする
                if ($user_info["account_model"] == "member") {
                    $session->set("api_login_type", "member");
                }
                // メンバー情報
                if (isset($login_info["member_info"])) {
                    $type = "member";
                    $session->set("member_info", $login_info["member_info"]);
                    $member_info = $login_info["member_info"];
                } else {
                    $type = "user";
                }
                if ($login_type == 'cybozu' && !$user_info['is_cybozu_option']){
                    return $this->display_error("1", "USER_DISABLED_API");
                }
                // 部屋情報
                $obj_N2MY_Account = new N2MY_Account($server_info["dsn"]);
                if ($member_info["use_sales"] || (!$member_info && $user_info["use_sales"] && $request["service_type"] == "sales")) {
                    $service_mode = "sales";
                } else {
                  $service_mode = "meeting";
                }
                // 部屋情報
                $obj_N2MY_Account = new N2MY_Account($server_info["dsn"]);
                if ($login_info["member_info"]["use_sales"]) {
                    $rooms = $obj_N2MY_Account->getFullRoomList( $user_info["user_key"], $member_info["member_key"], $user_info["account_model"] );
                } else if ( $login_info["member_info"] && ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre" || $user_info["account_model"] == "free")) {
                    $rooms = $obj_N2MY_Account->getOwnRoom( $member_info["member_key"], $user_info["user_key"]);
                } else if ($user_info["use_sales"]){
                  $rooms = $obj_N2MY_Account->getRoomList( $user_info["user_key"], $service_mode);
                } else {
                    $rooms = $obj_N2MY_Account->getRoomList( $user_info["user_key"] );
                }
                // 既存のセッションデータにも書き込む
                $session->set('room_info', $rooms);
                $session->set('service_mode', $service_mode);
                // ユーザー情報整形
                // 戻り値
                $data = array(
                    "type" => $type,
                    "session" => $session_id,
                    "user_info" => array(
                        'user_id'           => $user_info['user_id'],
                        'user_name'         => $user_info['user_company_name']
                        ),
                    "member_info" => array(
                        'member_id'         => $member_info['member_id'],
                        'member_email'      => $member_info['member_email'],
                        'member_name'       => $member_info['member_name'],
                        'member_name_kana'  => $member_info['member_name_kana'],
                        'timezone'          => $member_info['timezone'],
                        'lang'              => $member_info['lang'],
                        'is_admin'          => $member_info['is_admin'],
                        ),
                    );
                $this->session = $session;
                $this->add_operation_log("login");
                return $this->output($data);
            }
        }
    }

    //vcubeIdに登録されている情報と照合
    function vcubeId_auth($vIdAuthToken = null, $consumerKey = NULL){
      if(!$vIdAuthToken){
        $res["result"]  = false;
        $res["backUrl"] = "";
        return $res;
      }
      ini_set("soap.wsdl_cache_enabled", "0");
      $wsdl = $this->config->get('VCUBEID','wsdl');

      try{
        $soap = new SoapClient($wsdl,array('trace' => 1));
    $this->logger2->info(array($consumerKey, $vIdAuthToken));
    $response = $soap->tokenCheck($consumerKey, $vIdAuthToken);

        $this->logger2->info($response);
        if($response["result"] === true){
          $res["result"]    = true;
          $res["userInfo"]  = $response["userInfo"];
        }else{
          $res["result"]    = false;
        }

      }
      catch(Exception $e){
        $this->logger2->error($e->getMessage());
        $res["result"]  = false;
        $res["backUrl"] = $_POST["backUrl"]? $_POST["backUrl"]: "";
      }
      return $res;

    }

    /**
     * VCUBEIDで認証してログイン処理
     *
     * @param string id ユーザーID
     * @param string pw ユーザーパスワード
     * @return string $output テンプレートを表示
     */
    function action_vcubeid_login()
    {
        require_once("classes/mgm/MGM_Auth.class.php");
        require_once("classes/N2MY_Auth.class.php");
        require_once("classes/N2MY_Account.class.php");

        //vcubeIDで認証
        $service = $this->request->get("service");
        switch ($service) {
            case "meeting":
                $consumerKey = $this->config->get('VCUBEID','meeting_consumer_key');
                break;
            case "paperless":
                $consumerKey = $this->config->get('VCUBEID','paperless_free_consumer_key');
                break;
            case "meeting_free":
                $consumerKey = $this->config->get('VCUBEID','meeting_free_consumer_key');
                break;
            case "paperless_free":
                $consumerKey = $this->config->get('VCUBEID','paperless_free_consumer_key');
                break;
            case "messenger":
                $consumerKey = $this->config->get('VCUBEID','messenger_consumer_key');
                break;
            default:
                return $this->display_error("100", "PARAMETER_ERROR", "service");
        }
        $res = $this->vcubeId_auth($this->request->get("vcubeid_token"), $consumerKey);
    $this->logger2->info($res);
    if($res["result"] ===  false){  //なにかおかしかったらそれなりのところに飛ばす
          return $this->display_error("1", "Login failed / Invalid auth token", "");
        }

        $lang = $this->get_language();
        $lang_conf_file = N2MY_APP_DIR."config/lang/".$lang."/message.ini";
        $this->_message = parse_ini_file($lang_conf_file, true);
        // パラメタ取得
        $id          = $res["userInfo"];
        $lang        = $this->request->get("lang", "ja");
        $country     = $this->request->get("country", "jp");
        $time_zone   = $this->request->get(API_TIME_ZONE, "9");
        $enc         = $this->request->get("enc", null);
        $output_type = $this->request->get("output_type");
        $login_type  = $this->request->get("login_type", null);

        $this->logger->info(sprintf("api v1 action_login id:%s enc:%s login_type:%s",
                                    $id, $enc, $login_type));
        // DB選択
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        $rules = array(
            "id"     => array(
                "required" => true
                ),
            "lang"     => array(
                "allow" => array_keys($this->get_language_list()),
                ),
            "country"     => array(
                "allow" => array_keys($this->get_country_list()),
                ),
            API_TIME_ZONE     => array(
                "allow" => array_keys($this->get_timezone_list()),
                ),
            "enc" => array(
                "allow" => array("md5", "sha1"),
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $request = $this->request->getAll();
        //古いAPI利用者用対応(zhの場合はzh-cnに変更)
        if ($request["lang"] == "zh") {
            $request["lang"] = "zh-cn";
        }
        $request["id"] = $id;
    $err_obj = $this->error_check($request, $rules);
        if (!$user_info = $obj_MGMClass->getUserInfoById( $id ) ){
            $err_obj->set_error("id", "SELECT_USER_ERROR", $id);
        } else {
            if (!$server_info = $obj_MGMClass->getServerInfo( $user_info["server_key"] )){
                $err_obj->set_error("", "SELECT_USER_ERROR", $id);
                return $this->display_error("1", "SELECT_SERVER_ERROR");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            $login = new N2MY_Auth($server_info["dsn"]);
            $user_type = "";
            // ユーザ確認
            //$login_info = $login->check($id, $pw, $enc);
            $login_info = $login->checkVcubeId($id);
            $obj_N2MY_Account = new N2MY_Account($server_info["dsn"]);
            if (!$login_info) {
                return $this->display_error("1", "USER_ID_PASS_INVALID");
            } else if (false === $obj_N2MY_Account->checkRemoteAddress($login_info["user_info"]["user_key"], $_SERVER["REMOTE_ADDR"])){
                    $session = EZSession::getInstance();
                    $session->removeAll();
                    $err_obj->set_error("ip_white_list", "200105", $id);
                    return $this->display_error("1", "Login failed / Invalid auth token", $this->get_error_info($err_obj));
            } else if ($login_info["member_info"]["member_type"] == "terminal" || $login_info["member_info"]["member_type"] == "centre") {
                    return $this->display_error("1", "Login failed / Invalid member type");
            } else {
                // セッションスタート
                $session = EZSession::getInstance();
                // ID を取得
                $session_id = session_id();
                // ユーザ情報配置
                if  ($output_type) {
                    $session->set("output_type", $output_type);
                }
                if ($country == "auto") {
                  $result_country_key = $this->_get_country_key($country);
                  $country_key = $result_country_key;
                }
                $this->logger2->debug($login_info);
                $session->set("login", "1");
                $session->set("lang", $lang);
                $session->set("country_key", $country);
                $session->set("time_zone", $time_zone);
                $session->set("dsn_key", $server_info["host_name"]);
                $session->set("server_info", $server_info);
                // ユーザー情報
                $user_info = $login_info["user_info"];
                $session->set("user_info", $login_info["user_info"]);
                //メンバー課金の場合は会議内招待ができないようにguest扱いにする
                if ($user_info["account_model"] == "member") {
                    $session->set("api_login_type", "member");
                }
                // メンバー情報
                if (isset($login_info["member_info"])) {
                    $type = "member";
                    $session->set("member_info", $login_info["member_info"]);
                    $member_info = $login_info["member_info"];
                } else {
                    $type = "user";
                }
                if ($login_type == 'cybozu' && !$user_info['is_cybozu_option']){
                    return $this->display_error("1", "USER_DISABLED_API");
                }
                // 部屋情報
                $obj_N2MY_Account = new N2MY_Account($server_info["dsn"]);
                if ($login_info["member_info"]["use_sales"]) {
                    $rooms = $obj_N2MY_Account->getFullRoomList( $user_info["user_key"], $member_info["member_key"], $user_info["account_model"] );
                } else if ( $login_info["member_info"] && ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre" || $user_info["account_model"] == "free")) {
                    $rooms = $obj_N2MY_Account->getOwnRoom( $member_info["member_key"], $user_info["user_key"]);
                } else {
                    $rooms = $obj_N2MY_Account->getRoomList( $user_info["user_key"] );
                }
                // 既存のセッションデータにも書き込む
                $session->set('room_info', $rooms);
                // ユーザー情報整形
                // 戻り値
                $data = array(
                    "type" => $type,
                    "session" => $session_id,
                    "user_info" => array(
                        'user_id'           => $user_info['user_company_address'],
                        'user_name'         => $user_info['user_company_name']
                        ),
                    "member_info" => array(
                        'member_id'         => $member_info['member_id'],
                        'member_email'      => $member_info['member_email'],
                        'member_name'       => $member_info['member_name'],
                        'member_name_kana'  => $member_info['member_name_kana'],
                        'timezone'          => $member_info['timezone'],
                        'lang'              => $member_info['lang'],
                        ),
                    );
                $this->session = $session;
                $this->add_operation_log("login");
                return $this->output($data);
            }
        }
    }

    /**
     * メンバー一覧取得
     *
     * @param
     */
    function action_get_member_list() {
        $this->checkAuthorization();
        // チェックルール編集
        $rules = array(
            "output_type"     => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        // チェックデータ（リクエスト）編集
        $request = $this->request->getAll();
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        require_once("classes/dbi/member.dbi.php");
        $memberTable  = new MemberTable($this->get_dsn());
        $user_info = $this->session->get("user_info");
        $where = "user_key = ".addslashes($user_info["user_key"]).
             " AND member_status = 0";
        $member_list = $memberTable->getRowsAssoc($where);
        if (!$member_list) {
            return $this->display_error(1, "MEMBER_NOTFOUND");
        }
        foreach($member_list as $_key => $_val) {
            $data["members"]["member"][$_key] = array(
                'member_id'         => $_val['member_id'],
                'member_email'      => $_val['member_email'],
                'member_name'       => $_val['member_name'],
                'member_name_kana'  => $_val['member_name_kana'],
                'timezone'          => $_val['timezone'],
                'lang'              => $_val['lang']
                );
//            unset($member_list[$_key]["member_pass"]);
//            unset($member_list[$_key]["member_key"]);
//            unset($member_list[$_key]["user_key"]);
//            unset($member_list[$_key]["member_status"]);
//            unset($member_list[$_key]["member_group"]);
//            unset($member_list[$_key]["create_datetime"]);
//            unset($member_list[$_key]["update_datetime"]);
//            unset($member_list[$_key]["room_key"]);
        }
//        $data["members"]["member"] = $member_list;
        return $this->output($data);
    }

    /**
     * メンバー情報編集
     *
     * @param
   */
    function action_edit_member_info() {

      $this->checkAuthorization();
      $member_info = $this->session->get("member_info");
      if (!$member_info) {
        return $this->display_error(1, "PLEASE USE MEMBER ID LOGIN FIRST.");
      }
        $request = array(
            "member_pass"         => $this->request->get("password", null),
            "member_email"        => $this->request->get("member_email", null),
            "member_name"         => $this->request->get("member_name", null),
            "member_name_kana"    => $this->request->get("member_name_kana", null),
            "timezone"            => $this->request->get("timezone", null),
            "lang"                => $this->request->get("lang", null),
            "output_type"         => $this->request->get("output_type", null),
        );
        $rules = array(
            "member_email"  => array(
                "email" => true
            ),
            "member_name"     => array(
                "maxlen" => 255,
            ),
            "member_name_kana"     => array(
                "maxlen" => 255,
            ),
            "timezone"     => array(
                "allow" => array_keys($this->get_timezone_list()),
            ),
            "lang"     => array(
                "allow" => array_keys($this->get_language_list()),
            ),
            "output_type"     => array(
                "allow" => $this->get_output_type_list(),
            ),
        );

        $err_obj = $this->error_check($request, $rules);
        if($request["member_pass"]){
            if (!preg_match('/^[!-\[\]-~]{8,128}$/', $request["member_pass"])) {
                $err_obj->set_error("pw", "USER_ERROR_PASS_INVALID_01");
            } elseif (!preg_match('/[[:alpha:]]+/', $request["member_pass"]) || preg_match('/^[[:alpha:]]+$/', $request["member_pass"])) {
                $err_obj->set_error("pw", "USER_ERROR_PASS_INVALID_02");
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(100, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        require_once ('lib/EZLib/EZUtil/EZEncrypt.class.php');
        require_once("classes/dbi/member.dbi.php");
        $objMember = new MemberTable($this->get_dsn());
        $data = array(
            "member_pass"       => $request["member_pass"]?EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $request["member_pass"]):$member_info["member_pass"],
            "member_email"      => $request["member_email"]?$request["member_email"]:$member_info["member_email"],
            "member_name"       => $request["member_name"]?$request["member_name"]:$member_info["member_name"],
            "member_name_kana"  => $request["member_name_kana"]?$request["member_name_kana"]:$member_info["member_name_kana"],
            "timezone"          => $request["timezone"]?$request["timezone"]:$member_info["timezone"],
            "lang"              => $request["lang"]?$request["lang"]:$member_info["lang"],

        );
        $this->logger->info(__FUNCTION__."#member info",__FILE__,__LINE__,$data);
        $where = "member_key = ".$member_info["member_key"];
        $objMember->update($data, $where);
        // メンバー情報更新
        $member_info = $objMember->getDetail($member_info["member_key"]);
        $this->session->set("member_info", $member_info);
        return $this->output();
    }

    /**
     * 入室可能な部屋一覧取得
     */
    function action_get_room_list() {
        require_once("classes/dbi/room.dbi.php");
        $this->checkAuthorization();
        $user_info = $this->session->get("user_info");
        $room_info = $this->session->get("room_info");
        if (!$room_info) {
            return $this->display_error(100, "ROOM_NOTFOUND");
        }

        /*
        $obj_Room = new RoomTable($this->get_dsn());
        $where = "user_key = ".addslashes($user_info["user_key"]).
             " AND room_status = 1";
        $max_seat = $_REQUEST["max_seat"];
        $this->logger2->info($max_seat);
        $result = $obj_Room->getRowsAssoc($where, null, null, null, "room_key,room_name","room_key");
        */
        $max_seat = $this->request->get("max_seat", "");
        $only_nomal_room = $this->request->get("only_nomal_room", "0");

        if(!is_numeric($max_seat)){
          $max_seat = null;
        }
        $room_list = array();
        foreach($room_info as $room_key => $room) {
            if($room['room_info']['is_extend_room'] == 1 && $only_nomal_room){
                continue;
            }
            $enable_transcoder = 0;
            if ($room['room_info']['default_h264_use_flg'] == 1 && $room['options']['h264']) {
                $h264 = 1;
                //configでも有効であれば、Transcorderも有効にする
                $enable_transcoder = $this->config->get('N2MY', 'enable_h264_transcoder') ? 1 : 0;
            } else {
                $h264 = 0;
            }
            require_once("classes/dbi/ives_setting.dbi.php");
            $ivesSettingTable = new IvesSettingTable($this->get_dsn());
            $where = "room_key = '" . addslashes($room['room_info']['room_key']) . "' AND is_deleted = 0";
            $ives_setting = $ivesSettingTable->getRow($where);
            if(!$max_seat || $max_seat == $room['room_info']['max_seat']){
              $roomList = array(
                "room_info" => array(
                  'room_id'               => $room['room_info']['room_key'],
                  'max_seat'              => $room['room_info']['max_seat'],
                  'max_audience_seat'     => $room['room_info']['max_audience_seat'],
                  'max_whiteboard_seat'   => $room['room_info']['max_whiteboard_seat'],
                  'room_name'             => $room['room_info']['room_name'],
                  'use_sales'             => $room['room_info']['use_sales_option'],
                  'is_extend_room'        => $room['room_info']['is_extend_room'],
                  'media_server_key'      => $ives_setting['media_mixer_key'] ? $ives_setting['media_mixer_key'] : "",
                  'transcoder'            => $enable_transcoder,
                ),
                "options" => array(
                  'meeting_ssl'           => $room['options']['meeting_ssl'],
                  'desktop_share'         => $room['options']['desktop_share'],
                  'high_quality'          => $room['options']['high_quality'],
                  'mobile_phone_number'   => $room['options']['mobile_phone_number'],
                  'h323_number'           => $room['options']['h323_number'],
                  'whiteboard'            => $room['options']['whiteboard'],
                  'multicamera'           => $room['options']['multicamera'],
                  'telephone'             => $room['options']['telephone'],
                  'smartphone'            => $room['options']['smartphone'],
                  'record_gw'             => $room['options']['record_gw'],
                  'h264'                  => $h264,
                  'video_conference'      => $room['options']['video_conference'],
                )
              );
              $room_list[] = $roomList;
      }
        }
        // チェックルール編集
        $rules = array(
            "output_type"     => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        // チェックデータ（リクエスト）編集
        $request = array(
            "output_type" => $this->request->get("output_type", null),
            );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $data["rooms"] = array(
            "room" => $room_list,
        );
        return $this->output($data);
    }

    /**
     * 入室可能な部屋詳細取得
     */
    function action_get_room_detail() {
        require_once("classes/dbi/room.dbi.php");
        $this->checkAuthorization();
        $room_key = $this->request->get(API_ROOM_ID);
        // チェックルール編集
        $rules = array(
            API_ROOM_ID     => array(
                "required" => true,
                "maxlen"   => 64,
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        // チェックデータ（リクエスト）編集
        $request = array(
            API_ROOM_ID    => $room_key,
            "output_type" => $this->request->get("output_type", null),
            );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // 存在チェック
        $room_info = $this->session->get("room_info");
        if (!$room_info[$room_key]) {
            $err_obj->set_error(API_ROOM_ID, "ROOM_NOTFOUND", $room_key);
        }
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        //
        $room_info[$room_key]["room_info"][API_ROOM_ID] = $room_info[$room_key]["room_info"]["room_key"];
        if ($this->config->get("N2MY", "mail_wb_upload")) {
            $room_info[$room_key]["room_info"]["mfp_address"] = $room_info[$room_key]["room_info"]["room_key"]."@".$this->config->get("N2MY", "mail_wb_host");
        }

        $enable_transcoder = 0;
        if ($room_info[$room_key]['room_info']['default_h264_use_flg'] == 1 && $room_info[$room_key]['options']['h264']) {
            $h264 = 1;
            //configでも有効であれば、Transcorderも有効にする
            $enable_transcoder = $this->config->get('N2MY', 'enable_h264_transcoder') ? 1 : 0;
        } else {
            $h264 = 0;
        }
        $room_info[$room_key] = array(
            "room_info" => array(
                'room_id'                 => $room_info[$room_key]['room_info']['room_key'],
                'max_seat'                => $room_info[$room_key]['room_info']['max_seat'],
                'max_audience_seat'       => $room_info[$room_key]['room_info']['max_audience_seat'],
                'max_whiteboard_seat'     => $room_info[$room_key]['room_info']['max_whiteboard_seat'],
                'room_name'               => $room_info[$room_key]['room_info']['room_name'],
                'mfp'                     => $room_info[$room_key]['room_info']['mfp'],
                'mfp_address'             => $room_info[$room_key]['room_info']['address'],
                'cabinet'                 => $room_info[$room_key]['room_info']['cabinet'],
                'transcoder'        => $enable_transcoder,
              'is_device_skip'          => $room_info[$room_key]['room_info']['is_device_skip'],
              'default_microphone_mute' => $room_info[$room_key]['room_info']['default_microphone_mute'],
              'default_camera_mute'     => $room_info[$room_key]['room_info']['default_camera_mute'],
              ),
          "options" => array(
                'meeting_ssl'             => $room_info[$room_key]['options']['meeting_ssl'],
                'desktop_share'           => $room_info[$room_key]['options']['desktop_share'],
                'high_quality'            => $room_info[$room_key]['options']['high_quality'],
                'mobile_phone_number'     => $room_info[$room_key]['options']['mobile_phone_number'],
                'h323_number'             => $room_info[$room_key]['options']['h323_number'],
                'whiteboard'              => $room_info[$room_key]['options']['whiteboard'],
                'multicamera'             => $room_info[$room_key]['options']['multicamera'],
                'telephone'               => $room_info[$room_key]['options']['telephone'],
                'smartphone'              => $room_info[$room_key]['options']['smartphone'],
                'record_gw'               => $room_info[$room_key]['options']['record_gw'],
                'h264'                    => $h264,
                'video_conference'        => $room_info[$room_key]['options']['video_conference'],
            )
        );
        $data = $room_info[$room_key];
        return $this->output($data);
    }

    /**
     * 部屋のステータス取得
     */
    function action_get_room_status() {
        require_once ("classes/dbi/reservation.dbi.php");
        require_once ("classes/dbi/room.dbi.php");
        require_once ('lib/EZLib/EZUtil/EZDate.class.php');
        require_once ("classes/AppFrame.class.php");
        require_once ("classes/N2MY_Reservation.class.php");
        require_once ("classes/core/Core_Meeting.class.php");
        require_once ("classes/core/dbi/Meeting.dbi.php");
        $this->checkAuthorization();
        $objReservation = new N2MY_Reservation($this->get_dsn());
        $obj_Room = new RoomTable($this->get_dsn());
        $_rooms = $this->request->get(API_ROOM_ID);
        $reservations = array();
        // 部屋と会議キーを指定
        if (is_array($_rooms)) {
            foreach ($_rooms as $room_key => $meeting_key) {
                // 契約した部屋のみ取得可
                $query["rooms[".$room_key."]"] = $meeting_key;
                // 予約情報取得
//                $reservations[$room_key] = $this->_get_reservation_list($room_key);
            }
        // 部屋のみ指定（現在の状態）
        } else {
            $roomkeys = explode(",", $_rooms);
            $rooms = array();
            foreach($roomkeys as $room_key) {
                $rooms[] = $room_key;
            }
            $where = "room_key in ('".join("','", $rooms)."')";
            $rooms = $obj_Room->getRowsAssoc($where, null, null, null, "room_key, meeting_key");
            if ( DB::isError( $rooms ) ) {
                $this->logger->error( $rs->getUserInfo() );
                return $rooms;
            }
            foreach( $rooms as $key => $val ){
                $query[$val["room_key"]] = $val["meeting_key"];
//                $reservation_list = $this->_get_reservation_list( $val["room_key"] );
            }
        }
        // 指定会議の予約情報取得
        $obj_CoreMeeting = new Core_Meeting($this->get_dsn());
        $room_list = $obj_CoreMeeting->getMeetingStatus( $query );
//        $this->logger2->info($room_list);
        $list = array();
        foreach( $room_list as $key => $room){
            $participants = array();
            foreach($room["participants"] as $participant) {
                // 余計な情報は公開しない
                $participants["participant"][] = array(
                    "use_count"             => $participant["use_count"],
                    "participant_id"        => $participant["participant_session_id"],
                    "participant_name"      => $participant["participant_name"],
                    "participant_type"      => $participant["participant_type_name"],
                    );
            }
            //$room["participants"]["participant"] = $participants;
            $room["is_active"] = $room["status"];
            // 会議中か、空室かという意味にする。
            $room["status"] = (($room["status"] == 1 && $room["pcount"] > 0)) ? 1 : 0;
            $meeting_info = "";
            if ($room["is_active"] == 1) {
                require_once 'classes/dbi/meeting.dbi.php';
                $objMeeting = new MeetingTable($this->get_dsn());
                $where = "meeting_key = '".addslashes($room["meeting_key"])."'" .
                    " AND is_active = 1" .
                    " AND is_deleted = 0";
                $meeting_info = $objMeeting->getRow($where, "pin_cd");
            }
            // パラメタ
            $options = array(
                "start_time" => date("Y-m-d H:i:s"),
                "end_time"   => "",
                "limit"      => RESERVATION_MAX_VIEW,
                "offset"     => null,
                "sort_key"   => "reservation_starttime",
                "sort_type"  => "asc",
                );

            $_resavation_list = $objReservation->getList($room["room_key"], $options);
            foreach($_resavation_list as $_key => $_val) {
                $resavation_list[] = array(
                'reservation_session'       => $_val['reservation_session'],
                'meeting_id'                => $_val['meeting_key'],
                'reservation_name'          => $_val['reservation_name'],
                'reservation_pw'            => $_val['reservation_pw'],
                'sender_name'               => $_val['sender_name'],
                'sender_email'              => $_val['sender_email'],
                'status'                    => $_val['status'],
                'reservation_start_date'    => $_val['reservation_starttime'],
                'reservation_end_date'      => $_val['reservation_endtime']
                );
//
//                unset($resavation_list[$_key]["reservation_key"]);
//                unset($resavation_list[$_key]["reservation_place"]);
//                unset($resavation_list[$_key]["user_key"]);
//                unset($resavation_list[$_key]["room_key"]);
//                $resavation_list[$_key][API_MEETING_ID] = $this->ticket_to_session($resavation_list[$_key]["meeting_key"]);
//                unset($resavation_list[$_key]["meeting_key"]);
//                unset($resavation_list[$_key]["reservation_info"]);
//                unset($resavation_list[$_key]["reservation_status"]);
//                unset($resavation_list[$_key]["reservation_registtime"]);
//                unset($resavation_list[$_key]["reservation_updatetime"]);
//
//                $resavation_list[$_key]["reservation_start_date"] = $resavation_list[$_key]["reservation_starttime"];
//                unset($resavation_list[$_key]["reservation_starttime"]);
//                $resavation_list[$_key]["reservation_end_date"] = $resavation_list[$_key]["reservation_endtime"];
//                unset($resavation_list[$_key]["reservation_endtime"]);
//                if (!$resavation_list[$_key]["reservation_pw"]) {
//                    $resavation_list[$_key]["reservation_pw"] = 0;
//                } else {
//                    $resavation_list[$_key]["reservation_pw"] = 1;
//                }
            }
            $room[API_ROOM_ID] = $room["room_key"];
            $room[API_MEETING_ID] = $this->ticket_to_session($room["meeting_ticket"]);
            $room = array(
            'room_id'       => $room['room_id'],
            'meeting_id'    => $room['meeting_id'],
            'pin_cd'        => $meeting_info ? $meeting_info["pin_cd"] : "",
            'status'        => $room['status'],
            'pcount'        => $room['pcount'],
            'participants'  => $participants
            );

//            unset($room["meeting_ticket"]);
//            unset($room["room_key"]);
//            unset($room["meeting_key"]);
//            unset($room["reservation_key"]);
//            unset($room["user_key"]);

            $room["reservations"]["reservation"] = $resavation_list;
            $list[] = $room;
        }
        $data = array(
            "room_status" => $list,
        );
        return $this->output($data);
    }

    function action_get_country_list()
    {
        require_once("classes/N2MY_Auth.class.php");
        $get_language = $this->get_language();
        $lang_conf_file = N2MY_APP_DIR."config/lang/".$get_language."/message.ini";
        $this->_message = parse_ini_file($lang_conf_file, true);
        $rules = array(
            "lang"     => array(
                "allow" => array_keys($this->get_language_list()),
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $request = $this->request->getAll();
        //古いAPI利用者用対応(zhの場合はzh-cnに変更)
        if ($request["lang"] == "zh") {
            $request["lang"] = "zh-cn";
        }
        $lang = $this->request->get("lang");
        $err_obj = $this->error_check($request, $rules);
        if (!$lang && $this->checkAuthorizationStatus()) {
            $lang = $this->session->get("lang");
        } else if (!$lang) {
            $err_obj->set_error("lang", "required");
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }

        $output_type = $this->request->get("output_type");
        $lang = $this->get_language($lang);
        $lang_conf_file = N2MY_APP_DIR."config/lang/".$lang."/message.ini";
        $this->_message = parse_ini_file($lang_conf_file, true);

        $country_list = $this->get_country_list();
        foreach ($country_list as $country) {
            $result[] = array("country_key" => $country["country_key"],
                              "country_name" => $country["country_name"]);
        }
        $data["country_list"] = array(
            "country" => $result,
        );
        return $this->output($data);
    }

    /**
     * パスワード変更
     */
    function action_change_password() {
        $this->checkAuthorization();
        $new_passwd = $this->request->get("pw");
//        $new_passwd = $this->request->get("passwd");
        $rules = array(
            "pw"     => array(
                "required" => true,
                "range" => array(8, 128), // 6～10文字
                ),
        );
        $data = array(
            "pw"     => $new_passwd,
        );
        $err_obj = $this->error_check($data, $rules);
        if (!preg_match('/^[!-\[\]-~]{8,128}$/', $new_passwd)) {
            $err_obj->set_error("pw", "USER_ERROR_PASS_INVALID_01");
        } elseif (!preg_match('/[[:alpha:]]+/', $new_passwd) || preg_match('/^[[:alpha:]]+$/', $new_passwd)) {
            $err_obj->set_error("pw", "USER_ERROR_PASS_INVALID_02");
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(100, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        if ($member_info = $this->session->get("member_info")) {
            require_once("classes/dbi/member.dbi.php");
            $objMember = new MemberTable($this->get_dsn());
            $result = $objMember->changePasswd($member_info["member_key"], $new_passwd);
            $this->add_operation_log("change_login_password");
        } else {
            $user_info = $this->session->get("user_info");
            require_once("classes/dbi/user.dbi.php");
            $objUser = new UserTable($this->get_dsn());
            $result = $objUser->changePasswd($user_info["user_key"], $new_passwd);
            $this->add_operation_log("change_login_password");
        }
        return $this->output();
    }

    /**
     * ユーザー情報の更新
     *
     * ユーザー情報自体がそんなに無いので取りあえず、名前の変更ぐらいにしておく
     */
    function action_update() {
        $this->checkAuthorization();
        // チェックルール編集
        $rules = array(
            "email"     => array(
                "email" => true
                ),
            "member_name"     => array(
                "maxlen" => 196605,
                ),
            "member_name_kana"     => array(
                "maxlen" => 196605,
                ),
            "timezone"     => array(
                "allow" => array_keys($this->get_timezone_list()),
                ),
            "lang"     => array(
                "allow" => array_keys($this->get_language_list()),
                ),
            "country"     => array(
                "allow" => array_keys($this->get_country_list()),
                ),
            "output_type"     => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        // チェックデータ（リクエスト）編集
        $request = array(
            "email"       => $this->request->get("email", null),
            "name"        => $this->request->get("name", null),
            "name_kana"   => $this->request->get("name_kana", null),
            "timezone"    => $this->request->get("timezone", null),
            "lang"        => $this->request->get("lang", null),
            "country"     => $this->request->get("country", null),
            "output_type" => $this->request->get("output_type", null),
            );
        //古いAPI利用者用対応(zhの場合はzh-cnに変更)
        if ($request["lang"] == "zh") {
            $request["lang"] = "zh-cn";
        }
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }

        // メンバーの場合はユーザー情報を変更
        if ($member_info = $this->session->get("member_info")) {
            require_once("classes/dbi/member.dbi.php");
            $objMember = new MemberTable($this->get_dsn());
            // メール
            if ($this->request->get("email")) {
                    $data["member_email"] = $this->request->get("email");
            }
            // 名前
            if ($this->request->get("name")) {
                $data["member_name"] = $this->request->get("name");
            }
            // かな
            if ($this->request->get("name_kana")) {
                $data["member_name_kana"] = $this->request->get("name_kana");
            }
            // タイムゾーン
            if ($this->request->get("timezone")) {
                $data["timezone"] = $this->request->get("timezone");
            }
            // 言語
            if ($this->request->get("lang")) {
                $data["lang"] = $this->request->get("lang");
            }
            $where = "member_key = ".$member_info["member_key"];
            $objMember->update($data, $where);
            // メンバー情報更新
            $member_info = $objMember->getDetail($member_info["member_key"]);
            $this->session->set("member_info", $member_info);
        }
        // タイムゾーン
        if ($this->request->get("timezone")) {
            $this->session->set("time_zone", $this->request->get("timezone"));
        }
        // 言語
        if ($this->request->get("lang")) {
            $this->session->set("lang", $this->request->get("lang"));
        }
        // 開催地域
        if ($this->request->get("country")) {
            $this->session->set("country_key", $this->request->get("country"));
        }
        return $this->output();
    }

    function action_get_info () {
        $this->checkAuthorization();
        $request = $this->request->getAll();
        $lang = $this->request->get("lang");
        $rules = array(
            "lang"     => array(
                "required" => true,
                "allow" => array_keys($this->get_language_list()),
                ),
            "output_type"     => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
        $cd_lang = EZLanguage::getLangCd($lang);
        require_once("classes/dbi/news.dbi.php");
        $objNewsTable = new NewsTable($this->get_auth_dsn());
        $where = "news_lang = '".$cd_lang."' AND news_delete = 0";
        $news_list = $objNewsTable->getRowsAssoc($where);
        foreach ($news_list as $data) {
            $information["title"] = $data["news_title"];
            $information["contents"] = $data["news_contents"];
            //ファイルリスト
            require_once("classes/dbi/news_cabinet.dbi.php");
            $objNewsCabinetTable = new NewsCabinetTable($this->get_auth_dsn());
            $news_cabinets = $objNewsCabinetTable->get_news_cabinet_list($data["news_key"]);
            foreach ($news_cabinets as $news_cabinet) {
            if($news_cabinet) {
              $file["file_id"] = $news_cabinet["session_id"];
                $file["name"] = $news_cabinet["file_name"];
                $file["type"] = $news_cabinet["type"];
                $file["extension"] = $news_cabinet["extension"];
                $file["tmp_name"] = $news_cabinet["tmp_name"];
                $file["path"] = $news_cabinet["file_path"] ? $this->config->get("INFORMATION", "scp_dir").$news_cabinet["file_path"]: "";
//                 if($news_cabinet["file_path"]) {
//                   $file["download_url"] = N2MY_BASE_URL."api/v1/user/?action_download_file=&id=".$news_cabinet["session_id"];
//                 } else {
//                   $file["download_url"] = "";
//                 }
                $information["files"]["file"][] = $file;
            }

            }
            $informations[] = $information;
        }
        $this->logger2->debug($informations);
        return $this->output($informations);
    }

    function action_download_file() {
        $session_id = $this->request->get("id");
        //$this->logger2->info($session_id);
        require_once("classes/dbi/news_cabinet.dbi.php");
        $objNewsCabinetTable = new NewsCabinetTable($this->get_auth_dsn());
        $objNewsCabinetTable->download_news_cabinet($session_id);
    }

    function action_get_mediaserver_list () {
        $this->checkAuthorization();
        $request = $this->request->getAll();
        $rules = array(
            "output_type"     => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        require_once("classes/mgm/dbi/media_mixer.dbi.php");
        $objMediaMixerTable = new MediaMixerTable($this->get_auth_dsn());
        $media_list = $objMediaMixerTable->getRowsAssoc();
        foreach ($media_list as $media) {
            $media_server["media_server_key"] = $media["media_mixer_key"];
            $media_server["media_server_status"] = $media["is_available"];
            $media_servers[] = $media_server;
        }
        $data = array(
                    "mediaservers" => array(
                            "mediaserver" => $media_servers
                    )
            );
        $this->logger2->info($data);
        return $this->output($data);
    }



    function action_update_mediaserver_status () {
        $this->checkAuthorization();
        $request = $this->request->getAll();
        $rules = array(
            "media_server_key"     => array(
                "required" => true,
                ),
            "media_server_status"     => array(
                "allow" => array(0, 1),
                ),
            "output_type"     => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        require_once("classes/mgm/dbi/media_mixer.dbi.php");
        $objMediaMixerTable = new MediaMixerTable($this->get_auth_dsn());
        $where = "media_mixer_key = ".addslashes($request["media_server_key"]);
        $media_sever = $objMediaMixerTable->getRow($where);
        if (!$media_sever) {
            return $this->display_error("100", "PARAMETER_ERROR", "media_server_key error");
        }
        $data = array("is_available" => $request["media_server_status"]);
        $objMediaMixerTable->update($data, $where);
        return $this->output();
    }

    /**
     * ログアウト処理
     *
     * @param
     * @return
     */
    function action_logout()
    {
        $this->checkAuthorization();
        // チェックルール編集
        $rules = array(
            "output_type"     => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        // チェックデータ（リクエスト）編集
        $request = array(
            "output_type" => $this->request->get("output_type", null),
            );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $status = 0;
        if (session_destroy()) {
            $status = 1;
            $this->add_operation_log("logout");
        }
        return $this->output();
    }

}
$main = new N2MY_Meeting_Auth_API();
$main->execute();
