#!/usr/bin/perl

use LWP 5.64;

#my $serverUrl = 'http://172.21.98.184:8080/TestWebApp/resources/guestdid?guestDid=';
#my $serverUrl = 'http://172.21.98.184:8080/mcuGuestController-war/resources/conferenceDid?guestDid=';
my $serverUrl = 'http://localhost:8080/mcuGuestController/resources/conferenceDid?guestDid=';

sub getConferenceDid
{
  my $message = shift(@_);
  #OpenSIPS::log(L_INFO, "message=".$message."\n");

  my $guestDid = $message->pseudoVar("\$rU");
  #OpenSIPS::log(L_INFO, "perl received guestDid=".$guestDid."\n");
  my $confDid = "0";

  my $url = $serverUrl.$guestDid;
  my $browser = LWP::UserAgent->new;
  my $response = $browser->get($url);
  if ($response->is_success) {
    $confDid = $response->content;
  }
  OpenSIPS::log(L_INFO, "perl guestDid=".$guestDid." confDid=".$confDid."\n");

  OpenSIPS::AVP::add("confDid", $confDid);

  return(1);
}
