<?php

class LogFileLoader
{
    const TARGET_STR_PUT    = 'REST.PUT.OBJECT';
    const TARGET_STR_DELETE = 'REST.DELETE.OBJECT';

    private $accessLogDirPath;

    public function __construct($accessLogDirPath)
    {
        $this->accessLogDirPath = $accessLogDirPath;
    }

    public function getUpdateLog($excludePath)
    {
        date_default_timezone_set('GMT');

        $targetObjects['put']    = array();
        $targetObjects['delete'] = array();

        $targetPrefix = date('Y-m-d-H-', strtotime('-1 hour'));

        date_default_timezone_set('Asia/Tokyo');

        $targetFiles = array();

        if( !is_dir($this->accessLogDirPath) ) return false;

        $files = scandir($this->accessLogDirPath);

        foreach($files as $file){
            if( preg_match("/^".$targetPrefix."/", $file) ){
                $targetFiles[] = sprintf('%s/%s', $this->accessLogDirPath, $file);
            }
        }

        if( count($targetFiles) < 1 ) return $targetObjects;

        foreach($targetFiles as $file){
            $fp = fopen($file, "r");
            while( !feof($fp) ){
                $buf = fgets($fp);


                if( preg_match("/".self::TARGET_STR_PUT."/", $buf) ){
                    $data = explode(" ", $buf);
                    $sync = true;
                    if(count($excludePath) > 0){
                        foreach($excludePath as $v){
                            if( preg_match("|^".$v."|", $data[8]) ) $sync = false;
                        }
                    }

                    if($sync === true) $targetObjects['put'][$data[8]] = $data[8];

                }else if( preg_match("/".self::TARGET_STR_DELETE."/", $buf) ){
                    $data = explode(" ", $buf);

                    $sync = true;
                    if(count($excludePath) > 0){
                        foreach($excludePath as $v){
                            if( preg_match("|^".$v."|", $data[8]) ) $sync = false;
                        }
                    }

                    if($sync === true) $targetObjects['delete'][$data[8]] = $data[8];

                }
            }
        }

        return $targetObjects;

    }
}
