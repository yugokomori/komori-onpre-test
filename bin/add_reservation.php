#!/usr/local/bin/php
<?php
require_once("set_env.php");
require_once("classes/AppFrame.class.php");
require_once ("classes/N2MY_Reservation.class.php");
require_once("classes/dbi/reservation_user.dbi.php");
require_once("classes/dbi/reservation_extend.dbi.php");
require_once ("config/config.inc.php");

class AddReservation extends AppFrame {

    var $_mail_flg = 1;
    var $dsn = null;
    var $objReservation = null;
    var $objReservationUser = null;

    function __destruct() {
        //unlink(N2MY_APP_DIR."/tmp/sess_".session_id());
    }

    function init() {
        $this->dsn = $this->get_dsn();
        $this->objReservation = new N2MY_Reservation($this->dsn);
        $this->objReservationUser = new ReservationUserTable($this->get_dsn());
        
    }

    function default_view() {
    	//$rooms = array("test-1-95df", "test-2-7882");
    	$_rooms = "niiuser-1-a728, niiuser-2-1476, niiuser-3-d4a6, niiuser-4-f391, niiuser-5-f837, niiuser-6-35f2, niiuser-7-9458, niiuser-8-ecda, niiuser-9-d0ab, niiuser-10-5385, niiuser-11-e69f, niiuser-12-d360, niiuser-13-4abc, niiuser-14-94f3, niiuser-15-e359, niiuser-16-8b9b, niiuser-17-c37f, niiuser-18-1ede, niiuser-19-9aff, niiuser-20-a01e, niiuser-21-fa42, niiuser-22-6830, niiuser-23-b621, niiuser-24-7472, niiuser-25-20bd, niiuser-26-be8d, niiuser-27-511e, niiuser-28-eee0, niiuser-29-c2a0, niiuser-30-0248, niiuser-31-d869, niiuser-32-176b, niiuser-33-0656, niiuser-34-13ba, niiuser-35-1d4c, niiuser-36-5a48, niiuser-37-38e4, niiuser-38-4b7a, niiuser-39-5401, niiuser-40-2a87, niiuser-41-a263, niiuser-42-7a32, niiuser-43-8495, niiuser-44-c00b, niiuser-45-2606, niiuser-46-50cf, niiuser-47-880d, niiuser-93-9aef, niiuser-94-8b4c, niiuser-95-1304, niiuser-96-55d3, niiuser-97-4912, niiuser-98-1728, niiuser-99-9f43, niiuser-100-5c80, niiuser-101-8dc8, niiuser-102-884f, niiuser-103-aeea, niiuser-104-8b38, niiuser-105-b5ac, niiuser-106-5f3c, niiuser-107-5279, niiuser-108-6e81, niiuser-109-fed7, niiuser-110-f25f, niiuser-111-c91d, niiuser-112-5171, niiuser-113-f64f, niiuser-114-da8d, niiuser-115-fd48, niiuser-116-6a47, niiuser-117-7db3, niiuser-118-08ca, niiuser-119-b6a9, niiuser-120-849d, niiuser-121-c028, niiuser-122-1841, niiuser-123-af5e, niiuser-124-aa2f, niiuser-125-8c38, niiuser-126-ca17, niiuser-127-cfe8, niiuser-128-63cd, niiuser-129-59f6, niiuser-130-a2c5, niiuser-131-4363, niiuser-132-654f, niiuser-133-50f5, niiuser-134-2ea2, niiuser-135-01d7, niiuser-136-c2aa, niiuser-137-aec0, niiuser-138-6366, niiuser-139-32be, niiuser-140-ed2b, niiuser-141-097d, niiuser-142-5500, niiuser-143-66eb, niiuser-144-58bf, niiuser-145-f69c, niiuser-146-0977, niiuser-147-f6ac, niiuser-148-cc1f, niiuser-149-07da, niiuser-150-c555, niiuser-151-fd1d, niiuser-152-54e7, niiuser-153-e860, niiuser-154-bc02, niiuser-155-849d, niiuser-156-5940, niiuser-157-04c0, niiuser-158-aeef, niiuser-159-5062, niiuser-160-5817, niiuser-161-82ea, niiuser-162-c415, niiuser-163-1430, niiuser-164-c4fa, niiuser-165-3480, niiuser-166-a1b9, niiuser-167-d927, niiuser-168-84af, niiuser-169-4dab, niiuser-170-80ba, niiuser-171-e1e9, niiuser-172-322a, niiuser-173-c8ef, niiuser-174-2398, niiuser-175-1012, niiuser-176-8433, niiuser-177-0452, niiuser-178-46b5, niiuser-179-0b4d, niiuser-180-cc2c, niiuser-181-b944, niiuser-182-e4aa, niiuser-183-082e, niiuser-184-2732, niiuser-185-f5e2, niiuser-186-37ab";
    	$rooms = split('[, ]', $rooms);
    	$user_key = "1";
    	$member_key = "1";
    	for($j=1; $j<6; $j++) {
    		for($i=10; $i<18; $i++) {
    			$reservation_starttime = "2014-11-".$j." ".$i.":00:00";
    			$reservation_endtime = "2014-11-".$j." ".($i+1).":00:00";
    			$this->logger2->info($reservation_starttime);
    			foreach($rooms as $room_key) {
    				$reservation_data["info"] = array(
    						"user_key" => $user_key,
    						"member_key" => $member_key,
    						"room_key" => $room_key,
    						"reservation_name" => "test-mess-reservation",
    						"reservation_place" => "9",
    						"reservation_starttime" => $reservation_starttime,
    						"reservation_endtime" => $reservation_endtime,
    						"sender" => "",
    						"sender_mail" => "",
    						"sender_lang" => "ja",
    						"mail_body" => "",
    						"mail_type"				 => "",
    						"is_reminder_flg"        => "",
    						"is_convert_wb_to_pdf"   => "1",
    						"is_rec_flg"             => "1",
    						"is_cabinet_flg"         => "1",
    						"is_desktop_share"       => "1",
    						"is_invite_flg"          => "1",
    						"is_limited_function"    => "1",
    						"payment_type"           => "1",
    						"is_publish"             => "1",
    				);
    				$reservation_data["guest_flg"] = 0;
    				$reservation_data["guests"] = array();
    				$reservation_data["organizer"] = array(
    						"name" => "",
    						"email" => "",
    						"timezone" => "",
    						"lang" => "",
    				);
    				$this->logger2->info($reservation_data);
    				$result = $this->objReservation->add($reservation_data);
    			}
    		}
    	}
    }


}
$main = new AddReservation();
$main->execute();
