#!/usr/local/bin/php
<?php
set_time_limit ( 0 );
ini_set ( "memory_limit", "2048M" );
require_once ("set_env.php");
require_once ("classes/AppFrame.class.php");
require_once("classes/pis_daily_createCSV.class.php");
//エラーログ定数
define("ERROR_FTP_WIDS_KIHON", "error FTP WIDS_KIHON");
define("ERROR_FTP_WIDS_ZAISEKI", "error FTP WIDS_ZAISEKI");
define("ERROR_FTP_WIDS_HENKAN", "error FTP WIDS_HENKAN");
define("ERROR_FTP_WIDS_GID_KIHON", "error FTP WIDS_GID_KIHON");
define("ERROR_FTP_WIDS_GID_SYOZOKU", "error FTP WIDS_GID_SYOZOKU");
define("ERROR_KIHON_FILE_NOT_FOUND", "KIHON File Not Found");
define("ERROR_ZAISEKI_FILE_NOT_FOUND", "ZAISEKI File Not Found");
define("ERROR_HENKAN_FILE_NOT_FOUND", "HENKAN File Not Found");
define("ERROR_GID_KIHON_FILE_NOT_FOUND", "GID_KIHON File Not Found");
define("ERROR_GID_SYOZOKU_FILE_NOT_FOUND", "GID_SYOZOKU File Not Found");
define("ERROR_CREATE_TID_LIST", "Failed to create TID_LIST");
define("ERROR_CREATE_GID_LIST", "Failed to create GID_LIST");
define("ERROR_TID_LIST_DOSE_NOT_EXIT", "TID_LIST does not exist");
define("ERROR_GID_LIST_DOSE_NOT_EXIT", "GID_LIST does not exist");
define("ERROR_CREATE USER_LIST", "Failed to create USER_LIST");
define("ERROR_IDE_NOT_FOUND", "ID not found");
define("ERROR_MUITIPLE_START_UP", "Multiple Start up");
define("ERROR_PROCESS_KILL", "Process Kill");

class AppDailyCreateCsv extends AppFrame {

	/*
	 * ファイルパスについてはconfig/config.inc.phpで定義しておくこと
	 * WIDS_KIHON_PATH : 基本情報
	 * WIDS_ZAISEKI_PATH : 原籍情報
	 * WIDS_HENKAN_PATH : ID変換情報ファイル
	 * WIDS_GID_KIHON_PATH :ID基本情報ファイル
	 * WIDS_GID_SYOZOKU_PATH :所属情報ファイル
	 * TID_LIST_PATH :TID_LISTファイル
	 * WIDS_GID_LIST_PATH :GID_LISTファイル
	 * USER_LIST_PATH:USER_LISTファイル
	 * WIDS_USER_LIST_FLG_PATH:USER_LIST作成日書込みファイル
	 *
	 * ユーザキーも定義しておくこと
	 * ADMIN_USER_KEY : ユーザキー(利用ログでも使用する)
	 * MEMBER_PASSWORD : メンバーパスワード
	 *
	 * 実行時、引数があれば月次処理とする
	 *
	 * FTP用の設定もconfig/config.inc.phpで設定する(5ファイルそれぞれ設定すること)
	 * FTP_USEFLG :FTP使用有無(0:使用しない 1:使用する)
	 * FTP_SERVER_KIHON : FTP server ドメイン or IP
	 * FTP_SERVER_PORT_KIHON : ポート
	 * FTP_SERVER_ID_KIHON : ログインID
	 * FTP_SERVER_PW_KIHON : ログインパスワード
	 * FTP_SERVER_SYOZOKU : FTP server ドメイン or IP
	 * FTP_SERVER_PORT_SYOZOKU : ポート
	 * FTP_SERVER_ID_SYOZOKU : ログインID
	 * FTP_SERVER_PW_SYOZOKU : ログインパスワード
	 * FTP_SERVER_HENKAN : FTP server ドメイン or IP
	 * FTP_SERVER_PORT_HENKAN : ポート
	 * FTP_SERVER_ID_HENKAN : ログインID
	 * FTP_SERVER_PW_HENKAN : ログインパスワード
	 * FTP_SERVER_GID_KIHON : FTP server ドメイン or IP
	 * FTP_SERVER_PORT_GID_KIHON : ポート
	 * FTP_SERVER_ID_GID_KIHON : ログインID
	 * FTP_SERVER_PW_GID_KIHON : ログインパスワード
	 * FTP_SERVER_GID_SYOZOKU : FTP server ドメイン or IP
	 * FTP_SERVER_PORT_GID_SYOZOKU : ポート
	 * FTP_SERVER_ID_GID_SYOZOKU : ログインID
	 * FTP_SERVER_PW_GID_SYOZOKU : ログインパスワード
	 * FTP_SERVER_WIDS_KIHON_PATH : ID基本リストのパス
	 * FTP_SERVER_WIDS_ZAISEKI_PATH : ID原籍リストのパス
	 * FTP_SERVER_WIDS_HENKAN_PATH : ID変換リストのパス
	 * FTP_SERVER_WIDS_GID_KIHON_PATH : GID_ID基本リストのパス
	 * FTP_SERVER_WIDS_GID_SYOZOKU_PATH : GID_ID原籍リストのパス
	 *
	 */

	function __construct($argv) {
		parent::__construct ();
	}
	function __destruct() {
	}
	function init() {
		$this->dsn = $this->get_dsn ();
		$this->obj_class_pis_daily_createCSV = new AppDailyCsvClass($this->dsn);
		$this->logger  = & EZLogger::getInstance();
		$this->logger2 = & EZLogger2::getInstance();
	}
	function default_view() {
		$this->logger2->info ( "start:" . time () );
		$this->logger2->info ( "start_memory:" . memory_get_usage () );

		//多重起動チェック
		$multipleflg = null;
		$multipleflg = $this->obj_class_pis_daily_createCSV->multipleCeheck();

		//多重起動時は停止
		if($multipleflg != 1 && $multipleflg == ERROR_PROCESS_KILL){
			$this->error_log($multipleflg,false);
		}elseif($multipleflg != 1 && $multipleflg == ERROR_MUITIPLE_START_UP){
			$this->error_log($multipleflg,true);
		}

		if(FTP_USEFLG == 1){
			// 基本情報ファイル取得
			$this->_ftp_file_get(FTP_SERVER_WIDS_KIHON_PATH , WIDS_KIHON_PATH);

			// 原籍情報ファイル取得
			$this->_ftp_file_get(FTP_SERVER_WIDS_ZAISEKI_PATH , WIDS_ZAISEKI_PATH);

			//変換情報ファイル
			$this->_ftp_file_get(FTP_SERVER_WIDS_HENKAN_PATH , WIDS_HENKAN_PATH);
		}

		// TID_LIST作成
		// KIHON、ZAISEKIをマージする。
		// HENKANを参照し、統合IDが一致する場合はグローバルIDを入れる
		$Tidflg = null;
		$Tidflg = $this->obj_class_pis_daily_createCSV->create_tid_list();

		//ID無しの場合は処理継続、それ以外は停止
		if($Tidflg != 1 && $Tidflg == ERROR_IDE_NOT_FOUND){
			$this->error_log($Tidflg,false);
		}elseif ($Tidflg != 1){
			$this->error_log($Tidflg,true);
		}

		if (FTP_USEFLG == 1) {
			// グローバル基本情報ファイル取得
			$this->_ftp_file_get ( FTP_SERVER_WIDS_GID_KIHON_PATH, WIDS_GID_KIHON_PATH );

			// グローバル原籍情報ファイル取得
			$this->_ftp_file_get ( FTP_SERVER_WIDS_GID_SYOZOKU_PATH, WIDS_GID_SYOZOKU_PATH );
		}

		// GID_LIST作成
		$Gidflg = null;
		$Gidflg = $this->obj_class_pis_daily_createCSV->create_gid_list();
		//ID無しの場合は処理継続、それ以外は停止
		if($Gidflg != 1 && $Gidflg == ERROR_IDE_NOT_FOUND){
			$this->error_log($Gidflg,false);
		}elseif ($Gidflg != 1){
			$this->error_log($Gidflg,true);
		}

		// TID_LIST作成、GID_LIST作成からUSER_LIST作成
		$Userflg = null;
		$Userflg = $this->obj_class_pis_daily_createCSV->create_user_list();
		//ID無しの場合は処理継続、それ以外は停止
		if($Userflg != 1 && $Userflg == ERROR_IDE_NOT_FOUND){
			$this->error_log($Userflg,false);
		}elseif ($Userflg != 1){
			$this->error_log($Userflg,true);
		}

		//正常終了時のみ処理結果書込み
		$this->obj_class_pis_daily_createCSV->result_write();

		$this->logger2->info ( "end:" . time () );
		$this->logger2->info ( "end_memory:" . memory_get_usage () );
	}

	/*
	 * FTPでIDリストを取得する
	 */
	private function _ftp_file_get($server_file_path, $local_file_path) {
		//ftp取得情報
		$errrmsg = null;
		$ftpserver = null;
		$ftpserverport = null;
		$ftpserverid = null;
		$ftpserverpw = null;

		switch ($server_file_path) {
			case FTP_SERVER_WIDS_KIHON_PATH:
				$errrmsg =ERROR_FTP_WIDS_KIHON;
				$ftpserver = FTP_SERVER_KIHON;
				$ftpserverport = FTP_SERVER_PORT_KIHON;
				$ftpserverid = FTP_SERVER_ID_KIHON;
				$ftpserverpw = FTP_SERVER_PW_KIHON;
				break;
			case FTP_SERVER_WIDS_ZAISEKI_PATH:
				$errrmsg =ERROR_FTP_WIDS_ZAISEKI;
				$ftpserver = FTP_SERVER_ZAISEKI;
				$ftpserverport = FTP_SERVER_PORT_ZAISEKI;
				$ftpserverid = FTP_SERVER_ID_ZAISEKI;
				$ftpserverpw = FTP_SERVER_PW_ZAISEKI;
				break;
			case FTP_SERVER_WIDS_HENKAN_PATH:
				$errrmsg =ERROR_FTP_WIDS_HENKAN;
				$ftpserver = FTP_SERVER_HENKAN;
				$ftpserverport = FTP_SERVER_PORT_HENKAN;
				$ftpserverid = FTP_SERVER_ID_HENKAN;
				$ftpserverpw = FTP_SERVER_PW_HENKAN;
				break;
			case FTP_SERVER_WIDS_GID_KIHON_PATH:
				$errrmsg =ERROR_FTP_WIDS_GID_KIHON;
				$ftpserver = FTP_SERVER_GID_KIHON;
				$ftpserverport = FTP_SERVER_PORT_GID_KIHON;
				$ftpserverid = FTP_SERVER_ID_GID_KIHON;
				$ftpserverpw = FTP_SERVER_PW_GID_KIHON;
				break;
			case FTP_SERVER_WIDS_GID_SYOZOKU_PATH:
				$errrmsg =ERROR_FTP_WIDS_GID_SYOZOKU;
				$ftpserver = FTP_SERVER_GID_SYOZOKU;
				$ftpserverport = FTP_SERVER_PORT_GID_SYOZOKU;
				$ftpserverid = FTP_SERVER_ID_GID_SYOZOKU;
				$ftpserverpw = FTP_SERVER_PW_GID_SYOZOKU;
				break;
			default :
				break;
		}

		$ftp = ftp_connect ( $ftpserver, $ftpserverport );
		ftp_login ( $ftp, $ftpserverid, $ftpserverpw );

		//FTP取得失敗時はエラーログを出力するが、後続処理は前日ファイルで実施
		if (! $result = ftp_get ( $ftp, $local_file_path, $server_file_path, FTP_ASCII )) {
			$this->error_log ( $errrmsg, false );
		}
		ftp_close ( $ftp );
		return $result;
	}

	private function _log($string, $result_log_dir, $error_flg = null) {
		if ($error_flg === "error") {
			$log_prefix = "daily_id_error_list";
		} else {
			$log_prefix = "daily_id_success_list";
		}
		$log_file = sprintf ( "%s/%s_%s.log", $result_log_dir, $log_prefix, date ( 'Ymd', time () ) );
		$fp = fopen ( $log_file, "a" );

		if ($fp) {
			fwrite ( $fp, $string . "\n" );
		}
		fclose ( $fp );
	}

	/**
	 * エラーログ出力用
	 * @param  [type] $value    エラーコード情報
	 * @param  [type] $exit_flg 処理強制終了判定
	 * @param  [type] $table_error_messages  各テーブルのエラー発生時
	 */
	function error_log ($value,$exit_flg=null,$table_error_messages=null){
		switch($value){
			case ERROR_FTP_WIDS_KIHON :
				$error_code = "VCUBE_001_001"; // FTP取得失敗
				break;
			case ERROR_FTP_WIDS_ZAISEKI :
				$error_code = "VCUBE_001_002"; // FTP取得失敗
				break;
			case ERROR_FTP_WIDS_HENKAN :
				$error_code = "VCUBE_001_003"; // FTP取得失敗
				break;
			case ERROR_FTP_WIDS_GID_KIHON :
				$error_code = "VCUBE_001_004"; // FTP取得失敗
				break;
			case ERROR_FTP_WIDS_GID_SYOZOKU :
				$error_code = "VCUBE_001_005"; // FTP取得失敗
				break;
			case ERROR_KIHON_FILE_NOT_FOUND :
				$error_code = "VCUBE_001_006"; // ファイル読込み失敗
				break;
			case ERROR_ZAISEKI_FILE_NOT_FOUND :
				$error_code = "VCUBE_001_007"; // ファイル読込み失敗
				break;
			case ERROR_HENKAN_FILE_NOT_FOUND :
				$error_code = "VCUBE_001_008"; // ファイル読込み失敗
				break;
			case ERROR_GID_KIHON_FILE_NOT_FOUND :
				$error_code = "VCUBE_001_009"; // ファイル読込み失敗
				break;
			case ERROR_GID_SYOZOKU_FILE_NOT_FOUND :
				$error_code = "VCUBE_001_010"; // ファイル読込み失敗
				break;
			case ERROR_CREATE_TID_LIST :
				$error_code = "VCUBE_001_011"; // TID_LIST作成失敗
				break;
			case ERROR_CREATE_GID_LIST :
				$error_code = "VCUBE_001_012"; // GID_LIST作成失敗
				break;
			case ERROR_TID_LIST_DOSE_NOT_EXIT :
				$error_code = "VCUBE_001_013"; // ファイル読込み失敗
				break;
			case ERROR_GID_LIST_DOSE_NOT_EXIT :
				$error_code = "VCUBE_001_014"; // ファイル読込み失敗
				break;
			case ERROR_CREATE_USER_LIST :
				$error_code = "VCUBE_001_015"; // USER_LIST作成失敗
				break;
			case ERROR_IDE_NOT_FOUND :
				$error_code = "VCUBE_001_016"; // ID無しデータ読込み
				break;
			case ERROR_MUITIPLE_START_UP :
				$error_code = "VCUBE_001_017"; // 多重起動
				break;
			case ERROR_PROCESS_KILL :
				$error_code = "VCUBE_001_018"; // プロセスkill
				break;
			default:
				// 例外エラー
				$value = "VCUBE_unknown error";
				break;
		}

		$this->logger2->error($error_code . " [ ".$value ." ]");
		if($exit_flg){
			// エラーコードつきの終了
			exit($error_code);
		}
	}
}
$main = new AppDailyCreateCsv($argv[1]);
$main->execute ();
