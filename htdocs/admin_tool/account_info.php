<?php

// 環境設定
require_once("bin/set_env.php");
require_once("classes/AppFrame.class.php");

// ユーザ情報
require_once ('classes/mgm/MGM_Auth.class.php');
require_once("classes/dbi/user.dbi.php");

class AppAccountInfo extends AppFrame {


    // 利用開始用アカウント情報取得
    private $account_data = null;

    function init()
    {
    }


    /**
     * default view routine
     *
     */
    function default_view()
    {

        $this->logger->info(__FUNCTION__." # Account Info Operation Start ... ", __FILE__, __LINE__);

        // 初期化
        $status = 0;
        $data = array();

        // パラメタ取得
        $user_id = $this->request->get("i1");

        // パラメタ取得
        $action = $this->request->get("i2");

        //ユーザー取得
        if ($user_id){
            $account_data = $this->get_account_info($user_id);
            if ($account_data){
                $status = 1;
                //ユーザー取得
                if ($action == "starting_notification" ){
                    $data = $this->set_starting_notification_data($account_data);
                } elseif ($action == "add_client" ){
                    $data = $this->set_add_client_data($account_data);
                } else {
                    $data = $account_data;
                }
            } else {
                $data = null;
            }
        } else {
            $data = null;
        }

        // 戻り値編集
        $account_data = array(
            "status" => $status,
            "data" => $data,
        );

        print_r(serialize($account_data));

        $this->logger->info(__FUNCTION__." # Account Info Operation End ... ", __FILE__, __LINE__);
    }


    /**
     * get_account_info
     *
     */
    function get_account_info($user_id=null)
    {
        // 初期化
        $data = null;

        //ユーザ取得
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        if ( ! $user_info = $obj_MGMClass->getUserInfoById( $user_id ) ){
            $this->logger2->warn($user_id, "User Info Not Found");
            return false;
        } else {
            $this->logger->info(__FUNCTION__." # UserInfo ... ", __FILE__, __LINE__, $user_info);
        }
        //サーバー取得
        if ( ! $server_info = $obj_MGMClass->getServerInfo( $user_info["server_key"] ) ){
            $this->logger->info(__FUNCTION__." # User Server Not Found ... ", __FILE__, __LINE__, "server_key = " . $user_info["server_key"]);
        } else {
            $this->logger->info(__FUNCTION__." # UserServerInfo ... ", __FILE__, __LINE__, $server_info);
            // 該当ユーザーデータ取得
            $user_obj = new UserTable($server_info['dsn']);
            $user_where = " `user_id` = '" . $user_id . "'" .
                          " AND `invoice_flg` = 1 "; // 請求対象のアカウントのみとする。
            if ( ! $user_detail = $user_obj->getRow( $user_where ) ){
                $this->logger->info(__FUNCTION__." # User Detail Not Found ... ", __FILE__, __LINE__, "user_key = " . $user_id);
            } else {
                $this->logger->info(__FUNCTION__." # UserDetail ... ", __FILE__, __LINE__, $user_detail);
                $data = $user_detail;
            }
        }
        return $data;
    }


    /**
     * set_starting_notification_data
     *
     */
    function set_starting_notification_data($user_info=null)
    {

        // 初期化
        $ret = array(
            "ap" => null, // user_password
            "aap" => null, // user_admin_password
            "start_date" => null, // user_starttime
        );

        // 編集
        $ret['ap'] = $user_info['user_password'];
        $ret['aap'] = $user_info['user_admin_password'];
        $ret['start_date'] = $user_info['user_starttime'];

        //
        return $ret;

    }


    /**
     * set_add_client_data
     *
     */
    function set_add_client_data($user_info=null)
    {

        // 初期化
        /*
        $ret = array(
            "country_id" => null,
            "name" => null,
            "zip" => null,
            "state" => null,
            "city" => null,
            "address" => null,
            "tel" => null,
            "fax" => null,
            "staff_department" => null,
            "staff_name" => null,
            "staff_mail" => null,
        );
        */

        // 編集
        
        $user_info['country_id'] = $user_info['country_key'];
        $user_info['name'] = $user_info['user_company_name'];
        $user_info['zip'] = $user_info['user_company_postnumber'];
        $user_info['state'] = $user_info['user_company_address'];
        $user_info['city'] = "";
        $user_info['address'] = "";
        $user_info['tel'] = $user_info['user_company_phone'];
        $user_info['fax'] = $user_info['user_company_fax'];
        $user_info['staff_department'] = $user_info['user_staff_department'];
        $user_info['staff_name'] = $user_info['user_staff_lastname'] . "　" . $user_info['user_staff_firstname'];
        $user_info['staff_mail'] = $user_info['user_staff_email'];

        //
        return $user_info;

    }


}

$main = new AppAccountInfo();
$main->execute();
?>