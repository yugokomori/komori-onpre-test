<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once ('classes/AppFrame.class.php');
require_once ('classes/mgm/dbi/user.dbi.php');
require_once("classes/N2MY_Account.class.php");
require_once ('classes/dbi/member.dbi.php');
require_once ('classes/dbi/member_group.dbi.php');
require_once ('classes/dbi/member_status.dbi.php');
require_once ('classes/dbi/user.dbi.php');
require_once ('config/config.inc.php');
require_once ('lib/EZLib/EZUtil/EZEncrypt.class.php');
require_once( "lib/EZLib/EZCore/EZLogger2.class.php" );
require_once ('lib/EZLib/EZUtil/EZCsv.class.php');

class AppMember extends AppFrame
{
    var $logger = null;
    var $_name_space = null;
    var $memberTable = null;
    var $groupTable = null;
    var $statusTable = null;
    var $account_dsn = null;
    var $max_address_cnt = null;

    function init() {
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->memberTable = new MemberTable($this->get_dsn());
        $this->groupTable = new MemberGroupTable($this->get_dsn());
        $this->_name_space = md5(__FILE__);
        $this->max_address_cnt  = 50000;
    }

    /**
     * ログイン認証
     */
    function auth() {
        $this->checkAuth();
        $this->checkAdminAuth('admin/session_error.t.html');
    }
    /**
     * デフォルト処理
     */
    function default_view() {
        $this->logger->trace("test", __FILE__, __LINE__, __FUNCTION__);
        $this->action_show_top();
    }

    function action_show_top()
    {
        $this->display('admin/member/index.t.html');
    }

    function action_admin_member($message = ""){
        $this->session->remove("conditions");
        $this->render_admin_member();
    }

    function action_search_member() {
        $this->setCond();
        $this->render_admin_member();
    }

    function action_pager() {
        $this->render_admin_member();
    }

    function render_admin_member($message = ""){

        if($message){
            $this->template->assign('message', $message);
        }
        //user_key取得
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];
        // グループ一覧
        $where = "user_key = ".addslashes($user_key);
        $group_list = $this->groupTable->getRowsAssoc($where);
        $this->template->assign('group_list', $group_list);
        //user_keyからメンバーを取得
        $conditions = $this->getCond();
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$conditions);
        $sort_key  = ($conditions["sort_key"]) ? $conditions["sort_key"] : "member_id";
        $sort_type = ($conditions["sort_type"]) ? $conditions["sort_type"] : "asc";
        $sort_key_rules = array("member_name","member_id","expense_department_code","accounting_unit_code");
        if (!EZValidator::valid_allow($sort_key, $sort_key_rules)) {
            $sort_key = "member_name";
        }
        $sort_type_rules = array("asc","desc");
        if (!EZValidator::valid_allow($sort_type, $sort_type_rules)) {
            $sort_type = "desc";
        }
        $limit = $this->request->get("page_cnt");
        if (!$limit) {
            $limit = 10;
        }
        $page      = $this->request->get("page", 1);
        $offset    = ($page - 1) * $limit;
        // 条件
        $where = "user_key = ".addslashes($user_key).
            " AND member_status > -1";
        if ($conditions["member_group"]) {
            $where .= " AND member_group = ".addslashes($conditions["member_group"]);
        }
        if ($conditions["member_name"]) {
            $where .= " AND (member_name like '%".addslashes($conditions["member_name"])."%'";
            $where .= " OR member_id like '%".addslashes($conditions["member_name"])."%'";
            $where .= " OR expense_department_code like '%".addslashes($conditions["member_name"])."%'";
            $where .= " OR accounting_unit_code like '%".addslashes($conditions["member_name"])."%')";
        }
        if($this->session->get("service_mode") == "sales") {
          $where .= " AND outbound_id IS NOT NULL";
        } else {
          $where .= " AND (outbound_id IS NULL OR outbound_id = '')";
        }
        $where .= " AND is_admin = 1";
        $member_list = $this->memberTable->getRowsAssoc($where, array($sort_key => $sort_type), $limit, $offset);
        $count = $this->memberTable->numRows($where);
        $obj_MgmUserTable = new MgmUserTable( $this->get_auth_dsn() );
        $member_info = array();
        if ($user_info['account_model'] == 'member') {
            if ($conditions['month'] == 'prev') {
                // 前月
                $condition = array(
                    'actual_start_datetime' => array(
                        'min' => date('Y-m-01 00:00:00', strtotime("-1 Month")),
                        'max' => date('Y-m-01 00:00:00'),
                    )
                );
            } else {
                // 当月
                $condition = array(
                    'actual_start_datetime' => array(
                        'min' => date('Y-m-01 00:00:00'),
                        'max' => date('Y-m-01 00:00:00', strtotime("+1 Month")),
                    )
                );
            }
            require_once("classes/dbi/member_usage_details.dbi.php");
            $objMemberUseLog = new DBI_MemberUsageDetails($this->get_dsn());
            $use_count = $objMemberUseLog->getSummaryUseCount($user_key, $condition);
            $this->template->assign('total_use_count', $use_count);
        }
        foreach( $member_list as $member ){
            if ($user_info['account_model'] == 'member') {
                $condition['member_key'] = $member['member_key'];
                $use_count = $objMemberUseLog->getSummaryUseCount($user_key, $condition);
            }
            $member_info[] = array(
                            "auth_user_key" => $obj_MgmUserTable->getOne( sprintf( "user_id='%s'", $member["member_id"] ), "user_key" ),
                            "group"         => $this->_getMemberGroupName( $member['member_group'] ),
                            "status"        => $this->_getMemberStatusName( $member['member_status'] ),
                            "key"           => $member['member_key'],
                            "id"            => $member['member_id'],
                            "expense_department_code" => $member['expense_department_code'],
                            "accounting_unit_code" => $member['accounting_unit_code'],
                            "name"          => $member['member_name'],
                            "type"          => $member['member_type'],
                            "use_count"     => $use_count
                            );
        }
        $add_flg = ( ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") &&
            $user_info["max_member_count"] <= count( $member_info ) ) ? false : true ;
        $this->template->assign("add_flg", $add_flg );
        $this->template->assign('info', $member_info);
        $this->template->assign('vcubeid_add_use_flg', $user_info["vcubeid_add_use_flg"]);
        // ページング
        $this->template->assign('conditions', $conditions);
        $pager = $this->setPager($limit, $page, $count);
        $this->template->assign('pager', $pager);
        $this->display('admin/member/administrator/index.t.html');
    }

    function setCond() {
        $conditions  = $this->request->get("conditions");
        if ($conditions) {
            $this->session->set("conditions", $conditions);
        }
    }

    function getCond() {
        $condition = $this->session->get("conditions");
        return $condition;
    }

    function action_change_vcubeid_flg() {
        $user_info = $this->session->get("user_info");
        $vcubeid_use_flg = $this->request->get("vcubeid_add_use_flg");
        $user_info["vcubeid_add_use_flg"] = $vcubeid_use_flg;
        $objUser = new UserTable($this->get_dsn());
        $where = "user_key = ". $user_info["user_key"];
        $data = array("vcubeid_add_use_flg" => $vcubeid_use_flg );
        $objUser->update($data, $where);
        $this->session->set("user_info", $user_info);
        $this->action_admin_member();
    }

    function action_add_member($message = "")
    {
        $member_info = ( 1 == $this->request->get( "action_add_member") ) ? "" : $this->session->get('new_member_info');
        //user_key取得
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];

        $obj_MemberStatus = new MemberStatusTable( $this->get_auth_dsn() );
        $group_list = $this->groupTable->getRowsAssoc( sprintf( "user_key='%s'", $user_key ) );
        foreach( $group_list as $group_info ){
            $member_group[] = array(
                                "key" => $group_info['member_group_key'],
                                "name" => $group_info['member_group_name']
                                );
        }

        $member_status_list = $obj_MemberStatus->getRowsAssoc();
        foreach( $member_status_list as $status_info) {
                $member_status[] = array(
                    "key"     => $status_info['member_status_key'],
                    "name"     => $status_info['member_status_name']
                );
        }

        //エラー時にユーザインプットの再表示
        if( $member_info ){
            foreach($member_info as $key => $value){
                $this->template->assign($key, $value);
            }
        }
        if($message){
            $this->template->assign('message', $message);
        }

        $this->template->assign('user_info', $user_info);
        $this->template->assign('member_group', $member_group);
        $this->template->assign('member_status', $member_status);
        $this->display('admin/member/administrator/add/index.t.html');
    }

    function action_add_member_confirm()
    {
        $this->set_submit_key($this->_name_space);
        // 入力値を保持
        $request = $this->request->getAll();
        $this->session->set('new_member_info', $request);
        $user_info = $this->session->get( "user_info");

        //$member_list = $this->memberTable->getRowsAssoc( sprintf( "user_key ='%s' and member_status > -1", $user_info['user_key'] ) );

        $message = null;
        $message = $this->check_member_info($request);
        if( $message ){
            $this->action_add_member($message);
        } else {
            foreach($request as $key => $value){
                if($key == "member_group_key"){
                    $this->template->assign('member_group_name', $this->_getMemberGroupName($value));
                }elseif($key == "member_status_key"){
                    $this->template->assign('member_status_name', $this->_getMemberStatusName($value));
                }elseif($key == "member_pass"){
                    $length = mb_strlen($value);
                    $value = "";
                    for($i=0;$i<$length;$i++){
                        $value .= "*";
                    }
                }else if($key == "timezone"){
                    if( 100 == $value ){
                        $timezone = MEMBER_UNDEFINED_TIMEZONE;
                    } else {
                        $timezoneList = $this->get_timezone_list();
                        foreach( $timezoneList as $k => $zone ){
                            if( $zone["key"] == $value ){
                                $timezone = $zone["value"];
                                break 1;
                            }
                        }
                    }
                    $this->template->assign( 'timezoneInfo', $timezone );
                }
                $this->template->assign($key, $value);
            }
            $this->display('admin/member/administrator/add/confirm.t.html');
        }
    }
    public function action_add_member_csv() {
        $this->template->assign('max_address_cnt',  $this->max_address_cnt);
        $this->display('admin/member/administrator/csv_index.t.html');
    }

    function action_format_download() {
        $output_encoding = $this->get_output_encoding();
        $csv = new EZCsv(true,$output_encoding,"UTF-8");
        $dir = $this->get_work_dir();
        $tmpfile = tempnam($dir, "csv_");
        $csv->open($tmpfile, "w");

        header('Content-Type: application/octet-stream;');
        //header('Content-Disposition: attachment; filename="sample_member.csv"');
        header('Content-Disposition: attachment; filename="sample_administrator_id.csv"');

        // ヘッダ
        //$header = array("member_id","expense_department_code","accounting_unit_code", "member_pass", "name", "name_kana", "email", "timezone", "lang(ja.en)","flag","error");
        //$sample = array("1234567","1qa","2ws","vcube201307","ＳＡＭＰＬＥ","sample","vcube@example.jp","9","en","A","This is a sample data. Please delete.");
        $header = array("administrator_id", "administrator_pass", "name","flag","error");
        $sample = array("adminid","adminpassword","太郎","A","This is a sample data. Please delete.");
        $this->logger2->debug(__FUNCTION__, __FILE__, __LINE__, $header);
        $csv->setHeader($header);
        $csv->write($header);
        $csv->write($sample);
        $csv->close();
        $fp = fopen($tmpfile, "r");
        $contents = fread($fp, filesize($tmpfile));
        fclose($fp);
        print $contents;
        unlink($tmpfile);
    }

    public function action_import_member_csv() {
        $request["upload_file"] = $_FILES["import_file"];

        $data = pathinfo($request["upload_file"]["name"]);
        //CSVファイルチェック
        if (empty($data['filename'])) {
            $message .= CSV_NO_FILE_ERROR;
            return $this->action_import_complete($message);
        } elseif($data['extension'] != "csv") {
            $message .= CSV_FILE_FORMAT_ERROR ;
            return $this->action_import_complete($message);
        }
        $filename = $this->get_work_dir()."/".tmpfile();
        move_uploaded_file($request["upload_file"]["tmp_name"], $filename);
        $request["upload_file"]["tmp_name"] = $filename;
        $output_encoding = $this->get_output_encoding();
        $csv = new EZCsv(true,$output_encoding,"UTF-8");
        $csv->open($filename, "r");
        //取り込んだファイルのヘッダチェック
        //$head_ex = "member_id,expense_department_code,accounting_unit_code,member_pass,name,name_kana,email,timezone,lang(ja.en),flag,error";
        $head_ex = "administrator_id,administrator_pass,name,flag,error";
        $h_ar = implode(",", $csv->getHeader());
        $this->logger2->info($h_ar);
        if($head_ex !== $h_ar) {
            $message .= CSV_FILE_FORMAT_ERROR;
            return $this->action_import_complete($message);
        }
        $rows = array();
        $count = 0;
        while($row = $csv->getNext(true)) {
//             $row["lang"] = $row["lang(ja.en)"];
//             unset($row["lang(ja.en)"]);
//             if($row["timezone"] == "")
//               $row["timezone"] = "100";
            $data["member_id"] = $row["administrator_id"];
            $data["member_pass"] = $row["administrator_pass"];
            $data["name"] = $row["name"];
            $data["is_admin"] = 1;
            $data["flag"] = $row["flag"];
            $rows[] = $data;
            $count++;
        }
        $csv->close();
        unlink($filename);
        //記録メンバー数チェック
        if(empty($rows)) {
            $message .= CSV_NO_ITEM_ERROR;
            return $this->action_import_complete($message);
        } elseif($count > $this->max_address_cnt) {
            $message .= CSV_LIMIT_ERROR;
            return $this->action_import_complete($message);
        }

        //追加・編集・削除処理
        $user_info = $this->session->get( "user_info" );
        $result_array = array();
        $error_flg = 0;
        $this->logger2->info($rows);
        foreach($rows as $member_row) {
            $member_row["storage_flg"] = "Y";
            switch($member_row["flag"]) {
                case "X":
                    break;
                case "A":
                    if(!$this->_isExist($member_row["member_id"])){
                        //追加こちから
                        if ($error_message = $this->_isMemberInfo($member_row)){
                            //入力情報をバリデーションエラー
                            $error_msg = "";
                            foreach($error_message as $msg) {
                                $error_msg .= $msg." ";
                            }
                            $member_row[error] = $error_msg;
                            $error_flg = 1;
                        } elseif ($user_info["max_member_count"] != 0 && $this->_getMemberCount($user_info["user_key"]) >= $user_info["max_member_count"]) {
                            //契約上限値越えた
                            $member_row[error] = MEMBER_ERROR_OVER_COUNT;
                            $error_flg = 1;
                        } else {
                            //追加処理
                            if($member_row["group_name"]) {
                                $group_key = $this->_isGroupExist($member_row["group_name"],$user_info["user_key"],true);
                                $member_row["member_group"] = $group_key;
                            }
                            $add_data = array(
                                "user_key"           => $user_info["user_key"],
                                "member_id"          => $member_row['member_id'],
                                "is_admin"          => $member_row['is_admin'],
                                "expense_department_code" => $member_row['expense_department_code'],
                                "accounting_unit_code"    => $member_row['accounting_unit_code'],
                                "member_pass"        => EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $member_row['member_pass']),
                                "member_email"       => $member_row['email'],
                                "member_name"        => $member_row['name'],
                                "member_name_kana"   => $member_row['name_kana'],
                                "member_status"      => "0",
                                "member_type"        => (($member_row['member_type']) ? $member_row['member_type'] : ""),
                                "member_group"       => $member_row['member_group']?$member_row['member_group']:0,
                                "timezone"           => $member_row['timezone'],
                                "lang"               => $member_row['lang'],
                                "use_shared_storage" => $member_row['storage_flg'] == "Y"? 1:0,
                            );
                            $this->logger2->info($add_data);
                            $ret = $this->memberTable->add( $add_data );
                            if (DB::isError($ret)) {
                                $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                            } else {
                                $this->logger->info(__FUNCTION__."#add adminstrator successful!",__FILE__,__LINE__,$temp);
                            }
                            //認証サーバーへ追加
                            $obj_MgmUserTable = new MgmUserTable( N2MY_MDB_DSN );
                            $session = $this->session->get( "server_info" );
                            $auth_data = array(
                                "user_id"     => $member_row['member_id'],
                                "server_key"    => $session["server_key"]
                            );
                            $result = $obj_MgmUserTable->add( $auth_data );
                            if (DB::isError($result)) {
                                $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$result->getUserInfo());
                            } else {
                                $this->logger->info(__FUNCTION__."#add user_id successful!",__FILE__,__LINE__,$auth_data);
                            }
                            //メンバー課金対応
                            //部屋追加
                            if( $user_info["account_model"] == "member" || $user_info["account_model"] == "centre" ){
                              require_once( "classes/dbi/room.dbi.php" );
                              require_once( "classes/dbi/user_plan.dbi.php" );
                              require_once( "classes/dbi/room_plan.dbi.php" );
                              $obj_Room = new RoomTable( $this->get_dsn() );
                              $objUserPlan = new UserPlanTable( $this->get_dsn() );
                              $objRoomPlanTable = new RoomPlanTable( $this->get_dsn() );
                              if ($now_plan_info = $objUserPlan->getRow("user_key='".addslashes($user_info["user_key"])."' AND user_plan_status = 1")) {
                                $service_db = new N2MY_DB( N2MY_MDB_DSN, "service" );
                                $service_info = $service_db->getRow("service_key = '".addslashes($now_plan_info['service_key'])."' AND service_status = 1");
                                $this->logger2->debug(array($now_plan_info, $service_info));
                              }
                              $where = sprintf( "user_key=%d", $user_info["user_key"] );
                              $count = $obj_Room->numRows($where);
                              $room_key = $user_info["user_id"]."-".($count + 1)."-".substr( md5( uniqid( time(), true ) ), 0, 4 );
                              $room_sort = $count + 1;
                              //部屋登録
                              $room_data = array (
                                  "room_key"              => $room_key,
                                  "max_seat"              => ($service_info["max_seat"]) ? $service_info["max_seat"] : 10,
                                  "max_audience_seat"     => ($service_info["max_audience_seat"]) ? $service_info["max_audience_seat"] : 0,
                                  "max_whiteboard_seat"   => ($service_info["max_whiteboard_seat"]) ? $service_info["max_whiteboard_seat"] : 0,
                                  "max_room_bandwidth"    => ($service_info["max_room_bandwidth"]) ? $service_info["max_room_bandwidth"] : "2048",
                                  "max_user_bandwidth"    => ($service_info["max_user_bandwidth"]) ? $service_info["max_user_bandwidth"] : "256",
                                  "min_user_bandwidth"    => ($service_info["min_user_bandwidth"]) ? $service_info["min_user_bandwidth"] : "30",
                                  "user_key"              => $user_info["user_key"],
                                  "room_name"             => $member_row['name'],
                                  "room_sort"             => $room_sort,
                                  "room_status"           => "1",
                                  "room_registtime"       => date("Y-m-d H:i:s"),
                              );
                              $this->logger2->info($room_data);
                              $add_room = $obj_Room->add($room_data);
                              if (DB::isError($add_room)) {
                                $this->logger2->info( $add_room->getUserInfo());
                              }
                              //ユーザープランからroom_planへコピー
                              $planList = $objUserPlan->getRowsAssoc(sprintf("user_key='%s'", $user_info["user_key"]));
                              for( $i = 0; $i < count( $planList ); $i++ ){
                                $data = array(
                                    "room_key"=>$room_key,
                                    "service_key"=>$planList[$i]["service_key"],
                                    "discount_rate"=>$planList[$i]["discount_rate"],
                                    "contract_month_number"=>$planList[$i]["contract_month_number"],
                                    "room_plan_yearly"=>$planList[$i]["user_plan_yearly"],
                                    "room_plan_yearly_starttime"=>$planList[$i]["user_plan_yearly_starttime"],
                                    "room_plan_starttime"=>$planList[$i]["user_plan_starttime"],
                                    "room_plan_endtime"=>$planList[$i]["user_plan_endtime"],
                                    "room_plan_status"=>$planList[$i]["user_plan_status"],
                                    "room_plan_registtime"=>$planList[$i]["user_plan_registtime"],
                                    "room_plan_updatetime"=>$planList[$i]["user_plan_updatetime"]
                                );
                                $addPlan = $objRoomPlanTable->add( $data );
                                if (DB::isError( $addPlan ) ) {
                                  $this->logger2->info( $addPlan->getUserInfo() );
                                }
                              }
                              // userのプランオプション(user_service_option)からmemberの部屋のオプションを追加する(ordered_service_option)
                              $this->objMeetingDB = new N2MY_DB( $this->get_dsn(), "user_service_option" );
                              // $condition = sprintf( "user_key='%s' AND user_service_option_starttime<='%s' AND user_service_option_status=1", $user_info["user_key"], date( "Y-m-d" ) );
                              $condition = sprintf( "user_key='%s' AND service_option_key !=14", $user_info["user_key"] );
                              $optionList = $this->objMeetingDB->getRowsAssoc( $condition );
                              for( $i = 0; $i < count( $optionList ); $i++ ){
                                $data = array(
                                    "room_key" => $room_key,
                                    "user_service_option_key" => $optionList[$i]["user_service_option_key"],
                                    "service_option_key" => $optionList[$i]["service_option_key"],
                                    "ordered_service_option_status" => $optionList[$i]["user_service_option_status"],
                                    "ordered_service_option_starttime" => $optionList[$i]["user_service_option_starttime"],
                                    "ordered_service_option_registtime" => $optionList[$i]["user_service_option_registtime"],
                                    "ordered_service_option_deletetime" => $optionList[$i]["user_service_option_deletetime"]
                                 );
                                require_once("classes/dbi/ordered_service_option.dbi.php");
                                $ordered_service_option = new OrderedServiceOptionTable( $this->get_dsn() );
                                 $ordered_service_option->add( $data );
                              }
                              //部屋リレーション追加
                              $where = "user_key = ".$user_info["user_key"].
                              " AND member_id='".$member_row['member_id']."'";
                              $info = $this->memberTable->getRow( $where, "member_key" );
                              require_once("classes/dbi/member_room_relation.dbi.php");
                              $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
                              $data = array("member_key" => $info["member_key"],
                                  "room_key"   => $room_key,
                                  "create_datetime" => date("Y-m-d H:i:s"));
                              $this->logger2->debug($data);
                              $objRoomRelation->add($data);
                              if (DB::isError($ret)) {
                                $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                              }
                              $this->logger2->debug("容量追加");
                              //部屋keyメンバーrowに入れる
                              $update_data = array(
                                  "room_key" => $room_key,
                              );
                              $ret = $this->memberTable->update( $update_data, sprintf("member_key='%s'", $info["member_key"]));
                              if (DB::isError($ret)) {
                                $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                              } else {
                                $this->logger->info(__FUNCTION__."#add adminstrator successful!",__FILE__,__LINE__,$temp);
                              }
                              //容量追加
                               if ($now_plan_info = $objUserPlan->getRow("user_key='".addslashes($user_info["user_key"])."' AND user_plan_status = 1")) {
                                 $service_db = new N2MY_DB( N2MY_MDB_DSN, "service" );
                                 $service_info = $service_db->getRow("service_key = '".addslashes($now_plan_info['service_key'])."' AND service_status = 1");
                                 $this->logger2->info(array($now_plan_info, $service_info));
                                 $update_data = array(
                                    "max_storage_size" => intval($user_info["max_storage_size"]) + intval($service_info["member_storage_size"])
                                );
                                $update_where = "user_key='".addslashes($user_info["user_key"])."'";
                              }
                              //容量更新あるよって、user_infoも最新データに変更する
                              $obj_UserTable = new UserTable( $this->get_dsn() );
                              $obj_UserTable->update($update_data, $update_where);
                              $user_info = $obj_UserTable->getRow("user_key='".addslashes($user_info["user_key"])."'");
                              $this->session->set("user_info", $user_info);
                            }
                        }
                    } else {
                        //編集こちから
                        $member_key = $this->_isMemberExist($member_row["member_id"],$user_info["user_key"]);
                        if(!$member_key) {
                            //他ユーザーのメンバーがこのメンバーIDを使っています
                            $member_row[error] = MEMBER_ERROR_ID_EXIST;
                            $error_flg = 1;
                        } elseif ($error_message = $this->_isMemberInfo($member_row, "modify")) {
                            //入力情報をバリデーションエラー
                            $error_msg = "";
                            foreach($error_message as $msg) {
                                $error_msg .= $msg." ";
                            }
                            $member_row[error] = $error_msg;
                            $error_flg = 1;
                        } else if(!$this->memberTable->getOne("member_key ='".$member_key."'", "is_admin")) {
                            $member_row[error] = "同名のIDが存在しているため、管理者IDとして追加・変更・削除はできません。";
                            $error_flg = 1;
                        } else {
                            //更新処理
                            if($member_row["group_name"]) {
                                $group_key = $this->_isGroupExist($member_row["group_name"],$user_info["user_key"],true);
                                $member_row["member_group"] = $group_key;
                            }
                            $edit_data = array(
                                "is_admin"          => $member_row['is_admin'],
                                "expense_department_code" => $member_row['expense_department_code'],
                                "accounting_unit_code"    => $member_row['accounting_unit_code'],
                                "member_email"       => $member_row['email'],
                                "member_name"        => $member_row['name'],
                                "member_name_kana"   => $member_row['name_kana'],
                                "member_status"      => "0",
                                "member_type"        => (($member_row['member_type']) ? $member_row['member_type'] : ""),
                                "member_group"       => $member_row['member_group'],
                                "timezone"           => $member_row['timezone'],
                                "lang"               => $member_row['lang'],
                                "use_shared_storage" => $member_row['storage_flg'] == "Y"? 1:0,
                            );
                            if ($member_row['member_pass']) {
                                $edit_data['member_pass'] = EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $member_row['member_pass']);
                            }
                            $where =  sprintf( "member_key='%s'", $member_key );
                            $this->logger2->info($edit_data);
                            $result = $this->memberTable->update($edit_data, $where);
                            if (DB::isError($result)) {
                                $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$result->getUserInfo());
                            } else {
                                $this->logger->info(__FUNCTION__."#edit adminstrator successful!",__FILE__,__LINE__,$auth_data);
                            }
                        }
                    }
                    break;
                case "D":
                    //削除こちから
                    $member_key = $this->_isMemberExist($member_row["member_id"],$user_info["user_key"]);
                    if(!$member_key) {
                        //メンバーIDが使っていません
                        $member_row[error] = MEMBER_ERROR_DELETE;
                        $error_flg = 1;
                    } else if(!$this->memberTable->getOne("member_key ='".$member_key."'", "is_admin")) {
                        $member_row[error] = "同名のIDが存在しているため、管理者IDとして追加・変更・削除はできません。";
                        $error_flg = 1;
                    } else {
                        //削除処理
                        $delete_data['member_status']=-1;
                        $delete_data["member_id"] = $member_row["member_id"]."_delete_".date("YmdHi");
                        $delete_data["member_email"] = $member_row["email"]."_delete_".date("YmdHi");
                        $ret = $this->memberTable->update($delete_data, sprintf( "member_key = '%s'", $member_key ) );
                        if (DB::isError($ret)) {
                            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
                        } else {
                            $this->logger->info(__FUNCTION__."#delete adminstrator successful!",__FILE__,__LINE__,array(
                                "member_key" => $member_key)
                            );
                            $obj_MgmUserTable = new MgmUserTable( N2MY_MDB_DSN );
                            $delete_mgm_data["user_id"] = $delete_data["member_id"];
                            $result = $obj_MgmUserTable->update($delete_mgm_data,  sprintf("user_id='%s'", $member_row["member_id"] ));
                            if (DB::isError($result)) {
                                $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$result->getUserInfo());
                            } else {
                                $this->logger->info(__FUNCTION__."#delete user_id successful!",__FILE__,__LINE__,$auth_data);
                                if( $user_info["account_model"] == "member" || $user_info["account_model"] == "centre" ){
                                  $memberInfo =  $this->memberTable->getRow(sprintf( "member_key = '%s'", $member_key));
                                  $this->logger2->debug($memberInfo);
                                  require_once( "classes/dbi/room.dbi.php" );
                                  $where = sprintf( "room_key='%s'", $memberInfo["room_key"] );
                                  $this->logger2->debug($where, "make room inactive");
                                  $obj_Room = new RoomTable( $this->get_dsn() );
                                  $data = array( "room_status" => 0,
                                      "room_deletetime" => date("Y-m-d H:i:s") );
                                  $obj_Room->update( $data, $where );
                                  //room_plan -> inactive
                                  require_once( "classes/dbi/room_plan.dbi.php" );
                                  $objRoomPlanTable = new RoomPlanTable( $this->get_dsn() );
                                  $where = sprintf("room_key='%s'", $memberInfo["room_key"] );
                                  $data = array(
                                      "room_plan_status" => "0",
                                      "room_plan_updatetime" => date("Y-m-d H:i:s")
                                  );
                                  $deletePlan = $objRoomPlanTable->update( $data, $where );
                                  if (DB::isError( $deletePlan ) ) {
                                    $this->logger2->info( $deletePlan->getUserInfo() );
                                  }
                                  //容量減ると、セッションuser_info更新
                                  require_once( "classes/dbi/user_plan.dbi.php" );
                                  $objUserPlan = new UserPlanTable( $this->get_dsn() );
                                  if ($now_plan_info = $objUserPlan->getRow("user_key='".addslashes($user_info["user_key"])."' AND user_plan_status = 1")) {
                                    $service_db = new N2MY_DB( N2MY_MDB_DSN, "service" );
                                    $service_info = $service_db->getRow("service_key = '".addslashes($now_plan_info['service_key'])."' AND service_status = 1");
                                    $this->logger2->info(array($now_plan_info, $service_info));
                                    $update_data = array(
                                        "max_storage_size" => intval($user_info["max_storage_size"]) - intval($service_info["member_storage_size"])
                                    );
                                    $update_where = "user_key='".addslashes($user_info["user_key"])."'";
                                  }
                                  //容量更新あるよって、user_infoも最新データに変更する
                                  $obj_UserTable = new UserTable( $this->get_dsn() );
                                  $obj_UserTable = new UserTable($this->get_dsn());
                                  $obj_UserTable->update($update_data, $update_where);
                                  $user_info = $obj_UserTable->getRow("user_key='".addslashes($user_info["user_key"])."'");
                                  $this->session->set("user_info", $user_info);
                                }
                            }
                        }
                    }
                    break;
                default:
                    $member_row[error] = MEMBER_ERROR_CSV_FLAG;
                    $error_row[] = $member_row;
                    $error_flg = 1;
                    break;
            }
            $result_array[] = $member_row;
        }
        $this->logger2->info($result_array);
        if($error_flg) {
            $message = "一部のデータの登録ができませんでした。";
        }
        return $this->action_import_complete($message, $result_array,$error_flg);
    }

    public function action_import_complete($message="",$result_array = "",$error_flg = 0) {
        if ($message) {
            $this->template->assign('message', $message);
        }
        if ($error_flg) {
            $this->session->set("result_array",$result_array);
            $this->template->assign('is_error', true);
        }
        $this->display('admin/member/administrator/csv_done.t.html');
    }

    public function action_export_member_csv() {
        $user_info = $this->session->get("user_info");
        $user_key = $user_info["user_key"];

        // CSV出力
        $output_encoding = $this->get_output_encoding();
        $csv = new EZCsv(true,$output_encoding,"UTF-8");
        $dir = $this->get_work_dir();
        $tmpfile = tempnam($dir, "csv_");
        $csv->open($tmpfile, "w");
        header('Content-Type: application/octet-stream;');
        //header('Content-Disposition: attachment; filename="member_list_'.date("Ymd").'.csv"');
        header('Content-Disposition: attachment; filename="administrator_id_list_'.date("Ymd").'.csv"');
//         $header = array(
//             "member_id"   => "member_id",
//             "expense_department_code"   => "expense_department_code",
//             "accounting_unit_code"   => "accounting_unit_code",
//             "member_pass" => "member_pass",
//             "name"        => "name",
//             "name_kana"   => "name_kana",
//             "email"       => "email",
//             "timezone"    => "timezone",
//             "lang"        => "lang(ja.en)",
//             "flag"        => "flag",
//           "error"       => "error",
//         );
        $header = array(
                "member_id"   => "administrator_id",
                "member_pass" => "administrator_pass",
                "name"        => "name",
                "flag"        => "flag",
                "error"       => "error"
        );
        $csv->setHeader($header);
        $csv->write($header);
        $where =  sprintf( "user_key='%s' and member_status ='0' AND (outbound_id IS NULL OR outbound_id = '')", $user_key );
        $where .= "AND is_admin = 1";
        $_list = $this->memberTable->getRowsAssoc($where);
        foreach($_list as $_key => $row) {
            $line = array();
            $line["member_id"]      = $row["member_id"];
            //$line["expense_department_code"]   = $row["expense_department_code"];
            //$line["accounting_unit_code"]      = $row["accounting_unit_code"];
            $line["member_pass"]    = "";
            $line["name"]           = $row["member_name"];
            //$line["name_kana"]      = $row["member_name_kana"];
           // $line["email"]          = $row["member_email"];
           // $line["timezone"]       = $row["timezone"];
            //$line["lang"]           = $row["lang"];
            $line["flag"]           = "X";
            $line["error"]           = "";

            $this->logger2->info($line);
            $csv->write($line);
        }

        // CSV出力
        $csv->close();
        $fp = fopen($tmpfile, "r");
        $contents = fread($fp, filesize($tmpfile));
        fclose($fp);

        print $contents;
        unlink($tmpfile);
    }

    public function action_err_csv_download() {
        $rows = $this->session->get("result_array");
        if(!empty($rows)) {
            // エラーの出力
            $output_encoding = $this->get_output_encoding();
            $csv = new EZCsv(true,$output_encoding,"UTF-8");
            $dirName = "/member";
            $dir = $this->get_work_dir().$dirName;
            if( !is_dir( $dir ) ){
                mkdir( $dir );
                chmod( $dir ,0777 );
            }
            $tmpfile = tempnam($dir, "csv_");
            $csv->open($tmpfile, "w");

            header('Content-Type: application/octet-stream;');
            header('Content-Disposition: attachment; filename="administrator_id_err.csv"');
            //header('Content-Disposition: attachment; filename="member_err.csv"');

//             $header = array(
//                 "member_id"   => "member_id",
//                 "expense_department_code"   => "expense_department_code",
//                 "accounting_unit_code"   => "accounting_unit_code",
//                 "member_pass" => "member_pass",
//                 "name"        => "name",
//                 "name_kana"   => "name_kana",
//                 "email"       => "email",
//                 "timezone"    => "timezone",
//                 "lang"        => "lang(ja.en)",
//                 "flag"        => "flag",
//                 "error"       => "error"
//             );
            $header = array(
                    "member_id"   => "administrator_id",
                    "member_pass" => "administrator_pass",
                    "name"        => "name",
                    "flag"        => "flag",
                    "error"       => "error"
            );
            $csv->setHeader($header);
            $csv->write($header);

            foreach($rows as $a_key => $row) {
                $err_list["member_id"]      = $row["member_id"];
                //$err_list["expense_department_code"]      = $row["expense_department_code"];
                //$err_list["accounting_unit_code"]      = $row["accounting_unit_code"];
                $err_list["member_pass"]    = $row["member_pass"];
                $err_list["name"]           = $row["name"];
                //$err_list["name_kana"]      = $row["name_kana"];
                //$err_list["email"]          = $row["email"];
                //$err_list["timezone"]       = $row["timezone"];
               // $err_list["lang"]           = $row["lang"];
                $err_list["flag"]           = $row["flag"];
                $err_list["error"]          = $row["error"];

                $this->logger2->info($err_list);
                $csv->write($err_list);
            }
            $csv->close();
            $fp = fopen($tmpfile, "r");
            $contents = fread($fp, filesize($tmpfile));
            fclose($fp);
            chmod($tmpfile, 0777);
            print $contents;

            unlink($tmpfile);
        }
        unset($_SESSION['tmp_file']);
    }

    private function _isMemberExist($member_id,$user_key) {
        $where = sprintf( "user_key = '%s' AND member_id='%s'", $user_key, $member_id );
        $member_key = $this->memberTable->getOne($where, "member_key");
        return $member_key;
    }

    private function _getMemberCount($user_key) {
        $where = sprintf( "user_key = '%s' AND member_status >='0'", $user_key );
        $member_count = $this->memberTable->numRows($where);
        return $member_count;
    }

    private function _isMemberInfo($request, $type = null) {

        $message = array();

        /*
         * 一括登録時のバリデーション
         */

        //ID
        $idVali = $this->idValidationCheck($request['member_id'] , $type);
        if ($idVali) {
            $message [] = $idVali ;
        }

        // 名前のバリデーションチェック
        $nameVali = $this->nameValidationCheck($request['name']);
        if ($nameVali) {
            $message [] = $nameVali ;
        }

        // pw のバリデーションチェック
        // 新規の場合はパスワード必須、編集の場合は変更時のみ必要
        // csv時はパスワードチェック欄はないため同一のものを渡す
        if ($type != "modify") {
            // 新規
            $pwVali = $this->pwValidationCheck ( $request ['member_pass'], $request ['member_pass'] );
        } elseif( $request ['member_pass']) {
            // 編集
            $pwVali = $this->pwValidationCheck ( $request ['member_pass'], $request ['member_pass'] );
        }
        if ($pwVali) {
            $message [] = $pwVali;
        }

        return $message;
    }

    private function _isGroupExist($group_name,$user_key, $isCreate = false) {
        $group_key = $this->groupTable->getOne( sprintf( "member_group_name='%s' AND user_key = '%s'", $group_name, $user_key),"member_group_key");
        if($group_key){
            return $group_key;
        } elseif ($isCreate) {
            //add group
            $data = array();
            $data["member_group_name"] = trim($group_name);
            $data["user_key"] = $user_key;
            $key = $this->groupTable->add($data);
            return $key;
        } else {
            return 0;
        }
    }
    //member_group_key からそのmember_group_nameの取得
    private function _getMemberGroupName($key)
    {
        if ($key == 0) {
            return false;
        } else {
            $group = $this->groupTable->getRow( sprintf( "member_group_key='%s' AND user_key != 0", $key ) );
            return $group['member_group_name'];
        }
    }

    //member_status_key からそのmember_status_nameの取得
    private function _getMemberStatusName($key){
        if ($key == 0) {
            return false;
        } else {
            $obj_MemberStatus = new MemberStatusTable( $this->get_auth_dsn() );
            $ret = $obj_MemberStatus->select('member_status_key = '.$key);
            $status = $ret->fetchRow(DB_FETCHMODE_ASSOC);
            return $status['member_status_name'];
        }
    }

    /**
     * id からそのユーザーが存在するかチェック
     */
    private function _isExist($id){
        //member, user, 認証DBのuser のテーブルをチェック
        $obj_UserTable = new UserTable( $this->get_dsn() );
        $obj_MgmUserTable = new MgmUserTable( $this->get_auth_dsn() );

        if ( $obj_UserTable->numRows( sprintf("user_id='%s'", $id ) ) > 0 ||
             $this->memberTable->numRows( sprintf("member_id='%s'", $id ) )  > 0 ||
             $obj_MgmUserTable->numRows( sprintf("user_id='%s'", $id ) )  > 0 ){
            return true;
        } else {
            return false;
        }
    }

    private function check_member_info( $request ){
        $message = null;

        /*
         * 新規追加時のバリデーション
         */

        //ID
        $idVali = $this->idValidationCheck($request['member_id']);
        if ($idVali) {
            $message .= '<li>'. $idVali . '</li>';
        }

        // 名前のバリデーションチェック
        $nameVali = $this->nameValidationCheck($request['member_name']);
        if ($nameVali) {
            $message .= '<li>'. $nameVali . '</li>';
        }

        // pw のバリデーションチェック
        $pwVali = $this->pwValidationCheck($request['member_pass'],$request['member_pass_check']);
        if ($pwVali) {
            $message .= '<li>' . $pwVali . '</li>';
        }

        return $message;
     }

    /**
     * member_id と member_keyからそのIDがそのメンバーのものかチェック
     */
    private function _isExist_edit( $id, $key, $auth_user_key ){
        //member  と　user のテーブルをチェック
        $obj_UserTable = new UserTable( $this->get_dsn() );
        $obj_MgmUserTable = new MgmUserTable( $this->get_auth_dsn() );
        $auth_info = $obj_MgmUserTable->numRows( sprintf( "user_key != '%s' AND user_id='%s'", $auth_user_key, $id ) );
        if( $auth_info > 0 ){
            $this->logger2->info(array( $id, $key, $auth_user_key ));
            return false;
        }
        return true;
        /*
        $info = $this->memberTable->getRow( sprintf( "member_key='%s'", $key ) );
        $this->logger2->info($info);
        if ( $obj_UserTable->numRows( sprintf( "user_id='%s'", $id  ) ) > 0 ||
             $this->memberTable->numRows( sprintf( "member_id='%s'", $id ) ) > 1) {
            return true;
        } elseif ( $this->memberTable->numRows( sprintf( "member_id='%s'", $id ) ) == 1 &&
                   $id != $info['member_id']) {
            return true;
        } else {
            return false;
        }
        */
    }

    //同一ユーザーキーをもつメンバー同士でのメールアドレス重複チェック
    /*
    private function duplicateEmail( $user_key, $email )
    {
        require_once( "classes/dbi/address_book.dbi.php" );
        $obj_AddressBook = new AddressBookTable( $this->get_dsn() );
        $addressInfo = $obj_AddressBook->numRows( sprintf( "user_key = '%s' AND email = '%s'", $user_key, $email ) );
        return $addressInfo > 0 ? false : true;
    }
    */
    private function duplicateEmail( $user_key, $email, $member_key=null )
    {
        require_once( "classes/dbi/member.dbi.php" );
        $obj_Member = new MemberTable( $this->get_dsn() );
        $where = $member_key ?
                    sprintf( "user_key = '%s' AND member_key != '%s' AND member_email = '%s' AND member_status=0", $user_key, $member_key, $email ):
                    sprintf( "user_key = '%s' AND member_email = '%s' AND member_status=0", $user_key, $email );
        $addressInfo = $obj_Member->numRows( $where );
        return $addressInfo > 0 ? false : true;
    }

    /**
     * ＤＢにメンバー情報の登録
     */
    function action_add_member_complete()
    {
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            header( "Location: ?action_add_member=1" );
            exit;
        }
        //部屋追加
        $user_info = $this->session->get( "user_info" );
        $request = $this->session->get('new_member_info');

        $obj_Room = new RoomTable( $this->get_dsn() );
        require_once("classes/dbi/ordered_service_option.dbi.php");
        $ordered_service_option = new OrderedServiceOptionTable( $this->get_dsn() );
        //メンバー課金
        if( $user_info["account_model"] == "member" || $user_info["account_model"] == "centre" ){
            require_once( "classes/dbi/room.dbi.php" );
            require_once( "classes/dbi/user_plan.dbi.php" );
            require_once( "classes/dbi/room_plan.dbi.php" );
            $objUserPlan = new UserPlanTable( $this->get_dsn() );
            $objRoomPlanTable = new RoomPlanTable( $this->get_dsn() );
            if ($now_plan_info = $objUserPlan->getRow("user_key='".addslashes($user_info["user_key"])."' AND user_plan_status = 1")) {
                $service_db = new N2MY_DB( N2MY_MDB_DSN, "service" );
                $service_info = $service_db->getRow("service_key = '".addslashes($now_plan_info['service_key'])."' AND service_status = 1");
                $this->logger2->info(array($now_plan_info, $service_info));
            }
            $where = sprintf( "user_key=%d", $user_info["user_key"] );
            $count = $obj_Room->numRows($where);
            $room_key = $user_info["user_id"]."-".($count + 1)."-".substr( md5( uniqid( time(), true ) ), 0, 4 );
            $room_sort = $count + 1;
            //部屋登録
            $room_data = array (
                "room_key"              => $room_key,
                "max_seat"              => ($service_info["max_seat"]) ? $service_info["max_seat"] : 10,
                "max_audience_seat"     => ($service_info["max_audience_seat"]) ? $service_info["max_audience_seat"] : 0,
                "max_whiteboard_seat"   => ($service_info["max_whiteboard_seat"]) ? $service_info["max_whiteboard_seat"] : 0,
                "max_room_bandwidth"    => ($service_info["max_room_bandwidth"]) ? $service_info["max_room_bandwidth"] : "2048",
                "max_user_bandwidth"    => ($service_info["max_user_bandwidth"]) ? $service_info["max_user_bandwidth"] : "256",
                "min_user_bandwidth"    => ($service_info["min_user_bandwidth"]) ? $service_info["min_user_bandwidth"] : "30",
                "user_key"              => $user_info["user_key"],
                "room_name"             => $request['member_name'],
                "room_sort"             => $room_sort,
                "room_status"           => "1",
                "room_registtime"       => date("Y-m-d H:i:s"),
                );
            $this->logger2->info($room_data);
            $add_room = $obj_Room->add($room_data);
            if (DB::isError($add_room)) {
                $this->logger2->info( $add_room->getUserInfo());
            }
            /* userのプランからroom_planへコピー*/
            $planList = $objUserPlan->getRowsAssoc(sprintf("user_key='%s'", $user_info["user_key"]));
            for( $i = 0; $i < count( $planList ); $i++ ){
                $data = array(
                            "room_key"=>$room_key,
                            "service_key"=>$planList[$i]["service_key"],
                            "discount_rate"=>$planList[$i]["discount_rate"],
                            "contract_month_number"=>$planList[$i]["contract_month_number"],
                            "room_plan_yearly"=>$planList[$i]["user_plan_yearly"],
                            "room_plan_yearly_starttime"=>$planList[$i]["user_plan_yearly_starttime"],
                            "room_plan_starttime"=>$planList[$i]["user_plan_starttime"],
                            "room_plan_endtime"=>$planList[$i]["user_plan_endtime"],
                            "room_plan_status"=>$planList[$i]["user_plan_status"],
                            "room_plan_registtime"=>$planList[$i]["user_plan_registtime"],
                            "room_plan_updatetime"=>$planList[$i]["user_plan_updatetime"]
                             );
                $addPlan = $objRoomPlanTable->add( $data );
                if (DB::isError( $addPlan ) ) {
                    $this->logger2->info( $addPlan->getUserInfo() );
                }
            }

            // userのプランオプション(user_service_option)からmemberの部屋のオプションを追加する(ordered_service_option)
            $this->objMeetingDB = new N2MY_DB( $this->get_dsn(), "user_service_option" );
//            $condition = sprintf( "user_key='%s' AND user_service_option_starttime<='%s' AND user_service_option_status=1", $user_info["user_key"], date( "Y-m-d" ) );
            $condition = sprintf( "user_key='%s' AND service_option_key !=14", $user_info["user_key"] );
            $optionList = $this->objMeetingDB->getRowsAssoc( $condition );

            for( $i = 0; $i < count( $optionList ); $i++ ){
                $data = array(
                    "room_key" => $room_key,
                    "user_service_option_key" => $optionList[$i]["user_service_option_key"],
                    "service_option_key" => $optionList[$i]["service_option_key"],
                    "ordered_service_option_status" => $optionList[$i]["user_service_option_status"],
                    "ordered_service_option_starttime" => $optionList[$i]["user_service_option_starttime"],
                    "ordered_service_option_registtime" => $optionList[$i]["user_service_option_registtime"],
                    "ordered_service_option_deletetime" => $optionList[$i]["user_service_option_deletetime"]
                );
                $ordered_service_option->add( $data );
            }
        } else {
            $room_key = $request['member_room_key'];
        }

        //member追加
        $obj_UserTable = new UserTable( $this->get_dsn() );

        $user_key = $user_info['user_key'];
        $temp = array(
            "user_key"           => $user_key,
            "member_id"          => $request['member_id'],
            "expense_department_code" => $request['expense_department_code'],
            "accounting_unit_code"    => $request['accounting_unit_code'],
            "member_pass"        => EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $request['member_pass']),
            "member_email"       => $request['member_email'],
            "member_name"        => $request['member_name'],
            "member_name_kana"   => $request['member_name_kana'],
            "member_status"      => (($request['member_status_key']) ? $request['member_status_key'] : 0),
            "member_type"        => (($request['member_type']) ? $request['member_type'] : ""),
            "member_group"       => $request['member_group_key'],
            "room_key"           => $room_key,
            "timezone"           => $request['timezone'],
            "lang"               => $request['lang'],
            "use_shared_storage" => $request['use_shared_storage'],
            "is_admin"           => "1",
        );
        $ret = $this->memberTable->add( $temp );
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
        } else {
            $this->logger->info(__FUNCTION__."#add adminstrator successful!",__FILE__,__LINE__,$temp);
        }

        //認証サーバーへ追加
        $obj_MgmUserTable = new MgmUserTable( N2MY_MDB_DSN );
        $session = $this->session->get( "server_info" );
        $auth_data = array(
            "user_id"     => $request['member_id'],
            "server_key"    => $session["server_key"]
            );
        $result = $obj_MgmUserTable->add( $auth_data );
        if (DB::isError($result)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$result->getUserInfo());
        } else {
            $this->logger->info(__FUNCTION__."#add adminstrator successful!",__FILE__,__LINE__,$auth_data);
        }

        // 操作ログ
        $this->add_operation_log('member_add', array('member_id' => $request['member_id']));
        $this->display('admin/member/administrator/add/done.t.html');
    }

    function action_edit_member($message = ""){
        if($message){
            $this->template->assign('message', $message);
        }
        //user_key取得
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];

        $obj_MemberStatus = new MemberStatusTable( N2MY_MDB_DSN );

        $member_status_list = $obj_MemberStatus->getRowsAssoc();
        foreach( $member_status_list as $status_info) {
                $member_status[] = array(
                    "key"     => $status_info['member_status_key'],
                    "name"     => $status_info['member_status_name']
                );
        }

        $this->template->assign('member_status', $member_status);

        if($this->request->get('mode') == "edit_user"){
            //member_keyからメンバーを取得
            $member_key = $this->request->get('member_key');
            $info = $this->memberTable->getRow( sprintf( "user_key='%s' AND member_key='%s'", $user_key , $member_key ) );
            // 不正アクセス対策
            if(!$info){
                $this->logger2->warn(__FUNCTION__, __FILE__, __LINE__,$user_info["user_key"]);
                return $this->action_admin_member();
            }
            $member_info['auth_user_key']      = $this->request->get( "auth_user_key" );
            $member_info['member_key']         = $member_key;
            $member_info['member_id']          = $info['member_id'];
            $member_info['expense_department_code'] = $info['expense_department_code'];
            $member_info['accounting_unit_code']    = $info['accounting_unit_code'];
            $member_info['member_email']       = $info['member_email'];
            $member_info['member_pass']        = $info['member_pass'];
            $member_info['member_pass_check']  = $info['member_pass'];
            $member_info['member_name']        = $info['member_name'];
            $member_info['member_name_kana']   = $info['member_name_kana'];
            $member_info['member_status_key']  = $info['member_status'];
            $member_info['member_type']        = $info['member_type'];
            $member_info['member_group_key']   = $info['member_group'];
            $member_info['timezone']           = $info['timezone'];
            $member_info['lang']               = $info['lang'];
            $member_info['use_shared_storage'] = $info['use_shared_storage'];
            require_once("classes/dbi/member_room_relation.dbi.php");
            $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
            $where = "member_key = '".$member_key."'";
            $relation_info = $objRoomRelation->getRowsAssoc($where, array("create_datetime" => "desc"));
            if ($relation_info) {
                foreach ($relation_info as $relation) {
                    $member_room_keys[$relation["room_key"]] = $relation["room_key"];
                }
            }
            $member_info['member_room_keys']  = $member_room_keys;
            $this->logger2->debug($member_info['member_room_keys']);
        } else {
            $member_info = $this->session->get('m_user_edit_info');
        }
        foreach($member_info as $key => $value){
            $this->template->assign($key, $value);
        }
        $this->display('admin/member/administrator/edit/index.t.html');
    }

    function action_edit_member_confirm(){

        // 入力値を保持
        $request = $this->request->getAll();
        $this->session->set('m_user_edit_info', $request);
        $user_info = $this->session->get( "user_info");
        $message = null;

        /*
         * 編集時のバリデーション
         */
        // 管理者IDは変更不可のためチェックなし


        // 名前のバリデーションチェック
        $nameVali = $this->nameValidationCheck($request['member_name']);
        if ($nameVali) {
            $message .= '<li>'. $nameVali . '</li>';
        }

        // pw のバリデーションチェック
        $pwVali = $this->pwValidationCheck($request['member_pass'],$request['member_pass_check']);
        if ($pwVali) {
            $message .= '<li>' . $pwVali . '</li>';
        }
        if ($message) {
            $this->action_edit_member ( $message );
        } else {
            foreach ( $request as $key => $value ) {
                if ($key == "member_group_key") {
                    $this->template->assign ( 'member_group_name', $this->_getMemberGroupName ( $value ) );
                } elseif ($key == "member_status_key") {
                    $this->template->assign ( 'member_status_name', $this->_getMemberStatusName ( $value ) );
                } elseif ($key == "member_pass") {
                    $length = mb_strlen ( $value );
                    $value = "";
                    for($i = 0; $i < $length; $i ++) {
                        $value .= "*";
                    }
                }
                $this->template->assign ( $key, $value );
            }
            $this->display ( 'admin/member/administrator/edit/confirm.t.html' );
        }
    }

    function action_edit_member_complete()
    {
        $request = $this->session->get('m_user_edit_info');
        $temp = array(
            "member_name"        => $request['member_name'],
            "member_pass"        => $request['member_pass']
        );
        $temp["member_pass"] = EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $request['member_pass']);

        // 更新処理
        $ret = $this->memberTable->update($temp, sprintf( "member_key='%s'", $request['member_key'] ));

        // エラー処理
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
        } else {
            $this->logger->info(__FUNCTION__."#edit administrator successful!",__FILE__,__LINE__,$temp);
        }
        // 操作ログ
        $this->add_operation_log('member_update', array('member_id' => $request['member_id']));
        $this->display('admin/member/administrator/edit/done.t.html');
    }

    function action_delete_member(){
        $user_info = $this->session->get( "user_info");
        $member_key = $this->request->get('member_key');
        $memberInfo = $this->memberTable->getRow( sprintf( "user_key='%s' AND member_key='%s'",$user_info["user_key"] , $member_key ) );
        // 不正アクセス対策
        if(!$memberInfo){
            $this->logger2->warn(__FUNCTION__, __FILE__, __LINE__,$user_info["user_key"]);
            return $this->action_admin_member();
        }

        $key = $this->request->get('member_key');
        $temp['member_status']=-1;
        $temp["member_id"] = $memberInfo["member_id"]."_delete_".date("YmdHi");
        $temp["member_email"] = $memberInfo["member_email"]."_delete_".date("YmdHi");
        $ret = $this->memberTable->update($temp, sprintf( "member_key = '%s'", $key ) );
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
        } else {
            $this->logger->info(__FUNCTION__."#delete adminstrator successful!",__FILE__,__LINE__,array(
                "member_key" => $key)
                );
            $obj_MgmUserTable = new MgmUserTable( $this->get_auth_dsn() );
            $data["user_id"] = $temp["member_id"];
            $obj_MgmUserTable->update($data,  sprintf("user_id='%s'", $memberInfo["member_id"] ));
        }
        // 操作ログ
        $this->add_operation_log('member_delete', array('member_id' => $memberInfo['member_id']));
        $message .= "<li>".ADMINISTRATOR_DELETE_USER . "</li>";
        $this->render_admin_member($message);
    }


    /*
     * バリデーションチェック
     */

    function idValidationCheck($id , $type = null){
        $message = null;

        $regex_rule = '/^[[:alnum:]._-]{3,20}$/';
        $valid_result = EZValidator::valid_regex ( $id, $regex_rule );
        if ($valid_result == false) {
            $message = ADMINISTRATOR_ERROR_ID_LENGTH ;
        } elseif ($this->_isExist ( $id ) && $type !== "modify" ) {
            $member = $this->memberTable->getRow ( sprintf ( "member_id='%s'", $id ) );
            $this->logger2->info($member);

            if ($member["is_admin"] === "0") {
                $message = MEMBER_ERROR_ID_ADMIN_EXIST2 ;
            } else {
                $message = MEMBER_ERROR_ID_EXIST ;
            }
        }
        return $message;
    }

    function nameValidationCheck($name){

        $message = null ;
        if(!$name){
            $message = MEMBER_ERROR_NAME;
        } elseif (mb_strlen($name ) > 50) {
            $message = MEMBER_ERROR_NAME_LENGTH;
        }
        return $message;
    }

    function pwValidationCheck($pw,$pw_check){
        $message = null;
        if (! $pw && ! $pw_check) {
            $message = MEMBER_ERROR_PASS;
        } elseif ($pw != $pw_check) {
            $message = MEMBER_ERROR_PASS_DIFF;
            // ユーザーパスワードをチェック
        } elseif (! preg_match ( '/^[!-\[\]-~]{8,128}$/', $pw )) {
            $message = USER_ERROR_PASS_INVALID_01;
        } elseif (! preg_match ( '/[[:alpha:]]+/', $pw ) || preg_match ( '/^[[:alpha:]]+$/', $pw )) {
            $message = USER_ERROR_PASS_INVALID_02;
        }
        return $message;
    }

    function pwCsvValidationCheck($pw){

        $message = null ;

        if (!$pw && !$pw_check) {
            $message  = MEMBER_ERROR_PASS ;
        }elseif ($pw != $pw_check) {
            $message  = MEMBER_ERROR_PASS_DIFF ;
            //ユーザーパスワードをチェック
        } elseif (!preg_match('/^[!-\[\]-~]{8,128}$/', $pw)) {
            $message  = USER_ERROR_PASS_INVALID_01;
        } elseif (!preg_match('/[[:alpha:]]+/',$pw) || preg_match('/^[[:alpha:]]+$/',$pw)) {
            $message  = USER_ERROR_PASS_INVALID_02;
        }
        return $message;
    }

    function showAdminLogin($message = ""){

        if ($message) {
            $this->template->assign('message', $message);
        }
        $this->display('user/admin_login.tmpl');
    }

    function showAdminTop(){
        $main = new Main();
        if ($message = $main->checkAdminLoginInfo()) {
            showAdminLogin($message);
            exit;
        }
        header("Location: /service/admin.php");
    }
}

$main =& new AppMember();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>

