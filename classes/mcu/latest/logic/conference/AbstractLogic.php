<?php 
require_once("classes/mcu/config/McuConfigProxy.php");
require_once("classes/mcu/model/gateway/McuGatewayProxy.php");

require_once(dirname(__FILE__)."/../../model/MCUControllerProxy.php");
require_once(dirname(__FILE__)."/../../model/MeetingProxy.php");
require_once(dirname(__FILE__)."/../../model/ConferenceProxy.php");

require_once(dirname(__FILE__)."/../../logic/FixMediaController.php");
require_once(dirname(__FILE__)."/../../logic/MosaicController.php");

class AbstractLogic {
	protected $logger;
	protected $config;
	
	protected $mosaicController;
	protected $mediaController;
	
	protected $mcuControllerProxy;
	protected $conferenceProxy;
	protected $mcuGatewayProxy;
	
	function __construct() {
		$this->configProxy	= new McuConfigProxy();
		$this->logger = EZLogger2::getInstance();

		$this->conferenceProxy		= ConferenceProxy::getInstance();
		$this->mcuControllerProxy	= MCUControllerProxy::getInstance();
		$this->mcuGatewayProxy		= new McuGatewayProxy();
		$this->meetingProxy			= MeetingProxy::getInstance();
		
		$this->mosaicController = new MosaicController();
		$this->mediaController = new FixMediaController();
	}
}