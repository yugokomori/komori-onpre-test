<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("lib/pear/PEAR.php");
require_once("config/config.inc.php");
require_once("classes/amf/AmfService.class.php");
require_once("classes/mgm/MGM_Auth.class.php");
require_once("classes/N2MY_DBI.class.php");
require_once("lib/EZLib/EZCore/EZConfig.class.php");
require_once("lib/EZLib/EZCore/EZLogger.class.php");
require_once("lib/EZLib/EZSession/EZSession.class.php");
require_once("lib/Smarty/Smarty.class.php");

class FlashGateway extends AmfService
{
    var $logger = null;
    var $config = null;
    var $dsn = null;
    var $template = null;
    var $_result = array();
    var $session;

    /**
     * コンストラクタ
     */
    function FlashGateway()
    {
        $this->config = parse_ini_file( sprintf( "%sconfig/config.ini", N2MY_APP_DIR ), true);
        if (!defined("N2MY_MDB_DSN")) {
            define("N2MY_MDB_DSN", $this->config["GLOBAL"]["auth_dsn"]);
        }
        $this->logger = EZLogger::getInstance($this->config["LOGGER"]);
        $this->session = EZSession::getInstance();
        // 結果の初期化
        $this->_result["status"] = 1;
        // イベント定義
        $this->_addCalledMethod('login', array('id', 'password', 'session'));
        $this->_addCalledMethod('getUserList', array('member_key', 'session'));
        $this->_addCalledMethod('getRoomList', array('member_key', 'session'));
        $this->_addCalledMethod('getMeeting', array('room_key'));
        $this->_addCalledMethod('changePassword', array('member_key', 'password', 'session'));
        $this->_addCalledMethod('createMeeting', array('room_key', 'member_key', 'session', 'option'));
        $this->_addCalledMethod('startMeeting', array('member_key', 'meeting_key', 'type', 'session', 'option'));
        // テンプレート
        $this->template = new Smarty();
        $this->template->template_dir = N2MY_APP_DIR.'templates/common/';
        $this->template->config_dir   = N2MY_APP_DIR.'lib/Smarty/config/';
        $this->template->compile_dir  = N2MY_APP_DIR.'webapp/smarty/compile/';
        $this->template->cache_dir    = N2MY_APP_DIR.'webapp/smarty/cache/';
    }

    /**
     * login：ログイン
     *
     * @param string $id ログインID
     * @param string $password パスワード
     * @return integer メンバーキー
     */
    function login($id, $password, $session)
    {
        session_id($session);
        require_once( "classes/dbi/member.dbi.php" );
        require_once( "classes/dbi/member_group.dbi.php" );
        $obj_MGMAuth = new MGM_AuthClass( N2MY_MDB_DSN );
        $user_info = $obj_MGMAuth->getUserInfoById( $id );
        $result["status"] = 0;
        if ( ! $user_info = $obj_MGMAuth->getUserInfoById( $id ) ) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,array($id, $password));
            $this->template->assign("result", $result);
            $output = $this->_templateFetch("amf/login.t.xml");
            $result = array(
                                "session" => $session,
                                "xml" => $output
                            );
            return $result;
        }
        if ( ! $server_info = $obj_MGMAuth->getServerInfo( $user_info["server_key"] ) ) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$server_info);
            $this->template->assign("result", $result);
            $output = $this->_templateFetch("amf/login.t.xml");
            $result = array(
                                "session" => $session,
                                "xml" => $output
                            );
            return $result;
        } else {
            $this->session->set( "server_info", $server_info );
        }

        $obj_Member = new MemberTable( $this->get_dsn() );
        $user_info = $obj_Member->checkLogin( $id, $password );
        $output = "";
        if (!$user_info) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$user_info);
        } else {
            $obj_Group = new MemberGroupTable( $this->get_dsn() );
            $group_list = $obj_Group->getAssoc( $user_info["user_key"] );
            $user_info["group_name"] = $group_list[$user_info["member_group"]];
            $this->logger->info(__FUNCTION__,__FILE__,__LINE__, array($id, $password, $user_info, $this->session->get("server_info")));
            $this->session->set( 'login', '1' );
            require_once ('lib/EZLib/EZUtil/EZEncrypt.class.php');
            $user_info["member_pass"] = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $user_info['member_pass']);
            $this->session->set( 'user_info', $user_info );
            $this->template->assign("user_info", $user_info);

            require_once("classes/N2MY_Account.class.php");
            $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
            $rooms = $obj_N2MY_Account->getRoomList($user_info["user_key"]);
            $this->session->set('room_info', $rooms);
            $this->logger->info("successful",__FILE__,__LINE__, $id);
            $output = $this->_templateFetch("amf/login.t.xml");
            $result["status"] = 1;
        }
        $result = array(
            "session" => $session,
            "xml" => $output
            );
        return $result;
    }

    /**
     * getUserList：ユーザ取得
     *
     * @param integer $member_key メンバーキー
     * @return string グループごとのメンバー情報一覧XML
     */
    function getUserList($member_key, $session)
    {
        session_id($session);
        $this->logger->info(__FUNCTION__,__FILE__,__LINE__, $this->session->get("server_info"));
        require_once( "classes/dbi/member.dbi.php" );
        require_once( "classes/dbi/member_group.dbi.php" );

        $obj_Member = new MemberTable( $this->get_dsn() );
        $obj_Group = new MemberGroupTable( $this->get_dsn() );

        $member_info = $obj_Member->getRow( sprintf( "member_key='%s'", $member_key ) );
        if (DB::isError($member_info)) {
            $this->_result["status"] = 0;
        } else {
            $user_key = $member_info["user_key"];
            $group_list = $obj_Group->getAssoc($user_key);
            if ( DB::isError( $member_info ) ) {
                $this->_result["status"] = 0;
            } else {
                $this->logger->debug(__FUNCTION__,__FILE__,__LINE__, $group_list);
                $_list = $obj_Member->getMemberList( $user_key );
                foreach($_list as $_key => $row) {
                    $group_id = $row["member_group"];
                    if ($group_list[$group_id]) {
                        $list[$group_id]["id"] = $group_id;
                        $list[$group_id]["name"] = $group_list[$group_id];
                        $list[$group_id]["users"][] = $row;
                    } else {
                        $list["undefined"]["id"] = $group_id;
                        $list["undefined"]["name"] = $group_list[$group_id];
                        $list["undefined"]["users"][] = $row;
                    }
                }
            }
        }
        $this->template->assign("groups", $list);
        $output = $this->_templateFetch("amf/user_list.t.xml");
        $result = array(
            "member_key" => $member_key,
            "session" => $session,
            "xml" => $output
            );
        return $result;
    }

    /**
     * getRoomList：メンバーが使用可能な部屋情報一覧を取得
     *
     * @param integer $member_key メンバーキー
     * @return string グループごとのメンバー情報一覧XML
     */
    function getRoomList($member_key, $session)
    {
        session_id($session);
        $this->logger->trace(__FUNCTION__, __FILE__, __LINE__);
        require_once( "classes/dbi/member.dbi.php" );
        require_once( "classes/dbi/room.dbi.php" );

        $obj_Member = new MemberTable( $this->get_dsn() );
        $member_info = $obj_Member->getDetail($member_key);
        if (DB::isError($member_info)) {
            $this->_result["status"] = 0;
        } else {
            $user_key = $member_info["user_key"];
            $obj_Room = new RoomTable( $this->get_dsn() );
            $where = sprintf( "user_key='%s' AND room_status != 0", $user_key );
            $order = array( "room_key"=>"asc" );
            $room_list = $obj_Room->getRowsAssoc( $where, $order );
            $this->template->assign("rooms", $room_list);
        }
        $output = $this->_templateFetch("amf/room_list.t.xml");
        $result = array(
            "member_key" => $member_key,
            "session" => $session,
            "xml" => $output
            );
        return $result;
    }

    /**
     * changePassword：パスワード変更
     *
     * @param integer $member_key メンバーキー
     * @param string $password 変更後のパスワード
     * @return string パスワード変更結果
     */
    function changePassword ($member_key, $password, $session)
    {
        session_id($session);
        $this->logger->trace(__FUNCTION__, __FILE__, __LINE__);
        require_once( "classes/dbi/member.dbi.php" );
        $obj_Member = new MemberTable( $this->get_dsn() );
        if (!$password || !is_numeric($member_key)) {
            $this->_result["status"] = 0;
        } else {
            $member_row = $obj_Member->getDetail($member_key);
            if (DB::isError($member_row)) {
                $this->_result["status"] = 0;
            } else {
                $this->logger->debug(__FUNCTION__,__FILE__,__LINE__, array($member_key, $password));
                require_once ('lib/EZLib/EZUtil/EZEncrypt.class.php');
                $password = EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $password);
                $data['member_pass'] = $password;
                $ret = $obj_Member->update( $data, 'member_key = '.$member_key);
                if (DB::isError($ret)) {
                    $this->_result["status"] = 0;
                } else {
                    $this->logger->debug(__FUNCTION__,__FILE__,__LINE__, $member_row);
                    $member_row = $obj_Member->getDetail($member_key);
                    $this->template->assign("user_info", $member_row);
                    $this->logger->info("change password successful",__FILE__,__LINE__, $member_key);
                }
            }
        }
        $output = $this->_templateFetch("amf/change_password.t.xml");
        $result = array(
            "member_key" => $member_key,
            "session" => $session,
            "xml" => $output
            );
        return $result;
    }

    /**
     * 現在利用可能な会議キー取得
     */
    function getMeeting($room_key) {
        $provider_id = $this->config->get("CORE","provider_id");
        $provider_pw = $this->config->get("CORE","provider_pw");
        $core_url = $this->config->get("CORE","base_url");
        $meeting = new Meeting( $this->get_dsn(), $provider_id, $provider_pw, $core_url);
        $ret = $meeting->getNowMeeting($room_key);
        return $ret;
    }

    /**
     * createMeeting：会議作成
     *
     * @param string $room_key 部屋のID
     * @param integer $member_key メンバーキー
     * @return string 会議開始用キー
     */
    function createMeeting($room_key, $member_key, $session, $extension = array())
    {
        session_id($session);
        $this->logger->trace(__FUNCTION__, __FILE__, __LINE__);
        require_once( "classes/N2MY_Meeting.class.php" );
        $obj_N2MYMeeting = new N2MY_Meeting( $this->get_dsn() );
        $obj_Room = new RoomTable( $this->get_dsn() );
        $where = "room_key = '".$room_key."'";
        $user_key = $obj_Room->getOne($where, "user_key");
        $now_meeting = $obj_N2MYMeeting->getNowMeeting( $room_key );
        $meeting_key = $now_meeting["meeting_key"];
        $meeting_name = $now_meeting["meeting_name"];
        $meeting_password = $now_meeting["meeting_password"];
        // センタサーバ指定
        $country_id = "jp";
        if (isset($extension["locale"])) {
            switch ($extension["locale"]) {
            case "jp" :
            case "us" :
            case "cn" :
                $country_id = $extension["locale"];
                break;
            }
        }
        // オプション指定
        $option = array(
            "user_key" => $user_key,
            "meeting_name" => $meeting_name,
            "start_time" => $now_meeting["start_time"],
            "end_time" => $now_meeting["end_time"],
            "country_id" => $country_id,
        );
        $core_meeting_key = $obj_N2MYMeeting->createMeeting($room_key, $meeting_key, $option);
        $now_meeting["meeting_key"] = $core_meeting_key;
        if (!$meeting_key) {
            $this->_result["status"] = 0;
        }
        $this->session->set( "room_key", $room_key );
        $this->logger->info(__FUNCTION__, __FILE__, __LINE__,$now_meeting);
        $this->template->assign("meeting", $now_meeting);
        $output = $this->_templateFetch("amf/create_meeting.t.xml");
        $result = array(
            "member_key" => $member_key,
            "session" => $session,
            "xml" => $output
            );
        return $result;
    }

    /**
     * startMeeting：会議開始
     *
     * @param integer $member_key メンバーキー
     * @param string $meeting_key 会議開始用キー
     * @param string $type ユーザタイプ(デフォルト:"normal")
     * @return string 開始用のURL
     */
    function startMeeting($member_key, $meeting_key, $type = "normal", $session = "", $option = array())
    {
        if ($session) {
            session_id($session);
        }
        require_once( "classes/dbi/member.dbi.php" );
        require_once( "classes/dbi/user.dbi.php" );
        require_once( "classes/N2MY_Meeting.class.php" );
        $obj_N2MYMeeting = new N2MY_Meeting( $this->get_dsn() );
        $obj_Member = new MemberTable( $this->get_dsn() );
        $obj_User = new UserTable( $this->get_dsn() );

        $member_row = $obj_Member->getDetail($member_key);
        $user = $obj_User->getRow("user_key = '".$member_row["user_key"]."'");
        if (DB::isError($member_row) ) {
            $this->_result["status"] = 0;
        } else {
            $this->logger->info(__FUNCTION__, __FILE__, __LINE__,$option);
            // ユーザ情報を整形
            $user_info["id"] = $member_row["member_id"];
            $user_info["name"] = $member_row["member_name"];
            $user_info["narrow"] = 0;
            $user_info["lang"] = isset($option["lang"]) ? $option["lang"] : "ja";
            $user_info["skin_type"] = isset($option["skin_type"]) ? $option["skin_type"] : "";
            // 会議開始URL取得
            $meetingDetail = $obj_N2MYMeeting->startMeeting($meeting_key, $type, $user_info);

            $this->logger->info(__FUNCTION__, __FILE__, __LINE__,$meetingDetail);
            $this->session->set( "type", $meetingDetail["participant_type_name"] );
            $this->session->set( "meeting_key", $meetingDetail["meeting_key"] );
            $this->session->set( "participant_key", $meetingDetail["participant_key"] );
            $this->session->set( "lang", $user_info["lang"] );
            $this->session->set( "user_info", $user );
            $url = sprintf( "&PHPSESSID=%s", session_id() );
            $this->template->assign("url", $url);
        }
        $output = $this->_templateFetch("amf/start_meeting.t.xml");
        $result = array(
            "member_key" => $member_key,
            "session" => $session,
            "xml" => $output
            );
        return $result;
    }

    /**
     * テンプレートを出力
     *
     */
    function _templateFetch($template_file)
    {
        $template_path = $this->template->template_dir."/".$template_file;
        $this->template->assign("result", $this->_result);
        $output = $this->template->fetch($template_file);
        return $output;
    }

    private function get_dsn()
    {
        if ($this->_dsn) {
            return $this->_dsn;
        }
        if ( $serverdata = $this->session->get("server_info")) {
            return $this->_dsn = $serverdata["dsn"];
        } else {
            $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);
            return $this->_dsn = $server_list["SERVER_LIST"][N2MY_DEFAULT_DB];
        }
    }

}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
