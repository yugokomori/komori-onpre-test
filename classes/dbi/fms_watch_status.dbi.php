<?php
require_once("classes/N2MY_DBI.class.php");

class FmsWatchTable extends N2MY_DB {

    var $table = 'fms_watch_status';
    protected $primary_key = "watch_status_key";

    function FmsWatchTable($dsn)
    {
        $this->logger =& EZLogger::getInstance();
        $this->init($dsn, $this->table);
    }
}
