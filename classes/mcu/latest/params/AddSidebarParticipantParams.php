<?php
require_once(dirname(__FILE__)."/../params/WSDLParams.php");

class AddSidebarParticipantParams extends WSDLParams {
	protected $confId;
	protected $sidebarId;
	protected $partId;
	public function __construct($obj) {
		parent::__construct($this, $obj);
	}
	
	public function getParams() {
		return parent::getParams($this);
	}
}
