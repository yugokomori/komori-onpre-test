<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
// +----------------------------------------------------------------------+
// |                                                                      |
// | [            ]                                                       |
// |                                                                      |
// +----------------------------------------------------------------------+
// | PHP version 4                                                        |
// +----------------------------------------------------------------------+
// | Authors:kiyomizu                                                     |
// |                                                                      |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006               all rights reserved.                |
// +----------------------------------------------------------------------+
//
require_once("classes/N2MY_DBI.class.php");

class CustomerMessageTable extends N2MY_DB {

    function CustomerMessageTable($dsn) {
        $this->init($dsn, "customer_message");
    }

    function add($data) {
        $data["send_datetime"] = date("Y-m-d H:i:s");
        return parent::add($data);
    }

    //メインページ　一言メッセージ取得
    function getMessageListMain($room_key){
        $message_list = array();
        $where = " `room_key` = '". $room_key ."' " .
                 " AND status = 1 ";
        $sort = array(
                    "send_datetime" => "desc",
                );
        $message_data = $this->getRowsAssoc($where, $sort, "5", null, null, null);
        return $message_data;
    }
}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
