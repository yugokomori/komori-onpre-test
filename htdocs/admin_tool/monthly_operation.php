<?php
set_time_limit(3600);
// 環境設定
require_once("bin/set_env.php");
require_once("classes/AppFrame.class.php");

// 日計ログ
require_once("classes/mgm/dbi/meeting_date_log.dbi.php");

// メンバー利用ログ
require_once("classes/dbi/member_usage_details.dbi.php");
require_once("classes/dbi/member.dbi.php");

// ユーザー関連
require_once("classes/dbi/staff.dbi.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/dbi/user_plan.dbi.php");
require_once("classes/dbi/user_service_option.dbi.php");

// 部屋関連
require_once("classes/dbi/room.dbi.php");
require_once("classes/dbi/room_plan.dbi.php");

// オプション関連
require_once("classes/dbi/service.dbi.php"); // 部屋の基本料金、初期費用取得
require_once("classes/mgm/dbi/service_option.dbi.php"); // オプションの基本料金、単価取得
require_once("classes/dbi/ordered_service_option.dbi.php"); // 部屋に付随するオプション取得

class AppMonthlyOperation extends AppFrame {

    private $v3_url = "";
    private $v3_account = "";
    private $v3_info = array();

    private $_core_db_dsn = "";
    private $billing_mains_obj = null;
    private $billing_subs_obj = null;
    private $month_log_init = "";
    private $product = "";
    private $base_url = "";
    private $service_list = "";
    private $service_option_list = "";
    private $month_summary_info = "";

    // 請求処理・対象月取得
    private $target_year = "";
    private $target_month = "";
    private $target_year_month = "";

    private $user_list = "";
    private $room_list = "";
    private $plan_list = "";
    private $option_list = "";

    private $room_plan_obj = "";
    private $user_plan_obj = "";

    private $_tax_rate = 0;
    private $_sales_tax = 0;
    private $_total_charge = 0;

    private $more_than_standard_plan_keys = array();
    private $premium_plan_keys = array();
    private $option_keys = array();
    private $hq_plan_key = null;
    private $easiness_plan_key = null;
    private $unrestricted_plan_key = null;
    private $user_service_option_id_support = null;

    private $monthly_billing_data = array();

    function init()
    {
        // DBサーバ情報取得
        $this->dsn = $this->_get_dsn();
        //$this->logger->info(__FUNCTION__." # Array Of Dsn Lists! ", __FILE__, __LINE__, $this->dsn);

        // ｖ３請求APIのUrl設定
        $this->v3_url = $this->config->get("V3_API", "v3_url");

        // ミーティングｖ３サーバアカウント
        $this->v3_account = $this->config->get("V3_API", "v3_account");

        // サービス名設定
        $this->product = "meeting";

        // 資料共有プランKey
        $wb_plan_key = "56";

        // 上位プラン（スタンダード以上）設定
        $this->more_than_standard_plan_keys = array(
            "28" => "Professional",
            "43" => "Standard",
            "44" => "High_Quality",
            "45" => "Premium20",
            "46" => "Premium30",
            "47" => "Premium40",
            "50" => "Premium50",
            "51" => "Premium60",
            "52" => "Premium70",
            "53" => "Premium80",
            "54" => "Premium90",
            "55" => "Premium100",
            "65" => "Premium20Plus",
            "66" => "Premium30Plus",
        );

        // プレミアムプラン設定
        $this->premium_plan_keys = array(
            "45" => "Premium20",
            "46" => "Premium30",
            "47" => "Premium40",
            "50" => "Premium50",
            "51" => "Premium60",
            "52" => "Premium70",
            "53" => "Premium80",
            "54" => "Premium90",
            "55" => "Premium100",
        );

        // 高画質プラン設定
        $this->hq_plan_key = "44";

        // アプリタス（メンバー課金）用の固定値
        $this->easiness_plan_key = "48"; // おてがるプラン設定
        $this->unrestricted_plan_key = "49"; // 使い放題プラン設定
        $this->user_service_option_id_sharing = "3"; // 画面共有（オプション）設定
        $this->user_service_option_id_hdd = "4"; // 追加録画容量（オプション）設定
        $this->user_service_option_id_support = "14"; // 導入サポート（オプション）設定

        // オプションKEY設定
        $this->option_keys = array(
            "messenger"      => 1,
            "ssl"            => 2,
            "sharing"        => 3,
            "hdd"            => 4,
            "hispec"         => 5,
            "mobile"         => 6,
            "audience"       => 7,
            "h323"           => 8,
            "on_demand"      => 9,
            "on_demand_hdd"  => 10,
            "on_demand_user" => 11,
            "seminar_hdd"    => 12,
            "compact"        => 13,
            "wb"             => 16,
            "multicamera"    => 18,
            "telephone"      => 19,
            "smartphone"     => 21,
            "wb_video"       => 22,
        );

    }

    /**
     * default view routine
     *
     */
    function default_view()
    {
        $this->logger->info(__FUNCTION__." # Monthly Operation Start ... ", __FILE__, __LINE__);
        //echo "[".__FUNCTION__."] # Start ... \r\n";

        // リクエスト取得（請求年月）
        $_year = ($_REQUEST['billing_year'] ?$_REQUEST['billing_year'] : null);
        $_month = ($_REQUEST['billing_month'] ?$_REQUEST['billing_month'] : null);

        // 処理年月（請求）チェック・取得
        if (!$_year) {
            $_year = date("Y",time()); // 請求年（当年）
            //$this->logger->info(__FUNCTION__." # no param [year] ... ", __FILE__, __LINE__, $_year);
        } else {
            //$this->logger->info(__FUNCTION__." # check param [year] ... ", __FILE__, __LINE__, $_year);
        }
        if (!$_month) {
            $_month = date("m",time()); // 請求月（当月）
            //$this->logger->info(__FUNCTION__." # no param [month] ... ", __FILE__, __LINE__, $_month);
        } else {
            //$this->logger->info(__FUNCTION__." # check param [month] ... ", __FILE__, __LINE__, $_month);
        }

        $_target_datetime = mktime(0, 0, 0, $_month, 1, $_year);

        // 請求年月設定
        $this->billing_year_month = $_year . "-" . $_month;
        $this->billing_year = $_year; // 請求年
        $this->billing_month = $_month; // 請求月

        // 取込対象・売上年月設定
        $this->target_year_month = date("Y-m",strtotime("-1 month", $_target_datetime));
        $year_month_array = split("-", $this->target_year_month); // 取込対象・売上年月（請求月の前月）
        $this->target_year = $year_month_array[0]; // 取込対象・売上年（請求月の前月）
        $this->target_month = $year_month_array[1]; // 取込対象・売上対象月（請求月の前月）

        // 初期化
        $this->v3_info = array();
        $this->service_list = array();
        $this->service_option_list = array();
        $this->month_summary_info = array();

        //ｖ３請求対象情報リスト取得
        //ｖ３完全廃止！
        //$this->v3_info = $this->_get_v3_info($this->target_year, $this->target_month);
        //$this->logger->info(__FUNCTION__." # V3List ... ", __FILE__, __LINE__, $this->v3_info);
        $this->logger->info(__FUNCTION__." # V3ListCount ... ", __FILE__, __LINE__, count($this->v3_info));

        // 部屋サービスプラン一覧取得
        $this->service_list = $this->getServiceList();
        //$this->logger->info(__FUNCTION__." # ServiceList ... ", __FILE__, __LINE__, $this->service_list);

        // オプション一覧取得
        $this->service_option_list = $this->getOptionPriceList();
        //$this->logger->info(__FUNCTION__." # ServiceOptionList ... ", __FILE__, __LINE__, $this->service_option_list);

        // 当月利用ログ一覧取得
        $date_log_obj = new MeetingDateLogTable(N2MY_MDB_DSN);
        $this->month_summary_info = $date_log_obj->month_summary($this->target_year_month);
        //$this->logger->info(__FUNCTION__." # MonthSummaryInfo ... ", __FILE__, __LINE__, $this->month_summary_info);

        //PHPでのメモリ使用上限設定変更
        ini_set("memory_limit", "128M");

        // 請求データ作成（メイン）処理（用意されているDBの分だけ集計処理を繰り返す）
        $this->monthly_billing_data = array();
        foreach ($this->dsn as $server_key => $server_dsn) {
            $this->_billing_data_routine($server_dsn);
        }
        //$this->logger->info(__FUNCTION__." # MemoryInfo ... ", __FILE__, __LINE__, memory_get_usage());
        $this->_clear_private_info();
        //$this->logger->info(__FUNCTION__." # MemoryInfo ... ", __FILE__, __LINE__, memory_get_usage());

        //$this->logger->info(__FUNCTION__." # monthly_billing_data ... ", __FILE__, __LINE__, $this->monthly_billing_data);
        foreach ($this->monthly_billing_data as $main_key => $main_val) {
            $sub_serialize_info[$main_key] = $main_val;
            $sub_serialize_info[$main_key]['billing_sub_list'] = gzcompress(serialize($main_val['billing_sub_list']), 9);
        }
        unset($this->monthly_billing_data);
        //$this->logger->info(__FUNCTION__." # MemoryInfo ... ", __FILE__, __LINE__, memory_get_usage());

        print_r(serialize($sub_serialize_info));

        //PHPでのメモリ使用上限設定変更解除
        ini_restore("memory_limit");

        $this->logger->info(__FUNCTION__." # Monthly Operation End ... ", __FILE__, __LINE__);
    }


    /**
     * 請求データ作成
     * （dataDB毎に実施）
     *
     */
    private function _billing_data_routine($data_db_dsn=null)
    {
        $param = array();
        $billing_mains = array();
        $billing_subs = array();
        $this->user_list = array();
        $this->room_list = array();
        $this->plan_list = array();
        $this->option_list = array();
        $billing_date = $this->billing_year_month . "-01 00:00:00";
        $target_date = $this->target_year_month . "-01 00:00:00";

        $this->plan_obj = new RoomPlanTable($data_db_dsn);
        $this->room_plan_obj = new RoomPlanTable($data_db_dsn);
        $this->user_plan_obj = new UserPlanTable($data_db_dsn);

        // 有効なユーザー一覧取得
        $this->user_list = $this->getEffectiveUserList($data_db_dsn, $billing_date, $target_date);
        //$this->logger->info(__FUNCTION__." # UserList ... ", __FILE__, __LINE__, $this->user_list);
        //$this->logger->info(__FUNCTION__." # UserListCount ... ", __FILE__, __LINE__, count($this->user_list));

        // 有効な部屋一覧取得（参加人数制限情報も取得）
        $this->room_list = $this->getEffectiveRoomList($data_db_dsn, $billing_date, $target_date);
        //$this->logger->info(__FUNCTION__." # RoomList ... ", __FILE__, __LINE__, $this->room_list);

        // 有効な加入プラン一覧取得（オプション算出用）
        $this->plan_list = $this->getEffectivePlanList($billing_date, $target_date);
        //$this->logger->info(__FUNCTION__." # PlanList ... ", __FILE__, __LINE__, $this->plan_list);

        // 有効な付加オプション一覧取得（前月までの付属オプション）
        $this->option_list = $this->getEffectiveOptionList($data_db_dsn, $billing_date, $target_date);
        //$this->logger->info(__FUNCTION__." # OptionList ... ", __FILE__, __LINE__, $this->option_list);

        // 有効なメンバー課金用加入プラン一覧取得
        $this->user_plan_list = $this->getEffectiveUserPlanList($data_db_dsn, $billing_date, $target_date);
        //$this->logger->info(__FUNCTION__." # UserPlanList ... ", __FILE__, __LINE__, $this->user_plan_list);

        // 有効なメンバー課金用付加オプション一覧取得
        $this->user_option_list = $this->getEffectiveUserOptionList($data_db_dsn, $billing_date, $target_date);
        //$this->logger->info(__FUNCTION__." # UserOptionList ... ", __FILE__, __LINE__, $this->user_option_list);

        // メンバー課金用利用状況一覧取得（ＣＳＶデータ）
        $this->member_usage_list = $this->getMemberUsageDetailList($data_db_dsn, $billing_date, $target_date);
        //$this->logger->info(__FUNCTION__." # MemberUsageDetailList ... ", __FILE__, __LINE__, $this->member_log_list);

        // メンバー数一覧取得
        $this->member_count_list = $this->getEffectiveMemberCountList($data_db_dsn, $billing_date, $target_date);
        //$this->logger->info(__FUNCTION__." # EffectiveMemberCountList ... ", __FILE__, __LINE__, $this->member_count_list);

        // 月次請求登録処理
        if (!$this->user_list) {
            // $this->logger->info(__FUNCTION__." # DateLog has No Data! >> target_month is ... ". $this->target_month, __FILE__, __LINE__);
        }else{
            // ユーザー一覧が存在する場合
            foreach ($this->user_list as $key => $user_info) { // ユーザーごとのループ -- Start --

                $billing_subs = array(); // メモリー大量消費の原因の一つ（解消）

                // 請求すべき情報が存在するユーザか
                if ($this->valid_invoice_info($user_info)) { // valid

                    // 請求合計情報初期設定
                    $billing_mains = $this->_edit_billing_mains($user_info);

                    // １ユーザー分の請求明細データ "$billing_subs" を編集する
                    if ($user_info['account_model'] == "member") {
                        // メンバー課金対象ユーザーだったら
                        $billing_subs = $this->member_account_edit($billing_mains, $user_info);
                    } else {
                        // 部屋課金対象ユーザーだったら
                        $billing_subs = $this->room_account_edit($billing_mains, $user_info);
                    }

                    // 請求明細編集
                    $detail_group_number = 1;
                    foreach($billing_subs[$user_info['user_id']] as $key_room => $val_room){ //
                        $account_type_number = 1;
                        foreach($val_room as $key => $val){
                            // 請求明細情報の登録
                            if ("_total_" == $val['account_type_id']) {
                                $val['account_type_number'] = 100;
                            } else {
                                $val['account_type_number'] = $account_type_number;
                            }
                            if (!$val['detail_group_number']) {
                                $val['detail_group_number'] = $detail_group_number;
                            }
                            $account_type_number += 1;
                            $billing_mains['billing_sub_list'][] = $val;
                        }
                        $detail_group_number += 1;
                    }

                    $this->monthly_billing_data[] = $billing_mains;

                } else {
                    $valid_error_list[$user_info['user_key']] = $user_info['user_id'];
                } // valid
            } // ユーザーごとのループ -- End --
            //$this->logger->info(__FUNCTION__." # DetailInfo ... ", __FILE__, __LINE__, $billing_subs);
            $this->logger->info(__FUNCTION__." # valid error user ... [".count($valid_error_list)."]", __FILE__, __LINE__, $valid_error_list);
            //$this->logger->info(__FUNCTION__." # billing info ... [".count($this->monthly_billing_data)."]", __FILE__, __LINE__, null);
        }
    }


    /**
     * 部屋課金方式の請求明細情報編集
     * （本処理、v3のみ部屋が存在する場合には、対応しておりません）
     *
     */
    private function room_account_edit($billing_main=null, $user_info=array())
    {
        $ret = array();
        $v3_flg = 0;
        $_3info = array();
        $room_list = array();

        // 役務提供年月算出
        $target_date = $this->_get_summing_up_date($billing_main);
        //$this->logger->info(__FUNCTION__." # Room summing_up_date ['" . $user_info['payment_terms'] . "'] ... ", __FILE__, __LINE__, $target_date);

        // 部屋ごとの明細データ作成
        // ----------------------------------------------------- //
        if (!$this->room_list) {
            $this->logger->info(__FUNCTION__." # No Room List!", __FILE__, __LINE__);
        } elseif (array_key_exists($user_info['user_key'], $this->room_list) === false) {
            $this->logger->info(__FUNCTION__." # No Room! >> User is ... [". $user_info['user_key']."]", __FILE__, __LINE__, $user_info['user_key']);
        } else {
            // ｖ３請求情報の存在確認
            if (array_key_exists($user_info['user_id'], $this->v3_info)) {
                $_3info = $this->v3_info[$user_info['user_id']];
                $_3room_count = $_3info['room_count'];
                if (($_3room_count == 1) &&
                    (count($this->room_list[$user_info['user_key']]) == 1)) {
                    // ｖ３請求情報の存在確認
                    //（ｖ３：ｖ４＝１：１であればｖ４の部屋に対して追加料金の情報を足しこむ）
                    $v3_flg = 1;
                    $room_list = $this->_add_v3_extends_info($this->room_list[$user_info['user_key']], $_3info);
                } elseif ($_3room_count > 1) {
                    $v3_flg = 2;
                    $room_list = $this->room_list[$user_info['user_key']];
                } else { // $v3_flg = 0;
                    $_3info = array();
                    $room_list = $this->room_list[$user_info['user_key']];
                }
            } else { // $v3_flg = 0;
                $_3info = array();
                $room_list = $this->room_list[$user_info['user_key']];
            }

            // ｖ４請求明細情報編集
            foreach ($room_list as $room_key => $room_info) { // 部屋ごとのループ -- Start --
                //$this->logger->info(__FUNCTION__." # room info ... ", __FILE__, __LINE__, $room_info);
                // 課金項目ごとの編集＆集計
                $ret[$user_info['user_id']][$room_key] = $this->_edit_billing_subs($room_info, $user_info, $target_date);
            } // 部屋ごとのループ -- End --
            unset($room_list);

            // ｖ３請求明細情報の作成（編集。ｖ３情報はｖ４の明細の後に）
            if ($v3_flg == 2) {
                $_3info = $this->v3_info[$user_info['user_id']];
                // ｖ３、ｖ４が複数部屋存在した場合、ｖ３の明細を作りこむ
                foreach($_3info['room_info'] as $key => $val){
                    // 請求明細情報の登録
                    $_3_sub_info = $this->_set_v3_sub_data($val);
                    $ret[$user_info['user_id']][$val['room_key']] = $_3_sub_info;
                    $detail_group_number += 1;
                }
            }
        }
        // ----------------------------------------------------- //

        // その他オプション情報の編集
        // ----------------------------------------------------- //
        $_option_data = null;
        // $this->logger->info(__FUNCTION__." # User Key ... ", __FILE__, __LINE__, $user_info['user_key']);
        // $this->logger->info(__FUNCTION__." # Member List ... ", __FILE__, __LINE__, $this->member_count_list);
        // v4メンバー情報が存在するか？
        if (!$this->member_count_list) {
            // $this->logger->info(__FUNCTION__." # V4 Presence Appli ... ", __FILE__, __LINE__, " === No Member === ");
        } elseif (array_key_exists($user_info['user_key'], $this->member_count_list) === true) {
            $member_cnt = 0;
            $member_cnt = $this->member_count_list[$user_info['user_key']];
            //$this->logger->info(__FUNCTION__, __FILE__, __LINE__, " # V4 Presence Appli Count ... ".$member_cnt);
            if ($member_cnt >= 1) {
                $_option_data['presence_appli']['member_count'] = $member_cnt;
            }
            //$this->logger->info(__FUNCTION__." # Memberあり ... ", __FILE__, __LINE__, $this->member_count_list[$user_info['user_key']]);
        }
        // v3オプション情報が存在するか？
        if ($v3_flg != 0) {
            $v3_option = $this->_add_v3_option_info($_3info);
            // ユーザーごとの編集＆集計（メッセンジャー[メンバー]オプション：ｖ３のみ）
            if ($v3_option['v3_option']['messenger']['charge'] > 0) {
                $_option_data['v3'] = $v3_option;
            }
        }
        //$this->logger->info(__FUNCTION__, __FILE__, __LINE__ . " # Option Data ... ", $_option_data);
        // オプション明細情報が存在するか？
        if ($_option_data) {
            $other_options_info = array();
            $other_options_info = $this->_edit_billing_detail_option($_option_data);
            //$this->logger->info(__FUNCTION__, __FILE__, __LINE__ . " # Option Info ... ", $other_options_info);
            foreach ($other_options_info as $key => $val) {
                $ret[$user_info['user_id']]['_option_'][] = $val;
                //$this->logger->info(__FUNCTION__, __FILE__, __LINE__ . " # Option Info Val [".$user_info['user_id']."] ... ", $val);
            }
            //$this->logger->info(__FUNCTION__, __FILE__, __LINE__, $ret);
        }
        // ----------------------------------------------------- //
        //$this->logger->info(__FUNCTION__, __FILE__, __LINE__, $ret);

        return $ret;
    }


    /**
     * メンバー課金方式の請求明細情報編集
     * （本処理、v3には対応しておりません）
     *
     */
    private function member_account_edit($billing_main=null, $user_info=array())
    {
        $ret = array();
        $user_id = $user_info['user_id'];
        $detail_group_id = '_member_';
        $account_type_number = 1;
        $sub_total_charge = 0;
        $_billing_detail_init = array(
                                   "billing_main_id"     => null,
                                   "detail_group_number" => 100,
                                   "detail_group_id"     => $detail_group_id,
                                   "detail_group_name"   => "メンバー課金",
                                   "account_type_number" => null,
                                   "account_type_id"     => null,
                                   "account_type_name"   => null,
                                   "charge"              => 0,
                                   "amount"              => 0,
                                   "unit"                => null,
                                   "price"               => 0,
                                   "remarks"             => null,
                                   "serialize_info"      => null,
                               );

        $user_plan_info = array();
        $user_option_info = array();
        $member_usage_info = array();
        $service_info = array();
        $option_info = array();

        // 役務提供年月算出
        $target_date = $this->_get_summing_up_date($billing_main);

        // 有効な加入メンバープラン一覧取得（部屋基本料金算出用）
        $advance_plan_info = $this->getAdvanceUserPlan($user_info['user_key'], $target_date);
        $advance_service_info = $this->service_list[$advance_plan_info['service_key']];
        //$this->logger->info(__FUNCTION__." # AdvanceUserPlanInfo[" . $target_date . "] ... ", __FILE__, __LINE__, $advance_plan_info);


        // ユーザープラン情報取得（48：おてがる、49：使い放題）
        if (!$this->user_plan_list) {
        } elseif (array_key_exists($user_info['user_key'], $this->user_plan_list) !== true) {
        } else {
            $user_plan_info = $this->user_plan_list[$user_info['user_key']];
            $service_info = $this->service_list[$user_plan_info['service_key']];
        }

        // ユーザーオプション情報取得
        //$this->logger->info(__FUNCTION__." # UserOptionList ... ", __FILE__, __LINE__, $this->user_option_list);
        if (!$this->user_option_list) {
        } elseif (array_key_exists($user_info['user_key'], $this->user_option_list) !== true) {
        } else {
            $user_option_info = $this->user_option_list[$user_info['user_key']];
            //$this->logger->info(__FUNCTION__." # AdvanceUserOptionInfo[" . $user_info['user_key'] . "] ... ", __FILE__, __LINE__, $user_option_info);
        }

        // メンバー利用情報取得
        if (!$this->member_usage_list) {
        } elseif (array_key_exists($user_info['user_key'], $this->member_usage_list) !== true) {
        } else {
            $member_usage_info = $this->member_usage_list[$user_info['user_key']];
        }

        // --------------------------------------------------------------- //
        // アプリタス（メンバー課金）であれば、初期費用は初回の請求書に乗せる。（明細追加）
        // 期間契約判定
        if ($advance_plan_info) {
            if ($advance_plan_info['contract_month_number'] > 1) { // 契約月数が2ヶ月以上で期間契約とする
                $period_contract_flg = "1";
            }
        }

        // --------------------------------------------------------------- //
        // プラン初期費用作成
        // --------------------------------------------------------------- //
        $init_flg = null;
        // 初回請求判定条件設定
        if ($period_contract_flg) {
            $_target_date = split("-", $target_date);
            $target_month = $_target_date[0] . "-" . $_target_date[1];
            $_start_date = split("-", $advance_plan_info['user_plan_starttime']);
            $start_month = $_start_date[0] . "-" . $_start_date[1];
            // $this->logger->info(__FUNCTION__, __FILE__, __LINE__, " ### target_month = " . $target_month);
            // $this->logger->info(__FUNCTION__, __FILE__, __LINE__, " ### start_month = " . $start_month);
            if ($target_month == $start_month) { // 初回請求だった場合
                $price_on_flg = "1";
                $discount_rate = $advance_plan_info['discount_rate'];
                $contract_month_number = $advance_plan_info['contract_month_number'];
                $init_flg = "1";
            }
        } else {
            $price_on_flg = "1";
            $discount_rate = 0;
            $contract_month_number = 1;
        }
        // 初回請求判定
        if ($init_flg) {
            $data = $_billing_detail_init;
            $data['account_type_number'] = $account_type_number;
            $data['account_type_id'] = "basic";
            $data['account_type_name'] = "初期費用";
            $data['price'] = $advance_service_info['service_initial_charge']; // 単価
            $data['amount'] = 1; // 数量[契約メンバーID数]
            $data['unit'] = "";
            $data['charge'] = $data['price'] * $data['amount']; // 数量[契約メンバーID数]
            $ret[$user_id][$detail_group_id][] = $data;
            $account_type_number += 1;
            $sub_total_charge += $data['charge'];
        }

        // --------------------------------------------------------------- //
        // プラン基本料金作成（メンバー数　×　プラン基本料金）
        // --------------------------------------------------------------- //
        $data = $_billing_detail_init;
        $data['account_type_number'] = $account_type_number;
        $data['account_type_id'] = "basic";
        // $data['price'] = $service_info['service_charge']; // 単価
        $data['amount'] = $user_info['max_member_count']; // 数量[契約メンバーID数]
        $data['unit'] = "ID";

        // 単価設定
        if ($advance_service_info) {
            if ($price_on_flg) { // 契約月数が2ヶ月以上で期間契約とする
                $_price = $advance_service_info['service_charge'] * $contract_month_number * (100 - $discount_rate); // 単価
                //$this->logger->info(__FUNCTION__, __FILE__, __LINE__, "### discount_rate ...".$discount_rate);
                //$this->logger->info(__FUNCTION__, __FILE__, __LINE__, "### _price ...".$_price);
                if ($_price == 0) {
                    $price = 0; // 単価
                } else {
                    $price = floor($_price / 100); // 単価
                }
            } else {
                $price = 0; // 単価
            }
        }
        $data['price'] = $price; // 単価
        $data['charge'] = $data['price'] * $data['amount'];

        // 備考欄編集
        if ($advance_plan_info['contract_month_number'] > 1) {
            $limit_year = date("Y", strtotime($advance_plan_info['user_plan_starttime']));
            $limit_month = date("m", strtotime($advance_plan_info['user_plan_starttime']));
            $end_date = date("Y-m-d",strtotime("-1 day", mktime(0, 0, 0, $limit_month + $advance_plan_info['contract_month_number'], 1, $limit_year)));
            $discount_rate = number_format(((100 - $advance_plan_info['discount_rate']) / 100), 2, '.', '');
            $_name = "期間利用料";
        } else {
            $_name = "月額基本料";
        }
        if ($advance_plan_info['contract_month_number'] == 12) {
            $_remarks = "（ 年間契約";
            if ($price_on_flg) {
                $_remarks .= "基本料 : ". number_format($service_info['service_charge']) . "×" . number_format($data['amount']) . "ID×" . $advance_plan_info['contract_month_number'] . "×" . $discount_rate;
            }
            $_remarks .= "　有効期限 : ". $end_date . " ）";
        } elseif ($advance_plan_info['contract_month_number'] == 6) {
            $_remarks = "（ 半年契約";
            if ($price_on_flg) {
                $_remarks .= "基本料 : ". $service_info['service_charge'] . "×" . number_format($data['amount']) . "ID×" . $advance_plan_info['contract_month_number'] . "×" . $discount_rate;
            }
            $_remarks .= "　有効期限 : ". $end_date . " ）";
        } elseif ($advance_plan_info['contract_month_number'] >= 2) {
            $_remarks = "（ " . $advance_plan_info['contract_month_number'] . "ヶ月契約";
            if ($price_on_flg) {
                $_remarks .= "基本料 : ". $service_info['service_charge'] . "×" . number_format($data['amount']) . "ID×" . $advance_plan_info['contract_month_number'] . "×" . $discount_rate;
            }
            $_remarks .= "　有効期限 : ". $end_date . " ）";
        } else {
            $_remarks = null;
        }

        // 必要な情報：年契約判定フラグ、年契約開始日、支払条件、請求処理月
        //$data['account_type_name'] = $service_info['service_name'] . "　基本料金";
        $data['account_type_name'] = $service_info['service_name'] . "　" . $_name;
        $data['remarks'] = $_remarks;
        $serialize_info_wk = array("start_date" => $user_plan_info['user_plan_starttime']);
        $data['serialize_info'] = serialize($serialize_info_wk);
        $ret[$user_id][$detail_group_id][] = $data;
        $account_type_number += 1;
        $sub_total_charge += $data['charge'];

        // --------------------------------------------------------------- //
        // プラン超過料金編集
        // --------------------------------------------------------------- //
        $data = $_billing_detail_init; // 初期化
        $data['account_type_number'] = $account_type_number;
        $data['account_type_id'] = "extends";
        $data['account_type_name'] = $service_info['service_name'] . "　超過料金";
        $data['price'] = $service_info['service_additional_charge'];
        $total_use_count = 0;
        // 利用時間集計
        foreach ($member_usage_info as $member_key => $member_val) {
            if ($service_info['service_key'] == $this->easiness_plan_key) { // おてがるプラン設定
                $use_count = 0;
                if ($member_val['extends'] < 60) {
                   $use_count = $member_val['extends'] - 60;
                }
            } elseif ($service_info['service_key'] == $this->unrestricted_plan_key) { // 使い放題プラン設定
                $use_count = 0;
            }
            $total_use_count += $use_count;
        }
        $data['amount'] = $total_use_count;
        $data['unit'] = "分";
        $data['charge'] = $data['price'] * $data['amount'];
        $_remarks = "( ￥". number_format($data['price']) . " × " . number_format($data['amount']) . $data['unit'] . " )";
        $data['remarks'] = $_remarks;
        $ret[$user_id][$detail_group_id][] = $data;
        $account_type_number += 1;
        $sub_total_charge += $data['charge'];

        $_member_cnt = $user_info['max_member_count']; // 数量[契約メンバーID数]
        // --------------------------------------------------------------- //
        // 画面共有（シェアリング）料金編集　※任意追加したユーザーのみ
        // --------------------------------------------------------------- //
        $_option_cnt_sharing = 0;
        if (array_key_exists($this->user_service_option_id_sharing, $user_option_info) === true) {
            $_option_cnt_sharing = $user_option_info[$this->user_service_option_id_sharing]['count']; // 数量[オプション数]
            $data = $_billing_detail_init; // 初期化
            $data['account_type_number'] = $account_type_number;
            $data['account_type_id'] = "share";
            $data['account_type_name'] = "動画共有";
            $data['price'] = 1000; // 固定値（動画共有・単価）
            // $data['amount'] = $user_info['max_member_count']; // 数量[契約メンバーID数]
            $data['amount'] = $_member_cnt * $_option_cnt_sharing; // 数量（メンバー数×オプション数）
            $data['unit'] = "ID";
            $data['charge'] = $data['price'] * $data['amount']; // 料金
            $_remarks = "( ￥". number_format($data['price']) . " × " . number_format($data['amount']) . $data['unit'] . " )";
            $data['remarks'] = $_remarks;
            $ret[$user_id][$detail_group_id][] = $data;
            $account_type_number += 1;
            $sub_total_charge += $data['charge'];
        }

        // --------------------------------------------------------------- //
        // 録画機能（500MB）料金編集　※任意追加したユーザーのみ
        // --------------------------------------------------------------- //
        if (array_key_exists($this->user_service_option_id_hdd, $user_option_info) === true) {
            $_option_cnt_hdd = $user_option_info[$this->user_service_option_id_hdd]['count']; // 数量[オプション数]
            $data = $_billing_detail_init; // 初期化
            $data['account_type_number'] = $account_type_number;
            $data['account_type_id'] = "hdd";
            // タイトル
            $_i = null;
            $_u = 500;
            $_title = '録画機能（';
            $_price = 0;
            if ($_option_cnt_hdd <= 1) {
                $_i = $_u;
                $_title .= $_u . 'MB）';
                $_price = 5000;
            } else {
                $_i = round((($_u * $_option_cnt_hdd) / 1000), 2);
                $_title .= $_i . 'GB）';
                $_price = 10000 * $_i;
            }
            //$data['account_type_name'] = "録画機能（" . (500 * $_option_cnt_hdd) . "MB）";
            $data['account_type_name'] = $_title;
            //$data['price'] = 5000; // 固定値（録画機能・単価）
            $data['price'] = $_price; // 固定値（録画機能・単価）
            //$data['amount'] = $user_info['max_member_count']; // 数量[契約メンバーID数]
            //$data['amount'] = $_member_cnt * $_option_cnt_hdd; // 数量（メンバー数×オプション数）
            $data['amount'] = $_member_cnt; // 数量（メンバー数）
            $data['unit'] = "ID";
            $data['charge'] = $data['price'] * $data['amount']; // 料金
            $_remarks = "( ￥". number_format($data['price']) . " × " . number_format($data['amount']) . $data['unit'] . " )";
            $data['remarks'] = $_remarks;
            $ret[$user_id][$detail_group_id][] = $data;
            $account_type_number += 1;
            $sub_total_charge += $data['charge'];
        }

        // --------------------------------------------------------------- //
        // 導入サポート費の料金編集
        // --------------------------------------------------------------- //
        if (array_key_exists($this->user_service_option_id_support, $user_option_info) === true) {
            $data = $_billing_detail_init; // 初期化
            $data['account_type_number'] = $account_type_number;
            $data['account_type_id'] = "support";
            $data['account_type_name'] = "導入サポート費";
            $data['price'] = 30000; // 固定値（導入サポート費用・単価）
            $data['amount'] = $user_option_info[$this->user_service_option_id_support]['count']; // 数量[前月サポート回数]
            $data['unit'] = "回";
            $data['charge'] = $data['price'] * $data['amount']; // 料金
            $_remarks = null;
            $data['remarks'] = $_remarks;
            $ret[$user_id][$detail_group_id][] = $data;
            $account_type_number += 1;
            $sub_total_charge += $data['charge'];
        }

        // --------------------------------------------------------------- //
        // 小計（合計）
        // --------------------------------------------------------------- //
        $account_type_number = 100;
        $data = $_billing_detail_init;
        $data['detail_group_name'] = "メンバー課金契約";
        $data['account_type_number'] = $account_type_number;
        $data['account_type_id'] = "_total_";
        $data['account_type_name'] = "メンバー課金契約小計";
        $data['charge'] = $sub_total_charge;
        $data['amount'] = 1;
        $data['unit'] = null;
        $data['price'] = $sub_total_charge;
        $_remarks = null;
        $data['remarks'] = $_remarks;
        $ret[$user_id][$detail_group_id][] = $data;

        // --------------------------------------------------------------- //
        //$this->logger->info(__FUNCTION__, __FILE__, __LINE__, $ret);
        return $ret;
    }


    /**
     * BillingTotalInfoDB登録情報編集
     * "$billing_mains" に "$billing_subs" を合わせて編集される
     *
     */
    private function _edit_billing_mains($user_info=array())
    {
        // 請求合計初期化用オブジェクト初期設定
        $_billing_mains = array(
                                   "billing_year"            => null,
                                   "billing_month"           => null,
                                   "invoice_type"            => "normal",
                                   "product_id"              => $this->product,
                                   "country_id"              => null,
                                   "client_id"               => null,
                                   "client_key"              => null,
                                   "client_name"             => null,
                                   "client_zip"              => null,
                                   "client_state"            => null,
                                   "client_city"             => null,
                                   "client_address"          => null,
                                   "client_tel"              => null,
                                   "client_fax"              => null,
                                   "client_staff_department" => null,
                                   "client_staff_name"       => null,
                                   "client_staff_mail"       => null,
                                   "paymethod"               => null, // 支払い情報（請求管理システムで付与）
                                   "payment_terms"           => null, // 支払い情報
                                   "paymethod_info"          => null, // 支払い情報（請求管理システムで付与）
                                   "staff_id"                => null,
                                   "staff_section_id"        => null,
                                   "staff_info"              => null,
                                   "tax_rate"                => 0, // 金額情報（請求管理システムで設定を行う）
                                   "sales_tax"               => 0, // 金額情報（請求管理システムで設定を行う）
                                   "total_charge"            => 0, // 金額情報（請求管理システムで設定を行う）
                                   "pay_limit_date"          => "0000-00-00", // その他情報
                                   "billing_status"          => "0", // その他情報
                                   "billing_date"            => "0000-00-00", // その他情報
                                   "remarks"                 => null, // その他情報
                                   "serialize_info"          => null, // その他情報
                                   "billing_sub_list"        => null, // その他情報
                               );

        // 請求合計情報の編集＆集計
        $_billing_mains["billing_year"]     = $this->billing_year;
        $_billing_mains["billing_month"]    = $this->billing_month;
//      $_billing_mains["invoice_type"]     = "normal"; // 上流で設定済み
//      $_billing_mains["product_id"]       = "meeting"; // 上流で設定済み

        // クライアント情報
        $_billing_mains["country_id"]       = $user_info['country_key'];
        $_billing_mains["client_id"]        = "";
        $_billing_mains["client_key"]       = $user_info['user_id'];
        $_billing_mains["client_name"]      = $user_info['user_company_name'];
        $_billing_mains["client_zip"]       = $user_info['user_company_postnumber'];
        $_billing_mains["client_state"]     = $user_info['user_company_address'];
        $_billing_mains["client_city"]      = "";
        $_billing_mains["client_address"]   = "";
        $_billing_mains["client_tel"]       = $user_info['user_company_phone'];
        $_billing_mains["client_fax"]       = $user_info['user_company_fax'];
        $_billing_mains["client_staff_department"] = "";
        $_billing_mains["client_staff_name"]       = $user_info['user_staff_lastname'] . "　" . $user_info['user_staff_firstname'];
        $_billing_mains["client_staff_mail"]       = $user_info['user_staff_email'];

        // 支払い情報
        $_billing_mains["payment_terms"]    = $user_info['payment_terms']; // 請求管理システムで付与

        return $_billing_mains;
    }


    /**
     * BillingDetailInfoDB登録情報編集
     *
     */
    private function _edit_billing_subs($room_info = array(), $user_info = array(), $target_date=null)
    {

        // 請求明細初期化用オブジェクト初期設定
        $v3_charge_time = 0;
        $charge_time = 0;
        $advance_info = array('plan' => null, 'service' => null);
        $base_info = array('plan' => null, 'service' => null);
        $_billing_subs = array();
        $_billing_detail_work = array();
        $_billing_detail_init = array(
                                   'detail_group_number'     => null,
                                   'detail_group_id'         => $room_info['room_key'],
                                   'detail_group_name'       => $room_info['room_name'],
                                   'account_type_number'     => null,
                                   'account_type_id'         => null,
                                   'account_type_name'       => null,
                                   'charge'                  => 0,
                                   'amount'                  => 0,
                                   'unit'                    => null,
                                   'price'                   => 0,
                                   'remarks'                 => null,
                                   'serialize_info'          => null,
                               );

        // 初期化
        $_billing_detail_work = $_billing_detail_init;

        // 基本料金プラン情報取得（部屋基本料金算出用）
        $advance_info['plan'] = $this->getAdvanceRoomPlan($room_info['room_key'], $target_date);
        $advance_info['service'] = $this->service_list[$advance_info['plan']['service_key']];
        // $this->logger->info(__FUNCTION__." # AdvanceRoomPlanInfo[" . $target_date . "] ... ", __FILE__, __LINE__, $advance_plan_info);

        // 前月利用プラン情報取得（部屋利用料金とその他オプション料金算出用）
        if (!$this->plan_list) {
        } elseif (array_key_exists($room_info['room_key'], $this->plan_list) !== true) {
        } else {
            $base_info['plan'] = $this->plan_list[$room_info['room_key']];
            $base_info['service'] = $this->service_list[$base_info['plan']['service_key']];
        }

        // 利用時間
        $charge_info = $this->month_summary_info[$user_info['user_id']][$room_info['room_key']];

        // 課金時間算出
        if (array_key_exists('v3_info', $room_info) !== true) {
        }else{
            // ｖ３の課金対象時間は、無料通話時間をひいた時間になっている
            $v3_charge_time = $room_info['v3_info']['charge_time'];
        }

        // ｖ４で課金対象時間を算出する（無料時間の反映）
        $charge_time = ($charge_info['extends'] - $base_info['service']['service_freetime']);

        // 合計超過利用時間（ｖ４超過利用時間にｖ３超過利用分の合算）
        if ($charge_time < 0) {
            $charge_time = 0 + $v3_charge_time;
        } else {
            $charge_time = $charge_time + $v3_charge_time;
        }

        // オプション情報取得
        // $options = $this->option_list[$room_info['room_key']];

        // 明細レコード生成（roomタイトル情報）
        $_billing_subs[] = $this->_edit_title($_billing_detail_init, $advance_info, $base_info, $charge_time);

        // 明細レコード生成（基本料金）
        $_billing_subs[] = $this->_edit_basic_charge($_billing_detail_init, $advance_info, $target_date);

        // 明細レコード生成（追加料金）
        $_billing_subs[] = $this->_edit_extend_charge($_billing_detail_init, $base_info, $charge_time);

        // 明細レコード生成（携帯テレビ電話月額基本料金）
        $_billing_subs[] = $this->_edit_option_basic_charge($_billing_detail_init, $room_info, $base_info, "mobile");

        // 明細レコード生成（携帯テレビ電話追加料金（通常））
        $_billing_subs[] = $this->_edit_option_extend_charge($_billing_detail_init, $room_info, $charge_info, $base_info, "mobile", "normal");

        // 明細レコード生成（携帯テレビ電話追加料金（特殊））
        $_billing_subs[] = $this->_edit_option_extend_charge($_billing_detail_init, $room_info, $charge_info, $base_info, "mobile", "special");

        // 明細レコード生成（Ｈ.３２３通信月額基本料金）
        $_billing_subs[] = $this->_edit_option_basic_charge($_billing_detail_init, $room_info, $base_info, "h323");

        // 明細レコード生成（Ｈ.３２３通信追加料金）
        $_billing_subs[] = $this->_edit_option_extend_charge($_billing_detail_init, $room_info, $charge_info, $base_info, "h323");

        // 明細レコード生成（SSL通信）
        $_billing_subs[] = $this->_edit_option_basic_charge($_billing_detail_init, $room_info, $base_info, "ssl");

        // 明細レコード生成（高画質モード基本料金）
        $_billing_subs[] = $this->_edit_option_basic_charge($_billing_detail_init, $room_info, $base_info, "hispec");

        // 明細レコード生成（高画質モード追加料金）
        $_billing_subs[] = $this->_edit_option_extend_charge($_billing_detail_init, $room_info, $charge_info, $base_info, "hispec");

        // 明細レコード生成（デスクトップシェアリング）
        $_billing_subs[] = $this->_edit_option_basic_charge($_billing_detail_init, $room_info, $base_info, "sharing");

        // 明細レコード生成（追加録画容量）
        $_billing_subs[] = $this->_edit_option_basic_charge($_billing_detail_init, $room_info, $base_info, "hdd");

        // 明細レコード生成（コンパクト）
        $_billing_subs[] = $this->_edit_option_basic_charge($_billing_detail_init, $room_info, $base_info, "compact");

        $plan_info = $this->plan_list[$room_info['room_key']];

        // 明細レコード生成（オーディエンス）
        $audience_info = null;
        if ((array_key_exists($plan_info['service_key'], $this->premium_plan_keys) === false) && // room_plan=プレミアムプラン以外　かつ
             ($room_info['max_audience_seat'] > 0)) { // オーディエンス座席数１以上
            if (array_key_exists($this->option_keys['audience'], $this->option_list[$room_info['room_key']]) === false) {
                $audience_info = null;
            } else {
                $audience_info = $this->option_list[$room_info['room_key']][$this->option_keys['audience']];
            }
            if ($audience_info) {
                if ($audience_info['init_count'] >= 1) {
                    // 明細レコード生成（オーディエンス初期費用）
                    $_billing_subs[] = $this->_edit_audience_first_charge($_billing_detail_init, $room_info, $base_info);
                }
            }
            // 明細レコード生成（オーディエンス月額基本料金）
            $_billing_subs[] = $this->_edit_option_basic_charge($_billing_detail_init, $room_info, $base_info, "audience");
            // 明細レコード生成（オーディエンス追加料金）
            $_billing_subs[] = $this->_edit_option_extend_charge($_billing_detail_init, $room_info, $charge_info, $base_info, "audience");
        }

        // 明細レコード生成（multicamera：マルチカメラオプション）プラン関係ない
        if (array_key_exists($this->option_keys['multicamera'], $this->option_list[$room_info['room_key']]) === true) {
            // 契約している部屋情報にマルチカメラオプションが存在した場合
            $_billing_subs[] = $this->_edit_option_basic_charge($_billing_detail_init, $room_info, $base_info, "multicamera");
        }

        // 明細レコード生成（telephone：電話連携オプション）プラン関係ない
        if (array_key_exists($this->option_keys['telephone'], $this->option_list[$room_info['room_key']]) === true) {
            // 契約している部屋情報に電話連携オプションが存在した場合
            $_billing_subs[] = $this->_edit_option_basic_charge($_billing_detail_init, $room_info, $base_info, "telephone");
        }

        // 明細レコード生成（smartphone：スマートフォン連携オプション）プラン関係ない
        if (array_key_exists($this->option_keys['smartphone'], $this->option_list[$room_info['room_key']]) === true) {
            // 契約している部屋情報にスマートフォン連携オプションが存在した場合
            $_billing_subs[] = $this->_edit_option_basic_charge($_billing_detail_init, $room_info, $base_info, "smartphone");
        }

        // 明細レコード生成（iPad Basicオプション）スタンダード以上は１つおまけ
        if ($room_info['max_whiteboard_seat'] > 0) { // iPad座席数１以上
            $_billing_subs[] = $this->_edit_option_basic_charge($_billing_detail_init, $room_info, $base_info, "wb");
        }

        // 明細レコード生成（iPad Plusオプション）スタンダード以上は１つおまけ
        if (array_key_exists($this->option_keys['wb_video'], $this->option_list[$room_info['room_key']]) === true) {
            // 契約している部屋情報にiPad Plusオプションが存在した場合
            $_billing_subs[] = $this->_edit_option_basic_charge($_billing_detail_init, $room_info, $base_info, "wb_video");
        }

        // 明細レコード生成（小計情報）
        $_billing_subs[] = $this->_edit_total($_billing_detail_init, $_billing_subs);

        return $_billing_subs;

    }


    /**
     * BillingDetailInfoDB登録情報編集
     */
    private function _edit_billing_detail_option($option_info=array())
    {

        // 請求明細初期化用オブジェクト初期設定
        $presence_appli_option = null;
        $v3_option = null;

        $_billing_subs = array();
        $_billing_detail_work = array();
        $_billing_detail_init = array(
                                   "detail_group_number"     => 100,
                                   "detail_group_id"         => "_option_",
                                   "detail_group_name"       => "その他オプション",
                                   "account_type_number"     => null,
                                   "account_type_id"         => null,
                                   "account_type_name"       => null,
                                   "charge"                  => 0,
                                   "amount"                  => 0,
                                   "unit"                    => null,
                                   "price"                   => 0,
                                   "remarks"                 => null,
                                   "serialize_info"          => null,
                                   "create_datetime"         => null,
                                   "update_datetime"         => "0000-00-00 00:00:00",
                                );
        // 初期化
        $_billing_detail_work = $_billing_detail_init;

        // 明細レコード生成（プレゼンスアプリ利用料金）
        if (array_key_exists("presence_appli", $option_info) === true) {
            $presence_appli_option = $option_info['presence_appli'];
            $_billing_subs[] = $this->_edit_presence_charge($_billing_detail_init, "presence", $presence_appli_option);
        }

        // 明細レコード生成（メッセンジャー月額基本料金）
        if (array_key_exists("v3", $option_info) === true) {
            $v3_option = $option_info['v3'];
            $_billing_subs[] = $this->_edit_messenger_charge($_billing_detail_init, "messenger", $v3_option);
        }

        // 明細レコード生成（room_option合計情報）
        $_billing_subs[] = $this->_edit_total($_billing_detail_init, $_billing_subs);

        return $_billing_subs;

    }


    /**
     * 部屋タイトル情報の個別編集
     *
     */
    private function _edit_title($init=array(), $advance_info=null, $base_info=null, $charge_time=null)
    {
        $advance_service_info = $advance_info['service'];
        $base_service_info = $base_info['service'];
        // 出力項目設定
        $init['account_type_id'] = "_title_";
        $init['price'] = 0; // 単価
        $init['amount'] = 0; // 数量
        $init['charge'] = 0; // 料金
        $init['remarks'] = null; // 備考
        if ($base_service_info) {
            // 通話編集
            if ($base_service_info['service_freetime'] == 60000) {
                $_free_time_disp = "通話無制限";
            } else {
                $_free_time_disp = $base_service_info['service_freetime'] . "分";
            }
        }
        // サービス名称は役務提供日付の対象プランとする。
        // 利用時間と無料通話分は、前月までのプランを参照する。（暫定）
        if ($advance_service_info) {
            $_title_remarks = "ご利用プラン：".$advance_service_info['service_name']."\n".
                              "無料通話時間：".$_free_time_disp.
                              "　　　　課金対象利用時間: ".$charge_time."分";
            $init['remarks'] = $_title_remarks;
        }
        // サービス情報の埋め込み
        $serialize_info_wk = array(
                                 'plan_key' => $advance_service_info['service_key'],
                                 'plan_name' => $advance_service_info['service_name'],
                                 'version' => "version4",
                             );
        $init['serialize_info'] = serialize($serialize_info_wk);
        return $init;
    }


    /**
     * 部屋の基本料金の個別編集
     *
     */
    private function _edit_basic_charge($init=array(), $advance_info=null, $target_date=null)
    {
        $advance_service_info = $advance_info['service'];
        $advance_plan_info = $advance_info['plan'];
        $period_contract_flg = null; // 期間契約(PeriodContract)フラグ
        $price_on_flg = null; // 期間契約初回支払いフラグ
        $price = 0;
        $discount_rate = 0;
        $ret_account_info = array();
        $init['account_type_id'] = "basic";
        // 期間契約判定
        if ($advance_plan_info) {
            if ($advance_plan_info['contract_month_number'] > 1) { // 契約月数が2ヶ月以上で期間契約とする
                $period_contract_flg = "1";
            }
        }
        // 初回請求判定
        if ($period_contract_flg) {
            $_target_date = split("-", $target_date);
            $target_month = $_target_date[0] . "-" . $_target_date[1];
            $_start_date = split("-", $advance_plan_info['room_plan_starttime']);
            $start_month = $_start_date[0] . "-" . $_start_date[1];
            // $this->logger->info(__FUNCTION__, __FILE__, __LINE__, " ### target_month = " . $target_month);
            // $this->logger->info(__FUNCTION__, __FILE__, __LINE__, " ### start_month = " . $start_month);
            if ($target_month == $start_month) { // 初回請求だった場合
                $price_on_flg = "1";
                $discount_rate = $advance_plan_info['discount_rate'];
                $contract_month_number = $advance_plan_info['contract_month_number'];
            }
        } else {
            $price_on_flg = "1";
            $discount_rate = 0;
            $contract_month_number = 1;
        }
        // 単価設定
        if ($advance_service_info) {
            if ($price_on_flg) { // 契約月数が2ヶ月以上で期間契約とする
                $_price = $advance_service_info['service_charge'] * $contract_month_number * (100 - $discount_rate); // 単価
                //$this->logger->info(__FUNCTION__, __FILE__, __LINE__, "### discount_rate ...".$discount_rate);
                //$this->logger->info(__FUNCTION__, __FILE__, __LINE__, "### _price ...".$_price);
                if ($_price == 0) {
                    $price = 0; // 単価
                } else {
                    $price = floor($_price / 100); // 単価
                }
            } else {
                $price = 0; // 単価
            }
        }
        $init['price'] = $price; // 単価
        $init['amount'] = 1; // 数量
        $init['charge'] = $price * $init['amount']; // 料金
        // 必要な情報：期間契約月数、契約開始日、支払条件、請求処理月
        if ($advance_plan_info) {
            $ret_account_info = $this->get_account_info($init, $advance_plan_info, $advance_service_info, $price_on_flg);
            $init['account_type_name'] = $ret_account_info['name'];
            $init['remarks'] = $ret_account_info['data'];
            $serialize_info_wk = array(
                                     'start_date' => $advance_plan_info['room_plan_starttime'],
                                     'plan_key' => $advance_service_info['service_key'],
                                     'plan_name' => $advance_service_info['service_name'],
                                     'version' => "version4",
                                 );
            $init['serialize_info'] = serialize($serialize_info_wk);
        }
        //$this->logger->info(__FUNCTION__, __FILE__, __LINE__, $init);
        return $init;
    }


    /**
     * 部屋の追加料金の個別編集
     *
     */
    private function _edit_extend_charge($init=array(), $base_info=null, $charge_time=0)
    {
        $base_service_info = $base_info['service'];
        $base_plan_info = $base_info['plan'];
        $init['account_type_id'] = "extends"; // 課金科目
        if ($base_service_info) {
            $init['price'] = $base_service_info['service_additional_charge']; // 単価
            // 利用時間
            if ($base_service_info['service_additional_charge'] == 0) {
                $init['amount'] = 0;
            } else {
                // free time 換算
                $init['amount'] = $charge_time;
            }
        }
        $init['charge'] = $init['price'] * $init['amount']; // 料金
        // 必要な情報：年契約判定フラグ、年契約開始日、支払条件、請求処理月
        if ($base_plan_info) {
            $ret_account_info = $this->get_account_info($init, $base_plan_info);
            $init['account_type_name'] = $ret_account_info['name'];
            $init['unit'] = $ret_account_info['unit'];
            $init['remarks'] = $ret_account_info['data'];
        }
        $serialize_info_wk = array(
                                 'plan_key' => $base_service_info['service_key'],
                                 'plan_name' => $base_service_info['service_name'],
                                 'version' => "version4",
                             );
        $init['serialize_info'] = serialize($serialize_info_wk);
        return $init;
    }


    /**
     * オプション基本料金の共通編集
     *
     */
    private function _edit_option_basic_charge($init=array(), $room_info=array(), $base_info=null, $option_name=null)
    {
        $base_service_info = $base_info['service'];
        $base_plan_info = $base_info['plan'];
        $room_key = $room_info['room_key'];
        $amount = 0;
        $price = 0;
        $init['account_type_id'] = $option_name."_basic";
        if ($base_plan_info) {
            if (array_key_exists($room_key, $this->option_list) === false) {
            } else {
                $option_info = $this->option_list[$room_key];
            }
            if (array_key_exists($this->option_keys[$option_name], $this->service_option_list) === false) {
            } else {
                $service_option_info = $this->service_option_list[$this->option_keys[$option_name]];
            }
            if ($option_name == "audience") {
                // オーディエンスはオプション情報が正しくないことがあるので部屋の座席数より判断する
                $audiense_amount = ceil($room_info['max_audience_seat'] / 10); // 端数切上げ
                $init['amount'] = $audiense_amount; // 表示用数量
                $amount = $init['amount']; // 課金用数量
                $init['price'] = $service_option_info['price']; // 単価
            } else if ($option_name == "wb") {
                // 資料共有はオプション情報が正しくないことがあるので部屋の資料共有座席数より判断する
                $wb_amount = ceil($room_info['max_whiteboard_seat'] / 5); // 端数切上げ
                $init['amount'] = $wb_amount; // 表示用数量
                if (array_key_exists($base_plan_info['service_key'], $this->more_than_standard_plan_keys) !== true) {
                    $amount = $init['amount']; // 課金用数量
                    if ($base_plan_info['service_key'] == $this->wb_plan_key) {
                        $init['price'] = 20000; // 単価（資料共有プランに付与した場合、固定で20,000円となる）
                    } else {
                        $init['price'] = 5000; // 単価
                    }
                } else {
                    $amount = (($init['amount'] - 1) < 0 ? 0 : ($init['amount'] - 1)); // 課金用数量
                    $init['price'] = 5000; // 単価
                }
            } else { // iPad Plus, multicamera, telephone, smartphone はここを通過する
                // 資料共有とオーディエンス以外はオプション情報よりデータを作成する
                if ($option_info) {
                    if (array_key_exists($this->option_keys[$option_name], $option_info) === false) {
                    } else {
                        $service_option_info = $this->service_option_list[$this->option_keys[$option_name]];
                        $init['price'] = $service_option_info['price']; // 単価
                        $init['amount'] = $option_info[$this->option_keys[$option_name]]['count']; // 数量
                        $amount = $init['amount']; // 課金用数量（仮設定）
                        // プランに付属される数量の減算
                        switch ($option_name) {
                            case "mobile" :
                                if (array_key_exists($base_plan_info['service_key'], $this->more_than_standard_plan_keys) !== true) {
                                } else {
                                    $amount = (($init['amount'] - 1) < 0 ? 0 : ($init['amount'] - 1)); // 課金用数量
                                }
                                break;
                            case "sharing" :
                                if (array_key_exists($base_plan_info['service_key'], $this->more_than_standard_plan_keys) !== true) {
                                    if ($base_plan_info['service_key'] == $this->wb_plan_key) {
                                        $amount = (($init['amount'] - 1) < 0 ? 0 : ($init['amount'] - 1)); // 課金用数量
                                    }
                                } else {
                                    $amount = (($init['amount'] - 1) < 0 ? 0 : ($init['amount'] - 1)); // 課金用数量
                                }
                                break;
                            case "hispec" :
                                if ($base_plan_info['service_key'] == $this->hq_plan_key) {
                                    $amount = 0; // 課金用数量
                                }
                                break;
                            default :
                                break;
                        }
                    }
                }
            }
            $init['charge'] = $init['price'] * $amount; // 料金
            // 必要な情報：年契約判定フラグ、年契約開始日、支払条件、請求処理月
            $ret_account_info = $this->get_account_info($init, $base_plan_info);
            $init['account_type_name'] = $ret_account_info['name'];
            $init['unit'] = $ret_account_info['unit'];
            $init['remarks'] = $ret_account_info['data'];
            $serialize_info_wk = array(
                                     'plan_key' => $base_service_info['service_key'],
                                     'plan_name' => $base_service_info['service_name'],
                                     'version' => "version4",
                                 );
            $init['serialize_info'] = serialize($serialize_info_wk);
        }
        return $init;
    }


    /**
     * オプション追加料金の共通編集
     *
     */
    private function _edit_option_extend_charge($init=array(), $room_info=array(), $charge_info=null, $base_info=null, $option_name=null, $extend_type=null)
    {
        $base_service_info = $base_info['service'];
        $base_plan_info = $base_info['plan'];
        $option_info = array();
        $option_extends_name = null;
        if ($extend_type) {
            $option_extends_name = $option_name . "_extends_" . $extend_type;
        } else {
            $option_extends_name = $option_name . "_extends";
        }
        $init['account_type_id'] = $option_extends_name;
        if ($base_plan_info) {
            if (array_key_exists($room_info['room_key'], $this->option_list) === false) {
            } else {
                $option_info = $this->option_list[$room_info['room_key']];
            }
            if (array_key_exists($this->option_keys[$option_name], $this->service_option_list) === false) {
            } else {
                $service_option_info = $this->service_option_list[$this->option_keys[$option_name]];
            }

            if ($option_name == "audience") {
                $init['price'] = $service_option_info['running_price']; // 単価
                $amount = $charge_info[$option_extends_name];
                $init['amount'] = ($amount == null? 0: $amount);
                $init['charge'] = $init['price'] * $init['amount']; // 料金
                // v3足しこみ
                if (array_key_exists("v3_info", $room_info) !== true) {
                }else{
                    $v3_amount = $room_info['v3_info']['room'][$option_extends_name]['amount'];
                    $v3_charge = $room_info['v3_info']['room'][$option_extends_name]['charge'];
                    $init['amount'] += $v3_amount;
                    $init['charge'] += $v3_charge; // 料金
                }
            }else{
                if ($option_info) {
                    if (array_key_exists($this->option_keys[$option_name], $option_info) === false) {
                    } else {
                        //$this->logger->info(__FUNCTION__."user = ".$init['detail_group_id'], __FILE__, __LINE__, $service_option_info);
                        if (($option_name == "mobile") && ($extend_type == "special")) {
                            $price = 80; // 単価
                        } else {
                            $price = $service_option_info['running_price']; // 単価
                        }
                        $init['price'] = $price; // 単価
                        // 利用時間
                        if ($price == 0) {
                            $init['amount'] = 0;
                        } else {
                            $amount = $charge_info[$option_extends_name];
                            $init['amount'] = ($amount == null? 0: $amount);
                        }
                        // 高画質プランの値引き
                        if ($option_name == "hispec") {
                            if ($base_plan_info['service_key'] != $this->hq_plan_key) {
                                $init['charge'] = $init['price'] * $init['amount']; // 料金
                            } else {
                                $init['charge'] = 0; // 料金
                            }
                        } else {
                            $init['charge'] = $init['price'] * $init['amount']; // 料金
                        }
                    }



                    // v3足しこみ
                    if (array_key_exists("v3_info", $room_info) !== true) {
                    }else{
                        $v3_amount = $room_info['v3_info']['room'][$option_extends_name]['amount'];
                        $v3_charge = $room_info['v3_info']['room'][$option_extends_name]['charge'];
                        $init['amount'] += $v3_amount;
                        $init['charge'] += $v3_charge; // 料金
                    }
                }
            }
            // 必要な情報：年契約判定フラグ、年契約開始日、支払条件、請求処理月
            $ret_account_info = $this->get_account_info($init, $base_plan_info);
            $init['account_type_name'] = $ret_account_info['name'];
            $init['unit'] = $ret_account_info['unit'];
            $init['remarks'] = $ret_account_info['data'];
            $serialize_info_wk = array(
                                     'plan_key' => $base_service_info['service_key'],
                                     'plan_name' => $base_service_info['service_name'],
                                     'version' => "version4",
                                 );
            $init['serialize_info'] = serialize($serialize_info_wk);
        }
        return $init;
    }


    /**
     * オーディエンス初期費用の個別編集
     *
     */
    private function _edit_audience_first_charge($init=array(), $room_info=array(), $base_info=null)
    {
        $base_service_info = $base_info['service'];
        $room_key = $room_info['room_key'];
        $option_name="audience";
        $init['account_type_id'] = "audience_first";
        if (!$this->option_list) {
        } elseif (array_key_exists($room_key, $this->option_list) !== true) {
        } else {
            $option_info = $this->option_list[$room_key];
            if (array_key_exists($this->option_keys[$option_name], $option_info) !== true) {
            }else{
                $service_option_info = $this->service_option_list[$this->option_keys[$option_name]];
                $init['price'] = $service_option_info['init_price']; // 単価
                $init['amount'] = $option_info[$this->option_keys['audience']]['init_count']; // 数量
                $init['charge'] = $init['price'] * $init['amount']; // 料金
            }
        }
        // 必要な情報：年契約判定フラグ、年契約開始日、支払条件、請求処理月
        $ret_account_info = $this->get_account_info($init);
        $init['account_type_name'] = $ret_account_info['name'];
        $init['remarks'] = $ret_account_info['data'];
        $serialize_info_wk = array(
                                 'plan_key' => $base_service_info['service_key'],
                                 'plan_name' => $base_service_info['service_name'],
                                 'version' => "version4",
                             );
        $init['serialize_info'] = serialize($serialize_info_wk);
        return $init;
    }


    /**
     * プレゼンスアプリ利用料金の個別編集（ｖ４のみ）
     *
     */
    private function _edit_presence_charge($init=array(), $option_name=null, $_presence_info=array())
    {
        $init['account_type_id'] = $option_name."_basic";
        $init['price'] = 100; // 単価
        $init['amount'] = $_presence_info['member_count']; // 数量
        $init['charge'] = $init['price'] * $init['amount']; // 料金
        // 必要な情報：年契約判定フラグ、年契約開始日、支払条件、請求処理月
        $ret_account_info = $this->get_account_info($init);
        $init['account_type_name'] = $ret_account_info['name'];
        $init['unit'] = $ret_account_info['unit'];
        $init['remarks'] = $ret_account_info['data'];
        return $init;
    }


    /**
     * メッセンジャー料金の個別編集（ｖ３のみ）
     *
     */
    private function _edit_messenger_charge($init=array(), $option_name=null, $_v3=array())
    {
        $init['account_type_id'] = $option_name."_basic";
        if (!$this->service_option_list) {
        } elseif (array_key_exists($this->option_keys[$option_name], $this->service_option_list) !== true) {
        } else {
            $service_option_info = $this->service_option_list[$this->option_keys[$option_name]];
            $init['price'] = $service_option_info['price']; // 単価
            $init['amount'] = $_v3['v3_option']['messenger']['amount']; // 数量
            $init['charge'] = $_v3['v3_option']['messenger']['charge']; // 料金（ｖ３で計算済み）
        }
        // 必要な情報：年契約判定フラグ、年契約開始日、支払条件、請求処理月
        $ret_account_info = $this->get_account_info($init);
        $init['account_type_name'] = $ret_account_info['name'];
        $init['unit'] = $ret_account_info['unit'];
        $init['remarks'] = $ret_account_info['data'];
        return $init;
    }


    /**
     * 合計（小計）金額の個別編集
     *
     */
    private function _edit_total($init=array(), $detail_info=array())
    {
        $total_charge = 0;
        foreach ($detail_info as $key => $val) {
            $total_charge += $val['charge'];
        }
        $init['account_type_id'] = "_total_";
        $init['price'] = 0; // 単価
        $init['amount'] = 0; // 数量
        $init['charge'] = $total_charge; // 料金
        $this->_total_charge += $total_charge;

        // 必要な情報：年契約判定フラグ、年契約開始日、支払条件、請求処理月
        $ret_account_info = $this->get_account_info($init);
        $init['account_type_name'] = $ret_account_info['name'];
        $init['remarks'] = $ret_account_info['data'];
        // サービス情報の埋め込み
        $serialize_info_wk = array(
                                 'version' => "version4",
                             );
        $init['serialize_info'] = serialize($serialize_info_wk);

        return $init;
    }


    /**
     * 請求科目摘要欄個別編集処理
     *
     */
    private function get_account_info($val = array(), $plan_info=array(), $service_info=array(), $period_contract_init=null)
    {
        $ret = array();
        $_name = "";
        $_unit = "";
        $_data = "";
        switch ($val['account_type_id']) {
            case "basic" :
                $_unit = null;
                if ($plan_info['contract_month_number'] > 1) {
                    $limit_year = date("Y", strtotime($plan_info['room_plan_starttime']));
                    $limit_month = date("m", strtotime($plan_info['room_plan_starttime']));
                    $end_date = date("Y-m-d",strtotime("-1 day", mktime(0, 0, 0, $limit_month + $plan_info['contract_month_number'], 1, $limit_year)));
                    $discount_rate = number_format(((100 - $plan_info['discount_rate']) / 100), 2, '.', '');
                    $_name = "期間利用料";
                } else {
                    $_name = "月額基本料";
                }
                if ($plan_info['contract_month_number'] == 12) {
                    $_data = "（ 年間契約";
                    if ($period_contract_init) {
                        $_data .= "基本料 : ". number_format($service_info['service_charge']) . "×" . $plan_info['contract_month_number'] . "×" . $discount_rate;
                    }
                    $_data .= "　有効期限 : ". $end_date . " ）";
                } elseif ($plan_info['contract_month_number'] == 6) {
                    $_data = "（ 半年契約";
                    if ($period_contract_init) {
                        $_data .= "基本料 : ". $service_info['service_charge'] . "×" . $plan_info['contract_month_number'] . "×" . $discount_rate;
                    }
                    $_data .= "　有効期限 : ". $end_date . " ）";
                } elseif ($plan_info['contract_month_number'] >= 2) {
                    $_data = "（ " . $plan_info['contract_month_number'] . "ヶ月契約";
                    if ($period_contract_init) {
                        $_data .= "基本料 : ". $service_info['service_charge'] . "×" . $plan_info['contract_month_number'] . "×" . $discount_rate;
                    }
                    $_data .= "　有効期限 : ". $end_date . " ）";
                } else {
                    $_data = null;
                }
                break;
            case "extends" :
                $_name = "追加料金";
                $_unit = "分";
                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "分 ）";
                break;
            case "mobile_basic" :
                $_name = "携帯テレビ電話月額基本料金";
                $_unit = "回線";
                if (array_key_exists($plan_info['service_key'], $this->more_than_standard_plan_keys) === false) {
                    $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "回線 ）";
                } else {
                    $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "回線　内1回線　プランに付属 ）";
                }
                break;
            case "mobile_extends_normal" :
                $_name = "携帯テレビ電話追加料金 \n（平日8時から19時）";
                $_unit = "分";
                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "分 ）";
                break;
            case "mobile_extends_special" :
                $_name = "携帯テレビ電話追加料金 \n（19時から翌朝8時及び土、日曜祝日）";
                $_unit = "分";
                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "分 ）";
                break;
            case "h323_basic" :
                $_name = "Ｈ.３２３通信月額基本料金";
                $_unit = "回線";
                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "回線 ）";
                break;
            case "h323_extends" :
                $_name = "Ｈ.３２３通信追加料金";
                $_unit = "分";
                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "分 ）";
                break;
            case "ssl_basic" :
                $_name = "暗号化オプション";
                $_unit = null;
                $_data = null;
                break;
            case "hispec_basic" :
                $_name = "高画質モード基本料金";
                $_unit = null;
                if ($plan_info['service_key'] != $this->hq_plan_key) {
                    $_data = null;
                } else {
                    $_data = "（ プラン付属 ）";
                }
                break;
            case "hispec_extends" :
                $_name = "高画質モード追加料金";
                $_unit = "分";
                if ($plan_info['service_key'] != $this->hq_plan_key) {
                    $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "分 ）";
                } else {
                    $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "分 ）（ プラン付属 ）";
                }
                break;
            case "sharing_basic" :
                $_name = "デスクトップシェアリング";
                $_unit = null;
                if (array_key_exists($plan_info['service_key'], $this->more_than_standard_plan_keys) === false) {
                    $_data = null;
                } else {
                    $_data = "（ プラン付属 ）";
                }
                break;
            case "hdd_basic" :
                $_name = "追加録画容量";
                $_unit = "ＧＢ";
                $_data = "（ 追加容量 ". number_format($val['amount']) . "ＧＢ ）";
                break;
            case "compact_basic" :
                $_name = "コンパクト";
                $_unit = null;
                $_data = null;
                break;
            case "audience_first" :
                $_name = "オーディエンス機能初期費用";
                $_unit = "地点";
                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "地点 ）";
                break;
            case "audience_basic" :
                $_name = "オーディエンス機能基本料金";
                $_unit = "地点";
                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "地点 ）";
                break;
            case "audience_extends" :
                $_name = "オーディエンス機能利用料金";
                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "分 ）";
                break;
            case "presence_basic" :
                $_name = "プレゼンスアプリ利用料金";
                $_unit = "名";
                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "名 ）";
                break;
            case "messenger_basic" :
                $_name = "メッセンジャー利用料金";
                $_unit = "名";
                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "名 ）";
                break;
            case "multicamera_basic" :
                $_name = "マルチカメラオプション";
                $_unit = null;
                $_data = null;
                break;
            case "telephone_basic" :
                $_name = "電話連携オプション";
                $_unit = "契約";
                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "契約 ）";
                break;
            case "smartphone_basic" :
                $_name = "スマートフォン連携オプション";
                $_unit = "契約";
                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "契約 ）";
                break;
            case "wb_basic" :
                $_name = "iPad Basic オプション";
                $_unit = "契約";
                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "契約 ）";
                break;
            case "wb_video_basic" :
                $_name = "iPad Plus オプション";
                $_unit = "契約";
                $_data = "（ ￥". number_format($val['price']) . " × " . number_format($val['amount']) . "契約 ）";
                break;
            case "_total_" :
                if ($val['detail_group_id'] == "_option_") {
                    $_name = "その他オプション小計";
                } else {
                    $_name = "小計";
                }
                $_unit = null;
                $_data = null;
                break;
        }
        $ret = array(
                    "name" => $_name,
                    "unit" => $_unit,
                    "data" => $_data,
                );
        // $this->logger->info(__FUNCTION__, __FILE__, __LINE__, $ret);
        return $ret;
    }


    /**
     * DataDBのDSN一覧取得
     *
     */
    private function _get_dsn()
    {
        static $server_list;
        if (!$server_list) {
            $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        }
        if (!$server_list) {
            return null;
        } elseif (!is_array($server_list)) {
            return null;
        } else {
            if (array_key_exists("SERVER_LIST", $server_list)) {
                return $server_list["SERVER_LIST"];
            } else {
                return null;
            }
        }
    }


    /**
     * 請求内容チェック
     *
     */
    private function valid_invoice_info($user_info=array())
    {
        $ret = true;
        $user_key = $user_info['user_key'];
        // 請求対象にすべきかの妥当性チェック
        if ($user_info['account_model'] === "member") {
            if (array_key_exists($user_key, $this->user_plan_list) === false) {
                $ret = false; // 請求対象外にする
            }
        } else {
            // 部屋の確認（部屋課金契約の場合）
            if (array_key_exists($user_key, $this->room_list) === false) {
                $ret = false; // 請求対象外にする
            } else {
                foreach ($this->room_list[$user_key] as $room_key => $room_val) {
                    if (array_key_exists($room_key, $this->plan_list) === false) {
                        unset($this->room_list[$user_key][$room_key]);
                        if (array_key_exists($room_key, $this->option_list) === false) {
                        } else {
                            unset($this->option_list[$room_key]);
                        }
                    } else {
                        $service_key = $this->plan_list[$room_key]['service_key'];
                        if (array_key_exists($service_key, $this->service_list) === false) {
                            unset($this->room_list[$user_key][$room_key]);
                            unset($this->plan_list[$room_key]);
                            if (array_key_exists($room_key, $this->option_list) === false) {
                            } else {
                                unset($this->option_list[$room_key]);
                            }
                        }
                    }
                }
                // 請求対象となるｖ４の部屋情報がなくなった場合
                if (empty($this->room_list[$user_key])) {
                    $ret = false; // 請求対象外にする
                }
            }
        }
        // メンバー課金契約の場合
        return $ret;
    }


    /**
     * プラン価格一覧表取得
     *
     */
    function getServiceList()
    {
        $ret = null;
        $service_obj = new ServiceTable(N2MY_MDB_DSN);
        // 有効なサービス一覧取得
        $where = " `service_charge` != 0 OR `service_additional_charge` != 0 ";
        $ret = $service_obj->getRowsAssoc($where, null, null, null, null, "service_key");
        return $ret;
    }


    /**
     * オプション価格一覧表取得
     *
     */
    function getOptionPriceList($country_key=null)
    {
        $ret = null;
        $list = null;
        $service_option_obj = new ServiceOptionTable(N2MY_MDB_DSN);
        // 有効なオプション一覧取得
        $where = null;
        if ($country_key) {
            $where = " country_key = '" . addslashes($country_key) . "'";
        }
        $list = $service_option_obj->getRowsAssoc($where);
        if(DB::isError($list)) {
            $this->logger->info(__FUNCTION__." # Error Is ... ",__FILE__,__LINE__, $list->getMessage());
            return $list;
        }
        $ret = array();
        foreach($list as $key => $val) {
            $ret[$val['service_option_key']]['name'] = $val['service_option_name'];
            $ret[$val['service_option_key']]['price'] = $val['service_option_price'];
            $ret[$val['service_option_key']]['init_price'] = $val['service_option_init_price'];
            $ret[$val['service_option_key']]['running_price'] = $val['service_option_running_price'];
        }
        return $ret;
    }


    /**
     * その時点で有効なユーザ一覧を取得
     * (user_status 0:未開始 1:開始 2:トライアル)
     * (user_delete_status 0:有効 1:削除)
     * 条件：
     * 　ユーザーＩＤが空以外　かつ
     * 　トライアルアカウント以外　かつ
     * 　請求年月月初より前に開始されている。　かつ
     * 　（削除ステータス以外　もしくは
     * 　　削除日が請求年月月初以降）
     * のもの
     *
     */
    function getEffectiveUserList($data_db_dsn=null, $billing_date = null, $target_base_date = null)
    {
        $rows = null;
        $user_obj = new UserTable($data_db_dsn);
        // ID指定があった場合
// test condition area start
//		$test_user_id = " 'jsrwebmtg' "; // ｖ３テスト
//		$test_user_id = " 'jigyo', 'jsrwebmtg', 'being', 'synthes', 'kgtvsys' "; // ｖ３テスト
//		$test_user_id = " 'ad010', 'kawada', 'kgtvsys' "; //オーディエンステスト
//		$test_user_id = " 'oec-wm' "; // messengerテスト
//		$where = " `user_id` in (" . $target_user_id . ") "; // テスト用ユーザ指定（呼び出し側で指定する）
// test condition area  end
        $where = " LENGTH(user_id) > 0 "; // IDが空じゃない
        $where .= " AND `invoice_flg` = 1 " . // 請求対象アカウント
                 " AND `user_status` = 1 " . // 利用開始ユーザのみ（トライアルではない）
                 " AND `user_group` IS NULL " . // グループ課金ユーザ以外
                 " AND `user_starttime` < '" . $billing_date . "' " . // サービス開始済み
                 " AND ( (`user_delete_status` != 2) OR  " .
                 "       (`user_delete_status`  = 2 AND " .
                 "        `user_deletetime` > '". $target_base_date ."') ) " . // 有効 or 削除日が請求月の月初後
                 " ";
        $coulumns = " `user_key`, `country_key`, `user_id`, `account_model`, `max_member_count`, `max_rec_size`, `user_company_name`, " .
                    " `user_company_postnumber`, `user_company_address`, `user_company_phone`, `user_company_fax`, " .
                    " `user_staff_firstname`, `user_staff_lastname`, `user_staff_email`, `addition`, `staff_key`, `payment_terms` " .
                    " ";
        $rows = $user_obj->getRowsAssoc($where, null, null, null, $coulumns);
        return $rows;
    }


    /**
     * その時点で有効な部屋一覧を取得
     * (room_status 0:未開始 1:開始)
     * 条件：
     * 　請求年月月初より前に開始されている　かつ
     * 　（ステータスが有効　もしくは
     * 　　削除日が請求年月月初以降）
     * のもの
     *
     */
    function getEffectiveRoomList($data_db_dsn=null, $billing_date = null, $target_base_date = null)
    {
        $rows = null;
        $_rows = null;
        $room_obj = new RoomTable($data_db_dsn);
        $where = " `room_registtime` < '". $billing_date ."' ".
                 " AND ((`room_status`  = 1) OR " .
                 "      (`room_status` != 1 AND `room_deletetime` > '". $target_base_date ."')) ";
        $coulumns = " `user_key`, `room_key`, `room_name`, `max_seat`, `max_audience_seat`, `room_registtime` ";
        $rows = $room_obj->getRowsAssoc($where, null, null, null, $coulumns);
        foreach ($rows as $key => $val) {
            $_rows[$val['user_key']][$val['room_key']] = $val;
        }
        return $_rows;
    }


    /**
     * プランの情報取得（基本料金用のプラン）
     *
     */
    function getAdvanceRoomPlan($room_key=null, $target_date=null)
    {
        $rows = null;
        // 基本料金取得対象のプラン取得
        $where = " `room_key` = '". $room_key ."' " .
                 " AND ((`room_plan_starttime` >= '". $target_date ." 00:00:00') OR " .
                 "      (`room_plan_starttime` < '". $target_date ." 00:00:00' " .
                 " AND ((`room_plan_endtime` IS NULL) OR " .
                 "      (`room_plan_endtime` = '0000-00-00 00:00:00') OR " .
                 "      (`room_plan_endtime` > '". $target_date ." 00:00:00')))) " .
                 " AND ((`room_plan_status` = 1) OR " .
                 "      (`room_plan_status` = 2)) ";
//        $this->logger->info(__FUNCTION__." # R Where ... ",__FILE__,__LINE__, $where);
        $coulumns = " `room_key`, `service_key`, `room_plan_starttime`, `room_plan_endtime`, `discount_rate`, `contract_month_number` ";
//        $rows = $this->room_plan_obj->getRowsAssoc($where, null, null, null, $coulumns, "room_key");
        $rows = $this->room_plan_obj->getRowsAssoc($where, null, null, null, $coulumns);
        return $rows[0];
    }


    /**
     * その時点で有効な部屋プラン一覧を取得（オプション算出用）
     * 条件：
     * 　請求年月月初より前に開始されている　かつ
     * 　（ステータスが有効　もしくは
     * 　　更新日が請求年月月初以降）
     * のもの
     *
     */
    function getEffectivePlanList($billing_date = null, $target_base_date = null)
    {
        $rows = null;
        $where = " `room_plan_starttime` < '". $billing_date ."' " .
                 " AND ((`room_plan_status`  = 1) OR " .
                 "      (`room_plan_status` != 1 AND `room_plan_updatetime` > '". $target_base_date ."')) ";
        $coulumns = " `room_key`, `service_key`, `room_plan_starttime`, `room_plan_endtime`, `discount_rate`, `contract_month_number` ";
        $rows = $this->room_plan_obj->getRowsAssoc($where, null, null, null, $coulumns, "room_key");
        return $rows;
    }


    /**
     * その時点で有効なオプション一覧を取得
     * 条件：
     * 　請求年月月初より前に開始されている　かつ
     * 　（ステータスが有効　もしくは
     * 　　削除日が請求年月月初以降）
     * のもの
     *
     */
    function getEffectiveOptionList($data_db_dsn=null, $billing_date = null, $target_base_date = null)
    {
        $rows = null;
        $_rows = null;
        $ordered_option_obj = new OrderedServiceOptionTable($data_db_dsn);
        $where = " `ordered_service_option_starttime` < '". $billing_date ."' " .
                 " AND ((`ordered_service_option_status`  = 1) OR " .
                 "      (`ordered_service_option_status` != 1 AND " .
                 "       `ordered_service_option_deletetime` > '". $target_base_date ."')) ";
        $sort = array(
                    "room_key" => "ASC",
                    "service_option_key" => "ASC",
                );
        $coulumns = " `room_key`, `service_option_key`, `ordered_service_option_starttime` ";
        $rows = $ordered_option_obj->getRowsAssoc($where, null, null, null, $coulumns);
        foreach ($rows as $key => $val) {
            $_rows[$val['room_key']][$val['service_option_key']]['service_option_key'] = $val['service_option_key'];
            $_rows[$val['room_key']][$val['service_option_key']]['count'] += 1;
            if (($val['ordered_service_option_starttime'] < $billing_date) &&
                ($val['ordered_service_option_starttime'] >= $target_base_date)) {
                // 初期費用カウント（初期費は付与した月の請求分にしか上乗せしない。）
                $_rows[$val['room_key']][$val['service_option_key']]['init_count'] += 1;
            } else {
                $_rows[$val['room_key']][$val['service_option_key']]['init_count'] += 0;
            }
        }
        return $_rows;
    }


    /**
     * メンバープランの情報取得（基本料金のターゲット）
     *
     */
    function getAdvanceUserPlan($user_key=null, $target_date=null)
    {
        $rows = null;
        // 基本料金取得対象のプラン取得
        $where = " `user_key` = '". $user_key ."' " .
                 " AND ((`user_plan_starttime` >= '". $target_date ." 00:00:00') OR " .
                 "      (`user_plan_starttime` < '". $target_date ." 00:00:00' " .
                 " AND ((`user_plan_endtime` IS NULL) OR " .
                 "      (`user_plan_endtime` = '0000-00-00 00:00:00') OR " .
                 "      (`user_plan_endtime` >= '". $target_date ." 00:00:00')))) " .
                 " AND ((`user_plan_status` = 1) OR " .
                 "      (`user_plan_status` = 2)) ";
//        $this->logger->info(__FUNCTION__." # U Where ... ",__FILE__,__LINE__, $where);
        $coulumns = " `user_key`, `service_key`, `user_plan_starttime`, `user_plan_endtime`, `discount_rate`, `contract_month_number` ";
//        $rows = $this->user_plan_obj->getRowsAssoc($where, null, null, null, $coulumns, "user_key");
        $rows = $this->user_plan_obj->getRowsAssoc($where, null, null, null, $coulumns);
        return $rows[0];
    }


    /**
     * その時点で有効なメンバーアカウントプラン一覧を取得
     * 条件：
     * 　請求年月月初より前に開始されている　かつ
     * 　（ステータスが有効　もしくは
     * 　　更新日が請求年月月初以降）
     * のもの
     *
     */
    function getEffectiveUserPlanList($data_db_dsn=null, $billing_date = null, $target_base_date = null)
    {
        $rows = null;
        $where = " `user_plan_starttime` < '". $billing_date ."' " .
                 " AND ((`user_plan_status`  = 1) OR " .
                 "      (`user_plan_status` != 1 AND `user_plan_updatetime` > '". $target_base_date ."')) ";
        $coulumns = " `user_key`, `service_key`, `user_plan_starttime`, `user_plan_endtime`, `discount_rate`, `contract_month_number` ";
        $rows = $this->user_plan_obj->getRowsAssoc($where, null, null, null, $coulumns, "user_key");
        return $rows;

    }


    /**
     * その時点で有効なメンバーオプション一覧を取得
     * 条件：
     * 　請求年月月初より前に開始されている　かつ
     * 　（ステータスが有効　もしくは
     * 　　更新日が請求年月月初以降）
     * のもの
     * $billing_date ･･･ 当月
     * $target_base_date ･･･ 前月
     *
     */
    function getEffectiveUserOptionList($data_db_dsn=null, $billing_date = null, $target_base_date = null)
    {
        $_rows = null;
        $user_option_obj = new UserServiceOptionTable($data_db_dsn);
        $where = " `user_service_option_starttime` < '". $billing_date ."' " .
                 " AND ((`user_service_option_status`  = 1) OR " .
                 "      (`user_service_option_status` != 1 AND " .
                 "       `user_service_option_deletetime` > '". $target_base_date ."')) ";
        $sort = array(
                    "user_key" => "ASC",
                    "service_option_key" => "ASC",
                );
        $coulumns = " `user_key`, `service_option_key`, `user_service_option_starttime` ";
        $rows = $user_option_obj->getRowsAssoc($where, null, null, null, $coulumns);
        foreach ($rows as $key => $val) {
            if ($val['service_option_key'] == $this->user_service_option_id_support) {
                // 導入サポートだけは発生したときのみの課金となる
                if (($val['user_service_option_starttime'] < $billing_date) &&
                    ($val['user_service_option_starttime'] >= $target_base_date)) {
                    $_rows[$val['user_key']][$val['service_option_key']]['service_option_key'] = $val['service_option_key'];
                    $_rows[$val['user_key']][$val['service_option_key']]['count'] += 1;
                }
            } else {
                $_rows[$val['user_key']][$val['service_option_key']]['service_option_key'] = $val['service_option_key'];
                $_rows[$val['user_key']][$val['service_option_key']]['count'] += 1;
                if (($val['user_service_option_starttime'] < $billing_date) &&
                    ($val['user_service_option_starttime'] >= $target_base_date)) {
                    // 初期費用カウント（初期費は付与した月の請求分にしか上乗せしない。）
                    $_rows[$val['user_key']][$val['service_option_key']]['init_count'] += 1;
                } else {
                    $_rows[$val['user_key']][$val['service_option_key']]['init_count'] += 0;
                }
            }
        }
        return $_rows;
    }


    /**
     * 期間を指定して、月間集計を行う
     * （メンバ利用情報）
     * 注意：集計する月に前月利用分の集計を行うので
     */
    function getMemberUsageDetailList($data_db_dsn=null, $biling_month=null, $target_month=null, $user_id_list = array())
    {
        $member_usage_obj = new DBI_MemberUsageDetails($data_db_dsn);
        $start_date = "";
        $end_date = "";
        $sql = "SELECT " .
               " '" . $target_month . "' as log_month" .
               ", `user_key`" .
               ", `member_key`" .
               ", sum(`use_count`) as extends" .
               " FROM `member_usage_details` " .
               " WHERE `create_datetime` LIKE '" . addslashes($biling_month) . "%' ";
        if ($user_id_list) { // userの指定があれば対象のuserの分だけの月間集計を実施する。
            $sql .= " AND `user_id` in ('" . implode("','", $user_id_list) . "') ";
        }
        $sql .= " GROUP BY `user_key`, `member_key` " .
                " ORDER BY `user_key`, `member_key` ";

        $rs = $member_usage_obj->_conn->query($sql);
//        $this->logger->info(__FUNCTION__,__FILE__,__LINE__,$sql);
        if(DB::isError($rs)) {
            $this->logger->info(__FUNCTION__." # Error Is ... ",__FILE__,__LINE__, $rs->getMessage());
            return $rs;
        }
        $ret = array();
        while ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            $ret[$row['user_key']][$row['member_key']] = $row;
        }
//        $this->logger->info(__FUNCTION__." # Result ... ",__FILE__,__LINE__, $ret);
        return $ret;
    }


    /**
     * その時点で有効なメンバー一覧を取得
     * 条件：
     * 　請求年月月初より前に開始されている　かつ
     * 　（ステータスが有効　もしくは
     * 　　更新日が請求年月月初以降）
     * のもの
     * （削除日が必要だと思う）
     *
     * 追記：請求管理で必要が無くなったので、他で使ってなければ削除します。
     *
     * 注）member_status (0:有効、-1:無効)
     *
     */
    function getEffectiveMemberCountList($data_db_dsn=null, $billing_date=null, $target_base_date=null)
    {
        $member_obj = new MemberTable($data_db_dsn);
        $where = " `create_datetime` < '". $billing_date ."' " .
                 " AND ((`member_status`  = 0) OR " .
                 "      (`member_status` != -1 AND " .
                 "       `update_datetime` > '". $target_base_date ."')) ";
        $sort = array(
                    "user_key" => "ASC",
                    "member_key" => "ASC",
                );
        $coulumns = " `user_key`, `member_key` ";
        $rows = $member_obj->getRowsAssoc($where, $sort, null, null, $coulumns, null);
        foreach ($rows as $_key => $_val) {
            $_rows[$_val['user_key']] += 1;
        }
        return $_rows;
    }


    /**
     * 役務提供（計上）年月算出
     *
     */
    function _get_summing_up_date($billing_main=null) {
        $ret = null;
        $year = $billing_main['billing_year'];
        $month = $billing_main['billing_month'];
        $payment_terms = $billing_main['payment_terms'];
        $invoice_type = $billing_main['invoice_type'];
        if ("init" == $invoice_type) {
            $ret = $year . "-" . $month . "-01";
        } else {
            $billing_datetime = mktime(0, 0, 0, $month, 1, $year);
            switch ($payment_terms) {
                case "pre" :
                    $ret = date("Y-m", strtotime("+1 month", $billing_datetime));
                    break;
                case "on" :
                    $ret = $year . "-" . $month;
                    break;
                case "post" :
                    $ret = date("Y-m", strtotime("-1 month", $billing_datetime));
                    break;
                default :
                    $ret = date("Y-m", strtotime("-1 month", $billing_datetime));
                    break;
            }
            $ret = $ret . "-01";
        }
        //$this->logger->info(__FUNCTION__,__FILE__,__LINE__ . " ### SummingUpDate = " . $ret,$billing_main);
        return $ret;
    }


    /**
     * ミーティングV3情報取得
     * @param string $year （請求年）
     * @param string $month （請求月）
     * @param string $client_key （クライアントＩＤを設定）
     * @param string $reseller_id （代理店ＩＤを設定）
     * @param string $mode （常に再計算して持ってくる設定）
     *
     */
    function _get_v3_info($year=null, $month=null, $reseller_id="all", $mode="1") {
        $data = array();

        // $client_keys = serialize($client_key_list);

        // ｖ３請求ＡＰＩ引数編集
        $param = array(
//			"key"           => $client_keys,
            "mode"           => "invoice", // 支払い処理を行わない
            "reseller_key"  => $reseller_id,
            "year"          => $year,
            "month"         => $month,
            "retry"         => $mode
        );
        // 請求データ取得ＡＰＩのＵＲＬ取得（各プロダクト）
        $url = $this->v3_url;

        // ＵＲＬ内容取得
        $_url = parse_url($url);
        if ($_url["scheme"] == "https") {
            $url = "http://";
            if ($_url["user"]) {
                $url .= $_url["user"].":";
            }
            if ($_url["pass"]) {
                $url .= $_url["pass"]."@";
            }
            $url .= $_url["host"].$_url["path"];
            if ($_url["query"]) {
                $url .= "?".$_url["query"];
            }
        }
        $contents = "";
        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_USERPWD, $this->v3_account);
        curl_setopt ($ch, CURLOPT_HTTPGET, true);
        curl_setopt ($ch, CURLOPT_HEADER, 0);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1); // TIMEOUT設定
        // curl_setopt($ch, CURLOPT_TIMEOUT, 1); // TIMEOUT設定
        if ($param) {
            $fields = "";
            while (list($key, $val)=each($param)) {
                $fields .= "$key=" . urlencode( $val ) . "&";
            }
            curl_setopt ($ch, CURLOPT_POSTFIELDS, rtrim( $fields, "& " ));
        }
        $this->logger->info(__FUNCTION__."# v3 api info (curl) ... ", __FILE__, __LINE__,
            array(
                "url" => $url,
                "account" => $this->v3_account,
                "param" => $fields,
            )
        );
        $contents = curl_exec($ch);
        $this->logger->info(__FUNCTION__."# v3 api result ... [". curl_errno($ch) ."]", __FILE__, __LINE__);
        if (curl_errno($ch) != 0) { // cUrlエラー発生
            $this->logger->info(__FUNCTION__."# v3 api cUrl Err Msg ... ", __FILE__, __LINE__, curl_error($ch));
        }
        curl_close($ch);
        $v3_billing_data = unserialize($contents);
        if ($v3_billing_data) {
            $data = $v3_billing_data;
        } else {
            $data = null;
        }

        return $data;
    }

    /**
     * ミーティングV3情報リスト編集
     */
    function _get_v3_data_sub_list($main_data=array(), $v3data_list=array()) {
        $ret = array();

        // 計上年月
        $target_datetime = mktime(0, 0, 0, $main_data['billing_month'], 1, $main_data['billing_year']);

        if ($v3data_list['room_count'] != 0) {
            if ($main_data['payment_terms'] == "pre") {
                $ret['target_month'] = date("m",strtotime("+1 month", $target_datetime));
            }elseif ($main_data['payment_terms'] == "on") {
                $ret['target_month'] = date("m", $target_datetime);
            }elseif ($main_data['payment_terms'] == "post") {
                $ret['target_month'] = date("m",strtotime("-1 month", $target_datetime));
            }
            foreach ($v3data_list['room_info'] as $key => $val) {
                $ret[$key]['room_info'] = $val;
                $ret[$key]['room_usetime'] = $v3data_list['room_usetime'][$key];
                $ret[$key]['sv_freetime'] = $v3data_list['sv_freetime'][$key];
                $ret[$key]['charge_time'] = $v3data_list['charge_time'][$key];
                $ret[$key]['base_charge'] = $v3data_list['base_charge'][$key];
                $ret[$key]['target_date'] = $v3data_list['target_date'][$key];
                $ret[$key]['yearly'] = $v3data_list['yearly'][$key];
                $ret[$key]['extra_charge'] = $v3data_list['extra_charge'][$key];
                $ret[$key]['mobile'] = $v3data_list['mobile'][$key];
                $ret[$key]['h323'] = $v3data_list['h323'][$key];
                $ret[$key]['ssl'] = $v3data_list['ssl'][$key];
                $ret[$key]['hispec'] = $v3data_list['hispec'][$key];
                $ret[$key]['share'] = $v3data_list['share'][$key];
                $ret[$key]['hdd'] = $v3data_list['hdd'][$key];
                $ret[$key]['compact'] = $v3data_list['compact'][$key];
                if (array_key_exists('audience_fixed', $v3data_list)) {
                    $ret[$key]['audience_fixed'] = $v3data_list['audience_fixed'][$key];
                }
                if (array_key_exists('audience_running', $v3data_list)) {
                    $ret[$key]['audience_running'] = $v3data_list['audience_running'][$key];
                }
                $ret[$key]['sub_total'] = $v3data_list['sub_total'][$key];
            }
            if (array_key_exists('messenger', $v3data_list)) {
                $ret['messenger'] = $v3data_list['messenger'];
            }
        }
        return $ret;
    }

    /**
     * ミーティングV3追加料金情報加算
     */
    function _add_v3_extends_info($_4list=array(), $_3info=array()) {
        $ret = array();
        foreach($_4list as $key => $_4info){
            $ret[$key] = $_4info;
            $ret[$key]['v3_info']['charge_time'] = str_replace(",", "", $_3info['room_info'][0]['charge_time']);
            $ret[$key]['v3_info']['room']['extends']['charge'] = 0 + str_replace(",", "", $_3info['room_info'][0]['extra_charge']['charge']);
            $ret[$key]['v3_info']['room']['extends']['amount'] = 0 + str_replace(",", "", $_3info['room_info'][0]['extra_charge']['time']);
            $ret[$key]['v3_info']['room']['mobile_extends_normal']['charge'] = 0 + str_replace(",", "", $_3info['room_info'][0]['mobile']['extra']);
            $ret[$key]['v3_info']['room']['mobile_extends_normal']['amount'] = 0 + str_replace(",", "", $_3info['room_info'][0]['mobile']['time']);
            $ret[$key]['v3_info']['room']['mobile_extends_special']['charge'] = 0 + str_replace(",", "", $_3info['room_info'][0]['mobile']['extra2']);
            $ret[$key]['v3_info']['room']['mobile_extends_special']['amount'] = 0 + str_replace(",", "", $_3info['room_info'][0]['mobile']['time2']);
            $ret[$key]['v3_info']['room']['h323_extends']['charge'] = 0 + str_replace(",", "", $_3info['room_info'][0]['h323']['extra']);
            $ret[$key]['v3_info']['room']['h323_extends']['amount'] = 0 + str_replace(",", "", $_3info['room_info'][0]['h323']['time']);
            $ret[$key]['v3_info']['room']['hispec_extends']['charge'] = 0 + str_replace(",", "", $_3info['room_info'][0]['hispec']['extra']);
            $ret[$key]['v3_info']['room']['hispec_extends']['amount'] = 0 + str_replace(",", "", $_3info['room_info'][0]['hispec']['time']);
            $ret[$key]['v3_info']['room']['audience_extends']['charge'] = 0 + str_replace(",", "", $_3info['audience_running']['charge']);
            $ret[$key]['v3_info']['room']['audience_extends']['amount'] = 0 + str_replace(",", "", $_3info['audience_running']['time']);
        }
        return $ret;
    }

    /**
     * ミーティングV3オプション追加料金情報加算
     */
    function _add_v3_option_info($_3info=array()) {
        $ret = array();
        $ret['v3_option']['messenger']['charge'] = str_replace(",", "", $_3info['messenger']['charge']);
        $ret['v3_option']['messenger']['amount'] = str_replace(",", "", $_3info['messenger']['number']);
        $ret['v3_option']['sub_total'] = str_replace(",", "", $_3info['messenger']['charge']);
        return $ret;
    }

    /**
     * ミーティングV3情報登録用編集
     */
    function _set_v3_sub_data($v3data=array()) {

        $ret = array();
        $account_type_number = 1;
        $_billing_detail_init = array(
                                   "billing_main_id"     => null,
//                                   "detail_group_number" => $sub_count,
                                   "detail_group_number" => null,
                                   "detail_group_id"     => $v3data['room_key'],
                                   "detail_group_name"   => $v3data['room_name'],
                                   "account_type_number" => null,
                                   "account_type_id"     => null,
                                   "account_type_name"   => null,
                                   "charge"              => 0,
                                   "amount"              => 0,
                                   "unit"                => null,
                                   "price"               => 0,
                                   "remarks"             => null,
                                   "serialize_info"      => null,
                               );
        $serialize_info_wk = array(
                                 'plan_key' => $v3data['service_key'],
                                 'plan_name' => $v3data['service_name'],
                                 'version' => "version3",
                             );
        $_serialize_info = serialize($serialize_info_wk);

        // 初期化
// _title_
        $data = $_billing_detail_init;
        $data['account_type_number'] = $account_type_number;
        $data['account_type_id'] = "_title_";
        //$data['account_type_name'] = null;
        $data['charge'] = 0;
        $data['amount'] = 0;
        $data['unit'] = null;
        $data['price'] = 0;
        // 通話時間文言編集
        $service_freetime = str_replace(",", "", $v3data['service_freetime']);
        if ($service_freetime == 60000) {
            $_free_time_disp = "通話無制限";
        } else {
            $_free_time_disp = $service_freetime;
        }
        // 備考内容編集
        $_title_remarks = "ご利用プラン：".$v3data['service_name']."\n".
                          "無料通話時間：".$_free_time_disp."　　バージョン: Version3".
                          "　　課金対象利用時間: ".str_replace(",", "", $v3data['extra_charge']['charge'])."分";
        $data['remarks'] = $_title_remarks;
        $data['serialize_info'] = $_serialize_info;
        $ret[] = $data;
        $account_type_number += 1;

// basic
        $data = $_billing_detail_init;
        $service_charge = str_replace(",", "", $v3data['service_charge']);
        $data['account_type_number'] = $account_type_number;
        $data['account_type_id'] = "basic";
        $data['account_type_name'] = "月額基本料";
        $data['amount'] = 1;
        $data['unit'] = null;
        $data['price'] = $service_charge;
        $data['charge'] = $service_charge;
        // 備考欄編集
        if ($v3data['yearly']['yearly'] == "1") {
            $yearly_limit = date("Y-m-d",strtotime("+12 month", time($v3data['yearly']['start'])));
            $_remarks = "( 年間契約　有効期限 : ". $yearly_limit . " )";
        }else{
            $_remarks = null;
        }
        $data['remarks'] = $_remarks;
        $data['serialize_info'] = $_serialize_info;
        $ret[] = $data;
        $account_type_number += 1;

// extends
        $data = $_billing_detail_init;
        $data['account_type_number'] = $account_type_number;
        $data['account_type_id'] = "extends";
        $data['account_type_name'] = "追加料金";
        $data['charge'] = str_replace(",", "", $v3data['extra_charge']['charge']);
        $data['amount'] = str_replace(",", "", $v3data['extra_charge']['time']);
        $data['unit'] = "分";
        $data['price'] = str_replace(",", "", $v3data['extra_charge']['rate']);
        $_remarks = "( ￥". number_format($data['price']) . " × " . number_format($data['amount']) . "分 )";
        $data['remarks'] = $_remarks;
        $data['serialize_info'] = $_serialize_info;
        $ret[] = $data;
        $account_type_number += 1;

// mobile_basic
        $data = $_billing_detail_init;
        $data['account_type_number'] = $account_type_number;
        $data['account_type_id'] = "mobile_basic";
        $data['account_type_name'] = "携帯テレビ電話月額基本料金";
        if ($v3data['mobile']) {
            $data['charge'] = str_replace(",", "", $v3data['mobile']['charge']);
            $data['amount'] = str_replace(",", "", $v3data['mobile']['num']);
            $data['price'] = str_replace(",", "", $v3data['mobile']['basic']);
        }
        $data['unit'] = "回線";
        $_remarks = "( ￥". number_format($data['price']) . " × " . number_format($data['amount']) . "回線　内1回線　プランに付属 )";
        $data['remarks'] = $_remarks;
        $data['serialize_info'] = $_serialize_info;
        $ret[] = $data;
        $account_type_number += 1;

// mobile_extends_normal
        $data = $_billing_detail_init;
        $data['account_type_number'] = $account_type_number;
        $data['account_type_id'] = "mobile_extends_normal";
        $data['account_type_name'] = "携帯テレビ電話追加料金 \n（平日8時から19時）";
        if ($v3data['mobile']) {
            $data['charge'] = str_replace(",", "", $v3data['mobile']['extra']);
            $data['amount'] = str_replace(",", "", $v3data['mobile']['time']);
            $data['price'] = str_replace(",", "", $v3data['mobile']['running']);
        }
        $data['unit'] = "分";
        $_remarks = "( ￥". number_format($data['price']) . " × " . number_format($data['amount']) . "分 )";
        $data['remarks'] = $_remarks;
        $data['serialize_info'] = $_serialize_info;
        $ret[] = $data;
        $account_type_number += 1;

// mobile_extends_special
        $data = $_billing_detail_init;
        $data['account_type_number'] = $account_type_number;
        $data['account_type_id'] = "mobile_extends_special";
        $data['account_type_name'] = "携帯テレビ電話追加料金 \n（19時から翌朝8時及び土、日曜祝日）";
        if ($v3data['mobile']) {
            $data['charge'] = str_replace(",", "", $v3data['mobile']['extra2']);
            $data['amount'] = str_replace(",", "", $v3data['mobile']['time2']);
            $data['price'] = str_replace(",", "", $v3data['mobile']['running2']);
        }
        $data['unit'] = "分";
        $_remarks = "( ￥". number_format($data['price']) . " × " . number_format($data['amount']) . "分 )";
        $data['remarks'] = $_remarks;
        $data['serialize_info'] = $_serialize_info;
        $ret[] = $data;
        $account_type_number += 1;

// h323_basic
        $data = $_billing_detail_init;
        $data['account_type_number'] = $account_type_number;
        $data['account_type_id'] = "h323_basic";
        $data['account_type_name'] = "Ｈ.３２３通信月額基本料金";
        if ($v3data['h323']) {
            $data['charge'] = str_replace(",", "", $v3data['h323']['charge']);
            $data['amount'] = str_replace(",", "", $v3data['h323']['num']);
            $data['price'] = str_replace(",", "", $v3data['h323']['basic']);
        }
        $data['unit'] = "回線";
        $_remarks = "( ￥". number_format($data['price']) . " × " . number_format($data['amount']) . "回線 )";
        $data['remarks'] = $_remarks;
        $data['serialize_info'] = $_serialize_info;
        $ret[] = $data;
        $account_type_number += 1;

// h323_extends
        $data = $_billing_detail_init;
        $data['account_type_number'] = $account_type_number;
        $data['account_type_id'] = "h323_extends";
        $data['account_type_name'] = "Ｈ.３２３通信追加料金";
        if ($v3data['h323']) {
            $data['charge'] = str_replace(",", "", $v3data['h323']['extra']);
            $data['amount'] = str_replace(",", "", $v3data['h323']['time']);
            $data['price'] = str_replace(",", "", $v3data['h323']['running']);
        }
        $data['unit'] = "分";
        $_remarks = "( ￥". number_format($data['price']) . " × " . number_format($data['amount']) . "分 )";
        $data['remarks'] = $_remarks;
        $data['serialize_info'] = $_serialize_info;
        $ret[] = $data;
        $account_type_number += 1;

// ssl_basic
        $data = $_billing_detail_init;
        $data['account_type_number'] = $account_type_number;
        $data['account_type_id'] = "ssl_basic";
        $data['account_type_name'] = "暗号化オプション";
        if ($v3data['ssl']) {
            $data['charge'] = str_replace(",", "", $v3data['ssl']['charge']);
            $data['amount'] = str_replace(",", "", $v3data['ssl']['num']);
            $data['price'] = str_replace(",", "", $v3data['ssl']['basic']);
        }
        $data['unit'] = null;
        $_remarks = null;
        $data['remarks'] = $_remarks;
        $data['serialize_info'] = $_serialize_info;
        $ret[] = $data;
        $account_type_number += 1;

// hispec_basic
        $data = $_billing_detail_init;
        $data['account_type_number'] = $account_type_number;
        $data['account_type_id'] = "hispec_basic";
        $data['account_type_name'] = "高画質モード基本料金";
        if ($v3data['hispec']) {
            $data['charge'] = str_replace(",", "", $v3data['hispec']['charge']);
            $data['amount'] = str_replace(",", "", $v3data['hispec']['num']);
            $data['price'] = str_replace(",", "", $v3data['hispec']['basic']);
        }
        $data['unit'] = null;
        $_remarks = null;
        $data['remarks'] = $_remarks;
        $data['serialize_info'] = $_serialize_info;
        $ret[] = $data;
        $account_type_number += 1;

// hispec_extends
        $data = $_billing_detail_init;
        $data['account_type_number'] = $account_type_number;
        $data['account_type_id'] = "hispec_extends";
        $data['account_type_name'] = "高画質モード追加料金";
        if ($v3data['hispec']) {
            $data['charge'] = str_replace(",", "", $v3data['hispec']['extra']);
            $data['amount'] = str_replace(",", "", $v3data['hispec']['time']);
            $data['price'] = str_replace(",", "", $v3data['hispec']['running']);
        }
        $data['unit'] = "分";
        $_remarks = "( ￥". number_format($data['price']) . " × " . number_format($data['amount']) . "分 )";
        $data['remarks'] = $_remarks;
        $data['serialize_info'] = $_serialize_info;
        $ret[] = $data;

// sharing_basic
        $data = $_billing_detail_init;
        $data['account_type_number'] = $account_type_number;
        $data['account_type_id'] = "sharing_basic";
        $data['account_type_name'] = "デスクトップシェアリング";
        if ($v3data['share']) {
            $data['charge'] = str_replace(",", "", $v3data['share']['charge']);
            $data['amount'] = str_replace(",", "", $v3data['share']['num']);
            $data['price'] = str_replace(",", "", $v3data['share']['basic']);
        }
        $data['unit'] = null;
        $_remarks = "( プラン付属 )";
        $data['remarks'] = $_remarks;
        $data['serialize_info'] = $_serialize_info;
        $ret[] = $data;
        $account_type_number += 1;

// hdd_basic
        $data = $_billing_detail_init;
        $data['account_type_number'] = $account_type_number;
        $data['account_type_id'] = "hdd_basic";
        $data['account_type_name'] = "追加録画容量";
        if ($v3data['hdd']) {
            $data['charge'] = str_replace(",", "", $v3data['hdd']['charge']);
            $data['amount'] = str_replace(",", "", $v3data['hdd']['num']);
            $data['price'] = str_replace(",", "", $v3data['hdd']['basic']);
        }
        $data['unit'] = "ＧＢ";
        $_remarks = "( 追加容量 ". number_format($data['amount']) . $data['unit'] . " )";
        $data['remarks'] = $_remarks;
        $data['serialize_info'] = $_serialize_info;
        $ret[] = $data;
        $account_type_number += 1;

// compact_basic
        $data = $_billing_detail_init;
        $data['account_type_number'] = $account_type_number;
        $data['account_type_id'] = "compact_basic";
        $data['account_type_name'] = "コンパクト";
        if ($v3data['compact']) {
            $data['charge'] = str_replace(",", "", $v3data['compact']['charge']);
            $data['amount'] = str_replace(",", "", $v3data['compact']['num']);
            $data['price'] = str_replace(",", "", $v3data['compact']['basic']);
        }
        $data['unit'] = null;
        $_remarks = null;
        $data['remarks'] = $_remarks;
        $data['serialize_info'] = $_serialize_info;
        $ret[] = $data;
        $account_type_number += 1;

// audience_first
        if (array_key_exists("audience_fixed", $v3data)) {
        if ($v3data['audience_fixed']['first_number'] != 0 ) {
        $data = $_billing_detail_init;
        $data['account_type_number'] = $account_type_number;
        $data['account_type_id'] = "audience_first";
        $data['account_type_name'] = "オーディエンス機能初期費用";
        $data['charge'] = str_replace(",", "", $v3data['audience_fixed']['first_charge']);
        $data['amount'] = str_replace(",", "", $v3data['audience_fixed']['first_number']);
        $data['unit'] = "地点";
        $data['price'] = 0;
        $_remarks = "( ￥". number_format($data['price']) . " × " . number_format($data['amount']) . $data['unit'] . " )";
        $data['remarks'] = $_remarks;
        $data['serialize_info'] = $_serialize_info;
        $ret[] = $data;
        $account_type_number += 1;
        }

// audience_basic
        $data = $_billing_detail_init;
        $data['account_type_number'] = $account_type_number;
        $data['account_type_id'] = "audience_basic";
        $data['account_type_name'] = "オーディエンス機能基本料金";
        $data['charge'] = str_replace(",", "", $v3data['audience_fixed']['basic_charge']);
        $data['amount'] = str_replace(",", "", $v3data['audience_fixed']['basic_number']);
        $data['unit'] = "地点";
        $data['price'] = 0;
        $_remarks = "( ￥". number_format($data['price']) . " × " . number_format($data['amount']) . $data['unit'] . " )";
        $data['remarks'] = $_remarks;
        $data['serialize_info'] = $_serialize_info;
        $ret[] = $data;
        $account_type_number += 1;
        }

// audience_extends
        if (array_key_exists('audience_running', $v3data)) {
        $data = $_billing_detail_init;
        $data['account_type_number'] = $account_type_number;
        $data['account_type_id'] = "audience_extends";
        $data['account_type_name'] = "オーディエンス機能利用料金";
        $data['charge'] = str_replace(",", "", $v3data['audience_running']['charge']);
        $data['amount'] = str_replace(",", "", $v3data['audience_running']['man']);
        $data['unit'] = "分";
        $data['price'] = 0;
        $_remarks = "( ￥". number_format($data['price']) . " × " . number_format($data['amount']) . "分 )";
        $data['remarks'] = $_remarks;
        $data['serialize_info'] = $_serialize_info;
        $ret[] = $data;
        $account_type_number += 1;
        }

        $account_type_number = 100;
// _total_
        $data = $_billing_detail_init;
        $data['account_type_number'] = $account_type_number;
        $data['account_type_id'] = "_total_";
        $data['account_type_name'] = "小計";
        // v3課金合計（従量課金分のみ）
        $_v3_sub_total = 0;
        $_v3_sub_total = str_replace(",", "", $v3data["extra_charge"]["charge"])
                       + str_replace(",", "", $v3data["mobile"]["extra"])
                       + str_replace(",", "", $v3data["mobile"]["extra2"])
                       + str_replace(",", "", $v3data["h323"]["extra"])
                       + str_replace(",", "", $v3data["hispec"]["extra"]);

        $data['charge'] = $_v3_sub_total;
        $data['amount'] = 1;
        $data['unit'] = null;
        $data['price'] = $_v3_sub_total;
        $_remarks = null;
        $data['remarks'] = $_remarks;
        $data['serialize_info'] = $_serialize_info;
        $ret[] = $data;
        $account_type_number += 1;

        $account_type_number = 1;
// messenger_basic
        if (array_key_exists('messenger', $v3data)) {
        if ($v3data['messenger']['number'] != 0) {
        $data = $_billing_detail_init;
        $data['detail_group_number'] = 100;
        $data['detail_group_id'] = "_option_";
        $data['detail_group_name'] = "その他オプション";
        $data['account_type_number'] = $account_type_number;
        $data['account_type_id'] = "_total_";
        $data['account_type_name'] = "メッセンジャー利用料金";
        $data['charge'] = str_replace(",", "", $v3data['messenger']['charge']);
        $data['amount'] = str_replace(",", "", $v3data['messenger']['number']);
        $data['unit'] = "名";
        $data['price'] = 100;
        $_remarks = "( ￥". number_format($data['price']) . " × " . number_format($data['amount']) . "名 )";
        $data['remarks'] = $_remarks;
        //$data['serialize_info'] = null;
        $ret[] = $data;
        $data = $this->initBillingSubData;
        $account_type_number += 1;

        $account_type_number = 100;
// _total_
        $data = $_billing_detail_init;
        $data['detail_group_number'] = 100;
        $data['detail_group_id'] = "_option_";
        $data['detail_group_name'] = "その他オプション";
        $data['account_type_number'] = $account_type_number;
        $data['account_type_id'] = "_total_";
        $data['account_type_name'] = "その他オプション小計";
        $data['charge'] = str_replace(",", "", $v3data['messenger']['charge']);
        $data['amount'] = 1;
        $data['unit'] = null;
        $data['price'] = str_replace(",", "", $v3data['messenger']['charge']);
        $_remarks = null;
        $data['remarks'] = $_remarks;
        //$data['serialize_info'] = null;
        $ret[] = $data;
        $account_type_number += 1;
        }
        }

//		$this->debug(__FILE__, __LINE__, $ret);
        return $ret;
    }


    /**
     * private変数クリア（メモリ対策）
     */
    function _clear_private_info() {
        unset($this->v3_url);
        unset($this->v3_account);
        unset($this->v3_info);

        unset($this->_core_db_dsn);
        unset($this->billing_mains_obj);
        unset($this->billing_subs_obj);
        unset($this->product);
        unset($this->base_url);
        unset($this->service_list);
        unset($this->service_option_list);
        unset($this->month_summary_info);

        // 請求処理・対象月取得
        $this->target_year = null;
        $this->target_month = null;
        $this->target_year_month = null;

        unset($this->user_list);
        unset($this->room_list);
        unset($this->plan_list);
        unset($this->option_list);

        unset($this->_tax_rate);
        unset($this->_sales_tax);
        unset($this->_total_charge);

        unset($this->more_than_standard_plan_keys);
        unset($this->premium_plan_keys);
        unset($this->option_keys);
        unset($this->hq_plan_key);
        unset($this->easiness_plan_key);
        unset($this->unrestricted_plan_key);
        unset($this->user_service_option_id_support);
    }


}

$main = new AppMonthlyOperation();
$main->execute();
?>