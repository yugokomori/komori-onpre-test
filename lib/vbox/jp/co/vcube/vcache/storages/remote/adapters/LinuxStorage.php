<?php

/**
 * @author Yuki Asano <y-asano@vcube.co.jp>
 * @copyright Vcube, Inc. All Rights Reserved.
 * @version 0.1
 */

require_once( "jp/co/vcube/vcache/storages/remote/RemoteStorage.php");

abstract class LinuxStorage extends RemoteStorage
{

    /**
     * @private $result ResultArrayObject - A result value from copyTo method.
     */
    private $result;

    /**
    * The abstract method for each storage config.
    */
    abstract public function getStorageConfig();

    /** @public $transfer_option TransferOption - Transfer option object. */
    public $transfer_option;

    /** @public $default_transfer_mode Integer - Default transfer mode. Default value is MountStorage::TRANSFER_MODE_SCP */
    public $default_transfer_mode = RemoteStorage::TRANSFER_MODE_SCP;

    public function downloadTo( $target )
    {
        if( $this->isRemoteStorage() && $target->isRemoteStorage() ){
            throw new Exception( "RemoteStorage can not allowed with RemoteStorage." );
        }

        if ( !$this->transfer_option )
            $this->transfer_option = new TransferOption( $this->default_transfer_mode );

        $result = null;
        switch( $this->transfer_option->getTransferMode() ) {
            case AbstractStorage::TRANSFER_MODE_SCP:
                $result = $this->transferBySCP( $target );
                break;

            /*case AbstractStorage::TRANSFER_MODE_SFTP:
                $result = $this->transferBySFTP( $target );
                break;

            case AbstractStorage::TRANSFER_MODE_RSYNC:
                $result = $this->transferByRSYNC( $target );
                break;*/

            default:
                throw new Exception( "Invalid transfer_mode. - ".$this->transfer_mode );
        }

        if ( !($result instanceof ResultArrayObject) && !is_subclass_of($result, ResultArrayObject) )
            throw new Exception( "Invalid return value. Allow only inherited ResultArrayObject class. - ". ($result ? get_class($result) : "void") );

        $this->result = $result;
    }

    public function upload( $target ){}

    public function download( $target ){}

    /**
     * A result value from copyTo method.
     * @return ResultArrayObject - Return value must ResultArrayObject or inherited it.
     */
    public function getResultArrayObject()
    {
        return $this->result;
    }

    /**
     * A transfer method for SCP.
     * @param $target Object - Storage instance that inherited AbstractStorage.
     * @throws Exception - ExecCommand exceptions directly.
     * @return ResultArrayObject - Return value must ResultArrayObject or inherited it.
     */
    final private function transferBySCP( $target )
    {
        $src_config = $this->getStorageConfig();

        $cmd = new ExecCommand( $this->transfer_option->timeout, $this->transfer_option->expect_return );
        $cmd->setTrustedArgument( $this->transfer_option->first_argument );

        $target_path = sprintf( "%s@%s:%s", $src_config->user, $src_config->host, $this->filepath );
        if ( $this->unescape )
            $cmd->setTrustedArgument( $target_path );
        else
            $cmd->setArgument( $target_path );

        if ( $target->unescape )
            $cmd->setTrustedArgument( $target->filepath );
        else
            $cmd->setArgument( $target->filepath );

        $cmd->execute( "scp" );

        return new ResultArrayObject( $cmd->getResults() );
    }
}


