<?php
require_once("classes/N2MY_DBI.class.php");

class WordTable extends N2MY_DB {

    var $table = 'word';
    var $logger = null;
    protected $primary_key = "word_no";

    /**
     * DB接続
     */
    function __construct($dsn) {
        $this->init($dsn, $this->table);
        $this->logger = EZLogger::getInstance();
    }
}
