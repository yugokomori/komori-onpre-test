ALTER TABLE  `mcu_server` ADD  `controller_port` INT( 4 ) NOT NULL DEFAULT  '8080' AFTER  `server_address` ,
ADD  `socket_port` INT( 4 ) NOT NULL DEFAULT  '1500' AFTER  `controller_port` ,
ADD  `sip_proxy_port` INT( 4 ) NOT NULL DEFAULT  '5060' AFTER  `socket_port`;

ALTER TABLE  `mcu_server` ADD  `ip` VARCHAR( 20 ) NOT NULL AFTER  `server_address` ,
ADD INDEX ( ip );

ALTER TABLE  `mcu_server` ADD  `number_domain` VARCHAR( 255 ) NOT NULL AFTER  `server_address`;

ALTER TABLE  `mcu_server` ADD  `mobile_mix` INT( 1 ) NOT NULL AFTER  `sip_proxy_port` ,
ADD INDEX ( mobile_mix );


CREATE TABLE  `media_mixer` (
`media_mixer_key` INT NOT NULL ,
`mcu_server_key` INT( 10 ) NOT NULL ,
`media_name` VARCHAR( 255 ) NOT NULL ,
`url` VARCHAR( 255 ) NOT NULL ,
`media_ip` VARCHAR( 255 ) NOT NULL ,
`local_net` VARCHAR( 255 ) NOT NULL ,
`public_ip` VARCHAR( 255 ) NOT NULL ,
`mobile_mix` INT( 1 ) NOT NULL ,
`is_available` INT( 1 ) NOT NULL ,
`create_datetime` DATETIME NOT NULL ,
`update_datetime` INT NOT NULL ,
PRIMARY KEY (  `media_mixer_key` ) ,
INDEX (  `mcu_server_key` ,  `is_available` )
) ENGINE = MYISAM;
