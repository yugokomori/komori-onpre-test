ALTER TABLE  `service` ADD `member_storage_size` int(11) NOT NULL DEFAULT '0' COMMENT '1メンバーのストレージ容量(Mbyte)' AFTER  `service_expire_day`;
ALTER TABLE  `service_option` ADD `option_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'オプションステータス' AFTER  `service_option_running_price`;
ALTER TABLE  `service_option` ADD `use_user_option` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'ユーザーオプションフラグ' AFTER  `option_status`;
INSERT INTO `service_option` VALUES (31 , 1, 'user_max_storage_size_extention_1G',10000,0,0,1,1);
UPDATE service SET use_member_plan = 1 WHERE service_key IN (48,49,60,42,79,69,95);
UPDATE service SET member_storage_size = 500 WHERE service_key IN (79);

INSERT INTO `service` VALUES(96, 1, 'ブルーIDプラン', 9, 16, 0, 0, 0, 0, 0, '', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 69900, 3000, 0, 0, NOW(), 1, 0, 200);
INSERT INTO `service` VALUES(97, 1, 'グリーンIDホストプラン', 4, 21, 0, 0, 0, 0, 0, '', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 10000, 5000, 0, 0, NOW(), 1, 0, 200);
INSERT INTO `service` VALUES(98, 1, 'グリーンIDホストプランライト', 4, 5, 0, 0, 0, 0, 0, '', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 5000, 2500, 0, 0, NOW(), 1, 0, 200);
INSERT INTO `service` VALUES(99, 1, 'グレーID従量プラン', 4, 21, 0, 0, 0, 0, 0, '', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 10000, 5000, 10, 60, NOW(), 1, 0, 0);
INSERT INTO `service` VALUES(100, 1, 'Docomo_Document_Free', 5, 0, 5, 0, 0, 0, 0, '320x240', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, 0, NOW(), 1, 0, 0);
