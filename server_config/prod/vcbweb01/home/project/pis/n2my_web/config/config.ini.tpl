[GLOBAL]
;アカウントDB
auth_dsn        = "{N2MY_AUTH_DSN}"
; 多言語化管理Dsn
;lang_dsn        = "{N2MY_LANG_DSN}"
; アンケート集計用Dsn
; enquete_dsn     = "{N2MY_ENQUETE_DSN}"


; 出力時の文字コード
html_output_char = "UTF-8"
; サーバタイムゾーン
time_zone        = 9
; サーバタイムゾーン名
time_zone_name   = "Asia/Tokyo"

; FMS ID
fms_id           = "{FMS_ID}"
; FMS PW
fms_password     = "{FMS_PW}"
; FMS ポート
fms_port         = 1111

[IGNORE_MENU]
; フロントサイト
front_site          = 0
; 予約
reservation         = 0
; 予約リマインダー
reservation_remainder = 1
; 招待メール
invite              = 0
; ツール・マニュアル
tool                = 0
; FAQ
faq                 = 0
; 問い合わせ
inquiry             = 0
; メンテナンス
maintenance         = 0
; 広告
advertise           = 0
; 利用ログ
admin_log           = 0
: 利用プラン票
admin_log_plan      = 0
; 料金表
admin_log_fee       = 0
; 部屋設定
admin_room_info     = 0
; メンバー管理
admin_member        = 0
; メンバー追加
admin_member_add        = 0
; アカウント
admin_account       = 0
; アカウント情報表示
admin_account_info  = 0
; セキュリティ設定
admin_security = 0
; 問い合わせ
admin_inquiry       = 0
; アプリバージョン情報
tool_version_info   = 0
; プレゼンスアプリ
tool_presence_app   = 0
; チェッカーアプリ
tool_cheker_app     = 0
; シェアリング
tool_sharing_app    = 0
; シェアリング2
tool_sharing2_app   = 0
; シェアリング3
tool_sharing3_app   = 0
;outlook
tool_outlook_app    = 0
;V-CUBEモバイル
tool_mobile_app     = 0
; 通常マニュアル
normal_manual       = 0
; オーディエンスマニュアル
audience_manual     = 0
;h323マニュアル
h323_manual         = 0
; クイックマニュアル
quick_manual        = 0
; V-CUBE モバイルマニュアル
mobile_manual       = 0
; スマートフォン連携 iPhoneマニュアル
iphone_manual       = 0
; スマートフォン連携 Androidマニュアル
android_manual      = 0
; マルチカメラマニュアル
multicamera_manual  = 0
; China Fast Line
china_fastline_manual = 0
; 共有ストレージ
shared_storage_manual = 0
; Global Link
global_link_manual = 0
; Video会議連携（Polycom連携）
video_conference     = 0
; 企業ロゴ
company_logo        = 0
; エコメーター
eco_meter           = 0
; フリガナ
furigana            = 0
;アドレス帳
display_address     = 0
; エコーキャンセラー
echo_canceler       = 0
; 資料のベクタ変換
document_vector     = 0
; PGI連携
teleconference      = 0
; アンケート機能
questionnaire       = 1
; 利用規約
terms               = 1
; カスタマーサポート情報
support_info        = 1

[FMS_COUNTRY_PRIORITY]
ja = "jp"
etc = "jp"

;ASP用↓
;jp = "jp,cn,us,us2"
;us = "us,us2,jp,cn"
;us2 = "us2,us,jp,cn"
;cn = "cn,jp,us,us2"
;sg = "jp,cn,us,us2"
;etc = "jp,cn,us,us2"

[FMS_DC]
;jp = "2,7,1"

[LOGGER]
; ログディレクトリ
log_dir             = "{N2MY_WEB_HOME}/logs/"
auto_flush          = true
log_display         = false
; ログレベル
log_level           = "{LOG_LEVEL}"
log_notify          = false
log_override        = false
log_span            = "Ym"
log_timestamp       = "Y-m-d H:i:s"
log_prefix          = "n2my"

[SMARTY]
cache_lifetime       = 1
caching              = false
compile_check        = true
compile_id           = "ja"
debugging            = false
debugging_ctrl       = "NONE"
force_compile        = false

[SMARTY_DIR]
template_dir = "{N2MY_WEB_HOME}/templates/"
plugins_dir  = "{N2MY_WEB_HOME}/lib/Smarty/plugins/"
cache_dir    = "{N2MY_WEB_HOME}/webapp/smarty/cache/"
compile_dir  = "{N2MY_WEB_HOME}/webapp/smarty/compile/"

[CORE]
; FMSの会議アプリケーション名
app_name        = "{APP_NAME}"
; 議事録、映像用のアプリケーション名
[ONDEMAND]
app_name        = "{APP_ONDEMAND_NAME}"

[N2MY]
; 部屋の状態取得用 FMSのサーバ
fms_server          = "{FMS_SERVER}"
; mobileのプロトコル名
mobile_protcol        = "vcube-meeting"
mobile_protcol_sales  = "vcube-sales"
; 資料のみのプロトコル名
doc_share_protcol     = "{DOC_SHARE_PROTOCOL_NAME}"
; 部屋の状態取得用FMSアプリケーション名
room_status_checker = "RoomChecker"
; ECOメータ用の経路検索API
transit_uri = "{TRANSIT_URI}"

; SSL
ssl_mode            = "http"
; 製品名の略称
short_name          = "N2MY"
; 製品名
full_name           = "nice to meet you"
; 事前アップロード可能な資料をカンマ区切り(Office2007対応の場合はdocx,xlsx,pptxを追加)
convert_format      = "doc,docx,xls,xlsx,ppt,pptx,pdf,gif,jpg,jpeg,png,bmp,tif,tiff,ai,psd"
; 事前アップロード可能なファイル形式
convert_format_view = "Word,Excel,PowerPoint,PDF,GIF,JPEG,PNG,BMP,TIFF"
; ページ表示秒数で警告を出す(0の場合は出力なし)
alert_time        = 0
; メモリ使用量で警告を出す
alert_memory      = 10
; デバッグモード
debug               = 0

; ブラウザ指定の言語が存在しない場合の対応
;default_lang       = "en"
; 使用しない言語一覧
lang_deny           = ""
; 使用する言語一覧
lang_allow          = ""

; 使用しないデータセンターの地域
country_deny        = ""
; 使用するデータセンターの地域
country_allow       = ""

; 使用しないタイムゾーン
timezone_deny       = ""
; 使用するタイムゾーン
timezone_allow      = ""

; 自動ログイン
auto_login          = 0
; ログインユーザID(この機能を利用する場合、メンバー機能をオフにしてください)
login_user          = ""
; ログインパスワード
login_password      = ""

; メール資料貼り込み機能
mail_wb_upload      = 0
; メール資料貼り込み受付ホスト名
mail_wb_host        = "web.example.com"
; noreply用アドレス
noreply_address        = "noreply"

; 参加者を検索する
search_participant = 1

; Google Analytics
google_analytics_cd = ""

; アドレス帳自動登録機能
auto_address_book = 0

; アドレス帳の最大登録件数
address_book_count = 2000
; メンテナンス告知
maintenance_notification = 0
; ターミナルユーザー設定
terminal_edit = 0

; H264 Transcoder 利用フラグ
enable_h264_transcoder = 1

; モバイルミックス用 利用時はsmallを指定
mcu_profile_type = ""

[DOCUMENT]
; FMSサーバ
;資料変換処理受付用のFMS
fms = "{DOC_FMS}"
;資料変換通知
rtmp = "rtmp://%s/%s"
;アプリ
appli = "{DOC_APPLI}"
;ユーザーＩＤ
user = "{DOC_USER}"
;資料変換ディレクトリ
doc_dir = "docs"
;資料変換時の幅
max_x = 1200
;資料変換時の高さ
max_y = 900
; フリーユーザーの広告保存ディレクトリ
free_plan_ads_dirname = "vCubeIdFreePlan"
; フリーユーザーの広告保存デフォルトディレクトリ
free_plan_ads_default_dirname = "allUser"

[DOCUMENT_IMAGES]
bmp     = "bmp"
gif     = "gif"
jpg     = "jpg,jpeg"
png     = "png"
tif     = "tif,tiff"
eps     = "eps,epsf,epsi"
psd     = "ai,psd,psb,pdd"
;pict    = "pict,pic,pct"
;psp     = "psp"
;svg     = "svg,svgz"

[DOCUMENT_FILES]
word   = "doc,docx,rtf,odt"
excel  = "xls,xlsx,xlt,ods"
ppt    = "ppt,pptx,odp"
vsd    = "vsd"
pdf    = "pdf"
;text   = "txt"

[NOTIFICATION]
; 利用状況通知機能(送信元)
to   = ""
; 送信先
from = ""

[N2MY_API]
; API用アドミンID
admin_login_id = "";
; API用アドミンパスワード
admin_login_pw = "";

;アラートメール送信フラグ
[ALERT_MAIL]
done = 0

[RECORD_GW]
; 録画GWサーバURL
url = "http://recgateway.vcube.net/"
; SCPユーザ
user = "mtg-user"

[TELEPHONE]
ja = "0123456789,0987(65)4321"
en = "0123456789,0987-65-4321"

[ECHO_CANCEL]
highPriorityMicrophoneNames = "PJP,Sennhei,Plantro,USB Audio"
excludeEchoCancelNames = "PJP,Sennhei,Plantro"

[CENTRE]
plan_list = ""
;default_lang = "ja_JP"
;default_timezone = "9"
;default_locale = "jp"

[QUESTION_LANG]
ja = lang1
en = lang2
zh-cn = lang3
zh-tw = lang4
fr = lang5
th = lang6
in = lang7
ja_JP = "lang1"
en_US = "lang2"
zh_CN = "lang3"
zh_TW = "lang4"
fr_FR = "lang5"
th_TH = "lang6"
in_ID = "lang7"

[STORAGE]
max_image_size = "5"
max_file_size = "20"
max_video_size = "150"

[VIDEO_EXTENSION]
asf = "asf"
avi = "avi"
flv = "flv"
wmv = "wmv"
mp4 = "mp4"