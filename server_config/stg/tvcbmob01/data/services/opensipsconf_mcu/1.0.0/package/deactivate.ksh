#!/bin/bash
# Permet de supprimer le liens entre le fichier de conf d'opensips (/data/services/opensipsconf_mcu/vX.XOpensips/cfg/vX.XScript/opensips.hostname/ et /etc/opensips/opensips.cfg
# DATE: 12/06/2012
# Author : Thomas Carvello
#

function deactivate
{
    echo "desactivation de la conf opensips"
    rm -f /etc/opensips/opensips.cfg
}

case $1 in
        "pre-prod")
                echo "Suppression des specificites $SERVICENAME version $SERVICEVERSION";;
        "prod")
                echo "Suppression des specificites $SERVICENAME version $SERVICEVERSION"
                deactivate;;
        *)
                echo "usage: activate.ksh [options]"
                echo "options :"
                echo "  prod         mise en production du service" ;;
esac
