#!/usr/local/bin/php
<?php
set_time_limit(0);
ini_set("memory_limit","2048M");
require_once("set_env.php");
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_Reservation.class.php");
require_once("lib/EZLib/EZUtil/EZCsv.class.php");
require_once ('classes/mgm/dbi/user.dbi.php');
require_once("classes/pis_daily_id_list.class.php");
require_once("classes/pis_error_code_list.class.php");

define("ERROR_WHITE_UPDATE", "Failed to update(white)");
define("ERROR_MUITIPLE_START_UP", "Multiple Start up");
define("ERROR_PROCESS_KILL", "Process Kill");

class AppDailyIdList extends AppFrame {

    /*
    * ファイルパスについてはconfig/config.inc.phpで定義しておくこと
    *
    * ADMIN_USER_KEY : ユーザキー(利用ログでも使用する)
    * MEMBER_PASSWORD : メンバーパスワード
    *
    * 実行時、引数があれば月次処理とする
    *
    *
    * ○csv 関連
    * WIDS_KIHON_PATH : 基本情報
    * WIDS_ZAISEKI_PATH : 原籍情報
    * WIDS_HENKAN_PATH : ID変換情報ファイル
    * WIDS_GID_KIHON_PATH :ID基本情報ファイル
    * WIDS_GID_SYOZOKU_PATH :所属情報ファイル
    * TID_LIST_PATH :TID_LISTファイル
    * GID_LIST_PATH :GID_LISTファイル
    * USER_LIST_PATH:USER_LISTファイル
    * USER_LIST_FLG_PATH:USER_LIST作成日書込みファイル
    *
    * ○FTP関連
    * FTP用の設定もconfig/config.inc.phpで設定する
    * FTP_USEFLG :FTP使用有無(0:使用しない 1:使用する)
    * FTP_SERVER_KIHON : FTP server ドメイン or IP
    * FTP_SERVER_PORT_KIHON : ポート
    * FTP_SERVER_ID_KIHON : ログインID
    * FTP_SERVER_PW_KIHON : ログインパスワード
    * FTP_SERVER_SYOZOKU : FTP server ドメイン or IP
    * FTP_SERVER_PORT_SYOZOKU : ポート
    * FTP_SERVER_ID_SYOZOKU : ログインID
    * FTP_SERVER_PW_SYOZOKU : ログインパスワード
    * FTP_SERVER_HENKAN : FTP server ドメイン or IP
    * FTP_SERVER_PORT_HENKAN : ポート
    * FTP_SERVER_ID_HENKAN : ログインID
    * FTP_SERVER_PW_HENKAN : ログインパスワード
    * FTP_SERVER_GID_KIHON : FTP server ドメイン or IP
    * FTP_SERVER_PORT_GID_KIHON : ポート
    * FTP_SERVER_ID_GID_KIHON : ログインID
    * FTP_SERVER_PW_GID_KIHON : ログインパスワード
    * FTP_SERVER_GID_SYOZOKU : FTP server ドメイン or IP
    * FTP_SERVER_PORT_GID_SYOZOKU : ポート
    * FTP_SERVER_ID_GID_SYOZOKU : ログインID
    * FTP_SERVER_PW_GID_SYOZOKU : ログインパスワード
    * FTP_SERVER_WIDS_KIHON_PATH : ID基本リストのパス
    * FTP_SERVER_WIDS_ZAISEKI_PATH : ID原籍リストのパス
    * FTP_SERVER_WIDS_HENKAN_PATH : ID変換リストのパス
    * FTP_SERVER_WIDS_GID_KIHON_PATH : GID_ID基本リストのパス
    * FTP_SERVER_WIDS_GID_SYOZOKU_PATH : GID_ID原籍リストのパス    *
    */

    var $_mail_flg = 1;
    var $dsn = null;
    var $passwprd = "Gt0vsS1hVdOsoNfI01yG";


    // 月次処理フラグ
    var $month_flag = false;

    function __construct($argv) {
        parent::__construct();
        $this->month_flag = $argv;
    }

    function __destruct() {

    }

    function init() {
        $this->dsn               = $this->get_dsn();
        $this->obj_class_pis_daily_id_list = new AppDailyIdListClass($this->dsn);
        $this->obj_class_pis_error_code_list = new AppErrorCodeListClass($this->dsn);
    }

    function default_view() {
        $this->logger2->info("start:" . time());
        $this->logger2->info("start_memory:" . memory_get_usage());
        $start_time  =   time();

        //多重起動チェック
        $multipleflg = null;
        $multipleflg = $this->obj_class_pis_daily_id_list->multipleCeheck();

        //多重起動時は停止
        if($multipleflg != 1 && $multipleflg == ERROR_PROCESS_KILL){
        	$this->obj_class_pis_error_code_list->error_log($multipleflg,false);
        }elseif($multipleflg != 1 && $multipleflg == ERROR_MUITIPLE_START_UP){
            $this->obj_class_pis_error_code_list->error_log($multipleflg,true);
        }
        require_once ('lib/EZLib/EZUtil/EZEncrypt.class.php');

        // ユーザ取込処理 前提処理
        $user_list = $this->obj_class_pis_daily_id_list->prepare();

        // 正常時は配列として取得
        if (is_array($user_list)) {

            // DB登録エラー情報
            $error_list = array();

            // 月次処理だったらメンバーIDのadmin_flagの者以外を無効化
            if($this->month_flag){
                $this->logger2->info ( "month flg:true" );
                $result = $this->obj_class_pis_daily_id_list->monthly_process();
                if(!$result[0]){
                    $this->obj_class_pis_error_code_list->error_log($result[1],true);
                }
            }else{
                $this->logger2->info ( "month flg:false" );
            }

            // USER_LIST ループ
            foreach ($user_list as $id => $user) {

                // ホワイトコードチェック
                $wc_pattern = $this->obj_class_pis_daily_id_list->check_white_code($user);

                // ホワイトIDチェック
                $wi_pattern = $this->obj_class_pis_daily_id_list->check_white_list($user);

                // DB登録
                $result = $this->obj_class_pis_daily_id_list->user_list_refrection($user, $wc_pattern, $wi_pattern);

                if ($result === true) {
                    // 正常終了
                } else {
                    $table_name = $result["table"];
                    $db_message = $result["error"];

                    if ((isset($table_name)) && (array_key_exists($table_name, $error_list) === false)) {
                        $error_message = "$table_name DB ERROR! [$db_message]";
                        // エラー情報格納
                        $error_list[$table_name] = $error_message;
                    }
                }
            }

            foreach ($error_list as $message) {
                $this->obj_class_pis_error_code_list->error_log(ERROR_WHITE_UPDATE,false,$message);
            }

            // var_dump($basic_list);
            $this->logger2->info ( "end:" . time () );
            $this->logger2->info ( "sum:" . (time () - $start_time) );
            $this->logger2->info ( "end_memory:" . memory_get_usage () );

            // メモリ開放
            $this->obj_class_pis_daily_id_list->clean_white_company_list();
            $this->obj_class_pis_daily_id_list->clean_white_list();
            unset($user_list);

            // ブラックリストと比較
            $result = $this->obj_class_pis_daily_id_list->black_list_reflection();
            if(!$result[0]){
                $this->obj_class_pis_error_code_list->error_log($result[1]);
            }
        }else{
            // 前提処理エラー
            $this->obj_class_pis_error_code_list->error_log($user_list,true);
        }
    }
}

$main = new AppDailyIdList($argv[1]);
$main->execute();
