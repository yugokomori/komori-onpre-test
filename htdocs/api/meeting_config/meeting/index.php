<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/AppFrame.class.php");

class API_LoadConfig extends AppFrame
{
    function default_view() {
        $this->render_default();
    }

    private function render_default() {
        $user_info = $this->session->get("user_info");
        $meeting_version = $this->session->get("meeting_version") ? $this->session->get("meeting_version") : $user_info["meeting_version"];
        if ($meeting_version >= "4.7.0.0") {
           $version = "4.7.0.0";
        } else {
           $version = "4.6.5.0";
        }
        $this->logger2->info($meeting_version);
        $this->template->assign( "pathToLibrary"                     , "/lang/".$this->_lang."/flash/meeting/Lib.swf" );
        $this->template->assign( "pathToApplicationAppearanceDataXML", $version."/application_appearance.php" );
        $this->template->assign( "pathToConfigDataXML"               , $version."/vrms.php" );
        $this->template->assign( "pathToCoreAppearanceDataXML"       , $version."/core_appearance.php" );
        $this->template->assign( "pathTolanguageXML"                 , "/lang/".$this->_lang."/flash/language.xml" );
        $this->template->assign( "pathToLayoutXML"                   , $version."layout.php" );
        $this->display( "core/api/meeting_config/meeting/project.t.xml");
    }
}

$main = new API_LoadConfig();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
