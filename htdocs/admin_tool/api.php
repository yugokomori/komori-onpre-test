<?php
require_once "classes/AppFrame.class.php";
require_once "lib/EZLib/EZXML/EZXML.class.php";
require_once "htdocs/admin_tool/dBug.php";

/**
 * @todo INPUT TYPEに合わせた送信
 * @todo エラー時の処理を入力フォームにマッピングしたい
 * @todo レスポンスのIFをどこかにリンクしたい
 * @todo OAuth対応
 */
class N2MY_API_TEST extends AppFrame {

    var $api_info = null;
    var $ns = "";
    var $groups = array();
    var $methods = array();

    function init() {
        $this->ns = md5(__FILE__);
        $xml = new EZXML();
        $data = file_get_contents(N2MY_APP_DIR."setup/api/v1/api_info.xml");
        $this->api_info = $xml->openXML($data);
        foreach($this->api_info["api_list"]["group"] as $key => $group) {
            $this->groups[$group["_attr"]["path"]] = $group["_attr"];
            foreach($group["method"] as $_key => $method) {
                //print_r($method);
                $params = array();
                foreach ($method["params"][0]["param"] as $param) {
                    if (strpos($param["_attr"]["id"], "[") !== false) {
                        $pattern = '\[';
                        $param["_attr"]["array"] = str_replace("guests[", "guests][", $param["_attr"]["id"]);
                    }
                    $params[$param["_attr"]["id"]] = $param["_attr"];
                }
                $errors = array();
                foreach ($method["errors"][0]["error"] as $error) {
                    $errors[] = $error["_attr"];
                }
                $this->methods[$group["_attr"]["path"]][$method["_attr"]["method"]] = array(
                    "name"      => $method["_attr"]["name"],
                    "method"    => $method["_attr"]["method"],
                    "desc"      => $method["desc"][0]["_data"],
                    "params"    => $params,
                    "response"  => $method["response"][0]["_data"],
                    "errors"    => $errors,
                    "info"      => $method
                );
            }
        }
        $this->template->assign("groups", $this->groups);
        $this->template->assign("methods", $this->methods);
    }

    function default_view() {
        $this->action_top();
    }

    function action_top() {
        $this->display("admin_tool/api/top.t.html");
    }

    function action_menu() {
        $this->display("admin_tool/api/menu.t.html");
    }

    function action_create_key() {
        $this->display("admin_tool/api/create_key.t.html");
    }

    function action_method() {
        $path   = $this->request->get("path");
        $method = $this->request->get("method");
        $session = $this->session->get("n2my_session", $this->ns);
        $api_info = $this->get_api_info($path, $method);
        $this->template->assign("api", $api_info);
        $this->template->assign("session", $session);

        $data = file_get_contents(N2MY_APP_DIR."setup/api/v1/response_info.xml");
        $xml = new EZXML();
        $response_info = $xml->openXML($data);
        $elem_cnt = 7;
        foreach($response_info["api_list"]["group"] as $key => $group) {
            if ($group["_attr"]["path"] == $path) {
                foreach($group["method"] as $_key => $methods) {
                    if ($methods["_attr"]["method"] == $method) {
//                        $this->logger2->info($methods);
                        $response_info = $this->_response_info($methods, -1, $elem_cnt);
//                        $this->logger2->info($response_info);
                    }
                }
            }
        }
        $this->template->assign("elem_cnt", $elem_cnt);
        $this->template->assign("response_info", $response_info);
        $this->display("admin_tool/api/method.t.html");
    }

    // 再起関数
    function _response_info($data, $i = 0, $elem_cnt = 10) {
        static $bbb;
        unset($data["_attr"]);
        unset($data["_data"]);
        $i++;
        foreach($data as $key => $val) {
            $val["0"]["_attr"]["id"] = $key;
            $val["0"]["_attr"]["index"] = $i;
            $val["0"]["_attr"]["colspan"] = $elem_cnt - $i;
            $bbb[] = $val["0"]["_attr"];
            $this->_response_info($val["0"], $i, $elem_cnt);
        }
//        $this->logger2->info($bbb);
        return $bbb;
    }

    function action_method_exec() {
        $params = $this->request->get("params");
        $path  = $this->request->get("path");

        $request_action = null;
        foreach ($params as $key => $value) {
            if (substr($key, 0, 7) == 'action_') {
                $request_action = $key;
                break;
            }
        }
        $param_info = $this->methods[$path][$request_action]["params"];
        foreach ($params as $key => $value) {
            if (is_null($value) && !$param_info[$key]["required"]) {
                unset($params[$key]);
            }
        }
        $this->logger2->info($params);
        $url = N2MY_BASE_URL.$path;
        $ch = curl_init();
        $option = array(
            CURLOPT_URL => $url,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query($params, '', '&'),
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => 20,
            CURLOPT_TIMEOUT => 20,
            );
        curl_setopt_array($ch, $option);
        $contents = curl_exec($ch);
        // デコード
        switch ($params["output_type"]) {
        case "xml":
            require_once("lib/pear/XML/Unserializer.php");
            $unserializer = new XML_Unserializer();
            $unserializer->unserialize($contents);
            $result = $unserializer->getUnserializedData();
            break;
        case "json":
            $result = json_decode($contents, true);
            break;
        case "php":
            $result = unserialize($contents);
            break;
        case "yaml":
            require_once("lib/spyc/spyc.php");
            $result = Spyc::YAMLLoad($contents);
            break;
        }
        // ログイン
        if ('action_login' == $request_action) {
            if ($result["status"] == "1") {
                $session = $result["data"]["session"];
                $this->logger2->info($session);
                $this->session->set("n2my_session", $session, $this->ns);
            }
        } elseif ('action_logout' == $request_action) {
            if ($result["status"] == "1") {
                $this->session->remove("n2my_session", $this->ns);
            }
        }
        $this->logger2->info(array($contents, $result));
        $query_str = http_build_query($params);
        $query_str = "?" . $query_str;
        $exec_time = $this->_get_passage_time();
        $result = array(
            "url" => $url.$query_str,
            "exec_time" => $exec_time,
            "response" => $contents,
            );
        print json_encode($result);
    }

    function get_api_info($path, $method_name) {
        $group_info = $this->groups[$path];
        $method_info = $this->methods[$path][$method_name];
        #print_r($method_info);
        $api = array(
            "group"     => $group_info["name"],
            "path"      => $group_info["path"],
            "method"    => $method_info["method"],
            "name"      => $method_info["name"],
            "desc"      => $method_info["desc"],
            "params"    => $method_info["params"],
            "response"  => $method_info["response"],
            "errors"    => $method_info["errors"],
        );
        return $api;
    }

}
$main = new N2MY_API_TEST();
$main->execute();
?>