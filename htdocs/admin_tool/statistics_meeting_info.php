<?php

// 環境設定
require_once('classes/AppFrame.class.php');
// 部屋情報
require_once('classes/dbi/meeting.dbi.php');

/* ***********************************************
 * 会議の情報を請求管理DBへ反映するための処理
 *
 * ***********************************************/
class AppStatisticsMeetingInfo extends AppFrame {

    var $_dsn = null;
    var $account_dsn = null;

    function init()
    {
        // DBサーバ情報取得
        if (file_exists(sprintf( "%sconfig/slave_list.ini", N2MY_APP_DIR ))) {
            $server_list = parse_ini_file( sprintf( "%sconfig/slave_list.ini", N2MY_APP_DIR ), true);
            $this->_dsn = $server_list["SLAVE_LIST"][N2MY_DEFAULT_DB];
            $this->account_dsn = $server_list["SLAVE_LIST"]["auth_dsn"];
        } else {
            $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);
            $this->_dsn = $server_list["SERVER_LIST"][N2MY_DEFAULT_DB];
            $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        }
        //$this->logger->info(__FUNCTION__.' # Array Of Dsn Lists! ', __FILE__, __LINE__, $this->_dsn);
    }


    /**
     * default view routine
     *
     */
    function default_view()
    {

        $this->logger->info(__FUNCTION__.' # GetMeetingMeetings Info Operation Start ... ', __FILE__, __LINE__);

        // 初期化
        $mtg_meetings = array();

        // パラメータ取得
        $target_day = $this->request->get('target_day');
        //$target_day = date('Y-m-d', time() - (3600*24)) . ' 00:00:00'; // 常に前日

        // パラメータ検証 ( boolearn : true / false )
        $param_is_safe = $this->_check_param($target_day);

        // パラメータ判定
        if ($param_is_safe){
            // MTG利用統計用部屋データ（差分データ）作成
            $this->mtg_meetings = array();
            //foreach ($this->_dsn as $server_key => $server_dsn) {
                $_tmp_data = null;
                //$_tmp_data = $this->_get_meeting_routine($server_dsn, $target_day);
                $_tmp_data = $this->_get_meeting_routine($this->_dsn, $target_day);
                if (is_array($_tmp_data) === true) {
                    //$this->logger->info(__FUNCTION__.' # MeetingInfo ... ', __FILE__, __LINE__, $_tmp_data);
                    //$this->logger->info(__FUNCTION__.' # MeetingInfo ... ', __FILE__, __LINE__, "### Merge Meeting Info!!");
                    $mtg_meetings = array_merge($mtg_meetings, $_tmp_data);
                    //$this->logger->info(__FUNCTION__.' # MeetingInfo ... ', __FILE__, __LINE__, $mtg_meetings);
                }
            //}
        } else {
            //$mtg_meetings = array("param_error" => "T");
            $mtg_meetings = array();
        }
        $this->logger->info(__FUNCTION__.' # MeetingInfo Count ... ', __FILE__, __LINE__, count($mtg_meetings));
        //$this->logger->debug(__FUNCTION__.' # MeetingInfo ... ', __FILE__, __LINE__, $mtg_meetings);

        // 戻り値編集
        //print_r($mtg_meetings);
        //include_once("dBug.php"); //TEST用
        //new dBug($mtg_meetings); //TEST用
        print serialize($mtg_meetings);

        $this->logger->info(__FUNCTION__.' # GetMeetingMeetings Info Operation End ... ', __FILE__, __LINE__);
    }


    /**
     * メイン取得
     *
     */
    private function _get_meeting_routine($data_db_dsn, $target_day)
    {
        $meeting_obj = new MeetingTable($data_db_dsn);
        $row = null;
        $_rows = null;

$sql = "
SELECT
 `mtg`.`meeting_key`, '' AS `fms_path`, `mtg`.`layout_key`, `mtg`.`server_key`, `mtg`.`sharing_server_key`,
 `mtg`.`user_key`, `u`.`user_id`, `mtg`.`room_key`, '' AS `meeting_ticket`, NULL AS`meeting_session_id`, `mtg`.`pin_cd`,
 `mtg`.`meeting_country`, `mtg`.`meeting_room_name`, `mtg`.`meeting_name`, `mtg`.`meeting_tag`,
 `mtg`.`meeting_max_audience`, `mtg`.`meeting_max_whiteboard`, `mtg`.`meeting_max_extendable`,
 `mtg`.`meeting_max_seat`, `mtg`.`meeting_size_recordable`, `mtg`.`meeting_size_uploadable`,
 `mtg`.`meeting_size_used`, `mtg`.`meeting_log_owner`, '' AS `meeting_log_password`,
 `mtg`.`meeting_start_datetime`, `mtg`.`meeting_stop_datetime`, `mtg`.`meeting_extended_counter`,
 `mtg`.`has_recorded_minutes`, `mtg`.`has_recorded_video`, `mtg`.`is_active`, `mtg`.`is_extended`,
 `mtg`.`is_locked`, `mtg`.`is_blocked`, `mtg`.`is_reserved`, `mtg`.`intra_fms`, `mtg`.`is_publish`,
 `mtg`.`version`, `mtg`.`create_datetime`, `mtg`.`update_datetime`, `mtg`.`room_addition`, `mtg`.`is_deleted`,
 `mtg`.`use_flg`, `mtg`.`actual_start_datetime`, `mtg`.`actual_end_datetime`, `mtg`.`meeting_use_minute`,
 `mtg`.`member_keys`, '' AS `participant_names`, `mtg`.`eco_move`, `mtg`.`eco_time`, `mtg`.`eco_fare`, `mtg`.`eco_co2`,
 '' AS `eco_info`, `mtg`.`pgi_setting_key`, `mtg`.`pgi_conference_id`, '' AS `pgi_m_pass_code`,
 '' AS `pgi_p_pass_code`, '' AS `pgi_l_pass_code`, '' AS `pgi_phone_numbers`, `mtg`.`pgi_api_status`,
 `mtg`.`tc_type`, `mtg`.`tc_teleconf_note`, `mtg`.`use_pgi_dialin`, `mtg`.`use_pgi_dialin_free`, `mtg`.`use_pgi_dialout`
 FROM `meeting` AS `mtg` LEFT JOIN  `user` AS  `u` ON (  `u`.`user_key` =  `mtg`.`user_key` )
 WHERE `mtg`.`create_datetime` LIKE '" . addslashes($target_day) . "%'
    OR `mtg`.`update_datetime` LIKE '" . addslashes($target_day) . "%' ";

        $rs = $meeting_obj->_conn->query($sql);
        if (DB::isError($rs)) {
        	$this->logger->info(__FUNCTION__." # DBError", __FILE__, __LINE__, $sql);
        }else{
            while ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                //$this->logger->info(__FUNCTION__.' # Meetings Info ... ', __FILE__, __LINE__, $row);
                $row['fms_path'] = '';
                $row['meeting_ticket'] = '';
                $row['meeting_session_id'] = '';
                $row['meeting_log_password'] = '';
                $row['eco_info'] = '';
                $row['participant_names'] = '';
                $row['pgi_m_pass_code'] = '';
                $row['pgi_p_pass_code'] = '';
                $row['pgi_l_pass_code'] = '';
                $row['pgi_phone_numbers'] = '';
                $_rows[] = $row;
            }
            //$this->logger->info(__FUNCTION__.' # Meetings Info ... ', __FILE__, __LINE__, $_rows);
        }
        return $_rows;
    }


    /**
     * パラメータ検証
     *
     */
    private function _check_param($_param = null)
    {
        $ret = false;
        //未設定チェック
        if (!$_param) {
            return $ret;
        }
        //文字数チェック
        if (strlen($_param) < 7) {
            return $ret;
        }
        //指定不可文字存在チェック
        if (preg_match("/[_%*?]/", $_param)) {
            return $ret;
        }
        //チェック OK!
        $ret = true;

        return $ret;

    }


}

$main = new AppStatisticsMeetingInfo();
$main->execute();
?>