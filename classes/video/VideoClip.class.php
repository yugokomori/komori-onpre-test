<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * PHP versions 4 and 5
 *
 * @package     nicetomeetyou For Video API Accessor
 * @author      Yuki Asano <y-asano@vcube.co.jp>
 * @copyright   V-cube Inc.
 * @license     http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version     CVS: $Id:$
 */
require_once('Video_Request.class.php');

class VideoClip extends Video_Request
{

    //---- set Variable ----
    var $_post_tmpl      = "classes/video/templates/clip/post.xml";
    var $_edit_tmpl      = "classes/video/templates/clip/edit.xml";
    var $_countEdit_tmpl = "classes/video/templates/clip/countEdit.xml";
    var $_delete_tmpl    = "classes/video/templates/clip/delete.xml";

// ------ constructor -------------------------------------------------
    //{{{ VideoClip()
    /**
     * コンストラクタ
     *
     * @access  public
     * @since   3.1.0
     */
    function VideoClip($url=null, $timeout=array('timeout'=>620))
    {
        parent::Video_Request($url, $timeout);
    }
    //}}}

// ------ public ------------------------------------------------------
    //{{{ delete()
    /**
     * 論理削除API
     *
     * @access  public
     * @param   string   $url
     * @param   string   $user
     * @param   string   $password
     * @param   array    $param
     * @param   string   $binary
     * @since   3.1.0
     */
    function delete($url, $user, $password , $param=array())
    {

        // --- set Url ---
        $this->setUrl($url);

        // --- set method ---
        $this->setMethod("delete");

        //--- create X-WSSE Heaader ---
        $xwsse = $this->createXWSSEHeader($user, $password);

        // --- add headers ---
        $this->addHeaders($xwsse);

        if ($param) {
            // --- quote ---
            $quoted = $this->quoteAll($param);

            // --- generate XML---
            $this->assignXML('p', $param);
            $xml = $this->fetchXML($this->_edit_tmpl);

            // --- add raw data ---
            $this->addRawPostData($xml, true);
        }

        // --- Request execute ---
        if (!PEAR::isError($this->sendRequest())) {
            return $this->getResponseBody();
        }

        return true;
    }
    //}}}
    //{{{ countEdit()
    /**
     * 投稿APIへリクエスト
     *
     * @access  public
     * @param   string   $url
     * @param   string   $user
     * @param   string   $password
     * @param   array    $param
     * @param   string   $binary
     * @since   3.1.0
     */
    function countEdit($url, $user, $password , $param=array())
    {

        // --- set Url ---
        $this->setUrl($url);

        // --- set method ---
        $this->setMethod("put");

        //--- create X-WSSE Heaader ---
        $xwsse = $this->createXWSSEHeader($user, $password);

        // --- add headers ---
        $this->addHeaders($xwsse);

        // --- quote ---
        $quoted = $this->quoteAll($param);

        // --- generate XML---
        $this->assignXML('p', $quoted);
        $xml = $this->fetchXML($this->_countEdit_tmpl);

        // --- add raw data ---
        $this->addRawPostData($xml, true);

        // --- Request execute ---
        if (!PEAR::isError($this->sendRequest())) {
            return $this->getResponseBody();
        }

        return true;
    }
    //}}}
    //{{{ edit()
    /**
     * 投稿APIへリクエスト
     *
     * @access  public
     * @param   string   $url
     * @param   string   $user
     * @param   string   $password
     * @param   array    $param
     * @param   string   $binary
     * @since   3.1.0
     */
    function edit($url, $user, $password , $param=array())
    {

        // --- set Url ---
        $this->setUrl($url);

        // --- set method ---
        $this->setMethod("put");

        //--- create X-WSSE Heaader ---
        $xwsse = $this->createXWSSEHeader($user, $password);

        // --- add headers ---
        $this->addHeaders($xwsse);

        // --- quote ---
        $quoted = $this->quoteAll($param);

        // --- generate XML---
        $this->assignXML('p', $quoted);
        $xml = $this->fetchXML($this->_edit_tmpl);
    	// --- add raw data ---
        $this->addRawPostData($xml, true);

        // --- Request execute ---
        if (!PEAR::isError($this->sendRequest())) {
            return $this->getResponseBody();
        }

        return true;
    }
    //}}}
    //{{{ post()
    /**
     * 投稿APIへリクエスト
     *
     * @access  public
     * @param   string   $url
     * @param   string   $user
     * @param   string   $password
     * @param   array    $param
     * @param   string   $binary
     * @since   3.1.0
     */
    function post($url, $user, $password , $param=array(), $binary=null, $resource=false, $store=false)
    {

        // --- set Url ---
        $this->setUrl($url);

        // --- set method ---
        $this->setMethod("post");

        //--- create X-WSSE Heaader ---
        $xwsse = $this->createXWSSEHeader($user, $password);

        // --- add headers ---
        $this->addHeaders($xwsse);

        // --- binary to string ---

        if($store){
            $store = $binary;
            $data  = "";
        }else if ($resource) {
            $data =& $binary;
        } else {
            $data =& $this->getFileText($binary, true);
        }

        // --- quote ---
        $quoted = $this->quoteAll($param);

        // --- generate XML---
        $this->assignXML('p', $quoted);
        $this->assignXML('data', $data);
        $this->assignXML('store', $store);

        $xml = $this->fetchXML($this->_post_tmpl);

        // --- add raw data ---
        $this->addRawPostData($xml, true);

        // --- Request execute ---
        if (!PEAR::isError($this->sendRequest())) {
            return $this->getResponseBody();
        }

        return true;
    }
    //}}}
    //{{{ feed()
    /**
     * feedClip
     * AtomPPでのフィードメソッドはGET
     *
     * @access  private
     * @param   string   $url
     * @param   string   $user
     * @param   string   $password
     * @param   array    $param
     * @since   3.1.0
     */
    function feed($url, $user, $password, $param=array())
    {

        //--- set Url ---
        $this->setUrl($url);

        //--- set method ---
        $this->setMethod('get');

        //--- create X-WSSE Heaader ---
        $xwsse = $this->createXWSSEHeader($user, $password);

        //--- add headers ---
        $this->addHeaders($xwsse);

        //--- add qeury strings ---
        $this->addQueryStrings($param);

        // --- Request execute ---
        if (!PEAR::isError($this->sendRequest())) {
            return $this->getResponseBody();
        }

        return false;
    }
    //}}}

// ------ private -----------------------------------------------------

}
?>
