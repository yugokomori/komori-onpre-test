<?php
require_once("classes/N2MY_Api.class.php");
class N2MY_Meeting_Auth_API extends N2MY_Api
{
    var $session_id = null;
    var $api_version = null;
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        // SSL対応
        header('Pragma:');
        //$this->getApiInterface();
    }

    function auth()
    {
        // ログイン以外の場合は認証させる
    }

    /**
     * API情報を返す
     */
    function action_get_info() {
        $this->logger2->info();
        $data = $this->getApiVersion();
        return $this->output($data);
    }

    function action_pin_login() {
        require_once("classes/mgm/MGM_Auth.class.php");
        require_once("classes/N2MY_Auth.class.php");
        require_once("classes/N2MY_Account.class.php");
        require_once 'classes/dbi/meeting.dbi.php';
        require_once 'classes/dbi/user.dbi.php';
        // DB選択
        $request = $this->request->getAll("pin_cd");
        $pin_cd  = $request["pin_cd"];
        // パラメタチェック
        $rules = array(
            "pin_cd" => array(
                "required" => true,
                "valid_integer" => true,
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        if (!$dsn_info = $obj_MGMClass->getRelationDsn($pin_cd, "telephone")) {
            $err_obj->set_error("pin_cd", "200800");
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $server_info = $obj_MGMClass->getServerInfo($dsn_info["server_key"]);
        if (!$dsn_info) {
            $err_obj->set_error("pin_cd", "200800");
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        // 会議情報
        $objMeeting = new MeetingTable($server_info["dsn"]);
        $where = "pin_cd = '".$pin_cd."'";
        $meeting_info = $objMeeting->getRow($where);
        if (!$meeting_info) {
            $err_obj->set_error("pin_cd", "200800");
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        if ($meeting_info["is_reserved"] &&
            ((strtotime($meeting_info['meeting_start_datetime']) > time() ||
             (strtotime($meeting_info['meeting_stop_datetime'])) < time())
            )
        ) {
            $err_obj->set_error("pin_cd", "200801");
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        // ユーザー
        $objUser = new UserTable($server_info["dsn"]);
        $where = "user_key = ".$meeting_info["user_key"];
        $user_info = $objUser->getRow($where);
        if (!$user_info) {
            $err_obj->set_error("pin_cd", "200800");
            return $this->display_error("101", "User not found", $this->get_error_info($err_obj));
        }
        $lang = $this->get_language();
        $lang_conf_file = N2MY_APP_DIR."config/lang/".$lang."/message.ini";
        $this->_message = parse_ini_file($lang_conf_file, true);
        // パラメタ取得
        $id          = $user_info["user_id"];
        $pw          = $user_info["user_password"];
        $lang        = $this->request->get("lang", "ja");
        $country     = $this->request->get("country", "jp");
        $time_zone   = $this->request->get(API_TIME_ZONE, "9");
        $enc         = $this->request->get("enc", null);
        $output_type = $this->request->get("output_type");
        $rules = array(
            "lang"     => array(
                "allow" => array_keys($this->get_language_list()),
                ),
            "country"     => array(
                "allow" => array_keys($this->get_country_list()),
                ),
            API_TIME_ZONE     => array(
                "allow" => array_keys($this->get_timezone_list()),
                ),
            "enc" => array(
                "allow" => array("md5", "sha1"),
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $request = $this->request->getAll();
        $err_obj = $this->error_check($request, $rules);
        if (!$user_info = $obj_MGMClass->getUserInfoById( $id ) ){
            $err_obj->set_error("id", "200802", $id);
        } else {
            if (!$server_info = $obj_MGMClass->getServerInfo( $user_info["server_key"] )){
                $err_obj->set_error("id", "200802", $id);
                return $this->display_error("1", "Login failed / Invalid auth token", $this->get_error_info($err_obj));
            }
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "Parameter error", $this->get_error_info($err_obj));
        } else {
            $login = new N2MY_Auth($server_info["dsn"]);
            $user_type = "";
            // ユーザ確認
            $login_info = $login->check($id, $pw, $enc);
            if (!$login_info) {
                $err_obj->set_error("pin_cd", "200802");
                return $this->display_error("1", "USER_ID_PASS_INVALID", $this->get_error_info($err_obj));
            } else if (false === $obj_N2MY_Account->checkRemoteAddress($login_info["user_info"]["user_key"], $_SERVER["REMOTE_ADDR"])){
                    $session = EZSession::getInstance();
                    $session->removeAll();
                    $err_obj->set_error("ip_white_list", "200105", $id);
                    return $this->display_error("1", "Login failed / Invalid auth token", $this->get_error_info($err_obj));
            } else {
                // セッションスタート
                $session = EZSession::getInstance();
                // ID を取得
                $session_id = session_id();
                // ユーザ情報配置
                if  ($output_type) {
                    $session->set("output_type", $output_type);
                }
                $session->set("login", "1");
                $session->set("lang", $lang);
                $session->set("country_key", $country);
                $session->set("time_zone", $time_zone);
                $session->set("dsn_key", $server_info["host_name"]);
                $session->set("server_info", $server_info);
                // ユーザー情報
                $user_info = $login_info["user_info"];
                $session->set("user_info", $login_info["user_info"]);
                $type = "user";
                // 使用可能な部屋
                $obj_N2MY_Account = new N2MY_Account($server_info["dsn"]);
                $rooms = $obj_N2MY_Account->getOwnRoomByRoomKey( $meeting_info["room_key"], $user_info["user_key"]);
                // 既存のセッションデータにも書き込む
                $session->set('room_info', $rooms);
                $session->set('api_login_flg', 1);
                // 戻り値
                $data = array(
                    "session"      => $session_id,
                    API_ROOM_ID    => $meeting_info["room_key"],
                    API_MEETING_ID => $meeting_info["meeting_session_id"],
                    );
                $this->session = $session;
                $this->add_operation_log("pin_login");
                return $this->output($data);
            }
        }
    }

    function action_login() {
        require_once("classes/mgm/MGM_Auth.class.php");
        require_once("classes/N2MY_Auth.class.php");
        require_once("classes/N2MY_Account.class.php");
        require_once("lib/EZLib/EZUtil/EZLanguage.class.php");
        $lang = $this->get_language();
        $lang_conf_file = N2MY_APP_DIR."config/lang/".$lang."/message.ini";
        $this->_message = parse_ini_file($lang_conf_file, true);
        
        // zh -> zh-cnに変換。
        if($this->request->get('lang') == 'zh') {
            $this->request->set('lang', EZLanguage::getLang(EZLanguage::getLangCd($this->request->get('lang'))));
        }
        // パラメタ取得
        $api_key     = $this->request->get("api_id");
        $id          = $this->request->get("id");
        $pw          = $this->request->get("pw");
        $lang        = $this->request->get("lang", "ja");
        $country     = $this->request->get("country", "jp");
        $time_zone   = $this->request->get(API_TIME_ZONE, "9");
        $login_type  = $this->request->get("login_type", "");
        $mac_address = $this->request->get("mac_address");
        if ($mac_address != "") {
            $mac_address = strtolower(str_replace('-', ':', $mac_address));
        }
        $enc         = $this->request->get("enc", null);
        $output_type = $this->request->get("output_type");
        // DB選択
        $this->logger->info(sprintf("api v2 action_login id:%s enc:%s login_type:%s",
                                   $id, $enc, $login_type));
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        $rules = array(
            "id"     => array(
                "required" => true
                ),
            "pw"     => array(
                "required" => true
                ),
            "lang"     => array(
                "allow" => array_keys($this->get_language_list()),
                ),
            "country"     => array(
                "allow" => array_keys($this->get_country_list()),
                ),
            API_TIME_ZONE     => array(
                "allow" => array_keys($this->get_timezone_list()),
                ),
            "login_type"     => array(
                "allow" => array('centre', 'terminal'),
                ),
            "enc" => array(
                "allow" => array("md5", "sha1"),
                ),
            "mac_address"     => array(
                "minlen" => 12,
                "regex" => '/^(([0-9a-zA-Z]{2}[:-]){5}[0-9a-zA-Z]{2},*)*$/',
                ),
            "output_type" => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $request = $this->request->getAll();
        $err_obj = $this->error_check($request, $rules);
        if (!$user_info = $obj_MGMClass->getUserInfoById( $id ) ){
            $err_obj->set_error("id", "200100", $id);
        } else {
            if (!$server_info = $obj_MGMClass->getServerInfo( $user_info["server_key"] )){
                $err_obj->set_error("id", "200100", $id);
                return $this->display_error("1", "SELECT_SERVER_ERROR", $this->get_error_info($err_obj));
            }
        }
        if (EZValidator::isError($err_obj)) {
            $this->logger2->warn(array($request, $err_obj), "PARAMETER_ERROR");
            return $this->display_error("100", "PARAMETER_ERROR", $this->get_error_info($err_obj));
        } else {
            $login = new N2MY_Auth($server_info["dsn"]);
            $user_type = "";
            // ユーザ確認
            $login_info = $login->check($id, $pw, $enc, $api_key);
            if (!$login_info) {
                $err_obj->set_error("id", "200100", $id);
                return $this->display_error("1", "Login failed / Invalid auth token", $this->get_error_info($err_obj));
            }
            $obj_N2MY_Account = new N2MY_Account($server_info["dsn"]);
            if (false === $obj_N2MY_Account->checkRemoteAddress($login_info["user_info"]["user_key"], $_SERVER["REMOTE_ADDR"])){
                $session = EZSession::getInstance();
                $session->removeAll();
                $err_obj->set_error("ip_white_list", "200105", $id);
                return $this->display_error("1", "Login failed / Invalid auth token", $this->get_error_info($err_obj));
            }
            //
            if ($login_type == "centre" || $login_type == "terminal") {
                // 指定したログインタイプが異なる
                if ($login_info["member_info"]['member_type'] != $login_type) {
                    $err_obj->set_error("id", "200103", $id);
                    return $this->display_error("1", "Login failed / Invalid auth token", $this->get_error_info($err_obj));
                //  メンバーではないため
                } elseif (!$login_info["member_info"]) {
                    $err_obj->set_error("id", "200103", $id);
                    return $this->display_error("1", "Login failed / Invalid auth token", $this->get_error_info($err_obj));
                // 部屋が割り当てられていない
                } elseif ($login_info['member_info']['room_key'] == "") {
                    $err_obj->set_error("id", "200104", $id);
                    return $this->display_error("1", "Login failed / Invalid auth token", $this->get_error_info($err_obj));
                }
                // MACアドレス 登録済み
                if ($login_info["member_info"]['mac_address'] != '') {
                    $mac_address_list = split(',', strtolower($login_info["member_info"]['mac_address']));
                    $now_mac_address_list = split(',', strtolower($mac_address));
                    $sw = 0;
                    foreach($now_mac_address_list as $_mac_address) {
                        $this->logger2->info(array($_mac_address, $mac_address_list));
                        if (in_array($_mac_address, $mac_address_list)) {
                            $sw = 1;
                            $this->logger2->info();
                            break;
                        }
                    }
                    if ($sw == 0) {
                        $this->logger2->warn(array($id, $mac_address_list), "mac_address error");
                        $err_obj->set_error("mac_address", "200101", $id);
                        return $this->display_error("1", "Login failed / Invalid auth token", $this->get_error_info($err_obj));
                    }
                // 初回ログイン時にのみMACアドレスを記録する
                } else {
                    if ($mac_address != "") {
                        require_once("classes/dbi/member.dbi.php");
                        $memberTable = new MemberTable($server_info["dsn"]);
                        $where = "member_key = ".$login_info["member_info"]['member_key'];
                        $data = array(
                            'mac_address'       => $mac_address,
                            'update_datetime'   => date('Y-m-d H:i:s')
                        );
                        $ret = $memberTable->update($data, $where);
                        if (DB::isError($ret)) {
                            $this->logger2->info($ret->getUserInfo());
                        }
                    }
                }
            }
            session_regenerate_id(true);
            // セッションスタート
            $session = EZSession::getInstance();
            $session->removeAll();
            // ID を取得
            $session_id = session_id();
            // ユーザ情報配置
            if  ($output_type) {
                $session->set("output_type", $output_type);
            }
            $this->logger2->debug($login_info);
            $session->set("login", "1");
            $session->set("lang", $lang);
            $session->set("login_type", $login_type);
            $session->set("country_key", $country);
            $session->set("time_zone", $time_zone);
            $session->set("dsn_key", $server_info["host_name"]);
            $session->set("server_info", $server_info);
            // ユーザー情報
            $user_info = $login_info["user_info"];
            $session->set("user_info", $login_info["user_info"]);
            // メンバー情報
            if (isset($login_info["member_info"])) {
                $type = "member";
                $session->set("member_info", $login_info["member_info"]);
                $member_info = $login_info["member_info"];
            } else {
                $type = "user";
            }
            // 部屋情報
            $obj_N2MY_Account = new N2MY_Account($server_info["dsn"]);
            //member かつ　ネオジャパンサービスの人は自分の部屋をもってくる
            if ( $login_info["member_info"] && ($user_info["account_model"] == "member")) {
                $rooms = $obj_N2MY_Account->getOwnRoom( $member_info["member_key"], $user_info["user_key"]);
            } else {
                $rooms = $obj_N2MY_Account->getRoomList( $user_info["user_key"] );
            }
            // 既存のセッションデータにも書き込む
            $session->set('room_info', $rooms);
            $session->set('api_login_flg', 1);
            // ユーザー情報整形
            // 戻り値
            $data = array(
                "type" => $type,
                "session" => $session_id,
                "summer_time_flg" => $this->getSummerTime(),
                "user_info" => array(
                    'user_id'           => $user_info['user_id'],
                    'user_name'         => $user_info['user_company_name'],
                    'is_minutes_delete' => $user_info['is_minutes_delete'] ? $user_info['is_minutes_delete'] : 0,
                    'is_compulsion_pw'  => $user_info['is_compulsion_pw'] ? $user_info['is_compulsion_pw'] : 0,
                    ),
                "member_info" => array(
                    'member_id'         => $member_info['member_id'],
                    'member_email'      => $member_info['member_email'],
                    'member_name'       => $member_info['member_name'],
                    'member_name_kana'  => $member_info['member_name_kana'],
                    'timezone'          => $member_info['timezone'],
                    'lang'              => $member_info['lang'],
                    ),
                );
            $this->session = $session;
            $this->add_operation_log("login");
            return $this->output($data);
        }
    }

    function action_get_login_info() {
        $this->checkAuthorization();
        // チェックルール編集
        $rules = array(
            "output_type"     => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        // チェックデータ（リクエスト）編集
        $request = $this->request->getAll();
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "Parameter error", $this->get_error_info($err_obj));
        }
        $user_info   = $this->session->get("user_info");
        $member_info = $this->session->get("member_info");
        if ($member_info) {
            $type = 'member';
        } else {
            $type = 'member';
        }
        // 戻り値
        $data = array(
            "type" => $type,
            "user_info" => array(
                'user_id'           => $user_info['user_id'],
                'user_name'         => $user_info['user_company_name']
                ),
            "member_info" => array(
                'member_id'         => $member_info['member_id'],
                'member_email'      => $member_info['member_email'],
                'member_name'       => $member_info['member_name'],
                'member_name_kana'  => $member_info['member_name_kana'],
                'timezone'          => $member_info['timezone'],
                'lang'              => $member_info['lang'],
                ),
            );
        return $this->output($data);
    }

    function action_get_presence_info() {
        $this->checkAuthorization();
        // チェックルール編集
        $rules = array(
            "output_type"     => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        // チェックデータ（リクエスト）編集
        $request = $this->request->getAll();
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "Parameter error", $this->get_error_info($err_obj));
        }
        $user_info = $this->session->get('user_info');
        $data = array(
            'fms_server' => $this->config->get('PRESENCE_APP', 'fms'),
            'appli'      => $this->config->get('PRESENCE_APP', 'appli'),
            'has_chat'   => $user_info['has_chat'] ? $user_info['has_chat'] : 0,
        );
        return $this->output($data);
    }


    /**
     * メンバー一覧取得
     *
     * @param
     */
    function action_get_member_list() {
        $this->checkAuthorization();
        // チェックルール編集
        $rules = array(
            "output_type"     => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        // チェックデータ（リクエスト）編集
        $request = $this->request->getAll();
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "Parameter error", $this->get_error_info($err_obj));
        }
        require_once("classes/dbi/member.dbi.php");
        require_once("classes/dbi/member_group.dbi.php");
        $memberTable        = new MemberTable($this->get_dsn());
        $memberGroupTable   = new MemberGroupTable($this->get_dsn());

        $server_timezone = $this->config->get("GLOBAL", "time_zone", N2MY_SERVER_TIMEZONE); // このサーバーのタイムゾーン
        $user_info = $this->session->get("user_info");
        if ($request['last_update']) {
            require_once ("lib/EZLib/EZUtil/EZDate.class.php");
            if (is_numeric($request['last_update'])) {
                $last_update_ts = $request['last_update'];
            } else {
                $user_timezone = $this->session->get('time_zone');
                $last_update_ts = EZDate::getLocateTime($request['last_update'], $server_timezone, $user_timezone);
            }
            $last_update = EZDate::getZoneDate("Y-m-d H:i:s", $server_timezone, $last_update_ts);
            $where = "user_key = ".addslashes($user_info["user_key"]).
                 " AND update_datetime > '".$last_update."'";
        } else {
            $where = "user_key = ".addslashes($user_info["user_key"]).
                 " AND member_status = 0";
        }
        $login_type = $this->session->get('login_type');
        if ($login_type == 'terminal' || $login_type == 'centre') {
            $where .= " AND (member_type = 'terminal' OR member_type = 'centre')";
        }
        $data["members"]["member"] = array();
        $member_list = $memberTable->getRowsAssoc($where);
        $group_list  = $memberGroupTable->getAssoc($user_info["user_key"]);

        foreach($member_list as $_key => $_val) {
            $data["members"]["member"][$_key] = array(
                'member_id'         => $_val['member_id'],
                'member_email'      => $_val['member_email'],
                'member_name'       => $_val['member_name'],
                'member_name_kana'  => $_val['member_name_kana'],
                'member_group'      => ($_val['member_group']) ? $_val['member_group'] : null,
                'member_group_name' => $group_list[$_val['member_group']],
                'timezone'          => $_val['timezone'],
                'lang'              => $_val['lang']
                );
            //
            if ($request['last_update']) {
                $data["members"]["member"][$_key]['is_delete'] = ($_val['member_status'] == "0") ? 0 : 1;
//                $data["members"]["member"][$_key]['update_datetime'] = $_val['update_datetime'];
            }
        }
        return $this->output($data);
    }

    /**
     * 入室可能な部屋一覧取得
     */
    function action_get_room_list() {
        require_once("classes/N2MY_Account.class.php");
        require_once("classes/dbi/room.dbi.php");
        $this->checkAuthorization();
        $server_info = $this->session->get("server_info");
        $obj_N2MY_Account = new N2MY_Account($server_info["dsn"]);
        $user_info = $this->session->get("user_info");
        $member_info = $this->session->get("member_info");
        // チェックルール編集
        $rules = array(
            "output_type"     => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        $room_info = array();
        if ( $member_info && ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre" || $member_info["room_key"] != "")) {
            $room_info = $obj_N2MY_Account->getOwnRoom( $member_info["member_key"], $user_info["user_key"]);
        } else {
            $room_info = $obj_N2MY_Account->getRoomList( $user_info["user_key"] );
        }
        /*
        $obj_Room = new RoomTable($this->get_dsn());
        $where = "user_key = ".addslashes($user_info["user_key"]).
             " AND room_status = 1";
        $result = $obj_Room->getRowsAssoc($where, null, null, null, "room_key,room_name","room_key");
        */
        $room_list = array();
        foreach($room_info as $room_key => $room) {
            // 記録最大容量
            if( $user_info["account_model"] == 'member'){
                $meeting_rec_size = 0;
                $meeting_rec_size += $room['options']["hdd_extention"] * 500 * 1024 * 1024;  // 500m
            } else {
                $meeting_rec_size = RECORD_SIZE * (1024 * 1024);          // デフォルト500MByte
                $meeting_rec_size += $room['options']["hdd_extention"] * 1024 * 1024 * 1024;  // 1GBtype/契約
            }
            $roomList = array(
                "room_info" => array(
                    'room_id'               => $room['room_info']['room_key'],
                    'max_seat'              => $room['room_info']['max_seat'],
                    'max_audience_seat'     => $room['room_info']['max_audience_seat'],
                    'max_whiteboard_seat'   => $room['room_info']['max_whiteboard_seat'],
                    'room_name'             => $room['room_info']['room_name'],
                    'mfp'                   => $room['room_info']['mfp'],
                    'mfp_address'           => $room['room_info']['address'],
                    'cabinet'               => $room['room_info']['cabinet'],
                    'max_record_size'       => $meeting_rec_size
                ),
                "options" => array(
                    'meeting_ssl'           => $room['options']['meeting_ssl'],
                    'desktop_share'         => $room['options']['desktop_share'],
                    'high_quality'          => $room['options']['high_quality'],
                    'mobile_phone_number'   => $room['options']['mobile_phone_number'],
                    'h323_number'           => $room['options']['h323_number'],
                    'whiteboard'            => $room['options']['whiteboard'],
                    'multicamera'           => $room['options']['multicamera'],
                    'telephone'             => $room['options']['telephone'],
                    'smartphone'            => $room['options']['smartphone'],
                    'record_gw'             => $room['options']['record_gw'],
                )
            );
            $room_list[] = $roomList;
        }
        // チェックルール編集
        $rules = array(
            "output_type"     => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        // チェックデータ（リクエスト）編集
        $request = array(
            "output_type" => $this->request->get("output_type", null),
            );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "Parameter error", $this->get_error_info($err_obj));
        }
        $data["rooms"] = array(
            "room" => $room_list,
        );
        return $this->output($data);
    }

    /**
     * 入室可能な部屋詳細取得
     */
    function action_get_room_detail() {
        require_once("classes/dbi/room.dbi.php");
        $this->checkAuthorization();
        $room_key = $this->request->get(API_ROOM_ID);
        // チェックルール編集
        $rules = array(
            API_ROOM_ID     => array(
                "required" => true,
                "maxlen"   => 64,
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        // チェックデータ（リクエスト）編集
        $request = array(
            API_ROOM_ID    => $room_key,
            "output_type" => $this->request->get("output_type", null),
            );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // 存在チェック
        $room_info = $this->session->get("room_info");
        if (!$room_info[$room_key]) {
            $err_obj->set_error(API_ROOM_ID, "200003", $room_key);
        }
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "Parameter error", $this->get_error_info($err_obj));
        }
        //
        $room_info[$room_key]["room_info"][API_ROOM_ID] = $room_info[$room_key]["room_info"]["room_key"];
        if ($this->config->get("N2MY", "mail_wb_upload")) {
            $room_info[$room_key]["room_info"]["mfp_address"] = $room_info[$room_key]["room_info"]["room_key"]."@".$this->config->get("N2MY", "mail_wb_host");
        }

        $room_info[$room_key] = array(
            "room_info" => array(
                'room_id'                 => $room_info[$room_key]['room_info']['room_key'],
                'max_seat'                => $room_info[$room_key]['room_info']['max_seat'],
                'max_audience_seat'       => $room_info[$room_key]['room_info']['max_audience_seat'],
                'max_whiteboard_seat'     => $room_info[$room_key]['room_info']['max_whiteboard_seat'],
                'room_name'               => $room_info[$room_key]['room_info']['room_name'],
                'mfp'                     => $room_info[$room_key]['room_info']['mfp'],
                'mfp_address'             => $room_info[$room_key]['room_info']['address'],
                'cabinet'                 => $room_info[$room_key]['room_info']['cabinet'],
              ),
          "options" => array(
                'meeting_ssl'             => $room_info[$room_key]['options']['meeting_ssl'],
                'desktop_share'           => $room_info[$room_key]['options']['desktop_share'],
                'high_quality'            => $room_info[$room_key]['options']['high_quality'],
                'mobile_phone_number'     => $room_info[$room_key]['options']['mobile_phone_number'],
                'h323_number'             => $room_info[$room_key]['options']['h323_number'],
                'whiteboard'              => $room_info[$room_key]['options']['whiteboard'],
                'multicamera'             => $room_info[$room_key]['options']['multicamera'],
                'telephone'               => $room_info[$room_key]['options']['telephone'],
                'smartphone'              => $room_info[$room_key]['options']['smartphone'],
                'record_gw'               => $room_info[$room_key]['options']['record_gw'],
            )
        );

//        unset($room_info[$room_key]["room_info"]["room_key"]);
//        unset($room_info[$room_key]["room_info"]["user_key"]);
//        unset($room_info[$room_key]["room_info"]["meeting_key"]);
//        unset($room_info[$room_key]["room_info"]["room_layout_type"]);
//        unset($room_info[$room_key]["room_info"]["room_status"]);
//        unset($room_info[$room_key]["room_info"]["room_registtime"]);
//        unset($room_info[$room_key]["room_info"]["room_updatetime"]);
//        unset($room_info[$room_key]["room_info"]["room_deletetime"]);
//        unset($room_info[$room_key]["room_info"]["room_sort"]);
//        unset($room_info[$room_key]["displayRoom"]);
//        unset($room_info[$room_key]["options"]["hdd_extention"]);
//        unset($room_info[$room_key]["service_info"]);
        /*
        unset($room_info[$room_key]["service_info"]["service_key"]);
        unset($room_info[$room_key]["service_info"]["country_key"]);
        unset($room_info[$room_key]["service_info"]["service_initial_charge"]);
        unset($room_info[$room_key]["service_info"]["service_charge"]);
        unset($room_info[$room_key]["service_info"]["service_additional_charge"]);
        unset($room_info[$room_key]["service_info"]["service_freetime"]);
        unset($room_info[$room_key]["service_info"]["service_registtime"]);
        unset($room_info[$room_key]["service_info"]["service_status"]);
        unset($room_info[$room_key]["service_info"]["trial"]);
        */
        $data = $room_info[$room_key];
        return $this->output($data);
    }

    /**
     * 部屋のステータス取得
     */
    function action_get_room_status() {
        require_once ("classes/dbi/reservation.dbi.php");
        require_once ("classes/dbi/room.dbi.php");
        require_once ('lib/EZLib/EZUtil/EZDate.class.php');
        require_once ("classes/AppFrame.class.php");
        require_once ("classes/N2MY_Reservation.class.php");
        require_once ("classes/core/Core_Meeting.class.php");
        require_once ("classes/core/dbi/Meeting.dbi.php");
        $this->checkAuthorization();
        $objReservation = new N2MY_Reservation($this->get_dsn());
        $obj_Room = new RoomTable($this->get_dsn());
//        $_rooms = $this->request->get(API_ROOM_ID);
        $room_key = $this->request->get(API_ROOM_ID);
        $reservations = array();
        $room_info = $this->session->get("room_info");

        // チェックルール編集
        $rules = array(
            API_ROOM_ID     => array(
                "required" => true,
                "allow"    => array_keys($room_info),
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        // チェックデータ（リクエスト）編集
        $request = array(
            API_ROOM_ID    => $room_key,
            "output_type" => $this->request->get("output_type", null),
            );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "Parameter error", $this->get_error_info($err_obj));
        }

        // 部屋と会議キーを指定
//        if (is_array($_rooms)) {
//            foreach ($_rooms as $room_key => $meeting_key) {
//                // 契約した部屋のみ取得可
//                $query["rooms[".$room_key."]"] = $meeting_key;
//                // 予約情報取得
////                $reservations[$room_key] = $this->_get_reservation_list($room_key);
//            }
//        // 部屋のみ指定（現在の状態）
//        } else {
//            $roomkeys = explode(",", $_rooms);
//            $rooms = array();
//            foreach($roomkeys as $room_key) {
//                $rooms[] = $room_key;
//            }
//            $where = "room_key in ('".join("','", $rooms)."')";
//            $rooms = $obj_Room->getRowsAssoc($where, null, null, null, "room_key, meeting_key");
//            if ( DB::isError( $rooms ) ) {
//                $this->logger->error( $rs->getUserInfo() );
//                return $rooms;
//            }
//            foreach( $rooms as $key => $val ){
//                $query[$val["room_key"]] = $val["meeting_key"];
////                $reservation_list = $this->_get_reservation_list( $val["room_key"] );
//            }
            $where = "room_key = '".addslashes($room_key)."'";
            $room_info = $obj_Room->getRow($where, "room_key, meeting_key");
            if ( DB::isError( $room_info ) ) {
                $this->logger->error( $room_info->getUserInfo() );
            } else {
                $query[$room_info["room_key"]] = $room_info["meeting_key"];
//                $reservation_list = $this->_get_reservation_list( $val["room_key"] );
            }
//        }
        // 指定会議の予約情報取得
        $obj_CoreMeeting = new Core_Meeting($this->get_dsn());
        $room_list = $obj_CoreMeeting->getMeetingStatus( $query );
//        $this->logger2->info($room_list);
        $list = array();
        foreach( $room_list as $key => $room){
            $participants = array();
            $participants["participant"] = array();
            foreach($room["participants"] as $participant) {
                // 余計な情報は公開しない
                $participants["participant"][] = array(
//                    "use_count"             => $participant["use_count"],
                    "participant_id"        => $participant["participant_session_id"],
                    "participant_name"      => $participant["participant_name"],
                    "participant_type"      => $participant["participant_type_name"],
                    );
            }
            //$room["participants"]["participant"] = $participants;
            // 会議中か、空室かという意味にする。
            $room["status"] = (($room["status"] == 1 && $room["pcount"] > 0)) ? 1 : 0;
            // パラメタ
            $options = array(
                "start_time" => date("Y-m-d H:i:s"),
                "end_time"   => "",
                "limit"      => RESERVATION_MAX_VIEW,
                "offset"     => null,
                "sort_key"   => "reservation_starttime",
                "sort_type"  => "asc",
                );

            $_resavation_list = $objReservation->getList($room["room_key"], $options);
            $resavations["reservation"] = array();
            foreach($_resavation_list as $_key => $_val) {
                if ($reservation_data = $objReservation->getDetail($_val['reservation_session'])) {
                    $info = array(
                        'reservation_name'      => $reservation_data["info"]["reservation_name"],
                        'reservation_pw'        => ($reservation_data["info"]["reservation_pw"]) ? 1 : 0 ,
                        'status'                => $reservation_data["info"]["status"],
                        'sender'                => $reservation_data["info"]["sender"],
                        'sender_mail'           => $reservation_data["info"]["sender_mail"],
                        'mail_body'             => $reservation_data["info"]["mail_body"],
                        API_ROOM_ID             => $reservation_data["info"]["room_key"],
                        API_MEETING_ID          => $this->ticket_to_session($reservation_data["info"]["meeting_key"]),
                        API_RESERVATION_ID      => $reservation_data["info"]["reservation_session"],
                        API_RESERVATION_START   => strtotime($reservation_data["info"]["reservation_starttime"]),
                        API_RESERVATION_END     => strtotime($reservation_data["info"]["reservation_endtime"]),
                        'url'                   => $reservation_data["info"]["url"]
                        );
                    $country_key = $this->session->get("country_key");
                    $guests = array();
                    foreach ($reservation_data["guests"] as $_key => $guest) {
                        $guests[] = array(
                            'name'       => $guest["name"],
                            'email'      => $guest["email"],
                            'timezone'   => $guest["timezone"],
                            'lang'       => $guest["lang"],
                            'type'       => ($guest["type"] == 1) ? "audience" : "" ,
                            API_GUEST_ID => $guest["r_user_session"],
                            'invite_url' => N2MY_BASE_URL."r/".$guest["r_user_session"].'&c='.$country_key.'&lang='.$guest["lang"]
                         );
                    }
                    $documents = array();
                    foreach ($reservation_data["documents"] as $_key => $_val) {
                        $documents[] = array(
                            'document_id'   => $_val["document_id"],
                            'name'          => $_val["name"],
                            'type'          => $_val["type"],
                            'category'      => $_val["category"],
                            'status'        => $_val["status"],
                            'index'         => $_val["index"],
                            'sort'          => $_val["sort"]
                         );
                    }
                    $reservation_info = array(
                        "info"      => $info,
                        "guests"    => array("guest" => $guests),
                        "documents" => array("document" => $documents)
                        );
                    $resavations["reservation"][] = $reservation_info;
                }
            }
            $room[API_ROOM_ID] = $room["room_key"];
            $current_meeting_pw = $objReservation->obj_Reservation->getOne("meeting_key = '".addslashes($room["meeting_ticket"])."'", 'reservation_pw');
            $this->logger2->info(array("meeting_key = '".addslashes($room["meeting_ticket"])."'", $current_meeting_pw));
            $room[API_MEETING_ID] = $this->ticket_to_session($room["meeting_ticket"]);
            $room = array(
                'room_id'       => $room['room_id'],
                'meeting_id'    => $room['meeting_id'],
                'use_password'  => ($current_meeting_pw) ? 1 : 0,
                'status'        => $room['status'],
                'pcount'        => $room['pcount'],
                'participants'  => $participants,
                'reservations'  => $resavations,
            );
//            $list[] = $room;
//            $room;
            break;
        }
        $data = array(
            "room_status" => $room,
        );
        return $this->output($data);
    }

    function action_get_room_info() {
        require_once ("classes/dbi/reservation.dbi.php");
        require_once ("classes/dbi/room.dbi.php");
        require_once ('lib/EZLib/EZUtil/EZDate.class.php');
        require_once ("classes/AppFrame.class.php");
        require_once ("classes/N2MY_Reservation.class.php");
        require_once ("classes/core/Core_Meeting.class.php");
        require_once ("classes/core/dbi/Meeting.dbi.php");
        $this->checkAuthorization();
        $objReservation = new N2MY_Reservation($this->get_dsn());
        $obj_Room       = new RoomTable($this->get_dsn());
        $objMeeting     = new DBI_Meeting($this->get_dsn());

        $room_key = $this->request->get(API_ROOM_ID);
        $reservations = array();
        $user_info = $this->session->get("user_info");
        $room_info = $this->session->get("room_info");
            //$this->logger2->info($room_info);
        // チェックルール編集
        $rules = array(
            API_ROOM_ID     => array(
                "required" => true,
                "allow"    => array_keys($room_info),
                ),
            "output_type"  => array(
                "allow"    => $this->get_output_type_list(),
                ),
        );
        // チェックデータ（リクエスト）編集
        $request = array(
            API_ROOM_ID    => $room_key,
            "output_type" => $this->request->get("output_type", null),
            );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "Parameter error", $this->get_error_info($err_obj));
        }
        $where = "room_key = '".addslashes($room_key)."'";
        $room_keys = $obj_Room->getRow($where, "room_key, meeting_key");
        if ( DB::isError( $room_keys ) ) {
            $this->logger->error( $room_keys->getUserInfo() );
        } else {
            $query[$room_keys["room_key"]] = $room_keys["meeting_key"];
        }
        // 指定会議の予約情報取得
        $obj_CoreMeeting = new Core_Meeting($this->get_dsn());
        $room_list = $obj_CoreMeeting->getMeetingStatus( $query );
        $list = array();

        foreach( $room_list as $key => $room){
            $participants = array();
            $participants["participant"] = array();
            foreach($room["participants"] as $participant) {
                // 余計な情報は公開しない
                $participants["participant"][] = array(
                    "participant_id"            => $participant["participant_session_id"],
                    "participant_name"          => $participant["participant_name"],
                    "participant_station"       => $participant["participant_station"],
                    "participant_start_date"    => ($participant["uptime_start"]) ? strtotime($participant["uptime_start"]) : "",
                    "participant_end_date"      => ($participant["uptime_end"]) ? strtotime($participant["uptime_end"]) : "",
                    "participant_type"          => $participant["participant_type_name"],
                    "participant_mode"          => $participant["participant_mode_name"],
                    "member_id"                 => $this->_get_member_id($user_info["user_key"], $participant["member_key"]),
                    "use_count"                 => $participant["use_count"],
                    );
            }
            // 記録最大容量
            if( $user_info["account_model"] == 'member'){
                $meeting_rec_size = 0;
                $meeting_rec_size += $room_info[$room['room_key']]['options']["hdd_extention"] * 500 * 1024 * 1024;  // 500m
            } else {
                $meeting_rec_size = RECORD_SIZE * (1024 * 1024);          // デフォルト500MByte
                $meeting_rec_size += $room_info[$room['room_key']]['options']["hdd_extention"] * 1024 * 1024 * 1024;  // 1GBtype/契約
            }
            $roomList = array(
                "room_info" => array(
                    'room_id'               => $room_info[$room['room_key']]['room_info']['room_key'],
                    'max_seat'              => $room_info[$room['room_key']]['room_info']['max_seat'],
                    'max_audience_seat'     => $room_info[$room['room_key']]['room_info']['max_audience_seat'],
                    'max_whiteboard_seat'   => $room_info[$room['room_key']]['room_info']['max_whiteboard_seat'],
                    'room_name'             => $room_info[$room['room_key']]['room_info']['room_name'],
                    'mfp'                   => $room_info[$room['room_key']]['room_info']['mfp'],
                    'mfp_address'           => $room_info[$room['room_key']]['room_info']['address'],
                    'cabinet'               => $room_info[$room['room_key']]['room_info']['cabinet'],
                    'max_record_size'       => $meeting_rec_size,
                ),
                "options" => array(
                    'meeting_ssl'           => $room_info[$room['room_key']]['options']['meeting_ssl'],
                    'desktop_share'         => $room_info[$room['room_key']]['options']['desktop_share'],
                    'high_quality'          => $room_info[$room['room_key']]['options']['high_quality'],
                    'mobile_phone_number'   => $room_info[$room['room_key']]['options']['mobile_phone_number'],
                    'h323_number'           => $room_info[$room['room_key']]['options']['h323_number'],
                    'whiteboard'            => $room_info[$room['room_key']]['options']['whiteboard'],
                    'multicamera'           => $room_info[$room['room_key']]['options']['multicamera'],
                    'telephone'             => $room_info[$room['room_key']]['options']['telephone'],
                    'smartphone'            => $room_info[$room['room_key']]['options']['smartphone'],
                    'record_gw'             => $room_info[$room['room_key']]['options']['record_gw'],
                )
            );

            //$room["participants"]["participant"] = $participants;
            // 会議中か、空室かという意味にする。
            $room["status"] = (($room["status"] == 1 && $room["pcount"] > 0)) ? 1 : 0;
            // パラメタ
            $options = array(
                "start_time" => date("Y-m-d H:i:s"),
                "end_time"   => "",
                "limit"      => RESERVATION_MAX_VIEW,
                "offset"     => null,
                "sort_key"   => "reservation_starttime",
                "sort_type"  => "asc",
                );
            $room[API_ROOM_ID] = $room["room_key"];
            $current_reservation_info = $objReservation->obj_Reservation->getRow("meeting_key = '".addslashes($room["meeting_ticket"])."'");
            //$this->logger2->info(array("meeting_key = '".addslashes($room["meeting_ticket"])."'", $current_meeting_pw));
            $room[API_MEETING_ID] = $this->ticket_to_session($room["meeting_ticket"]);
            if ($room['meeting_id']) {
                $current_meeting_info = $objMeeting->getRow("meeting_session_id = '".addslashes($room['meeting_id'])."'");
            }
            $room = array(
                'meeting_id'        => $room['meeting_id'],
                'reservation_id'    => $current_reservation_info['reservation_session'],
                'meeting_name'      => $current_meeting_info['meeting_name'],
                'use_password'      => ($current_reservation_info['reservation_pw']) ? 1 : 0,
                'status'            => $room['status'],
                'pcount'            => $room['pcount'],
                'participants'      => $participants,
            );
            break;
        }
        $data = array(
            "room" => $roomList,
            "status" => $room,
        );
        return $this->output($data);
    }

    /**
     * パスワード変更
     */
    function action_change_password() {
        $this->checkAuthorization();
        $new_passwd = $this->request->get("pw");
//        $new_passwd = $this->request->get("passwd");
        $rules = array(
            "pw"     => array(
                "required" => true,
                "minlen" => 8,
                "maxlen" => 128,
                ),
        );
        $data = array(
            "pw"     => $new_passwd,
        );
        $err_obj = $this->error_check($data, $rules);
        if (!preg_match('/^[!-\[\]-~]{8,128}$/', $new_passwd)) {
            $err_obj->set_error("pw", "200200");
        } elseif (!preg_match('/[[:alpha:]]+/', $new_passwd) || preg_match('/^[[:alpha:]]+$/', $new_passwd)) {
            $err_obj->set_error("pw", "200201");
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(100, "Parameter error", $this->get_error_info($err_obj));
        }
        if ($member_info = $this->session->get("member_info")) {
            require_once("classes/dbi/member.dbi.php");
            $objMember = new MemberTable($this->get_dsn());
            $result = $objMember->changePasswd($member_info["member_key"], $new_passwd);
            $this->add_operation_log("change_login_password");
        } else {
            $user_info = $this->session->get("user_info");
            require_once("classes/dbi/user.dbi.php");
            $objUser = new UserTable($this->get_dsn());
            $result = $objUser->changePasswd($user_info["user_key"], $new_passwd);
            $this->add_operation_log("change_login_password");
        }
        return $this->output();
    }

    /**
     * ユーザー情報の更新
     *
     * ユーザー情報自体がそんなに無いので取りあえず、名前の変更ぐらいにしておく
     */
    function action_update() {
        $this->checkAuthorization();
        // チェックルール編集
        $rules = array(
            "email"     => array(
                "email" => true
                ),
            "member_name"     => array(
                "maxlen" => 196605,
                ),
            "member_name_kana"     => array(
                "maxlen" => 196605,
                ),
            "timezone"     => array(
                "allow" => array_keys($this->get_timezone_list()),
                ),
            "lang"     => array(
                "allow" => array_keys($this->get_language_list()),
                ),
            "country"     => array(
                "allow" => array_keys($this->get_country_list()),
                ),
            "output_type"     => array(
                "allow" => $this->get_output_type_list(),
                ),
        );

        // zh -> zh-cnに変換。
        if($this->request->get('lang') == 'zh') {
            $this->request->set('lang', EZLanguage::getLang(EZLanguage::getLangCd($this->request->get('lang'))));
        }

        // チェックデータ（リクエスト）編集
        $request = array(
            "email"       => $this->request->get("email", null),
            "name"        => $this->request->get("name", null),
            "name_kana"   => $this->request->get("name_kana", null),
            "timezone"    => $this->request->get("timezone", null),
            "lang"        => $this->request->get("lang", null),
            "country"     => $this->request->get("country", null),
            "output_type" => $this->request->get("output_type", null),
            );
        $err_obj = $this->error_check($request, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "Parameter error", $this->get_error_info($err_obj));
        }

        // メンバーの場合はユーザー情報を変更
        if ($member_info = $this->session->get("member_info")) {
            require_once("classes/dbi/member.dbi.php");
            $objMember = new MemberTable($this->get_dsn());
            // メール
            if ($this->request->get("email")) {
                    $data["member_email"] = $this->request->get("email");
            }
            // 名前
            if ($this->request->get("name")) {
                $data["member_name"] = $this->request->get("name");
            }
            // かな
            if ($this->request->get("name_kana")) {
                $data["member_name_kana"] = $this->request->get("name_kana");
            }
            // タイムゾーン
            if ($this->request->get("timezone")) {
                $data["timezone"] = $this->request->get("timezone");
            }
            // 言語
            if ($this->request->get("lang")) {
                $data["lang"] = $this->request->get("lang");
            }
            $where = "member_key = ".$member_info["member_key"];
            $objMember->update($data, $where);
            // メンバー情報更新
            $member_info = $objMember->getDetail($member_info["member_key"]);
            $this->session->set("member_info", $member_info);
        }
        // タイムゾーン
        if ($this->request->get("timezone")) {
            $this->session->set("time_zone", $this->request->get("timezone"));
        }
        // 言語
        if ($this->request->get("lang")) {
            $this->session->set("lang", $this->request->get("lang"));
        }
        // 開催地域
        if ($this->request->get("country")) {
            $this->session->set("country_key", $this->request->get("country"));
        }
        return $this->output();
    }

    /**
     * ログアウト処理
     *
     * @param
     * @return
     */
    function action_logout()
    {
        $this->checkAuthorization();
        // チェックルール編集
        $rules = array(
            "output_type"     => array(
                "allow" => $this->get_output_type_list(),
                ),
        );
        // チェックデータ（リクエスト）編集
        $request = array(
            "output_type" => $this->request->get("output_type", null),
            );
        // チェック
        $err_obj = $this->error_check($request, $rules);
        // チェック判定
        if (EZValidator::isError($err_obj)) {
            return $this->display_error("100", "Parameter error", $this->get_error_info($err_obj));
        }
        $status = 0;
        setcookie(session_name(), '', time() - 1800, '/');
        if (session_destroy()) {
            $status = 1;
            $this->add_operation_log("logout");
        }
        return $this->output();
    }

}
$main = new N2MY_Meeting_Auth_API();
$main->execute();
