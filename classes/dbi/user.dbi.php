<?php
require_once("classes/N2MY_DBI.class.php");

/*
 * Created on 2006/02/15
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
class UserTable extends N2MY_DB {

    var $table = 'user';
    var $logger = null;
    protected $primary_key = "user_key";

    /**
     * DB接続
     */
    function UserTable($dsn) {
        $this->init($dsn, $this->table);
        $this->logger = EZLogger::getInstance();
    }

    /**
     * ログイン認証を行い、存在すればユーザ情報を返す
     */
    function checkLogin($user_id, $user_pw, $enc_type = null, $api_key = null, $login_type = null) {
        $where = " user_id = '".addslashes($user_id)."'".
//            " AND user_password = '".addslashes($user_pw)."'".
            " AND user_starttime <= '".date("Y-m-d H:i:s")."'".
            " AND user_delete_status < 2";
        $user_info = $this->getRow( $where );
        if (DB::isError( $user_info ) ) {
            $this->logger->error(__FUNCTION__, __FILE__, __LINE__, $user_info->getUserInfo());
            return $user_info;
        }
        if ($user_info) {
            require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
            $user_password = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $user_info["user_password"]);
            $user_admin_password = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $user_info["user_admin_password"]);
            $this->logger2->debug(array($user_info));
            if ($api_key) {
                require_once("classes/mgm/dbi/api_auth.dbi.php");
                $objApiAuth = new ApiAuthTable(N2MY_MDB_DSN);
                $where = "api_key = '".$api_key."'" .
                        " AND status = 1";
                $secret = $objApiAuth->getOne($where, 'secret');
                if (DB::isError($secret)) {
                    $this->logger2->error($secret->getUserInfo());
                    return false;
                } else {
                    if ($secret) {
                        $db_password = hash('sha256', $user_id.hash('sha256', $user_password).$secret);
                    } else {
                        $this->logger2->warn('不正なAPI_KEYが指定されました。');
                        return false;
                    }
                }
            } else if ("centre_admin_login" == $login_type) {
                switch ($enc_type) {
                    case "md5":
                        $db_password = md5($user_admin_password);
                        break;
                    case "sha1":
                        $db_password = sha1($user_admin_password);
                        break;
                    default :
                        $db_password = $user_admin_password;
                        break;
                }
            } else {
                switch ($enc_type) {
                    case "md5":
                        $db_password = md5($user_password);
                        break;
                    case "sha1":
                        $db_password = sha1($user_password);
                        break;
                    default :
                        $db_password = $user_password;
                        break;
                }
            }
            if (($user_info["user_id"] == $user_id) && ($db_password === $user_pw)) {
                $this->logger->info(__FUNCTION__."#Login", __FILE__, __LINE__, array(
                    $user_id,
                    //$user_info["user_id"],
                    //$user_pw,
                    //$db_password,
                ));
                return $user_info;
            } else {
                $this->logger->info(__FUNCTION__."#大文字、小文字違い", __FILE__, __LINE__, array(
                    $user_id,
                    $user_info["user_id"],
                    $user_pw,
                    $db_password,
                ));
                return null;
            }
        } else {
            return null;
        }
    }

    function changePasswd($user_key, $new_password) {
        if (!$user_key) {
            return false;
        }
        //ユーザーパスワードをチェック
        if (!preg_match('/^[!-\[\]-~]{8,128}$/', $new_password)) {
            return false;
        } elseif (!preg_match('/[[:alpha:]]+/',$new_password) || preg_match('/^[[:alpha:]]+$/',$new_password)) {
            return false;
        }
        require_once ('lib/EZLib/EZUtil/EZEncrypt.class.php');
        $data = array("user_password" => EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $new_password));
        $where = "user_key = ".$user_key;
        return $this->update($data, $where);
    }

    function changeAdminPasswd($user_key, $new_password) {
        if (!$user_key) {
            return false;
        }
        //ユーザーパスワードをチェック
        if (!preg_match('/^[!-\[\]-~]{8,128}$/', $new_password)) {
            return false;
        } elseif (!preg_match('/[[:alpha:]]+/',$new_password) || preg_match('/^[[:alpha:]]+$/',$new_password)) {
            return false;
        }
        require_once ('lib/EZLib/EZUtil/EZEncrypt.class.php');
        $data = array("user_admin_password" => EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $new_password));
        $where = "user_key = ".$user_key;
        return $this->update($data, $where);
    }
    function find($user_key)
    {
        if (!$user_key) {
            return false;
        }
        $where = 'user_key='.mysql_real_escape_string($user_key);

        $res = $this->select($where,$sort = array() , $limit = 1);
        if ($res === false || DB::isError($res)) {
            $this->logger->error($res->getQuery());
            return false;
        }
        return $res->fetchRow(DB_FETCHMODE_ASSOC);
    }

    public function update( $data, $where )
    {
        $data["user_updatetime"] = date("Y-m-d H:i:s");
        return parent::update($data, $where);
    }

}
