<?php
/**
 *【EZValidator】バリデータークラス
 *
 * <b>説明</b><br />
 *
 * <code>
 * </code>
 *
 * @access     public
 * @author     Kiyomizu Hiroyuki
 * @package    EZLib
 * @subpackage EZHTML
 * @version    $Revision
 */
class EZValidator
{

    /**
     * データリソース
     *
     * @access public
     * @var    array
     */
    var $_resource = array();

    /**
     * エラースタック
     *
     * @access public
     * @var    array
     */
    var $_errstack = array();

    /**
     * コンストラクタ
     *
     * @access public
     * @param  array  $resource データリソース
     * @return
     */
    function __construct($resource)
    {
        //set resource
        $this->_setResource($resource);
        return true;
    }

    /**
     * データリソース取得
     *
     * @access private
     * @return array
     */
    function _getResource()
    {
        return $this->_resource;
    }

    /**
     * データリソース設定
     *
     * @access private
     * @param  array   $resource データリソース
     * @return boolean
     */
    function _setResource(&$resource)
    {
        $this->_resource = $resource;
    }

    /**
     * 半角英数字 + 記号チェック
     */
    public function valid_alnum($data) {
        //check
        if (preg_match("/^[0-9a-zA-Z_-]+$/", $data)) {
            return true;
        }
        return false;
    }

    /**
     * 英字チェック
     */
    public function valid_alphabet($data) {
        //check
        if (preg_match("/^[a-zA-Z]+$/", $data)) {
            return true;
        }
        return false;
    }

    /**
     * emailチェック
     */
    public function valid_email($data, $attr = null) {
        $strict = true;
//        $regex = $strict ? '/^([.0-9a-z_+-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,})$/i' : '/^([*+!.&#$|\'\\%\/0-9a-z^_`{}=?~:-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,})$/i';
        $regex = '/^([*+!.&#$|\'\\%\/0-9a-z^_`{}=?~:-]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i';
        if (preg_match($regex, trim($data), $matches)) {
            return array($matches[1], $matches[2]);
        } else {
            return false;
        }
    }

    /**
     * V-CUBE ID のチェック(メンバー登録時に使用する)
     */
    public function valid_vcube_id($data){
    	$regex = '/^([*+!.&#$|\'\\%\/0-9a-z^_`{}=?~:-]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i';
    	if (preg_match($regex, trim($data), $matches)) {
    		return array($matches[1], $matches[2]);
    	} else {
    		return false;
    	}
    }

    /**
     * 整数チェック
     */
    public function valid_integer($data) {
        if (is_numeric($data)) {
            $data = $data * 1;
        }
        //check
        if (is_integer($data) && $data >= 0) {
            return true;
        }
        return false;
    }

    /**
     * 最大文字数チェック
     */
    public function valid_maxlen($data, $attr) {
        //convert cast
        if (is_numeric($attr)) {
            $attr = $attr * 1;
        }
        //check argument
        if (!is_integer($attr)) {
            return false;
        }
        //check
        if (mb_strlen($data) <= $attr) {
            return true;
        }
        return false;
    }

    /**
     * 最小文字数チェック
     */
    public function valid_minlen($data, $attr) {
        //convert cast
        if (is_numeric($attr)) {
            $attr = $attr * 1;
        }
        //check argument
        if (!is_integer($attr)) {
            return false;
        }
        //check
        if (mb_strlen($data) >= $attr) {
            return true;
        }
        return false;
    }

    /**
     * 数値チェック
     */
    public function valid_numeric($data) {
        //check
        if (is_numeric($data)) {
            return true;
        }
        return false;
    }

    /**
     * 文字数範囲チェック
     */
    public function valid_range($data, $attr) {
        //check argument
        if (!is_array($attr) || !isset($attr[0]) || !isset($attr[1])) {
            return false;
        }
        //convert cast
        if (is_numeric($attr[0])) {
            $attr[0] = $attr[0] * 1;
        }
        if (is_numeric($attr[1])) {
            $attr[1] = $attr[1] * 1;
        }
        if (!is_integer($attr[0]) || !is_integer($attr[1])) {
            return false;
        }

        if ($attr[0] > $attr[1]) {
            return false;
        }
        //check
        $_size = strlen($data);
        if ($_size >= $attr[0] && $_size <= $attr[1]) {
            return true;
        }
        return false;
    }

    /**
     * 正規表現パターンチェック
     *
     * @param string $data チェックデータ
     * @param mixed $attr 正規表現(配列で複数指定可能)
     */
    public function valid_regex($data, $attr) {
        // 正規表現のパターンを複数指定可能
        if (is_array($attr)) {
            foreach($attr as $_attr) {
                //check
                if (!preg_match($_attr, $data)) {
                    return false;
                }
            }
            return true;
        } else {
            //check
            if (!preg_match($attr, $data)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 同じ値かチェック
     *
     */
    public function valid_equal($data, $attr) {
        //check
        if ($data !== $attr) {
            return false;
        }
        return true;
    }

    /**
     * 必須入力チェック
     */
    public function valid_required($data)
    {
        //check
        if (strlen($data) > 0) {
            return true;
        }
        return false;
    }

    /**
     * ファイル拡張子チェック
     */
    public function valid_file_ext($filename, $attr = array()) {
        //check
        $pos = strrpos($filename, ".");
        if ($pos !== false) {
            $extension = strtolower(substr($filename, $pos + 1));
            foreach($attr as $val) {
                if ($extension == strtolower($val)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 数値の最小値チェック
     */
    public function valid_minval($data, $attr) {
        if ($data > $attr) {
            return true;
        }
        return false;
    }

    /**
     * 数値の最大値チェック
     */
    public function valid_maxval($data, $attr) {
        if ($data < $attr) {
            return true;
        }
        return false;
    }

    /**
     * 指定値のみ入力可
     */
    public function valid_allow($data, $attr = array()) {
        foreach ($attr as $val) {
            if ($val === 0) {
                if ($data == "0") {
                    return true;
                }
            } else {
                if ($data == $val) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 指定不可な入力がされている
     */
    public function valid_deny($data, $attr = array()) {
        if (!in_array($data, $attr)) {
            return true;
        }
        return false;
    }

    /**
     * 不正な日付が入力されている
     * @param datetime $data
     */
    public function valid_datetime($data) {
        // パラメータ形式チェック
        list($date, $time) = explode(' ', $data); // 時間と日付の分離
        $date = str_replace(array('/', '.'), '-', $date);
        $sep_date = explode('-', $date);  // 日付の分離（年、月、日）
        if (count($sep_date) != 3) {
            return false;
        }
        if (!checkdate($sep_date[1], $sep_date[2], $sep_date[0])) { // 日付の整合性が取れていない
            return false;
        }
        // 時分秒チェック
        if ($time) {
            $sep_time = explode(':', $time); // 時刻の分離（時、分、秒）
            if (count($sep_time) != 3) {
                return false;
            }
            if (!is_numeric($sep_time[0])) { // 時間が数値ではない
                return false;
            }
            if (($sep_time[0] < 0) ||
                ($sep_time[0] > 23)) { // 時間が範囲外
                return false;
            }
            if (!is_numeric($sep_time[1])) { // 分が数値ではない
                return false;
            }
            if (($sep_time[1] < 0) ||
                ($sep_time[1] > 59)) { // 分が範囲外
                return false;
            }
            if (!is_numeric($sep_time[2])) { // 秒が数値ではない
                return false;
            }
            if (($sep_time[2] < 0) ||
                ($sep_time[2] > 59)) { // 秒が範囲外
                return false;
            }
        }
        // チェックOK
        return true;
    }
    /**
     * 言語を許可するチェック
     */
    public function valid_language($data, $language_list) {
    	//check the language value by key
    	if(array_key_exists ($data, $language_list))
    		return true;
    	else
    		return false;
    }



    /**
     * 入力チェック
     *
     * <code>
     * $data = array("title" => "title",
     *               "email" => "aaa@bbb,jp");
     * # チェック条件
     * $rules = array("title" => array("minlength" => 30,
     *                                 "required"  => true),
     *                "email" => array("email" => true);
     *
     * $check_obj = new EZValidator($data);
     * foreach($rules as $field => $rule) {
     *   $check_obj->check($field, $rule);
     * }
     *
     * if (EZValidator::isError($check_obj)) {
     *    print "error !";
     * } else {
     *    print "successful !";
     * }
     * </code>
     *
     * @author kiyomizu
     * @param string $element フィールド名
     * @param array $rule 連想配列でチェック条件を記述
     * @param boolean $is_all エラーをスタック true:する、false:しない
     * @return boolean エラー あり：false、なし：true
     *
     */
    function check($element, $rules, $is_all = true) {
        //get resource
        $resource = $this->_getResource();
        $value = isset($resource[$element]) ? $resource[$element] : null;
        // 最初に必須項目のチェック
        if (isset ($rules["required"])) {
            $ret = $this->valid_required($value);
            if ($ret === false) {
                $this->_errstack[$element]["required"] = false;
                return false;
            }
            // チェック済みなので
            unset($rules["required"]);
        }
        // 未入力はスルーする
        if (is_array($value)) {
            if (count($value) == 0) {
                return true;
            }
        } elseif ($value == '') {
            return true;
        }
        // その他チェック
        $result = true;
        foreach ($rules as $rule => $option) {
            $method = "valid_" . $rule;
            $ret = true;
            if (method_exists($this, $method)) {
                $ret = $this->$method($value, $option);
            }
            if ($ret === false) {
                $this->_errstack[$element][$rule] = $option;
                // エラーをスタックしない
                if ($is_all == false) {
                    return false;
                }
                $result = false;
            }
        }
        return $result;
    }

    function getError($element = null) {
        if ($element) {
            if (isset($this->_errstack[$element])) {
                return $this->_errstack[$element];
            } else {
                return null;
            }
        } else {
            return $this->_errstack;
        }
    }

    /**
     * エラー存在チェック
     *
     * @author kiyomizu
     * @param object $obj EZValidatorのインスタンス
     * @return boolean true:エラーあり、false:エラーなし
     */
    function isError($obj) {
        if (is_object($obj)) {
            if(get_class($obj) == 'EZValidator' || is_subclass_of($obj,'EZValidator')) {
                if (is_array($obj->_errstack) && count($obj->_errstack) > 0)  {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * エラーフィールド一覧を取得
     *
     * @author kiyomizu
     * @return array エラーフィールド一覧
     */
    function error_fields() {
        if (is_array($this->_errstack)) {
            $fields = array_keys($this->_errstack);
        } else {
            $fields = array();
        }
        return $fields;
    }

    /**
     * エラー属性を取得
     *
     * @author kiyomizu
     * @param string $element エラーフィールド
     * @param boolean $is_all true : エラー属性をすべて取得、false : 一つだけ
     * @return mixed
     */
    function get_error_type($element, $is_all = false) {
        if (isset($this->_errstack[$element])) {
            $types = array_keys($this->_errstack[$element]);
            if ($is_all == true) {
                return $types;
            } else {
                return $types[0];
            }
        }
    }

    /**
     * エラー条件を取得
     *
     * @author kiyomizu
     * @param string $element エラーフィールド
     * @param string $type エラータイプ（指定なし時は対象フィールドのエラーをすべて取得）
     * @return mixed
     */
    function get_error_rule($element, $type = null) {
        if (isset($this->_errstack[$element])) {
            if ($type == null) {
                return $this->_errstack[$element];
            }
            if (isset($this->_errstack[$element][$type])) {
                return $this->_errstack[$element][$type];
            }
        }
        return false;
    }

    /**
     * エラーをセット
     */
    function set_error($element, $key, $value = null) {
        $this->_errstack[$element][$key] = $value;
    }
}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
