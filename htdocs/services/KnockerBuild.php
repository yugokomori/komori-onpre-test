<?php
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_Account.class.php");
require_once("classes/N2MY_Reservation.class.php");

class AppKnocker extends AppFrame {

    function default_view() {
        $request = $this->request->getAll();
        if (mb_strlen($request["msg"]) <= 100) {
            require_once( "classes/dbi/meeting.dbi.php" );
            require_once( "classes/mgm/dbi/FmsServer.dbi.php" );
            // 直前の会議取得
            $obj_Room = new RoomTable($this->get_dsn());
            $where = "room_key = '".addslashes($request["room_key"])."'";
            $meeting_ticket = $obj_Room->getOne($where, "meeting_key");
            // 会議のステータス取得
            $obj_Meetng = new MeetingTable($this->get_dsn());
            $where = "meeting_ticket = '".addslashes($meeting_ticket)."'";
            $meeting_info = $obj_Meetng->getRow($where);
            // サーバ情報
            if ($meeting_info["intra_fms"]) {
                require_once( "classes/dbi/user_fms_server.dbi.php" );
                $obj_FmsServer = new UserFmsServerTable($this->get_dsn());
                $server_info = $obj_FmsServer->getRow(sprintf("fms_key=%d", $meeting_info["server_key"]));
            } else {
                $obj_FmsServer = new DBI_FmsServer(N2MY_MDB_DSN);
                $server_info = $obj_FmsServer->getRow( sprintf("server_key=%d", $meeting_info["server_key"] ));
            }
            $this->logger2->info(array($request["room_key"], $meeting_ticket, $request["msg"]));
            $msg = $request["msg"];
            $this->template->assign("fms",$server_info["server_address"]);
            $this->template->assign("app_name",$this->config->get("CORE", "app_name"));
            $meeting_id = $obj_Meetng->getMeetingID($meeting_info["meeting_key"]);
            $this->template->assign("meeting_key",$meeting_info["fms_path"].$meeting_id);
            $this->template->assign("message",$msg);
            $this->display("user/knocker.t.html");
        } else {
            print "0";
        }
    }
}
$main = new AppKnocker();
$main->execute();
