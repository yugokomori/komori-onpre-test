<?php 

interface IDestroyedMeeting {
	function removeMeetingRecord();
	function setMeetingRecord();
}
