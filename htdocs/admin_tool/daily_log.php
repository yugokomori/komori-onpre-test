<?php

require_once("classes/AppFrame.class.php");
require_once("classes/HolidayOfJapan.class.php");
require_once("classes/mgm/dbi/meeting_date_log.dbi.php");
require_once("classes/dbi/meeting_use_log.dbi.php");
require_once("classes/N2MY_DBI.class.php");

class AppDailyLog extends AppFrame {

    var $_core_db_dsn = "";
    var $date_log_init = "";

    function init()
    {
        // MGM DBサーバ情報取得
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        // DBサーバ情報取得
        $this->dsn = $this->_get_dsn();
        //$this->logger->info(__FUNCTION__." # Array Of Dsn Lists! ", __FILE__, __LINE__, $this->dsn);
        // 日計ログ初期化用オブジェクト初期設定
        $this->date_log_init = array(
            "log_date"                    => null,
            "user_id"                     => null,
            "staff_id"                    => null,
            "room_key"                    => null,
            "use_time"                    => 0,
            "use_time_mobile_normal"      => 0,
            "use_time_mobile_special"     => 0,
            "use_time_hispec"             => 0,
            "use_time_audience"           => 0,
            "use_time_h323"               => 0,
            "use_time_h323ins"            => 0,
            "account_time_mobile_normal"  => 0,
            "account_time_mobile_special" => 0,
            "account_time_hispec"         => 0,
            "account_time_audience"       => 0,
            "account_time_h323"           => 0,
            "account_time_h323ins"        => 0,
            "meeting_cnt"                 => 0,
            "participant_cnt"             => 0,
        );
    }

    function default_view()
    {
        $this->logger->info(__FUNCTION__." # Start ... ", __FILE__, __LINE__);

        // 初期化
        $log_data = array();

        // 登録先
        $date_log_obj = new MeetingDateLogTable(N2MY_MDB_DSN);

        // ログ集計実行日付
        $operation_date = (empty($_REQUEST['od'])? date("Y-m-d", time()): $_REQUEST['od']); // 実行日付（引数受け取り：引数が無い場合、当日）
        if (!$operation_date) { // パラメータが空
            die("param empty!");
        } else {
            $this->logger->info(__FUNCTION__." # Operation Date is ... " . $operation_date, __FILE__, __LINE__);
            print("実行日付：". $operation_date. "<br />");
        }
        // ログ集計対象日付
        $ret  = $this->_get_target_date($operation_date); //実行日の前日が対象
        if ($ret['status'] == 1) {
            $target_date  = $ret['data'];
        } else {
            die($ret['data']);
        }

        $this->logger->info(__FUNCTION__." # Target Date is ... " . $target_date, __FILE__, __LINE__);
        print("集計対象日付：". $target_date. "<br />");

        $target_date_wk = explode("-", $target_date);
        $target_year  = $target_date_wk[0];
        $target_month = $target_date_wk[1];
        $target_day   = $target_date_wk[2];
        $holiday_flg  = 0;

        // 祝日判定クラス（携帯利用時間（通常・特殊）算出用）
        // 祝日設定期限：2015年まで。その後の設定は手動で行う。
        // また、法改定で休（祝）日が増減した場合も手動で設定しなおす。
        $holiday_obj = new HolidayOfJapan();
        if ($holiday_obj->isHoliday($target_year, $target_month, $target_day)) {
            $this->logger->info(__FUNCTION__." # " . $target_date . " >> is Holiday! ", __FILE__, __LINE__);
            print(" # " . $target_date . " >> is Holiday! <br />");
            $holiday_flg  = 1;
        }else{
            $this->logger->info(__FUNCTION__." # " . $target_date . " >> is not Holiday! ", __FILE__, __LINE__);
            print(" # " . $target_date . " >> is not Holiday! <br />");
            $holiday_flg  = 0;
        }

        $this->logger->info(__FUNCTION__." # Start Daily Log adds! ", __FILE__, __LINE__);
        print(" # Start Daily Log adds! <br />");

        if (!$this->dsn) {
            $this->logger->info(__FUNCTION__." # No DSN Keys! ", __FILE__, __LINE__);
            print(" # No DSN Keys! <br />");
            die();
        }
        # -- 用意されているDBの分だけ集計処理を繰り返す
        foreach ($this->dsn as $server_key => $server_dsn) {

            // 当日利用ログ一覧
            $log_obj = new MeetingUseLogTable($server_dsn);
            $user_obj = new N2MY_DB($server_dsn, "user");
            $room_obj = new N2MY_DB($server_dsn, "room");
            $room_plan_obj = new N2MY_DB($server_dsn, "room_plan");
            $ordered_service_option_obj = new N2MY_DB($server_dsn, "ordered_service_option");

            $sql = "SELECT " .
                   " `C`.`log_date` " .
                   ",`C`.`room_key` " .
                   ",`C`.`type` " .
                   ",`C`.`meeting_key` " .
                   ",`C`.`participant_id` " .
//                   ",`C`.`create_datetime` " .
                   ",`C`.`log_no` " .
                   "FROM " .
                   "(SELECT " .
                   " '" . $target_date ."' as log_date" .
                   " ,`room_key` ";
            if ($holiday_flg == 0) {
                // ※平日の場合
                // （携帯オプションは、時間帯により通常料金と特別料金が切り分けられる）
                $sql .= " , if (`type` is not null ," .
                        "       if (`type` = 'phone', " .
                        "           if ((`create_datetime` BETWEEN '" . $target_date ." 08:00:00' AND '" . $target_date ." 19:00:00') " .
                        "               , 'phone', 'phone2') ".
                        "           ,`type`) ".
                        "       , 'normal') as type ";
            }else{
                // ※土日・祝日の場合
                // （携帯オプションは、すべて特別料金での課金となる）
                $sql .= " , if (`type` is not null " .
                        "   , if (`type` = 'phone', 'phone2', `type`) " .
                        "   , 'normal') as type ";
            }
            $sql .= " , `meeting_key` " .
                    " , if (`type` is not null , participant_id, 0) as participant_id " .
//                    " , `create_datetime` " .
                    " , `log_no` " .
                    " FROM `meeting_use_log` " .
                    " WHERE `create_datetime` like '" . $target_date ."%' ";
            $sql .= ") as C " .
                    "ORDER BY  `C`.`log_date` ,`C`.`room_key` ,`C`.`type`, `C`.`meeting_key`, `C`.`participant_id` ";
            $rs = $log_obj->_conn->query($sql);
            if (DB::isError($rs)) {
                $this->logger->info(__FUNCTION__." # DBError", __FILE__, __LINE__, $sql);
            }else{
                while ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                    $log_data[$row['log_date']][$row['room_key']][$row['type']][$row['meeting_key']][$row['participant_id']][$row['log_no']] = 1;
                }
            }

            // 利用ログの存在確認
			if (count($log_data) < 1 ) { 
                $this->logger->info(__FUNCTION__." # No Log Data! ", __FILE__, __LINE__, $log_data);
                print(" # No Log Data! <br />");
			} else { 
                //$this->logger->info(__FUNCTION__." # Log Data! ", __FILE__, __LINE__, $log_data[$target_date]);
                $this->logger->info(__FUNCTION__." # Log Counts : ".count($log_data[$target_date]), __FILE__, __LINE__);
                print(" # Log Counts : ". count($log_data[$target_date]) ." <br />");
                # -- room_keyの抽出
                foreach($log_data[$target_date] as $key => $val) {
                    // 日計ログ更新
                    $room_key_info[] = $key;
                }

                # -- room_keyからのユーザーKEY取得
                $room_sql = " SELECT `A`.`room_key` as room_key, `B`.`user_id` as user_id " .
                            " FROM `room` A, `user` B " .
                            " WHERE `A`.`room_key` IN ('" . implode("','", $room_key_info) . "') " .
                            "   AND `A`.`user_key` = `B`.`user_key` ";

                $rs = $room_obj->_conn->query($room_sql);
                $user_info = array();
                if (DB::isError($rs)) {
                    $this->logger->info(__FUNCTION__." # DBError", __FILE__, __LINE__);
                }else{
                    while ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                        $user_info[$row['room_key']] = $row['user_id'];
                    }
                }

                # -- 日計ログ登録用DBオブジェクト
                $dto_date_log = array();
                $_date_log_wk = array();
                $_date_log_wk = $this->date_log_init;
                # -- 日計ログ登録情報編集
                foreach($log_data[$target_date] as $key => $val) { // 部屋 -- for --
                    // 日計ログ更新
                    $_date_log_wk['log_date'] = $target_date;
                    $_date_log_wk['user_id'] = $user_info[$key];
                    $_date_log_wk['room_key'] = $key;
                    // 会議数
                    $_date_log_wk['meeting_cnt'] = 0;
                    // 参加者数
                    $_date_log_wk['participant_cnt'] = 0;
                    foreach($val as $_key => $_val) { // 種別 -- for --
                        $_use_time = 0;
                        $_meeting_cnt = 0;
                        $_participant_cnt = 0;
                        foreach($_val as $__key => $__val) { // 会議 -- for --
                            $_meeting_cnt += 1;
                            foreach($__val as $___key => $___val) { // 参加者 -- for --
                                $_participant_cnt += 1;
                                $_use_time = $_use_time + count($___val);
                            } // 参加者 -- for --
                        } // 会議 -- for --
                        switch ($_key) {
                            case "normal" :
                                $_date_log_wk['use_time'] = $_use_time;
                                $_date_log_wk['meeting_cnt'] = $_meeting_cnt;
                                break;
                            case "phone" :
                                $_date_log_wk['use_time_mobile_normal'] = $_use_time;
                                $_date_log_wk['account_time_mobile_normal'] = $_use_time;
                                $_date_log_wk['participant_cnt'] = $_participant_cnt;
                                break;
                            case "phone2" :
                                $_date_log_wk['use_time_mobile_special'] = $_use_time;
                                $_date_log_wk['account_time_mobile_special'] = $_use_time;
                                $_date_log_wk['participant_cnt'] = $_participant_cnt;
                                break;
                            case "hispec" :
                                $_date_log_wk['use_time_hispec'] = $_use_time;
                                $_date_log_wk['account_time_hispec'] = $_use_time;
                                $_date_log_wk['participant_cnt'] = $_participant_cnt;
                                break;
                            case "audience" :
                                $_date_log_wk['use_time_audience'] = $_use_time;
                                $_date_log_wk['account_time_audience'] = $_use_time;
                                $_date_log_wk['participant_cnt'] = $_participant_cnt;
                                break;
                            case "h323" :
                                $_date_log_wk['use_time_h323'] = $_use_time;
                                $_date_log_wk['account_time_h323'] = $_use_time;
                                $_date_log_wk['participant_cnt'] = $_participant_cnt;
                                break;
                            case "h323ins" :
                                $_date_log_wk['use_time_h323ins'] = $_use_time;
                                $_date_log_wk['account_time_h323ins'] = $_use_time;
                                $_date_log_wk['participant_cnt'] = $_participant_cnt;
                                break;
                            default :
                                break;
                        }
                    } // 種別 -- for --
                    // 既存データ確認
                    $where = " log_date = '" . addslashes($_date_log_wk["log_date"]) . "'".
                             " AND user_id = '" . addslashes($_date_log_wk["user_id"]) . "'".
                             " AND room_key = '" . addslashes($_date_log_wk["room_key"]) . "'";
                    // $count = $date_log_obj->numRows($where);
                    $daily_log_info = $date_log_obj->getRow($where, "log_no");
                    if (DB::isError($daily_log_info)) {
                        $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getMessage());
                        var_dump(array(__FUNCTION__,__FILE__,__LINE__,$ret->getMessage()));
                    }
                    $daily_log_no = $daily_log_info['log_no'];
                    if (!$daily_log_no) {
                        // 新規追加
                        $_date_log_wk['create_datetime'] = date("Y-m-d H:i:s", time());
                        $ret = $date_log_obj->add($_date_log_wk);
                        if (DB::isError($ret)) {
                            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getMessage());
                            var_dump(array(__FUNCTION__,__FILE__,__LINE__,$ret->getMessage()));
                        }
                    } else {
                        // 上書き更新
                        $where = " log_no = '" . addslashes($daily_log_no) . "'";
                        $_date_log_wk['update_datetime'] = date("Y-m-d H:i:s", time());
                        $ret = $date_log_obj->update($_date_log_wk, $where);
                        if (DB::isError($ret)) {
                            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getMessage());
                            var_dump(array(__FUNCTION__,__FILE__,__LINE__,$ret->getMessage()));
                        }
                    }
                    //
                    $dto_date_log[] = $_date_log_wk;
                    $_date_log_wk = $this->date_log_init;
                } // 部屋 -- for --
                // $this->logger->info(__FUNCTION__." # dto_date_log", __FILE__, __LINE__, $dto_date_log);
			} // 利用ログ -- if --

        } // server_list

        $this->logger->info(__FUNCTION__." # Ending Daily Log adds! ", __FILE__, __LINE__);
        print(" # Ending Daily Log adds! <br />");
        //終了後に実行完了メール送信
        $mail_subject = "【ミーティング】daily_log.php処理完了".$target_day;
        $mail_body = "【ミーティング】daily_log.php処理が完了しました。" ."\n\n" .
                "■ Date: ".$target_day."\n".
                "■ Memory usage: ".memory_get_usage()."\n".
                "■ URL: ".N2MY_BASE_URL.$_SERVER["REQUEST_URI"]."\n";
        if (defined("N2MY_CRON_FROM") && defined("N2MY_CRON_TO") && N2MY_CRON_FROM && N2MY_CRON_TO) {
            $this->sendReport(N2MY_CRON_FROM, N2MY_CRON_TO, $mail_subject, $mail_body);
        }
        $this->logger->info(__FUNCTION__." # End ... ", __FILE__, __LINE__);

    }

    function sendReport($mail_from, $mail_to, $mail_subject, $mail_body) {
        require_once("lib/EZLib/EZMail/EZSmtp.class.php");
        // 入力チェック
        if (!$mail_from || !$mail_to || !$mail_subject || !$mail_body) {
            return false;
        }
        require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
        $lang = EZLanguage::getLangCd("ja");
        $obj_SendMail = new EZSmtp(null, $lang, "UTF-8");
        $obj_SendMail->setFrom($mail_from);
        $obj_SendMail->setReturnPath($mail_from);
        $obj_SendMail->setTo($mail_to);
        $obj_SendMail->setSubject($mail_subject);
        $obj_SendMail->setBody($mail_body);
        // メール送信
        $result = $obj_SendMail->send();
        $this->logger2->info(array($mail_from, $mail_to, $mail_subject, $mail_body));
    }

    private function _get_dsn()
    {
        static $server_list;
        if (!$server_list) {
            $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        }
        if (!$server_list) {
            return null;
        } elseif (!is_array($server_list)) {
            return null;
        } else {
            if (array_key_exists("SERVER_LIST", $server_list)) {
                return $server_list["SERVER_LIST"];
            } else {
                return null;
            }
        }
    }

    /**
     * 実行時に指定されたパラメータ（実行日付）より、集計処理を行う対象日付を生成する。
     * 
     */
    private function _get_target_date($operation_date=null)
    {
        $status = 1;
        $data = null;
        if ($sep_date = explode('-', $operation_date)) {
            if (array_key_exists(0, $sep_date) !== true) { // 年が空
                $status = 0;
                $data = "param empty (year, month, day)";
                return array("status" => $status, "data" => $data);
            } else {
                if (!is_numeric($sep_date[0])) { // 年が数値ではない
                    $status = 0;
                    $data = "input year is not numeric!";
                    return array("status" => $status, "data" => $data);
                }
            }
            if (array_key_exists(1, $sep_date) !== true) { // 月が空
                $status = 0;
                $data = "param empty (month, day)";
                return array("status" => $status, "data" => $data);
            } else {
                if (!is_numeric($sep_date[1])) { // 月が数値ではない
                    $status = 0;
                    $data = "input month is not numeric!";
                    return array("status" => $status, "data" => $data);
                }
            }
            if (array_key_exists(2, $sep_date) !== true) { // 日が空
                $status = 0;
                $data = "param empty (day)";
                return array("status" => $status, "data" => $data);
            } else {
                if (!is_numeric($sep_date[2])) { // 日が数値ではない
                    $status = 0;
                    $data = "input day is not numeric!";
                    return array("status" => $status, "data" => $data);
                }
            }
            if ($sep_date[0] < 1950) { // 1901-12-15以前だと前日が作り出せない。
                $status = 0;
                $data = "input year is too small!";
                return array("status" => $status, "data" => $data);
            }
            if (!checkdate($sep_date[1], $sep_date[2], $sep_date[0])) { // 日付の整合性が取れていない
                $status = 0;
                $data = "date format error!!";
                return array("status" => $status, "data" => $data);
            }
        } else { // ハイフンで区切られていない
            $status = 0;
            $data = "date format error!!";
            return array("status" => $status, "data" => $data);
        }
		$operation_date_time = mktime(0, 0, 0, $sep_date[1], $sep_date[2], $sep_date[0]);
        $data  = date("Y-m-d",strtotime("-1 day", $operation_date_time)); //実行日の前日が対象
        return array("status" => $status, "data" => $data);

    }

}

$main = new AppDailyLog();
$main->execute();
?>
