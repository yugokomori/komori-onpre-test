var ecoLogin = new Object();

function ecoLoginInit()
{
    ecoLogin = new EcoLoginClass();
};

var EcoLoginClass = Class.create();
EcoLoginClass.prototype =
{
    eco: "",
    FormClass: new FormClass(),
/*
    post: function()
    {
       var login_form = document.getElementById( "login_form" );
       login_form.submit();
    },
*/
    setOnSubmit: function()
    {
        var login_form = document.getElementById( "login_form" );
        var userId = document.getElementById( "userId" );
        var userPassword = document.getElementById( "userPassword" );

        if ( userId.value == "") {
            alert("ユーザIDの入力がありません");
            userId.focus();
            return false;
        }
        if ( userPassword.value == "") {
            alert("パスワードの入力がありません");
            userPassword.focus();
            return false;
        }
        if( searchFlg ){
            eco.search( true );
            return false;
        }
        this.FormClass.excute();
    },

    setFormEvent: function()
    {
        var form = document.getElementById( "login_form" );
        if (form) {
            form.onsubmit= null;
            form.onsubmit = function(){ EcoLoginClass.prototype.setOnSubmit();return false;};
        }
    },

    initialize: function()
    {
        this.setFormEvent();
        //this.eco = new EcoClass();
    }
}