#! /bin/sh

HOSTNAME="`hostname`"
LOGFILE="/var/log/${HOSTNAME}.log"
SCRIPT_NAME="`basename $0`"
RC=0

MONIT_LSYNCD_CONF="/etc/monit.d/lsyncd.rc"

LOG()
{
    STATUS=$1
    MSG=$2
    echo [`date '+%Y/%m/%d %T'`] [${STATUS}] "${HOSTNAME}" "${SCRIPT_NAME}" "${MSG}" >> ${LOGFILE}
}

if [ -f ${MONIT_LSYNCD_CONF} ]; then
    \/bin/mv ${MONIT_LSYNCD_CONF} ${MONIT_LSYNCD_CONF}.stop
    /sbin/service monit  stop
    /sbin/service lsyncd stop
fi

if [ -f /var/run/lsyncd.pid ]; then
    LOG "err" "\"lsyncd stop failed.\""
    RC=1
fi

exit ${RC}