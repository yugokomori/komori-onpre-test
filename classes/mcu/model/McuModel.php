<?php

class McuModel {
	private $content;
	public function __construct($obj) {
		$this->content= array(
					"status"=>			$obj["status"],
					"contract"			=>$obj["contract"],
					"sipServer"			=>$obj["sipServer"],
					"sipServerDomain"	=>$obj["sipServerDomain"],
					"sipProxyIp"		=>$obj["sipProxyIp"],
					"sipProxyPort"		=>$obj["sipProxyPort"],
					"socketServer"		=>$obj["socketServer"],
					"socketServerPort"	=>$obj["socketServerPort"],
					"mcuAuth"=>array(
							"default"=>array(
									"id"	=>$obj["defaultId"],
									"pw"	=>$obj["defaultPw"],
									"viewId"=>$obj["defaultViewId"],
									"viewPw"=>$obj["defaultViewPw"]),
							"small"=>array(
									"id"	=>$obj["smallId"],
									"pw"	=>$obj["smallPw"],
									"viewId"=>$obj["smallViewId"],
									"viewPw"=>$obj["smallViewPw"]),
							"telephone"=>array(
									"id"	=>$obj["telephoneId"],
									"pw"	=>$obj["telephonePw"])
					),
					"did"	=>$obj["did"]
				);
	}
	
	public function getContent() {
		return $this->content;
	}
}
