<?php 
require_once(dirname(__FILE__)."/../params/WSDLParams.php");
class GetDocSharingStatusRequestParams extends WSDLParams {
	protected $confId;
	public function __construct($obj) {
		parent::__construct($this, $obj);
	}
	
	public function getParams() {
		return parent::getParams($this);
	}
}
