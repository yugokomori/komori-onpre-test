<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
set_time_limit(0);
ini_set("memory_limit","2048M");

require_once ('classes/AppFrame.class.php');
require_once ('classes/mgm/dbi/user.dbi.php');
require_once("classes/N2MY_Account.class.php");
require_once ('classes/dbi/user.dbi.php');
require_once ('config/config.inc.php');
require_once ('lib/EZLib/EZUtil/EZEncrypt.class.php');
require_once( "lib/EZLib/EZCore/EZLogger2.class.php" );
require_once ('lib/EZLib/EZUtil/EZCsv.class.php');
require_once ('classes/dbi/whitecompanylist.dbi.php');

class AppMember extends AppFrame
{
    var $logger = null;
    var $_name_space = null;
    var $companyTable = null;
    var $statusTable = null;
    var $max_address_cnt = null;

    function init() {
        $this->companyTable = new white_company_listTable($this->get_dsn());
        $this->_name_space = md5(__FILE__);
        $this->max_address_cnt  = 50000;
    }

    /**
     * ログイン認証
     */
    function auth() {
        $this->checkAuth();
        $this->checkAdminAuth('admin/session_error.t.html');
    }
    /**
     * デフォルト処理
     */
    function default_view() {
        $this->logger->trace("test", __FILE__, __LINE__, __FUNCTION__);
        $this->action_show_top();
    }

    function action_show_top()
    {
        $this->display('admin/member/index.t.html');
    }

    function action_admin_company($message = ""){
        $this->session->remove("conditions");
        $this->render_admin_company();
    }

    function action_search_member() {
        $this->setCond();
        $this->render_admin_company();
    }

    function action_pager() {
        $this->render_admin_company();
    }

    function render_admin_company($message = ""){

        if($message){
            $this->template->assign('message', $message);
        }
        //user_key取得
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];

        //user_keyからメンバーを取得
        $conditions = $this->getCond();
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$conditions);
        $sort_key  = ($conditions["sort_key"]) ? $conditions["sort_key"] : "company_id";
        $sort_type = ($conditions["sort_type"]) ? $conditions["sort_type"] : "asc";
        $sort_key_rules = array("company_name","company_id","classification","accounting_unit_code","expense_department_code");
        if (!EZValidator::valid_allow($sort_key, $sort_key_rules)) {
            $sort_key = "company_name";
        }
        $sort_type_rules = array("asc","desc");
        if (!EZValidator::valid_allow($sort_type, $sort_type_rules)) {
            $sort_type = "desc";
        }
        $limit = $this->request->get("page_cnt");
        if (!$limit) {
            $limit = 10;
        }
        $page      = $this->request->get("page", 1);
        $offset    = ($page - 1) * $limit;
        // 条件

        $where = "user_key = ".addslashes($user_key).
            " AND white_company_list_status > -1";
            if ($conditions["classification"] != null ){
                 $where .= " AND classification = '".addslashes($conditions["classification"])."'";
            }
        if ($conditions["company_name"]) {
            $where .= " AND (company_name like '%".addslashes($conditions["company_name"])."%'";
            $where .= " OR company_id like '%".addslashes($conditions["company_name"])."%'";
            $where .= " OR expense_department_code like '%".addslashes($conditions["company_name"])."%'";
            $where .= " OR accounting_unit_code like '%".addslashes($conditions["company_name"])."%')";
        }
        $company_list = $this->companyTable->getRowsAssoc($where, array($sort_key => $sort_type), $limit, $offset);
        $count = $this->companyTable->numRows($where);
        $company_info = array();
        foreach( $company_list as $company ){
            $company_info[] = array(
                            "status"        => $this->_getCompanyStatusName( $company['white_company_list_status'] ),
                            "key"           => $company['white_company_list_key'],
                            "id"            => $company['company_id'],
                            "expense_department_code" => $company['expense_department_code'],
                            "accounting_unit_code" => $company['accounting_unit_code'],
                            "name"          => $company['company_name'],
                            "classification"          => $company['classification']
                            );
        }
        $add_flg = ( ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") &&
            $user_info["max_company_count"] <= count( $company_info ) ) ? false : true ;
                $this->template->assign("add_flg", $add_flg );
        $this->template->assign('info', $company_info);
        $this->template->assign('vcubeid_add_use_flg', $user_info["vcubeid_add_use_flg"]);
        // ページング
        $this->template->assign('conditions', $conditions);
        $pager = $this->setPager($limit, $page, $count);
        $this->template->assign('pager', $pager);
        $this->display('admin/member/companylist/index.t.html');
    }

    function setCond() {
        $conditions  = $this->request->get("conditions");
        if ($conditions) {
            $this->session->set("conditions", $conditions);
        }
    }

    function getCond() {
        $condition = $this->session->get("conditions");
        return $condition;
    }

    function action_add_company($message = "")
    {
        $company_info = ( 1 == $this->request->get( "action_add_company") ) ? "" : $this->session->get('new_company_info');
        //user_key取得
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];

        //エラー時にユーザインプットの再表示
        if( $company_info ){
            foreach($company_info as $key => $value){
                $this->template->assign($key, $value);
            }
        }
        if($message){
            $this->template->assign('message', "$message");
        }

        $this->template->assign('user_info', $user_info);
        //$this->template->assign('member_group', $member_group);
        //$this->template->assign('member_status', $member_status);
        $this->display('admin/member/companylist/add/index.t.html');
    }

    function action_add_company_confirm()
    {
        $this->set_submit_key($this->_name_space);
        // 入力値を保持
        $request = $this->request->getAll();
        $this->session->set('new_company_info', $request);
        // user_key取得
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];

        $message = null;
        $message = $this->check_company_info($request, $user_key);
        if( $message ){
            $this->action_add_company($message);
        } else {
            foreach($request as $key => $value){
                if($key == "member_status_key"){
                    $this->template->assign('member_status_name', $this->_getMemberStatusName($value));
                }
                $this->template->assign($key, $value);
            }
            $this->display('admin/member/companylist/add/confirm.t.html');
        }
    }
    public function action_add_company_csv() {
        $this->template->assign('max_address_cnt',  $this->max_address_cnt);
        $this->display('admin/member/companylist/csv_index.t.html');
    }

    function action_format_download() {
        $output_encoding = $this->get_output_encoding();
        $company_csv = new EZCsv(true,$output_encoding,"UTF-8");
        $dir = $this->get_work_dir();
        $tmpfile = tempnam($dir, "csv_");
        $company_csv->open($tmpfile, "w");

        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="sample_whitecode.csv"');

        // ヘッダ
        $header = array("key","company_code","company_name","classification","accounting_unit_code","expense_department_code","flag","error");
        $sample = array("1","123","サンプル会社","0","1qa","2ws","A","This is a sample data. Please delete.");
        $this->logger2->debug(__FUNCTION__, __FILE__, __LINE__, $header);
        $company_csv->setHeader($header);
        $company_csv->write($header);
        $company_csv->write($sample);
        $company_csv->close();
        $fp = fopen($tmpfile, "r");
        $contents = fread($fp, filesize($tmpfile));
        fclose($fp);
        print $contents;
        unlink($tmpfile);
    }

    public function action_import_company_csv() {
        $request["upload_file"] = $_FILES["import_file"];

        $data = pathinfo($request["upload_file"]["name"]);
        //CSVファイルチェック
        if (empty($data['filename'])) {
            $message .= CSV_NO_FILE_ERROR;
            return $this->action_import_complete($message);
        } elseif($data['extension'] != "csv") {
            $message .= CSV_FILE_FORMAT_ERROR ;
            return $this->action_import_complete($message);
        }
        $filename = $this->get_work_dir()."/".tmpfile();
        move_uploaded_file($request["upload_file"]["tmp_name"], $filename);
        $request["upload_file"]["tmp_name"] = $filename;
        $output_encoding = $this->get_output_encoding();
        $company_csv = new EZCsv(true,$output_encoding,"UTF-8");
        $company_csv->open($filename, "r");
        //取り込んだファイルのヘッダチェック
        $head_ex = "key,company_code,company_name,classification,accounting_unit_code,expense_department_code,flag,error";
        $h_ar = implode(",", $company_csv->getHeader());
        if($head_ex !== $h_ar) {
            $message .= CSV_FILE_FORMAT_ERROR;
            return $this->action_import_complete($message);
        }
        $rows = array();
        $count = 0;
        while($row = $company_csv->getNext(true)) {
//             $row["lang"] = $row["lang(ja.en)"];
//             unset($row["lang(ja.en)"]);
//             if($row["timezone"] == "")
//               $row["timezone"] = "100";
            $rows[] = $row;
            $count++;
        }
        $company_csv->close();
        unlink($filename);
        //記録メンバー数チェック
        if(empty($rows)) {
            $message .= CSV_NO_ITEM_ERROR;
            return $this->action_import_complete($message);
        } elseif($count > $this->max_address_cnt) {
            $message .= CSV_LIMIT_ERROR;
            return $this->action_import_complete($message);
        }

        //追加・編集・削除処理
        $user_info = $this->session->get( "user_info" );
        $result_array = array();
        $error_flg = 0;
        foreach($rows as $company_row) {

            $white_company_list_key = $company_row["key"];
            //$member_row["storage_flg"] = "Y";
            switch($company_row["flag"]) {
                case "X":
                    break;
                case "A":
                    if(!$white_company_list_key){
                        //追加こちから
                        if ($error_message = $this->_iscompanyInfo($company_row, $user_info['user_key'], "modify")){
                            //入力情報をバリデーションエラー
                            $error_msg = "";
                            foreach($error_message as $msg) {
                            $error_msg .= $msg." ";
                            }
                            $company_row[error] = $error_msg;
                            $error_flg = 1;
                        }elseif($this->_isExist($company_row["company_code"], $company_row['accounting_unit_code'], $company_row['expense_department_code'], $user_info["user_key"])) {
                            // ホワイトコード登録済み
                            $company_row[error] = COMPANY_ERROR_DUPLICATE;
                            $error_flg = 1;
                        } elseif ($user_info["max_company_count"] != 0 && $this->_getcompanyCount($user_info["user_key"]) >= $user_info["max_company_count"]) {
                            //契約上限値越えた
                            $company_row[error] = MEMBER_ERROR_OVER_COUNT;
                            $error_flg = 1;
                        } else {
                            //追加処理
                            $add_data = array(
                                "user_key"           => $user_info["user_key"],
                                "company_id"          => $company_row['company_code'],
                                "classification"        => $company_row['classification'],
                                "accounting_unit_code"    => $company_row['accounting_unit_code'],
                                "expense_department_code" => $company_row['expense_department_code'],
                                "company_name"        => $company_row['company_name'],
                                "white_company_list_status"      => "0",
                            );
                            $this->logger2->info($add_data);
                            $ret = $this->companyTable->add( $add_data );
                            if (DB::isError($ret)) {
                                $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
                            } else {
                                $this->logger->info(__FUNCTION__."#add companylist successful!",__FILE__,__LINE__,$temp);
                            }
                        }
                     } else {
                        // 編集こちから
                        // 会社名だけの変更は処理を実施するためにチェック
                        $companyCheck = $this->companyTable->getRow ( sprintf ( "user_key='%s' AND white_company_list_status = 0 AND white_company_list_key !='%s' AND company_id='%s' AND  accounting_unit_code='%s' AND expense_department_code='%s'",
                                                                    $user_info ["user_key"], $request ['white_company_list_key'], $request ['company_id'], $request ['accounting_unit_code'], $request ['expense_department_code'] ) );

                         if(!$this->_isCompanyExist($white_company_list_key, $user_info["user_key"])) {
                            $company_row[error] = COMPANY_ERROR_KEY_FIND;
                            $error_flg = 1;

                        } elseif ($companyCheck !== null){
                            // ホワイトコード登録済み
                            $company_row [error] = COMPANY_ERROR_DUPLICATE;
                            $error_flg = 1;
                        } else {
                            if ($error_message = $this->_iscompanyInfo ( $company_row, $user_info ['user_key'], "modify" )) {
                                // 入力情報をバリデーションエラー
                                $error_msg = "";
                                foreach ( $error_message as $msg ) {
                                    $error_msg .= $msg . " ";
                                }
                                $company_row [error] = $error_msg;
                                $error_flg = 1;
                            } else {
                            //更新処理
                            $edit_data = array(
                                "company_id"   => $company_row['company_code'],
                                "company_name" => $company_row['company_name'],
                                "classification"          => $company_row['classification'],
                                "expense_department_code" => $company_row['expense_department_code'],
                                "accounting_unit_code"    => $company_row['accounting_unit_code'],
                                "white_company_list_status"      => "0",
                            );
                            $where =  sprintf( "white_company_list_key='%s'", $white_company_list_key );

                            $this->logger2->info($edit_data);
                            $result = $this->companyTable->update($edit_data, $where);
                            }
                        }
                        if (DB::isError($result)) {
                            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$result->getUserInfo());
                        } else {
                            $this->logger->info(__FUNCTION__."#edit companylist successful!",__FILE__,__LINE__,$auth_data);
                        }
                    }
                    break;
                case "D":
                    //削除こちから
//                    $white_company_list_key = $company_row["key"];
                    if(!$white_company_list_key) {
                        //メンバーIDが使っていません
                        $company_row[error] = COMPANY_ERROR_KEY_FIND;
                        $error_flg = 1;
                    } elseif (!$this->_isCompanyExist($white_company_list_key, $user_info["user_key"])) {
                        $company_row[error] = COMPANY_ERROR_KEY_FIND;
                        $error_flg = 1;
                    } else {
                        //削除処理
                        $delete_data['white_company_list_status']=-1;
                        $ret = $this->companyTable->update($delete_data, sprintf( "white_company_list_key = '%s'", $white_company_list_key ) );
                        if (DB::isError($ret)) {
                            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
                        } else {
                            $this->logger->info(__FUNCTION__."#delete companylist successful!",__FILE__,__LINE__,array(
                                "white_company_list_key" => $white_company_list_key)
                            );
                        }
                    }
                    break;
                default:
                    $company_row[error] = MEMBER_ERROR_CSV_FLAG;
                    $error_row[] = $company_row;
                    $error_flg = 1;
                    break;
            }
            $result_array[] = $company_row;
        }
        $this->logger2->info($result_array);
        if($error_flg) {
            $message = DATA_REGISTERED_ERROR;
        }
        return $this->action_import_complete($message, $result_array,$error_flg);
    }

    public function action_import_complete($message="",$result_array = "",$error_flg = 0) {
        if ($message) {
            $this->template->assign('message', $message);
        }
        if ($error_flg) {
            $this->session->set("result_array",$result_array);
            $this->template->assign('is_error', true);
        }
        $this->display('admin/member/companylist/csv_done.t.html');
    }

    public function action_export_company_csv() {
        $user_info = $this->session->get("user_info");
        $user_key = $user_info["user_key"];

        // CSV出力
        $output_encoding = $this->get_output_encoding();
        $company_csv = new EZCsv(true,$output_encoding,"UTF-8");
        $dir = $this->get_work_dir();
        $tmpfile = tempnam($dir, "csv_");
        $company_csv->open($tmpfile, "w");
        header('Content-Type: application/octet-stream;');
        header('Content-Disposition: attachment; filename="whitecode_'.date("Ymd").'.csv"');
        $header = array(
            "white_company_list_key"  => "key",
            "company_id"   => "company_code",
            "company_name" => "company_name",
            "classification" => "classification",
            "accounting_unit_code"    => "accounting_unit_code",
            "expense_department_code" => "expense_department_code",
            "flag"        => "flag",
            "error"       => "error",
        );
        $company_csv->setHeader($header);
        $company_csv->write($header);
        $where =  sprintf( "user_key='%s' and white_company_list_status ='0'", $user_key );
        $_list = $this->companyTable->getRowsAssoc($where);
        foreach($_list as $_key => $row) {
            $line = array();
            $line["white_company_list_key"]  = $row["white_company_list_key"];
            $line["company_id"]              = $row["company_id"];
            $line["company_name"]            = $row["company_name"];
            $line["classification"]          = $row["classification"];
            $line["accounting_unit_code"]    = $row["accounting_unit_code"];
            $line["expense_department_code"] = $row["expense_department_code"];
            $line["flag"]  = "X";
            $line["error"] = "";

            $this->logger2->info($line);
            $company_csv->write($line);
        }

        // CSV出力
        $company_csv->close();
        $fp = fopen($tmpfile, "r");
        $contents = fread($fp, filesize($tmpfile));
        fclose($fp);

        print $contents;
        unlink($tmpfile);
    }

    public function action_err_csv_download() {
        $rows = $this->session->get("result_array");
        if(!empty($rows)) {
            // エラーの出力
            $output_encoding = $this->get_output_encoding();
            $company_csv = new EZCsv(true,$output_encoding,"UTF-8");
            $dirName = "/member";
            $dir = $this->get_work_dir().$dirName;
            if( !is_dir( $dir ) ){
                mkdir( $dir );
                chmod( $dir ,0777 );
            }
            $tmpfile = tempnam($dir, "csv_");
            $company_csv->open($tmpfile, "w");

            header('Content-Type: application/octet-stream;');
            header('Content-Disposition: attachment; filename="whitecode_err.csv"');

            $header = array(
                "white_company_list_key"  => "key",
                "company_id"   => "company_code",
                "company_name" => "company_name",
                "classification" => "classification",
                "accounting_unit_code"    => "accounting_unit_code",
                "expense_department_code" => "expense_department_code",
                "flag"        => "flag",
                "error"       => "error",
            );
            $company_csv->setHeader($header);
            $company_csv->write($header);

            foreach($rows as $a_key => $row) {
                $err_list["white_company_list_key"]  = $row["key"];
                $err_list["company_id"]              = $row["company_code"];
                $err_list["company_name"]            = $row["company_name"];
                $err_list["classification"]          = $row["classification"];
                $err_list["accounting_unit_code"]    = $row["accounting_unit_code"];
                $err_list["expense_department_code"] = $row["expense_department_code"];
                $err_list["flag"]  = $row["flag"];
                $err_list["error"] = $row["error"];

                $this->logger2->info($err_list);
                $company_csv->write($err_list);
            }
            $company_csv->close();
            $fp = fopen($tmpfile, "r");
            $contents = fread($fp, filesize($tmpfile));
            fclose($fp);
            chmod($tmpfile, 0777);
            print $contents;

            unlink($tmpfile);
        }
        unset($_SESSION['tmp_file']);
    }

    private function _isCompanyExist($white_company_list_key, $user_key) {
        $where = sprintf( "user_key = '%s' AND white_company_list_key='%s' AND white_company_list_status=0",
                         $user_key, $white_company_list_key);

        $white_company_list_key = $this->companyTable->getOne($where, "white_company_list_key");
        return $white_company_list_key;
    }

    /*
     * 会社keyがないとき、会社ID,経理費単位コード、費用部課コード有効な会社があるか
     */
    private function _isExist($company_id,$accounting_unit_code ,$expense_department_code,$user_key) {
        $where = sprintf( "user_key = '%s' AND company_id='%s' AND accounting_unit_code='%s' AND expense_department_code='%s' AND white_company_list_status=0",
                $user_key, $company_id, $accounting_unit_code, $expense_department_code);

        $white_company_list_key = $this->companyTable->getOne($where, "white_company_list_key");
        return $white_company_list_key;
    }


    private function _getCompanyCount($user_key) {
        $where = sprintf( "user_key = '%s' AND white_company_list_status >='0'", $user_key );
        $member_count = $this->companyTable->numRows($where);
        return $member_count;
    }

    private function _iscompanyInfo($request, $user_key, $type = null) {
        $message = array();
        /*
         * csvバリデーション
         */

        //ホワイト会社ID
        $idVali = $this->idValidationCheck(
                $request['company_code'],
                $request['key'],
                $request['accounting_unit_code'],
                $request['expense_department_code'],
                $user_key
        );
        if ($idVali) {
            $message[] = $idVali;
        }

        // 会社名
        $nameVali = $this->nameValidationCheck($request['company_name']);
        if ($nameVali) {
            $message[] = $nameVali ;
        }

        // 内外フラグ
        $classificationVali = $this->classificationValidationCheck($request['classification']);
        if ($classificationVali) {
            $message[] = $classificationVali ;
        }

        if (mb_strlen ( $request ['accounting_unit_code'] ) > 0) {
            // 経理単位コード
            $accVali = $this->accValidationCheck ( $request ['accounting_unit_code'] );
            if ($accVali) {
                $message[] = $accVali ;
            }

            // 費用部課コード
            $expVali = $this->expValidationCheck ( $request ['expense_department_code'] );
            if ($expVali) {
                $message[] = $expVali;
            }
        } else {
            // 経理単位コード未入力 かつ 費用部課コード入力の場合は登録不可
            $notAccVali = $this->notAccValidationCheck ( $request ['expense_department_code'] );
            if ($notAccVali) {
                $message[] = $notAccVali;
            }
        }

        return $message;
    }

    //company_status_name からそのcompany_status_nameの取得
    private function _getCompanyStatusName($key){
        if ($key == 0) {
            return false;
        } else {
            $obj_MemberStatus = new MemberStatusTable( $this->get_auth_dsn() );
            $ret = $obj_MemberStatus->select('member_status_key = '.$key);
            $status = $ret->fetchRow(DB_FETCHMODE_ASSOC);
            return $status['member_status_name'];
        }
    }

    /**
     * ホワイト会社IDと経理単位コード、費用部課コードが同じものがないかチェック
     */
    private function _isExistCode($company_id, $accounting, $expaense, $user_key){
        if ( $this->companyTable->numRows( sprintf("user_key = '%s' AND white_company_list_status = 0 AND company_id='%s' AND accounting_unit_code='%s' AND expense_department_code='%s'"  , $user_key,$company_id, $accounting, $expaense ) )  > 0 ){
            return true;
        }
        return false;
    }

    private function check_company_info( $request, $user_key ){
        $message = null;

        /*
         * 新規追加 バリデーション
         */

        //ホワイト会社ID
        $idVali = $this->idValidationCheck(
                $request['company_id'],
                $request['white_company_list_key'],
                $request['accounting_unit_code'],
                $request['expense_department_code'],
                $user_key
        );
        if ($idVali) {
            $message .= '<li>'. $idVali . '</li>';
        }

        // 会社名
        $nameVali = $this->nameValidationCheck($request['company_name']);
        if ($nameVali) {
            $message .= '<li>'. $nameVali . '</li>';
        }

        // 内外フラグ
        $classificationVali = $this->classificationValidationCheck($request['classification']);
        if ($classificationVali) {
            $message .= '<li>'. $classificationVali . '</li>';
        }

        if (mb_strlen ( $request ['accounting_unit_code'] ) > 0) {
            // 経理単位コード
            $accVali = $this->accValidationCheck ( $request ['accounting_unit_code'] );
            if ($accVali) {
                $message .= '<li>' . $accVali . '</li>';
            }

            // 費用部課コード
            $expVali = $this->expValidationCheck ( $request ['expense_department_code'] );
            if ($expVali) {
                $message .= '<li>' . $expVali . '</li>';
            }
        } else {
            // 経理単位コード未入力 かつ 費用部課コード入力の場合は登録不可
            $notAccVali = $this->notAccValidationCheck ( $request ['expense_department_code'] );
            if ($notAccVali) {
                $message .= '<li>' . $notAccVali . '</li>';
            }
        }
        return $message;
    }

    /**
     * member_id と member_keyからそのIDがそのメンバーのものかチェック
     */
    private function _isExist_edit( $id, $key, $auth_user_key ){
        //member  と　user のテーブルをチェック
        if( $auth_info > 0 ){
            $this->logger2->info(array( $id, $key, $auth_user_key ));
            return false;
        }
        return true;
            /*
        $info = $this->companyTable->getRow( sprintf( "member_key='%s'", $key ) );
        $this->logger2->info($info);
        if ( $obj_UserTable->numRows( sprintf( "user_id='%s'", $id  ) ) > 0 ||
             $this->companyTable->numRows( sprintf( "member_id='%s'", $id ) ) > 1) {
            return true;
        } elseif ( $this->companyTable->numRows( sprintf( "member_id='%s'", $id ) ) == 1 &&
                   $id != $info['member_id']) {
            return true;
        } else {
            return false;
        }
        */
    }

    /**
     * ＤＢにメンバー情報の登録
     */
    function action_add_company_complete()
    {
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            header( "Location: ?action_add_company=1" );
            exit;
        }

        $user_info = $this->session->get( "user_info" );
        $request = $this->session->get('new_company_info');
        $user_key = $user_info['user_key'];

        $temp = array(
            "user_key"                => $user_key,
            "company_id"              => $request['company_id'],
            "company_name"            => $request['company_name'],
            "classification"          => $request['classification'],
            "accounting_unit_code"    => $request['accounting_unit_code'],
            "expense_department_code" => $request['expense_department_code'],
            "white_company_list_status"      => (($request['member_status_key']) ? $request['member_status_key'] : 0),
        );
        $ret = $this->companyTable->add( $temp );
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
        } else {
            $this->logger->info(__FUNCTION__."#add companylist successful!",__FILE__,__LINE__,$temp);
        }

        $this->logger->info(__FUNCTION__."#companylist add",__FILE__,__LINE__,$temp);
        $this->session->remove('new_company_info');
        // 操作ログ
        $this->add_operation_log('white_company_list_add', array('company_id' => $request['company_id']));
        $this->display('admin/member/companylist/add/done.t.html');
    }

    function action_edit_company($message = ""){
        if($message){
            $this->template->assign('message', $message);
        }
        //user_key取得
        $user_info = $this->session->get('user_info');
        $user_key = $user_info['user_key'];

        if($this->request->get('mode') == "edit_company"){
            //white_company_list_keyからメンバーを取得
            $key = $this->request->get('key');
            $info = $this->companyTable->getRow( sprintf( "user_key='%s' AND white_company_list_key='%s'", $user_key , $key ) );
            // 不正アクセス対策
            if(!$info){
                $this->logger2->warn(__FUNCTION__, __FILE__, __LINE__,$user_info["user_key"]);
                return $this->action_admin_company();
            }

            $company_info['white_company_list_key'] = $key;
            $company_info['company_id'] = $info['company_id'];
            $company_info['company_name'] = $info['company_name'];
            $company_info['classification'] = $info['classification'];
            $company_info['accounting_unit_code'] = $info['accounting_unit_code'];
            $company_info['expense_department_code'] = $info['expense_department_code'];
        } else {
            $company_info = $this->session->get('m_company_edit_info');
        }
        foreach($company_info as $key => $value){
            $this->template->assign($key, $value);
        }
        $this->display('admin/member/companylist/edit/index.t.html');
    }

    function action_edit_company_confirm(){
        // 入力値を保持
        $request = $this->request->getAll();
        $this->session->set('m_company_edit_info', $request);
        $user_info = $this->session->get("user_info");

        $white_company_list_key = $this->request->get('white_company_list_key');
        $message = null;

        /*
         * 個別編集 バリデーション
         */

        //ホワイト会社ID
        $idVali = $this->idValidationCheck(
                  $request['company_id'],
                  $white_company_list_key,
                  $request['accounting_unit_code'],
                  $request['expense_department_code'],
                  $user_info['user_key']
        );
        if ($idVali) {
            $message .= '<li>'. $idVali . '</li>';
        }

        // 会社名
        $nameVali = $this->nameValidationCheck($request['company_name']);
        if ($nameVali) {
            $message .= '<li>'. $nameVali . '</li>';
        }

        // 内外フラグ
        $classificationVali = $this->classificationValidationCheck( $request['classification']);
        if ($classificationVali) {
            $message .= '<li>'. $classificationVali . '</li>';
        }

        if (mb_strlen ( $request ['accounting_unit_code'] ) > 0) {
            // 経理単位コード
            $accVali = $this->accValidationCheck ( $request ['accounting_unit_code'] );
            if ($accVali) {
                $message .= '<li>' . $accVali . '</li>';
            }

            // 費用部課コード
            $expVali = $this->expValidationCheck ( $request ['expense_department_code'] );
            if ($expVali) {
                $message .= '<li>' . $expVali . '</li>';
            }
        } else {
            // 経理単位コード未入力 かつ 費用部課コード入力の場合は登録不可
            $notAccVali = $this->notAccValidationCheck ( $request ['expense_department_code'] );
            if ($notAccVali) {
                $message .= '<li>' . $notAccVali . '</li>';
            }
        }

        if ($message) {
            $this->action_edit_company($message);
        } else {
            foreach($request as $key => $value){
                if($key == "member_status_key"){
                    $this->template->assign('member_status_name', $this->_getMemberStatusName($value));
                }
                $this->template->assign($key, $value);
            }
            $this->display('admin/member/companylist/edit/confirm.t.html');
        }
    }

    function action_edit_company_complete()
    {
        $request = $this->session->get('m_company_edit_info');

        $temp = array(
            "white_company_list_key"          => $request['white_company_list_key'],
            "company_id"          => $request['company_id'],
            "company_name"        => $request['company_name'],
            "classification" => $request['classification'],
            "accounting_unit_code"    => $request['accounting_unit_code'],
            "expense_department_code" => $request['expense_department_code'],
            "white_company_list_status"      => (($request['white_company_list_status']) ? $request['white_company_list_status'] : 0)
        );

        $user_info = $this->session->get('user_info');

        // 更新処理
        $ret = $this->companyTable->update($temp, sprintf( "white_company_list_key='%s'", $request['white_company_list_key'] ) );

        // エラー処理
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
        } else {
            $this->logger->info(__FUNCTION__."#edit companylist successful!",__FILE__,__LINE__,$temp);
        }

        $this->logger->info(__FUNCTION__."#companylist_edit",__FILE__,__LINE__,$temp);
        $this->session->remove('m_company_edit_info');
        // 操作ログ
        $this->add_operation_log('company_list_update', array('white_company_list_key' => $request['white_company_list_key']));
        $this->display('admin/member/companylist/edit/done.t.html');
    }

    function action_delete_company(){
        $user_info = $this->session->get( "user_info");
        $white_company_list_key = $this->request->get('white_company_list_key');
        $companyInfo = $this->companyTable->getRow( sprintf( "user_key='%s' AND white_company_list_key='%s'",$user_info["user_key"] , $white_company_list_key ) );
        // 不正アクセス対策
        if(!$companyInfo){
            $this->logger2->warn(__FUNCTION__, __FILE__, __LINE__,$user_info["user_key"]);
            return $this->action_admin_company();
        }
        $temp['white_company_list_status']=-1;
        $ret = $this->companyTable->update($temp, sprintf( "white_company_list_key = '%s'", $white_company_list_key ) );
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
        } else {
            $this->logger->info(__FUNCTION__."#delete companylist successful!",__FILE__,__LINE__,array(
                "white_company_list_key" => $white_company_list_key));
            $data["user_id"] = $temp["company_id"];
        }
        // 操作ログ
        $this->add_operation_log('company_delete', array('company_id' => $companyInfo['company_id']));
        $message .= "<li>".WHITE_COMPANY_DELETE_USER . "</li>";
        $this->render_admin_company($message);
    }

    /*
     * バリデーションチェック
     */

    //会社ID
    function idValidationCheck
    ($company_id, $white_company_list_key, $accounting_unit_code, $expense_department_code, $user_key){

        $regex_rule = '/^[[:alnum:]]{1,9}$/';
        $valid_result = EZValidator::valid_regex($company_id, $regex_rule);
        if ($valid_result == false){
            $message = MEMBER_COMPANY_ERROR_ID_LENGTH;
        }else{
            //会社名だけの変更は処理を実施するためにチェック
            $companyCheck = $this->companyTable->numRows( sprintf( "user_key='%s' AND white_company_list_status = 0 AND white_company_list_key !='%s' AND company_id='%s' AND  accounting_unit_code='%s' AND expense_department_code='%s'",
                                                $user_key, $white_company_list_key, $company_id, $accounting_unit_code, $expense_department_code));
            if ($companyCheck > 0) {
                // 既に同レコードがあるものは重複エラー出力
                $message = COMPANY_ERROR_DUPLICATE;
            }
        }
        return $message;
    }

    // 会社名
    function nameValidationCheck($name){

        if (! $name ) {
            $message = MEMBER_COMPANY_NAME ;
        } elseif (mb_strlen ( $name ) > 40) {
            $message = MEMBER_COMPANY_ERROR_NAME_LENGTH ;
        }
        return $message;
    }

    // 内外フラグ
    function classificationValidationCheck($flg){

        if ($flg === "1" || $flg === "0" || $flg === "-1") {
            // 正常
        } else {
            $message = MEMBER_CLASSIFICATION_ERROR ;
        }
        return $message;
    }

    function accValidationCheck($accounting_unit_code) {
        $regex_rule = '/^[[:alnum:]]{1,8}$/';
        $valid_result = EZValidator::valid_regex ( $accounting_unit_code, $regex_rule );
        if ($valid_result == false) {
            $message = ACCOUNTING_ERROR_ID_LENGTH;
        }
        return $message;
    }

    function expValidationCheck($expense_department_code) {
        // 費用部課コード
        if ($expense_department_code) {
            $regex_rule = '/^[[:alnum:]]{1,5}$/';
            $valid_result = EZValidator::valid_regex ( $expense_department_code, $regex_rule );
            if ($valid_result == false) {
                $message = DEPARTMENT_ERROR_ID_LENGTH;
            }
        }
        return $message;
    }

    function notAccValidationCheck($expense_department_code) {
        // 経理単位コード未登録 かつ 費用部課コードが存在する場合登録不可
        if (mb_strlen ( $expense_department_code ) > 0 ) {
    	    $message = DEPARTMENT_ERROR_ONLY_REGISTERED;
        }
      return $message;
    }

    function showAdminLogin($message = ""){

        if ($message) {
            $this->template->assign('message', $message);
        }
        $this->display('user/admin_login.tmpl');
    }

    function showAdminTop(){
        $main = new Main();
        if ($message = $main->checkAdminLoginInfo()) {
            showAdminLogin($message);
            exit;
        }
        header("Location: /service/admin.php");
    }

    function action_white_company_list($message = "")
    {
    //user_key取得
        $user_info = $this->session->get('user_info');$this->logger->info("user_info=>".$user_info);
        $user_key = $user_info['user_key'];

    //エラー時にユーザインプットの再表示
    if( $member_info ){
        foreach($member_info as $key => $value){
            $this->template->assign($key, $value);
        }
    }
    if($message){
        $this->template->assign('message', $message);
    }
      $this->display('admin/member/companylist/company/index.t.html');
    }
}

$main =& new AppMember();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>

