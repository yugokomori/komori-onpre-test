<?php
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_DBI.class.php");
require_once("classes/dbi/pgi_setting.dbi.php");

class ChangePGiOption extends AppFrame {

    function init() {
        // DBサーバ情報取得
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $this->dsn = $this->get_dsn();
        //$this->dsn = "mysql://n2my:n2myforsi@localhost:3306/test_data1?charset=utf8";
    }

    function default_view() {

        print $this->_header();
print <<<EOM
<form action="change_pgi_option.php" name="encrypt" method="post">
<input type="hidden" name="action_chenge_pgi_option" value="">
<table border="0" cellspacing="0" cellpadding="5">
<tr><td>
<select name="pgi_system_key">
<option value="VCUBE GM NEW-TEST-PPM">VCUBE非課金(VCUBE GM NEW-TEST-PPM)</option>
<option value="VCUBE GM TEST-PPM">VCUBE非課金(VCUBE GM TEST-PPM)</option>
<option value="VCUBE TEST-PPM">VCUBE非課金(VCUBE TEST-PPM)</option>
</select>
</td></tr>
<input type="submit" name="submit" value="変換">
</form>
EOM;
        print $this->_footer();
    }

    function action_chenge_pgi_option() {
        $pgi_system_key = $this->request->get("pgi_system_key");
        $this->logger2->info($pgi_system_key);
        $objPgiSetting = new N2MY_DB($this->dsn, "pgi_setting");
        
        $where = "system_key = '".$pgi_system_key."'".
                 " AND is_deleted = 0";
        $pgi_setting_list = $objPgiSetting->getRowsAssoc($where);
        $this->logger2->info($pgi_setting_list);
        
        $objRoom = new N2MY_DB($this->dsn, "room");
        require_once("classes/dbi/ordered_service_option.dbi.php");
        $ordered_service_option = new OrderedServiceOptionTable($this->dsn);
        
        foreach ($pgi_setting_list as $pgi_setting) {
            $where_room = "room_key = '".addslashes($pgi_setting["room_key"])."'";
            $_room_info = $objRoom->getRow($where_room);
            $objUser = new N2MY_DB($this->dsn, "user");
            $where_user = "user_key = ".$_room_info["user_key"];
            $_user_info = $objUser->getRow($where_user, "user_key, user_id, user_company_name, meeting_version");
            if ($_user_info["user_delete_status"] != "2") {
                //オプション変更
                $delete_result = $this->action_delete_pgi_setting($pgi_setting["pgi_setting_key"], $_room_info["room_key"]);
                if (!$delete_result) {
                   $error_data = array($_user_info, $_room_info);
                   $this->logger2->info($error_data);
                } else if ($_user_info["meeting_version"]) {
                    $ret = $this->action_pgi_setting($_room_info["room_key"], "VCUBE HYBRID TEST-PPM");
                    if ($ret["message"]) {
                        $this->logger2->info($ret);
                    } else {
                        $this->logger2->info(array($_room_info["room_key"], $pgi_system_key), "success");
                        $success_user_list[] = $_user_info;
                    }
                } else {
                    $where_option = "room_key = '".addslashes($pgi_setting["room_key"])."'".
                                    " AND service_option_key = 23";
                    $data = array(
                        "ordered_service_option_status" => 0,
                        "ordered_service_option_deletetime" => date("Y-m-d H:i:s"),
                    );
                    $ordered_service_option->update($data, $where_option);
                }
            }
        }

        print $this->_header();
        if ($success_user_list) {
            $count = 0;
            foreach ($success_user_list as $user) {
                $data = array();
                if ($user["user_id"]) {
                    $count++;
                }
            }
        }
        print "成功総数：".$count;
        print $this->_footer();
    }

function _header() {
        return <<<EOM
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>PGiオプション変更</title>
<style TYPE="text/css">
<!--
body {
text-align: center;
font-size: 12px;
font-family: "Arial, sans-serif, ＭＳ Ｐゴシック", Osaka, "ヒラギノ角ゴ Pro W3" ;
margin: 10px 0px 0px 0px;
}
.title {
font-size: 16px;
font-weight: bold;
margin: 20px auto 0px auto;
width: 760px
}
#content {
text-align: center;
}
#content table {
border-collapse: collapse;
border-spacing: 0;
empty-cells: show;
border: 1px solid #999999;
margin: 20px auto;
width: 760px
}
#content th {
border: 1px solid #999999;
padding: 4px 5px;
background-color: #EFEFEF;
text-align: left;
}
#content td {
border: 1px solid #999999;
padding: 4px 10px;
text-align: left;
}
.errorSection {
color: #FF0000;
}
-->
</style>
</head>
<body>
<div id="content">
<h1>PGiオプション変更</h1>
EOM;
    }

    function _footer() {
       print "</body></html>";
    }
    
    function action_delete_pgi_setting($pgi_setting_key, $room_key)
    {
        if (!$pgi_setting_key || !$room_key) {
            return false;
        }

        require_once("classes/dbi/pgi_setting.dbi.php");
        $pgi_setting_table = new PGiSettingTable($this->dsn);
        $pgi_setting       = $pgi_setting_table->findByKey($pgi_setting_key);
        if (!$pgi_setting) {
            return false;
        }
        if ($pgi_setting['room_key'] != $room_key) {
            return false;
        }

        $data  = array('is_deleted' => 1);
        $where = sprintf("room_key='%s' AND pgi_setting_key='%s'", $room_key, $pgi_setting_key);
        $pgi_setting_table->update($data, $where);
        if ($pgi_setting['room_key'] != $room_key) {
            return false;
        }
        return true;

    }
    
    function action_pgi_setting($room_key, $system_key) {

        require_once("classes/dbi/pgi_setting.dbi.php");
        require_once("classes/dbi/pgi_rate.dbi.php");
        $pgiSettingTable = new PGiSettingTable($this->dsn);
        $pgiRateTable    = new PGiRateTable($this->dsn);
        if (!$room_key) {
            return false;
        }

        try {
            $pgiSettings = $pgiSettingTable->findByRoomKey($room_key);
        } catch (Exception $e) {
            return false;
        }
        $form_data = array("system_key" => $system_key,
                           "company_id_type" => "default",
                           "pgi_start_date" => date("Y/m"));
        $validated = $this->validatePGiSetting($form_data, $room_key);
        if ($validated['message']) {
            return $validated['message'];
        }

        $validated['room_key'] = $room_key;

        $this->request->set("room_key", $room_key);


        $pgi_setting_data = array('room_key'   => $room_key,
                                  'startdate'  => $validated['data']['startdate'],
                                  'system_key' => $validated['data']['system_key'],
                                  'company_id' => $validated['data']['company_id'],);
        try {
            $pgi_setting_key = $pgiSettingTable->add($pgi_setting_data);
        } catch (Exception $e) {
            return false;
        }

        require_once('classes/pgi/PGiSystem.class.php');
        $pgi_system = PGiSystem::findBySystemKey($pgi_setting_data['system_key']);

        if ($pgi_system->rates) { // 設定にデフォルト料金が登録されているsystemはpgi_rateに料金を保存
            require_once("classes/pgi/PGiRate.class.php");
            $this->logger2->debug($validated['data']);
            $rate_config = PGiRate::findGmConfigAll();
            foreach ($rate_config as $pgi_rate) {
                $pgi_rate_data = array('pgi_setting_key' => $pgi_setting_key,
                                       'rate_type'       => (string)$pgi_rate->type,
                                       'rate'            => $validated['data'][(string)$pgi_rate->type]);
                try {
                    $pgiRateTable->add($pgi_rate_data);
                } catch (Exception $e) {
                    return false;
                }
            }
        }
    }
    
    
    //{{{validatePGiSetting
    private function validatePGiSetting($form, $room_key)
    {
        $res = array('data'    => $form,
                     'message' => '');
        if (!$form["pgi_start_date"]) {
            $res['message'] .= "Please input the start time.<br />";
        } else {
            list($y, $m) = explode('/',$form["pgi_start_date"]);
            if (!checkdate($m, 1, $y)) {
                $res['message'] .= "Start Time is not correct.<br />";
            } else {
                $startdate = str_replace('/','-',$form["pgi_start_date"])."-01";
                // 過去分の設定はできません
                if ($startdate < date('Y-m').'-01') {
                    $res['message'].= "Can not be a past time.<br />";
                }
            }
        }
        $res['data']['startdate']  = $startdate;

        require_once('classes/pgi/PGiSystem.class.php');
        $pgiSystems    = PGiSystem::findConfigAll();
        $pgiSystemKeys = array();
        $selectedSystem         = null;
        foreach ($pgiSystems as $system) {
            if (@$form["system_key"] == (string)$system->key) {
                $selectedSystem = $system;
            }
        }
        if (!$form["system_key"]) {
            $res['message'] .= "Please input the system_key.<br />";
        } elseif (!$selectedSystem) {
            $res['message'] .= "The system_key is not correct.<br />";
        }

        if ($form["company_id_type"] == 'default') {
            $form["company_id"]        = $selectedSystem->defaultCompanyID;
            $res['data']['company_id'] = $selectedSystem->defaultCompanyID;
        }

        if (!$form["company_id"]) {
            $res['message'] .= "Please input the company_id.<br />";
        } else {
            if ($selectedSystem && $selectedSystem->rates) {
                require_once("classes/pgi/PGiRate.class.php");
                // validate rate's values
                $rate_config = PGiRate::findGmConfigAll();

                foreach ($rate_config as $rate){
                    $col  = (string)$rate->type;
                    $name = (string)$rate->description;
                    if (!$form[$col]) {
                        $res['message'] .= "Please input the ".$name.".<br />";
                    } elseif (is_int($form[$col])) {
                        $res['message'] .= $name." should be a number.<br />";
                    } elseif ($form[$col] <= 0) {
                        $res['message'] .= $name."should be larger than 0.<br />";
                    }
                }
            }
        }

        require_once("classes/dbi/pgi_setting.dbi.php");
        $pgi_setting_table = new PGiSettingTable($this->dsn);
        try {
            $pgi_settings  = $pgi_setting_table->findByRoomKey($room_key);
        } catch(Exception $e) {
            die('error'.$e->getMessage());
        }
        if (0 == count($pgi_settings)) {
            return $res;
        }

        foreach ($pgi_settings as $setting) {
            if ($startdate == $setting['startdate']) {
                $res['message'] .= $startdate."have already been set.(auto generate when meeting start）<br />";
                return $res;
            }
        }

        return $res;
    }

}
$main = new ChangePGiOption();
$main->execute();