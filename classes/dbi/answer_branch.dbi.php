<?php
require_once("classes/N2MY_DBI.class.php");

class AnswerBranchTable extends N2MY_DB {

    var $table = 'answer_branch';
    protected $primary_key = "answer_key";

    function __construct($dsn) {
        $this->init($dsn, $this->table);
    }

    function add($data) {
        $data["created_datetime"] = date("Y-m-d H:i:s");
        $res = parent::add($data);
        if (PEAR::isError($res)) {
            throw new Exception("db errror where:$where , data:".print_r($data,true));
        }
        //get answersheet_key
        $where = sprintf( "answer_id='%s' AND question_branch_id='%s'", $data['answer_id'], $data['question_branch_id']);
        $sort = array( "answer_id" => "desc" );
        $result = $this->getRow( $where, "answer_id", $sort );
        return $result["answer_id"];
    }
}
