<?php
require_once('classes/AppFrame.class.php');
require_once("classes/N2MY_Account.class.php");
require_once("classes/mgm/dbi/media_mixer.dbi.php");
require_once("classes/mgm/dbi/mcu_server.dbi.php");

class AppSecurity extends AppFrame
{

    var $objMcuServer = null;
    var $objMediaMixer = null;

    function init() {
        $this->objMcuServer = new McuServerTable(N2MY_MDB_DSN);
        $this->objMediaMixer = new MediaMixerTable(N2MY_MDB_DSN);
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->_name_space = md5(__FILE__);
    }

    function auth() {
        $this->checkAuth();
        $this->checkAdminAuth('admin/session_error.t.html');
    }

    function default_view() {
        return $this->action_top();
    }

    function action_top() {
      //
      return $this->display_mcu_list();
    }

    function action_ip_list() {
        return $this->display_mcu_list();
    }

    function display_mcu_list($message = null) {

        $where = "";
        $sort = array("create_datetime" => "asc");
        $_mcu_list = $this->objMcuServer->getRowsAssoc($where, $sort, null, null);
        foreach ($_mcu_list as $key => $mcu_info) {
          $where = sprintf("media_ip='%s'", $mcu_info["ip"]);
          $sort = array("create_datetime" => "desc");
          $media_mixer = $this->objMediaMixer->getRow($where, "*", $sort);
          $mcu_list[$key]["mcu_info"] = $mcu_info;
          $mcu_list[$key]["media_mixer"] = $media_mixer;
        }
        //$this->logger2->info($mcu_list);
        $this->template->assign("mcu_list", $mcu_list);

        if ($message) {
            $this->template->assign("message", $message);
        }
        $this->set_submit_key();
        $this->display('admin/mcu/mcu_manage.t.html');
    }

    function action_media_mixer_off(){
        if (!$this->check_submit_key()) {
            return $this->display_mcu_list();
        }
        $key = $this->request->get("key");
        $where = sprintf("media_mixer_key='%s'", $key);
        $data = array(
            "is_available" => "0",
            "update_datetime" => date("Y-m-d H:i:s"),
            );
       $this->objMediaMixer->update($data, $where);

       $media_server = $this->objMediaMixer->getRow($where);
       $where_now_mcu = "ip LIKE '%".mysql_escape_string($media_server["media_ip"])."%'";
       $now_mcu_server = $this->objMcuServer->getRow($where_now_mcu);
       $this->logger2->info($now_mcu_server );

       //mcu_nortificationにデータ登録
       require_once 'classes/dbi/mcu_notification.dbi.php';
       $obj_McuNotification = new McuNotificationTable(N2MY_MDB_DSN);
       $add_mcuNotification_data = array(
           "server_key" => $now_mcu_server["server_key"],
           "server_address" => $now_mcu_server["server_address"],
           "server_down_flg" => 0,
           "notification_type" => 3,
           //"reservation_ids" => "1d03f53ca2850e3d79853c1ef17862e4,c601cd3617ab9b56a86745c563694392,8594a05860e89895464a2afa153d2ccb,17b9e1ae4ef0000266ccbd4d10942bc4",
           "server_down_datetime" => date('Y-m-d H:i:s'),
       );
       $obj_McuNotification->add($add_mcuNotification_data);
        //コントローラーでの利用か確認。コントローラーだった場合はmcuを切り替える。
        if ($now_mcu_server["is_available"]) {
            //停止しているサーバを取得
          $available_media_where = "is_available = 1";
          $available_media_servers = $this->objMediaMixer->getRowsAssoc($available_media_where);
          $available_media_server_ip = "";
          foreach ($available_media_servers as $available_media_server) {
            $available_media_server_ip[] =  "'".$available_media_server["media_ip"]."'";
          }
            $where = "is_available = 0 AND system_down_flg = 0";
            if($available_media_server_ip) {
              $where .= ' AND ip IN ('.implode(",",$available_media_server_ip).')';
              $sort = array(
                      "server_key" => "asc");
              $mcu_servers = $this->objMcuServer->getRowsAssoc($where,$sort);
              $this->logger2->info($mcu_servers);
            } else {
              $mcu_servers = "";
            }

            if ($mcu_servers){

                foreach ($mcu_servers as $mcu_server) {
                    //別mcuを有効に変更
                    $cmd = sprintf("ssh " .N2MY_MCU_SCP_USER . '@%s "%s"' , $mcu_server["server_address"], $this->config->get("MCU_NOTIFICATION", "reload_cmd"));
                    exec($cmd, $arr, $res);
                    if($res !== 0){
                        $this->logger2->info(array($res,$arr,$cmd));
                    } else {
                        $where = "server_key = ".$mcu_server["server_key"];
                        $data = array("is_available" => 1);
                        $this->objMcuServer->update($data, $where);
                        break;
                    }
                }
            }
            //システムダウンステータスに変更
            $data = array("is_available" => 0);
            $this->objMcuServer->update($data, $where_now_mcu);
        }

       //APサーバに通知
       $alert_status = 0;
       //$alert_status = 1; //for test
       $ap_servers = explode(',', $this->config->get("MCU_NOTIFICATION", "ap_server_list"));
       $ap_server_symfony_path = $this->config->get("MCU_NOTIFICATION", "ap_server_symfony_path");
       $ap_server_symfony_task = $this->config->get("MCU_NOTIFICATION", "ap_server_symfony_task");
       if ($ap_server_symfony_task && $ap_server_symfony_path && $ap_servers) {
         foreach ($ap_servers as $ap_server) {
           $cmd = sprintf("ssh  " .N2MY_AP_SCP_USER . '@%s "%s"' , $ap_server, '"'.$ap_server_symfony_path.'"'.' "'.$ap_server_symfony_task.'"');
           exec($cmd, $arr, $res);
           if($res !== 0){
             $this->logger2->info(array($res,$arr,$cmd));
           } else {
             $this->logger2->info(array($res,$arr,$cmd));
             $alert_status = 1;
             break;
           }
         }
       } else {
         $this->logger->warn(array($ap_servers, $ap_server_symfony_path, $ap_server_symfony_task),"notfound ap server config");
       }

       if ($alert_status) {
         require_once ("classes/N2MY_Reservation.class.php");
         $objReservation = new N2MY_Reservation($this->get_dsn(), $this->session->get("dsn_key"));
         require_once ("classes/dbi/reservation.dbi.php");
         $obj_Reservation = new ReservationTable($this->get_dsn());
         //現在の会議をチェック
         $now_reservations = array();
         $now_mediaMixer = $media_server;
         //$this->logger2->info($now_mediaMixer)
         $where_now_reservation = "reservation_status = 1 AND media_mixer_key = '".mysql_escape_string($now_mediaMixer["media_mixer_key"])."'";
         $where_now_reservation .= "AND reservation_starttime < '".date('Y-m-d H:i:s')."'";
         $where_now_reservation .= "AND reservation_extend_endtime > '".date('Y-m-d H:i:s')."'";
         $_now_reservations = $obj_Reservation->getRowsAssoc($where_now_reservation);
         $this->logger2->info($where_now_reservation);
         foreach($_now_reservations as $key => $_reservation_data) {
           if($_reservation_data) {
             $reservation_info = array();
             $reservation_data = $objReservation->getDetail($_reservation_data["reservation_session"]);
             $this->logger2->info($reservation_data);
             require_once 'classes/dbi/member.dbi.php';
             $obj_Member = new MemberTable($this->get_dsn());
             $where = "member_status = 0 AND member_key = '".addslashes($reservation_data["info"]["member_key"])."'";
             $member_info = $obj_Member->getRow($where);
             $reservation_info["member_info"] = $member_info;
             $reservation_info["info"] = $reservation_data["info"];
             //extend参加人数
             require_once("classes/dbi/reservation_extend.dbi.php");
             $obj_reservationExtend = new ReservationExtendTable($this->get_dsn());
             $reservation_extends = $obj_reservationExtend->getRowsAssoc("reservation_session_id = '".$reservation_info["info"]["reservation_session"]."'");
             //$this->logger2->info($reservation_extend);
             foreach ($reservation_extends as $reservation_extend) {
               if($reservation_extend["extend_seat"]) {
                 $reservation_info["info"]["max_seat"] += $reservation_extend["extend_seat"];
               }
             }

             $now_reservations[] = $reservation_info;

             //mcu_down_flgを立つ
             $data = array();
             $data["mcu_down_flg"] = 1;
             $where = "reservation_key = '".addslashes($reservation_data["info"]["reservation_key"])."'";
             $obj_Reservation->update($data, $where);
           }
         }
         //notificationに入ってreservation_idsの確認
         $future_reservations = array();
         $last_mcu_notification = $obj_McuNotification->getRow("","*",array("server_down_datetime" => "desc"));
         //$this->logger2->info($last_mcu_notification);
         $reservation_sessions = split(",", $last_mcu_notification["reservation_ids"]);
         $this->logger2->info($reservation_sessions);

         foreach($reservation_sessions as $reservation_session) {
           $reservation_data = $objReservation->getDetail($reservation_session);
           if($reservation_data && $reservation_data["info"]["status"] != "end") {
             require_once 'classes/dbi/member.dbi.php';
             $obj_Member = new MemberTable($this->get_dsn());
             $where = "member_status = 0 AND member_key = '".addslashes($reservation_data["info"]["member_key"])."'";
             $member_info = $obj_Member->getRow($where);
             $reservation_info["member_info"] = $member_info;
             $reservation_info["info"] = $reservation_data["info"];

             $future_reservations[] = $reservation_info;

             //mcu_down_flgを立つ
             $data = array();
             $data["mcu_down_flg"] = 1;
             $where = "reservation_key = '".addslashes($reservation_data["info"]["reservation_key"])."'";
             $obj_Reservation->update($data, $where);
           }

         }

         //メール送信
         require_once ("lib/EZLib/EZMail/EZSmtp.class.php");
         $mail = new EZSmtp(null, $lang_cd, "UTF-8");
         $mail->setSubject("ＶＡ 会議システム　予約再割り振り実施のお知らせ");
         $mail->setFrom(N2MY_RETRY_RESERVATION_FROM);
         $mail->setReturnPath(NOREPLY_ADDRESS);
         $mail->setTo(N2MY_RETRY_RESERVATION_TO);
         $template = new EZTemplate($this->config->getAll('SMARTY_DIR'));
         $template->assign('reservations', $future_reservations);
         $template->assign('now_reservations', $now_reservations);
         $template->assign('has_mcu_server_down', 1);
         $body = $template->fetch('common/mail/send_reservation.t.txt');
         $this->logger2->info($body);
         $mail->setBody($body);
         $mail->send();
       } else {
             //エラーログ出す
             $this->logger->warn(array($ap_servers, $ap_server_symfony_path, $ap_server_symfony_task),"no response from ap server");
       }


        $this->display_mcu_list();
    }

    function action_media_mixer_on(){
        $message = "";
        if (!$this->check_submit_key()) {
            return $this->display_mcu_list();
        }
        $key = $this->request->get("key");
        //サーバ疎通チェック
        $where_media = sprintf("media_mixer_key='%s'", $key);
        $media_mixer = $this->objMediaMixer->getRow($where_media);
        $where_mcu = sprintf("ip='%s'", $media_mixer["media_ip"]);
        $now_mcu_server = $this->objMcuServer->getRow($where_mcu);

        $mcu_status = $this->check_msu_server($now_mcu_server);
        if (!$mcu_status) {
            $message = $this->get_message("MCU_NOTIFICATION", "status_notification");
            $this->display_mcu_list($message);
            exit;
        }

        $data = array(
            "is_available" => "1",
            "update_datetime" => date("Y-m-d H:i:s"),
            );
       $this->objMediaMixer->update($data, $where_media);
       //MCUサーバーダウンフラグを落ち
       $data = array(
           "system_down_flg" => "0",
           "update_datetime" => date("Y-m-d H:i:s"),
       );
       //現在有効のコントローラがない場合に、こちのサーバーが有効になる
       $available_mcu_server = $this->objMcuServer->numRows("is_available = 1");
       if(!$available_mcu_server) {
           $cmd = sprintf("ssh " .N2MY_MCU_SCP_USER . '@%s "%s"' , $now_mcu_server["server_address"], $this->config->get("MCU_NOTIFICATION", "reload_cmd"));
           exec($cmd, $arr, $res);
           if($res !== 0){
             $this->logger2->info(array($res,$arr,$cmd));
           } else {
             $data["is_available"] = "1";
           }
       }
       $this->objMcuServer->update($data, $where_mcu);

       //Server Notificationフラグを落ち
       require_once 'classes/dbi/mcu_notification.dbi.php';
       $obj_McuNotification = new McuNotificationTable(N2MY_MDB_DSN);
       $where_mcu_notification = "server_down_flg = 1 AND notification_type = 1";
       $update_mcuNotification_data = array(
           "server_down_flg" => 0,
           "server_up_datetime" => date('Y-m-d H:i:s'),
       );
       $obj_McuNotification->update($update_mcuNotification_data,$where_mcu_notification);

       //予約にMCUダウンフラグを落ち
       $data = array(
           "mcu_down_flg" => "0",
           "reservation_updatetime" => date("Y-m-d H:i:s"),
       );
       $where = "media_mixer_key = '".addslashes($media_mixer["media_mixer_key"])."'";
       $where .= " AND mcu_down_flg = 1";
       $where .= " AND reservation_starttime >'".date("Y-m-d H:i:s")."'";
       $this->logger2->info($where);
       require_once ("classes/dbi/reservation.dbi.php");
       $obj_Reservation = new ReservationTable($this->get_dsn());
       $obj_Reservation->update($data, $where);

        //APサーバに通知
        $ap_servers = explode(',', $this->config->get("MCU_NOTIFICATION", "ap_server_list"));
        $ap_server_symfony_path = $this->config->get("MCU_NOTIFICATION", "ap_server_symfony_path");
        $ap_server_symfony_task = $this->config->get("MCU_NOTIFICATION", "ap_server_symfony_task");
        if ($ap_server_symfony_task && $ap_server_symfony_path && $ap_servers) {
            foreach ($ap_servers as $ap_server) {
                $cmd = sprintf("ssh " .N2MY_AP_SCP_USER . '@%s %s' , $ap_server, '"'.$ap_server_symfony_path.'"'.' "'.$ap_server_symfony_task.'"');
                exec($cmd, $arr, $res);
                if($res !== 0){
                    $this->logger2->info(array($res,$arr,$cmd));
                } else {
                    $this->logger2->info(array($res,$arr,$cmd));
                    break;
                }
            }
        } else {
            $this->logger->warn(array($ap_servers, $ap_server_symfony_path, $ap_server_symfony_task),"notfound ap server config");
        }

        $this->display_mcu_list();
    }

    function action_controller_setup(){
      if (!$this->check_submit_key()) {
        return $this->display_mcu_list();
      }
      $key = $this->request->get("key");
      $where = sprintf("server_key='%s'", $key);
      $new_controller = $this->objMcuServer->getRow($where);
      $mcu_status = $this->check_msu_server($new_controller);
      if (!$mcu_status) {
          $message = $this->get_message("MCU_NOTIFICATION", "status_notification");
          $this->display_mcu_list($message);
          exit;
      }
      $cmd = sprintf("ssh  " .N2MY_MCU_SCP_USER . '@%s "%s"' , $new_controller["server_address"], $this->config->get("MCU_NOTIFICATION", "reload_cmd"));
      exec($cmd, $arr, $res);
      if($res !== 0){
          $this->logger2->info($arr );
          $this->logger2->info($cmd );
          $message = $this->get_message("MCU_NOTIFICATION", "status_notification");
          return $this->display_mcu_list($message);
      }

      $where = "is_available = 1";
      $data = array(
          "is_available" => "0",
          "update_datetime" => date("Y-m-d H:i:s"),
      );
      $this->objMcuServer->update($data, $where);
      $where = sprintf("server_key='%s'", $key);
      $data = array(
          "is_available" => "1",
          "update_datetime" => date("Y-m-d H:i:s"),
      );
      $this->objMcuServer->update($data, $where);
      $this->display_mcu_list();
    }

    function check_msu_server($mcu_server = "") {
        $url = "http://".$mcu_server["server_address"].":".$mcu_server["controller_port"];
        $this->logger2->debug($url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        $result = curl_exec($ch);
        return $result;
    }

}
$main = new AppSecurity();
$main->execute();
