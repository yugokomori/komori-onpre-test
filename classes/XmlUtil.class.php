<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 *
 * @package     PEAR.MAIL
 * @author      Yuki Asano <y-asano@vcube.co.jp>
 * @copyright   2006 The Nice to meet you for VIDEO Project
 * @version     CVS: $Id:$
 */

class XmlUtil
{

    var $values;
    var $index;

    //{{{ XmlUtil
    function XmlUtil()
    {
    }
    //}}}
    //{{{ parse_into_struct()
    /**
    * attribute取得
    *
    * @access public
    * @return array
    */
    function parse_into_struct($body)
    {

        $xp = xml_parser_create();
        $res = xml_parse_into_struct($xp, $body, $this->values, $this->index);
        if ($res) {
            return false;
        }
        xml_parser_free($xp);

        return true;
    }
    //}}}
    //{{{ getIndex()
    /**
    * index取得
    *
    * @access public
    * @return array
    */
    function getIndex()
    {
        return $this->index;
    }
    //}}}
    //{{{ getValues()
    /**
    * 配列取得
    *
    * @access public
    * @return array
    */
    function getValues()
    {
        return $this->values;
    }
    //}}}
    //{{{ getTagValues()
    /**
     * データ整形
     *
     * @access  public
     * @param   string   $prefix tag のプレフィクスを比較し取得
     * @param   bool     $rm_pre プレフィクスを削除
     * @since   3.1.0
     * @return  boolean
     */
    function getTagValues($prefix=null, $rm_pre=true)
    {
        $prefix = strtoupper($prefix);
        $index  = $this->getIndex();
        $values = $this->getValues();
        $xmlArray = array();
        if (is_array($index)) {
            foreach ($index as $k => $v) {
                if ($prefix && !preg_match("|^${prefix}|", $k)) {
                    continue;
                }
                foreach ($v as $key => $val) {
                    if ($rm_pre) {
                        $xmlArray[$key][preg_replace("|^${prefix}|", "", $k)] = @$values[$val]['value'];
                    } else {
                        $xmlArray[$key][$k] = @$values[$val]['value'];
                    }
                }
            }
        }
        return $xmlArray;
    }
    //}}}
}

?>
