CREATE TABLE `custom_domain_service` (
  `custom_domain_service_key` int(11) NOT NULL AUTO_INCREMENT COMMENT 'カスタムドメインキー',
  `service_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '提供サービス名',
  `domain` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ドメイン名',
  `status` int(11) DEFAULT '1' COMMENT 'ステータス',
  `create_datetime` datetime DEFAULT NULL COMMENT '作成日',
  `update_datetime` datetime DEFAULT NULL COMMENT '更新日',
  PRIMARY KEY (`custom_domain_service_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='カスタムドメイン用' AUTO_INCREMENT=4 ;

INSERT INTO `custom_domain_service` VALUES(1, 'paperless', 'paperless.nice2meet.us', 1, '2011-08-09 15:47:54', '2011-08-09 15:47:54');
INSERT INTO `custom_domain_service` VALUES(2, 'pgi', 'pgi.nice2meet.us', 1, '2011-08-09 15:47:54', '2011-08-09 15:47:54');
INSERT INTO `custom_domain_service` VALUES(3, 'tact', 'schoolie-gaia.nice2meet.us', 1, '2011-08-09 15:47:54', '2011-08-09 15:47:54');

INSERT INTO `service_option` (`service_option_key` ,`country_key` ,`service_option_name` ,`service_option_price` ,`service_option_init_price` ,`service_option_running_price`) VALUES (23 , '0', 'teleconference', '0', '0', '0');
