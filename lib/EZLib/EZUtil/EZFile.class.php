<?php
class EZFile {

    /**
     * 指定ディレクトリの一括削除
     * 超注意して使ってね！！
     */
    function rmtree($dir) {
        if (!$dir) return false;
        if($objs = glob($dir."/*")){
            foreach($objs as $obj) {
                is_dir($obj)? $this->rmtree($obj) : unlink($obj);
            }
        }
        @rmdir($dir);
    }

    function mkdir_r($pathname, $mode)
    {
        is_dir(dirname($pathname)) || EZFile::mkdir_r(dirname($pathname), $mode);
        return is_dir($pathname) || @mkdir($pathname, $mode);
    }
}
?>