<?php
/*
 * Created on 2013/10/31
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once("lib/EZLib/EZUtil/EZString.class.php");
require_once("classes/N2MY_Api.class.php");
require_once ("classes/core/dbi/Storage.dbi.php");
require_once ("classes/dbi/storage_favorite.dbi.php");
require_once ("classes/N2MY_Storage.class.php");
require_once ("classes/N2MY_Clip.class.php");

class N2MY_Meeting_Storage_API extends N2MY_API
{


    private $_name_space                 = null;
    private $_userKey                    = null;
    private $_userId                     = null;
    private $_memberKey                  = null;
    private $_obj_Storage      = null;
    private $_obj_StorageFavorite        = null;
    private $_obj_StorageTable        = null;
    private $_obj_N2MY_Clip        = null;


    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        // SSL対応
        header('Pragma:');
        $this->_name_space = md5(__FILE__);
    }

    /**
     * 認証処理
     *
     * @param
     * @return
     */
    function auth()
    {
        $this->checkAuthorization();
        $userInfo                      = $this->session->get("user_info");
        if($userInfo["max_storage_size"] == 0){
            header("Location: ".'/');
        }
        $userKey                       = $userInfo["user_key"];
        $serverInfo                    = $this->session->get("server_info");
        $memberInfo                    = $this->session->get("member_info");
        $this->_userKey                = $userKey;
        if($memberInfo["member_key"]){
            $this->_memberKey              = $memberInfo["member_key"];
        }else{
            $this->_memberKey              = 0;
        }

        $dsn                           = $this->get_dsn();
        $this->_obj_Storage  = new N2MY_Storage($dsn, $serverInfo["host_name"]);
        $this->_userId                 = $this->_obj_Storage->getUserId("", $userKey);
        $this->_obj_StorageFavorite    = new StorageFavoriteTable($dsn);
        $this->_obj_StorageTable        = new DBI_StorageFile($dsn);
        $this->_obj_N2MY_Clip = new N2MY_Clip( $this->get_dsn() );

    }

    function action_get_folder_key(){

        $external_service_account_key = $this->request->get("external_service_account_key");
        $external_service_name = $this->request->get("external_service_name");

        $rules = array(
                "external_service_account_key"      => array("required" => true,
                                                    "maxlen"   => 255,
                                                ),
                "external_service_name"             => array("required" => true,
                                                    "maxlen"   => 255,
                                                ),
                "output_type"           => array("allow"    => $this->get_output_type_list()),
        );

        $param = array(
                "external_service_account_key"    => $external_service_account_key,
                "external_service_name"    => $external_service_name,
                );
        $err_obj = $this->error_check($param, $rules);
        $folder_name = $this->request->get("folder_name")? $this->request->get("folder_name") : $external_service_name;
        $where = "status = 1 AND user_key = ".$this->_userKey . " AND member_key = " . $this->_memberKey . " AND external_service_account_key = '" . addslashes($external_service_account_key) . "'";



        $folder_info = $this->_obj_Storage->getFolderInfoByexternal($where);
        $this->logger2->info($folder_info);

        if (EZValidator::isError($err_obj)) {
            return $this->display_error(100, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }

        if($folder_info == null){
            // フォルダ作成
            $folder_key = $this->_obj_Storage->addExternalServiceFolder(0 , $this->_userKey , $this->_memberKey , $folder_name , $external_service_account_key , $external_service_name);
            if($folder_key == -1){
                return $this->display_error(100, "FOLDER COUNT OVER ERROR", "");
            }
        }else{
            $folder_key = $folder_info["external_service_folder_key"];
        }

        $out_put["folder_key"] = $folder_key;
        $this->logger2->info($folder_key);
        return $this->output($out_put);
    }

    /**
     * 資料一覧
     *
     * @param string room_key ルームキー
     * @param string file ファイル情報
     * @return boolean
     */
    function action_get_file_list(){
        if(!$request){
            $request = $this->request->getAll();
            $return_flag = false;
        }

        $search_type = "all";

        $sort = array();

        // Default セット
        $sort_key = $request["sort_key"];
        if(!$sort_key){
            $sort_key = "create_datetime";
        }

        $sort_type = $request["sort_type"];
        if(!$sort_type){
            $sort_type = "asc";
        }

        $limit = $request["limit"];
        if($limit == null){
            $limit = 10;
        }

        $page_count = $request["page"];
        if(!$page_count){
            $page_count = 1;
        }

        $sort_key_list = array("external_service_file_key" , "file_name" , "file_size" , "format", "category" , "extension" , "document_index", "status" , "create_datetime");
        $rules = array(
                "limit"      => array("integer" => true),
                "page"             => array("integer" => true),
                "sort_key"             => array("allow" => $sort_key_list),
                "sort_type"             => array("allow" => array("asc", "desc")),
                "output_type"           => array("allow"    => $this->get_output_type_list()),
        );

        $param = array(
                "limit"    => $limit,
                "page"    => $page_count,
                "sort_key"    => $sort_key,
                "sort_type"    => $sort_type,
        );
         // 値チェック
        $err_obj = $this->error_check($param, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(100, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }

        $page = ($page_count-1) *  $limit;
        $sort = array($sort_key => $sort_type);
        $member_info = $this->session->get("member_info");
        $user_info = $this->session->get("user_info");

//        $this->logger2->info();
        if($this->_memberKey == 0 || $member_info["use_shared_storage"] == 0){
            $where = "category != 'personal_white' AND status !=3 AND storage_file.user_key=" . $this->_userKey. " AND storage_file.member_key = " . $this->_memberKey;
        }else{
            $where = "category != 'personal_white' AND status !=3 AND storage_file.user_key=" . $this->_userKey. " AND (storage_file.member_key = " . $this->_memberKey. " OR storage_file.member_key = 0)";
        }

        switch($search_type){
            case "all":
                break;
            case "favorite":
                $favorite = $this->_obj_Storage->_getWhereFavorite($this->_memberKey);
                break;
            case "owner":
                $where = $where . " AND owner=".$this->_memberKey;
                break;
            case "document":
                $where = $where . " AND category='document'";
                break;
            case "image":
                $where = $where . " AND category='image'";
                break;
            case "video":
                $where = $where . " AND category='video'";
                break;
        }

        // 外部フォルダーキー取得
        $external_service_folder_key = $this->request->get('folder_key');
        if($external_service_folder_key != null){
            $where_folder = "user_key = ".$this->_userKey . " AND member_key = " . $this->_memberKey . " AND external_service_folder_key = '" . addslashes($external_service_folder_key) . "'";
            $folder_info = $this->_obj_Storage->getFolderInfoByexternal($where_folder);
            if($folder_info == null){
                return $this->display_error(100, "folder_key error", "");
            }
            $folder_key = $folder_info["storage_folder_key"];
            $where = $where . " AND storage_folder_key = " .$folder_key;
        }

        //$this->logger2->info($where);

        $count = $this->_obj_Storage->numRows($where);

        $datas = $this->action_list($this->_obj_Storage->getList($where, $sort, $limit, $page, "storage_file.*",$favorite));
        foreach($datas as $data){
            $file = array(
                    "document_id"    => $data["document_id"],
                    "external_service_file_key" => $data["external_service_file_key"],
                    "file_name"      => $data["user_file_name"],
                    "file_size"      => $data["file_size"],
                    "format"         => $data["format"],
                    "category"       => $data["category"],
                    "extension"      => $data["extension"],
                    "document_index" => $data["document_index"],
                    "status"         => $data["status"],
                    "create_datetime"=> $data["create_datetime"]
                    );

            if($data["category"] == "video"){
                $file["document_id"] = $data["clip_key"];
            }

            $list["file"][] = $file;
        }

        //$list["file"] = $this->action_list($this->_obj_Storage->getList($where, $sort, 1000, 0, "storage_file.*",$favorite));
        $output_data["count"] = $count;
        $output_data["files"] = $list;

        return $this->output($output_data);
    }

    /**
     * web側にも同じ処理あり
     * ファイルリストを取得したらお気に入り確認やサイズの成形を行うため必ず通す TODO
     */
    function action_list($list){
        $list_count = count($list);
        $where = " user_key = " . $this->_userKey . " AND member_key = " . $this->_memberKey;
        // お気に入り取得
        $favoreite_list = array();
        $favoreite_data = $this->_obj_StorageFavorite->getRowsAssoc($where);
        // お気に入りリスト作成
        foreach($favoreite_data as $favoreite){
            $favoreite_list[] = $favoreite["storage_file_key"];
        }
        //$this->logger2->info($favoreite_list);
        return $list;
    }

    /**
     * 資料追加
     *
     * @param string
     * @param string file ファイル情報
     * @return
     */
    function action_add_file($output_flg = true , $old_file_name = null)
    {
        $userKey          = $this->_userKey;
        $memberKey        = $this->_memberKey;
        $format           = $this->request->get('format');
        if($format != "bitmap" && $format != "vector"){
            $format = "bitmap";
        }

        $user_info = $this->session->get("user_info");
        $objStorage = $this->_obj_Storage;
        $total_size = $this->_obj_Storage->get_total_size($userKey);
        if($total_size > $user_info["max_storage_size"] * 1024 * 1024){
            $this->logger->error("size over");
            return $this->display_error(100, "size over", "");
        }
        if(!($createDocument = $this->formatStorageData($format, $userKey, $memberKey))) {
            $this->logger->error("format error");
            return $this->display_error(100, "format error", "");
        }

        if($createDocument == 4){
            $this->logger->error("FILE size over");
            return $this->display_error(100, "FILE size over", "");
        }

        $external_service_file_key = $this->request->get('external_service_file_key');
        // 外部フォルダーキー取得
        $external_service_folder_key = $this->request->get('folder_key');
        $folder_key = 0;
        // DBから取得する
        if($external_service_folder_key != null){
             $where = "user_key = ".$this->_userKey . " AND member_key = " . $this->_memberKey . " AND external_service_folder_key = '" . addslashes($external_service_folder_key) . "'";
             $folder_info = $this->_obj_Storage->getFolderInfoByexternal($where);
             if($folder_info == null){
                 return $this->display_error(100, "folder_key error", "");
             }
             $folder_key = $folder_info["storage_folder_key"];
        }



        $file_name = $this->request->get('file_name');
        if($file_name || $file_name != ""){
            $createDocument["name"] = $file_name;
        }
        if($old_file_name != null){
            $createDocument["name"] = $old_file_name;
        }

        $id = "";

        if($createDocument["category"] == 'video'){
            $n2my_clip  = new N2MY_Clip($this->get_dsn());
            $params = array(
                    'title'       => $createDocument['name'],
                    'user_key'    => $userKey,
            );
            try {
                $clip_id = $n2my_clip->add($params, $createDocument["tmp_name"]);
                $add_res = $objStorage->add($userKey, $memberKey, $createDocument["tmp_name"], $createDocument["name"], $createDocument["size"], $format, $createDocument["category"], $createDocument["extension"], "as2",null,$clip_id,null,$folder_key,0,addslashes($external_service_file_key));
                $id = $clip_id;
                if($add_res === false){
                    return $this->display_error(1000, "DataBase error", "");
                }
            } catch (Exception $e) {
                    return $this->display_error(1000, "DataBase error", "");
            }
        }else{
            $document_id = $objStorage->addDocument($createDocument["tmp_name"], $createDocument["name"], $createDocument["format"], $userKey, $memberKey);
            // DBへ登録
            $add_res = $objStorage->add($userKey, $memberKey, $createDocument["tmp_name"], $createDocument["name"], $createDocument["size"], $format, $createDocument["category"], $createDocument["extension"], "as2",$document_id,null,null,$folder_key,0,$external_service_file_key);
            $id = $document_id;
            if($add_res === false){
                return $this->display_error(1000, "DataBase error", "");
            }
        }
        // tmpファイルを一応削除処理 (容量圧迫の原因になっていた)
        if(is_file($createDocument["tmp_name"])){
            unlink($createDocument["tmp_name"]);
        }

        if(!$id){
            return $this->display_error(1000, "upload error", "");
        }
        $out_put["document_id"] = $id;
        $out_put["external_service_file_key"] = $external_service_file_key;
        if($output_flg){
            return $this->output($out_put);
        }else{
            return $out_put;
        }
    }

    /**
     * web側にも同じ処理あり
     * 入力フォーマットを整形
     */
    private function formatStorageData($format, $userKey, $memberKey) {
        $this->logger2->info($_FILES);
        // ファイル一覧
        if(empty($_FILES)) {
            return;
        }
        //        $this->logger2->info($_FILES);

        // 対応フォーマット情報取得
        $lists = $this->_obj_Storage->getSupportFormat();
        $file = $_FILES["file"];
        $fileinfo = pathinfo($file["name"]);
        $string = new EZString();
        $storageKey = $string->create_id();
        //$this->logger2->info($file);
        $error = $file["error"];
        if ($error != 0) {
            $this->logger->error("error:" . $error);
            return;
        }

        if (0 == $file["size"]) {
            $error = 4;
            // 容量制限
        } else {
            if (in_array(strtolower($fileinfo["extension"]), $lists['image_ext_list'])) {
                $maxsize = $this->config->get("STORAGE", "max_image_size");
                $format = "bitmap";
                $category = "image";
                // 資料
            } else if (in_array(strtolower($fileinfo["extension"]), $lists['document_ext_list'])){
                $maxsize = $this->config->get("STORAGE", "max_file_size");
                $category = "document";
            }
            else if (in_array(strtolower($fileinfo["extension"]), $lists['video_ext_list'])) {
                $maxsize = $this->config->get("STORAGE", "max_video_size");
                $category = "video";
            }
            else{
                return;
            }
            if ($maxsize * 1024 * 1024 < $file["size"] ) {
                $error = 4;
            }
        }

        if (0 != $error) {
            $this->logger->error("error size over");
            return 4;
        }
        $dir = $this->get_work_dir();
        //        $this->logger->info($dir);

        if($category != "video"){
            $tmpName = $dir."/".$storageKey.".".$fileinfo["extension"];
            move_uploaded_file($file["tmp_name"], $tmpName);
        }else{
            $tmpName = $file["tmp_name"];
        }
        $name = $file["name"];
        $convetFormat = $format ? $format : "bitmap";
        $retArray = array(
                "name"        => $name,
                "type"        => $file["type"],
                "tmp_name"    => $tmpName,
                "error"       => $error,
                "size"        => $file["size"],
                "format"      => $convetFormat,
                "extension"   => $fileinfo["extension"],
                "category"    => $category,
        );
        //$this->logger2->info($retArray);

        return $retArray;
    }

    function  action_update_file(){
        $userKey          = $this->_userKey;
        $memberKey        = $this->_memberKey;
        $document_id = $this->request->get('document_id');
        $objStorage = $this->_obj_Storage;
        // ストレージ確認
        $where = " user_key=" .$userKey . " AND member_key=" . $memberKey . " AND (document_id = '" .addslashes($document_id) . "' OR clip_key='" . addslashes($document_id) . "')";

        $file_info = $objStorage->getRowStorage($where);
        $this->logger2->info($file_info);
        if(!$file_info || $file_info["status"] == 3){
            return $this->display_error(100, "Parameter error", "");
        }

        if($file_info["status"] == 0 || $file_info["status"] == 1){
            return $this->display_error(100, "File Status error", "");
        }

        // ファイル追加 エラーがあればexitされる
        $add_info = $this->action_add_file(false);
        //
        //$this->logger2->info($add_info);
        if($add_info){
            // ファイル削除
            if($file_info["category"] == "video"){
                $this->action_clip_delete_done($file_info["clip_key"] , $file_info["storage_file_key"]);
            }else{
                $result = $objStorage->delete_file($file_info["storage_file_key"],$userKey, $memberKey);
                if(!$result){
                    return $this->display_error(100, "Parameter error", "");
                }
            }
        }

        return $this->output($add_info);
    }

    function  action_delete_file(){
        $userKey          = $this->_userKey;
        $memberKey        = $this->_memberKey;
        $document_id = $this->request->get('document_id');
        $objStorage = $this->_obj_Storage;
        // ストレージファイル確認
        $where = " user_key=" .$userKey . " AND member_key=" . $memberKey . " AND (document_id = '" .addslashes($document_id) . "' OR clip_key='" . addslashes($document_id) . "')";

        $file_info = $objStorage->getRowStorage($where);
        $this->logger2->info($file_info);
        if(!$file_info || $file_info["status"] == 3){
            return $this->display_error(100, "Parameter error", "");
        }

        if($file_info["status"] == 0 || $file_info["status"] == 1){
            return $this->display_error(100, "File Status error", "");
        }

        if($file_info["category"] == "video"){
            $this->action_clip_delete_done($file_info["clip_key"] , $file_info["storage_file_key"]);
        }else{
            $result = $objStorage->delete_file($file_info["storage_file_key"],$userKey, $memberKey);
            if(!$result){
                return $this->display_error(100, "Parameter error", "");
            }
        }
        return $this->output("");
    }

    /** web側にも同じ処理あり
     * 動画詳細情報取得
     */
    private function _getClipInfo($clip_key = null)
    {

        if($clip_key == null){
            $request = $this->request->getAll();
        }else{
            $request["clip_key"] = $clip_key;
        }
        if( empty( $request["clip_key"] ) ){
            $this->logger2->error("clip key lost");
            $this->errorToExit(CLIP_ERROR_KEY_LOST);
        }

        $clip_obj = new ClipTable( $this->get_dsn() );

        $wheres[] = sprintf( "clip_key   = '%s'", mysql_real_escape_string( $request["clip_key"] ) );
        $wheres[] = sprintf( "user_key   = '%s'", mysql_real_escape_string( $this->_userKey ) );
        $wheres[] = sprintf( "is_deleted = '%s'", 0 );

        $rs = $clip_obj->getRow( implode( " AND ", $wheres ), "*", array(), 1 );
        if (DB::isError($rs) || empty($rs) ) {
            echo $this->display_error(1000, "DataBase error", "");
            $this->logger2->error("clip not found");
            exit;
        }

        return $rs;
    }

    /** web側にも同じ処理あり
     * 動画削除実行
     */
    private function action_clip_delete_done($clip_key ,$storage_file_key )
    {
        $clip_info             = $this->_getClipInfo($clip_key);
        $clip_info["duration"] = gmdate('H:i:s', $clip_info["duration"]);

        $user_info = $this->session->get( "user_info" );
        $meeting_clip_obj = new MeetingClipTable( $this->get_dsn() );
        $delete_data = array(
                "is_deleted" => 1
        );
        // ミーティングクリップも削除
        $meeting_clip_obj->update($delete_data, "storage_file_key = " . $storage_file_key);
        // 同クリップキーがないかチェック
        $where = sprintf("clip_key = '%s' AND status = %s" , $clip_key , 2);
        $res = $this->_obj_StorageTable->numRows($where);
        // 動画削除処理
        if($res == 0){
            try {
                $this->_obj_N2MY_Clip->delete( $clip_key, $user_info['user_key']);
            } catch (Exception $e) {
                $this->logger2->error( $e->getMessage() );
            }
        }
    }

}
$main = new N2MY_Meeting_Storage_API();
$main->execute();