<?php
require_once("classes/mcu/config/McuConfigProxy.php");
 
require_once(dirname(__FILE__)."/../../model/MCUControllerProxy.php");
require_once(dirname(__FILE__)."/../../model/MeetingProxy.php");
require_once(dirname(__FILE__)."/../../model/ConferenceProxy.php");

class ConferenceEndLogic {
	private $logger;
	private $requestProxy;
	
	// record
	public $conferenceRecord;
	public $settingRecord;
	
	// irregal
	public $irregal = false;
	
	function __construct($model) {
		$this->config	= new McuConfigProxy();
		$this->logger = EZLogger2::getInstance();

		$this->mcuControllerProxy	= MCUControllerProxy::getInstance();
		$this->meetingProxy			= MeetingProxy::getInstance();
		$this->requestProxy			= $model;
		$this->videoConferenceProxy	= ConferenceProxy::getInstance();
	}

	// IConference
	public function destroyConference() {
		$this->mcuControllerProxy->removeConferenceByConfId($this->requestProxy->confId);
	}
	
	public function setConferenceRecord() {
		$conferenceRecord = $this->videoConferenceProxy->getConferenceByConfId($this->requestProxy->confId);
		if (!$conferenceRecord) {
			throw new Exception("no conference record. confId: ".$this->requestProxy->confId);
		}
		$this->conferenceRecord = $conferenceRecord;
	}

	public function setSettingRecord() {
		$settingRecord = $this->meetingProxy->getSettingRecordByDid($this->requestProxy->did);
		if (count($settingRecord) > 1)
			throw new Exception("irregal record status. did:".$this->requestProxy->did);
		if (1 == count($settingRecord))
			$this->settingRecord = $settingRecord[0];
	}
}
