ALTER TABLE  `room` ADD `audience_chat_flg` TINYINT(1) DEFAULT 0 COMMENT 'オーディエンスのチャットフラグ' AFTER  `prohibit_extend_meeting_flg`;
ALTER TABLE  `user` ADD `force_stop_meeting_flg` tinyint(1) DEFAULT 0 NOT NULL COMMENT '強制会議終了機能使用フラグ' AFTER  `member_multi_room_flg`;
ALTER TABLE  `reservation` ADD `member_key` INT(11) DEFAULT NULL COMMENT 'メンバーKEY' AFTER  `user_key`;
ALTER TABLE  `room` ADD  `creating_meeting_flg` TINYINT NOT NULL COMMENT '会議データ作成中フラグ' DEFAULT  '0' AFTER  `meeting_limit_time`;
ALTER TABLE  `meeting_suggest_log` MODIFY COLUMN `level` VARCHAR(11) NULL COMMENT  'レベル';