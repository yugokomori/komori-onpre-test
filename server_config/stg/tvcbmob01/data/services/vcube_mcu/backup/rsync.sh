#! /bin/sh

COMMAND_NORMAL='/usr/bin/rsync'		# rsync command
COMMAND_DEBUG='echo /usr/bin/rsync'	# rsync command (test)
COMMAND_NORMAL_OPTION='-az --delete -e'	# rsync option 
COMMAND_DEBUG_OPTION='-azn --delete -e'	# rsync option (test)
SSH_OPTION='ssh'			# ssh command
ORIGINAL_DIRECTORY='/usr/local/sailfin/domains/domain1/config'	# original
#ORIGINAL_FILES=(ConferenceManagerListeners.xml profiles.xml mixers.xml template-guest-did.xml templates.xml domain.xml)	# array.
ORIGINAL_FILES=(ConferenceManagerListeners.xml profiles.xml template-guest-did.xml templates.xml domain.xml)	# array.
DEST_USER='root'							# ssh user
DEST_SERVERS=(dev-pis-mob02.nice2meet.us dev-pis-mob03.nice2meet.us)	# target server
#DEST_DIRECTORY='/home/wada/backup'					# target directory
DEST_DIRECTORY='/usr/local/sailfin/domains/domain1/config'		# target directory

# debug mode
arg=$1
if [ "${arg}" = "-x" ]; then
	COMMAND=${COMMAND_DEBUG}
	COMMAND_OPTION=${COMMAND_DEBUG_OPTION}
else
	COMMAND=${COMMAND_NORMAL}
	COMMAND_OPTION=${COMMAND_NORMAL_OPTION}
fi

# target
SEPARATOR='/'
ORIGINAL_FILE=''
ORIGINAL_FILE_SEPARATOR=' '

for FILE in ${ORIGINAL_FILES[@]}
do
	ORIGINAL_FILE=${ORIGINAL_FILE}${ORIGINAL_DIRECTORY}${SEPARATOR}${FILE}${ORIGINAL_FILE_SEPARATOR}
done

# exec
for SERVER in ${DEST_SERVERS[@]}
do
	${COMMAND} ${COMMAND_OPTION} "${SSH_OPTION}" ${ORIGINAL_FILE} ${DEST_USER}@${SERVER}:${DEST_DIRECTORY}
	echo "${COMMAND} ${COMMAND_OPTION} "${SSH_OPTION}" ${ORIGINAL_FILE} ${DEST_USER}@${SERVER}:${DEST_DIRECTORY}"
done

exit 0
