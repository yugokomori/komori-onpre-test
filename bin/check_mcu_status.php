#!/usr/local/bin/php
<?php
require_once("set_env.php");
require_once("classes/AppFrame.class.php");
require_once("classes/mgm/dbi/mcu_server.dbi.php");
require_once("classes/mgm/dbi/media_mixer.dbi.php");

class AppCheckMcuStatus extends AppFrame {

    var $_mail_flg = 1;
    var $dsn = null;
    var $objMcuServer = null;
    var $objMediaMixer = null;
    
    
    function __destruct() {
        //unlink(N2MY_APP_DIR."/tmp/sess_".session_id());
    }
    
    function init() {
        $this->dsn = $this->get_dsn();
        $this->objMcuServer = new McuServerTable(N2MY_MDB_DSN);
        $this->objMediaMixer = new MediaMixerTable(N2MY_MDB_DSN);
    }

    function default_view() {
    	
    	$where = "is_available = 0";
    	$media_servers = $this->objMediaMixer->getRowsAssoc($where);
    	$this->logger2->info($media_servers );
    	foreach($media_servers as $media_server) {
    		//$this->logger2->info($media_server );
    		if($media_server) {
    			$where_now_mcu = "ip LIKE '%".mysql_escape_string($media_server["media_ip"])."%'";
    			$mcu_server = $this->objMcuServer->getRow($where_now_mcu);
    			$this->logger2->info($mcu_server );
    			//mcu_nortificationにデータ登録
    			require_once 'classes/dbi/mcu_notification.dbi.php';
    			$obj_McuNotification = new McuNotificationTable(N2MY_MDB_DSN);
    			$add_mcuNotification_data = array(
    					"server_key" => $mcu_server["server_key"],
    					"server_address" => $mcu_server["server_address"],
    					"server_down_flg" => 0,
    					"notification_type" => 2,
    					//"reservation_ids" => "1d03f53ca2850e3d79853c1ef17862e4,c601cd3617ab9b56a86745c563694392,8594a05860e89895464a2afa153d2ccb,17b9e1ae4ef0000266ccbd4d10942bc4",
    					"server_down_datetime" => date('Y-m-d H:i:s'),
    			);
    			$obj_McuNotification->add($add_mcuNotification_data);
    		
    			//APサーバに通知
    			$alert_status = 0;
    			//$alert_status = 1; //for test
    			$ap_servers = explode(',', $this->config->get("MCU_NOTIFICATION", "ap_server_list"));
    			$ap_server_symfony_path = $this->config->get("MCU_NOTIFICATION", "ap_server_symfony_path");
    			$ap_server_symfony_task = $this->config->get("MCU_NOTIFICATION", "ap_server_symfony_task");
    			if ($ap_server_symfony_task && $ap_server_symfony_path && $ap_servers) {
    				foreach ($ap_servers as $ap_server) {
    					$cmd = sprintf("ssh  " .N2MY_AP_SCP_USER . '@%s "%s"' , $ap_server, '"'.$ap_server_symfony_path.'"'.' "'.$ap_server_symfony_task.'"');
    					exec($cmd, $arr, $res);
    					if($res !== 0){
    						$this->logger2->info(array($res,$arr,$cmd));
    					} else {
    						$this->logger2->info(array($res,$arr,$cmd));
    						$alert_status = 1;
    						break;
    					}
    				}
    			} else {
    				$this->logger->warn(array($ap_servers, $ap_server_symfony_path, $ap_server_symfony_task),"notfound ap server config");
    			}
    		
    			if ($alert_status) {
    				require_once ("classes/N2MY_Reservation.class.php");
    				$objReservation = new N2MY_Reservation($this->get_dsn(), $this->session->get("dsn_key"));
    				require_once ("classes/dbi/reservation.dbi.php");
    				$obj_Reservation = new ReservationTable($this->get_dsn());
    				//notificationに入ってreservation_idsの確認
    				$future_reservations = array();
    				$last_mcu_notification = $obj_McuNotification->getRow("","*",array("server_down_datetime" => "desc"));
    				//$this->logger2->info($last_mcu_notification);
    				$reservation_sessions = split(",", $last_mcu_notification["reservation_ids"]);
    				$this->logger2->info($reservation_sessions);
    		
    				foreach($reservation_sessions as $reservation_session) {
    					$reservation_data = $objReservation->getDetail($reservation_session);
    					if($reservation_data && $reservation_data["info"]["status"] != "end") {
    						require_once 'classes/dbi/member.dbi.php';
    						$obj_Member = new MemberTable($this->get_dsn());
    						$where = "member_status = 0 AND member_key = '".addslashes($reservation_data["info"]["member_key"])."'";
    						$member_info = $obj_Member->getRow($where);
    						$reservation_info["member_info"] = $member_info;
    						$reservation_info["info"] = $reservation_data["info"];
    		
    						$future_reservations[] = $reservation_info;
    		
    						//mcu_down_flgを立つ
    						$data = array();
    						$data["mcu_down_flg"] = 1;
    						$where = "reservation_key = '".addslashes($reservation_data["info"]["reservation_key"])."'";
    						$obj_Reservation->update($data, $where);
    					}
    						
    				}
    		
    				//メール送信
    				require_once ("lib/EZLib/EZMail/EZSmtp.class.php");
    				$mail = new EZSmtp(null, $lang_cd, "UTF-8");
    				$mail->setSubject("ＶＡ 会議システム　予約再割り振り実施のお知らせ");
    				$mail->setFrom(N2MY_RETRY_RESERVATION_FROM);
    				$mail->setReturnPath(NOREPLY_ADDRESS);
    				$mail->setTo(N2MY_RETRY_RESERVATION_TO);
    				$template = new EZTemplate($this->config->getAll('SMARTY_DIR'));
    				$template->assign('reservations', $future_reservations);
    				$template->assign('now_reservations', $now_reservations);
    				$template->assign('has_mcu_server_down', 0);
    				$body = $template->fetch('common/mail/send_reservation.t.txt');
    				$this->logger2->info($body);
    				$mail->setBody($body);
    				$mail->send();
    			} else {
    				//エラーログ出す
    				$this->logger->warn(array($ap_servers, $ap_server_symfony_path, $ap_server_symfony_task),"no response from ap server");
    			}
    		}
    	}
    	
    	return true;
    }


}
$main = new AppCheckMcuStatus();
$main->execute();