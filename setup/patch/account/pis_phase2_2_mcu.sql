ALTER TABLE  `mcu_server` ADD  `system_down_flg` TINYINT( 1 ) NOT NULL DEFAULT  '0' COMMENT  '障害発生フラグ' AFTER  `is_available`;
ALTER TABLE  `mcu_server` ADD  `system_down_datetime` DATETIME NOT NULL DEFAULT  '0000-00-00 00:00:00' COMMENT  'システムダウンフラグ変更時間' AFTER  `update_datetime`;

CREATE TABLE `mcu_notification` (
`mcu_notification_key` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'MCUノーティフィケーションキー',
`server_key` int(11) NOT NULL COMMENT  'MCUサーバーキー',
`server_address` varchar(64) NOT NULL COMMENT  'サーバーアドレス',
`reservation_ids` text DEFAULT NULL COMMENT  '割り振りできなかった予約ID（カンマで区切り）',
`server_down_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'サーバーノーティス実行フラグ',
`notification_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1:サーバーダウン;2:パッチ;3：手動無効化',
`server_down_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
`server_up_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
`update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
PRIMARY KEY (`mcu_notification_key`),
KEY `server_key` (`server_key`)
)ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;