#! /bin/sh

HOSTNAME="`hostname`"
LOGFILE="/var/log/${HOSTNAME}.log"
SCRIPT_NAME="`basename $0`"
MONIT_LSYNCD_CONF="/etc/monit.d/lsyncd.rc"
MCU_SERVERS=(tvcbmob01.mew.co.jp)
RC=0

LOG()
{
    STATUS=$1
    MSG=$2
    echo [`date '+%Y/%m/%d %T'`] [${STATUS}] "${HOSTNAME}" "${SCRIPT_NAME}" "${MSG}" >> ${LOGFILE}
}

# monit stop
/bin/sed -i '/^mo:2345:respawn/s/^/#/' /etc/inittab
/sbin/init q

# mcuWeb restart
/etc/init.d/mcuWeb stop
/etc/init.d/mcuWeb start

# mcuGold restart & RegisterConferenceManager
/data/services/vcube_mcu/mcuGold_restart.sh

# lsyncd stop (other mcu_server)
for SERVER in ${MCU_SERVERS[@]}
do
    if [ "${SERVER}" != "`hostname`" ]; then
        ssh ${SERVER} /data/services/vcube_mcu/lsyncd_stop.sh
        if [ $? -ne 0 ]; then
            LOG "err" "\"ssh(${SERVER}:lsyncd_stop failed.\""
            RC=1
        fi
    fi
done

# lsyncd start
#if [ ! -f ${MONIT_LSYNCD_CONF} ]; then
#    \/bin/mv ${MONIT_LSYNCD_CONF}.stop ${MONIT_LSYNCD_CONF}
#    /sbin/service lsyncd start
#fi
#
#if [ ! -f /var/run/lsyncd.pid ]; then
#    LOG "err" "\"lsyncd start failed.\""
#    RC=1
#fi

# monit start
/bin/sed -i '/monit$/s/^#//' /etc/inittab
/sbin/init q

exit ${RC}