#!/bin/bash
# $Header: /home/cvsroot/egen_usr/etc/rc.d/init.d/mgmtveth,v 1.9 2007/09/21 09:55:29 array Exp $

# Bring up egenera internal management interface
#set -x

INTERFACE=veth31
CONFIG=/proc/egenera/Egenera_Config/ifcfg-${INTERFACE}

if [ ! -f ${CONFIG} ] ; then
	if [ -f /proc/fs/egenfs ] ; then
		echo "mount" >/proc/fs/egenfs
	fi

	if [ ! -f ${CONFIG} ] ; then
		echo "Error accessing file ${CONFIG}"
		exit 0;
	fi
fi

function is_suse() {
	if [ -f /etc/SuSE-release -o -f /etc/UnitedLinux-release ]; then
		return 0
	elif [ -f /proc/version ]; then
		if grep -q '.suse.de' /proc/version; then
			return 0
		fi
	fi
	return 1
}

function is_redhat() {
	if [ -f /etc/redhat-release ]; then
		return 0
	elif [ -f /proc/version ]; then
		if grep -q 'Red Hat' /proc/version ||  grep -q '.redhat.com' /proc/version; then
			return 0
		fi
	fi
	return 1
}

function is_hypervisor() {
	if [ -f /boot/.pbroot_manifest ]; then
		if grep -q '^ *root_type *= *hypervisor *$' /boot/.pbroot_manifest; then
			return 0
		fi
	fi
	return 1
}

if is_suse; then
	. /etc/rc.status

	# Source function library.
	cd /etc/sysconfig/network
	[ -f scripts/functions ] && . scripts/functions
	[ -f config ] && . config

function echo_status() {
	if [ -z "$1" ]; then
		rc_failed 1
	else
		rc_failed "$1"
	fi
	rc_status -v
}
elif is_redhat; then
	# Source function library.
	. /etc/rc.d/init.d/functions

function echo_status() {
	if [ -z "$1" ]; then
		failure
	elif [ $1 -eq 0 ] ; then
		success
	else
		failure
	fi
}

else 
# can't determin what the OS is
function echo_status() {
	if [ -z "$1" ]; then
		echo -e "\tfailed"
	elif [ $1 -eq 0 ] ; then
		echo -e "\tdone"
	else
		echo -e "\tfailed"
	fi
}

fi


# source the config file
. ${CONFIG}

stop (){
	
	/sbin/ip route del ${NETWORK}/24 dev ${INTERFACE}
	/sbin/route del -net 224.224.224.224 netmask 255.255.255.255 dev ${INTERFACE}
	/sbin/ifconfig ${INTERFACE} down
	RETVAL=$?

	# Start the NTP server on hypervisors
	if is_hypervisor; then
		if [ -x /etc/init.d/ntpd ]; then
			/etc/init.d/ntpd stop
		fi
	fi

	echo_status $RETVAL

	return $RETVAL
}


start (){
	
	/sbin/ifconfig ${INTERFACE} ${IPADDR} netmask ${NETMASK} broadcast ${BROADCAST} mtu ${MTU} up
	RETVAL=$?

	/sbin/ip route replace ${NETWORK}/24 dev ${INTERFACE}
	/sbin/route add -net 224.224.224.224 netmask 255.255.255.255 dev ${INTERFACE}

	# SuSE doesn't apply sysctl.conf during system boot
	# So we do it here.
	sysctl="/sbin/sysctl"
	if [ ! -x $sysctl ]; then
		sysctl="sysctl"
	fi
	sysctlf="/etc/sysctl.conf"
	if [ ! -f $sysctlf ]; then
		sysctl=""
	fi
	$sysctl -e -p $sysctlf >/dev/null 2>&1

	# Start the NTP server on hypervisors
	if is_hypervisor; then
		if [ -x /etc/init.d/ntpd ]; then
			if [ -f /etc/ntp.conf ]; then
				mv -f /etc/ntp.conf /etc/ntp.conf.orig
			fi
			echo server `echo $NETWORK | sed -e 's^\.0$^.30^'` > /etc/ntp.conf
			echo server `echo $NETWORK | sed -e 's^\.0$^.31^'` >> /etc/ntp.conf
			echo driftfile /var/lib/ntp/drift >> /etc/ntp.conf
			echo keys /etc/ntp/keys >> /etc/ntp.conf
			/etc/init.d/ntpd start
		fi
	fi

	echo_status $RETVAL

	return $RETVAL
}

do_hostname(){

	HOSTNAME=`/bin/hostname`
	if [ -z ${HOSTNAME} -o ${HOSTNAME} = "(none)" -o ${HOSTNAME} = "localhost" -o ${HOSTNAME} = "localhost.localdomain" -o ${HOSTNAME} = "linux" ] ; then
	if [ -f /proc/egenera/Egenera_Config/network ] ; then
	. /proc/egenera/Egenera_Config/network
	/bin/hostname ${HOSTNAME}
	fi
fi
}
 

case "$1" in 

	start)
	echo -n $"Starting Egenera management interface: "
	do_hostname
	start
	;;
	stop)
	echo -n $"Stopping Egenera management interface: "
	stop
	;;

	restart)
	echo -n $"Stopping Egenera management interface: "
	stop
	echo -n $"Starting Egenera management interface: "
	start
	;;
	
	*)
	echo $"Usage: $0 {start|stop|restart}"
	exit 1
	;;
esac

