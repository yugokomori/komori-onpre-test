<?php
require_once("classes/AppFrame.class.php");
require_once("classes/mgm/MGM_Auth.class.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/dbi/member.dbi.php");
require_once("classes/dbi/room.dbi.php");

class AppOutbound extends AppFrame {

    var $_user_key = null;
    var $_offset = null;
    var $_name_space = null;
    var $customerMessageTable = null;

    function init()
    {
        $this->_name_space = md5(__FILE__);
    }

    function action_outbound_login()
    {
        //パスワード入力ハンドル
        $message = null;
        $outbound_id = $this->request->get("outbound_id");
        if(!$outbound_id){
            $message = OUTBOUND_ID_NOTFOUND;
        } elseif(!$room_info = $this->_isExist($outbound_id)){
            $message = OUTBOUND_ID_NOT_EXIST;
        } elseif($room_info["room_status"] == 0){
            $message = OUTBOUND_ID_NOT_EXIST;
        }
        //エラーがなければマイページへ
        if($message){
            //$this->template->assign('message', $message);
            $this->display('user/404_error.t.html');
            exit;
        }else{
            $this->action_show_mypage($room_info);
        }
    }

    function _isExist($outbound_id)
    {
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );

        if ( ! $server_info = $obj_MGMClass->getRelationDsn( $outbound_id, "outbound" ) ){
            return false;
        }
        $this->session->set("server_info", $server_info );
        $obj_Room = new RoomTable( $this->get_dsn() );
        $where = "outbound_id = '".$outbound_id."'".
                 " AND room_status != -1";
        $room_info = $obj_Room->getRow($where);
        //memberとのリレーションチェック
        require_once("classes/dbi/member_room_relation.dbi.php");
        $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
        $where = "room_key = '".addslashes($room_info["room_key"])."'";
        $room_relation = $objRoomRelation->getRow($where);
        if ($room_info && $room_relation) {
            $obj_Member = new MemberTable( $this->get_dsn() );
            $where = "outbound_id = '".addslashes($outbound_id)."'".
                     " AND member_key = ".$room_relation["member_key"];
            $member_info = $obj_Member->getRow($where);
            $this->logger->debug("room_info",__FILE__,__LINE__,$room_info);

            if($member_info){
                $addition = unserialize($room_info["addition"]);
                $room_info["email"] = $addition["email"];
                $room_info["title"] = $addition["title"];
                $room_info["room_image"] = $addition["room_image"];
                $room_info["outbound"] = $member_info["outbound_id"];
                return $room_info;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    function action_show_mypage($room_info, $message = "", $comment = "")
    {

        $this->template->assign("room", $room_info);
        $this->template->assign("outbound", $room_info["outbound"]);
        $this->template->assign("message", $message);
        $this->template->assign("comment", $comment);
        $this->session->set("login_type", 'customer');
        $this->session->set("service_mode", 'sales');

        $this->set_submit_key($this->_name_space);

        $template_file = $this->_check_template($room_info["outbound_id"]);
        if (!$template_file){
            $template_file = "outbound/mypage.t.html";
        }

        $this->logger->trace(__FUNCTION__,__FILE__,__LINE__, array(
            $this->template->template_dir."/".$template_file,
            ));
        $this->display($template_file);
    }

    //テンプレートファイルの有無を判別
    function _check_template($outbound_id){

        $this->_set_template_dir($this->_lang);
        $template_file = "outbound/mypage_".$outbound_id.".t.html";
        if (!file_exists($this->template->template_dir.$this->_lang."/".$template_file)) {
            if (!file_exists($this->template->template_dir.$this->_lang."/".$template_file)) {
                return false;
            }
        }
        return $template_file;
    }

    function nocache_header() {
        // 日付が過去
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        // 常に修正されている
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        // HTTP/1.1
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        // HTTP/1.0
        header("Pragma: no-cache");
    }

    function action_enter()
    {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        // 会議スタート後待機画面
        $this->display('outbound/done.t.html');
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
    }

    /**
     * 会議スタート
     */
    function action_meeting_start() {

        $this->session->remove("meeting_auth_flg");
        require_once("classes/N2MY_Meeting.class.php");
        $obj_N2MY_Meeting = new N2MY_Meeting($this->get_dsn());
        // 現在利用可能な会議情報取得
        $room_key = $this->request->get("room_key");
        if (!$room_key) {
            return false;
            exit;
        }
        $this->logger2->debug($room_key);
        // ログインタイプ取得
        $login_type = $this->session->get("login_type");

        // ユーザ情報
        require_once("classes/dbi/user.dbi.php");
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $room_info = $obj_N2MY_Account->getRoomInfo( $room_key );

        if (!$room_info) {
            $message = array(
                "title" => $this->get_message("MEETING_START", "using_title"),
                "text" => $this->get_message("MEETING_START", "using_text"),
                "back_url" => "javascript:window.close();",
                "back_url_label" => $this->get_message("MEETING_START", "using_back_url_label")
                );
            $this->logger2->debug($message);
            $this->template->assign("message", $message);
            return $this->display('outbound/error.t.html');
        }
        $obj_User = new UserTable($this->get_dsn());
        $user_info = $obj_User->getRow("user_key = ".$room_info["room_info"]["user_key"]);
        $this->session->set("user_info", $user_info);

        //資料共有部屋か確認
        if ($room_info["options"]["whiteboard"]) {
            $login_type = "whiteboard_customer";
        }

        //地域情報
        $country_key = $this->request->get("country");
        if ($country_key == "auto") {
          $result_country_key = $this->_get_country_key($country_key);
          $country_key = $result_country_key;
        }

        if (!$country_key) {
          $country_key = $this->session->get("country_key");
        }

        // 地域が特定できない場合は利用可能な地域をデフォルトとする
        $country_list = $this->get_country_list();

        if (!array_key_exists($country_key, $country_list)) {
          $_country_data = array_shift($country_list);
          $country_key = $_country_data["country_key"];
        }
        $this->session->set("country_key", $country_key);
        // 会議キー
        $meeting_key = $this->request->get("meeting_key");
        // 会議情報取得
        $meeting_info = $obj_N2MY_Meeting->getMeeting($room_key, $login_type, $meeting_key);
        if ($meeting_info["reservation_session"] || !$meeting_info) {
            $reservationInfo = $this->obj_Reservation->getRow(sprintf("reservation_session='%s'", $meeting_info["reservation_session"]));
        }
        if (!$meeting_info) {
            $message = array(
                "title" => $this->get_message("MEETING_START", "using_title"),
                "text" => $this->get_message("MEETING_START", "using_text"),
                "back_url" => "javascript:window.close();",
                "back_url_label" => $this->get_message("MEETING_START", "using_back_url_label")
                );
            $this->logger2->debug($message);
            $this->template->assign("message", $message);
            return $this->display('outbound/error.t.html');
        }
        $meeting_key = $meeting_info["meeting_key"];

        // オプション指定
        $options = array(
            "meeting_name" => $meeting_info["meeting_name"],
            "user_key"     => $user_info["user_key"],
            "start_time"   => $meeting_info["start_time"],
            "end_time"     => $meeting_info["end_time"],
            "country_id"   => $country_key,
            "password"     => $meeting_info["meeting_password"]
        );
        // 現在の部屋のオプションで会議更新
        $core_session = $obj_N2MY_Meeting->createMeeting($room_key, $meeting_key, $options);
        // モバイル判別
            if (strpos($_SERVER['HTTP_USER_AGENT'], "Android") || strpos($_SERVER['HTTP_USER_AGENT'],"iPhone") || strpos($_SERVER['HTTP_USER_AGENT'],"iPod") || strpos($_SERVER['HTTP_USER_AGENT'],"iPad")) {
                require_once( "classes/dbi/meeting.dbi.php" );
                $obj_Meetng = new MeetingTable($this->get_dsn());
                $where = "meeting_session_id = '".addslashes($core_session)."'" .
                        " AND user_key = '".$user_info["user_key"]."'" .
                        " AND is_active = 1" .
                " AND is_deleted = 0";
                $meeting_data = $obj_Meetng->getRow($where);
                $this->session->set('invited_meeting_ticket', $meeting_info['meeting_ticket']);
                $outbound_url = N2MY_BASE_URL."outbound/".$room_info["room_info"]["outbound_id"];
                $url    = $this->config->get("N2MY","mobile_protcol_sales", 'vcube-sales')."://join?session=".session_id()."&room_id=".$meeting_data["room_key"]."&entrypoint=".N2MY_BASE_URL."&pin_code=".$meeting_data['pin_cd']."&callbackurl=".urlencode($outbound_url);
                $this->logger2->info($url);
                header("Location: ".$url);
                exit();
            }
        // ユーザごとのオプションを取得
        $type        = $this->request->get("type");
        if ($room_info["options"]["whiteboard"]) {
            $type = "whiteboard_customer";
        }
        // マルチカメラが無効の場合はtypeをnormalに変更

        // ホワイトボードアップロードメールアドレス
        $mail_upload_address = ($this->config->get("N2MY", "mail_wb_host")) ? $room_key."@".$this->config->get("N2MY", "mail_wb_host") : "";

        /*typeを増やした場合の影響範囲がはかり知れないので元にもどします。。*/
        if ($this->session->get("invitedGuest")) $login_type = "invite";
        $user_options = array(
            "user_key"  => $user_info["user_key"],
            "name"      => "",
            "participant_email"         => $_COOKIE["personal_email_secure"],
            "participant_station"       => $this->request->get("station")?$this->request->get("station"):$_COOKIE["personal_station"],
            "narrow"    => $this->request->get("is_narrow"),
            "lang"      => $this->session->get("lang"),
            "skin_type" => $this->request->get("skin_type", ""),
            "mode"      => "",
            "role"      => $login_type,
            "mail_upload_address"       => $mail_upload_address,
            "account_model"       => $user_info["account_model"],
            );
        // 会議入室
        $meetingDetail = $obj_N2MY_Meeting->startMeeting($core_session, $type, $user_options);
        if ( !$meetingDetail ) {
            $message["title"] = MEETING_START_ERROR_TITLE;
            $message["body"] = MEETING_START_ERROR_BODY;
            return $this->render_valid($message);
        }

        //リダイレクト先の core/htdocs/sercie/*　の処理を直接書いています。
        $this->session->set( "room_key", $room_key );
        $this->session->set( "type", $meetingDetail["participant_type_name"] );
        $this->session->set( "meeting_key", $meetingDetail["meeting_key"] );
        $this->session->set( "meeting_type", $type );
        $this->session->set( "participant_key", $meetingDetail["participant_key"] );
        $this->session->set( "participant_name", "" );
        $this->session->set( "mail_upload_address", $mail_upload_address );
        $this->session->set( "fl_ver", "as3" );
        $this->session->set( "display_size", "16to9" );
        $this->session->set( "meeting_version", $user_info["meeting_version"] );
        $this->session->set( "reload_type", 'normal');
        // 参加者情報
        $this->session->set( "user_agent_ticket", uniqid());
        // 入室時のログ
        $this->logger2->debug(array(
            "room_key" => $room_key,
            "type" => $meetingDetail["participant_type_name"],
            "meeting_key" => $meetingDetail["meeting_key"],
            "meeting_type" => $type,
            "participant_key" => $meetingDetail["participant_key"],
            "participant_name" => "",
            ));
        // 会議入室
        $redirect_url = N2MY_BASE_URL."outbound/?action_meeting_display";
        if (!$room_info["room_info"]["room_password"]) {
            header( "Location: ".$redirect_url);
        } else {
            $this->session->set("redirect_url", $redirect_url, $this->_name_space);
            $params = array(
                "action_room_auth_login" => "",
                "room_key"               => $room_key,
                "ns"                     => $this->_name_space
                );
            $url = $this->get_redirect_url("outbound/", $params);
            $this->logger->info(__FUNCTION__."#end", __FILE__, __LINE__,$url);
            header("Location: ".$url);
            exit;
        }
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array($room_key, $meeting_info));
        return true;
    }

    function action_meeting_display()
    {
        $this->render_meeting_view();
    }

    private function render_meeting_view()
    {
        $type = $this->session->get( "type" );
        $this->template->assign('charset', "UTF-8" );

        // 会議情報取得
        $user_info = $this->session->get("user_info");
        $meeting_key = $this->session->get("meeting_key");
        require_once( "classes/dbi/meeting.dbi.php" );
        $obj_Meetng = new MeetingTable($this->get_dsn());
        $where = "meeting_key = '".addslashes($meeting_key)."'" .
                " AND user_key = '".$user_info["user_key"]."'" .
                " AND is_active = 1" .
                " AND is_deleted = 0";
        $meeting_info = $obj_Meetng->getRow($where);
        $this->template->assign('meeting_info' , $meeting_info );
        $obj_Room = new RoomTable($this->get_dsn());
        $room_info = $obj_Room->getRow("room_key = '".addslashes($meeting_info['room_key'])."'");
        $this->template->assign("room_info" , $room_info);

        //Skinsの選択を追加
        $skinName = $user_info["user_logo"] ? "SkinsFuji" : "Skins";
        $this->template->assign('skinName' , $skinName );
        $this->template->assign('locale' , $this->get_language() );

        $meeting_version = $this->session->get("meeting_version");
        if ($meeting_version >= "4.7.0.0") {
           $version = "4.7.0.0";
        } else {
           $version = "4.6.5.0";
        }
        $this->template->assign('meeting_version' , $version );

        $content_delivery_base_url = $this->get_content_delivery_base_url();
        $this->template->assign("content_delivery_base_url", $content_delivery_base_url);

        // eco canceler
        $certificate = substr(md5(uniqid(rand(), true)), 1, 16);
        $oneTime = array(
                    "certificate"    =>    $certificate,
                    "createtime"    =>    time());
        $this->session->set("oneTime", $oneTime);
        $this->template->assign('certificateId', $certificate );
        $this->template->assign('apiUrl', '/services/api.php?action_issue_certificate');

        // UserAgent更新
        $this->template->assign('user_agent_ticket' , $this->session->get("user_agent_ticket") );
        // ディスプレイサイズ指定
        $this->template->assign('display_size', htmlspecialchars($this->session->get("display_size"), ENT_QUOTES));
        if ($this->session->get("fl_ver") == "as3") {
            $this->template->assign('meeting_type' , htmlspecialchars($this->session->get("meeting_type"),ENT_QUOTES) );
            $this->template->assign('participant_name' , $this->session->get("participant_name") );
            // V-Cube Meetingのバージョンが違う場合、キャッシュしたデータを使わせない。
            $this->template->assign('cachebuster' , $this->get_meeting_version());
            $this->display( sprintf( 'core/%s/meeting_as3.t.html', $type ) );
        } else {
            $this->display( sprintf( 'core/%s/meeting_base.t.html', $type ) );
        }
    }

    function action_room_auth_login ($message = "") {
        $this->logger->trace(__FUNCTION__."#start",__FILE__,__LINE__);
        $this->template->assign('message', $message);
        $this->template->assign('ns', $this->request->get("ns"));
        $room_key = $this->request->get("room_key");
        $this->template->assign('room_key', $room_key);
        $this->display('outbound/auth.t.html');
    }

    function action_room_pw_check() {
        $this->logger->trace(__FUNCTION__."#start",__FILE__,__LINE__);
        $room_key = $this->request->get("room_key");
        $room_pw = $this->request->get("room_pw");
        if (!$room_key || !$room_pw) {
            $err_msg = RESERVATION_ERROR_NOTMUCTH_AUTH_PW;
        }
        $obj_Room = new RoomTable( $this->get_dsn() );
        $check_flg = $obj_Room->checkPassword($room_key, $room_pw);
        if ($check_flg == true && !$err_msg) {
            // 指定された名前空間のリダイレクト先取得
            $name_space = $this->request->get("ns");
            $redirect_url = $this->session->get("redirect_url", $name_space);
            // セッションから消去
            $this->session->remove("redirect_url", $name_space);
            $this->logger->trace(__FUNCTION__."#url",__FILE__,__LINE__,$redirect_url);
            header("Location: ".$redirect_url);
            return;
        } else {
            $err_msg = RESERVATION_ERROR_NOTMUCTH_AUTH_PW;
        }
        return $this->action_room_auth_login($err_msg);
    }

    //一言メッセージ送信フォーム表示
    function action_show_message($message = "")
    {
        $this->set_submit_key($this->_name_space);
        $outbound = $this->request->get("outbound");
        $room_info = $this->_isExist($outbound);
        $this->template->assign('room', $room_info);
        $this->template->assign('outbound', $outbound);
        $this->template->assign('comment', $this->session->get("comment"));
        $this->template->assign('message', $message);
        $this->display('outbound/message.t.html');
    }

    //一言メッセージ送信
    function action_send_message()
    {
        $request = $this->request->getAll();
        $this->logger->info("request",__FILE__,__LINE__,$request);
        $outbound = $request['outbound'];
        if (!$outbound) {
            $message .= "Error";
            $this->action_show_message($message);
            exit;
        }
        $room_info = $this->_isExist($outbound);
        $message = "";
        if (mb_strlen($request['message_company']) > 30) {
            $message .= "<li>".OUTBOUND_ERROR_MESSAGE_COMPANY_OVERLENGTH. "</li>";
        }
        if(!$request['message_name']){
            $message .= "<li>".OUTBOUND_ERROR_MESSAGE_NAME . "</li>";
        }elseif (mb_strlen($request['message_name']) > 20) {
            $message .= "<li>".OUTBOUND_ERROR_MESSAGE_NAME_OVERLENGTH . "</li>";
        }
        if(!$request['message_email']){
            $message .= "<li>".OUTBOUND_ERROR_MESSAGE_EMAIL . "</li>";
        }
        //E-mail
        if($request['message_email'] && !EZValidator::valid_email($request['message_email'])){
            $message .= "<li>".OUTBOUND_ERROR_INVALID_MESSAGE_EMAIL . "</li>";
        }
        if(!$request['message_comment']){
            $message .= "<li>".OUTBOUND_ERROR_MESSAGE_COMMENT . "</li>";
        }elseif (mb_strlen($request['message_comment']) > 200) {
            $message .= "<li>".OUTBOUND_ERROR_MESSAGE_COMMENT_OVERLENGTH ."</li>";
        }
        if($message){
            $this->session->set("comment", $request);
            $this->request->set("room_key", $request["room_key"]);
            $this->action_show_message($message);
            exit;
        }
        
        if ($this->check_submit_key($this->_name_space)) {
            // 入力チェック
            //メール送信
            if ($room_info["email"] && $this->config->get("N2MY", "message_mail") != 1) {
                foreach($request as $key => $value){
                        $this->template->assign($key, $value);
                }
                $lang = $this->session->get("lang");
                require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
                $cd_lang = EZLanguage::getLangCd($lang);
                
                require_once ("lib/EZLib/EZMail/EZSmtp.class.php");
                $mail = new EZSmtp(null, $lang_cd, "UTF-8"); 
                $body = $this->fetch( sprintf( '%s/mail/sales/outbound_message.t.txt', $cd_lang));
                //$this->logger2->debug($body);
                $mail->setFrom(USER_ASK_FROM, $request['message_name']);
                $mail->setSubject(OUTBOUND_MESSAGE_MAIL_SUBJECT);
                $mail->setReturnPath($request['message_email']);
                $mail->setTo($room_info["email"]);
                $mail->setBody($body);
                $mail->send();
            }

            //DBに情報を追加
            $message_data = array();
            $message_data['user_key']         = $room_info['user_key'];
            $message_data['room_key']         = $room_info['room_key'];
            $message_data['customer_name']    = $request['message_name'];
            $message_data['customer_company'] = $request['message_company'];
            $message_data['customer_email']   = $request['message_email'];
            $message_data['customer_comment'] = $request['message_comment'];
            $message_data['customer_ip']      = $_SERVER[REMOTE_ADDR];
            $message_data['status']           = 1;
            $this->logger->trace("message_data",__FILE__,__LINE__,$message_data);
            require_once ("classes/dbi/customer_message.dbi.php");
            $obj_CustomerMessage = new CustomerMessageTable($this->get_dsn());
            $obj_CustomerMessage->add($message_data);

            $message = "<li>".OUTBOUND_MESSAGE_COMPLETE."</li>";
        }
        $this->session->remove("comment");
        $this->action_send_message_complete();
    }

    function action_send_message_complete() {
        $this->display('outbound/message_done.t.html');
    }

    //一言メッセージ送信フォーム表示
    function action_sharing_download()
    {
        $this->display('outbound/sharing.t.html');
    }

    function action_set_language() {
      // 有効期限
      $limit_time = time() + 365 * 24 * 3600;
      // 言語コード
      $lang = $this->request->get("lang");
      $outbound_id = $this->request->get("outbound");
      if ($lang) {
        setcookie("lang", $lang, N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        $this->session->set("lang", $lang);
      }
      // 地域コード
      $_country_key = $this->request->get("country");
      $result_country_key = $this->_get_country_key($_country_key);
      if (!$result_country_key) {
        // 地域コードの不正な指定を防ぐ
        $country_list = $this->get_country_list();
        if (!array_key_exists($_country_key, $country_list)) {
          $country_keys = array_keys($country_list);
          $_country_key = $country_keys[0];
          $this->logger->warn(__FUNCTION__, __FILE__, __LINE__,$_country_key);
        }
        foreach ($country_list as $country_key => $country_row) {
          if ($_country_key == $country_key) {
            $this->session->set('country_key', $country_row["country_key"]);
            $result_country_key = $_country_key;
          }
        }
      } else {
        $this->session->set('country_key', $result_country_key);
      }
      setcookie("country", $result_country_key, N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
      $this->session->set("country_key", $result_country_key);
      $this->session->set('selected_country_key', $_country_key);
      setcookie("selected_country_key", $_country_key, N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);

    header("Location: /outbound/".$outbound_id);
    }

    private function render_valid()
    {
        $this->display( 'outbound/error.t.html' );
    }

    private function get_content_delivery_base_url() {
        $protocol = '';
        $content_delivery_base_url = '';
        $country_key = $this->session->get("country_key");

        if($this->config->get("CONTENT_DELIVERY", "is_enabled", false)) {
            $country_key = $this->session->get("country_key");
            // 中国からの入室の場合、データの参照元をCDNからではなくBravからに設定する。
            if($country_key == "cn") {
                $content_delivery_host = $this->config->get("CONTENT_DELIVERY_HOST", "cn", "");
                if(strlen($content_delivery_host)) {
                    $protocol = isset($_SERVER["HTTPS"]) ? 'https://' : 'http://';
                }
                $content_delivery_base_url = $protocol . $content_delivery_host;
                $meeting_file_dir = $this->config->get("CONTENT_DELIVERY", "meeting_file_dir", "");
                if(strlen($meeting_file_dir)) {
                    $content_delivery_base_url .= '/' . $meeting_file_dir;
                }
            }
        }
        return $content_delivery_base_url;
    }

    private function get_meeting_version() {
        $version_file = N2MY_APP_DIR."version.dat";
        if ($fp = fopen($version_file, "r")) {
            $version = trim(fgets($fp));
        }
        return $version;
    }
}

$main = new AppOutbound();
$main->execute();

?>
