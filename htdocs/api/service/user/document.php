<?php
/*
 * Created on 2008/02/14
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
require_once("classes/N2MY_Api.class.php");

class N2MY_Meeting_Document_API extends N2MY_API
{
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        // SSL対応
        header('Pragma:');
        $this->_name_space = md5(__FILE__);
    }

    /**
     * 認証処理
     *
     * @param
     * @return
     */
    function auth()
    {
        $this->checkAuthorization();
    }

    /**
     * 資料一覧
     *
     * @param string $room_key ルームキー
     * @param string reservation_session 予約セッション
     * @param int offset 開始件数 (デフォルト null)
     * @param int limit 表示件数 (デフォルト null)
     * @param string sort_key ソート基準 (デフォルト ソート番号順)
     * @param string sort_type 降順、昇順 (デフォルト 昇順)
     * @return string $output 資料キー、資料名、資料タイプ、資料サイズ、ソート番号をxmlで出力
     */
    function action_get_document()
    {
        require_once ("classes/N2MY_Reservation.class.php");
        require_once("classes/dbi/reservation.dbi.php");

        $core_url    = $this->config->get("CORE","base_url");
        $provider_id = $this->config->get("CORE","provider_id");

        $reservation     = new N2MY_Reservation($this->get_dsn());

        $room_key = $this->request->get("room_key");
        $reservation_session = $this->request->get("reservation_session");
        $offset = $this->request->get("offset");
        $limit = $this->request->get("limit");
        $sort_key = $this->request->get("sort_key");
        $sort_type = $this->request->get("sort_type");

        // 予約情報取得
        $reservation_obj = new ReservationTable($this->get_dsn());
        $where = "reservation_session = '".$reservation_session."'";
        $row = $reservation_obj->getRow($where);
        if ($row && $room_key) {
            // ファイル一覧取得
            $documents = $reservation->getDocument($provider_id, $room_key, $row["meeting_key"]);

            $result = array(
                "status" => 1,
                "data" => array(
                        "documents" => $documents,
                        ),
            );
            $output = $this->output($result);
            return true;
        }
        $result = array(
            "status" => 0,
            "data" => array(
                    "reservation_session" => $reservation_session,
                    ),
        );
        $output = $this->output($result);
        return true;
    }

    /**
     * 資料追加
     *
     * @param string room_key ルームキー
     * @param string reservation_session 予約セッション
     * @param string file ファイル情報
     * @param string file_name ファイル名
     * @param string file_type ファイルの種類
     * @return boolean
     */
    function action_add_document()
    {
        require_once("classes/dbi/reservation.dbi.php");

        $room_key            = $this->request->get("room_key");
        $reservation_session = $this->request->get("reservation_session");
        $document_name       = $this->request->get("document_name");
        // 予約情報取得
        $reservation_obj = new ReservationTable($this->get_dsn());
        $where = "reservation_session = '".$reservation_session."'";
        $row = $reservation_obj->getRow($where);
        $this->logger->info("user_info",__FILE__,__LINE__,$this->session->get("user_info"));

        if ($row && $room_key) {
            $data["label"] = $document_name;
            $data["file"]  = $_FILES["file"];
            $data["file"]["extension"] = $this->get_extension($data["file"]["name"]);
            $this->session->set("upfile_data", $data, $this->_name_space);
            $rules = array(
                "label" => array("required" => true),
                "file"  => array(
                    "required" => true,
                    "file_upload" => true),
                );
            if ($data["file"]["error"] == 0 && $data["file"]["size"] > 0) {
                $provider_id = $this->config->get("CORE","provider_id");
                $post_data["document_path"]     = $provider_id."/".$room_key."/".$row["meeting_key"];
                $post_data["extension"]         = $data["file"]["extension"];
                $post_data["name"]              = htmlspecialchars($data["label"]);
                $post_data["sort"]              = "";
                $post_data["file"]              = "@".$data["file"]["tmp_name"];
                $post_data["document_id"]       = "";
                $ch = @curl_init();
                $url = $this->get_core_url("/api/document/accept2.php");
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1 );
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $postResult = @curl_exec($ch);
                $this->logger->info(__FUNCTION__, __FILE__, __LINE__, array($url, $post_data, $postResult));
                // CURLエラー
                if (curl_errno($ch)) {
                    $this->logger->error(__FUNCTION__, __FILE__, __LINE__, curl_error($ch));
                    $status = 0;
                }
            }
            $result = array(
                "status" => 1,
                "data" => array(
                        "reservation_session"       => $reservation_session,
                        ),
            );
            $output = $this->output($result);
            exit;
        }

        $result = array(
            "status" => 0,
            "data" => array(
                    "room_key"       => $room_key,
                    "reservation_session"       => $reservation_session,
                    ),
        );
        $output = $this->output($result);

    }

    /**
     * 資料名変更
     *
     * @param string room_key ルームキー
     * @param string document_id ドキュメントID
     * @param string document_name ドキュメント名
     * @return boolean
     */
    function action_update_document()
    {
        // 資料名変更
        $this->logger->info(__FUNCTION__,__FILE__,__LINE__,$this->request->getAll());
        $status = 0;
        if ($this->request->get("document_id")) {
            $param = array(
                "document_id" => $this->request->get("document_id"),
                "name" => htmlspecialchars($this->request->get("document_name"))
            );
            $this->logger->info(__FUNCTION__,__FILE__,__LINE__,$param);
            $url = $this->get_core_url("api/document/update2.php", $param);
            $ret = $this->api_execute($url);
            $status = 1;
        }
        $result = array(
            "status" => $status,
            "data" => array(
                    "document_id"       => $this->request->get("document_id"),
                    ),
        );
        $output = $this->output($result);
    }

    /**
     * 資料削除
     *
     * @param string room_key ルームキー
     * @param string document_id ドキュメントID
     * @return boolean
     */
    function action_delete_document()
    {
        // 資料一覧取得
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,$this->request->getAll());
        $status = 0;
        if ($this->request->get("document_id")) {
            $param = array(
                "document_id" => $this->request->get("document_id"),
            );
            $url = $this->get_core_url("api/document/delete2.php", $param);
            $ret = $this->api_execute($url);
            $status = 1;
        }
        $result = array(
            "status" => $status,
            "data" => array(
                    "document_id"       => $this->request->get("document_id"),
                    ),
        );
        $output = $this->output($result);
    }

    /**
     * 資料ソート順を上げる
     *
     * @param string room_key ルームキー
     * @param int meeting_key 会議キー
     * @param string document_id ドキュメントID
     * @return
     */
    function action_up_document()
    {
        require_once("classes/dbi/reservation.dbi.php");

        $room_key = $this->request->get("room_key");
        $reservation_session = $this->request->get("reservation_session");
        $document_no = $this->request->get("document_no");
        $status = 0;
        if ($room_key && $reservation_session && $document_no) {

            // 予約情報取得
            $reservation_obj = new ReservationTable($this->get_dsn());
            $where = "reservation_session = '".$reservation_session."'";
            $row = $reservation_obj->getRow($where);

            $provider_id = $this->config->get("CORE","provider_id");
            $document_path = $provider_id."/".$room_key."/".$row["meeting_key"];
            $param = array(
                "document_path" => $document_path,
                "no" => $document_no,
                "sort_type" => "up"
            );
            $url = $this->get_core_url("api/document/sort2.php", $param);
            $ret = $this->api_execute($url);
            $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array($url, $ret));
            $status = 1;
        }
        $result = array(
            "status" => $status,
            "data" => array(
                    "reservation_session" => $reservation_session,
                    "room_key"            => $room_key,
                    "document_no"         => $document_no,
                    ),
        );
        $output = $this->output($result);
    }

    /**
     * 資料ソート順を下げる
     *
     * @param string room_key ルームキー
     * @param string meeting_key 会議キー
     * @param int document_id ドキュメントID
     * @return
     */
    function action_down_document()
    {
        require_once("classes/dbi/reservation.dbi.php");

        $room_key = $this->request->get("room_key");
        $reservation_session = $this->request->get("reservation_session");
        $document_no = $this->request->get("document_no");
        $status = 0;
        if ($room_key && $reservation_session && $document_no) {

            // 予約情報取得
            $reservation_obj = new ReservationTable($this->get_dsn());
            $where = "reservation_session = '".$reservation_session."'";
            $row = $reservation_obj->getRow($where);

            $provider_id = $this->config->get("CORE","provider_id");
            $document_path = $provider_id."/".$room_key."/".$row["meeting_key"];
            $param = array(
                "document_path" => $document_path,
                "no" => $document_no,
                "sort_type" => "down"
            );
            $url = $this->get_core_url("api/document/sort2.php", $param);
            $ret = $this->api_execute($url);
            $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array($url, $ret));
            $status = 1;
        }
        $result = array(
            "status" => $status,
            "data" => array(
                    "reservation_session" => $reservation_session,
                    "room_key"            => $room_key,
                    "document_no"         => $document_no,
                    ),
        );
        $output = $this->output($result);
    }

    function get_extension($filename)
    {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        $ret = "";
        switch ($this->get_file_type($filename)) {
            case "gif":
                $ret = "gif";
                break;
            case "pjpeg":
            case "jpeg":
                $ret = "jpg";
                break;
            case "png":
                $ret = "png";
                break;
            case "pdf":
                $ret = "pdf";
                break;
            case "excel":
                $ret = "xls";
                break;
            case "word":
                $ret = "doc";
                break;
            case "ppp":
                $ret = "ppt";
                break;
        }
        $_format_list = $this->config->get("N2MY", "convert_format");
        $format_list = split(",", $_format_list);
        if (!in_array($ret, $format_list)) {
            $ret = "";
        }
        return $ret;
    }

    /**
     *
     */
    function get_file_type($filename)
    {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        $pos = strrpos($filename, ".");
        if ($pos !== false) {
            $extension = substr($filename, $pos + 1);
        }
        $ret = "";
        switch (strtolower($extension)) {
        case "gif":
            $ret = "gif";
            break;
        case "jpg":
        case "jpeg":
            $ret = "jpeg";
            break;
        case "png":
            $ret = "png";
            break;
        case "pdf":
            $ret = "pdf";
            break;
        case "xls":
        case "xlsx":
            $ret = "excel";
            break;
        case "doc":
        case "docx":
            $ret = "word";
            break;
        case "ppt":
        case "pptx":
            $ret = "ppp";
            break;
        }
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, array($filename, $ret));
        return $ret;
    }
}
$main = new N2MY_Meeting_Document_API();
$main->execute();