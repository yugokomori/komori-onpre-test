<?php
require_once("classes/N2MY_DBI.class.php");

class MgmUserTable extends N2MY_DB {

    function __construct( $dsn ) {
        $this->init( $dsn, "user" );
    }

    public function add( $data )
    {
        $data["user_registtime"] = date( "Y-m-d H:i:s" );
        return parent::add( $data );
    }

    public function update( $data, $where )
    {
        $data["user_updatetime"] = date( "Y-m-d H:i:s" );
        parent::update( $data, $where );
    }
}