<?php
require_once(dirname(__FILE__)."/../../logic/participant/ParticipantCreatedLogic.php");
require_once(dirname(__FILE__)."/../../logic/CalleeParticipationController.php");

class CallingParticipantCreatedLogic extends ParticipantCreatedLogic {
	public $callingParticipant;
	public function setCallerParticipant() {
		$this->callingParticipant = $this->conferenceProxy->getCallingParticipant($this->meetingRecord["meeting_key"], $this->settingRecord["ives_did"]);
		if (!$this->callingParticipant)
			throw new Exception("uninvited guest. meetingKey: ".$this->meetingRecord["meeting_key"].", did: ".$this->settingRecord["ives_did"]);
	}

	public function updateCallerParticipant() {
		$data = array(
				"video_conference_key"=>$this->conferenceRecord["video_conference_key"],
				"media_mixer_key"=>$this->conferenceRecord["media_mixer_key"],
				"conference_id"=>$this->requestProxy->confId,
				"part_id"=>$this->requestProxy->partId,
				"updatetime"=>date("Y-m-d H:i:s"));
		$this->conferenceProxy->updateCallingParticipant($data, $this->callingParticipant["video_participant_key"]);
	}

	// IParticipant
	public function checkParticipatedNumber() {
		if (!$this->roomRecord) {
			throw new Exception("no room record.");
		}
		if (!$this->settingRecord) {
			throw new Exception("no setting record.");
		}
// 		if (!$this->participantRecordList)
// 			return true;
		// 拡張人数取得 for PIS
		$ReservationInfo = $this->meetingProxy->getReservationRecordByMeetingKey($this->meetingRecord["meeting_ticket"]);
		require_once("classes/dbi/reservation_extend.dbi.php");
		$where = "extend_seat != 0 AND reservation_session_id = '" .mysql_real_escape_string($ReservationInfo["reservation_session"]) . "'";
		$reserve_extend			= new ReservationExtendTable($this->dsn);
		$extend_infos = $reserve_extend->getRowsAssoc($where);
		$extend_seat = 0;
		if($extend_infos){
    		foreach($extend_infos as $extend_info){
    		    $extend_seat = $extend_seat + $extend_info["extend_seat"];
    		}
		}
		$this->logger->info($extend_seat);

		$participationController = new CalleeParticipationController();
		return $participationController->checkParticipation($this->requestProxy->confId, $this->participantRecordList, $this->roomRecord["max_seat"]+$extend_seat, $this->settingRecord["num_profiles"]);
	}
}
