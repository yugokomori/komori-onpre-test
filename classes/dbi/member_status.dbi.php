<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
// +----------------------------------------------------------------------+
// |                                                                      |
// | [            ]                                                       |
// |                                                                      |
// +----------------------------------------------------------------------+
// | PHP version 4                                                        |
// +----------------------------------------------------------------------+
// | Authors:oneda                                                        |
// |                                                                      |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006               all rights reserved.                |
// +----------------------------------------------------------------------+
//
require_once("classes/N2MY_DBI.class.php");

class MemberStatusTable extends N2MY_DB {

    function MemberStatusTable($dsn) {
        $this->rules = array(

            "member_status_key"     => array(
                "required" => true),
            "user_key"     => array(
                "required" => true),
            "member_status_name"     => array(
                "required" => true)
        );
        $this->init($dsn, "member_status");
    }

    function getStatusName($key){

        $query = "select * from member_status"
                       . " where member_status_key={$key}";

        $ret = $this->_conn->getone($query);

        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, $query);
            return $ret;
        }
        return $ret;
    }

}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
