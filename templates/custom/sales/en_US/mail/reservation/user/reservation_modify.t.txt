*This email is transmission only, please do not reply.

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
       Notice of Change from V-CUBE Sales & Support
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
       {$info.sender} has changed meeting using "V-CUBE Sales & Support". 

Click the invitation URL below at the starting time to attend the meeting.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

　□ Topic
　　 {$info.reservation_name}

　□ Date and Time
　　 {$guest.starttime}
　　 〜 {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

　□ {if $guest.member_key == ""}Customer{else}Staff{/if}URL
{if $info.guest_url_format == 1}
　 　{$base_url}guest/r/{$guest.r_user_session}&c={$guest.country_key}
{else}
　 　<{$base_url}guest/r/{$guest.r_user_session}&c={$guest.country_key}>
{/if}
　□ Password
　　 {if $info.reservation_pw}{$info.reservation_pw}{else}No Password{/if}


　□ Message from {$info.sender}
　　 {$info.mail_body}

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　　　　　　　　　　Important Information
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■

　□ How to use V-CUBE Sales & Support
     1. Connect a webcam and a headset to your PC.
     2. Click and run the "Environment Check Tool" below.
     3. Click the secured link invitation above to enter the meeting.
    ※Please do not plug in any additional hardware after you click the secured link invitation.
      It will not be available until you restart the meeting room.
    ※If you do not have a webcam, you can still join the meeting using a USB headset.


　□ Check Environment Check Tool
     This is V-CUBE's Environment Chcek tool (Simplified Version).
     It will automatically check network environment, camera and microphone setting.
     Access URL below, select each field and click "Check start" button.
  　 {if $info.guest_url_format ==1}{$base_url}services/tools/checker/{else}<{$base_url}services/tools/checker/>{/if}

　□ System Requirements
　　 Refer to our system requirements page.
　   {if $info.guest_url_format ==1}http://www.nice2meet.us/ja/requirements/sales.html{else}<http://www.nice2meet.us/ja/requirements/sales.html>{/if}

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　  About V-CUBE Sales&Support
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■

V-CUBE Sales & Support is a cloud-based video web conferencing system. 
By accessing specified address, users can have face to face communication using video and audio.
There are many tools for communication such as whiteboard, recording,
 and PC desktop sharing feature.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Sales & Support
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■Developed and Managed by:
V-cube, Inc. {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
Nakameguro GT Tower 20F, 2-1-1 Kamimeguro, Meguro-ku, Tokyo 153-0051
■Customer Support Center (Japanese Only)
Tel: 0570-002192 (Open 24/7)
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if} 
■Customer Support Center (English & Others)
Tel:
　Tokyo　+81-3-4560-1287
　Malaysia　+60-3-7724-9693
　Singapore　+65-3158-2832
　China　 +86-4006-618-2360
E-mail：{if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if} 
Support Time：Weekdays 9:00-18:00(GMT+8)
Support not available during weekends and public holidays in Malaysia
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
