※このメールは送信専用のため、ご返信いただけません。

■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
       セールス＆サポート予約の内容変更メール送信完了のお知らせ
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
               "V-CUBE セールス＆サポート" を
            ご利用頂きまして誠にありがとうございます。
 下記の内容でセールス＆サポート予約の内容変更メールをお送りいたしました。
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

　□ 会議名
　　 {$info.reservation_name}

　□ 開催日時
　　 {$info.reservation_starttime}
　　 〜 {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

　□ パスワード
　　 {if $info.reservation_pw}{$info.reservation_pw}{else}無し{/if}


■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
　　　　　　　　　　　　　　　参加者一覧
■━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━■
{if $guests}{foreach from=$guests item=guest}
　□ お名前: {$guest.name} 様{if $guest.type == 1} (オーディエンス){elseif $guest.type == 2} (資料ユーザ){/if}

　□ メールアドレス: {$guest.email}
　□ {if $guest.member_key == ""}お客様{else}スタッフ{/if}URL: {if $info.guest_url_format ==1}{$base_url}guest/r/{$guest.r_user_session}&c={$guest.country_key}{else}<{$base_url}guest/r/{$guest.r_user_session}&c={$guest.country_key}>{/if}

　□ 開催日時: {$guest.starttime}
　　           〜 {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})
-----------------------------------------------------------------------
{/foreach}{/if}

　□ メッセージ
　　 {$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE　セールス＆サポート
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■開発・運営:
ブイキューブグループ {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
〒153-0051 東京都目黒区上目黒2-1-1 中目黒GTタワー20F
■カスタマーサポートセンター（日本語専用）
電話番号: 0570-002192（24時間365日電話受付）
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if}
■カスタマーサポートセンター (英語その他)
電話番号：
　東京　+81-3-4560-1287
　マレーシア　+60-3-7724-9693
　シンガポール　+65-3158-2832
　中国　 +86-4006-618-2360
E-mail：{if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if}
対応時間：平日 9:00-18:00(GMT+8)
土日及びマレーシアの祝日は休業となります。
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
