ALTER TABLE  `user` ADD  `external_user_invitation_flg` TINYINT( 4 ) NOT NULL DEFAULT  '0' COMMENT  'ID制の外部招待許可フラグ' AFTER  `vcubeid_add_use_flg`;
ALTER TABLE  `reservation` ADD  `is_reminder_flg` TINYINT( 4 ) NOT NULL DEFAULT  '0' COMMENT  'リマインダーフラグ' AFTER  `is_wb_no_flg`;
ALTER TABLE  `reservation` ADD  `is_reminder_send_flg` TINYINT( 4 ) NOT NULL DEFAULT  '0' COMMENT  'リマインダーメール送信フラグ、0:未送信、1:送信済み、2:エラー' AFTER  `is_reminder_flg`;
ALTER TABLE  `reservation` ADD  `sender_lang` varchar(10) NULL COMMENT  '送信者言語' AFTER  `sender_email`;
ALTER TABLE  `reservation` ADD  `reservation_url` varchar(100) NULL COMMENT  '予約時のURL' AFTER  `sender_lang`;