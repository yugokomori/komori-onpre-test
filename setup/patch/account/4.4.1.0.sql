ALTER TABLE `fms_server` ADD `maintenance_time` VARCHAR( 8 ) NULL DEFAULT NULL COMMENT 'リブート時間' AFTER `is_ssl`;

--
-- mgm.service_option
--
INSERT INTO `service_option` (`service_option_key` ,`country_key` ,`service_option_name` ,`service_option_price` ,`service_option_init_price` ,`service_option_running_price`)VALUES ('15', '1', 'intra_fms', '0', '0', '0');

--
-- mgm.option
--
INSERT INTO `options` (`option_key` ,`option_name` ,`create_datetime` ,`update_datetime`)VALUES (15 , 'intra_fms', '0000-00-00 00:00:00', NULL);