ALTER TABLE `meeting`
  DROP `ives_did`,
  DROP `ives_conference_id`,
  DROP `ives_api_status`;

ALTER TABLE `participant` DROP `ives_did`;

 CREATE TABLE `video_conference_participant` (
`video_participant_key` INT( 11 ) NOT NULL AUTO_INCREMENT ,
`video_conference_key` INT( 11 ) NOT NULL ,
`conference_id` VARCHAR( 64 ) NOT NULL ,
`part_id` INT( 10 ) NOT NULL ,
`participant_key` INT( 10 ) NOT NULL ,
`meeting_key` INT( 10 ) NOT NULL ,
`createtime` DATETIME NOT NULL ,
`updatetime` DATETIME NOT NULL ,
PRIMARY KEY ( `video_participant_key` )
) ENGINE = InnoDB;

ALTER TABLE `video_conference_participant` ADD `is_active` TINYINT( 1 ) NOT NULL DEFAULT '0' AFTER `meeting_key` ;

ALTER TABLE `participant` DROP `ives_conference_id`;

ALTER TABLE `video_conference` ADD `meeting_key` INT( 10 ) NOT NULL AFTER `user_key` ;