<?php
require_once("classes/N2MY_DBI.class.php");

class LanguageTable extends N2MY_DB {

    var $table = 'language';
    var $logger = null;

    /**
     * DB接続
     */
    function __construct($dsn) {
        $this->init($dsn, $this->table);
        $this->logger = EZLogger::getInstance();
    }
}
