//
var DEFAULT_PRINTER_NAME = "Adobe PDF";

// var ADOBE_ACROBAT_INSTALLED_PATH = "C:\\Program\ Files\\Adobe\\Acrobat\ 9.0\\Acrobat\\Acrobat.exe";
var ADOBE_ACROBAT_INSTALLED_PATH = "C:\\Program\ Files\ (x86)\\Adobe\\Acrobat\ 11.0\\Acrobat\\Acrobat.exe";


/**
 * ベクター変換のための一次フォルダ
 * 「adobe pdf」プリンタの保存先フォルダと同じフォルダを指定してください
 */
// var CONVERT_TEMPORARY_PATH = "C:\\Documents\ and\ Settings\\vstaff\\My\ Documents\\";//
var CONVERT_TEMPORARY_PATH = "C:\\Users\\doc_user\\Documents\\";

/**
 * docuworks config
 */

/**
 * dwg変換アプリケーションのパス
 */
var ACME_CAD_CONVERTER_INSTALLED_PATH = "C:\\Program\ Files\\Acme\ CAD\ Converter\\AcmeCADConverter.exe";

/**
 *
 */
var SCRIPT_LOG_PATH	= "C:\\doc\\VrmsDocumentConverterAIR\\logs";

/**
 * bin-debug利用時
 */
var debug = 1;

/**
 * 
 */
var DEBUG_INSTALLED_PATH = "C:\\doc";

/**
 * 
 */
var SCRIPT_LOG_LVL = 1;