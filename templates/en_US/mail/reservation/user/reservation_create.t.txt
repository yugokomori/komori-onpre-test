*This email is transmission only, please do not reply.

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Notice from VA Meeting System
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
{$info.sender} has invited you to join a web meeting using "VA Meeting System".

Click the invitation URL below at the starting time to attend the meeting.

■Topic:
{$info.reservation_name}

■Date and Time:
{$guest.starttime} ～ {$guest.endtime} (GMT {if $guest.timezone >= 0}+{/if}{$guest.timezone})

■Organizer
{$info.organizer.name} 

■Secured link to the Meeting Room:
{if $info.guest_url_format ==1}
{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}
{else}
<{$base_url}r/{$guest.r_user_session}&c={$guest.country_key}>
{/if}

------------------------------
Message from {$info.sender}
------------------------------
{$info.mail_body}

※This invitation has been sent automatically from the system.
　Please send any inquiry for this meeting to this email address.
{if $info.guest_url_format ==1}
　{$info.sender_mail}
{else}
　<{$info.sender_mail}>
{/if}

------------------------------
Important Information
------------------------------
■How to use VA Meeting System
1. Connect a webcam and a headset to your PC.
2. Click and run the Checker tool below.
   It will help you configure the proper hardware and best network connectivity.
{if $info.guest_url_format ==1}
   {$base_url}services/tools/checker/
{else}
   <{$base_url}services/tools/checker/>
{/if}
3. Click the secured link invitation at the top of this email to enter the meeting.

※Please do not plug in any additional hardware after you click the secured link invitation.  It will not be available until you restart the meeting room.
※If you do not have a webcam, you can still join the meeting using a USB headset.

■System Requirements
Refer to our system requirements page.
{if $info.guest_url_format ==1}
http://www.nice2meet.us/ja/requirements/meeting.html
{else}
<http://www.nice2meet.us/ja/requirements/meeting.html>
{/if}

■Get V-CUBE Mobile Application
Get application for iphone or ipad from the links below to attend the meeting.
・Application for iOS
{if $info.guest_url_format ==1}
http://itunes.apple.com/jp/app/id431070449?mt=
{else}
<http://itunes.apple.com/jp/app/id431070449?mt=>
{/if}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
Panasonic Information Systems
SupportCenter
Phone：06-6906-2805
Extension：711-8787
E-mail：{if $info.guest_url_format ==1} help@ml.is-c.jp.panasonic.com {else} <help@ml.is-c.jp.panasonic.com>{/if} 
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
