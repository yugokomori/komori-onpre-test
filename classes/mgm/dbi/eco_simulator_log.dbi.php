<?php

require_once("classes/N2MY_DBI.class.php");

class EcoSimulatorLogTable extends N2MY_DB
{

    var $table = "eco_simulator_log";

    function __construct($dsn)
    {
        $this->init($dsn, $this->table);
    }
}