ALTER TABLE  `question` ADD  `lang6` TEXT CHARACTER SET utf8 NULL COMMENT  'タイ語' AFTER  `lang5`;
ALTER TABLE  `question` ADD  `lang7` TEXT CHARACTER SET utf8 NULL COMMENT  'インドネシア語' AFTER  `lang6`;

ALTER TABLE  `question_branch` ADD  `lang6` TEXT CHARACTER SET utf8 NULL COMMENT  'タイ語' AFTER  `lang5`;
ALTER TABLE  `question_branch` ADD  `lang7` TEXT CHARACTER SET utf8 NULL COMMENT  'インドネシア語' AFTER  `lang6`;

UPDATE `question` SET `lang4` = '您對本次網絡視频會議的使用滿意嗎？', `lang5` = 'Quel est votre degré de satisfaction concernant l\'utilisation de ce service de Web conférence?', `lang6` = 'คุณได้รับความพึงพอใจในการใช้การประชุมเว็บนี้?', `lang7` = 'Seberapa puas anda dengan web konferensi service ini?' WHERE `question_id` = '16';
UPDATE `question` SET `lang4` = '關於圖像（可以多選）', `lang5` = 'A propos de la vidéo (choix multiples)', `lang6` = 'สำหรับวิดีโอ (เลือกได้หลายแบบ)', `lang7` = 'tentang video ( beberapa seleksi)' WHERE `question_id` = '17';
UPDATE `question` SET `lang4` = '關於語音（可以多選）', `lang5` = 'A propos de l\'audio (choix multiples)', `lang6` = 'สำหรับเสียง (เลือกได้หลายแบบ)', `lang7` = 'tentang audio (beberapa seleksi)' WHERE `question_id` = '18';
UPDATE `question` SET `lang4` = '請您填寫其他的意見或建議。（自由闡述）', `lang5` = 'N\'hésitez pas à nous faire des suggestions et des commentaires', `lang6` = 'โปรดให้ความคิดเห็นและข้อเสนอแนะของคุณ (คำอธิบาย)', `lang7` = 'tolong berikan kepada kami masukan dan pendapat kamu (bebas deskripsi)' WHERE `question_id` = '19';
UPDATE `question` SET `lang4` = '姓名', `lang5` = 'Nom', `lang6` = 'ชื่อ', `lang7` = 'nama' WHERE `question_id` = '20';
UPDATE `question` SET `lang4` = '假名', `lang5` = 'Nom', `lang6` = 'นามแฝง', `lang7` = 'nama' WHERE `question_id` = '21';
UPDATE `question` SET `lang4` = '公司名·所屬部門', `lang5` = 'Société', `lang6` = 'ชื่อบริษัทหรือหน่วยงาน', `lang7` = 'company' WHERE `question_id` = '22';
UPDATE `question` SET `lang4` = '郵箱地址', `lang5` = 'Email', `lang6` = 'ที่อยู่อีเมล์', `lang7` = 'alamat email' WHERE `question_id` = '23';
UPDATE `question` SET `lang4` = '電話號碼', `lang5` = 'Numéro de téléphone', `lang6` = 'หมายเลขโทรศัพท์', `lang7` = 'nomor telepon' WHERE `question_id` = '24';

UPDATE `question_branch` SET `lang4` = '滿意', `lang5` = 'Satisfait', `lang6` = 'พึงพอใจ?', `lang7` = 'Puas' WHERE `question_branch_id` = '46';
UPDATE `question_branch` SET `lang4` = '一般', `lang5` = 'Couci-couça', `lang6` = 'ดังนั้น', `lang7` = 'Cukup Memuaskan' WHERE `question_branch_id` = '47';
UPDATE `question_branch` SET `lang4` = '不滿意', `lang5` = 'Mécontent', `lang6` = 'ไม่พอใจ', `lang7` = 'Tidak Puas' WHERE `question_branch_id` = '48';
UPDATE `question_branch` SET `lang4` = '未顯示對方圖像', `lang5` = 'Impossible de voir la vidéo des autres', `lang6` = 'ไม่ปรากฏภาพบุคคลอื่น', `lang7` = 'Tidak dapat melihat video yang lainnya' WHERE `question_branch_id` = '49';
UPDATE `question_branch` SET `lang4` = '未顯示我方圖像', `lang5` = 'Impossible de voir ma vidéo', `lang6` = 'ไม่ปรากฎภาพคุณเอง', `lang7` = 'Tidak dapa melihat video saya' WHERE `question_branch_id` = '50';
UPDATE `question_branch` SET `lang4` = '圖像品質差', `lang5` = 'La qualité vidéo n\'est pas bonne', `lang6` = 'คุณภาพของวิดีโอไม่ดี', `lang7` = 'Kualitas video tidak bagus' WHERE `question_branch_id` = '51';
UPDATE `question_branch` SET `lang4` = '無法接通我方語音', `lang5` = 'Impossible d\'entendre ma voix', `lang6` = 'ไม่ได้ยินเสียงคุณเอง', `lang7` = 'Tidak dapat mendengar suara saya sendiri' WHERE `question_branch_id` = '52';
UPDATE `question_branch` SET `lang4` = '無法聽到對方語音', `lang5` = 'Impossible d\'entendre la voix des autres', `lang6` = 'ไม่ได้ยินเสียงบุคคลอื่น', `lang7` = 'Tidak dapat mendengar suara lainnya' WHERE `question_branch_id` = '53';
UPDATE `question_branch` SET `lang4` = '經常聽不到聲音', `lang5` = 'Le son coupe régulièrement', `lang6` = 'เสียงถูกขัดจังหวะบ่อย', `lang7` = 'Suara sering hilang' WHERE `question_branch_id` = '54';
UPDATE `question_branch` SET `lang4` = '語音延遲', `lang5` = 'Il y a de la latence', `lang6` = 'เกิดความล่าช้า', `lang7` = 'Karena delay' WHERE `question_branch_id` = '55';
UPDATE `question_branch` SET `lang4` = '語音迴響（聽起來有回音）', `lang5` = 'Il y a de l\'echo', `lang6` = 'เกิดเสียงสะท้อน', `lang7` = 'Karena echo' WHERE `question_branch_id` = '56';
UPDATE `question_branch` SET `lang4` = '語音嘯響（聲音尖銳刺耳、大聲作響）', `lang5` = 'Il y a du bruit', `lang6` = 'เกิดเสียงหอน', `lang7` = 'Karena lolongan' WHERE `question_branch_id` = '57';