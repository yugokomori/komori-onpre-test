<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
// +----------------------------------------------------------------------+
// |                                                                      |
// | 認証クラス                                                           |
// |                                                                      |
// +----------------------------------------------------------------------+
// | PHP version 4                                                        |
// +----------------------------------------------------------------------+
// | Authors: Masayuki IIno <masayuki.iino@gmail.com>                     |
// |                                                                      |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006 Masayuki IIno all rights reserved.                |
// +----------------------------------------------------------------------+
//
// $Id: EZAuth.class.php,v 1.2 2006/01/10 04:02:15 iino Exp $

require_once('lib/EZLib/EZCore/EZBase.class.php');

// ----------------------------   define  ---------------------------------

// {{{ -- define
// }}}

/**
 *【EZAuth】認証クラス
 *
 * <b>説明</b><br />
 *
 * <code>
 * </code>
 *
 * @access     public
 * @author     Masayuki IIno <masayuki.iino@gmail.com>
 * @package    EZLib
 * @subpackage EZAuth
 * @version    $Revision
 */
class EZAuth extends EZBase
{
// ----------------------------    vars   ---------------------------------

// {{{ -- vars

    /**
     * 認証情報
     *
     * @access private
     * @var    array
     */
    var $_auth = array();

// }}}

// ----------------------------    init   ---------------------------------

// {{{ -- EZAuth()

    /**
     * コンストラクタ
     *
     * @access public
     * @param
     * @return
     */
    function EZAuth()
    {
    }

// }}}
// {{{ -- _EZAuth()

    /**
     * デストラクタ
     *
     * @access private
     * @param
     * @return
     */
    function _EZAuth()
    {
    }

// }}}

// ---------------------------- interface ---------------------------------

// ----------------------------  private  ---------------------------------

// ----------------------------   public  ---------------------------------

// {{{ -- all()

    /**
     * 全ての認証パラメータを取得
     *
     * @access public
     * @param
     * @return array
     */
    function all()
    {
        return $this->_auth;
    }

// }}}
// {{{ -- clear()

    /**
     * 全ての認証パラメータを完全に削除
     *
     * @access public
     * @param
     * @return
     */
    function clear()
    {
        $this->_auth = array();
    }

// }}}
// {{{ -- get()

    /**
     * 認証パラメータを取得
     *
     * @access public
     * @param  mixed  $keys    要素名（要素名を配列で渡すことにより複数指定可能）
     * @param  string $default デフォルト値
     * @return mixed
     */
    function get($keys, $default = null)
    {
        if (is_array($keys)) {
            $ra = array();
            foreach ($keys as $k) {
                if (isset($this->_auth[$k])) {
                    $ra[$k] = $this->_auth[$k];
                }
            }
            return $ra;

        } else {
            if (isset($this->_auth[$keys])) {
                return $this->_auth[$keys];
            } else {
                return $default;
            }
        }
    }

// }}}
// {{{ -- getAll()

    /**
     * 認証パラメータを全て取得
     *
     * @access public
     * @param
     * @return array
     */
    function getAll()
    {
        if (!isset($this->_auth)) {
            $this->_auth = array();
        }

        return $this->_auth;
    }

// }}}
// {{{ -- getKeys()

    /**
     * 認証パラメータ要素名をすべて取得
     *
     * @access public
     * @param
     * @return array
     */
    function getKeys()
    {
        if (isset($this->_auth)) {
            return array_keys($this->_auth);
        }

        return array();
    }

// }}}
// {{{ -- has()

    /**
     * 認証パラメータに指定された要素名が存在するかチェック
     *
     * @access public
     * @param  string  $key 要素名
     * @return boolean
     */
    function has($key)
    {
        if (isset($this->_auth[$key])) {
            return true;
        } else {
            return false;
        }
    }

// }}}
// {{{ -- remove()

    /**
     * 認証パラメータを削除
     *
     * @access public
     * @param  string $key 要素名
     * @return
     */
    function remove($key)
    {
        if (isset($this->_auth[$key])) {
            unset($this->_auth[$key]);
        }
    }

// }}}
// {{{ -- removeAll()

    /**
     * 認証パラメータを全て削除
     *
     * @access public
     * @param
     * @return
     */
    function removeAll()
    {
        if (isset($this->_auth)) {
            $this->_auth = array();
        }
    }

// }}}
// {{{ -- set()

    /**
     * 認証パラメータを設定
     *
     * @access public
     * @param  string $key   要素名
     * @param  mixed  $value 値
     * @return
     */
    function set($key, $value)
    {
        if (is_string($value)) {
            $this->_auth[$key] = trim($value);
        } else {
            $this->_auth[$key] = $value;
        }
    }

// }}}
// {{{ -- setAll()

    /**
     * 認証パラメータをまとめて設定
     *
     * @access public
     * @param  array   &$data 要素名と値のペア配列（参照渡し）
     * @param  boolean  $mode モード（true：既存のパラメータに上書き | false：設定値で入れ替え）
     * @return
     */
    function setAll(&$data, $mode = false)
    {
        if ($mode == true) {
            $this->_auth = $data;
        } else {
            if (!isset($this->_auth)) {
                $this->_auth = array();
            }
            $this->_auth = array_merge($this->_auth, $data);
        }
    }

// }}}

// ----------------------------   option  ---------------------------------

}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
