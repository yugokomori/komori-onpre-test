<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once ("classes/AppFrame.class.php");
require_once ("classes/N2MY_Reservation.class.php");
require_once ("classes/mgm/MGM_Auth.class.php");
require_once ('lib/EZLib/EZUtil/EZDate.class.php');
require_once ("lib/EZLib/EZUtil/EZString.class.php");
require_once ("classes/dbi/reservation_user.dbi.php");

class AppReservation extends AppFrame {

    var $_name_space = null;
    var $_reservation = null;
    var $_ssl_mode = null;
    var $_reservation_obj = null;
    private $obj_N2MY_Reservation = null;

    /**
     * 初期処理
     */
    function init() {
        $this->_name_space = md5(__FILE__);
    }

    /**
     * ログイン認証
     */
    function auth() {
        $this->checkAuth();
        // 認証後のみ
        $server_info = $this->session->get("server_info");
        $this->obj_N2MY_Reservation = new N2MY_Reservation($this->get_dsn(), $server_info["host_name"]);
        if(!$this->session->get('login_type')){
            $this->session->set('login_type', 'reservation');
        }
        $url = $this->get_redirect_url("services/reservation/?action_create=&new=1");
        header("Location: ".$url);
    }

    function action_detail() {
        $member_info = $this->session->get( "member_info" );
        $user_info = $this->session->get( "user_info" );
        $reservation_session = $this->request->get('reservation_session');
        $reservation_data = $this->obj_N2MY_Reservation->getDetail($reservation_session);
        if(!$reservation_data || $reservation_data["info"]["user_key"] != $user_info["user_key"]){
            header("Location: /services/reservation/yoyaku.php");
        }
        if (!$this->isAuthorized($reservation_data, 'yoyaku.php?action_detail=&reservation_session='.$reservation_session)) {
            return false;
        }
        //部屋のオプション確認
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $reservation_data["info"]["room_option"] = $obj_N2MY_Account->getRoomOptionList($reservation_data["info"]["room_key"]);
        require_once ("classes/dbi/room.dbi.php");
        $room_obj = new RoomTable($this->get_dsn());
        $room_info = $room_obj->getRow("room_key = '".$reservation_data["info"]["room_key"]."'", "cabinet");
        $reservation_data["info"]["room_option"]["cabinet"] = $room_info["cabinet"];
        $this->setReservationData($reservation_data);
        if(($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") &&
            $member_info["room_key"] != $reservation_data["info"]["room_key"] ){
            $reservation_data["info"]["status"] = "end";
        }
        $this->template->assign("reservation_data", $reservation_data);
        $this->template->assign("is_detail", true);
        $this->display('user/yoyaku/detail_view.t.html');
    }

    /**
     * 予約画面一覧を表示
     */
    function action_list() {
        $this->session->remove('reservation_auth', $this->_name_space);
        $user_info = $this->session->get( "user_info" );
        $member_info = $this->session->get( "member_info" );
        ( ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") && $member_info )?
            $this->render_participantedList():
            $this->render_list();
    }

    function action_timezone() {
        $this->display('user/yoyaku/timezone.t.html');
    }

    /**
     * 予約一覧
     */
    function action_top($room_key = "") {
        $this->session->remove("not_change_reservation_data");
        $this->session->remove('reservation_auth', $this->_name_space);
        $room_list = $this->get_room_info();
        if (!$room_key) {
            if (count($room_list) == 1) {
                $room_info = array_shift($room_list);
                $room_key = $room_info["room_info"]["room_key"];
            }
        }
        $user_info = $this->session->get("user_info");
        $room_key = $this->request->get("room_key", $room_key);
        $page = $this->request->get("page", 1);
        $limit = $this->request->get("page_cnt", 20);
        $options = array(
            "user_key"   => $user_info["user_key"],
            "room_key"   => $room_key,
            "start_time" => date("Y-m-d H:i:s"),
            "page"       => $page,
            "limit"      => $limit,
            "offset"     => $limit * ($page - 1),
            "sort_key"   => $this->request->get("sort_key") ? $this->request->get("sort_key") : "reservation_starttime",
            "sort_type"  => $this->request->get("sort_type") ? $this->request->get("sort_type") : "asc",
        );
        $this->session->set("condition", $options, $this->_name_space);
        $this->render_top();
    }

    public function action_sort_list() {
        $search = $this->session->get("condition", $this->_name_space);
        $search["page"]         = 1;
        $search["sort_key"]     = $this->request->get("sort_key") ? $this->request->get("sort_key") : "reservation_starttime";
        $search["sort_type"]    = $this->request->get("sort_type") ? $this->request->get("sort_type") : "desc";
        $this->session->set("condition", $search, $this->_name_space);
        $this->render_top();
    }

    /**
     * 予約一覧表示
     */
    function render_top() {
        $options = $this->session->get("condition", $this->_name_space);
        $member_info = $this->session->get( "member_info" );
        $user_info = $this->session->get( "user_info" );
        $room_info = $this->session->get( "room_info" );
        if( ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") && $member_info ){
            $reservation_list = $this->obj_N2MY_Reservation->getParticipantedMeetingList( $member_info, $options, $this->session->get('time_zone'), $room_info );
            $total = $this->obj_N2MY_Reservation->getParticipantedMeetingCount( $member_info, $options );
        } else {
            $reservation_list = $this->obj_N2MY_Reservation->getList($options["room_key"], $options, $this->session->get('time_zone'));
            $total = $this->obj_N2MY_Reservation->getCount($options["room_key"], $options);
        }
        $pager = $this->setPager($options["limit"], $options["page"], $total);
        $this->template->assign("room_key", $options["room_key"]);
        $this->template->assign("pager", $pager);
        $this->template->assign("conditions", $options);
        $this->template->assign("reservation_list", $reservation_list);
        $this->display('user/yoyaku/list.t.html');
    }

    /**
     * 会議予約確認画面
     */
    function action_create() {
        // 新規作成時
        if ($this->request->get("new") == "1") {
            // 入力をキャンセル
            $this->clearReservationData();
            // デフォルトの日時を指定
            $now_time = EZDate::getLocateTime(time(), N2MY_USER_TIMEZONE, N2MY_SERVER_TIMEZONE);
            $start_time = $now_time + (300 - ($now_time % 300));
            // 選択肢を初期化
            $room_list = $this->get_room_info();
            $default_room = array_shift($room_list);
            $user_info = $this->session->get("user_info");
            $member_info = $this->session->get("member_info");
            $room_key = $this->request->get("room_key", $default_room["room_info"]["room_key"]);
            //部屋のオプション確認
            require_once("classes/N2MY_Account.class.php");
            $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
            $room_option = $obj_N2MY_Account->getRoomOptionList($room_key);
            require_once ("classes/dbi/room.dbi.php");
            $room_obj = new RoomTable($this->get_dsn());
            $room_info = $room_obj->getRow("room_key = '".addslashes($room_key)."'", "cabinet,is_wb_no");
            // 予約内容
            $reservation_data["info"] = array(
                "country_id"            => $this->session->get("country_key"),
                "reservation_starttime" => $start_time,
                "reservation_endtime"   => strtotime("next hour", $start_time),
                "reservation_place"     => $this->session->get("time_zone", _EZSESSION_NAMESPACE, N2MY_SERVER_TIMEZONE),
                "user_key"              => $user_info["user_key"],
                "room_key"              => $room_key,
                "send_flg"              => 1,
                "sender"                => ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") ? $member_info["member_name"] :"",
                "sender_mail"           => ($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") ? $member_info["member_email"] :"",
                "mail_body"             => "",
                "reservation_place"     => $this->session->get("time_zone"),
                "auto_addressbook_flg"  => 0,
                "security_pw_flg"       => $user_info["is_compulsion_pw"],
                "is_limited_function"   => 0,
                "is_multicamera"        => $room_option["multicamera"] ? $room_option["multicamera"] :"0",
                "is_telephone"          => $room_option["telephone"] ? $room_option["telephone"] :"0",
                "is_h323"               => $room_option["h323_number"] ? $room_option["h323_number"] :"0",
                "is_mobile_phone"       => $room_option["mobile_phone_number"] ? $room_option["mobile_phone_number"] :"0",
                "is_high_quality"       => $room_option["high_quality"] ? $room_option["high_quality"] :"0",
                "is_desktop_share"      => $room_option["desktop_share"] ? $room_option["desktop_share"] :"0",
                "is_meeting_ssl"        => $room_option["meeting_ssl"] ? $room_option["meeting_ssl"] :"0",
                "is_invite_flg"         => 1,
                "is_rec_flg"            => 1,
                "is_cabinet_flg"        => $room_info["cabinet"] ? $room_info["cabinet"] :"0",
                "is_wb_no_flg"          => $room_info["is_wb_no"],
                );
            // クッキーに保存されてる差出人情報を初期値にする
            if(empty($reservation_data["info"]["sender"]) && $_COOKIE["sender_name_secure"]){
              $reservation_data["info"]["sender"] = $_COOKIE["sender_name_secure"];
              $reservation_data["name_save"] = 1;
            }

            if(empty($reservation_data["info"]["sender_mail"]) && $_COOKIE["sender_email_secure"]){
              $reservation_data["info"]["sender_mail"] = $_COOKIE["sender_email_secure"];
              $reservation_data["email_save"] = 1;
            }
            $reservation_data["info"]["room_option"] = $room_option;
            $reservation_data["info"]["room_option"]["cabinet"] = $room_info["cabinet"];
            // 招待者
            $reservation_data["guest_flg"] = 1;
            $reservation_data["guests"] = array();
            // 入力保持
            $this->setReservationData($reservation_data);
        } else {
            if ($room_key = $this->request->get("room_key")) {
                $reservation_data = $this->getReservationData();
                $reservation_data["info"]["room_key"] = $room_key;
                $this->setReservationData($reservation_data);
            }
        }
        // 会議予約画面を表示
        return $this->render_create();
    }

    /**
     * 会議予約画面の表示
     */
    function render_create($message = "") {
        $room_list = $this->get_room_info();
        $default_room = array_shift($room_list);
        // エラーメッセージ表示
        if($message && !$this->request->get('back')) {
            $this->template->assign('message', $message);
        }
        $_now = time() - (3600 * 24);
        $this->template->assign("start_limit", $_now);
        $this->template->assign("end_limit", $_now + (3600 * 24 * 367));
        // 会議予約情報取得、表示
        $reservation_data = $this->getReservationData();
        $room_info = $this->get_room_info($reservation_data["info"]["room_key"]);
        // 対応フォーマット取得
        require_once ("classes/N2MY_Document.class.php");
        $objDocument = new N2MY_Document($this->get_dsn());
        $support_document_list = $objDocument->getSupportFormat($reservation_data["info"]["room_key"]);
        $this->template->assign("document_list", join(", ", array_keys($support_document_list["document_list"])));
        $this->template->assign("image_list", join(", ", array_keys($support_document_list["image_list"])));
        $this->template->assign("display_filetype", join(", ", array_keys($support_document_list["image_list"] + $support_document_list["document_list"])));
        $this->template->assign("room_info", $room_info);
        $this->template->assign("reservation_data", $reservation_data);
        $cookie_lang = $this->request->getCookie("lang");
        $this->template->assign("cookie_lang", $cookie_lang);
        $this->template->assign("varsion", $this->_getMeetingVersion());
        $this->display('user/yoyaku/create.t.html');
    }

    /**
     * 入力データを保持したまま遷移
     */
    function action_change_room() {
        $request = $this->request->getAll();
        $reservation_info = $this->formatReservationData($request);
        //部屋のオプション確認
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $reservation_info["info"]["room_option"] = $obj_N2MY_Account->getRoomOptionList($reservation_info["info"]["room_key"]);
        require_once ("classes/dbi/room.dbi.php");
        $room_obj = new RoomTable($this->get_dsn());
        $room_info = $room_obj->getRow("room_key = '".$reservation_info["info"]["room_key"]."'", "cabinet");
        $reservation_info["info"]["room_option"]["cabinet"] = $room_info["cabinet"];
        //機能制限データ初期化
        $reservation_info["info"]["is_multicamera"] = $reservation_info["info"]["room_option"]["multicamera"] ? $reservation_info["info"]["room_option"]["multicamera"] :"0";
        $reservation_info["info"]["is_limited_function"] = 0;
        $reservation_info["info"]["is_telephone"] = $reservation_info["info"]["room_option"]["telephone"] ? $reservation_info["info"]["room_option"]["telephone"] :"0";
        $reservation_info["info"]["is_h323"] = $reservation_info["info"]["room_option"]["h323_number"] ? $reservation_info["info"]["room_option"]["h323_number"] :"0";
        $reservation_info["info"]["is_mobile_phone"] = $reservation_info["info"]["room_option"]["mobile_phone_number"] ? $reservation_info["info"]["room_option"]["mobile_phone_number"] :"0";
        $reservation_info["info"]["is_high_quality"] = $reservation_info["info"]["room_option"]["high_quality"] ? $reservation_info["info"]["room_option"]["high_quality"] :"0";
        $reservation_info["info"]["is_desktop_share"] = $reservation_info["info"]["room_option"]["desktop_share"] ? $reservation_info["info"]["room_option"]["desktop_share"] :"0";
        $reservation_info["info"]["is_meeting_ssl"] = $reservation_info["info"]["room_option"]["meeting_ssl"] ? $reservation_info["info"]["room_option"]["meeting_ssl"] :"0";
        $reservation_info["info"]["is_invite_flg"] = 1;
        $reservation_info["info"]["is_rec_flg"] = 1;
        $reservation_info["info"]["is_cabinet_flg"] = $reservation_info["info"]["room_option"]["cabinet"] ? $reservation_info["info"]["room_option"]["cabinet"] :"0";
        $reservation_info["info"]["is_wb_no_flg"] = 1;
        $this->setReservationData($reservation_info);
        return $this->render_create();
    }

    /**
     * 確認処理
     */
    function action_create_confirm() {
        // 入力値を保持
        $request = $this->request->getAll();
        $reservation_info = $this->formatReservationData($request);
        $this->setReservationData($reservation_info);
        if ($message = $this->check($reservation_info)) {
            return $this->render_create($message);
        } else {
            return $this->render_create_confirm();
        }
    }

    /**
     * 確認画面
     */
    function render_create_confirm() {
        // 二重登録回避
        $this->set_submit_key($this->_name_space);
        $reservation_data = $this->getReservationData();
        $this->template->assign("reservation_data", $reservation_data);
        $this->template->assign("upload_status", $this->getUploadStatus($reservation_data));
        $this->display('user/yoyaku/create_confirm.t.html');
    }

    /**
     * 会議予約登録
     */
    function action_create_complete() {
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_top();
        }
        $reservation_data = $this->getReservationData();
        // 入力チェック
        if ($message = $this->check($reservation_data)) {
            return $this->render_create($message);
        }
        // 資料事前アップロード設定
        $check_documents = $reservation_data["documents"];
        $create_documents = array();
        if (!$reservation_data["upload_flg"]) {
            foreach ($check_documents as $document_id => $document) {
                if (!$check_documents[$document_id]["tmp_name"]) {
                    $create_documents[$document_id] = $document[$document_id];
                }
            }
            $reservation_data["documents"] = $create_documents;
        }
        // いずれかのページでユーザーキーが設定なしの状態になったら
        if (!$reservation_data['info']['user_key']) {
            $this->logger2->warn($reservation_data, 'Invalid reservation.user_key');
            $user_info = $this->session->get("user_info");
            $reservation_data['info']['user_key'] = $user_info['user_key'];
        }
        // 予約登録
        $ret = $this->obj_N2MY_Reservation->add($reservation_data);
        // 差出人情報記憶
        // 有効期限
        $limit_time = time() + 365 * 24 * 3600;
        if($reservation_data["name_save"]){
          setcookie("sender_name_secure", $reservation_data["info"]["sender"], $limit_time, N2MY_COOKIE_PATH);
        } else {
          setcookie("sender_name_secure", "", (time()-1800),N2MY_COOKIE_PATH);
        }
        if($reservation_data["email_save"]){
          setcookie("sender_email_secure", $reservation_data["info"]["sender_mail"], $limit_time, N2MY_COOKIE_PATH);
        } else {
          setcookie("sender_email_secure", "", (time()-1800),N2MY_COOKIE_PATH);
        }

        $reservation_data["info"]["sender_lang"] =  $this->session->get("lang");
        $reservation_data["info"]["reservation_url"] = N2MY_BASE_URL;

        // 入力データをクリア
        $this->clearReservationData();
        // 登録完了（一覧表示）
        $this->action_top($reservation_data["info"]["room_key"]);
    }

    /**
     * 認証画面
     */
    function action_auth() {
        $reservation_info = $this->obj_N2MY_Reservation->getDetail($this->request->get('reservation_session'));
        if (!$this->isAuthorized($reservation_info, 'yoyaku.php?action_auth=&reservation_session='.$this->request->get('reservation_session'))) {
            return false;
        }
        //部屋のオプション確認
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $reservation_info["info"]["room_option"] = $obj_N2MY_Account->getRoomOptionList($reservation_info["info"]["room_key"]);
        require_once ("classes/dbi/room.dbi.php");
        $room_obj = new RoomTable($this->get_dsn());
        $room_info = $room_obj->getRow("room_key = '".$reservation_info["info"]["room_key"]."'", "cabinet");
        $reservation_info["info"]["room_option"]["cabinet"] = $room_info["cabinet"];
        $this->setReservationData($reservation_info);
//        if(!$reservation_info["info"]['reservation_pw']){
            return $this->action_view();
//        } else {
//            return $this->render_pw();
//        }
    }

    /**
     * パスワードを入力
     */
    function render_pw($message = null) {
        if($message){
            $this->template->assign('message', $message);
        }
        $this->display('user/yoyaku/auth.t.html');
    }

    /**
     * 参加者情報取得、表示
     */
    function action_view() {
        $data = $this->getReservationData();
        $user_info = $this->session->get("user_info");
        if ($data["info"]['reservation_pw']) {
            $data["pw_flg"] = 2;
            $data["info"]['old_reservation_pw'] = $data["info"]['reservation_pw'];
        }
        $data["info"]["security_pw_flg"] = $user_info["is_compulsion_pw"];
        // 登録有りの場合はフォームを表示
        if ($data["guests"]) {
            $data["guest_flg"] = 1;
        } else {
            $data["guest_flg"] = 0;
        }
        if ($data["documents"]) {
            $data["upload_flg"] = 1;
        }
        $this->setReservationData($data);
        $this->render_modify();
    }

    /**
     * 会議日程修正
     */
    function action_modify() {
        // 新規作成時
        if ($this->request->get("first") == "1") {
            $reservation_session = $this->request->get('reservation_session');
            $reservation_data = $this->obj_N2MY_Reservation->getDetail($reservation_session);
            $this->logger2->debug($reservation_data);
            $reservation_data["info"]["sender"]      = "";
            $reservation_data["info"]["sender_mail"] = "";
            $reservation_data["info"]["mail_body"]   = "";
            $reservation_data["info"]["auto_addressbook_flg"] = 0;
            //部屋のオプション確認
            require_once("classes/N2MY_Account.class.php");
            $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
            $reservation_data["info"]["room_option"] = $obj_N2MY_Account->getRoomOptionList($reservation_data["info"]["room_key"]);

            require_once ("classes/dbi/room.dbi.php");
            $room_obj = new RoomTable($this->get_dsn());
            $room_info = $room_obj->getRow("room_key = '".$reservation_data["info"]["room_key"]."'", "cabinet");
            $reservation_data["info"]["room_option"]["cabinet"] = $room_info["cabinet"];

            $this->setReservationData($reservation_data);
        }
        $this->render_modify();
    }

    /**
     * 予約詳細
     */
    function render_modify($message = "")
    {
        // エラーメッセージ表示
        if ($message && !$this->request->get('back')) {
            $this->template->assign('message', $message);
        }
        // カレンダー
        $_now = time() - (3600 * 24);
        $this->template->assign("start_limit", $_now);
        $this->template->assign("end_limit", $_now + (3600 * 24 * 367));
        $user_info = $this->session->get("user_info");
        // 編集中の詳細
        $reservation_data = $this->getReservationData();
        if(!$reservation_data || $reservation_data["info"]["user_key"] != $user_info["user_key"]){
            header("Location: /services/reservation/yoyaku.php");
        }
        $reservation_data["is_modified"] = 1;
        //初期情報
        if(!isset($reservation_data["has_guest"]))
          $reservation_data["has_guest"] = $reservation_data["guest_flg"];
        if(!isset($reservation_data["mail_send_type"]))
          $reservation_data["mail_send_type"] = 2;
        $this->logger2->debug($reservation_data);
        $room_info = $this->get_room_info($reservation_data["info"]["room_key"]);
        // 対応フォーマット情報取得
        require_once ("classes/N2MY_Document.class.php");
        $objDocument = new N2MY_Document($this->get_dsn());
        $support_document_list = $objDocument->getSupportFormat($reservation_data["info"]["room_key"]);
        // 変更前予約情報
        if (!$this->session->get( "not_change_reservation_data" )) {
            $this->session->set("not_change_reservation_data", $reservation_data);
        }
        $this->template->assign("document_list", join(", ", array_keys($support_document_list["document_list"])));
        $this->template->assign("image_list", join(", ", array_keys($support_document_list["image_list"])));
        $this->template->assign("display_filetype", join(", ", array_keys($support_document_list["image_list"] + $support_document_list["document_list"])));
        $this->template->assign("room_info", $room_info);
        $this->template->assign("is_modified",true);
        $this->template->assign("reservation_data", $reservation_data);
        $this->template->assign("varsion", $this->_getMeetingVersion());
        $this->session->set("delete_documents", $reservation_data["delete_documents"]);
        $this->display('user/yoyaku/update.t.html');
    }

    /**
     * 予約変更確認画面表示
     */
    function action_modify_confirm() {
        $request = $this->request->getAll();
        $reservation_data = $this->formatReservationData($request);
        $old_reservation_data = $this->session->get("not_change_reservation_data");
        $reservation_data["has_guest"] = $old_reservation_data["has_guest"];
        $this->logger2->debug($reservation_data);
        if($reservation_data["guest_flg"] == 0)
          $reservation_data["mail_send_type"] = 0;
        if($reservation_data["mail_send_type"] == 1)
        {
          //招待者情報を変更判別
          $old_guests = $old_reservation_data["guests"];
          foreach($reservation_data["guests"] as $guset_key => $guest_info)
          {
            if(!empty($guest_info["r_user_session"]))
            {
              $flag = 0;
              if($guest_info["name"]!=$old_guests[$guest_info["r_user_session"]]["name"])
                $flag = 1;
              if($guest_info["email"]!=$old_guests[$guest_info["r_user_session"]]["email"])
                $flag = 1;
              if($guest_info["type"]!=$old_guests[$guest_info["r_user_session"]]["type"])
                $flag = 1;
              if($guest_info["timezone"]!=$old_guests[$guest_info["r_user_session"]]["timezone"])
                $flag = 1;
              if($guest_info["lang"]!=$old_guests[$guest_info["r_user_session"]]["lang"])
                $flag =1;
              if($flag == 1)
              {
                $reservation_data["guests"][$guset_key]["is_changed"] = 1;
                $this->logger2->info($guest_info,"modify");
              }
            }
          }
        }
        $this->logger2->debug($reservation_data);
        $this->setReservationData($reservation_data);
        // 入力チェック
        if ($message = $this->check($reservation_data)) {
            return $this->render_modify($message);
        } else {
            return $this->render_modify_confirm();
        }
    }

    /**
     *
     */
    function render_modify_confirm() {
        // 二重登録回避
        $this->set_submit_key($this->_name_space);
        $reservation_data = $this->getReservationData();
        $this->template->assign("not_change_reservation_data", $this->session->get("not_change_reservation_data"));
        $this->template->assign("reservation_data", $reservation_data);
        $this->template->assign("upload_status", $this->getUploadStatus($reservation_data));
        $this->display('user/yoyaku/update_confirm.t.html');
    }

    /**
     * 予約変更完了画面
     */
    function action_modify_complete() {
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_top();
        }
        $reservation_data = $this->getReservationData();
        // 入力チェック
        if ($message = $this->check($reservation_data)) {
            return $this->render_create($message);
        }
        // 資料事前アップロード設定
        $check_documents = $reservation_data["documents"];
        $create_documents = array();
        if (!$reservation_data["upload_flg"]) {
            foreach ($check_documents as $document_id => $document) {
                if (!$check_documents[$document_id]["tmp_name"]) {
                    $create_documents[$document_id] = $document[$document_id];
                }
            }
            $reservation_data["documents"] = $create_documents;
        }
        $reservation_data["info"]["sender_lang"] =  $this->session->get("lang");

        // 開始時間が変更されていたらリマインダーメール送信フラグの更新
        $old_reservation_data = $this->session->get("not_change_reservation_data");
        $ord_start_time = strtotime($old_reservation_data["info"]["reservation_starttime"]);
        $new_start_time = strtotime($reservation_data["info"]["reservation_starttime"]);
        if($ord_start_time != $new_start_time){
            //更新用フラグ
            $reservation_data["start_time_modify"] = 1;
        }else {
            $reservation_data["start_time_modify"] = 0;
        }
        // 更新
        $this->obj_N2MY_Reservation->update($reservation_data);
        // 入力データをクリア
        $this->clearReservationData();
        $this->session->remove("delete_documents");
        $this->action_top($reservation_data["info"]["room_key"]);
    }

    /**
     *
     */
    function sendMail($mail_list, $info, $mode) {
        $this->logger2->info(array($mail_list, $info, $mode));
        // 送信者に送る
        require_once ("lib/EZLib/EZMail/EZSmtp.class.php");
        require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
        // Template
        $template = new EZTemplate($this->config->getAll('SMARTY_DIR'));
        // 招待者に送信
        if( $mail_list ){
            $this->logger->info($mail_list);
            // 時差変換
            foreach($mail_list as $key => $guest) {
                $lang_cd = EZLanguage::getLangCd($guest["lang"]);
                // 件名
                switch ($mode) {
                case "create":
                    $subject = $this->getMessage($lang_cd, "DEFINE", "RESERVATION_MAIL_SUBJECT");
                    break;
                case "modify":
                    $subject = $this->getMessage($lang_cd, "DEFINE", "RESERVATION_MAIL_MODIFY_SUBJECT");
                    break;
                case "delete":
                    $subject = $this->getMessage($lang_cd, "DEFINE", "RESERVATION_MAIL_DELETE_SUBJECT");
                    break;
                case "addlist" :
                    $subject = $this->getMessage($lang_cd, "DEFINE", "RESERVATION_MAIL_ADDLIST_SUBJECT");
                    break;
                case "deletelist":
                    $subject = $this->getMessage($lang_cd, "DEFINE", "RESERVATION_MAIL_DELLIST_SUBJECT");
                    break;
                case "modify_pw":
                    $subject = $this->getMessage($lang_cd, "DEFINE", "RESERVATION_MAIL_MODIFY_PW_SUBJECT");
                    break;
                }
                if ($guest["timezone"] == "100") {
                    $time_zone = $info["reservation_place"];
                } else {
                    $time_zone = $guest["timezone"];
                }
                $mail_list[$key]["timezone"] = $time_zone;
                $mail_list[$key]["starttime"] = date("Y-m-d H:i:s" ,EZDate::getLocateTime($info["reservation_starttime"], $time_zone, N2MY_SERVER_TIMEZONE));;
                $mail_list[$key]["endtime"]   = date("Y-m-d H:i:s" ,EZDate::getLocateTime($info["reservation_endtime"], $time_zone, N2MY_SERVER_TIMEZONE));;
                $subject .= " [" . $info['reservation_name'] . "]";
                $mail = new EZSmtp(null, $lang_cd, "UTF-8");
                $mail->setSubject($subject);
                $mail->setFrom(RESERVATION_FROM, $info["sender"]);
                $mail->setReturnPath($info["sender_mail"]);
                $mail->setTo($guest["email"], $guest["name"]);
                // 本文
                $info["reservation_address"] = sprintf( "%sr/%s&c=%s", N2MY_BASE_URL, $guest["r_user_session"], $guest["lang"] );
                $template->assign("base_url", N2MY_BASE_URL);
                $template->assign("info", $info);
                $template->assign("guest", $mail_list[$key]);
                $template_file = $lang_cd."/mail/reservation/user/reservation_" . $mode . ".t.txt";
                $body = $template->fetch($template_file);
                $this->logger2->debug(array($guest["email"], $body));
                $mail->setBody($body);
                $mail->send();
            }
        }
        switch ($mode) {
            case "create":
                $subject = $this->getMessage($this->_lang, "DEFINE", "RESERVATION_OWNER_SUBJECT");
                break;

            case "modify":
                $subject = $this->getMessage($this->_lang, "DEFINE", "RESERVATION_OWNER_MODIFY_SUBJECT");
                break;

            case "delete" :
                $subject = $this->getMessage($this->_lang, "DEFINE", "RESERVATION_OWNER_DELETE_SUBJECT");
                break;

            case "addlist":
                $subject = $this->getMessage($this->_lang, "DEFINE", "RESERVATION_OWNER_ADDLIST_SUBJECT");
                break;

            case "deletelist":
                $subject = $this->getMessage($this->_lang, "DEFINE", "RESERVATION_OWNER_DELLIST_SUBJECT");
                break;

            case "modify_pw" :
                $subject = $this->getMessage($this->_lang, "DEFINE", "RESERVATION_OWNER_MODIFY_PW_SUBJECT");
                break;
        }
        $subject = $subject . " [" . $info['reservation_name'] . "]";
        $mail = new EZSmtp(null, $this->_lang, "UTF-8");
        $mail->setSubject($subject);
        $mail->setFrom(RESERVATION_FROM);
        $mail->setReturnPath(NOREPLY_ADDRESS);
        $mail->setTo($info["sender_mail"], $info["sender"]);
        // 本文
        // 時差変換
        $template->assign("info", $info);
        $template->assign("guests", $mail_list);
        $template->assign("base_url", N2MY_BASE_URL);
        $template_file = $this->_lang."/mail/reservation_" . $mode . ".t.txt";
        $body = $template->fetch($template_file);
        $this->logger2->debug(array($info["sender_mail"], $body));
        $mail->setBody($body);
        $mail->send();
    }

    function getMessage($lang, $group, $id) {
        static $message;
        if (!$message[$lang]) {
            $config_file = N2MY_APP_DIR."config/lang/".$lang."/message.ini";
            $message[$lang] = parse_ini_file($config_file, true);
        }
        return $message[$lang][$group][$id];
    }

    function action_delete_list() {
        $reservation_session_ids = $this->request->get("reservation_session_id");
        foreach($reservation_session_ids as $reservation_session_id) {
            $this->obj_N2MY_Reservation->cancel($reservation_session_id);
        }
        $this->render_list();
    }

    /**
     * 予約削除確認画面
     */
    function action_delete_confirm()
    {
      // 二重登録回避
      $this->set_submit_key($this->_name_space);
      $member_info = $this->session->get( "member_info" );
        $user_info = $this->session->get( "user_info" );
        $reservation_session = $this->request->get('reservation_session');
        $reservation_data = $this->obj_N2MY_Reservation->getDetail($reservation_session);
        if(!$reservation_data || $reservation_data["info"]["user_key"] != $user_info["user_key"]){
            header("Location: /services/reservation/yoyaku.php");
        }
        if (!$this->isAuthorized($reservation_data, 'yoyaku.php?action_delete_confirm=&reservation_session='.$reservation_session)) {
            return false;
        }
        //部屋のオプション確認
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $reservation_data["info"]["room_option"] = $obj_N2MY_Account->getRoomOptionList($reservation_data["info"]["room_key"]);
        require_once ("classes/dbi/room.dbi.php");
        $room_obj = new RoomTable($this->get_dsn());
        $room_info = $room_obj->getRow("room_key = '".$reservation_data["info"]["room_key"]."'", "cabinet");
        $reservation_data["info"]["room_option"]["cabinet"] = $room_info["cabinet"];
        $this->setReservationData($reservation_data);
        if(($user_info["account_model"] == "member" || $user_info["account_model"] == "centre") &&
            $member_info["room_key"] != $reservation_data["info"]["room_key"] ){
            $reservation_data["info"]["status"] = "end";
        }
        $this->setReservationData($reservation_data);
        $this->template->assign("reservation_data", $reservation_data);
        $this->template->assign("is_detail", true);

        $this->display('user/yoyaku/delete_confirm.t.html');
    }

    /**
     * 予約削除完了画面
     */
    function action_delete_complete() {
        // 二重処理チェック
        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_top();
        }
        $reservation_data = $this->getReservationData();
        $this->logger->debug($reservation_data);
        $reservation_session = $reservation_data["info"]["reservation_session"];
        // キャンセル
        $this->obj_N2MY_Reservation->cancel($reservation_session);
        // メール送信
        if ($reservation_data["guest_flg"]) {
            $this->sendMail($reservation_data["guests"], $reservation_data["info"], "delete");
        }
        // 入力データをクリア
        $this->clearReservationData();
        // 登録完了（一覧表示）
        $this->action_top($reservation_data["info"]["room_key"]);
    }

    /**
     * パスワード確認処理
     */
    function action_pw_check()
    {
        $this->logger->trace(__FUNCTION__."#start",__FILE__,__LINE__);
        $this->_reservation->checkLogin();
        $data = $this->_reservation->checkPw();
        if (($data['reservation_pw'] == "") ||
            ($data['reservation_pw'] === $this->request->get('reservation_pw'))) {
            // 名前空間取得
            $name_space = $this->request->get("ns");
            // リダイレクト先取得
            $redirect_url = $this->session->get("redirect_url", $name_space);
            // セッションから消去
            $this->session->remove("redirect_url", $name_space);
            header("Location: ".$redirect_url);
            return;
        }
        return $this->render_login();
    }

    /**
     * ログイン処理
     */
    function action_login()
    {
        $this->logger->trace(__FUNCTION__."#start",__FILE__,__LINE__);
        $this->_reservation->checkLogin();
        $data = $this->_reservation->checkPw();
        // パスワード存在確認
        if (!$data['reservation_pw']) {
            // 名前空間取得
            $name_space = $this->request->get("ns");
            // リダイレクト先取得
            $redirect_url = $this->session->get("redirect_url", $name_space);
            // セッションから消去
            $this->session->remove("redirect_url", $name_space);
            header("Location: ".$redirect_url);
            return;
        }
        return $this->render_login();
    }

    /**
     * ログイン画面表示
     */
    function render_login()
    {
        $this->logger->trace(__FUNCTION__."#start",__FILE__,__LINE__);
        $this->template->assign('ns', $this->request->get("ns"));
        $this->template->assign('reservation_key', $this->request->get("reservation_key"));
        $this->display('user/yoyaku/login.t.html');
    }

    /**
     * デフォルトページ
     */
    function default_view()
    {
        $this->action_top();
    }

    /**
     * カレンダーのデータ作成
     */
    function _calender($year, $month, $limit = 1)
    {
        $EZDate = new EZDate();
        $_now = $EZDate->getLocateTime(time(), $this->session->get("time_zone"), N2MY_SERVER_TIMEZONE);
        $now = mktime(0,0,0,date("m",$_now),date("d",$_now),date("Y",$_now));
        $limit_date = mktime(0,0,0,date("m",$_now) + $limit, date("d",$_now), date("Y",$_now));
        // 初日の曜日
        $fast_week = date("w", mktime(0,0,0,$month,1, $year));
        $last_day = date("t", mktime(0,0,0,$month,1, $year));
        $week = array();

        for($i = 0, $j = 0, $w = 0, $flg = 0, $num = 1; $i < 42; $i++) {
            // 開始
            if ($fast_week == $i) {
                $flg = 1;
            } elseif ($last_day + 1 == $num) {
                $flg = 2;
                $num = 1;
            }
            if (($i > 0) && ($i % 7) == 0) {
                $j ++;
                $w = 0;
            }
            switch ($flg) {
            // 当月分
            case 1:
                $_day = mktime(0,0,0,$month,$num,$year);
                // 無効
                if (($now > $_day) || ($_day > $limit_date)) {
                    $week[$j][$w]["type"] = "1";
                // 有効
                } else {
                    $week[$j][$w]["type"] = "2";
                }
                $week[$j][$w]["num"] = $num;
                $num++;
                break;
            // 次月分
            case 2:
                $week[$j][$w]["type"] = "3";
                $week[$j][$w]["num"] = $num;
                $num++;
                break;
            default :
                $week[$j][$w]["type"] = "0";
                $week[$j][$w]["num"] = "";
            }
            $w++;
        }
        return $week;
    }

    /**
     * スケジュール一覧取得
     */
    function _render_schedule_list($room_key, $start_datetime, $end_datetime)
    {
        $target_date = $start_datetime;
        if ($end_datetime) {
            $next_date = $end_datetime;
        } else {
            $next_date = $target_date + (3600 * 24);
        }
        $_start_date = date("Y-m-d 00:00:00", $target_date);
        $_end_date = date("Y-m-d 00:00:00", $next_date);
        $where = "room_key = '$room_key'" .
                " AND reservation_status = 1" .
                " AND (" .
                "( reservation_starttime between '$_start_date' AND '$_end_date' )" .
                " OR ( reservation_endtime between '$_start_date' AND '$_end_date' )" .
                " OR ( '$_start_date' BETWEEN reservation_starttime AND reservation_endtime )" .
                " OR ( '$_end_date' BETWEEN reservation_starttime AND reservation_endtime ) )";
        $list = $this->_reservation_obj->getList($where, array("reservation_starttime" => "ask"));
        $this->template->assign("start_datetime", $start_datetime);
        $this->template->assign("end_datetime", $end_datetime - (3600 * 24));
        $this->template->assign("list", $list);
        $content = $this->fetch("user/yoyaku/schedule.t.html");
        return $content;
    }

    /**
     * スケジュール一覧の表示
     */
    function render_list()
    {
        $date_obj = new EZDate();
        // 日付計算
        $_target = $this->request->get("time", time());
        $target = $date_obj->getLocateTime($_target, $this->session->get('time_zone'), N2MY_SERVER_TIMEZONE);
        $target = mktime(0,0,0,date("m", $target),date("d", $target), date("Y", $target));
        $weekend = $target + (3600 * 24 * 6); // 次週
        $this->template->assign('start', date('Y/m/d', $target));
        $this->template->assign('end', date('Y/m/d', $weekend));
        $this->template->assign('weekend', $weekend);
        $this->template->assign('prev', $_target - (3600 * 24 * 7));
        $this->template->assign('yesterday', $_target - (3600 * 24 * 1));
        $this->template->assign('now', mktime(0,0,0,date("m"),date("d"),date("Y")));
        $this->template->assign('tomorrow', $_target + (3600 * 24 * 1));
        $this->template->assign('next', $_target + (3600 * 24 * 7));
        $table = null;
        // １週間分のスケジュール
        $schedule = $target;
        for ($w = 0; $w < 7; $w++) {
            $week[] = array(
                "date"      => $schedule,
                "weekday"   => date("w", $schedule)
            );
            $schedule = $schedule + (3600 * 24);
        }
        $this->template->assign('week', $week);
        // 利用可能な会議室一覧
        $user_info = $this->session->get('user_info');
        // 部屋一覧取得
//        $room_list = $this->get_room_info();
        $room_list =  $this->session->get("room_info");
        $target = $date_obj->getLocateTime($target, N2MY_SERVER_TIMEZONE ,$this->session->get('time_zone'));
        foreach($room_list as $room_key => $room_info) {
            // 会議室一覧取得
            $schedule = $target;
            // 一覧取得
            $next_day = $schedule + (3600 * 24 * 7);
            $options = array(
                "start_time" => date("Y-m-d H:i:s", $schedule),
                "end_time" => date("Y-m-d H:i:s", $next_day),
                "sort_key" => "reservation_starttime",
                "sort_type" => "asc",
                );
            $rows = $this->obj_N2MY_Reservation->getList($room_key, $options, $this->session->get('time_zone'));
            $room = array();
            foreach ($rows as $_key => $row) {
                $room[] = array(
                    "session"   => $row["reservation_session"],
                    "from"      => $row["reservation_starttime"],
                    "to"        => $row["reservation_endtime"],
                    "name"      => $row["reservation_name"],
                );
            }
            $rooms[$room_key] = $room;
        }
        $this->template->assign('rooms', $rooms);
        $this->display("user/yoyaku/weekly.t.html");
    }

    private function render_participantedList()
    {
        $date_obj = new EZDate();
        // 日付計算
        $_target = $this->request->get("time", time());
        $target = $date_obj->getLocateTime($_target, $this->session->get('time_zone'), N2MY_SERVER_TIMEZONE);
        $target = mktime(0,0,0,date("m", $target),date("d", $target), date("Y", $target));
        $weekend = $target + (3600 * 24 * 6); // 次週
        $this->template->assign('start', date('Y/m/d', $target));
        $this->template->assign('end', date('Y/m/d', $weekend));
        $this->template->assign('weekend', $weekend);
        $this->template->assign('prev', $_target - (3600 * 24 * 7));
        $this->template->assign('yesterday', $_target - (3600 * 24 * 1));
        $this->template->assign('now', mktime(0,0,0,date("m"),date("d"),date("Y")));
        $this->template->assign('tomorrow', $_target + (3600 * 24 * 1));
        $this->template->assign('next', $_target + (3600 * 24 * 7));
        $table = null;
        // １週間分のスケジュール
        $schedule = $target;
        for ($w = 0; $w < 7; $w++) {
            $week[] = array(
                "date"      => $schedule,
                "weekday"   => date("w", $schedule)
            );
            $schedule = $schedule + (3600 * 24);
        }
        $this->template->assign('week', $week);
        // 利用可能な会議室一覧
        $user_info = $this->session->get('user_info');
        $member_info = $this->session->get('member_info');
        $room_info =  $this->session->get("room_info");

        $target = $date_obj->getLocateTime($target, N2MY_SERVER_TIMEZONE ,$this->session->get('time_zone'));
        $room["name"] = $room_info[$member_info["room_key"]]["room_info"]["room_name"];

        $schedule = $target;
        // 一覧取得
        $next_day = $schedule + (3600 * 24 * 7);
        $options = array(
            "start_time" => date("Y-m-d H:i:s", $schedule),
            "end_time" => date("Y-m-d H:i:s", $next_day),
            "sort_key" => "reservation_starttime",
            "sort_type" => "asc",
            );
        $rows = $this->obj_N2MY_Reservation->getParticipantedMeetingList( $member_info, $options, $this->session->get('time_zone'), $room_info);
        $day = array();
        if ($rows) {
            $room = array();
            foreach ($rows as $_key => $row) {
                $room[] = array(
                    "session"   => $row["reservation_session"],
                    "from"      => $row["reservation_starttime"],
                    "to"        => $row["reservation_endtime"],
                    "name"      => $row["reservation_name"],
                );
            }
            $rooms[$member_info["room_key"]] = $room;
            $this->logger->trace("reservation", __FILE__, __LINE__, $row);
        }

        $this->template->assign('rooms', $rooms);
        $this->display("user/yoyaku/weekly.t.html");
    }

    /**
     * 予約入力チェック
     */
    function reservation_check($room_key, $starttime, $endtime, $reservation_key = null)
    {
        $ret = $this->_reservation_obj->getListBetween($room_key, $starttime, $endtime, $reservation_key);
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, $ret);
        $message = "";
        if (count($ret) > 0) {
            $message = '<li>'.RESERVATION_ERROR_TIME;
            foreach ($ret as $_key => $row) {
                $message .= "<br/>&nbsp;&nbsp;&nbsp;[ " . htmlspecialchars($row['reservation_name']) . " ] " .
                    date("Y/m/d H:i", strtotime($row['reservation_starttime'])) . " - " .
                    date("Y/m/d H:i", strtotime($row['reservation_endtime']));
            }
            $message .= '</li>';
        }
        return $message;
    }

    /**
     * レイアウトタイプ
     */
    function get_layout_type($room_key) {
        require_once ("classes/dbi/room.dbi.php");
        $room_obj = new RoomTable($this->get_dsn());
        $now_row = $room_obj->get_detail($room_key);
        if ($now_row["room_layout_type"]) {
            return $now_row["room_layout_type"];
        } else {
            return "1";
        }
    }

    /**
     * 入力フォーマットを整形
     */
    private function formatReservationData($reservation_data) {
        if ($reservation_data["info"]) {
            $request = $reservation_data["info"];
            // 開始日
            if ($request['startDate']) {
                $starttime = $request['startDate'] . " ".
                    $request['startHour'] . ":".
                    $request['startMin'] . ":00";
                // 終了日
                $endtime = $request['endDate'] . " ".
                    $request['endHour'] . ":".
                    $request['endMin'] . ":00";
                $reservation_data["info"]["reservation_starttime"] = $starttime;
                $reservation_data["info"]["reservation_endtime"] = $endtime;
            }
            $reservation_data["info"]["send_flg"] = isset($request["send_flg"]) ? 1 : 0;
        }
        $data = $this->getReservationData();
        $this->logger2->debug($data["info"]);
        $this->logger2->debug(date('Y-m-d H:i:s', $data["info"]["reservation_starttime"]));
        // パスワード無し
        if ($reservation_data["pw_flg"] == 0) {
            $reservation_data["info"]["reservation_pw"] = "";
            $reservation_data["info"]['reservation_pw_confirm'] = "";
        // パスワード保持
        } elseif (isset($data["info"]["reservation_key"]) && $reservation_data["pw_flg"] == 2) {
            $reservation_data["info"]["reservation_pw"]         = $data["info"]["old_reservation_pw"];
            $reservation_data["info"]["reservation_pw_confirm"] = $data["info"]["old_reservation_pw"];
        }
        require_once("lib/EZLib/EZUtil/EZArray.class.php");
        $objArray = new EZArray();
        $reservation_data["info"] = $objArray->mergeRecursiveReplace($data["info"], $reservation_data["info"]);
        // マルチカメラの利用制限
        if ($reservation_data["info"]["is_multicamera"]) {
            $reservation_data["info"]["is_telephone"] = 0;
            $reservation_data["info"]["is_h323"] = 0;
            $reservation_data["info"]["is_mobile_phone"] = 0;
        }
        // ゲスト
        $guests = array();
        if (isset($reservation_data["guests"])) {
            require_once('lib/EZLib/EZUtil/EZLanguage.class.php');
            // 件数
            $count = count($reservation_data["guests"]["name"]);
            for($i = 0; $i < $count; $i++) {
                if ($reservation_data["guests"]["email"][$i]) {
                    if(!is_numeric($reservation_data["guests"]["timezone"][$i])){
                        $reservation_data["guests"]["timezone"][$i] = 100;
                    }

                    if(!array_key_exists($reservation_data["guests"]["lang"][$i], $this->get_language_list())) {
                        $lang_cd = $this->get_language();
                        $guest_lang = EZLanguage::getLang($lang_cd);
                        $user_info = $this->session->get('user_info');
                        $this->logger2->warn(
                            array(
                                'user_key'                => (isset($user_info['user_key']) ? $user_info['user_key'] : ''),
                                'requested guest lang'    => $reservation_data['guests']['lang'][$i],
                                'user-side lang_allow'    => (isset($user_info['lang_allow']) ? $user_info['lang_allow'] : ''),
                                'language list'           => array_keys($this->get_language_list()),
                                'lang_cd'                 => $lang_cd,
                            ),
                            "No appropriate language found. Trying to use '$guest_lang' for guest invitation."
                        );
                    }
                    else {
                        $guest_lang = $reservation_data["guests"]["lang"][$i];
                    }
                    $guests[] = array(
                        "r_user_session"  => $reservation_data["guests"]["r_user_session"][$i],
                        "name"            => $reservation_data["guests"]["name"][$i],
                        "email"           => $reservation_data["guests"]["email"][$i],
                        "type"            => $reservation_data["guests"]["type"][$i],
                        "timezone"        => $reservation_data["guests"]["timezone"][$i],
                        "lang"            => $guest_lang,
                    );
                }
            }
        }
        if(!$reservation_data["guest_flg"]){
            $reservation_data["info"]["is_reminder_flg"] = 0;
        }
        $reservation_data["guests"] = $guests;
        // ファイル一覧
        $filename     = ($reservation_data["filename"]) ? $reservation_data["filename"] : array();
        $sortlist     = ($reservation_data["sort"]) ? $reservation_data["sort"] : array();
        $doc_format   = ($reservation_data["format"]) ? $reservation_data["format"] : array();
        $doc_version  = ($reservation_data["version"]) ? $reservation_data["version"] : array();
        $_documents = isset($data["documents"]) ? $data["documents"] : null;
        $documents = array();
        $delete_documents = array();
        $session_delete_documents = $this->session->get("delete_documents");
        if (isset($session_delete_documents)) {
            foreach ($session_delete_documents as $document_id => $document) {
                if (!$reservation_data["upload_flg"]) {
                    $documents[$document_id] = $session_delete_documents[$document_id];
                    $documents[$document_id]["not_delete"] = "true";
                } else {
                    $delete_documents["$document_id"] = $session_delete_documents[$document_id];
                }
            }
        }
        $this->logger2->debug($delete_documents);
        if (isset($_documents)) {
            foreach($_documents as $document_id => $document) {
                if (array_key_exists($document_id, $filename)) {
                    if ($reservation_data["upload_flg"] || !$_documents[$document_id]["tmp_name"] ||
                        !$reservation_data["upload_flg"] && $_documents[$document_id]["tmp_name"]) {
                        $documents[$document_id] = $_documents[$document_id];
                        $documents[$document_id]["name"] = $filename[$document_id];
                        $documents[$document_id]["sort"] = $sortlist[$document_id];
                        $documents[$document_id]["upload_status"] = true;
                    }
                } else {
                    // アップロード済み資料
                    if (isset($_documents[$document_id]["document_id"])) {
                        // 削除予定の資料
                        if ($reservation_data["upload_flg"]) {
                            $documents[$document_id] = $_documents[$document_id];
                            $documents[$document_id]["delete"] = true;
                            $delete_documents[$document_id] = $_documents[$document_id];
                        } else {
                            $documents[$document_id] = $_documents[$document_id];
                            $documents[$document_id]["sort"] = $sortlist[$document_id];
                            $documents[$document_id]["not_delete"] = true;
                        }
                    // フォームの資料
                    } elseif (file_exists($_documents[$document_id]["tmp_name"])) {
                        unlink($_documents[$document_id]["tmp_name"]);
                    }
                }
            }
        }
        $room_info = $this->get_room_info($data["info"]["room_key"]);
        // 対応フォーマット情報取得
        require_once ("classes/N2MY_Document.class.php");
        $objDocument = new N2MY_Document($this->get_dsn());
        $support_document_list = $objDocument->getSupportFormat($data["info"]["room_key"]);
        if(isset($_FILES["document"])) {
            $count = count($_FILES["document"]["name"]);
            for ($i = 0; $i < $count; $i++) {
                $fileinfo = pathinfo($_FILES["document"]["name"][$i]);
                $string = new EZString();
                $document_id = $string->create_id();
                $error = $_FILES["document"]["error"][$i];
                // 追加エラー処理
                if ($error === 0) {
                    // 拡張子判定
                    if (!in_array(strtolower($fileinfo["extension"]), $support_document_list['support_ext_list'])) {
                        $error = 1000;
                    // 空のファイル
                    } elseif ($_FILES["document"]["size"][$i] == 0) {
                        $error = 4;
                    // 容量制限
                    } else {
                        if (in_array(strtolower($fileinfo["extension"]), $support_document_list['image_ext_list'])) {
                            $maxsize = ($room_info["room_info"]["whiteboard_size"] <= 5) ? $room_info["room_info"]["whiteboard_size"] : 5;
                        } else {
                            $maxsize = isset($room_info["room_info"]["whiteboard_size"]) ? $room_info["room_info"]["whiteboard_size"] : 20;
                        }
                        if ($_FILES["document"]["size"][$i] > $maxsize * 1024 * 1024) {
                            $error = 4;
                        }
                    }
                }
                $tmp_name = "";
                if ($error === 0) {
                    $dir = $this->get_work_dir();
                    $tmp_name = $dir."/".$document_id.".".$fileinfo["extension"];
                    move_uploaded_file($_FILES["document"]["tmp_name"][$i], $tmp_name);
                    $name = ($filename[$i]) ? $filename[$i] : $_FILES["document"]["name"][$i];
                    $sort = $sortlist[$i];
                    $convet_format = ($doc_format[$i]) ? $doc_format[$i] : "bitmap";
                    $flash_version = ($doc_version[$i]) ? $doc_version[$i] : "";
                    $documents[$document_id] = array(
                        "name"        => $name,
                        "type"        => $_FILES["document"]["type"][$i],
                        "tmp_name"    => $tmp_name,
                        "error"       => $error,
                        "size"        => $_FILES["document"]["size"][$i],
                        "sort"        => $sort,
                        "format"      => $convet_format,
                        "version"     => $flash_version,
                    );
                }
            }
        }
        // 資料をソート
        $sort_arr = array();
        foreach($documents AS $uniqid => $row){
            foreach($row AS $key=>$value){
                $sort_arr[$key][$uniqid] = $value;
            }
        }
        if ($documents) {
            array_multisort($sort_arr["sort"], SORT_ASC, $documents);
        }

        $this->logger2->debug($documents);
        $reservation_data["documents"] = $documents;
        $reservation_data["delete_documents"] = $delete_documents;
        $this->logger2->debug($reservation_data);
        return $reservation_data;
    }

    /**
     * 入力内容を維持
     */
    private function setReservationData($reservation_data) {
        $this->session->set("reservation_data", $reservation_data);
    }

    /**
     * 設定されている値を取得
     */
    private function getReservationData() {
        return $this->session->get("reservation_data");
    }

    /**
     * 入力内容をクリア
     */
    private function clearReservationData() {
        return $this->session->remove("reservation_data");
    }

    /**
     * 入力チェック
     */
    function check($reservation_data, $method = "") {
        $reservation_info = $reservation_data["info"];
        $room_key = $reservation_info["room_key"];
        if ($method != "delete") {
            // パスワードチェック
            $message = "";
            $this->logger2->debug($reservation_info);
            if ($reservation_data['pw_flg'] == 1) {
                if (!$reservation_info['reservation_pw'] && !$reservation_info['reservation_pw_confirm']) {
                    $this->logger2->info(array($reservation_info['reservation_pw'], $reservation_info['reservation_pw_confirm']));
                    $message .= '<li>'.RESERVATION_ERROR_PW . '</li>';
                } elseif ( 0 != strcmp( $reservation_info['reservation_pw'], $reservation_info['reservation_pw_confirm'] )) {
                    $message .= '<li>'.RESERVATION_ERROR_DISAGREE_PW . '</li>';
                } elseif (!ctype_alnum($reservation_info['reservation_pw'])) {
                    $message .= '<li>'.RESERVATION_ERROR_INVALIDURL_PW . '</li>';
                } elseif (strlen($reservation_info['reservation_pw']) < 6) {
                    $message .= '<li>'.RESERVATION_ERROR_SHORTLENGTHL_PW . '</li>';
                } elseif (strlen($reservation_info['reservation_pw']) > 16) {
                    $message .= '<li>'.RESERVATION_ERROR_LONGLENGTHL_PW . '</li>';
                }
            }
            //会議名チェック
            if (mb_strlen($reservation_info['reservation_name']) > 100) {
                $message .= '<li>'.RESERVATION_ERROR_NAME_LENGTH . '</li>';
            }

            // 開始、終了時間をチェック
            $limit_start_time = time() - (2 * 24 * 3600); // サーバーの現地時刻で２日前
            $limit_end_time = time() + (367 * 24 * 3600); // サーバーの現地時刻で１年後の次の日
            $_starttime = EZDate::getLocateTime($reservation_info['reservation_starttime'], N2MY_SERVER_TIMEZONE, $reservation_info["reservation_place"]); // 比較対照をサーバーの地域にあわせる
            $_endtime = EZDate::getLocateTime($reservation_info['reservation_endtime'], N2MY_SERVER_TIMEZONE, $reservation_info["reservation_place"]); // 比較対照をサーバーの地域にあわせる
            if ($_starttime <= $limit_start_time) {
                $message .= '<li>'.RESERVATION_ERROR_STARTTIME_INVALID.'</li>';
            } elseif ($_endtime >= $limit_end_time) {
                $message .= '<li>'.RESERVATION_ERROR_ENDTIME_INVALID.'</li>';
            }
            //終了時間が現時刻より前だった場合のエラー処理
            elseif ($_endtime < time() ) {
                $message .= '<li>'.RESERVATION_ERROR_ENDTIME_INVALID.'</li>';
            } else {
                $message .= $this->obj_N2MY_Reservation->check($room_key, $reservation_data);
            }
            // メンテナンス時間をチェック
            if ($this->config->get("N2MY", "maintenance_notification")) {
                if (N2MY_MDB_DSN) {
                    require_once('classes/core/dbi/Notification.dbi.php');
                    $objNotification = new NotificationTable(N2MY_MDB_DSN);
                    $count = $objNotification->check(date("Y-m-d H:i:s", $_starttime), date("Y-m-d H:i:s", $_endtime));
                    if ($count > 0) {
                        $message .= '<li>'.RESERVATION_ERROR_MEINTENANCE_TIME.'</li>';
                    }
                }
            }
        }
        $room_info = $this->get_room_info($room_key);
        if ($reservation_data["guest_flg"]) {
            // メール
            if (!$reservation_info['sender']) {
                $message .= '<li>'.RESERVATION_ERROR_SENDER_NAME . '</li>';
            } elseif (mb_strlen($reservation_info['sender']) > 50) {
                $message .= '<li>'.RESERVATION_ERROR_SENDER_NAME_LENGTH . '</li>';
            }
            if (!$reservation_info['sender_mail']) {
                $message .= '<li>'.RESERVATION_ERROR_SENDER_MAIL . '</li>';
            } elseif (!EZValidator::valid_email($reservation_info['sender_mail'])) {
                $message .= '<li>'.RESERVATION_ERROR_SENDER_INVALIDMAIL . '</li>';
            }
            // 送信者
            if (mb_strlen($reservation_info['mail_body']) > 10000) {
                $message .= '<li>'.RESERVATION_ERROR_MAIL_BODY_LENGTH . '</li>';
            }
            // 人数制限
            $normal_num = 0;
            $audience_num = 0;
            foreach ($reservation_data['guests'] as $guest) {
                if ($guest["email"]) {
                    if ($guest["type"] == 1) {
                        $audience_num++;
                    } else {
                        $normal_num++;
                    }
                }
            }
            if (!$reservation_info["reservation_key"] && ( $normal_num + $audience_num ) == 0) {
                $message .= '<li>'.INVITATION_ERROR_NOUSER.'</li>';
            }
            if ($room_info["room_info"]["max_seat"] < $normal_num) {
                $this->logger2->debug(array($room_info["room_info"], $normal_num, $audience_num));
                $message .= '<li>'.sprintf(RESERVATION_ERROR_INVITE_NORMAL, $room_info["room_info"]["max_seat"]) . '</li>';
            }
            if ($room_info["room_info"]["max_audience_seat"] < $audience_num) {
                $this->logger2->debug(array($room_info["room_info"], $normal_num, $audience_num));
                $message .= '<li>'.sprintf(RESERVATION_ERROR_INVITE_AUDIENCE, $room_info["room_info"]["max_audience_seat"]). '</li>';
            }
            foreach ($reservation_data["guests"] as $_key => $guest) {
                if (mb_strlen($guest['name']) > 50) {
                    $message .= '<li>'.RESERVATION_ERROR_GUEST_NAME_LENGTH . '</li>';
                }
            }
        }
        // 事前ファイルアップロード
        if ($reservation_data["upload_flg"]) {
            if(isset($_FILES["document"])) {
                // 対応フォーマット取得
                require_once ("classes/N2MY_Document.class.php");
                $objDocument = new N2MY_Document($this->get_dsn());
                $support_document_list = $objDocument->getSupportFormat($room_key);
                foreach($_FILES["document"]["name"] as $key => $filename) {
                    if ($filename) {
                        $file_param = pathinfo($filename);
                        $file_extension = strtolower($file_param["extension"]);
                        if (!in_array($file_extension, $support_document_list['support_ext_list'])) {
                            $this->logger2->info(array($file_param["extension"], $support_document_list['support_ext_list']));
                            $message .= '<li>'.$this->get_message("ERROR", "filetype"). '</li>';
                        } else {
                            // 画像
                            if (in_array($file_extension, $support_document_list['image_ext_list'])) {
                                $maxsize = ($room_info["room_info"]["whiteboard_size"] <= 5) ? $room_info["room_info"]["whiteboard_size"] : 5;
                            // 資料
                            } else {
                                $maxsize = ($room_info["room_info"]["whiteboard_size"] <= 20) ? $room_info["room_info"]["whiteboard_size"] : 20;
                            }
                            if ($_FILES["document"]["size"][$key] > $maxsize * 1024 * 1024) {
                                $message .= '<li>'.$this->get_message("ERROR", "maxfilesize"). '</li>';
                            }
                            // 主に upload_max_filesize 以上の大きさのファイルがアップロードされた場合
                            else if ($_FILES["document"]["error"][$key] == UPLOAD_ERR_INI_SIZE) {
                                $message .= '<li>' . $this->get_message("ERROR", "maxfilesize") . '</li>';
                            }
                        }
                    }
                }
            }
            // アップロード済みの資料名
            foreach($reservation_data['documents'] as $document_id => $document) {
                if (!$document['name']) {
                    $message .= '<li>'.RESERVATION_ERROR_DOCUMENT_NAME. '</li>';
                    break;
                }
            }
        }
        // 入力エラー
        if ($message) {
            $this->logger2->info(array($reservation_data, $message));
        }
        return $message;
    }

    function action_wb_status() {
        $reservation_data = $this->getReservationData();
        $meeting_key = $reservation_data["info"]["meeting_key"];
        $this->logger2->debug($reservation_data);
        $rows = $this->obj_N2MY_Reservation->getDocumentList($meeting_key);
        if (DB::isError($rows)) {
            $this->logger2->error($rows->getUserInfo());
            return false;
        }
        $document_list = array();
        foreach($rows as $document) {
            $document_id = $document["document_id"];
            $document_list[] = array(
                "document_id"     => $document["document_id"],
                "name"            => $document["document_name"],
                "size"            => $document["document_size"],
                "category"        => $document["document_category"],
                "extension"       => $document["document_extension"],
                "status"          => $document["document_status"],
                "index"           => $document["document_index"],
                "sort"            => $document["document_sort"],
                "create_datetime" => $document["create_datetime"],
                "update_datetime" => $document["update_datetime"],
            );
        }
        print json_encode($document_list);
    }

    function action_wb_view() {
        $document_id = $this->request->get("document_id");
        $page = $this->request->get("page", 1);
        $reservation_data = $this->getReservationData();
        $this->logger2->debug($reservation_data);
        $meeting_key = $reservation_data["info"]["meeting_key"];
        if ($document_data = $this->obj_N2MY_Reservation->getDocumentData($meeting_key, $document_id)) {
            $this->logger2->debug($document_data);
            $file = N2MY_DOC_DIR.$document_data["document_path"].$document_id."/".$document_id."_".$page.".jpg";
            if (file_exists($file)) {
                header("Content-Type: image/jpg;");
                header("Cache-Control: public, must-revalidate");
                print file_get_contents($file);
                exit();
            }
        }
        print "error!";
    }

    /**
     * プレビュー取得
     */
    public function action_show_preview()
    {
        $request = $this->request->getAll();
        require_once ("classes/N2MY_Document.class.php");
        $objDocument = new N2MY_Document( $this->get_dsn() );
        $documentInfo = $objDocument->getDetail( $request["document_id"] );
        if ($documentInfo["document_format"] == "vector") {
            $return["fileName"] = sprintf( "%s_%d.swf", $documentInfo["document_id"], $request["index"] );
            $path = N2MY_DOC_DIR.$documentInfo["document_path"].$documentInfo["document_id"]."/".$return["fileName"];
            if (file_exists($path)){
                header('Content-type: application/x-shockwave-flash');
                header('Content-Length: '.filesize($path));
                $fp = fopen($path, "r");
                if ($fp) {
                    while (!feof($fp)) {
                        $buffer = fgets($fp, 4096);
                        echo $buffer;
                    }
                    fclose($fp);
                }
            }
        } else {
            $fileInfo = $objDocument->getPreviewFileInfo( $documentInfo, $request["index"] );
            //resize
            $max_width = 480;
            $max_height = 320;
            $percent = 0.5;

            list($width, $height) = getimagesize($fileInfo["targetImage"]);
            $x_ratio = $max_width / $width;
            $y_ratio = $max_height / $height;

            if( ($width <= $max_width) && ($height <= $max_height) ){
                $tn_width = $width;
                $tn_height = $height;
            } elseif (($x_ratio * $height) < $max_height) {
                $tn_height = ceil($x_ratio * $height);
                $tn_width = $max_width;
            } else {
                $tn_width = ceil($y_ratio * $width);
                $tn_height = $max_height;
            }

    //        $new_width = $width * $percent;
    //        $new_height = $height * $percent;
            header('Content-type: image/jpeg');
            // 再サンプル
            $image_p = imagecreatetruecolor($tn_width, $tn_height);
            $image = imagecreatefromjpeg($fileInfo["targetImage"]);
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $tn_width, $tn_height, $width, $height);
            imagejpeg($image_p, null, 100);

    //        mb_http_output("pass");
    //        header( sprintf( "Content-type: %s", $fileInfo["mime"] ) );
    //        header( sprintf( "Content-Disposition: inline; filename=%s", $fileInfo["fileName"] ) );
    //        readfile( $fileInfo["targetImage"] );
        }
    }

    /**
     * 予約のキャンセル
     */
    function action_cancel() {
        $reservation_session = $this->request->get('reservation_session');
        $reservation_data = $this->obj_N2MY_Reservation->getDetail($reservation_session);
        if (!$this->isAuthorized($reservation_data, '?action_cancel=&reservation_session='.$reservation_session)) {
            return false;
        }
        $this->obj_N2MY_Reservation->cancel($reservation_session);
        header("Location: /services/reservation/yoyaku.php");
    }

    /**
     * 認証済みか確認
     */
    function isAuthorized($reservation_data, $callback) {
        // パスワード無し
        if (!$reservation_data["info"]["reservation_pw"] || $reservation_data["info"]["reservation_pw_type"] != "1") {
            return true;
        } else {
            // 認証済み
            $reservation_auth = $this->session->get('reservation_auth', $this->_name_space);
            if ($reservation_auth == $reservation_data["info"]["reservation_session"]) {
                return true;
            // 認証なし
            } else {
                $redirect_url = '/services/reservation/'.$callback;
                $this->session->set("redirect_url", $redirect_url, $this->_name_space);
                $this->template->assign('reservation_session', $reservation_data["info"]["reservation_session"]);
                $this->display('user/yoyaku/auth2.t.html');
                false;
            }
        }
    }

    function action_check_pw() {
        $reservation_session = $this->request->get("reservation_session");
        $reservation_pw      = $this->request->get("reservation_pw");
        $user_info           = $this->session->get("user_info");
        $reservation_data = $this->obj_N2MY_Reservation->getDetail($reservation_session);
        require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
        require_once 'classes/dbi/user.dbi.php';
        $user_db = new UserTable($this->get_dsn());
        $admin_password = $user_db->getOne('user_key = ' . $user_info["user_key"] , "user_admin_password");
        if ($reservation_data["info"]["reservation_pw"] == $reservation_pw ||
            EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $admin_password) == $reservation_pw) {
            $redirect_url = $this->session->get("redirect_url", $this->_name_space);
            $reservation_auth = $this->session->set('reservation_auth', $reservation_session, $this->_name_space);
            header('Location: '.$redirect_url);
            //exit();
        }
        $this->logger->trace(__FUNCTION__."#start",__FILE__,__LINE__);
        $reservation_pw = $this->request->get("reservation_pw");
        $this->template->assign('reservation_session', $reservation_data["info"]["reservation_session"]);
        $this->display('user/yoyaku/auth2.t.html');
    }

    private function getUploadStatus($reservation_data)
    {
        foreach ($reservation_data["documents"] as $document_id => $document) {
            if ($reservation_data["upload_flg"] && !$document["delete"] ||
                !$reservation_data["upload_flg"] && !$document["tmp_name"] &&
                ($document["upload_status"] || $document["not_delete"])) {
                return true;
            }
        }
        return false;
    }
    private function _getMeetingVersion() {
        $version_file = N2MY_APP_DIR."version.dat";
        if ($fp = fopen($version_file, "r")) {
            $version = trim(fgets($fp));
        }
        return $version;
    }
}

$main = new AppReservation();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
