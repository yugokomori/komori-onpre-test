<?php
require_once("classes/dbi/clip.dbi.php");
require_once("classes/dbi/meeting_clip.dbi.php");
require_once("lib/EZLib/EZCore/EZLogger.class.php");
require_once("lib/EZLib/EZUtil/EZString.class.php");
require_once("lib/EZLib/EZUtil/EZFile.class.php");
require_once('classes/video/VideoClip.class.php');
require_once('classes/XmlUtil.class.php');

class N2MY_Clip
{
    var $dsn;
    var $config;
    var $uploadAlerts;
    static public $STATUS = array('CONVERTING' => 0, 'COMPLETE' => 2, 'UNKNOWN_ERROR' => 8);
    //{{{ __construct
    public function __construct($dsn)
    {
        $this->logger    = EZLogger2::getInstance();
        $this->dsn       = $dsn;
        $ezConfig        = EZConfig::getInstance(N2MY_CONFIG_FILE);
        $this->config    = $ezConfig->getAll("VIDEO");
        $this->clipTable = new ClipTable($this->dsn);
        $this->meetingClipTable = new MeetingClipTable($this->dsn);
    }
    //}}}
    //{{{delete
    /*
     * DB、VideoAPIのクリップ削除
    */
    public function delete($clip_key, $user_key)
    {
        if (!$clip_key || !$user_key) {
            throw new Exception("N2MY_CLip delete user_key : $user_key or  clip_key : $clip_key is empty");
        }
        // delete from Video
        $this->clipTable->_conn->query("begin");

        $this->deleteFromVideo($clip_key);

        $rs = $this->clipTable->deleteByUsersClipKey($clip_key, $user_key);
        if (PEAR::isError($rs)) {
            $this->clipTable->_conn->query("rollback");
            throw new Exception('clip table update error');
        }

        $this->clipTable->_conn->query("commit");

    }
    //}}}
    //{{{deleteFromVideo
    /*
     * VideoAPIのクリップ削除
    */
    private function deleteFromVideo($clip_key)
    {
        // --- set URL ---
        $param['client_id'] = $this->config['client_id'];

        // --- get response---
        $VideoClip  = new VideoClip();
        $VideoClip->delete($this->videoUrlAt('delete').$clip_key.'/', $this->config['l3_access_user'],
                           $this->config['l3_access_key'], $param);
        if (!$VideoClip->checkHTTPStatusCode()) {
            throw new Exception('VCUBE Video API delete - bad HTTP Status Code');
        }
    }
    //}}}
    //{{{ add
    /*
     * clipテーブル、VideoAPIのクリップ追加
    */
    public function add($params, $file_path)
    {
        if (!$file_path || !@$params['user_key']) {
            throw new Exception("file_path or group_id is empty file_path : $file_path , group_id : $group_id" );
        }
        $storage_no = rand(1, 100);

        // post to Video
        $clip_key   = $this->postToVideo($file_path, $params['user_key'], $storage_no);

        $params = array_merge($params, array('clip_key'        =>  $clip_key,
                                             'upload_filesize' =>  filesize($file_path),
                                             'storage_no'      =>  $storage_no));

        // inset to DB
        $clipTable = new ClipTable($this->dsn);
        $res = $clipTable->add($params);
        if (PEAR::isError($res)) {
            throw new Exception($res->getMessage());
        }
        return $clip_key;
    }
    //}}}
    //{{{ postToVideo
    /*
     * VideoAPIのクリップ追加
    */
    private function postToVideo($file_path, $user_key, $storage_no)
    {

        /*--- default ---*/
        $param = array('client_id'         => $this->config['client_id'],
                       'group_id'          => $user_key,
                       'title'             => '',
                       'body'              => '',
                       'callback_url'      => $this->config['callback_url'],
                       'storage'           => "/$storage_no/movie/",
                       'thumbnail_storage' => "/$storage_no/thumbnail/",
                       'play_url'          => sprintf("%s/%s/%s", $this->config['virtualhost_url'],
                                                                  $this->config['virtualhost_hush'], $storage_no),
                       'private'           => 1,
                       'qtconv'            => 'flv',
                       'video_bitrate'     => $this->config['default_video_bitrate']);

        if (!file_exists($file_path)){
            throw new Exception('upload file not exist file name : '.$file_path);
        }

        $VideoClip  = new VideoClip();
        $res = $VideoClip->post($this->videoUrlAt('post'), $this->config['l3_access_user'], $this->config['l3_access_key'],
                                $param, $file_path);
        if (!$VideoClip->checkHTTPStatusCode()) {
            throw new Exception('VCUBE Video API Post - bad HTTP Status code');
        }

        $XML = new XmlUtil($res);
        $XML->parse_into_struct($res);
        $ret = $XML->getTagValues();

        if (!isset($ret[0]['CLIP_KEY'])) {
            throw new Exception('VCUBE Video API Post - CLIP_KEY not found');
        }

        return $ret[0]['CLIP_KEY'];
    }
    //}}}
    //{{{createID
    public function createID()
    {
        return sha1(uniqid(rand(), true));
    }
    //}}}
    //}}}
    //{{{ restoreRelations
    /*
     * clipテーブルとmeeting_clipのリレーションの再作成。 1. 全消去, 2.追加
    */
    public function restoreRelations($meeting_key, $clip_keys)
    {
        $this->meetingClipTable->_conn->query("begin");
        try {
            $this->meetingClipTable->deleteByMeetingKey($meeting_key);
        } catch (Exception $e) {
            $this->meetingClipTable->_conn->query("rollback");
            throw $e;
        }

        if (count($clip_keys) <= 0) {
            $this->meetingClipTable->_conn->query("commit");
            return;
        }

        // task : クリップ存在確認
        try {
            $this->meetingClipTable->addClipsToMeeting($clip_keys, $meeting_key);
        } catch (Exception $e) {
            $this->meetingClipTable->_conn->query("rollback");
            throw $e;
        }

        $this->meetingClipTable->_conn->query("commit");
    }

    /*
     * セールス用リレーションの貼り直し
     */
    public function salesResoreRelations($room_key , $clip_keys){
        $where = sprintf("room_key = '%s' and is_deleted=0" , $room_key);
        $data  = array(
                  "updatetime" => date("Y-m-d H:i:s"),
                 "is_deleted" => 1
                );
        $this->meetingClipTable->update($data, $where);
        if($clip_keys){
            // task : クリップ存在確認
            try {
                $this->meetingClipTable->addClipsToMeeting($clip_keys, 0 , $room_key);
            } catch (Exception $e) {
                $this->meetingClipTable->_conn->query("rollback");
                throw $e;
            }
        }

    }

    public function salesDeleteRelations($room_key , $clip_key){
        $where = sprintf("clip_key = '%s' AND room_key = '%s' and is_deleted=0" , $clip_key , $room_key);
        $data  = array(
                "updatetime" => date("Y-m-d H:i:s"),
                "is_deleted" => 1
        );
        $this->meetingClipTable->update($data, $where);
    }

    ////}}}
    //{{{sizeOfConvertedByUser
    /*
     * user_keyからクリップのサイズ合計（fiv_filesize)取得
    */
    public function sizeOfConvertedByUser($user_key)
    {
        if (!$user_key) {
            throw new Exception("user_key is empty :  user_key : $user_key");
        }

        $clips = $this->clipTable->findConvertedByUserKey($user_key);
        if (count($clips) <= 0) {
            return 0;
        }
        $totalSize = 0;
        foreach ($clips as $clip) {
            $totalSize += $clip['flv_filesize'];
        }
        return $totalSize;
    }
    //}}}
    //{{{videoUrlAt
    private function videoUrlAt($method)
    {
        switch ($method) {
            case 'post':
            case 'POST':
               return sprintf("%s/post/", $this->config['clip_destination_host']);
               break;
            case 'delete':
            case 'DELETE':
               return sprintf("%s/key/", $this->config['clip_destination_host']);
               break;
            case 'feed':
            case 'FEED':
               return sprintf("%s/feed/", $this->config['clip_destination_host']);
               break;
           default:
               throw 'unknown method name : '.$method;
               break;
        }
    }
    //}}}
    //{{{ feedFromVideo
    public function feedFromVideo($clip_key)
    {
        // --- set Parameter---
        $param = array('client_id' => $this->config['client_id'],
                       'clip_key'  => $clip_key,
                       'type'      => "all");
        // --- get response---
        $VideoClip  = new VideoClip();
        $res = $VideoClip->feed($this->videoUrlAt('feed'),
                                      $this->config['l3_access_user'], $this->config['l3_access_key'], $param);
        if (!$VideoClip->checkHTTPStatusCode()) {
            throw new Exception('VCUBE Video API feed - bad HTTP Status Code');
        }

        // --- parse XML ---
        $XML = new XmlUtil($res);
        $XML->parse_into_struct($res);
        $ret = $XML->getTagValues('CLIP:');
        return @$ret[0];
    }
    //}}}
    //{{{findClipByMeetingKey
    public function findClipByUsersMeetingKey($meeting_key, $user_key)
    {
        $relations = $this->meetingClipTable->findByMeetingKey($meeting_key);
        if (count($relations) <= 0) {
            return array();
        }
        $clip_keys = array();
        foreach ($relations as $relation) {
            $clip_keys[] = $relation['clip_key'];
            $where = sprintf("clip_key= '%s'" , $relation['clip_key']);
            $clip_data = $this->clipTable->getRow($where);
            $clip_data["meeting_clip_name"] = $relation['meeting_clip_name'];
            $clip_datas[] = $clip_data;
        }

        $data = $this->clipTable->findByUsersClipKeys($clip_keys, $user_key);

        return $clip_datas;
    }
    public function findClipByUsersRoomKey($room_key, $user_key){
        $relations = $this->meetingClipTable->findByRoomKey($room_key);
        if (count($relations) <= 0) {
            return array();
        }
        $clip_keys = array();
        foreach ($relations as $relation) {
            $clip_keys[] = $relation['clip_key'];
            $where = sprintf("clip_key= '%s'" , $relation['clip_key']);
            $clip_data = $this->clipTable->getRow($where);
            $clip_data["meeting_clip_name"] = $relation['meeting_clip_name'];
            $clip_datas[] = $clip_data;
        }

        $data = $this->clipTable->findByUsersClipKeys($clip_keys, $user_key);

        return $clip_datas;
    }
    //}}}
    public function findIncDeletedByUsersMeetingKey($meeting_key, $user_key)
    {
        if (!$meeting_key || !$user_key) {
            throw new Exception(__FILE__." ".__FUNCTION__."  user_key : $user_key or  meeting_key : $meeting_key is empty");
        }

        $meeting_clips = $this->meetingClipTable->findByMeetingKey($meeting_key);
        if (count($meeting_clips) <= 0) {
            return array();
        }

        $clip_keys = array();
        foreach ($meeting_clips as $meeting_clip) {
            $clip_keys[] = $meeting_clip['clip_key'];
        }
        return $this->clipTable->findIncDeletedByUsersClipKeys($clip_keys, $user_key);
    }

    public function getClipListByRoomKey($room_key){
        $where = sprintf("room_key = '%s' AND is_deleted = 0" , $room_key);
        return $this->meetingClipTable->getRowsAssoc($where);
    }

}
