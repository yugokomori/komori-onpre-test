<?php
require_once("net_connection.php");
/*
 * Created on 2007/10/15
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

class RTMPHack {

  function RTMPHack() {
    $this->connection_uri = '';
    $this->connection_args = array();
    $this->method_name = '';
    $this->method_args = array();
  }

    // getter
    function getVar($varname) {
        if (isset($this->$varname)) {
            return $this->$varname;
        }
    }
    
    // setter
    function setVar($varname, $value) {
        if (isset($this->$varname)) {
            $this->$varname = $value;
        }
    }

  function execute() {
    $nc = new NetConnection();
    while($nc->on_connect(null)) { 
      $this->log("connected");
      $this->log("try calling '" + $this->method_name + "' with args:" + $this->method_args.to_s);
      while($res = $nc->call($this->method_name, $this->method_args)) {
        $this->log("calling '" + $this->method_name + "' succeeded. (returned value:" + $res + ")");
        // STDOUT.flush
        $nc->disconnect();
      }
    }
    while($nc->on_disconnect(null)) {
      $this->log("disconnected");
      exit;
    }
    while($err = $nc->on_error(null, null)) {
      $this->log("rejected");
      exit;
    }
    $nc->connect($this->connection_uri, $this->connection_args);
    $this->log("aborted");
  }

  private function log($msg) {
    print $msg;
  }
}
?>
