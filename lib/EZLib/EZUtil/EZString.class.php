<?php
class EZString {
    function __construct() {
    }

    function convertDependenceChar($str) {
        $pattern = array(
            '㎜' => 'mm',
            '㎝' => 'cm',
            '㎞' => 'km',
            '㎎' => 'mg',
            '㎏' => 'kg',
            '㏄' => 'cc',
            '㎡' => 'm^2',
            '№' => 'No.',
            '㏍' => 'K.K.',
            '℡' => 'TEL',
            '㍉' => 'ミリ',
            '㌔' => 'キロ',
            '㌢' => 'センチ',
            '㍍' => 'メートル',
            '㌘' => 'グラム',
            '㌧' => 'トン',
            '㌃' => 'アール',
            '㌶' => 'ヘクタール',
            '㍑' => 'リットル',
            '㍗' => 'ワット',
            '㌍' => 'カロリー',
            '㌦' => 'ドル',
            '㌣' => 'セント',
            '㌫' => 'パーセント',
            '㍊' => 'ミリバール',
            '㌻' => 'ページ',
            '㊤' => '上',
            '㊥' => '中',
            '㊦' => '下',
            '㊧' => '左',
            '㊨' => '右',
            '㈱' => '(株)',
            '㈲' => '(有)',
            '㈹' => '(代)',
            '㍾' => '明治',
            '㍽' => '大正',
            '㍼' => '昭和',
            '㍻' => '平成',
            'Ⅰ' => 'I',
            'Ⅱ' => 'II',
            'Ⅲ' => 'III',
            'Ⅳ' => 'IV',
            'Ⅴ' => 'V',
            'Ⅵ' => 'VI',
            'Ⅶ' => 'VII',
            'Ⅷ' => 'VIII',
            'Ⅸ' => 'IX',
            'Ⅹ' => 'X',
            '∑' => 'Σ',
            '⊿' => 'Δ',
            '∮' => '∫',
        );
        return str_replace( array_keys( $pattern), array_values( $pattern), $str);
    }

    /**
     * 乱数ID発行
     */
    function create_id() {
        list($a, $b) = explode(" ", microtime());
        $a = (int)($a * 100000);
        $a = strrev(str_pad($a, 5, "0", STR_PAD_LEFT));
        $c = $a.$b;
        // 36
        //print (base_convert($c, 10, 36));
        // 62
        return $this->dec2any($c);
    }

    /**
     * 数値から62進数のID発行
     */
    function dec2any( $num, $base=62, $index=false ) {
        if (! $base ) {
            $base = strlen( $index );
        } else if (! $index ) {
            $index = substr( '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ' ,0 ,$base );
        }
        $out = "";
        for ( $t = floor( log10( $num ) / log10( $base ) ); $t >= 0; $t-- ) {
            $a = floor( $num / pow( $base, $t ) );
            $out = $out . substr( $index, $a, 1 );
            $num = $num - ( $a * pow( $base, $t ) );
        }
        return $out;
    }

}
?>