#!/usr/local/bin/php
<?php
require_once("set_env.php");
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_MeetingLog.class.php");
require_once("classes/core/dbi/Meeting.dbi.php");
require_once("classes/core/dbi/MeetingSequence.dbi.php");
require_once("classes/core/Core_Meeting.class.php");

class AppDeleteOldMeetinglog extends AppFrame {

    var $_mail_flg = 1;
    var $dsn = null;
    var $N2MY_MeetingLog = null;
    var $obj_Meeting = null;
    var $Core_Meeting = null;
    var $obj_MeetingSequence = null;

    function __destruct() {
        //unlink(N2MY_APP_DIR."/tmp/sess_".session_id());
    }

    function init() {
        $this->dsn = $this->get_dsn();
        $this->N2MY_MeetingLog = new N2MY_MeetingLog($this->dsn);
        $this->obj_Meeting = new DBI_Meeting( $this->dsn );
        $this->Core_Meeting = new Core_Meeting( $this->dsn );
        $this->obj_MeetingSequence = new DBI_MeetingSequence( $this->dsn );
    }

    function default_view() {
    	$param["end_datetime"] = date('Y-m-d H:i:s' , strtotime("- ".MEETINGLOG_EXPIRE_DAY." day"));
    	//保護された記録を抜く
    	$param["is_locked"] = 0;
    	require_once ('classes/dbi/user.dbi.php');
    	$obj_User = new UserTable($this->dsn);
    	//$where =sprintf("user_id ='%s'", "test");
    	//$user_info = $obj_User->getRow($where);
    	$param["user_key"] = ADMIN_USER_KEY;
    	//$this->logger2->info($param);
        $meeting_list = $this->N2MY_MeetingLog->getList($param);
        //$this->logger2->info($meeting_list);
        foreach($meeting_list as $meeting) {
        	$this->delete_meetinglog($meeting["room_key"], $meeting["meeting_session_id"]);
        }
    }
    
    private function delete_meetinglog($room_key, $meeting_session_id){
    	$where = "room_key = '".addslashes( $room_key )."'" .
    			" AND meeting_session_id = '".addslashes( $meeting_session_id )."'" .
    			//                " AND meeting_key = '".addslashes( $meeting_key )."'" .
    	" AND is_active = 0".
    	" AND is_deleted = 0".
    	" AND is_locked = 0";
    	$meeting_info = $this->obj_Meeting->getRow( $where );
    	if ($meeting_info) {
    		if($meeting_info["has_recorded_minutes"]) {
    			// 議事録の削除
    			if(!$this->N2MY_MeetingLog->deleteMinutes( $meeting_info["meeting_session_id"] )) {
    				$this->logger2->error("会議記録の議事録削除失敗。 meeting_key=".$meeting_info["meeting_key"]." meeting_session_id=".$meeting_info["meeting_session_id"]);
    				return false;
    			}
    		}
    		if($meeting_info["has_recorded_video"]) {
    			// 映像の削除
	    		if(!$this->N2MY_MeetingLog->deleteVideo( $meeting_info["meeting_session_id"] )) {
	    			$this->logger2->error("会議記録の映像削除失敗。 meeting_key=".$meeting_info["meeting_key"]." meeting_session_id=".$meeting_info["meeting_session_id"]);
	    			return false;
	    		}
    		}
    		// ファイルキャビネットの削除
    		$this->Core_Meeting->deleteCabinet( $meeting_info["meeting_key"] );
    		// 動画生成データの削除
    		$where = sprintf( "meeting_key='%s'", $meeting_info["meeting_key"] );
    		$sequenceList = $this->obj_MeetingSequence->getRowsAssoc( $where );
    		foreach ($sequenceList as $sequence_info ) {
    			if ($sequence_info["record_status"] == "complete" && $sequence_info["record_filepath"]) {
    				if (file_exists(N2MY_MOVIES_DIR.$sequence_info["record_filepath"])) {
    					$this->logger->info(N2MY_MOVIES_DIR.$sequence_info["record_filepath"], "delete movie");
    					if(!unlink(N2MY_MOVIES_DIR.$sequence_info["record_filepath"])) {
    						$this->logger2->error("会議記録の動画削除失敗。 record path=".N2MY_MOVIES_DIR.$sequence_info["record_filepath"]);
    						return false;
    					}  						
    				}
    			}
    		}
    		//共有メモの削除
    		if( $meeting_info["has_shared_memo"] ) {
    			require_once("classes/dbi/shared_file.dbi.php");
    			$obj_sharedFile = new sharedFileTable($this->get_dsn());
    			$shared_where = sprintf("status = 1 AND meeting_key='%s'", $meeting_info["meeting_key"]);
    			$shared_files = $obj_sharedFile->getRowsAssoc($shared_where);
    			foreach($shared_files as $shared_file) {
    				if(!is_file($shared_file["file_path"])) {
    					$this->logger2->warn("file_path:".$shared_file["file_path"]." shared_file_key:".$shared_file["shared_file_key"]." SHARED FILE NOT FOUND");
    				} else {
    					$data = array("status" => "0");
    					$where = sprintf("shared_file_key='%s'", $shared_file["shared_file_key"]);
    					$obj_sharedFile->update($data, $where);
    					if(!unlink($shared_file["file_path"])) {
    						$this->logger2->error("会議記録の共有メモが削除失敗。 record path=".$shared_file["file_path"]);
    						return false;
    					}
    				}
    			}
    			
    		}
    		//DATAを物理削除（meeting,meeting_sequence,participant,meeting_use_log,reservation）
    		$data = array(
    				"is_deleted"=> 1,
    				"is_active" => 0
    		);
    		$where = sprintf( "meeting_key='%s'", $meeting_info["meeting_key"] );
    		require_once("classes/core/dbi/MeetingSequence.dbi.php");
    		$obj_MeetingSequence = new DBI_MeetingSequence($this->dsn);
    		$result = $obj_MeetingSequence->remove($where);
    		if (DB::isError($result)) {
    			$this->logger2->error("会議SEQUENCE削除失敗。 meeting_key=".$meeting_info["meeting_key"]);
    			return false;
    		}
    		require_once("classes/core/dbi/Participant.dbi.php");
    		$obj_Participant = new DBI_Participant($this->dsn);
    		$result = $obj_Participant->remove($where);
    		if (DB::isError($result)) {
    			$this->logger2->error("会議参加者削除失敗。 meeting_key=".$meeting_info["meeting_key"]);
    			return false;
    		}
    		require_once("classes/core/dbi/MeetingUseLog.dbi.php");
    		$obj_MeetingUseLog = new DBI_MeetingUseLog($this->dsn);
    		$result = $obj_MeetingUseLog->remove($where);
    		if (DB::isError($result)) {
    			$this->logger2->error("会議USE_LOG削除失敗。 meeting_key=".$meeting_info["meeting_key"]);
    			return false;
    		}
    		if($meeting_info["is_reserved"]) {
    			//予約情報を削除
    			require_once("classes/core/dbi/MeetingUseLog.dbi.php");
    			$obj_Reservation = new ReservationTable($this->dsn);
    			$result = $obj_Reservation->remove($where);
    			if (DB::isError($result)) {
    				$this->logger2->error("会議予約情報削除失敗。 meeting_key=".$meeting_info["meeting_key"]);
    				return false;
    			}
    		}
    		//$result = $this->obj_Meeting->update( $data, $where );
    		$result = $this->obj_Meeting->remove($where);
    		if (DB::isError($result)) {
    			$this->logger2->error("会議記録削除失敗。 meeting_key=".$meeting_info["meeting_key"]);
    			return false;
    		}
    		//$this->logger2->info("会議記録削除完了。 meeting_key=".$meeting_info["meeting_key"]);
    		return true;
    	}
    }


}
$main = new AppDeleteOldMeetinglog();
$main->execute();
