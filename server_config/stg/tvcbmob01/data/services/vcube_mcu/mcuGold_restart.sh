#! /bin/sh

HOSTNAME="`hostname`"
LOGFILE="/var/log/${HOSTNAME}.log"
SCRIPT_NAME="`basename $0`"
CML_FILE="/usr/local/sailfin/domains/domain1/config/ConferenceManagerListeners.xml"
RC=0

LOG()
{
    STATUS=$1
    MSG=$2
    echo [`date '+%Y/%m/%d %T'`] [${STATUS}] "${HOSTNAME}" "${SCRIPT_NAME}" "${MSG}" >> ${LOGFILE}
}

sleep 10
/usr/local/sailfin/bin/asadmin disable mcuGold
/usr/local/sailfin/bin/asadmin enable mcuGold
sleep 5

OUTPUT=`/data/services/vcube_mcu/RegisterManager.py`
sleep 1
if [ `grep confMngrListener ${CML_FILE} | wc -l` -eq 0 ]; then
    LOG "err" "${OUTPUT}"
    RC=1
fi

exit ${RC}