<?php
require_once("classes/N2MY_Api.class.php");
require_once("classes/dbi/room.dbi.php");
require_once("classes/dbi/mail_doc_conv.dbi.php");

class N2MY_MeetingAdminRoomAPI extends N2MY_Api
{
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        header('Pragma:');
    }

    function auth()
    {
        $this->checkAuthorizationAdmin();
        $this->obj_Room = new RoomTable($this->get_dsn());
        $user_info = $this->session->get("user_info");
        $this->obj_MailWB = new MailDocConvet($this->get_dsn(), $user_info["user_key"]);
    }

    /**
     * 設定
     */
    function action_update()
    {
        $room_key       = $this->request->get("room_id");
        $room_name      = $this->request->get("room_name");
        $cabinet        = $this->request->get("cabinet");
        $mail_wb_upload = $this->request->get("mail_wb_upload");
        $user_info = $this->session->get("user_info");
        $check_data = array(
            "room_id" => $room_key,
            "room_name" => $room_name,
            "cabinet" => $cabinet,
            "mail_wb_upload" => $mail_wb_upload,
        );
        $rules = array(
            "room_id" => array(
                "required" => true
            ),
            "room_name" => array(
                "required" => true
            ),
            "cabinet" => array(
                "required" => true,
                "allow" => array("0", "1"),
            ),
            "mail_wb_upload" => array(
                "required" => true,
                "allow" => array("all", "select", "deny"),
            ),
        );
        $err_obj = $this->error_check($check_data, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $data = array(
            "room_name" => $room_name,
            "cabinet" => $cabinet,
            "mfp" => $mail_wb_upload,
        );
        $where = "room_key = '".addslashes($room_key)."'";
        $this->obj_Room->update($data, $where);
        $this->add_operation_log('setup_room', array( 'room_key' => $room_key));
        $this->output();
    }

    /**
     *
     */
    function action_get_wb_mail() {
        $rows = $this->obj_MailWB->getList();
        $wb_mail_list = array();
        foreach ($rows as $row) {
            $wb_mail_list[] = array(
                "wb_mail_id" => $row["uniq_key"],
                "name" => $row["name"],
                "email" => $row["mail_address"],
            );
        }
        $this->output(array("wb_mail_list" => $wb_mail_list));
    }

    function action_add_wb_mail() {
        $name           = $this->request->get("name");
        $email          = $this->request->get("email");
        $check_data = array(
            "name" => $name,
            "email" => $email,
        );
        $rules = array(
            "name" => array(
                "required" => true
            ),
            "email" => array(
                "required" => true,
            	"regex" => '/^([*+!.&#$|\'\\%\/0-9a-zA-Z^_`{}=?~:-]+)?@((?:[-a-zA-Z0-9]+\.)+[a-zA-Z]{2,6})$/',
            ),
        );
        $err_obj = $this->error_check($check_data, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $data = array(
            "name"             => $name,
            "mail_address"     => $email
            );
        if (!$result = $this->obj_MailWB->add($data)) {
            return $this->display_error(1000, "DB Error", $this->get_error_info($err_obj));
        }
        $data = array(
            "wb_mail_id" => $result
        );
        $this->add_operation_log('mail_wb_upload_address_add', array('mail' => $email));
        return $this->output($data);
    }

    function action_update_wb_mail() {
        $wb_mail_key    = $this->request->get("wb_mail_id");
        $name           = $this->request->get("name");
        $email          = $this->request->get("email");
        $check_data = array(
            "wb_mail_key" => $wb_mail_key,
            "name" => $name,
            "email" => $email,
        );
        $rules = array(
            "wb_mail_key" => array(
                "required" => true
            ),
            "name" => array(
                "required" => true
            ),
            "email" => array(
                "required" => true,
            	"email" => true
            ),
        );
        $err_obj = $this->error_check($check_data, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $data = array(
            "name"             => $name,
            "mail_address"     => $email
            );
        $where = "uniq_key = '".addslashes($wb_mail_key)."'";
        $result = $this->obj_MailWB->update($data, $where);
        $this->add_operation_log('mail_wb_upload_address_update', array('mail' => $email));
        $this->output();
    }

    function action_delete_wb_mail() {
        $wb_mail_key    = $this->request->get("wb_mail_id");
        $check_data = array(
            "wb_mail_key" => $wb_mail_key,
        );
        $rules = array(
            "wb_mail_key" => array(
                "required" => true
            ),
        );
        $err_obj = $this->error_check($check_data, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $where = "uniq_key = '".addslashes($wb_mail_key)."'";
        $mail = $this->obj_MailWB->getOne($where, "mail_address");
        $result = $this->obj_MailWB->delete($where);
        $this->add_operation_log('mail_wb_upload_address_delete', array('mail' => $mail));
        $this->output();
    }

    function action_sort_up() {
        $room_key       = $this->request->get("room_id");
        $user_info = $this->session->get("user_info");
        $check_data = array(
            "room_id" => $room_key,
        );
        $rules = array(
            "room_id" => array(
                "required" => true
            ),
        );
        $err_obj = $this->error_check($check_data, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $this->obj_Room->up_sort($user_info["user_key"], $room_key);
        $this->output();
    }

    function action_sort_down() {
        $room_key       = $this->request->get("room_id");
        $user_info = $this->session->get("user_info");
        $check_data = array(
            "room_id" => $room_key,
        );
        $rules = array(
            "room_id" => array(
                "required" => true
            ),
        );
        $err_obj = $this->error_check($check_data, $rules);
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(0, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $this->obj_Room->down_sort($user_info["user_key"], $room_key);
        $this->output();
    }

}
$main = new N2MY_MeetingAdminRoomAPI();
$main->execute();