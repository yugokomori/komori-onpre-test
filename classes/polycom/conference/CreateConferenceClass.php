<?php

require_once(dirname(__FILE__)."/../../../lib/EZLib/EZCore/EZConfig.class.php");
require_once(dirname(__FILE__)."/../../../lib/EZLib/EZCore/EZLogger2.class.php");
require_once(dirname(__FILE__)."/../../../classes/polycom/Polycom.class.php");
require_once(dirname(__FILE__)."/../../../classes/polycom/conference/model/OnConferenceCreatedRequestModel.php");

class CreateConferenceClass {
    var $logger	= "";
    var $config	= null;
    private $model;
    
    function __construct($request){
    	$this->dsn		= $this->getDsn();
        $this->logger = EZLogger2::getInstance();
        $this->config = EZConfig::getInstance(N2MY_CONFIG_FILE);
        define("N2MY_MDB_DSN", $this->config->get("GLOBAL", "auth_dsn"));
        
        $this->polycomClass = new PolycomClass($this->dsn);
        $this->model = new OnConferenceCreatedRequestModel($request);
        
        $this->logger->info($this->model);
    }
    
    public function execute() {
    	// create mosaic
    	$this->polycomClass->callCreateMosaic($this->model->confId);
    	
    	// audio mixer
    	$this->polycomClass->callCreateSidebar($this->model->confId);
    	
		// active audio
    	$this->polycomClass->setPolycomMosaicSlot($this->model->confId, $this->model->did);

    	// active audio 
    	$this->polycomClass->setFlashMosaicSlot($this->model->confId, $this->model->did);
    }
	
	private function getDsn() {
		$server_list = parse_ini_file(dirname(__FILE__)."/../../../config/server_list.ini");
		return $server_list[N2MY_DEFAULT_DB];
	}
}
