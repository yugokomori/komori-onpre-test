<?php
require_once( "classes/AppFrame.class.php" );

class AppDBMonitor extends AppFrame
{
    var $sendFlg = 0;
    public function default_view()
    {

        $configIni = "../../config/config.ini";
        $serverListIni = sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR );
        $slaveListIni = sprintf( "%sconfig/slave_list.ini", N2MY_APP_DIR );

        $config = parse_ini_file( $configIni, true );
        //アラートメールを送信するのは一度のみ
        if( $config["ALERT_MAIL"]["done"] ) exit;

        $serverList = parse_ini_file( $serverListIni, true);
        $slaveList = parse_ini_file( $slaveListIni, true);

        foreach( $serverList["SERVER_LIST"] as $key => $dsn ){
            $this->connectionHandler( $dsn );
        }
        foreach( $slaveList["SLAVE_LIST"] as $key => $dsn ){
            $this->connectionHandler( $dsn, "slave" );
        }

        //送信フラグを挿入
        if( $this->sendFlg ){
            //slavelist.ini書き換え
            copy( $configIni, $configIni.".bak.".date( "YmdHis" ) );
            $fp = fopen( $configIni, "r" );
            $str = "";
            while( ! feof( $fp ) ){
                $fileline = fgets( $fp );
                if( preg_match( "#^done#", $fileline) ){
                    $str .= sprintf( "done = %s\n", $this->sendFlg );
                } else {
                    $str .= $fileline;
                }
            }
            fclose($fp);
            file_put_contents( $configIni, $str );
        }
    }

    private function connectionHandler( $dsn, $slave = 0 )
    {
        static $count = 0;
        while( $count < 3 ){
            $result = DB::connect( $dsn );
            if( DB::isError( $result ) ){
                sleep( 1 );
                ++$count;
            } else {
                return true;
            }
        }
        $this->sendFlg = 1;
        $count = 0;
        $param = parse_url( $dsn );

        require_once( "lib/pear/Mail.php" );
        $to = N2MY_ALERT_TO;
        $recipients = $to;
        $headers['From']    = N2MY_ALERT_FROM;
        $headers['To']      = $to;
        $headers['Subject'] = sprintf( "[meeting] %s DB 接続障害", ! $slave ? "** MASTER **" : "SLAVE", $param["host"] );
//        $body = print_r( $result, true );
        $body = sprintf("＜発生箇所＞
    ■ レプリケーションステータス: %s
    ■ 対象サーバ: %s　
    ■ 日時：%s

＜復旧方法、連絡先＞
    ■ 参照URL : ", ! $slave ? "master" : "slave", $param["host"], date( "Y/m/d H:i:s" ) );
        $host = ini_get("SMTP");

        $mail_options = array(
            'host'      => $host // ホスト名
        );
        $mail_object =& Mail::factory("SMTP",$mail_options );
        if (defined("N2MY_ALERT_FROM") && defined("N2MY_ALERT_TO") && N2MY_ALERT_FROM && N2MY_ALERT_TO) {
            $mail_object->send( $recipients, $headers, $body );
        }
    }
}


$main =& new AppDBMonitor();
$main->execute();
