<?php
require_once("lib/EZLib/Transit/common.php");
class Transit {

	function factory($uri, $transit_log = null) {
		$url_parts = parse_url($uri);
		$type = $url_parts["scheme"];
		$classfile = dirname(__FILE__).'/Transit/' . $type . '.php';
		if (include_once($classfile)) {
			$classname = 'Transit_' . $type;
			return new $classname($url_parts, $transit_log);
		} else {
			throw new Exception ('Driver not found');
		}
	}
}
