<?php
require_once("classes/dbi/member.dbi.php");
require_once("classes/dbi/user.dbi.php");

class N2MY_Auth{

    private $logger = "";
    private $user = "";
    private $member = "";

    function N2MY_Auth($dsn){
        $this->logger =& EZLogger::getInstance();
        $this->user = new UserTable($dsn);
        $this->member = new MemberTable($dsn);
    }

    /**
     * ログイン処理
     *
     * @param string $login_id ログインID
     * @param string $login_pw ログインパスワード
     * @param boolean $pw_encode パスワードの暗号化（MD5,sha1ハッシュ）
     * @param api_key $api_key
     * @return mixed 成功時:ユーザorメンバー情報、エラー時:false
     */
    function check($login_id, $login_pw, $pw_encode = "", $api_key = "", $login_type = ""){
        $this->logger->trace(__FUNCTION__."#START", __FILE__, __LINE__, array($login_id, $login_pw));
        // 未入力チェック
        if(!$login_id || !$login_pw){
            $this->logger->warn(__FUNCTION__."#not exists user_id or password", __FILE__, __LINE__, array(
                "login_id" => $login_id,
                "login_pw" => $login_pw
                ));
            return false;
        }
        // ユーザアカウントでチェック
        $user_info = $this->user->checkLogin($login_id, $login_pw, $pw_encode, $api_key, $login_type);
        $this->logger->info($user_info);
        if (DB::isError($user_info)) {
            $this->logger->error(__FUNCTION__, __FILE__, __LINE__, $user_info->getUserInfo());
            return false;
        } elseif ($user_info) {
            // ユーザログイン
            return array(
                "user_info" => $user_info,
                "member_info" => null,
                );
        } elseif ("centre_admin_login" == $login_type) {
            //centre管理者ページからはメンバーログイン不可
            return false;
        } else {
            // メンバー取得
            $member_info = $this->member->checkLogin($login_id, $login_pw, $pw_encode, $api_key, $login_type);
            if (DB::isError($member_info)) {
                $this->logger->error(__FUNCTION__, __FILE__, __LINE__, $member_info->getUserInfo());
                return false;
            }
            // ユーザの有効期限確認
            $user_where = " user_key = '".addslashes($member_info["user_key"])."'" .
                    " AND user_starttime <= '".date("Y-m-d H:i:s")."'".
                    " AND user_delete_status < 2";
            $user_info = $this->user->getRow($user_where);
            if (DB::isError($user_info)) {
                $this->logger->error(__FUNCTION__, __FILE__, __LINE__, $user_info->getUserInfo());
                return false;
            } else {
                if ($member_info && $user_info) {
                    // メンバーログイン
                    $this->logger->info(__FUNCTION__, __FILE__, __LINE__, array($user_info, $member_info));
                    return array(
                        "user_info" => $user_info,
                        "member_info" => $member_info
                        );
                }
            }
        }
        // アカウントが存在しない
        return false;
    }

    /**
     * メンバー課金招待 メールからのログイン処理
     *
     * @param string $login_id ログインID
     * @param string $login_pw ログインパスワード
     * @param string $user_id ユーザーID
     * @param boolean $pw_encode パスワードの暗号化（MD5,sha1ハッシュ）
     * @param api_key $api_key
     * @return mixed 成功時:メンバー情報、エラー時:false
     */
    function checkMember($login_id, $login_pw, $user_key, $pw_encode = "", $api_key = "", $external_member_invitation_flg = ""){
      $this->logger->trace(__FUNCTION__."#START", __FILE__, __LINE__, array($login_id, $login_pw));
      // 未入力チェック
      if(!$login_id || !$login_pw || !$user_key){
        $this->logger->warn(__FUNCTION__."#not exists user_id or password or user_id", __FILE__, __LINE__, array(
            "login_id" => $login_id,
            "login_pw" => $login_pw,
            "user_id"  => $user_key,
        ));
        return false;
      }
      // メンバー取得
      $member_info = $this->member->checkMemberLogin($login_id, $login_pw, $user_key, $pw_encode, $api_key,$external_member_invitation_flg);
      if (DB::isError($member_info)) {
        $this->logger->error(__FUNCTION__, __FILE__, __LINE__, $member_info->getUserInfo());
        return false;
      }
      if($member_info){
        return $member_info;
      }
      return false;

    }

    /**
     * VCUBE IDログイン処理
     *
     */
    function checkVcubeId($login_id){
        $this->logger->trace(__FUNCTION__."#START", __FILE__, __LINE__, array($login_id));
        // 未入力チェック
        if(!$login_id){
            $this->logger->warn(__FUNCTION__."#not exists user_id or password", __FILE__, __LINE__, array(
                "login_id" => $login_id,
                ));
            return false;
        }
        // ユーザアカウントでチェック
        // メンバー取得
        $where = " member_id = '".addslashes($login_id)."'".
            " AND member_status = 0";
        $member_info = $this->member->getRow($where);
        if (DB::isError($member_info)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$member_info->getUserInfo());
            return false;
        }
        if (DB::isError($member_info)) {
            $this->logger->error(__FUNCTION__, __FILE__, __LINE__, $member_info->getUserInfo());
            return false;
        }
        // ユーザの有効期限確認
        $user_where = " user_key = '".addslashes($member_info["user_key"])."'" .
                " AND user_starttime <= '".date("Y-m-d H:i:s")."'".
                " AND user_delete_status < 2";
        $user_info = $this->user->getRow($user_where);
        if (DB::isError($user_info)) {
            $this->logger->error(__FUNCTION__, __FILE__, __LINE__, $user_info->getUserInfo());
            return false;
        } else {
            if ($member_info && $user_info) {
                // メンバーログイン
                $this->logger->info(__FUNCTION__, __FILE__, __LINE__, array($user_info, $member_info));
                return array(
                    "user_info" => $user_info,
                    "member_info" => $member_info
                    );
            }
        }
        // アカウントが存在しない
        return false;
    }

    /**
     * メンバー課金利用の際メンバーキーのみで情報を取得
     */
    public function getMemberInfo( $member_key )
    {
        $where = sprintf( "member_key=%s AND member_status = 0;", $member_key );
        $member_info = $this->member->getRow( $where );
        // メンバー取得
        if (DB::isError($member_info) || !$member_info ) {
            $this->logger->error(__FUNCTION__, __FILE__, __LINE__, $member_info->getUserInfo());
            return false;
        }
        // ユーザの有効期限確認
        $user_where = " user_key = '".addslashes($member_info["user_key"])."'" .
                " AND user_starttime <= '".date("Y-m-d H:i:s")."'".
                " AND user_delete_status < 2";
        $user_info = $this->user->getRow( $user_where );
        if (DB::isError($user_info)) {
            $this->logger->error(__FUNCTION__, __FILE__, __LINE__, $user_info->getUserInfo());
            return false;
        } else {
            // メンバーログイン
            $this->logger->info(__FUNCTION__, __FILE__, __LINE__, array($user_info["user_id"], $member_info["member_id"]));
            return array(
                "user_info" => $user_info,
                "member_info" => $member_info
                );
        }
    }
}
