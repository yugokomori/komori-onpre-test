<?php
require_once 'classes/AppFrame.class.php';
require_once 'classes/N2MY_PGi_Client.class.php';
require_once 'htdocs/admin_tool/dBug.php';
require_once 'classes/core/dbi/Meeting.dbi.php';
require_once "classes/dbi/pgi_setting.dbi.php";
require_once "classes/mgm/dbi/pgi_session.dbi.php";
require_once "classes/dbi/user.dbi.php";
require_once "classes/pgi/PGiSystem.class.php";
require_once("classes/pgi/config/PGiConfig.php");
require_once ('lib/EZLib/EZUtil/EZDate.class.php');

class AppPgiClient extends AppFrame {

    var $pgiConfig;
    private $pgi_client;
    private $token;
    static  $DEFAULT_RESERVATION_DURATION = 180;
    //{{{ init
    function init()
    {
    }
    //}}}
    //{{{ preExecute
    private function preExecute()
    {
        if ($this->config->get("IGNORE_MENU", "teleconference")){
            $this->exitWithError('unable to teleconference');
        } else {
            $this->pgiConfig = PGiConfig::getInstance();
        }
    }
    //}}}
    //{{{action_pgi_login
    function action_pgi_login () 
    {
        $this->preExecute();
        
        $web_id = $this->request->get("web_id");
        $web_pw = $this->request->get("web_pw");
        $room_key = $this->request->get("room_key");
        $pgiSettingTable = new PGiSettingTable($this->get_dsn());
        $setting = $pgiSettingTable->findEnablleAtYM($room_key,
                                                     date('Y-m',strtotime(date("Y-m-d H:i:s") )));        //$system_key = $this->request->get("system_key", "PGi HYBRID PRODUCTION");
        $system_key = $this->request->get("system_key", "VCUBE HYBRID TEST-PPM");
        $token = $this->checkPGiLogin($web_id, $web_pw , $system_key, $setting);
        if (!$token){
            throw new Exception ("create pgi login failed");
        }
        return $token;
    }
    //}}}
    //{{{action_get_company
    function action_get_company () 
    {
        $this->preExecute();
        
        $room_key = $this->request->get("room_key");
        $pgiSettingTable = new PGiSettingTable($this->get_dsn());
        $setting = $pgiSettingTable->findEnablleAtYM($room_key,
                                                     date('Y-m',strtotime(date("Y-m-d H:i:s") )));        //$system_key = $this->request->get("system_key", "PGi HYBRID PRODUCTION");
        $system_key = $this->request->get("system_key", "VCUBE HYBRID TEST-PPM");
        $system  = PGiSystem::findBySystemKey($system_key);
        $token = $this->getCompanyPGI($system, $setting);
        if (!$token){
            throw new Exception ("create pgi login failed");
        }
        return $token;
    }
    //}}}
    //{{{action_get_phonetype
    function action_get_phonetype () 
    {
        $this->preExecute();
        $room_key = $this->request->get("room_key");
        $pgiSettingTable = new PGiSettingTable($this->get_dsn());
        $setting = $pgiSettingTable->findEnablleAtYM($room_key,
                                                     date('Y-m',strtotime(date("Y-m-d H:i:s") )));        //$system_key = $this->request->get("system_key", "PGi HYBRID PRODUCTION");
        $system_key = $this->request->get("system_key", "VCUBE HYBRID TEST-PPM");
        $system  = PGiSystem::findBySystemKey($system_key);
        $phoneType = $this->getPhoneTypePGI($system, $setting);
        if (!$phoneType){
            throw new Exception ("create pgi login failed");
        }
        return $phoneType;
    }
    //}}}
    //{{{action_get_company
    function action_get_reservation () 
    {
        $this->preExecute();
        
        $pgi_conf_id = $this->request->get("conf_id");
        $room_key = $this->request->get("room_key");
        $pgiSettingTable = new PGiSettingTable($this->get_dsn());
        $setting = $pgiSettingTable->findEnablleAtYM($room_key,
                                                     date('Y-m',strtotime(date("Y-m-d H:i:s") )));        //$system_key = $this->request->get("system_key", "PGi HYBRID PRODUCTION");
        $system_key = $this->request->get("system_key", "VCUBE HYBRID TEST-PPM");
        $system  = PGiSystem::findBySystemKey($system_key);
        $token = $this->getPGIResevation($pgi_conf_id, $system, $setting);
        if (!$token){
            throw new Exception ("create pgi login failed");
        }
        return $token;
    }
    
    function action_create_account ()
    {
        $this->preExecute();
        $system_key = $this->request->get("system_key", "VCUBE HYBRID TEST-PPM");
        $room_key = $this->request->get("room_key");
        $system  = PGiSystem::findBySystemKey($system_key);
        $pgiSettingTable = new PGiSettingTable($this->get_dsn());
        $setting = $pgiSettingTable->findEnablleAtYM($room_key,
                                                     date('Y-m',strtotime('2013-11-28 00:00:00' )));
        if (!$setting) {
            throw new Exception('pgi_setting not found room_key:'.$room_key);
        }
        if (!$setting['client_id']) {
            $setting = $this->createPGIAccountOfSetting($setting, $system, 241);
        }
    }
    
    function action_create_reservation()
    {
        $this->preExecute();

        $meeting_key = $this->request->get("meeting_key");
        if (!$meeting_key) {
            $this->exitWithError('meeting_key is required');
        }

        $meeting = $this->findMeeting($meeting_key);
        if (!$meeting) {
            $this->exitWithError('meeting not found key:'.$meeting_key);
        }


        switch ($meeting['pgi_api_status']) {
        case 'create-wait':
        case 'edit-wait':
        case 'error':
            $this->exitWithSuccess("pgi status: ".$meeting['pgi_api_status']." / meeting_key: $meeting_key");
            break;
        }

        $this->logger2->info("execute pgi (".$meeting['pgi_api_status'].") / meeting_key: $meeting_key");


        $pgiSettingTable = new PGiSettingTable($this->get_dsn());
        try {
            $setting = $pgiSettingTable->findEnablleAtYM($meeting['room_key'],
                                                         date('Y-m',strtotime($meeting['meeting_start_datetime'] )));
            if (!$setting) {
                throw new Exception('pgi_setting not found room_key:'.$meeting['room_key']);
            }

            $system  = PGiSystem::findBySystemKey($setting['system_key']);
            $this->logger2->debug($system);
            if (!$setting['client_id']) {
                $setting = $this->createPGIAccountOfSetting($setting, $system, $meeting['user_key']);
            }
        } catch (Exception $e) {
            $this->updateMeeting($meeting_key , array('pgi_api_status' => 'error', 'tc_type' => 'voip'));
            $this->exitWithError('pgi setting/account process failed : '.$e->getMessage());
        }

        // pgi情報が既に登録されていない、または現在有効なsettingで作成したpgi情報でなければcreate
        if (!$meeting['pgi_setting_key'] || ($meeting['pgi_setting_key'] != $setting['pgi_setting_key']))  {
            $requireCreate = true;
        } else {
            if ($meeting['pgi_api_status'] == 'complete') {
                $this->exitWithSuccess(" pgi_api($pgi_api_status) success (already complete) meeting_key : $meeting_key");
            }
            $requireCreate = false;
        }

        $this->logger2->info('execute pgi : '. ($requireCreate ? 'create' : 'edit'));
        $this->logger2->debug(array($this->pgi_client, $system));

        $setting["service_name"] = (string)$system->serviceName;
        // complete
        try {
            if ($requireCreate) {
                $this->createPGIResevationOfMeeting($meeting, $setting, $system);
            } else {
                $this->editPGIResevationOfMeeting($meeting, $setting, $system);
            }
        } catch (Exception $e) {
            $this->updateMeeting($meeting_key , array('pgi_api_status' => 'error', 'tc_type' => 'voip'));
            $this->exitWithError('create pgi resevation of meeting failed : '.$e->getMessage());
        }

        $this->exitWithSuccess(" pgi_api($pgi_api_status) success meeting_key : $meeting_key");
    }
    //}}}
    //{{{findMeeting
    private function findMeeting($meeting_key)
    {
        $obj_Meeting = new DBI_Meeting($this->get_dsn());
        $where       = "meeting_ticket='".mysql_real_escape_string($meeting_key)."'";

        return $obj_Meeting->getRow($where);
    }
    //}}}
    //{{{createPGIAccountOfSetting
    private function createPGIAccountOfSetting($setting, $pgi_system, $user_key)
    {
        $company_id  = $setting['company_id'] ? $setting['company_id'] : (string)$pgi_system->defaultCompanyID;
        $pgi_account = $this->createPGIAccount($company_id, $setting, $user_key, $pgi_system);
        if (!$pgi_account) {
            throw new Exception("createPGIAccount-'return false' pgi setting: company_id : $company_id");
        }

        $data = array("system_key" => (string)$pgi_system->key,
                      "company_id" => $company_id,
                      "client_id"  => $pgi_account['ClientID'],
                      "client_pw"  => $pgi_account['Password'],);

        $res  = $this->updateSetting($setting['pgi_setting_key'], $data);

        return array_merge($setting, $data);
    }
    //}}}
    //{{{ createPGIAccount
    private function createPGIAccount($company_id, $pgi_setting, $user_key, $pgi_system)
    {
        $this->logger2->info(sprintf('create pgi_account pgi_setting_key:%s / user_key:%s' , $pgi_setting["pgi_setting_key"], $user_key));

        $token = $this->checkPGiLogin((string)$pgi_system->webID, (string)$pgi_system->webPW, $pgi_system, $pgi_setting);
        $this->pgi_client = $this->createPGIClient($pgi_system, $this->pgiConfig->get("PGI", "account_url"));
        $pgi_params = array(
            "token"         => $token,
            // Account
            'company_id'    => $company_id,         // 必須（PGi発行）
            'password'      => $this->create_id().rand(2,9),               // 空の場合は自動発行
            'act_name'      => $user_key,
            'po_number'     => $pgi_setting["pgi_setting_key"],
            'time_zone'     => $this->pgiConfig->get("PGI", "pgi_api_time_zone"),    // 必須
            // ContactInfo (ダミーデータ)
            'first_name'    => 'vcube',
            'last_name'     => 'vcube',
            'phone'         => '0570-00-2192',  // 必須
            'email'         => 'vsupport@vcube.co.jp',
            'address1'      => 'Megoro-ku',             // 必須
            'city'          => 'Tokyo-To',      // 必須
            'country'       => 'JPN',           // 任意（デフォルトUSA）
            'state'         => '',
            'province'      => '',
            'zip'           => '',
            'service_name'           => $pgi_system->serviceName,
            'pgi_provisioning_url'     => $this->pgiConfig->get("PGI", "pgi_provisioning_url"),
            'pgi_schemas_client_url'     => $this->pgiConfig->get("PGI", "pgi_schemas_client_url"),
            'pgi_schemas_common_url'     => $this->pgiConfig->get("PGI", "pgi_schemas_common_url")
        );
        $headers['SOAPAction'] = '"'.$this->pgiConfig->get("PGI", 'account_create_soapaction').'"';
        $headers['Content-Type'] = "text/xml; charset=utf-8";

        $res  = $this->pgi_client->call('create_account', $pgi_params, $headers);
        //Error Check
        $error_msg = $res['ClientCreateResponse'][0]['ClientCreateResult'][0]['Errors'][0]["ApiError"][0];
        if ($error_msg) {
            $this->exitWithError($error_msg);
        }
        $client_id = $res['ClientCreateResponse'][0]['ClientCreateResult'][0]['a:ClientId'][0]["_data"];
        $data = array(
            "ClientID" => $client_id,
            "Password" => $pgi_params["password"]
        );
        return $data;
    }
    //}}}
    //{{{ updateSetting
    private function updateSetting($pgi_setting, $data)
    {
        $setting  = new PGiSettingTable($this->get_dsn());
        $where = sprintf("pgi_setting_key = '%s' and client_id = ''",
                         mysql_real_escape_string($pgi_setting["pgi_setting_key"]));

        $res = $setting->update($data, $where);
        if (PEAR::isError($res)) {
            throw new Exception("update room failed PEAR said : ".$res->getUserInfo());
        }
    }
    //}}}
    //{{{ createPGIResevationOfMeeting
    private function createPGIResevationOfMeeting($meeting, $setting, $pgi_system)
    {
        $meeting_time_zone = $this->pgiConfig->get("PGI", "pgi_meeting_time_zone") ? $this->pgiConfig->get("PGI", "pgi_meeting_time_zone") : 0;
        $res  =  $this->updateMeeting($meeting['meeting_ticket'],
                                      array('pgi_api_status' => 'create-wait'));
        $schedule_data = array();
        $schedule_data['meeting_start_datetime'] = date("Y-m-d H:i:s" ,EZDate::getLocateTime($meeting['meeting_start_datetime'], $meeting_time_zone, N2MY_SERVER_TIMEZONE));
        $schedule_data['meeting_stop_datetime'] = date("Y-m-d H:i:s" ,EZDate::getLocateTime($meeting['meeting_stop_datetime'], $meeting_time_zone, N2MY_SERVER_TIMEZONE));
        $this->logger2->info($schedule_data);
        if ($meeting['is_reserved']) {
            $schedule = array('adhocs'     => $this->scheculeToAdhocs($schedule_data['meeting_start_datetime'],
                                                                      $schedule_data['meeting_stop_datetime']),
                              'timezone'   => $this->pgiConfig->get("PGI", "pgi_api_time_zone"),);
        } else {
            $start_datetime = new DateTime($schedule_data['meeting_start_datetime']);
            $schedule = array('adhocs'     => array(array('start'    => $start_datetime->format('Y-m-d H:i:s'),
                                                          'duration' => self::$DEFAULT_RESERVATION_DURATION)),
                              'timezone'   => $this->pgiConfig->get("PGI", "pgi_api_time_zone"),);
        }

        $pgi_reservation = $this->createPGIResevation($setting, $schedule, $pgi_system);
        if (!$pgi_reservation){
            throw new Exception ("create pgi resevation failed");
        }

        $_phone_numbers = array();

        $_phone_numbers = $pgi_reservation["a:PhoneNumbers"][0]["a:ReservationPhoneNumber"];
        $phone_numbers = $this->makePhoneNumberArray($pgi_system, $_phone_numbers, $setting);
        $pgi_conf_id = $pgi_reservation['a:ConfId'][0]["_data"];
        $pgi_reservation_detail = $this->getPGIResevation($pgi_conf_id, $pgi_system, $setting);
        
        $data = array('pgi_api_status'    => 'complete',
                      'pgi_setting_key'   => $setting["pgi_setting_key"],
                      'pgi_sip_address'   => $pgi_reservation_detail["a:GetResults"][0]["a:ReservationGetItem"]["0"]["a:SIPNumbers"][0]["a:ReservationPhoneNumber"][0]["a:Number"][0]["_data"],
                      'pgi_conference_id' => $pgi_reservation["a:ConfId"][0]["_data"],
                      'pgi_m_pass_code'   => $pgi_reservation["a:PassCodes"][0]["a:ModeratorPassCode"][0]["_data"],
                      'pgi_p_pass_code'   => $pgi_reservation["a:PassCodes"][0]["a:ParticipantPassCode"][0]["_data"],
                      'pgi_l_pass_code'   => "",
                      'pgi_phone_numbers' => serialize($phone_numbers),
                      'pgi_service_name'  => $setting["service_name"],
                      'pgi_conference_status'  => 1);
        $this->logger2->debug($data);
        $this->updateMeeting($meeting["meeting_ticket"], $data);

        return array_merge($meeting, $data);
    }
    //}}}
    //{{{ editPGIResevationOfMeeting
    private function editPGIResevationOfMeeting($meeting, $setting, $pgi_system)
    {
        $this->updateMeeting($meeting['meeting_ticket'], array('pgi_api_status' => 'edit-wait'));
        $meeting_time_zone = $this->pgiConfig->get("PGI", "pgi_meeting_time_zone") ? $this->pgiConfig->get("PGI", "pgi_meeting_time_zone") : 0;
        $schedule_data = array();
        $schedule_data['meeting_start_datetime'] = date("Y-m-d H:i:s" ,EZDate::getLocateTime($meeting['meeting_start_datetime'], $meeting_time_zone, N2MY_SERVER_TIMEZONE));
        $schedule_data['meeting_stop_datetime'] = date("Y-m-d H:i:s" ,EZDate::getLocateTime($meeting['meeting_stop_datetime'], $meeting_time_zone, N2MY_SERVER_TIMEZONE));
        if ($meeting['is_reserved']) {
            $schedule = array('adhocs'     => $this->scheculeToAdhocs($schedule_data['meeting_start_datetime'],
                                                                      $schedule_data['meeting_stop_datetime']),
                              'timezone'   => $this->pgiConfig->get("PGI", "pgi_api_time_zone"),);
        } else {
            $schedule = null;
        }

        $this->editPGIResevation($setting['client_id'], $setting['client_pw'],
                                 $meeting['pgi_conference_id'], $schedule, $pgi_system);

        $data = array('pgi_api_status' => 'complete');
        $this->updateMeeting($meeting['meeting_ticket'], $data);

        return array_merge($meeting, $data);
    }
    //}}}
    //{{{ deletePGIResevationOfMeeting
    private function deletePGIResevationOfMeeting($meeting, $setting, $pgi_system)
    {
        $this->updateMeeting($meeting['meeting_ticket'], array('pgi_api_status' => 'delete-wait'));

        $system  = PGiSystem::findBySystemKey($setting['system_key']);
        $this->pgi_client = $this->createPGiClient($system);
        $setting["service_name"] = (string)$system->serviceName;
        
        $this->deletePGIResevation($setting, $meeting['pgi_conference_id'], $pgi_system);

        $data = array('pgi_api_status' => 'complete',
                      'pgi_conference_status' => 2);
        $this->updateMeeting($meeting['meeting_ticket'], $data);

        return array_merge($meeting, $data);
    }
    
    private function deletePGIResevation($pgi_setting, $pgi_conference_id, $pgi_system)
    {
        $token = $this->checkPGiLogin((string)$pgi_system->webID, (string)$pgi_system->webPW, $pgi_system, $pgi_setting);
        $this->pgi_client = $this->createPGIClient($pgi_system, $this->pgiConfig->get("PGI", "reservation_url"));
        $params = array('token'          => $token,
                        'client_id'             => $pgi_setting["client_id"],
                        'client_pw'             => $pgi_setting["client_pw"],
                        'pass_code_type' => 'Random10DigitStrong',
                        'service_name'   => (string)$pgi_system->serviceName,
                        'conf_id'        => $pgi_conference_id,
                        'pgi_services_url'     => $this->pgiConfig->get("PGI", "pgi_services_url"),
                        'pgi_schemas_client_url'     => $this->pgiConfig->get("PGI", "pgi_schemas_client_url"),
                        'pgi_schemas_common_url'     => $this->pgiConfig->get("PGI", "pgi_schemas_common_url"),
                        'pgi_schemas_reservation_url'     => $this->pgiConfig->get("PGI", "pgi_schemas_reservation_url"),
                        'pgi_serialization_arrays_url'     => $this->pgiConfig->get("PGI", "pgi_serialization_arrays_url"));
        $headers['SOAPAction'] = '"'.$this->pgiConfig->get("PGI", 'reservation_delete_soapaction').'"';
        $headers['Content-Type'] = "text/xml; charset=utf-8";
        $res        = $this->pgi_client->call('delete_reservation', $params, $headers);
        $this->logger2->debug($res);
        $error_msg = $res['ReservationCreateResponse'][0]['ReservationCreateResult'][0]['Errors'][0]["ApiError"][0];
        if ($error_msg) {
            $this->exitWithError($error_msg);
        }
        $this->logger2->info('pgi edit reservation success conf_id: '.$pgi_conference_id);
        return @$res['ReservationCreateResponse'][0]['ReservationCreateResult'][0];
    }
    //}}}
    //{{{exitWithError
    private function exitWithSuccess($msg)
    {
        $this->logger2->info($msg);
        print 1;
        exit;
    }
    //}}}
    //{{{exitWithError
    private function exitWithError($msg)
    {
        $this->logger2->error($msg);
        print 0;
        exit;
    }
    //}}}
    //{{{ updateMeeting
    private function updateMeeting($meeting_key, $data)
    {
        $meeting        = new DBI_Meeting($this->get_dsn());
        $where          = sprintf("meeting_ticket='%s'", mysql_real_escape_string($meeting_key));
        $res = $meeting->update($data, $where);
        if (PEAR::isError($res)) {
            throw new Exception("update meeting failed PEAR said : ".$res->getUserInfo());
        }
    }
    
    function checkPGiLogin($web_id, $web_pw, $pgi_system, $pgi_setting, $test=null) 
    {
        if (!$web_id || !$web_pw || !$pgi_system || !$pgi_setting){
            $this->logger2->warn(array($web_id, $web_pw, $pgi_system, $pgi_setting));
            throw new Exception ("pgi login parameter failed ");
        }
        $this->logger2->debug(array($pgi_system,$pgi_setting));
        $obj_PGiSession        = new PGiSessionTable(N2MY_MDB_DSN);
        $where = "system_key = '".mysql_real_escape_string((string)$pgi_system->key)."'".
                 " AND company_id = ".mysql_real_escape_string($pgi_setting["company_id"])."";
        $session_data = $obj_PGiSession->getRow($where);
        if (!$session_data) {
            $add_data = array("system_key" => mysql_real_escape_string((string)$pgi_system->key),
                              "company_id" => mysql_real_escape_string($pgi_setting["company_id"]));
            $obj_PGiSession->add($add_data);
        }
        
        $session_expire_time = "";
        if ($session_data["pgi_session_token"]) {
            $session_expire_time = date("Y-m-d H:i:s", strtotime($session_data["update_datetime"]." 10 hour"  ));
        }
        if ($pgi_setting["admin_client_id"]) {
            $admin_client_id = $pgi_setting["admin_client_id"];
            $admin_client_pw = $pgi_setting["admin_client_pw"];
        } else {
            $admin_client_id = (string)$pgi_system->adminClientID;
            $admin_client_pw = (string)$pgi_system->adminClientPW;
        }
        if (!$session_expire_time || $session_expire_time < date("Y-m-d H:i:s") ) {
            $token = $this->loginPGI($admin_client_id, $admin_client_pw, $web_id, $web_pw);
            $data = array("pgi_session_token" => $token);
            $obj_PGiSession->update($data, $where);
        } else {
            $token = $session_data["pgi_session_token"];
        }
        if (!$token){
            throw new Exception ("pgi login failed");
        }
        return $token;
    }
    
    //Login PGi
    private function loginPGI($pgi_client_id, $pgi_client_pw, $pgi_web_id, $pgi_web_pw)
    {
        $this->pgi_client = new N2MY_PGi_Client($this->pgiConfig->get("PGI", "logon_url"),
                                       (string)$pgi_web_id ,(string)$pgi_web_pw);
        $params = array('admin_client_id' => $pgi_client_id,
                        'admin_client_pw' => $pgi_client_pw,
                        'web_id'    => $pgi_web_id,
                        'web_pw'    => $pgi_web_pw,
                        'pgi_security_url'     => $this->pgiConfig->get("PGI", "pgi_security_url"),
                        'pgi_schemas_security_url'     => $this->pgiConfig->get("PGI", "pgi_schemas_security_url"),
                        'pgi_schemas_common_url'     => $this->pgiConfig->get("PGI", "pgi_schemas_common_url"));
                        $this->logger2->debug($params);
        $headers['SOAPAction'] = '"'.$this->pgiConfig->get("PGI", 'login_soapaction').'"';
        $headers['Content-Type'] = "text/xml; charset=utf-8";
        $res    = $this->pgi_client->call('login', $params, $headers);
        $attr = $res['LogOnResponse'][0]['LogOnResult'][0]['a:Token'][0]["_data"];
        return $attr;
    }
    
    //Get CompanyID
    private function getCompanyPGI($pgi_system, $pgi_setting)
    {
        $token = $this->checkPGiLogin((string)$pgi_system->webID, (string)$pgi_system->webPW, $pgi_system, $pgi_setting);
        $this->pgi_client = $this->createPGIClient($pgi_system, $this->pgiConfig->get("PGI", "account_url"));
        $params = array('token'          => $token,
                        'company_id'          => $pgi_setting["company_id"],
                        'service_name'           => $pgi_system->serviceName,
                        'pgi_delivery_url'     => $this->pgiConfig->get("PGI", "pgi_delivery_url"),
                        'pgi_provisioning_url'     => $this->pgiConfig->get("PGI", "pgi_provisioning_url"),
                        'pgi_schemas_company_url'     => $this->pgiConfig->get("PGI", "pgi_schemas_company_url"),
                        'pgi_schemas_common_url'     => $this->pgiConfig->get("PGI", "pgi_schemas_common_url"),
                        'pgi_serialization_arrays_url'     => $this->pgiConfig->get("PGI", "pgi_serialization_arrays_url"));
                        $this->logger2->debug($params);
        $headers['SOAPAction'] = '"'.$this->pgiConfig->get("PGI", 'get_company_soapaction').'"';
        $headers['Content-Type'] = "text/xml; charset=utf-8";
        $res    = $this->pgi_client->call('get_company', $params, $headers);
        $error_msg = $res['CompanyGetResponse'][0]['CompanyGetResult'][0]['Errors'][0]["ApiError"][0];
        if ($error_msg) {
            //エラーだった場合はログイン処理を実施
            $this->logger2->warn($pgi_system);
            $token = $this->loginPGI((string)$pgi_system->adminClientID, (string)$pgi_system->adminClientPW, (string)$pgi_system->webID, (string)$pgi_system->webPW);
        }
        return $token;
    }
    
    
    
    //Get PhoneType
    private function getPhoneTypePGI($pgi_system, $pgi_setting)
    {
        $token = $this->checkPGiLogin((string)$pgi_system->webID, (string)$pgi_system->webPW, $pgi_system, $pgi_setting);
        $this->pgi_client = $this->createPGIClient($pgi_system, $this->pgiConfig->get("PGI", "account_url"));
        $params = array('token'          => $token,
                        'company_id'          => $pgi_system->defaultCompanyID,
                        'service_name'           => $pgi_system->serviceName,
                        'pgi_provisioning_url'     => $this->pgiConfig->get("PGI", "pgi_provisioning_url"),
                        'pgi_schemas_common_url'     => $this->pgiConfig->get("PGI", "pgi_schemas_common_url"));
                        $this->logger2->debug($params);
        $headers['SOAPAction'] = '"'.$this->pgiConfig->get("PGI", 'get_phonetype_soapaction').'"';
        $headers['Content-Type'] = "text/xml; charset=utf-8";
        $res    = $this->pgi_client->call('get_phone_type', $params, $headers);
        $error_msg = $res['LookupResponse'][0]['LookupResult'][0]['Errors'][0]["ApiError"][0];
        $phoneTypes = $res['LookupResponse'][0]['LookupResult'][0]['a:LookupResults'][0]['a:LookupItem'];
        foreach ($phoneTypes as $phoneType ) {
            $code = $phoneType['a:Code'][0]['_data'];
            $phone_type[$code] = array("code" => $code,
                                     "type" => $phoneType['a:Name'][0]['_data']);
        }
        return $phone_type;
    }
    //}}}
    //{{{ createPGIResevation
    private function createPGIResevation($pgi_setting, $schedule = null, $pgi_system)
    {
        $token = $this->checkPGiLogin((string)$pgi_system->webID, (string)$pgi_system->webPW, $pgi_system, $pgi_setting, "test");
        $this->pgi_client = $this->createPGIClient($pgi_system, $this->pgiConfig->get("PGI", "reservation_url"));
        $params = array('token'          => $token,
                        'client_id'             => $pgi_setting["client_id"],
                        'client_pw'             => $pgi_setting["client_pw"],
                        'pass_code_type' => 'Random10DigitStrong',
                        'schedule'       => $schedule,
                        'service_name'   => (string)$pgi_system->serviceName,
                        'pgi_services_url'     => $this->pgiConfig->get("PGI", "pgi_services_url"),
                        'pgi_schemas_client_url'     => $this->pgiConfig->get("PGI", "pgi_schemas_client_url"),
                        'pgi_schemas_common_url'     => $this->pgiConfig->get("PGI", "pgi_schemas_common_url"),
                        'pgi_schemas_reservation_url'     => $this->pgiConfig->get("PGI", "pgi_schemas_reservation_url"),
                        'pgi_serialization_arrays_url'     => $this->pgiConfig->get("PGI", "pgi_serialization_arrays_url"));
                        $this->logger2->debug($params);
        $headers['SOAPAction'] = '"'.$this->pgiConfig->get("PGI", 'reservation_create_soapaction').'"';
        $headers['Content-Type'] = "text/xml; charset=utf-8";
        $res    = $this->pgi_client->call('create_reservation', $params, $headers);
        $error_msg = $res['ReservationCreateResponse'][0]['ReservationCreateResult'][0]['Errors'][0]["ApiError"][0];
        $this->logger2->debug($res);
        if ($error_msg) {
            $this->exitWithError($error_msg);
        }
        return @$res['ReservationCreateResponse'][0]['ReservationCreateResult'][0];
    }
    //}}}
    //{{{ editPGIResevation
    private function editPGIResevation($pgi_setting, $pgi_conference_id, $schedule, $pgi_system)
    {
        $token = $this->checkPGiLogin((string)$pgi_system->webID, (string)$pgi_system->webPW, $pgi_system, $pgi_setting);
        $this->pgi_client = $this->createPGIClient($pgi_system, $this->pgiConfig->get("PGI", "reservation_url"));
        $params = array('token'          => $token,
                        'client_id'             => $pgi_setting["client_id"],
                        'client_pw'             => $pgi_setting["client_pw"],
                        'pass_code_type' => 'Random10DigitStrong',
                        'schedule'       => $schedule,
                        'service_name'   => (string)$pgi_system->serviceName,
                        'conf_id'        => $pgi_conference_id,
                        'pgi_services_url'     => $this->pgiConfig->get("PGI", "pgi_services_url"),
                        'pgi_schemas_client_url'     => $this->pgiConfig->get("PGI", "pgi_schemas_client_url"),
                        'pgi_schemas_common_url'     => $this->pgiConfig->get("PGI", "pgi_schemas_common_url"),
                        'pgi_schemas_reservation_url'     => $this->pgiConfig->get("PGI", "pgi_schemas_reservation_url"),
                        'pgi_serialization_arrays_url'     => $this->pgiConfig->get("PGI", "pgi_serialization_arrays_url"));
        $headers['SOAPAction'] = '"'.$this->pgiConfig->get("PGI", 'reservation_update_soapaction').'"';
        $headers['Content-Type'] = "text/xml; charset=utf-8";
        $res        = $this->pgi_client->call('edit_reservation', $params, $headers);
        $error_msg = $res['ReservationCreateResponse'][0]['ReservationCreateResult'][0]['Errors'][0]["ApiError"][0];
        if ($error_msg) {
            $this->exitWithError($error_msg);
        }
        $this->logger2->debug($res);
        $this->logger2->info('pgi edit reservation success conf_id: '.$pgi_conference_id);
        return @$res['ReservationEditResponse'][0]['ReservationEditResult'][0];

    }
    
    //}}}
    //{{{ editPGIResevation
    private function getPGIResevation($pgi_conf_id, $pgi_system, $pgi_setting)
    {
        $this->logger2->debug(array((string)$pgi_system->webID, (string)$pgi_system->webPW, $pgi_system, $pgi_setting));
        $token = $this->checkPGiLogin((string)$pgi_system->webID, (string)$pgi_system->webPW, $pgi_system, $pgi_setting);
        $this->pgi_client = $this->createPGIClient($pgi_system, $this->pgiConfig->get("PGI", "reservation_url"));
        $params = array('token'          => $token,
                        'conf_id'        => $pgi_conf_id,
                        'pgi_services_url'     => $this->pgiConfig->get("PGI", "pgi_services_url"),
                        'pgi_schemas_client_url'     => $this->pgiConfig->get("PGI", "pgi_schemas_client_url"),
                        'pgi_schemas_common_url'     => $this->pgiConfig->get("PGI", "pgi_schemas_common_url"),
                        'pgi_schemas_reservation_url'     => $this->pgiConfig->get("PGI", "pgi_schemas_reservation_url"),
                        'pgi_serialization_arrays_url'     => $this->pgiConfig->get("PGI", "pgi_serialization_arrays_url"));
        $headers['SOAPAction'] = '"'.$this->pgiConfig->get("PGI", 'reservation_get_soapaction').'"';
        $headers['Content-Type'] = "text/xml; charset=utf-8";
        $res        = $this->pgi_client->call('find_reservation', $params, $headers);
        $this->logger2->info($res);
        $error_msg = $res['ReservationGetResponse'][0]['ReservationGetResult'][0]['Errors'][0]["ApiError"][0];
        if ($error_msg) {
            $this->exitWithError($error_msg);
        }
        $this->logger2->info('pgi get reservation success conf_id: '.$pgi_conf_id);
        return @$res['ReservationGetResponse'][0]['ReservationGetResult'][0];
    }
    //}}}
    //{{{default_view
    function default_view()
    {
        die('default view');
    }
    //}}}
    //}}}
    //{{{ formatPGiDateTime
    //private function formatPGiDateTime($datetime){
    //    return substr(str_replace(array(' ','-',':'),'', $datetime),0,12);
    //}
    private function scheculeToAdhocs($start_datetime, $end_datetime)
    {
        $adhocs = array();

        $start = new DateTime($start_datetime);
        $end   = new DateTime($end_datetime);

        $days   = $this->dateDiff($start, $end);
        $minute = $this->minuteDiff($start, $end);

        for ($i = 0; $i <= $days; $i++) {

            if ($i == $days) {
                $duration = $minute;
            } else {
                $duration = (60 * 24) - 1;
            }

            $adhocs[] = array('start'    => $start->format('Y-m-d H:i:s'),
                              'duration' => $duration);
            $start->modify('+1day');
        }
        return $adhocs;
    }
    //}}}
    // {{{ createPGIClient
    private function createPGIClient($pgi_system, $url)
    {
        return new N2MY_PGi_Client($url,(string)$pgi_system->webID ,(string)$pgi_system->webPW);
    }
    private function makePhoneNumberArray($pgi_system, $phone_numbers, $pgi_setting) {
        $phoneTypes = $this->getPhoneTypePGI($pgi_system, $pgi_setting);
        $numbers = array();
        foreach ($phone_numbers as $number) {
            $typecode = $number['a:PhoneType'][0]['_data'];
            $numbers[] =  array("Type" => $phoneTypes[$typecode]['type'],
                            "PhoneType" => $typecode,
                            "Number" => $number['a:Number'][0]['_data'],
                            "CustomLocation" => $number['a:Location'][0]['_data'],
                            "LocationID" => $number['a:LocationCode'][0]['_data']
            );
        }
        return $numbers;
    }
    //}}}
    // php 5.3以下でDateTImeのdiffが使えなかったので実装
    //{{{dateDiff
    function dateDiff($start, $end)
    {
        $dayTime = 60 * 60 * 24;

        $startTime = strtotime($start->format('Y-m-d H:i:s'));
        $endTime   = strtotime($end->format('Y-m-d H:i:s'));

        return floor(($endTime - $startTime) / $dayTime);
    }
    // }}}
    // {{{minuteDiff
    function minuteDiff($start, $end)
    {
        $dayTime = 60 * 60 * 24;

        $startTime = strtotime($start->format('Y-m-d H:i:s'));
        $endTime   = strtotime($end->format('Y-m-d H:i:s'));

        $diffSec = $endTime - $startTime;
        if ($diffSec <= $dayTime){
            return $diffSec / 60;
        }

        return $diffSec % $dayTime / 60;
    }
    //}}}
    

    /**
     * 乱数ID発行
     */
    function create_id() {
        list($a, $b) = explode(" ", microtime());
        $a = (int)($a * 100000);
        $a = strrev(str_pad($a, 5, "0", STR_PAD_LEFT));
        $c = $a.$b;
        // 36
        //print (base_convert($c, 10, 36));
        // 62
        return $this->dec2any($c);
    }

    /**
     * 数値から62進数のID発行
     */
    function dec2any( $num, $base=57, $index=false ) {
        if (! $base ) {
            $base = strlen( $index );
        } else if (! $index ) {
            $index = substr(
                    '23456789ab' .
                    'cdefghijkm' .
                    'nopqrstuvw' .
                    'xyzABCDEFG' .
                    'HJKLMNPQRS' .
                    'TUVWXYZ' ,0 ,$base );
        }
        $out = "";
        for ( $t = floor( log10( $num ) / log10( $base ) ); $t >= 0; $t-- ) {
            $a = floor( $num / pow( $base, $t ) );
            $out = $out . substr( $index, $a, 1 );
            $num = $num - ( $a * pow( $base, $t ) );
        }
        return $out;
    }
}

$main = new AppPgiClient();
$main->execute();
