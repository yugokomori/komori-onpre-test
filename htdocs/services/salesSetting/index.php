<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once ("classes/AppFrame.class.php");
require_once ("classes/mgm/MGM_Auth.class.php");
require_once ('lib/EZLib/EZUtil/EZDate.class.php');
require_once ("lib/EZLib/EZUtil/EZString.class.php");
require_once ("classes/dbi/room.dbi.php");
require_once ("classes/dbi/member.dbi.php");
require_once ("classes/dbi/customer_message.dbi.php");
require_once ('lib/EZLib/EZUtil/EZEncrypt.class.php');
require_once ("Pager.php");

class AppSalesSetting extends AppFrame {

    var $_name_space = null;
    var $_ssl_mode = null;

    /**
     * 初期処理
     */
    function init() {
        $this->_name_space = md5(__FILE__);
    }

    /**
     * ログイン認証
     */
    function auth() {
        $this->checkAuth();
        $user_info = $this->session->get("user_info");
        $member_info = $this->session->get("member_info");
        $this->logger2->debug($member_info);
        if(!$user_info["use_sales"]) {
            $message = "NO INBOUND OPTION";
            $this->template->assign("message", $message);
            $this->display("user/404_error.t.html");
        }
        // 認証後のみ
        $server_info = $this->session->get("server_info");
    }
     /**
     * デフォルト処理
     */
    function default_view() {
        $this->logger->trace("test", __FILE__, __LINE__, __FUNCTION__);
        $this->action_show_roomlist();
    }

    //ご案内窓口一覧
    function action_show_roomlist() {
        //ユーザーキーからオーディエンスオプションの契約状況を取得
        $country_key = $this->session->get("country_key");
        $user_info   = $this->session->get("user_info");
        $member_info = $this->session->get("member_info");

        $obj_Room = new RoomTable($this->get_dsn());
        // メンバーの場合自分の部屋のみ
        if($member_info){
            //memberとのリレーションチェック
            require_once("classes/dbi/member_room_relation.dbi.php");
            $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
            $where = "member_key = ".addslashes($member_info["member_key"]);
            $room_relation = $objRoomRelation->getRow($where);
            $where = "room_key = '".$room_relation["room_key"]."'".
                     " AND room_status = 1";
            $rooms = $obj_Room->getRowsAssoc($where);
        }else{//通常ユーザーは部屋一覧取得
            $where = "user_key = '".$user_info['user_key']."'".
                     " AND room_status = 1 and use_sales_option = 1";
            $rooms = $obj_Room->getRowsAssoc($where, array("room_sort" => "ask"));
        }

        $this->template->assign('data', $rooms);
        $this->template->assign('country_key',$country_key);
        $this->template->assign('member',$member_info["room_key"]);
        $this->display('user/salesSetting/index.t.html');
    }

    //ご案内窓口設定（会議室設定）
    function action_room_setting($message = "") {
        $room_key = $this->request->get("room_key");
        $obj_Room = new RoomTable($this->get_dsn());
        $room_data = $obj_Room->get_detail($room_key);
        $room_data["addition"] = unserialize($room_data["addition"]);

        //ルームーキーからオプションの契約状況を取得
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account    = new N2MY_Account($this->get_dsn());
        $room_option = $obj_N2MY_Account->getRoomOptionList($room_key);
        $this->logger->debug("option",__FILE__,__LINE__,$room_option);

        $country_key = $this->session->get('country_key');
        $this->set_submit_key($this->_name_space);

        //ルームテーブルにバナーサイズがあり、コアにイメージがあれば表示
        $user_info = $this->session->get("user_info");
        $image_url = "/banner/".$user_info["user_id"]."/";
        if ($room_data["addition"]["banner_width"]) {
            $room_data["image"] = $image_url.$room_key.".jpg";
        } else {
            $room_data["image"] = "";
        }
        //メンバーでログインの場合は最新のメンバー情報を取得（設定権限取得の為）
        $member_info = $this->session->get("member_info");
        $obj_Member = new MemberTable($this->get_dsn());
        // メンバーの場合自分の部屋のみ
        if($member_info){
            $where = "member_key = ".addslashes($member_info["member_key"]);
            $member_info = $obj_Member->getRow($where);
        }
        $this->logger2->info($room_data);
        $this->logger->debug("member_info",__FILE__,__LINE__,$member_info);
        $this->template->assign('message',$message);
        $this->template->assign('room_option',$room_option);
        $this->template->assign('country_key',$country_key);
        $this->template->assign('data', $room_data);
        $this->template->assign('member_info', $member_info);
        $this->display('user/salesSetting/room_edit.t.html');
    }

    //ご案内窓口名変更
    function action_edit_room_name(){
        $room_key = $this->request->get("room_key");
        $room_name = $this->request->get("room_name");
        $where = "room_key = '".$room_key."'";
        $data["room_name"] = addslashes($room_name);
        $obj_Room = new RoomTable($this->get_dsn());
        $obj_Room->update($data, $where);
        $this->request->set("room_key", $room_key);
        $this->action_room_setting();
    }

    //ご案内窓口 パスワード設定
    function action_set_password(){
        $room_key = $this->request->get("room_key");
        $password = $this->request->get("password");
        $this->logger->trace(__FUNCTION__, __FILE__, __LINE__, array(
            "room_key" => $room_key,
            "password" => $password
        ));
        $obj_Room = new RoomTable($this->get_dsn());
        $room_data = $obj_Room->get_detail($room_key);
        if ($room_data) {
            $room_data["room_password"] = $password;
            $where = "room_key = '".addslashes($room_key)."'";
            $obj_Room->update($room_data, $where);
        }
        $this->request->set("room_key", $room_key);
        $this->action_room_setting();
    }

    //ご案内窓口 パスワード解除
    function action_unset_password(){
        $room_key = $this->request->get("room_key");
        $this->logger->trace(__FUNCTION__, __FILE__, __LINE__, array(
            "room_key" => $room_key,
        ));
        $obj_Room = new RoomTable($this->get_dsn());
        $room_data = $obj_Room->get_detail($room_key);
        if ($room_data) {
            $room_data["room_password"] = "";
            $where = "room_key = '".addslashes($room_key)."'";
            $obj_Room->update($room_data, $where);
        }
        $this->request->set("room_key", $room_key);
        $this->action_room_setting();
    }

    //ご案内窓口 お客様側のカメラの利用切替
    function action_change_customer_camera_use_flg() {
        $room_key = $this->request->get("room_key");
        $obj_Room = new RoomTable($this->get_dsn());
        $room_data = $obj_Room->get_detail($room_key);
        //現在のステータスが表示もしくは設定がなければ非表示に変更(1の場合利用)
        if ($room_data["customer_camera_use_flg"] == "0" || !$room_data["customer_camera_use_flg"]){
            $room_data["customer_camera_use_flg"] = "1";
        //現在のステータスが許可であれば禁止に変更
        }elseif ($room_data["customer_camera_use_flg"] == "1"){
            $room_data["customer_camera_use_flg"] = "0";
        }
        $where = "room_key = '".addslashes($room_key)."'";
        $obj_Room->update($room_data, $where);
        $this->template->assign('data', $room_data);
        $this->action_room_setting();
    }

    //ご案内窓口 お客様側のカメラの表示・非表示切替
    function action_change_camera_status() {
        $room_key = $this->request->get("room_key");
        $obj_Room = new RoomTable($this->get_dsn());
        $room_data = $obj_Room->get_detail($room_key);
        //$addition = unserialize($room_data["addition"]);
        //$this->logger->debug("addition",__FILE__,__LINE__,$addition["customer_camera_status"]);
        //現在のステータスが表示もしくは設定がなければ非表示に変更(1の場合非表示)
        if ($room_data["customer_camera_status"] == "0" || !$room_data["customer_camera_status"]){
            $room_data["customer_camera_status"] = "1";
        //現在のステータスが非表示であれば表示に変更
        }elseif ($room_data["customer_camera_status"] == "1"){
            $room_data["customer_camera_status"] = "0";
        }
        //$room_data["addition"] = serialize($addition);
        $where = "room_key = '".addslashes($room_key)."'";
        $obj_Room->update($room_data, $where);
        $this->template->assign('data', $room_data);
        $this->action_room_setting();
    }

    //ご案内窓口 自動録画のON、OFF機能
    function action_change_video_status() {
        $room_key = $this->request->get("room_key");
        $this->logger2->info($room_key);
        $obj_Room = new RoomTable($this->get_dsn());
        $room_data = $obj_Room->get_detail($room_key);
        //現在のステータスがOFFもしくは設定がなければ自動録画するに変更(0の場合自動録画する)
        if ($room_data["default_auto_rec_use_flg"] == "0" || !$room_data["default_auto_rec_use_flg"]){
            $room_data["default_auto_rec_use_flg"] = "1";
        //現在のステータスがONであれば自動録画しないに変更
        }elseif ($room_data["default_auto_rec_use_flg"] == "1"){
            $room_data["default_auto_rec_use_flg"] = "0";
        }
        $where = "room_key = '".addslashes($room_key)."'";
        $obj_Room->update($room_data, $where);
        $this->template->assign('data', $room_data);
        $this->action_room_setting();
    }

    //ご案内窓口 お客様側SharingボタンのON、OFF機能
    function action_change_sharing_button_status() {
        $room_key = $this->request->get("room_key");
        $obj_Room = new RoomTable($this->get_dsn());
        $room_data = $obj_Room->get_detail($room_key);
        if ($room_data["customer_sharing_button_hide_status"] == "1"){
            $room_data["customer_sharing_button_hide_status"] = "0";
        }elseif ($room_data["customer_sharing_button_hide_status"] == "0"){
            $room_data["customer_sharing_button_hide_status"] = "1";
        }
        $where = "room_key = '".addslashes($room_key)."'";
        $obj_Room->update($room_data, $where);
        $this->template->assign('data', $room_data);
        $this->action_room_setting();
    }

    //ご案内窓口 お客様側Sharing3サイズ
    function action_change_sharing3_view_size() {
        $room_key = $this->request->get("room_key");
        $customer_sharing3_view_size = $this->request->get("customer_sharing3_view_size");
        $obj_Room = new RoomTable($this->get_dsn());
        $room_data = $obj_Room->get_detail($room_key);
        $addition = unserialize($room_data["addition"]);
        $addition["customer_sharing3_view_size"] = $customer_sharing3_view_size;
        $room_data["addition"] = serialize($addition);
        $where = "room_key = '".addslashes($room_key)."'";
        $obj_Room->update($room_data, $where);
        $this->template->assign('data', $room_data);
        $this->action_room_setting();
    }

    //ご案内窓口 操作権限初期設定
    function action_change_allow_flg() {
        $room_key = $this->request->get("room_key");
        $operation = $this->request->get("operation");
        $obj_Room = new RoomTable($this->get_dsn());
        $where = "room_key = '".addslashes($room_key)."'";
        $operation_flg = $obj_Room->getOne($where, $operation);
        //現在のステータスがOFFもしくは設定がなければ自動録画するに変更(0の場合自動録画する)
        if ($operation_flg == "0"){
            $operation_flg = "1";
        //現在のステータスがONであれば自動録画しないに変更
        }elseif ($operation_flg == "1"){
            $operation_flg = "0";
        }
        $data = array($operation => $operation_flg);
        $obj_Room->update($data, $where);
        $this->request->set("room_key", $room_key);
        $this->action_room_setting();
    }

    //4対3デフォルトレイアウト変更
    function action_change_4to3default_layout() {
        $room_key = $this->request->get("room_key");
        $layout_type = $this->request->get("default_layout_4to3");
        $obj_Room = new RoomTable($this->get_dsn());
        $where = "room_key = '".addslashes($room_key)."'";
        $data = array("default_layout_4to3" => $layout_type);
        $obj_Room->update($data, $where);
        $this->request->set("room_key", $room_key);
        $this->action_room_setting();
    }

    //16対3デフォルトレイアウト変更
    function action_change_16to9default_layout() {
        $room_key = $this->request->get("room_key");
        $layout_type = $this->request->get("default_layout_16to9");
        $obj_Room = new RoomTable($this->get_dsn());
        $where = "room_key = '".addslashes($room_key)."'";
        $data = array("default_layout_16to9" => $layout_type);
        $obj_Room->update($data, $where);
        $this->request->set("room_key", $room_key);
        $this->action_room_setting();
    }

    //ご案内窓口設定 バナーアップロード
    function action_banner_upload()
    {
        $message = "";
        if ($this->check_submit_key($this->_name_space)) {
            $room_key = $this->request->get('room_key');
            //バナーアップロード
            if($_FILES){
                $this->logger->info(__FUNCTION__,__FILE__,__LINE__,$_FILES);
                $move_path = $this->get_work_dir()."/new.jpg";
                $ret = $this->_bannerCheck($move_path);
                if($ret["message"]){
                    $message = $ret["message"];
                }else{
                    $this->_updateRoomBanner($room_key, $ret, $move_path);
                }
            }
        }
        return $this->action_room_setting($message);
    }

    //バナーチェック
    function _bannerCheck($move_path){

        $message = $path = $type = $banner_url = null;
        if (!$_FILES['banner']['tmp_name']) {
            $message .= '<li>'.BANNER_ERROR_FILE.'</li>';
        }else{
            $userfile = $_FILES['banner']['tmp_name'];
            $userfile_name = $_FILES['banner']['name'];
            $userfile_size = $_FILES['banner']['size'];
            $userfile_type = $_FILES['banner']['type'];
            $userfile_error = $_FILES['banner']['error'];

            if ($userfile_error > 0) {
                switch ($userfile_error) {
                    case 1 :
                        $message .= '<li>'.BANNER_ERROR_FILE_SIZE.'</li>';
                        break;
                    case 2 :
                        $message .= '<li>'.BANNER_ERROR_FILE_SIZE.'</li>';
                        break;
                    case 3 :
                        $message .= '<li>'.BANNER_ERROR_FILE_PART.'</li>';
                        break;
                    case 4 :
                        $message .= '<li>'.BANNER_ERROR_FILE_FAIL.'</li>';
                        break;
                }
            }
            if ($userfile_type != 'image/gif' && $userfile_type != 'image/pjpeg' &&
             $userfile_type != 'image/jpeg' && $userfile_type != 'image/jpg' &&
              $userfile_type != 'image/png' && $userfile_type != 'image/x-png') {
                $message .= '<li>'.BANNER_ERROR_FILE_TYPE.'</li>';
            }else{
                list ($width, $height, $type, $attr) = getimagesize($userfile);
                if ($userfile_size > 2000000) {
                    $message .= '<li>'.PROFILE_ERROR_FILE_SIZE.'</li>';
                } elseif ($height > BANNAR_HEIGHT_MAX) {
                    $message .= '<li>'.BANNER_ERROR_FILE_HEIGHT.'</li>';
                } elseif($width > BANNER_WIDTH_MAX) {
                    $message .= '<li>'.BANNER_ERROR_FILE_WIDTH.'</li>';
                } else {
                    if ($userfile_type == 'image/gif') {
                        $img_in=imagecreatefromgif( $userfile );
                    } else if ($userfile_type == 'image/png' || $userfile_type == 'image/x-png') {
                        $img_in=imagecreatefrompng( $userfile );
                    } else {
                        $img_in=imagecreatefromjpeg( $userfile );
                    }
                    ImageJPEG( $img_in, $move_path );
                    // ファイルのアップロード先
                    if (is_uploaded_file($userfile)) {
                        if (!move_uploaded_file($userfile, $move_path)) {
                            $message .= '<li>'.BANNER_ERROR_FILE_MOVE.'</li>';
                        }
                    } else {
                        $message .= '<li>'.BANNER_ERROR_FILE_ATTACK.'</li>';
                    }
                }
            }
        }
        return array("message" => $message, "path" => $move_path, "type" => $type,
                         "width" => $width, "height" => $height, "url" => $banner_url);
    }

    //バナーアップデート
    function _updateRoomBanner($room_key, $banner_info, $move_path){

        $user_info = $this->session->get("user_info");
        if ($move_path) {
            if(!is_dir(N2MY_DOCUMENT_ROOT.'banner')) {
                mkdir( N2MY_DOCUMENT_ROOT.'banner' );
                chmod( N2MY_DOCUMENT_ROOT.'banner' ,0777 );
            }
            $up_dir = N2MY_DOCUMENT_ROOT."banner/".$user_info["user_id"]."/";
            $this->logger2->info($up_dir,"up_dir");
            if (!is_dir($up_dir)) {
                if (!mkdir($up_dir)) {
                    $this->logger->error(__FUNCTION__."#error_mkdir", __FILE__, __LINE__, $up_dir);
                } else {
                    chmod( $up_dir ,0777 );
                }
            }
            $upfile = $up_dir.$room_key.".jpg";
            if (file_exists($upfile)) {
                unlink($upfile);
            }
            if (!rename($move_path, $upfile)) {
                $message .= '<li>'.BANNER_ERROR_FILE_MOVE.'</li>';
            }
        }
        $this->logger->info("banner_info",__FILE__,__LINE__,$banner_info);
        $obj_Room = new RoomTable($this->get_dsn());
        $room_data = $obj_Room->get_detail($room_key);
        $addition = unserialize($room_data["addition"]);
        if ($banner_info["width"] == "delete") {
            $addition["banner_width"] = "";
        } else if ($banner_info["width"]) {
            $addition["banner_width"] = addslashes($banner_info["width"]);
        }
        if ($banner_info["height"] == "delete") {
            $addition["banner_height"] = "";
        } else if ($banner_info["height"]) {
            $addition["banner_height"] = addslashes($banner_info["height"]);
        }
        if (($banner_info["width"] == "delete") && ($banner_info["height"] == "delete")) {
            $up_dir = realpath(dirname(__FILE__).'/../..')."/banner/";
            $upfile = $up_dir.$room_key.".jpg";
            if (file_exists($upfile)) {
                unlink($upfile);
            }
        }
        if ($banner_info["banner_url"] == "delete") {
            $addition["banner_url"] = "";
        } else if ($banner_info["banner_url"]) {
            $addition["banner_url"] = htmlspecialchars($banner_info["banner_url"]);
        }
        $this->logger->info("banner_url",__FILE__,__LINE__,$addition);
        $addition = serialize($addition);
        $data = array("addition" => $addition,);
        $where = "room_key = '".addslashes($room_key)."'";
        $obj_Room->update($data, $where);
    }

    ////ご案内窓口設定 バナー解除
    function action_banner_clear()
    {
        if ($this->check_submit_key($this->_name_space)) {
            $ret = array("width" => "delete", "height" => "delete");
            $this->_updateRoomBanner($this->request->get('room_key'), $ret, null);
        }
        return $this->action_room_setting();
    }

    //ご案内窓口設定 バナーURL登録
    function action_set_banner_url()
    {
        $message = "";
        if ($this->check_submit_key($this->_name_space)) {
            $room_key = $this->request->get("room_key");
            $banner_url = $this->request->get("banner_url");
            $this->logger->info("parse_url",__FILE__,__LINE__,$banner_url);
            $check_url = parse_url($banner_url);
            if (!$check_url["scheme"]) {
                $message = '<li>'.BANNER_ERROR_URL_HTTP.'</li>';
            } else if (!preg_match("/^[!-~]+$/", $banner_url)) {
                $message = '<li>'.BANNER_ERROR_URL_INVALID.'</li>';
            } else if (!preg_match("/^(http|HTTP)(s|S)?:\/\/+[A-Za-z0-9]+\.[A-Za-z0-9]/",$banner_url)) {
                $message = '<li>'.BANNER_ERROR_URL_INVALID.'</li>';
            } else {
                $ret = array("banner_url" => $banner_url);
                $this->_updateRoomBanner($room_key, $ret, null);
            }
        }
        return $this->action_room_setting($message);
    }

    //ご案内窓口設定 バナーURL解除
    function action_unset_banner_url()
    {
        if ($this->check_submit_key($this->_name_space)) {
            $ret = array("banner_url" => "delete");
            $this->_updateRoomBanner($this->request->get('room_key'), $ret, null);
        }
        return $this->action_room_setting();
    }

    //プロフィール設定（スタッフページ）
    function action_profile_setting($message = "") {
        $country_key = $this->session->get('country_key');
        $user_info = $this->session->get('user_info');

        // 部屋詳細取得
        $obj_Room = new RoomTable($this->get_dsn());
        $where = "user_key = ".$user_info["user_key"].
                 " AND room_key = '".addslashes($this->request->get("room_key"))."'";
        $room_info = $obj_Room->getRow($where);

        $obj_Member = new MemberTable($this->get_dsn());
        require_once("classes/dbi/member_room_relation.dbi.php");
        $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
        $where = "room_key = '".addslashes($room_info["room_key"])."'";
        $room_relation = $objRoomRelation->getRow($where);

        $where = "user_key = ".$user_info["user_key"].
                 " AND member_key = ".$room_relation["member_key"];
        $outbound_id = $obj_Member->getOne($where,"outbound_id");
        $room_info["outbound_url"] = N2MY_BASE_URL."outbound/".$outbound_id;

        $addition = unserialize($room_info["addition"]);
        $room_info["video_tag"] = $addition["video_tag"];
        $room_info["title"] = $addition["title"];
        if ($message) {
            $room_info["email"] = $this->request->get("error_email");
        } else {
            $room_info["email"] = $addition["email"];
        }
        $room_info["photo"] = $addition["room_image"];

        $room_key = $this->request->get('room_key');

        $this->set_submit_key($this->_name_space);
        $this->template->assign('message', $message);
        $this->template->assign('data', $room_info );
        header("Cache-Control: no-cache, must-revalidate");
        $this->display('user/salesSetting/profile_edit.t.html');
    }

    //プロフィール設定更新
    function action_profile_update() {
        if ($this->check_submit_key($this->_name_space)) {

            $room_key     = $this->request->get("room_key");
            $comment      = $this->request->get("comment");
            $sort         = $this->request->get("sort");
            $profile      = $this->request->get("addition");
            $image_path   = $this->request->get("image_path");
            $this->logger2->info($profile);

            $obj_Room = new RoomTable($this->get_dsn());
            $where        = "room_key = '".addslashes($room_key)."'";
            $room_data = $obj_Room->get_detail($room_key);
            $addition = unserialize($room_data["addition"]);
            $addition["flicli"] = $profile["flicli"];
            $addition["title"]  = $profile["title"];
            if ($profile["email"] && !EZValidator::valid_email($profile["email"])) {
                $message = '<li>'.PROFILE_ERROR_EMAIL_INVALID.'</li>';
                $this->request->set("error_email", $profile["email"]);
            } else {
                $addition["email"]  = $profile["email"];
            }
            //イメージアップロード
            if($_FILES["file"]["error"] == 0 || $_REQUEST["image"]){
                $new_image = $this->_picture_update($room_key);
                $addition["room_image"] = $new_image ? $new_image : $image_path;
            }

            $addition = serialize($addition);
            $data = array(
                "comment"    => $comment,
                "addition"   => $addition
            );
            $obj_Room->update($data, $where);

        }
        $this->action_profile_setting($message);
    }

    function _picture_update($room_key)
    {
        $file_name    = $_FILES["file"];

        //イメージファイル2MB以内だったら処理開始
        if($_FILES["file"]["size"] > 2000000) {
            $error = PROFILE_ERROR_FILE_SIZE;
            $this->logger->info("error",__FILE__,__LINE__,$error);
            $this->template->assign("error", $error);
        } else if($file_name["type"] != "image/jpeg" &&
           $file_name["type"] != "image/jpg" &&
           $file_name["type"] != "image/pjpeg" &&
           $file_name["type"] != "image/gif" &&
           $file_name["type"] != "image/png" &&
           $file_name["type"] != "image/x-png" &&
           $file_name["type"] != "") {
            $error = NEWS_ERROR_FILE_TYPE;
            $this->logger->info("error",__FILE__,__LINE__,$error);
            $this->template->assign("error", $error);
        } else if($_FILES["file"]["error"] == 0) {
            $this->logger->info("FILES",__FILE__,__LINE__,$_FILES["file"]);
            $file_tmp  = $file_name["tmp_name"];
            $user_info = $this->session->get('user_info');
            if(!is_dir(N2MY_DOCUMENT_ROOT.'photo')) {
                mkdir( N2MY_DOCUMENT_ROOT.'photo' );
                chmod( N2MY_DOCUMENT_ROOT.'photo' ,0777 );
            }
            $up_dir = N2MY_DOCUMENT_ROOT.'photo/'.$user_info["user_id"];
            $this->logger->info("up_dir",__FILE__,__LINE__,$up_dir);
            if( !is_dir( $up_dir ) ){
                mkdir( $up_dir );
                chmod( $up_dir ,0777 );
            }

            // 最大の高さ・幅を設定します
            $max_width = 420;
            $max_height = 332;
            // コンテントタイプ
            header('Content-type: image/jpeg');
            // 新規サイズを取得します
            list($width_original, $height_original, $type, $attr) = getimagesize($file_tmp);
            //設定サイズより大きかったら縮小
            if ($max_width < $width_original || $max_height < $height_original){
                $original_size = $width_original / $height_original;
                $this->logger->info("size",__FILE__,__LINE__,$original_size);
                // 画像比率を計算
                if ($max_width / $max_height > $original_size) {
                    $max_width = $max_height * $original_size;
                } else {
                    $max_height = $max_width / $original_size;
                }
                // 再サンプル
                $image_create = imagecreatetruecolor($max_width, $max_height);
                // JPG出力
                if($file_name["type"] == "image/jpeg" ||
                    $file_name["type"] == "image/jpg" ||
                    $file_name["type"] == "image/pjpeg"){
                        $up_file = $up_dir."/".$room_key.".jpg";
                    $image = imagecreatefromjpeg($file_tmp);
                    imagecopyresampled($image_create, $image, 0, 0, 0, 0, $max_width, $max_height, $width_original, $height_original);
                    $file = imagejpeg($image_create, $up_file, 100);
                    $file_type = "jpg";
                //GIF出力
                } else if($file_name["type"] == "image/gif"){
                    $up_file = $up_dir."/".$room_key.".gif";
                    $image = imagecreatefromgif($file_tmp);
                    imagecopyresampled($image_create, $image, 0, 0, 0, 0, $max_width, $max_height, $width_original, $height_original);
                    $file = imagegif($image_create, $up_file, 100);
                    $file_type = "gif";
                //PNG出力
                } else if($file_name["type"] == "image/png" ||
                          $file_name["type"] == "image/x-png"){
                    $up_file = $up_dir."/".$room_key.".png";
                    $image = imagecreatefrompng($file_tmp);
                    imagecopyresampled($image_create, $image, 0, 0, 0, 0, $max_width, $max_height, $width_original, $height_original);
                    $file = imagepng($image_create, $up_file, 9);
                    $file_type = "png";
                }
            } else {
                if($file_name["type"] == "image/jpeg" ||
                    $file_name["type"] == "image/jpg" ||
                    $file_name["type"] == "image/pjpeg"){
                    $up_file = $up_dir."/".$room_key.".jpg";
                    move_uploaded_file($file_tmp, $up_file);
                    $file_type = "jpg";
                //GIF出力
                } else if($file_name["type"] == "image/gif"){
                    $up_file = $up_dir."/".$room_key.".gif";
                    move_uploaded_file($file_tmp, $up_file);
                    $file_type = "gif";
                //PNG出力
                } else if($file_name["type"] == "image/png" ||
                          $file_name["type"] == "image/x-png"){
                    $up_file = $up_dir."/".$room_key.".png";
                    move_uploaded_file($file_tmp, $up_file);
                    $file_type = "png";
                }
            }
            if (file_exists($up_file)) {
                switch ($file_type) {
                    case "jpg":
                        if (file_exists($up_dir."/".$room_key.".png")) {
                            unlink($up_dir."/".$room_key.".png");
                        }
                        if (file_exists($up_dir."/".$room_key.".gif")) {
                            unlink($up_dir."/".$room_key.".gif");
                        }
                        break;
                    case "gif":
                        if (file_exists($up_dir."/".$room_key.".png")) {
                            unlink($up_dir."/".$room_key.".png");
                        }
                        if (file_exists($up_dir."/".$room_key.".jpg")) {
                            unlink($up_dir."/".$room_key.".jpg");
                        }
                        break;
                    case "png":
                        if (file_exists($up_dir."/".$room_key.".jpg")) {
                            unlink($up_dir."/".$room_key.".jpg");
                        }
                        if (file_exists($up_dir."/".$room_key.".gif")) {
                            unlink($up_dir."/".$room_key.".gif");
                        }
                        break;
                }
                //$addition["room_image"] = "/photo/".$user_info["user_id"]."/".$room_key.".".$file_type;
                $image_path = "/photo/".$user_info["user_id"]."/".$room_key.".".$file_type;
            } else {
                $this->logger->error(__FUNCTION__,__FILE__,__LINE__, "file up error");
            }

            $this->logger->debug(__FUNCTION__,__FILE__,__LINE__, array($room_key, $image_path, $file));
        }
        return $image_path;
    }

    //画像表示
    /*
    function action_image() {
        $room_key = $this->request->get("room_key");
        $obj_Room = new RoomTable($this->get_dsn());
        $where = "room_key = '".$room_key."'";
        $room_info = $obj_Room->getRow($where, "room_image");
        $this->logger->trace(__FUNCTION__, __FILE__, __LINE__, $room_info);
        $obj_id = $room_info["room_image"];
        if ($obj_id) {
            $obj_FileObject = new FileObjectTable($this->get_dsn());
            $file_info = $obj_FileObject->getFile($obj_id);
            if ($file_info["del_flg"] != "1") {
                $type = $file_info["type"];
                header("Content-Type: ".$type);
                print $file_info["source"];
                exit();
            }
        }
    }*/

    //画像削除
    function action_delete_photo() {
        $room_key = $this->request->get("room_key");
        $obj_Room = new RoomTable($this->get_dsn());
        $where = "room_key = '".$room_key."'";
        $room_info = $obj_Room->getRow($where);
        $addition = unserialize($room_info["addition"]);
        $delete_dir = N2MY_DOCUMENT_ROOT.$addition["room_image"];

        $addition["room_image"] = "";
        $data = array (
            "addition" => serialize($addition),
        );
        $this->logger->debug("addition",__FILE__,__LINE__,$delete_dir);
        if (file_exists($delete_dir)) {
            unlink($delete_dir);
        }
        $obj_Room->update($data, $where);
        $this->action_profile_setting();
    }

    //一言メッセージ一覧
    function action_show_message() {
        //メッセージを取得
        $room_key = $this->request->get("room_key");
        $obj_CustomerMessage = new CustomerMessageTable($this->get_dsn());
        $where = "room_key = '".addslashes($room_key)."'".
                 " AND status = 1";
        $allnum = $obj_CustomerMessage->numRows($where);
        $this->template->assign("total", $allnum);
        // ページ情報取得
        $page_cnt = $this->request->get("page_cnt");
        if (!$page_cnt) {
            $page_cnt = 10;
        }
        $current_page = $this->request->get("page", 1);
        $current_page = ($current_page > 1) ? $current_page :1;
        $pager = $this->setPager($page_cnt, $current_page, $allnum);
        $this->template->assign('pager', $pager);

        if ($pager["start"] > 0) {
            $offset = $pager["start"] - 1;
        } else {
            $offset = 0;
        }

        if($this->request->get("sort_key")){
            $sort_type = $this->request->get("sort_key");
        }else{
            $sort_type = "desc";
        }
        $sort_key = array ( "send_datetime" => $sort_type);
        $this->logger->trace("sort",__FILE__,__LINE__,$sort_key);
        $message_list = $obj_CustomerMessage->getRowsAssoc($where, $sort_key, $page_cnt, $offset);

        $this->logger->trace("message",__FILE__,__LINE__,$message_list);
        $this->template->assign("room_key", $room_key);
        $this->template->assign("sort_key", $sort_type);
        $this->template->assign("message_list", $message_list);
        $this->display('user/salesSetting/profile_message.t.html');
    }

    //一言メッセージ詳細
    function action_message_detail() {
        $no = $this->request->get("no");
        $room_key = $this->request->get("room_key");
        $user_info = $this->session->get("user_info");
        $obj_CustomerMessage = new CustomerMessageTable($this->get_dsn());
        $where = "room_key = '".addslashes($room_key)."'".
                 " AND user_key = ".$user_info["user_key"].
                 " AND no = ".$no;
        $message = $obj_CustomerMessage->getRow($where);
        $this->template->assign("message", $message);
        $this->display('user/salesSetting/message_detail.t.html');
    }

    //一言メッセージ削除
    function action_delete_message(){
        $room_key = $this->request->get("room_key");
        $message_id = $this->request->get("message_id");
        $this->logger->info("id",__FILE__,__LINE__,$message_id);
        if(!$message_id){
            $message .= "<li>".PROFILE_MESSAGE_DELETE_ERROR . "</li>";
            $this->template->assign('message', $message);
            $this->action_show_message($room_key);
        }else{
            $obj_CustomerMessage = new CustomerMessageTable($this->get_dsn());
            foreach ($message_id as $_key => $value) {
                $this->logger->info("value",__FILE__,__LINE__,$value);
                $where = "room_key = '".addslashes($room_key)."'".
                         " AND no = ".$value;
                $data = array(
                    "status" => "0",
                );
                $obj_CustomerMessage->update($data, $where);
            }
            $message .= "<li>".PROFILE_MESSAGE_DELETE . "</li>";
            $this->template->assign('message', $message);
            $this->action_show_message($room_key);
        }
    }

    //アカウント情報変更
    function action_member_edit($message = "") {
        if($message){
            $this->template->assign('message', $message);
        }
        //room_keyからメンバー情報取得
        if ($this->request->get('mode') == "edit_user") {
            $room_key = $this->request->get("room_key");
            require_once("classes/dbi/member_room_relation.dbi.php");
            $objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
            $where = "room_key = '".addslashes($room_key)."'";
            $room_relation = $objRoomRelation->getRow($where);
            $memberTable = new MemberTable($this->get_dsn());
            $where = "member_key = ".$room_relation["member_key"];
            $member_info = $memberTable->getRow($where);
        }else{
            $member_info = $this->session->get("member_edit_info");
        }
        $this->template->assign("member_info", $member_info);
        $this->display("user/salesSetting/member/index.t.html");
    }

    //メンバー情報修正確認画面
    function action_edit_member_confirm(){
        // 入力値を保持
        $request = $this->request->getAll();
        $this->session->set('member_edit_info', $request);

        $message = null;
        if (!$request['member_name']) {
            $message .= '<li>'.MEMBER_ERROR_NAME . '</li>';
        }
        if (!$request['member_pass'] && !$request['member_pass_check']) {
            $message .= '<li>'.MEMBER_ERROR_PASS . '</li>';
        } elseif ($request['member_pass'] != $request['member_pass_check']) {
            $message .= '<li>'.MEMBER_ERROR_PASS_DIFF . '</li>';
        } elseif (!preg_match('/^[!-\[\]-~]{8,128}$/', $request['member_pass'])) {
            $message .= '<li>'.USER_ERROR_PASS_INVALID_01. '</li>';
        } elseif (!preg_match('/[[:alpha:]]+/',$request['member_pass']) || preg_match('/^[[:alpha:]]+$/',$request['member_pass'])) {
            $message .= '<li>'.USER_ERROR_PASS_INVALID_02. '</li>';
        }
        if ($message) {
            $this->action_member_edit($message);
        } else {
            foreach($request as $key => $value){
                if($key == "member_pass"){
                    $length = mb_strlen($value);
                    $value = "";
                    for($i=0;$i<$length;$i++){
                        $value .= "*";
                    }
                }
                $this->template->assign($key, $value);
            }
            $this->display("user/salesSetting/member/confirm.t.html");
        }
    }

    //メンバー情報修正確認完了画面
    function action_edit_member_complete(){
        $request = $this->session->get("member_edit_info");
        $temp = array(
            "member_pass" 	=> EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $request['member_pass']),
            "member_email" 	=> $request["member_email"],
            "member_name" 	=> $request["member_name"],
        );
        // 更新処理
        $memberTable = new MemberTable($this->get_dsn());
        $where = "member_id = '".addslashes($request["member_id"])."'";
        $ret = $memberTable->update($temp, $where);
        // エラー処理
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#DB ERROR!",__FILE__,__LINE__,$ret->getUserInfo());
        } else {
            $this->logger->info(__FUNCTION__."#edit member successful!",__FILE__,__LINE__,$temp);
            $user_info = $this->session->get("user_info");
            $user_info["m_name"] = $request["member_name"];
            $this->session->set("user_info", $user_info);
        }
        $this->logger->debug(__FUNCTION__."#member_edit",__FILE__,__LINE__,$temp);
        $this->session->remove("member_edit_info");
        $this->display("user/salesSetting/member/done.t.html");
    }

    //部屋のソートを変更
    function action_up(){
        $obj_Room = new RoomTable($this->get_dsn());
        $user_info = $this->session->get('user_info');
        //メンバーの場合は無効
        if(!$user_info["m_room_key"]){
            $obj_Room->up_sort($user_info["user_key"],$_REQUEST["room_key"]);
        }
        $this->action_show_roomlist();
    }

    //部屋のソートを変更
    function action_down(){
        $obj_Room = new RoomTable($this->get_dsn());
        $user_info = $this->session->get('user_info');
        //メンバーの場合は無効
        if(!$user_info["m_room_key"]){
            $obj_Room->down_sort($user_info["user_key"],$_REQUEST["room_key"]);
        }
        $this->action_show_roomlist();
    }

    /**
     * @param $integer limit １ページの件数
     * @param $integer offset 開始件数
     * @param $integer page カレントページ
     * @param $total_count トータル件数
     *
     */
    function set_pager($limit, $page, $total_count) {
        $offset = ($limit * ($page - 1));
        $pager_options = array(
            'mode' => 'Sliding',
            'perPage' => $limit,
            'currentPage' => $page,
            'urlVar' => "action_reservation&page",
            'delta' => 3,
            'totalItems' => $total_count,
            'importQuery' => false,
            'firstPagePost' => '',
            );
        $pager = & Pager::factory($pager_options);
        $links = $pager->getLinks();
        $pager_info = array(
            "links" => $links,
            "limit" => $limit,
            "range" => $pager->range,
            "total" => $total_count,
            "current" => $pager->getCurrentPageID(),
            "next" => $pager->getNextPageID(),
            "prev" => $pager->getPreviousPageID(),
            "last" => $pager->numPages(),
            "start" => (($total_count) ? ($offset + 1) : 0),
            "end" => ($total_count < ($offset + $limit)) ? $total_count : ($offset + $limit)
            );
        return $pager_info;
    }
}

$main = new AppSalesSetting();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
