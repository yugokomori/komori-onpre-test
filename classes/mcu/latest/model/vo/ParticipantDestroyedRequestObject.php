<?php 
class ParticipantDestroyedRequestObject {
	private $confId;
	private $partId;
	
	function __construct($request) {
		$this->confId	= $request->confId;
		$this->partId	= $request->partId;
	}
	
	public function __get($key) {
		if (property_exists($this, $key)) {
			return $this->$key;
		}
		else {
			return null;
		}
	}
}