<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("classes/N2MY_DBI.class.php");

class MeetingInvitationTable extends N2MY_DB {

    var $table = "meeting_invitation";
    var $logger = null;
    protected $primary_key = "invitation_key";

    function __construct( $dsn ) {
        $this->init($dsn, $this->table);
    }

    function add($data) {
        $data["entrytime"] = date("Y-m-d H:i:s");
        return parent::add($data);
    }

    function update($data, $where) {
        $data["updatetime"] = date("Y-m-d H:i:s");
        return parent::update($data, $where);
    }

}
