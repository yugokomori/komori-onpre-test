<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/N2MY_DBI.class.php");

class UserPlanTable extends N2MY_DB {

    var $table = 'user_plan';
    protected $primary_key = "user_plan_key";

    function UserPlanTable( $dsn ) {
        $this->init( $dsn, $this->table );
    }
}


/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
