<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once 'lib/EZLib/EZUtil/EZLanguage.class.php';
require_once 'classes/AppFrame.class.php';
require_once 'classes/dbi/news.dbi.php';

class AppNews extends AppFrame {
    var $objNewsTable = null;
    var $_name_space = null;

    function init () {
        $this->_name_space = md5(__FILE__);
        $this->objNewsTable = new NewsTable($this->get_auth_dsn());
        $_lang_list = $this->get_language_list();
        foreach($_lang_list as $key => $val) {
            $lang_key = EZLanguage::getLangCd($key);
            $this->lang_list[$lang_key] = $val;
        }
    }

    function auth() {
        // 管理者以上の権限
    }

    function default_view() {
        $this->action_top();
    }

    //ニュースの一覧、メニューの表示
    function action_top() {
        $news_info = array();
        $lang = $this->request->get("lang", $this->get_language());
        $ret = $this->objNewsTable->getList($lang);
        while($info = $ret->fetchRow(DB_FETCHMODE_ASSOC)){
            $news_info[] = array(
                "id"        => $info['news_key'],
                "date"      => $info['news_date'],
                "title"     => htmlspecialchars(substr($info['news_title'],0,100)),
                "contents"  => htmlspecialchars(substr($info['news_contents'], 0, 100)),
                "type"      => ($info['news_show'] == '0') ? "activate" : "deactivate",
                "status"    => $info['news_show']
            );
        }
        $this->template->assign('lang_list', $this->lang_list);
        $this->template->assign('lang', $lang);
        $this->template->assign('info', $news_info);
        $this->display('admin_tool/news/index.t.html');
    }

    //新規ニュースのフォーム表示
    function action_form() {
        $news_info["lang"] = $this->request->get("lang", "ja");
        $news_info["date"] = date("Y-m-d");
        $this->session->set('news_info', $news_info);
        $this->display_form();
    }

    function action_back_form() {
        $this->display_form();
    }

    function display_form($err_msg = null) {
        $news_info = $this->session->get('news_info');
        $this->template->assign('lang_list', $this->lang_list);
        $this->template->assign('err_msg', $err_msg);
        $this->template->assign('news_info', $news_info);
        $this->display('admin_tool/news/create.t.html');
    }

    function action_confirm($del = ''){
        if($del != 'del'){
            $this->session->set('news_info', $_POST);
        }
        $news_info = $this->request->getAll();
        $this->session->set('news_info', $news_info);
        $rules = array(
            "date"     => array(
                "required" => true,
                ),
            "title"    => array(
                "required"  => true,
                ),
            "contents" => array(
                "required" => true,
            )
        );
        $err_obj = $this->objNewsTable->check($news_info, $rules);
        if (EZValidator::isError($err_obj)) {
            $msg_format = $this->get_message("error");
            $msg = $this->get_message("ERROR");
            $err_fields = $err_obj->error_fields();
            if (!$err_fields) {
                return array();
            } else {
                foreach($err_fields as $field) {
                    $type = $err_obj->get_error_type($field);
                    $err_rule = $err_obj->get_error_rule($field, $type);
                    if (($type == "allow") || $type == "deny") {
                        $err_rule = join(", ", $err_rule);
                    }
                    $_msg = sprintf($msg[$type], $err_rule);
                    $err_msg[$field] = sprintf($msg_format, $_msg);
                }
            }
            return $this->display_form($err_msg);
        }
        $this->set_submit_key();
        $this->template->assign('news_info', $news_info);
        $this->display('admin_tool/news/confirm.t.html');
    }

    //ＤＢにインサート
    function action_complete(){
        if (!$this->check_submit_key()) {
            return $this->action_top();
        }
        $news_info = $this->session->get('news_info');
        $this->objNewsTable->complete($news_info, $news_info["id"]);
        $this->session->remove('news_info');
        return $this->action_top();
    }

    //編集
    function action_edit() {
        $where = "news_key = ".addslashes($this->request->get('id'));
        $news = $this->objNewsTable->getRow($where);
        $this->session->remove('news_info');
        $info['id']         = $news['news_key'];
        $info['lang']       = $news['news_lang'];
        $info['date']       = $news['news_date'];
        $info['title']      = $news['news_title'];
        $info['contents']   = $news['news_contents'];
        $this->session->set('news_info', $info);
        $this->display_form();
    }

    //削除確認
    function action_deleteconfirm() {
        $where = "news_key = ".$_REQUEST['id'];
        $ret = $this->objNewsTable->select($where);
        $news = $ret->fetchRow(DB_FETCHMODE_ASSOC);
        $info['id']       = $news['news_key'];
        $info['date']     = $news['news_date'];
        $info['title']    = $news['news_title'];
        $info['contents'] = $news['news_contents'];
        $this->session->remove('news_info');
        $this->session->set('news_info', $info);
        $this->action_confirm('del');
    }

    //削除
    function action_delete(){
        $this->objNewsTable->newsDelete($_REQUEST['id']);
        $this->action_top();
        $this->session->remove('news_info');
    }

    //表示ステータスの変更
    function action_status(){
        $this->objNewsTable->changeStatus($_REQUEST['id'],$_REQUEST['type']);
        $this->action_top();
    }
}

$main =& new AppNews();
$main->execute();
/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>