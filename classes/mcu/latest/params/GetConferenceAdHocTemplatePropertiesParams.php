<?php 
require_once(dirname(__FILE__)."/../params/WSDLParams.php");

class GetConferenceAdHocTemplatePropertiesParams extends WSDLParams {
	protected $did;
	public function __construct($obj) {
		parent::__construct($this, $obj);
	}
	
	public function getParams() {
		return parent::getParams($this);
	}
}
