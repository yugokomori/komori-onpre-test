<?php

require_once("classes/mcu/config/McuConfigProxy.php");
require_once(dirname(__FILE__)."/../../../controller/conference/destroy/interface/IConference.php");
require_once(dirname(__FILE__)."/../../../controller/conference/destroy/interface/IConferenceParticipant.php");
require_once(dirname(__FILE__)."/../../../controller/conference/destroy/interface/IParticipant.php");
require_once(dirname(__FILE__)."/../../../logic/conference/ConferenceDestroyedLogic.php");

class DestroyedConferenceController implements IConference, IConferenceParticipant {
    private $logger;
    private $logic;
	private $soap;
    
    function __construct($model){
        $this->logic = new ConferenceDestroyedLogic($model);
		$this->logger = EZLogger2::getInstance();
    }
    
    private function initialize() {}
    
    // IConference
    public function setConferenceRecord() {
		$this->logic->setConferenceRecord();
	}
	
	public function removeConferenceRecord() {
		$this->logic->removeConferenceRecord();
	}
	
	public function destroyConference() {
		$this->logic->destroyConference();
	}
    // IConference

	// IConferenceParticipant
	public function setConferenceParticipantList() {
		$this->logic->setConferenceParticipantListByDid();
	}
	
	public function clearConferenceParticipantRecord() {
		$this->logic->clearConferenceParticipantRecordByDid();
	}
	// IConferenceParticipant
	
	//IParticipant
	public function clearParticipantRecord() {
		$this->logic->clearParticipantRecord();
	}
	//IParticipant
    
    public function execute() {
    	try {
	    	$this->setConferenceRecord();
	    	
			$this->logic->removeConferenceRecord();
	    	
	    	$this->setConferenceParticipantList();
	    	if ($this->logic->conferenceParticipantRecordList) {
	    		
				$this->clearParticipantRecord();
				
				$this->clearConferenceParticipantRecord();
	    	}

	    	$this->logic->clearCallingParticipantRecord();
	    	
// 	    	$this->logic->stopRenderingInSlot();
	    	if ($this->logic->irregal) {
	    		// reset	    		
	    	}
    	}
    	catch (Exception $e) {
    		// reset
    		
    		// reset
    		throw $e;
    	}
    }
}
