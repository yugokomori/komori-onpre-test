<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
// +----------------------------------------------------------------------+
// |                                                                      |
// | メール送受信ファクトリークラス                                       |
// |                                                                      |
// +----------------------------------------------------------------------+
// | PHP version 4                                                        |
// +----------------------------------------------------------------------+
// | Authors: Masayuki IIno <masayuki.iino@gmail.com>                     |
// |                                                                      |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006 Masayuki IIno all rights reserved.                |
// +----------------------------------------------------------------------+
//
// $Id: EZMail.class.php,v 1.1 2006/01/23 01:25:49 iino Exp $

require_once('lib/EZLib/EZMail/EZMesg.class.php');
require_once('lib/EZLib/EZMail/EZPop3.class.php');
require_once('lib/EZLib/EZMail/EZSmtp.class.php');

// ----------------------------   define  ---------------------------------

// {{{ -- define
// }}}

/**
 *【EZMail】メール送受信ファクトリークラス
 *
 * <b>説明</b><br />
 *
 * <code>
 * </code>
 *
 * @access     public
 * @author     Masayuki IIno <masayuki.iino@gmail.com>
 * @package    EZLib
 * @subpackage EZMail
 * @version    $Revision
 */
class EZMail
{
// ----------------------------    vars   ---------------------------------

// {{{ -- vars

    /**
     * Mesgオブジェクト
     *
     * @access private
     * @var    object EZMesg
     */
    var $mesg = null;

    /**
     * Pop3オブジェクト
     *
     * @access private
     * @var    object EZPop3
     */
    var $pop3 = null;

    /**
     * Smtpオブジェクト
     *
     * @access private
     * @var    object EZSmtp
     */
    var $smtp = null;

// }}}

// ----------------------------    init   ---------------------------------

// {{{ -- EZMail()

    /**
     * コンストラクタ
     *
     * @access public
     * @param
     * @return
     */
    function EZMail()
    {
        //create Mesg object
        $this->mesg =& new EZMesg();

        //create Pop3 object
        $this->pop3 =& new EZPop3();

        //create Smtp object
        $this->smtp =& new EZSmtp();
    }

// }}}
// {{{ -- _EZMail()

    /**
     * デストラクタ
     *
     * @access private
     * @param
     * @return
     */
    function _EZMail()
    {
    }

// }}}

// ---------------------------- interface ---------------------------------

// ----------------------------  private  ---------------------------------

// ----------------------------   public  ---------------------------------

// ----------------------------   option  ---------------------------------

}
/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
