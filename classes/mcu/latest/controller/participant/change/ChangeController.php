<?php

require_once("classes/mcu/config/McuConfigProxy.php");
require_once("classes/mcu/model/gateway/McuGatewayProxy.php");

require_once(dirname(__FILE__)."/../../../controller/participant/change/client/CalledParticipant.php");
require_once(dirname(__FILE__)."/../../../controller/participant/change/client/ChangeStateParticipant.php");
require_once(dirname(__FILE__)."/../../../controller/participant/change/client/FailedCalleeParticipant.php");

require_once(dirname(__FILE__)."/../../../model/do/State.php");
require_once(dirname(__FILE__)."/../../../model/ConferenceProxy.php");
require_once(dirname(__FILE__)."/../../../model/MeetingProxy.php");
require_once(dirname(__FILE__)."/../../../model/MCUControllerProxy.php");


require_once(dirname(__FILE__)."/../../../logic/participant/ParticipantStateChangeLogic.php");
require_once(dirname(__FILE__)."/../../../logic/FMSNetConnectionController.php");

class ChangeController {
	static function create($model) {
		$configProxy 	= new McuConfigProxy();
		$dsn = $configProxy->getDsn();
		$logger = EZLogger2::getInstance();
		
		$requestProxy = $model;
		
		$conferenceProxy = ConferenceProxy::getInstance();
		$mcuControllerProxy = MCUControllerProxy::getInstance();
		$meetingProxy = MeetingProxy::getInstance();
		
		// conference record
		$conferenceProxy->setActiveConferenceByConfIdThrowable($requestProxy->confId);
		
		// meeting record
		$meetingProxy->setActiveMeetingRecordByMeetingKeyThrowable($conferenceProxy->activeConferenceRecord["meeting_key"]);
		
		//BFCP中のポリコム端末を終了する際、処理を追加
		$fmsController = new FMSNetConnectionController($meetingProxy->activeMeetingRecord);
		$fmsController->connect();
		$return = $fmsController->getDocumentSharingStatus();
		if ($requestProxy->partState == "DISCONNECTED" && $requestProxy->partId == $return->partId ) {
			$mcuControllerProxy->stopRenderingInSlot($requestProxy->confId);
			$fmsController->noticeBFCPStatus(false);
			$mcuControllerProxy->stopDocSharing($requestProxy->confId, $return->partId);
		}
		
		try {
			$logic = new ParticipantStateChangeLogic($model);
			$logic->setConferenceParticipantRecord();
		}
		catch(Exception $e) {
			$logger->info($e->getMessage());
			return null;
		}
		
		if ($logic->isConnectionCompleteCallerParticipant()) {
			return new CalledParticipant($logic);
		}
		else if ($logic->isConnectionFailedCallerParticipant()) {
			return new FailedCalleeParticipant($logic);
		}
		else
			return new ChangeStateParticipant($logic);
	}
}
