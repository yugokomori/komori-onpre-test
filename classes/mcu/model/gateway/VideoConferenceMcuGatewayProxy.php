<?php
require_once(dirname(__FILE__)."/../../model/gateway/McuGatewayProxy.php");

class VideoConferenceMcuGatewayProxy extends McuGatewayProxy {
	function __construct($settingRecord) {
		parent::__construct();

		$this->setActiveMcuServerThrowable();

		$this->definedMCUController();
	}
}
