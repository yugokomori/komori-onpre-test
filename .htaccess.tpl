#----------
# Apache
#----------
AddType "text/html; charset=UTF-8" .html
ErrorDocument 404 /services/not_found.php

<FilesMatch "^\.ht">
    Order allow,deny
    Deny from all
</FilesMatch>

# rewite
<IfModule mod_rewrite.c>
        RewriteEngine on
        RewriteRule ^r/([0-9a-z\&\=\-]+)$ /services/invite.php?action_invite=1&r_user_session=$1 [L]
        RewriteRule ^g/([a-z\-]+)/([0-9a-z\&\=]+)/([0-9a-z\&\=]+)$ /services/invite.php?action_invitedGuest=1&meeting_session_id=$2&meeting_invitation_id=$3&lang=$1 [L]
        RewriteRule ^i/([0-9a-z\&\=]+)/$ /services/reservation/index.php?action_show_preview&document_id=$1 [L]
        RewriteRule ^d/([0-9a-z\&\=]+)/([0-9]+)/$ /services/reservation/index.php?action_show_preview&document_id=$1&index=$2 [L]
        RewriteRule ^c/([0-9]+)/(.*)$ /services/cabinet.php?cabinet_id=$1&file_name=$2 [L]
        RewriteRule ^a/([0-9a-z\&\=]+)$ /services/pwReset/index.php?action_authorize_confirm&id=$1 [L]
        RewriteRule ^k/([0-9a-z\&\=]+)$ /services/pwReset/index.php?action_ignore_confirm&id=$1 [L]
        RewriteRule ^y/(.*)$ /services/invite.php?action_guest=&reservation_id=$1 [L]
        RewriteRule ^ondemand/(.*)$ /services/ondemand.php?id=$1 [L]
        RewriteRule ^minute/(.*)$ /services/ondemand.php?id=$1 [L]
        RewriteRule ^mobile/regist/(.*)$ /trial/mobile/index.php?action_regist&id=$1 [L]
        RewriteRule ^rec_gw/(.*)$ /services/ondemand.php?id=$1&rec_gw=1 [L]
        RewriteRule .svn /services/not_found.php
        RewriteRule ^p/([0-9a-z\&\=]+)$ /services/pgi/index.php?action_phone_numbers&meeting_session_id=$1 [L]
        RewriteRule ^s/([0-9a-z\&\=]+)/([0-9]+)/([0-9]+)/$ /services/storage/index.php?action_show_preview&storage_file_key=$1&index=$2&member_key=$3 [L]
        RewriteRule ^inbound/([0-9A-z\&\=]+)$ /inbound/index.php?inbound_id=$1 [L]
        RewriteRule ^outbound/([0-9A-z\&\=]+)$ /outbound/index.php?action_outbound_login&outbound_id=$1 [L]
        RewriteRule ^outbound/login/([0-9]+)$ /outbound/login/index.php?action_number_login&number_id=$1 [L]
        RewriteRule ^direct/inbound/(.*)$ /api/v1/inbound/index.php?action_random_start=&inbound_id=$1 [L]
</IfModule>

#----------
# PHP (PHP_INI_PERDIRでしか設定できないもの)
#----------
# include path
php_value include_path {INC_PATH}
# file upload
php_value upload_max_filesize 20M
php_value post_max_size 200M
php_value magic_quotes_gpc 0
php_flag short_open_tag on
# language
php_value mbstring.detect_order         UTF-8,EUC-JP,SJIS
php_value mbstring.encoding_translation 1
php_value mbstring.language             neutral
php_value mbstring.internal_encoding    UTF-8
php_value mbstring.http_input           UTF-8
php_value mbstring.http_output          UTF-8
