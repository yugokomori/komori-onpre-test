<?php

class OnConferenceDestroyedRequestModel {
	private $confId;
	
	function __construct($request) {
		$this->confId	= $request->confId;
	}
	
	public function __get($key) {
		if (property_exists($this, $key)) {
			return $this->$key;
		}
		else {
			return null;
		}
	}
}