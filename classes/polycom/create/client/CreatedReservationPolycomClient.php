<?php
require_once(dirname(__FILE__)."/../../../../classes/polycom/create/client/CreatedClient.php");
require_once(dirname(__FILE__)."/../../../../classes/polycom/IExecuter.php");

class CreatedReservationPolycomClient extends CreatedClient implements IExecuter {
	private $reserve;
	public function __construct($request, $obj) {
		parent::__construct($request, $obj);
		
		try {
			$meeting = $this->obj->setMeetingByConfId($this->conferenceId);
			if (!$meeting) throw new Exception("meeting status invalid.");
			if ($meeting["is_reserved"]) {
				if (!$this->obj->setReservation()) throw new Exception("meeting status invalid.");
				
				//
				$this->obj->checkIndispensablePassword();
			}
			else {
				$this->obj->checkPassword();
			}
		}
		catch (Exception $e) {
			$this->reject($e);
		}
	}
	
	public function execute() {
		try {
			// 契約数確認
			$this->obj->checkContract($this->conferenceId, $this->did);
			
			// profile
			$this->obj->changeProfile($this->obj->getPolycomProfileParams($this->conferenceId, $this->participantId));
			
			// 入室許可
			$this->obj->accept($this->conferenceId, $this->participantId, $this->type);

			if (!$this->obj->hasMeetingSequence()) {
				$this->obj->resolveEnvironmentData();
					
				$this->obj->setMeetingSequence();

				// fms instance 起動
				$this->obj->bootFMSInstance($this->conferenceId, $this->did);
			}
			
			// participant 追加
			$this->obj->addPolycomParticipant($this->conferenceId, $this->did, $this->participantId, $this->port, $this->name);

			// 参加人数をFMSへ通知
			$this->obj->noticeParticipantListChanged();
			
			// mosaic
			$this->obj->mosaic($this->conferenceId, $this->participantId, $this->type);
			
			// レイアウト調整
			$this->obj->setComposition($this->conferenceId, $this->did);

			// add sidebar.
			$this->obj->callAddSidebarParticipant($this->conferenceId, $this->participantId, $this->sidebarId);
		}
		catch (Exception $e) {
//			$this->logger->warn($e->getMessage());
			$this->reject($e);
		}
	}
}
