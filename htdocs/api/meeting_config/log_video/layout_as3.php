<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("classes/N2MY_Account.class.php");
require_once("classes/AppFrame.class.php");

class API_Layout extends AppFrame
{
    function init() {
        $this->obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        $this->_name_space = md5(__FILE__);
    }

    function auth() {
        $this->checkAuth();
    }

    function default_view()
    {
    	//セッションを覆わる防ぐ
    	$this->logger2->info($this->request->get("type"),"meeting_type");
    	if($this->request->get("type") == "ondemand")
    	{
    		$this->template->assign("ondemand", $this->session->get("ondemand"));
    		$this->template->assign("display_deleteButton", false);
    		$this->session->set("log_meeting_key",$this->session->get( "ondemand_log_meeting_key"));
    		$this->session->set("log_sequence_key",$this->session->get( "ondemand_log_sequence_key"));
    		$this->session->set("log_type",$this->session->get( "ondemand_log_type"));
    	}
    	else
    	{
    		$this->template->assign("ondemand", false);
    		$this->template->assign("display_deleteButton", $this->session->get("display_deleteButton"));
    	}
        $this->logger2->info($this->session->get("ondemand"),"ondemand");
        if ($this->session->get("record_gw")) {
            $this->display("core/api/meeting_config/meetinglog/resources/layout_4to3_for_record_gw.t.xml");
        } else {
            $this->display("core/api/meeting_config/meetinglog/resources/layout_4to3.t.xml");
        }
    }
}

$main =& new API_Layout();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
