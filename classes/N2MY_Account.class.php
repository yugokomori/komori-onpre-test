<?php
require_once("lib/EZLib/EZXML/EZXML.class.php");
require_once("lib/EZLib/EZUtil/EZUrl.class.php");

require_once("classes/dbi/room.dbi.php");
require_once("classes/dbi/service.dbi.php");
require_once("classes/dbi/room_plan.dbi.php");
require_once("classes/dbi/ordered_service_option.dbi.php");
require_once("classes/mgm/dbi/service_option.dbi.php");
require_once("classes/dbi/ives_setting.dbi.php");

/**
 * アカウント情報
 */
class N2MY_Account {

    var $logger = "";
    var $dsn = null;

    private $obj_Room = null;
    private $obj_RoomPlan = null;
    private $obj_RoomSeat = null;
    private $obj_Service = null;
    private $obj_OrderedOption = null;

    /**
     * コンストラクタ
     */
    function __construct ($dsn) {
        $this->logger2 = & EZLogger2::getInstance();
        $this->dsn = $dsn;
        // DBI関連
        $this->obj_Room          = new RoomTable($this->dsn);
        $this->obj_RoomPlan      = new RoomPlanTable($this->dsn);
        $this->obj_Service       = new ServiceTable(N2MY_MDB_DSN);
        $this->obj_ServiceOption = new ServiceOptionTable(N2MY_MDB_DSN);
        $this->obj_OrderedOption = new OrderedServiceOptionTable($this->dsn);
    }

    /**
     * 部屋一覧情報を取得
     */
    function getRoomList($user_key, $service_mode = null, $avairable_intra_fms = 0, $sort = array("room_sort" => "ASC", "room_key" => "ASC"), $limit = null, $offset = 0) {
        // 部屋一覧取得
        $where = "user_key = ".$user_key.
            " AND room_status = 1";
        if($service_mode == "sales") {$where .= " AND use_sales_option = 1";}
        else {$where .= " AND use_sales_option != 1";}
        // intra未契約者及び接続不可能
//        if ( 0 === $avairable_intra_fms)
//            $where .= " AND intra_fms = ".$avairable_intra_fms;
        $room_keys = $this->obj_Room->getCol($where, "room_key", $sort, $limit, $offset);
        if (DB::isError($room_keys)) {
            $this->logger2->error($room_keys->getUserInfo());
            return false;
        }
        $room_list = array();
        if ($room_keys) {
            foreach ($room_keys as $room_key) {
                $room_list[$room_key] = $this->getRoomInfo($room_key, $avairable_intra_fms);
            }
        }
        return $room_list;
    }

    /**
     * 自分専用の部屋情報を取得
     * user_service_optionから取得後無理矢理上書き
     */
    function getOwnRoom( $member_key, $userKey, $service_mode = null  ) {
        require_once("classes/N2MY_IndividualAccount.class.php");
        $objIndividualAccount = new N2MY_IndividualAccount( $this->dsn, N2MY_MDB_DSN );
        $userOptionInfo = $objIndividualAccount->getOptionList( $userKey );
        require_once("classes/dbi/member_room_relation.dbi.php");
        $objRoomRelation = new MemberRoomRelationTable($this->dsn);
        $where = "member_key = ".$member_key;
        $relation_list = $objRoomRelation->getRowsAssoc($where, array("create_datetime" => "desc"));
        if ($relation_list) {
            foreach ($relation_list as $data) {
                $roomInfo = $this->getRoomInfo( $data["room_key"] );
                if ($userOptionInfo["desktop_share"]) {
                    $roomInfo["options"]["desktop_share"] = $userOptionInfo["desktop_share"];
                }
                if ($userOptionInfo["hdd_extension"]) {
                    $roomInfo["options"]["hdd_extension"] = $userOptionInfo["hdd_extension"];
                }
                $this->logger2->debug($service_mode);
                if ($service_mode == "sales") {
                    if ($roomInfo["room_info"]["use_sales_option"]) {
                        $room_list[$data["room_key"]] = $roomInfo;
                    }
                } else {
                    if (!$roomInfo["room_info"]["use_sales_option"]) {
                        $room_list[$data["room_key"]] = $roomInfo;
                    }
                }

            }
        }
        return $room_list;
    }

    function getFullRoomList($user_key, $member_key, $account_type, $sort = array("room_sort" => "ASC", "room_key" => "ASC"), $limit = null, $offset = 0) {
        //セールス部屋一覧取得
        require_once("classes/N2MY_IndividualAccount.class.php");
        $objIndividualAccount = new N2MY_IndividualAccount( $this->dsn, N2MY_MDB_DSN );
        $userOptionInfo = $objIndividualAccount->getOptionList( $user_key );
        require_once("classes/dbi/member_room_relation.dbi.php");
        $objRoomRelation = new MemberRoomRelationTable($this->dsn);
        $where = "member_key = ".$member_key;
        $relation_list = $objRoomRelation->getRowsAssoc($where, array("create_datetime" => "desc"));
        if ($relation_list) {
            foreach ($relation_list as $data) {
                $room_keys[] = $data["room_key"];
            }
        }
        if ($account_type == "memebr" && !$room_keys) {
            return false;
        }
        // 部屋一覧取得
        if ($account_type == "memebr" && $room_keys && $member_key) {
            $where = " room_key IN ('".join("','", $room_keys)."')";
        } else {
            $where = "user_key = ".$user_key.
                " AND room_status = 1".
                " AND use_sales_option != 1";
            if ($room_keys) {
                $where .= " OR room_key IN ('".join("','", $room_keys)."')".
                " AND use_sales_option = 1";
            }
        }
        $room_keys = $this->obj_Room->getCol($where, "room_key", $sort, $limit, $offset);
        if (DB::isError($room_keys)) {
            $this->logger2->error($room_keys->getUserInfo());
            return false;
        }
        $room_list = array();
        if ($room_keys) {
            foreach ($room_keys as $room_key) {
                $room_list[$room_key] = $this->getRoomInfo($room_key);
            }
        }
        return $room_list;
    }

    function getOwnRoomByRoomKey( $room_key, $userKey ) {
        require_once("classes/N2MY_IndividualAccount.class.php");
        $objIndividualAccount = new N2MY_IndividualAccount( $this->dsn, N2MY_MDB_DSN );
        $userOptionInfo = $objIndividualAccount->getOptionList( $userKey );
        $roomInfo = $this->getRoomInfo( $room_key );
        if ($userOptionInfo["desktop_share"]) {
            $roomInfo["options"]["desktop_share"] = $userOptionInfo["desktop_share"];
        }
        if ($userOptionInfo["hdd_extension"]) {
            $roomInfo["options"]["hdd_extension"] = $userOptionInfo["hdd_extension"];
        }
        $room_list[$room_key] = $roomInfo;
        return $room_list;
    }


    /**
     * Freeの部屋情報を取得
     */
    function getFreeRoom( $member_key, $userKey, $service_name ) {
        require_once("classes/dbi/member_room_relation.dbi.php");
        $objRoomRelation = new MemberRoomRelationTable($this->dsn);
        $where = "member_key = ".$member_key;
        $relation_list = $objRoomRelation->getRowsAssoc($where);
        if ($relation_list) {
            foreach ($relation_list as $data) {
                $roomInfo = $this->getRoomInfo( $data["room_key"] );
                if ($service_name == $roomInfo["service_info"]["service_name"]) {
                    $room_list[$data["room_key"]] = $roomInfo;
                }
            }
        }
        return $room_list;
    }

    /**
     * ユーザの部屋総件数
     */
    function getRoomCount($user_key, $service_mode = null) {
        $where = "user_key = ".$user_key.
            " AND room_status = 1";
        if($service_mode == "sales") {$where .= " AND use_sales_option = 1";}
        else {$where .= " AND use_sales_option != 1";}
        return $this->obj_Room->numRows($where);
    }

    function getOwnRoomCount( $member_key, $userKey, $service_mode = null ) {
        require_once("classes/dbi/room.dbi.php");
        $obj_Room = new RoomTable($this->dsn);
        $relate = "room.room_key = mr.room_key";
        $where = "member_key = ".$member_key;
        if($service_mode == "sales") {$where .= " AND use_sales_option = 1";}
        else {$where .= " AND use_sales_option != 1";}
        $total_count = $obj_Room->numRowsInnerJoin("member_room_relation mr",$relate ,$where);
        return $total_count;
    }

    /**
     * 契約オプション取得
     */
    function getRoomOptionList($room_key, $date = null) {
        $row = $this->obj_OrderedOption->getServiceInfo($room_key);
        $_service_list = $this->obj_ServiceOption->getRowsAssoc("", array(), null, null, "service_option_key,service_option_name");
        foreach($_service_list as $service) {
            $service_list[$service["service_option_key"]] = $service["service_option_name"];
        }
        $option["meeting_ssl"] = 0;
        $option["meeting_tls"] = 0;
        $option["desktop_share"] = 0;
        $option["high_quality"] = 0;
        $option["mobile_phone_number"] = 0;
        $option["h323_number"] = 0;
        $option["hdd_extention"] = 0;
        $option["intra_fms"] = 0;
        $option["whiteboard"] = 0;
        $option["multicamera"] = 0;
        $option["telephone"] = 0;
        $option["smartphone"] = 0;
        $option["record_gw"] = 0;
        $option["whiteboard_video"] = 0;
        $option["teleconference"] = 0;
        $option["video_conference"] = 0;
        $option["minimumBandwidth80"] = 0;
        $option["h264"] = 0;
        $option["ChinaFastLine"] = 0;
        $option["ChinaFastLine2"] = 0;
        $option["GlobalLink"] = 0;
        foreach ($row as $value) {
            if ($service_list[$value] == "meeting_ssl") {
                $option["meeting_ssl"] = 1;
            }
            elseif ($service_list[$value] == "meeting_tls") {
                $option["meeting_tls"] = 1;
            }
            elseif ($service_list[$value] == "desktop_share") {
                $option["desktop_share"] = 1;
            }
            elseif ($service_list[$value] == "high_quality") {
                $option["high_quality"]++;
            }
            elseif ($service_list[$value] == "whiteboard") {
                $option["whiteboard"] = 1;
            }
            elseif ($service_list[$value] == "multicamera") {
                $option["multicamera"] = 1;
            }
            elseif ($service_list[$value] == "telephone") {
                $option["telephone"] = 1;
            }
            elseif ($service_list[$value] == "smartphone") {
                $option["smartphone"] = 1;
            }
            elseif ($service_list[$value] == "record_gw") {
                $option["record_gw"] = 1;
            }
            elseif ($service_list[$value] == "whiteboard_video") {
                $option["whiteboard_video"] = 1;
            }
            elseif ($service_list[$value] == "mobile_phone") {
                $option["mobile_phone_number"]++;
            }
            elseif ($service_list[$value] == "h323_client") {
                $option["h323_number"]++;
            }
            elseif ($service_list[$value] == "hdd_extention") {
                $option["hdd_extention"]++;
            }
            elseif ($service_list[$value] == "intra_fms") {
                $option["intra_fms"]++;
            }
            elseif ($service_list[$value] == "teleconference") {
                $option["teleconference"] = 1;
            }
            elseif ($service_list[$value] == "video_conference") {
                $option["video_conference"] = 1;
            }
            elseif ($service_list[$value] == "minimumBandwidth80") {
                $option["minimumBandwidth80"] = 1;
            }
            elseif ($service_list[$value] == "h264") {
                $option["h264"] = 1;
            }
            elseif ($service_list[$value] == "ChinaFastLine") {
                $option["ChinaFastLine"] = 1;
            }
            elseif ($service_list[$value] == "ChinaFastLine2") {
                $option["ChinaFastLine2"] = 1;
            }
            elseif ($service_list[$value] == "GlobalLink") {
                $option["GlobalLink"] = 1;
            }
        }
        if($option["video_conference"]) {
          $objIvesSetting = new IvesSettingTable($this->dsn);
          $where = "room_key='$room_key' and is_deleted=0";
          $option["video_conference_number"] =$objIvesSetting->getOne($where, "num_profiles") ? $objIvesSetting->getOne($where, "num_profiles"):0;
        } else
          $option["video_conference_number"] = 0;
        if($option["h264"]) {
          $where = "room_key='$room_key'";
          $option["h264_use_flg"] = $this->obj_Room->getOne($where, "default_h264_use_flg");
        }
        return $option;
    }

    function getDisplayOptionList($room_key){
        // 表示するようのオプション以外ないことにする
        $option = $this->getRoomOptionList($room_key);
        $option["meeting_tls"] = 0;
        $option["mobile_phone_number"] = 0;
        $option["hdd_extention"] = 0;
        $option["intra_fms"] = 0;
        $option["whiteboard"] = 0;
        $option["smartphone"] = 0;
        $option["record_gw"] = 0;
        $option["whiteboard_video"] = 0;
        $option["minimumBandwidth80"] = 0;
        $option["ChinaFastLine2"] = 0;

        return $option;
    }

    /**
     * サービス一覧
     */
    function getRoomInfo($room_key, $avairable_intra_fms = 0) {
        // 部屋情報
        $service_info["room_info"] = $this->obj_Room->getRow( sprintf( "room_key='%s'", $room_key ) );

        // オプション情報取得
        $service_info["options"] = $this->getRoomOptionList($room_key);

        //intrafms利用部屋で尚かつ接続不可能の場合
        if (isset($service_info["options"]["intra_fms"]) && "1" == $service_info["options"]["intra_fms"] && "0" == $avairable_intra_fms) {
            $service_info["displayRoom"] = 1;
        } else {
            $service_info["displayRoom"] = 0;
        }
        // プラン
        $room_plan = $this->obj_RoomPlan->getDetail($room_key);
        $plan_info = $this->_getServiceInfo($room_plan["service_key"]);

        // トライアルモード
        $service_info["service_info"] = $plan_info;
        if ($plan_info["service_name"] == "Trial") {
            $service_info["service_info"]["trial"] = 1;
        } else {
            $service_info["service_info"]["trial"] = 0;
        }
        return $service_info;
    }

    /**
     * サービス取得
     */
    function _getServiceInfo($service_key) {
        static $service_list = null;
        if (!isset($service_list)) {
            $rs = $this->obj_Service->select();
            if (DB::isError($rs)) {
                $this->logger2->error($rs->getUserInfo());
                return false;
            } else {
                while ($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
                    $service_list[$row["service_key"]] = $row;
                }
            }
        }
        if (isset($service_list[$service_key])) {
            return $service_list[$service_key];
        } else {
//            $this->logger2->error($service_key);
            return false;
        }
    }

    public function checkRemoteAddress($user_key, $ip=null)
    {
        require_once("classes/mgm/dbi/ip_whitelist.dbi.php");
        require_once "lib/pear/Net/IPv4.php";
        $ipv4 = new Net_IPv4();
        $objIpList = new IpWhiteListTable(N2MY_MDB_DSN);
        $ipList = $objIpList->getRowsAssoc(
                                sprintf("user_key='%s' AND status=1", $user_key),
                                null,
                                null,
                                null,
                                "ip"
                                );
        if (count($ipList) < 1) {
            return true;
        } else if($ip == null) {
            $this->logger2->error(array($user_key, $ip));
            return false;
        } else {
            foreach($ipList as $key => $data) {
                // IP
                if ($ipv4->validateIP($data["ip"])) {
                    if ($data["ip"] == $_SERVER["REMOTE_ADDR"]) return true;
                } else {
                    // ネットマスク対応
                    $net = $ipv4->parseAddress($data["ip"]);
                    if (PEAR::isError($net)) {
                        $this->logger2->warn($net->getUserInfo());
                    } else {
                        $this->logger2->info($net);
                        if ($net->netmask) {
                            if ($ipv4->ipInNetwork($_SERVER["REMOTE_ADDR"], $data["ip"])) return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    // 4.6.5.0→4.7.0.0.にバージョンアップできるかチェック
    public function checkChangeVersion($user_info){

        if($user_info["meeting_version"] == "4.7.0.0"){
          $this->logger2->info('VERSION_MISS:user_key='.$user_info["user_key"]);
          return false;
        }

        // 変更を行うサービス(プラン)リスト
        $service_list = array(42,43,44,45,46,47,50,56,57,61,62,63,65);
        // ペーパーレスプランは例外
        $paper_ress_plan = 56;
        // 変更対象外のオプション
        $no_options_list = array(8,13,14,15,18);
        // 資料共有オプションは例外
        $whiteboard_option_key = 16;
        $where = "user_key = ".$user_info["user_key"]." AND room_status = 1";
        $room_list = $this->obj_Room->getCol($where, "room_key");
        // ペーパーレスプランの部屋数
        $paper_ress_plan_count = 0;
        // 資料共有の部屋数
        $whiteboard_option_room_count = 0;
        foreach($room_list as $room_key){
            // プランを確認
            $room_plan = $this->obj_RoomPlan->getDetail($room_key);
            if(!in_array($room_plan["service_key"],$service_list)){
                $this->logger2->info('PLAN_MISS:room_key='.$room_key.' user_key='.$user_info["user_key"]);
                return false;
            }
            if($room_plan["service_key"] == $paper_ress_plan){
                //4900でペーパレスプランも切り替えOKに
                //$paper_ress_plan_count++;
            }else{
                // オプション確認
                $room_option = $this->obj_OrderedOption->getServiceInfo($room_key);
                if(in_array($whiteboard_option_key,$room_option)){
                    //4900でペーパレスプランも切り替えOKに
                    //$whiteboard_option_room_count++;
                }
                foreach($room_option as $option){
                    if(in_array($option,$no_options_list)){
                        $this->logger2->info('OPTION_MISS:room_key='.$room_key.' user_key='.$user_info["user_key"]);
                        return false;
                    }
                }
            }

        }
        if(count($room_list) == ($whiteboard_option_room_count + $paper_ress_plan_count) ){
            $this->logger2->info('ALL_WHITE_BOARD:user_key='.$user_info["user_key"]);
            return false;
        }
        return true;
    }


    // 4.6.5.0→4.7.0.0.にバージョンアップ
    public function changeVersion($user_info){

        // 更新対象か確認
        if(!$this->checkChangeVersion($user_info)){
            $this->logger2->error(array($user_info));
            return false;
        }

        // 新しいプラン
        $new_plan_list = array(
            44	=>	74,
            43	=>	75,
            42	=>	80,
            45	=>	81,
            46	=>	82,
            47	=>	83,
            50	=>	84,
            57	=>	80,
            61	=>	85,
            62	=>	86,
            63	=>	87,
            65	=>	88
        );

        // H.264オプションキー
        $H264_option_key = 26;

        // ペーパーレスプランは例外
        $paper_ress_plan = 56;
        // 資料共有オプションは例外
        $whiteboard_option_key = 16;

        $data["meeting_version"] = "4.7.0.0";
        $where = "user_key = ".$user_info['user_key'];
        require_once("classes/dbi/user.dbi.php");
        $obj_user = new UserTable($this->dsn);
        // ユーザー情報切り替え
        $obj_user->update($data, $where);

        // 部屋情報取得
        $where = "user_key = ".$user_info["user_key"]." AND room_status = 1";
        $room_list = $this->obj_Room->getRowsAssoc($where);
        // プラン一覧取得
        $where = 'service_status = 1';
        $plan_list = $this->obj_Service->getRowsAssoc($where, null, null, null, null , 'service_key');

        foreach($room_list as $room_info){

            $room_plan = $this->obj_RoomPlan->getDetail($room_info['room_key']);
            if(!$room_plan){
                $this->logger2->error('NO ROOM_PLAN:room_key='.$room_info['room_key']);
                return false;
            }
            // 変更するのはプランがペーパーレスではなく、オプションに資料共有が含まれてない部屋のみ
            $option_list = $this->obj_OrderedOption->getServiceInfo($room_info['room_key']);
            if($room_plan['service_key'] != $paper_ress_plan && !in_array($whiteboard_option_key,$option_list)){
                $this->logger2->info($room_info);

                //現在のプランを停止
                $where = "room_key='".$room_info['room_key']."' AND room_plan_status = 1";
                $data = array(
                    "room_plan_status" => 0,
                    "room_plan_updatetime" => date("Y-m-d H:i:s"),
                );
                $this->obj_RoomPlan->update($data, $where);
                // 新しいプランを追加 基本的に現状のカラムをコピーでservice_keyを変更
                $new_data = $room_plan;
                $new_plan_id = $new_plan_list[$room_plan['service_key']];
                $new_data['service_key'] = $new_plan_id;
                $new_data['room_plan_registtime'] = date("Y-m-d H:i:s");
                $new_data['room_plan_updatetime'] = date("Y-m-d H:i:s");
                unset($new_data['room_plan_key']);
                $add_plan = $this->obj_RoomPlan->add($new_data);
                if (DB::isError($add_plan)) {
                    $this->logger2->error( $add_plan->getUserInfo());
                }

                // room情報変更
                $new_plan = $plan_list[$new_plan_id];
                $room_data['max_room_bandwidth'] = $new_plan['max_room_bandwidth'];
                $room_data['max_user_bandwidth'] = $new_plan['max_user_bandwidth'];
                $room_data['min_user_bandwidth'] = $new_plan['min_user_bandwidth'];
                $room_data['default_camera_size'] = $new_plan['default_camera_size'];
                $room_data['default_h264_use_flg'] = 0;
                $room_data['room_updatetime'] = date("Y-m-d H:i:s");
                $where = "room_key = '".$room_info['room_key'] . "'";
                $update_room = $this->obj_Room->update($room_data,$where);
                if (DB::isError($update_room)) {
                    $this->logger2->error( $update_room->getUserInfo());
                }

                // H264のオプション追加
                if(!in_array($H264_option_key,$option_list)){
                    $data = array(
                        "room_key" => $room_info['room_key'],
                        "service_option_key" => $H264_option_key,
                        "ordered_service_option_status" => 1,
                        "ordered_service_option_starttime" => date("Y-m-d H:i:s"),
                        "ordered_service_option_registtime" => date("Y-m-d 00:00:00"),
                        "ordered_service_option_deletetime" => "0000-00-00 00:00:00",
                    );
                    $option_add = $this->obj_OrderedOption->add($data);
                    if (DB::isError($option_add)) {
                        $this->logger2->error( $option_add->getUserInfo());
                    }
                }
                $this->logger2->info('change_room:room_key='.$room_info['room_key']);
            }else{
                $this->logger2->info('Exception_room:room_key='.$room_info['room_key']);
            }
        }
        $this->logger2->info('changeVersion:user_key='.$user_info['user_key']);
        return true;
    }

}
