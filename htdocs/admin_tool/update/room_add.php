<?php
require_once("classes/AppFrame.class.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/dbi/room.dbi.php");
require_once("classes/dbi/member.dbi.php");
require_once("classes/dbi/member_room_relation.dbi.php");
require_once("classes/dbi/ives_setting.dbi.php");
require_once("classes/mgm/dbi/mcu_server.dbi.php");
require_once("classes/polycom/Polycom.class.php");

class AppRoomRelation extends AppFrame {

    function init () {
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $this->dsn = $this->get_dsn();
        $this->objUser = new UserTable($this->get_dsn());
        $this->objRoom = new RoomTable($this->get_dsn());
        $this->objMember = new MemberTable($this->get_dsn());
        $this->objRoomRelation = new MemberRoomRelationTable($this->get_dsn());
    }

    function default_view() {

        print $this->_header();
print <<<EOM
<form action="/admin_tool/update/room_add.php" name="search" method="post">
<input type="hidden" name="action_add_room" value="">
ID : <input type="text" name="user_id" size="30" value="">
Media Mixer Key : <input type="text" name="media_server_key" size="30" value=""><br /><br /><br />
extend flag : <input type="text" name="is_extend_room" size="30" value="0">
<input type="submit" name="submit" value="部屋追加">
</form>
EOM;
        print $this->_footer();
    }

    function action_add_room() {
        $user_id = $this->request->get("user_id");
        $media_server_key = $this->request->get("media_server_key");
        $where = "user_id = '".addslashes($user_id)."'";
        $user_info = $this->objUser->getRow($where);

        $is_extend_room = $this->request->get("is_extend_room");
        if($is_extend_room){
            $is_extend_room = 1;
        }
        $where = "user_key='".addslashes($user_info["user_key"])."'";
        $room_count = $this->objRoom->numRows($where);

        if($is_extend_room == 0){

            $this->_add_room($user_info, "120", $media_server_key);

            $this->_add_room($user_info, "121", $media_server_key);
            $this->_add_room($user_info, "122", $media_server_key);
            $this->_add_room($user_info, "123", $media_server_key);
            $this->_add_room($user_info, "124", $media_server_key);
            $this->_add_room($user_info, "125", $media_server_key);
            $this->_add_room($user_info, "126", $media_server_key);
            $this->_add_room($user_info, "127", $media_server_key);
            $this->_add_room($user_info, "128", $media_server_key);
            $this->_add_room($user_info, "129", $media_server_key);
        }
        //10,9,8,7
        for ($i = 1; $i <= 2; $i++) {
            $this->_add_room($user_info, "130", $media_server_key, $is_extend_room);
            $this->_add_room($user_info, "131", $media_server_key, $is_extend_room);
            $this->_add_room($user_info, "132", $media_server_key, $is_extend_room);
            $this->_add_room($user_info, "133", $media_server_key, $is_extend_room);
        }
        //6
        for ($i = 1; $i <= 3; $i++) {
            $this->_add_room($user_info, "134", $media_server_key, $is_extend_room);
        }
        //5
        for ($i = 1; $i <= 4; $i++) {
            $this->_add_room($user_info, "135", $media_server_key, $is_extend_room);
        }

        //4
        for ($i = 1; $i <= 5; $i++) {
            $this->_add_room($user_info, "136", $media_server_key, $is_extend_room);
        }
        //3
        for ($i = 1; $i <= 6; $i++) {
            $this->_add_room($user_info, "137", $media_server_key, $is_extend_room);
        }
        //2
        for ($i = 1; $i <= 10; $i++) {
            $this->_add_room($user_info, "138", $media_server_key , $is_extend_room);
        }

        print $this->_header();
        print "Start Room Count：".$room_count;
        $end_room_count = $this->objRoom->numRows($where);

        print "<br><br><br>";
        print "Finished Room Count：".$end_room_count;

        print $this->_footer();
    }

    function _add_room($user_info, $service_key, $media_server_key, $is_extend_room = 0) {
        $where = "user_key='".addslashes($user_info["user_key"])."'";
        $room_count = $this->objRoom->numRows($where);
        $room_key = $user_info["user_id"]."-".($room_count + 1)."-".substr(md5(uniqid(time(), true)), 0,4);
        $room_sort = $room_count + 1;
        //部屋登録
        $service_db = new N2MY_DB($this->account_dsn, "service");
        $service_info = $service_db->getRow("service_key = ".$service_key);
        $room_data = array (
            "room_key"            => $room_key,
            //"room_name"           => "room-".($room_count + 1)."-mcu".$media_server_key."-".$service_info["max_seat"],
            "room_name"           => $service_info["max_seat"]."台用 / ".$service_info["max_seat"]." Ports Room",
            "user_key"            => $user_info["user_key"],
            "room_sort"           => $room_sort,
            "room_status"         => "1",
            "use_teleconf"        => "1",
            "use_pgi_dialin"      => "1",
            "use_pgi_dialin_free" => "1",
            "use_pgi_dialout"     => "1",
            "prohibit_extend_meeting_flg" => "1",
            "rtmp_protocol"       => "rtmp:1935,rtmps:443,rtmps-tls:443",
            "is_extend_room"      => $is_extend_room,
            "room_registtime"     => date("Y-m-d H:i:s"),
            "room_expiredatetime" => "0000-00-00 00:00:00"
            );
        $add_room = $this->objRoom->add($room_data);
        if (DB::isError($add_room)) {
            $this->logger2->info( $add_room->getUserInfo());
        }
        $acc_user_db = new N2MY_DB($this->account_dsn, "user");
        $where = "user_id='".addslashes($user_info["user_id"])."'";
        $acc_user_data = $acc_user_db->getRow($where);
        $relation_db = new N2MY_DB($this->account_dsn, "relation");
        $relation_data = array(
            "relation_key" => $room_key,
            "relation_type" => "mfp",
            "user_key" => $acc_user_data["user_id"],
            "status" => "1",
            "create_datetime" => date("Y-m-d H:i:s"),
            );
        $relation_db->add($relation_data);
        $this->action_room_plan_add($room_key, $service_key);

        $ivesSettingTable = new IvesSettingTable($this->get_dsn());
        $sip_info =  array(
                "uid" => rand(10000, 99999),
                "name" => rand(1000000, 9999999),
                "secret" => $this->create_id().rand(2,9)
            );

        require_once("classes/dbi/video_conference.dbi.php");
        require_once("classes/mgm/dbi/media_mixer.dbi.php");
        $room_db	= new RoomTable($this->get_dsn());
        $media_db     = new MediaMixerTable($this->get_auth_dsn());
        $mcu_db     = new McuServerTable($this->get_auth_dsn());

        $where   	= "room_key = '".addslashes($room_key)."'";
        $room		= $room_db->getRow($where);

        $where   	= "media_mixer_key = ".addslashes($media_server_key);
        $media_mixer_info = $media_db->getRow($where);

        $mcu_host = $mcu_db->getActiveServerAddress($media_mixer_info["mcu_server_key"]);
        $polycom = new PolycomClass($this->get_dsn());
        $polycom->setWsdlDomain($mcu_host);
        $did = $polycom->createDid();
        $vad =  true;
        $media_mixer_id = $media_mixer_info["media_name"]."@".$media_mixer_info["url"];
        $this->createAdHocConference($room, $did, $vad, $mcu_host, $media_mixer_id);

        $data = array(
            'room_key'           => $room_key,
            'mcu_server_key'     => 1,
            'media_mixer_key'   => $media_server_key,
            'user_key'           => $user_info["user_key"],
            'ives_did'           => $did,
            'sip_uid'            => $sip_info["uid"],
            'client_id'          => $sip_info["name"],
            'client_pw'          => $sip_info["secret"],
            'profile_id'         => 281,
            'num_profiles'       => 1,
            'use_active_speaker' => 0,
            'default_h264_use_flg'=> 1,
            'startdate'          => date("Y-m-d H:i:s"),
            'registtime'         => date("Y-m-d H:i:s")
        );
        $res = $ivesSettingTable->add($data);
        require_once("classes/dbi/ordered_service_option.dbi.php");
        $ordered_service_option = new OrderedServiceOptionTable($this->get_dsn());
        $data = array(
            "room_key" => $room_key,
            "service_option_key" => "24",
            "ordered_service_option_status" => 1,
            "ordered_service_option_starttime" => date("Y-m-d 00:00:00"),
            "ordered_service_option_registtime" => date("Y-m-d 00:00:00"),
            "ordered_service_option_deletetime" => "0000-00-00 00:00:00",
            );
       $ordered_service_option->add($data);
    }

     /**
     * ルームプラン登録（プランに合わせて人数、オプションも登録）
     */
    function action_room_plan_add($room_key, $service_key) {
        $service_key = $service_key;
        $start_time = date("Y-m-d H:i:s");
        $end_time = "0000-00-00 00:00:00";
        $discount_rate = "0";
        $contract_month_number = "0";
        $today = date("Y-m-d 23:59:59");
        $this->logger2->info($service_key);
        if (strtotime($start_time) <= strtotime($today)) {
            //プラン登録
            $room_plan_db = new N2MY_DB($this->get_dsn(), "room_plan");
            $room_plan = array (
                "room_key" => $room_key,
                "service_key" => $service_key,
                "room_plan_status" => "1",
                "room_plan_starttime" => $start_time,
                "room_plan_endtime" => $end_time,
                "discount_rate" => $discount_rate,
                "contract_month_number" => $contract_month_number,
                "room_plan_registtime" => date("Y-m-d H:i:s"),
                );
            $add_plan = $room_plan_db->add($room_plan);
            if (DB::isError($add_plan)) {
                $this->logger2->info( $add_plan->getUserInfo());
            }
            $service_db = new N2MY_DB($this->account_dsn, "service");
            $service_info = $service_db->getRow("service_key = ".$service_key);
            $this->logger2->info($service_key);

            //シート登録
            $room_db = new N2MY_DB($this->get_dsn(), "room");
            //オーディエンスオプション数で人数変更
            require_once("classes/dbi/ordered_service_option.dbi.php");
            $ordered_service_option = new OrderedServiceOptionTable($this->get_dsn());
            if ($service_info["max_audience_seat"] == "0") {
                $where = "room_key = '".addslashes($room_key)."'".
                         " AND service_option_key = 7".
                         " AND ordered_service_option_status = 1";
                $audience_count = $ordered_service_option->numRows($where);
                $this->logger->trace("audience",__FILE__,__LINE__,$audience_count);
                if ($audience_count > 0 ) {
                    $max_audience = $audience_count."0";
                } else {
                    $max_audience = $service_info["max_audience_seat"];
                }
            } else {
                $max_audience = $service_info["max_audience_seat"];
            }
            $max_whiteboard = $service_info["max_whiteboard_seat"];
            $where = "room_key = '".addslashes($room_key)."'";
            $room_seat = array (
                "max_seat" => $service_info["max_seat"],
                "max_audience_seat" => $max_audience,
                "max_whiteboard_seat" => $max_whiteboard,
                "max_guest_seat"       => ($service_info["max_guest_seat_flg"]) ? $service_info["max_guest_seat"] : 0,
                "max_guest_seat_flg"       => ($service_info["max_guest_seat_flg"]) ? 1 : 0,
                "extend_seat_flg"       => ($service_info["extend_seat_flg"]) ? 1 : 0,
                "extend_max_seat"  => ($service_info["extend_seat_flg"]) ? $service_info["extend_max_seat"] : 0,
                "extend_max_audience_seat" => ($service_info["extend_seat_flg"]) ? $service_info["extend_max_audience_seat"] : 0,
                "meeting_limit_time" => $service_info["meeting_limit_time"],
                "ignore_staff_reenter_alert"    => ($service_info["ignore_staff_reenter_alert"]) ? $service_info["ignore_staff_reenter_alert"] : 0 ,
                "default_camera_size" => $service_info["default_camera_size"],
                "hd_flg"              => $service_info["hd_flg"],
                "disable_rec_flg"       => $service_info["disable_rec_flg"],
                "room_updatetime" => date("Y-m-d H:i:s"),
                );
            if ($service_info["max_room_bandwidth"] != 0) {
                $room_seat["max_room_bandwidth"] = $service_info["max_room_bandwidth"];
            }
            if ($service_info["max_user_bandwidth"] != 0) {
                $room_seat["max_user_bandwidth"] = $service_info["max_user_bandwidth"];
            }
            if ($service_info["min_user_bandwidth"] != 0) {
                $room_seat["min_user_bandwidth"] = $service_info["min_user_bandwidth"];
            }
            if ($service_info["service_name"] == "Paperless_Free" || $service_info["service_name"] == "Meeting_Free" || $service_info["service_name"] == "PGi_Paperless_Free") {
                $room_seat["whiteboard_page"] = "10";
                $room_seat["whiteboard_size"] = "2";
                $whiteboard_filetype = array(
                    "document" => array("ppt","pdf"),
                    "image"    => array("jpg"),
                );
                $room_seat["whiteboard_filetype"] =serialize($whiteboard_filetype);
            } else {
                $room_seat["whiteboard_page"] = $service_info["whiteboard_page"] ? $service_info["whiteboard_page"] : 100;
                $room_seat["whiteboard_size"] = $service_info["whiteboard_size"] ? $service_info["whiteboard_size"] : 20;
                $room_seat["cabinet_size"] = $service_info["cabinet_size"] ? $service_info["cabinet_size"] : 20;
                if ($service_info["document_filetype"]) {
                    $whiteboard_filetype["document"] = explode(",", $service_info["document_filetype"]);
                }
                if ($service_info["image_filetype"]) {
                    $whiteboard_filetype["image"] = explode(",", $service_info["image_filetype"]);
                }
                if ($service_info["cabinet_filetype"]) {
                    $cabinet_filetype = explode(",", $service_info["cabinet_filetype"]);
                }
                $room_seat["cabinet_filetype"] =serialize($cabinet_filetype);
            }
            if ($service_info["service_name"] == "VCUBE_Doctor_Standard") {
                $room_seat["invited_limit_time"] = $service_info["invited_limit_time"] ? $service_info["invited_limit_time"] : 30;
            }
            $add_seat = $room_db->update($room_seat, $where);
            //プランのオプション情報取得
            $add_options = array();
            if ($service_info["meeting_ssl"] == 1) {
                $add_options[] = "2";
            }
            if ($service_info["desktop_share"] == 1) {
                $add_options[] = "3";
            }
            if ($service_info["high_quality"] == 1) {
                $add_options[] = "5";
            }
            if ($service_info["mobile_phone"] > 0) {
                $add_options[] = "6";
            }
            if ($service_info["h323_client"] > 0) {
                $add_options[] = "8";
            }
            //hdd_extentionは数値分登録
            if ($service_info["hdd_extention"] > 0) {
                $add_options[] = "4";
            }
            // ホワイトボードプラン
            if ($service_info["whiteboard"] == 1) {
                $add_options[] = "16";
            }
            // マルチカメラ
            if ($service_info["multicamera"] == 1) {
                $add_options[] = "18";
            }
            // 電話連携
            if ($service_info["telephone"] == 1) {
                $add_options[] = "19";
            }
            // 録画GW
            if ($service_info["record_gw"] == 1) {
                $add_options[] = "20";
            }
            // スマートフォン
            if ($service_info["smartphone"] == 1) {
                $add_options[] = "21";
            }
            // 資料共有映像再生許可
            if ($service_info["whiteboard_video"] == 1) {
                $add_options[] = "22";
            }
            // PGi連携
            if ($service_info["teleconference"] == 1) {
                $add_options[] = "23";
            }
            // 最低帯域アップ
            if ($service_info["video_conference"] == 1) {
                $add_options[] = "24";
            }
            // 最低帯域アップ
            if ($service_info["minimumBandwidth80"] == 1) {
                $add_options[] = "25";
            }
            // h264
            if ($service_info["h264"] == 1) {
                $add_options[] = "26";
            }
            // global_link
            if ($service_info["global_link"] == 1) {
                $add_options[] = "30";
            }
            //オプション登録
            if ($add_options) {
                foreach ($add_options as $value) {
                    $where = "room_key = '".addslashes($room_key)."'".
                             " AND service_option_key = ".$value.
                             " AND ordered_service_option_status = 1";
                    $count = $ordered_service_option->numRows($where);
                    $this->logger2->debug($count);
                    if ($count == 0 || "4" == $value || "6" == $value || "8" == $value) {
                        if ("4" == $value) {
                            $this->logger2->info($value);
                            for ($num = 1; $num <= $service_info["hdd_extention"]; $num++ ) {
                                $this->action_room_option_add($room_key, $value, null);
                            }
                        } else if ("6" == $value) {
                            $this->logger2->info($value);
                            for ($num = 1; $num <= $service_info["mobile_phone"]; $num++ ) {
                                $this->action_room_option_add($room_key, $value, null);
                            }
                        } else if ("8" == $value) {
                            $this->logger2->info($value);
                            for ($num = 1; $num <= $service_info["h323_client"]; $num++ ) {
                                $this->action_room_option_add($room_key, $value, null);
                            }
                        }else if ("23" == $value){ // teleconference for pgi
                            $this->logger2->info($value);
                            // TODO PGI 別の場所から設定したい
                            $pgi_form = $this->_getDefaultPGI();
                            $this->action_pgi_setting($pgi_form,$room_key,1);
                            $this->action_room_option_add($room_key, $value, null);
                        } else {
                            $this->action_room_option_add($room_key, $value, null);
                        }
                    }
                }
                if (DB::isError($add_seat)) {
                    $this->logger2->info( $add_seat->getUserInfo());
                }
            }
            //プレミアムプランへの変更時はオーディエンスオプションを停止
            if ($service_info["max_audience"] != 0) {
                $where = "room_key = '".addslashes($room_key)."'".
                             " AND service_option_key = 7".
                             " AND ordered_service_option_status = 1";
                $data = array (
                    "ordered_service_option_status" => "0"
                );
                $audience_option = $ordered_service_option->update($data, $where);
            }
            //自動停止日の設定があったら、expire_deteを更新する
            if ($service_info["service_expire_day"] > 0) {
                $where = "room_key = '".addslashes($room_key)."'";
                $room_info = $room_db->getRow($where, "user_key");
                $this->logger2->info(array($service_info,$room_info));
                if ($room_info["user_key"]) {
                    $where = "user_key = ".addslashes($room_info["user_key"]);
                    $expire_date = strtotime($start_time);
                    $expire_date = date( "Y-m-d 00:00:00" , strtotime( "+".$service_info["service_expire_day"]." day" , $expire_date) );
                    $data = array("user_expiredatetime" => $expire_date);
                    $objUser = new N2MY_DB($this->get_dsn(), "user");
                    $updateUser = $objUser->update($data, $where);
                    if (DB::isError($updateUser)) {
                        $this->logger2->info( $updateUser->getUserInfo());
                    }
                }
            }
            //ストレージ容量付与
            $add_storage_size = $service_info["add_storage_size"];
            // ユーザー情報取得
            $room_db =  new N2MY_DB($this->get_dsn(), "room");
            $room_info = $room_db->getRow("room_key= '" . $room_key . "'");
            $user_key = $room_info["user_key"];
            $where = "user_key='".addslashes($user_key)."'";
            $user_db = new N2MY_DB($this->get_dsn(), "user");
            $user_info = $user_db->getRow($where);
            $new_storage_size = $user_info["max_storage_size"] + $add_storage_size;
            if($new_storage_size < 0){
                $new_storage_size = 0;
            }

            if($new_storage_size != $user_info["max_storage_size"]){
                $data = array(
                        "max_storage_size" => $new_storage_size
                );
                $user_db->update($data, $where);
            }
        } else {
            //プラン登録
            $room_plan_db = new N2MY_DB($this->get_dsn(), "room_plan");
                $room_plan = array (
                "room_key" => $room_key,
                "service_key" => $service_key,
                "room_plan_status" => "2",
                "room_plan_starttime" => $start_time,
                "room_plan_endtime" => $end_time,
                "discount_rate" => $discount_rate,
                "contract_month_number" => $contract_month_number,
                "room_plan_registtime" => date("Y-m-d H:i:s"),
            );
            $add_plan = $room_plan_db->add($room_plan);
            if (DB::isError($add_plan)) {
                $this->logger2->info( $add_plan->getUserInfo());
            }
            $service_db = new N2MY_DB($this->account_dsn, "service");
            $service_info = $service_db->getRow("service_key = ".$service_key);
            if($service_info["teleconference"] == 1){
                $pgi_form = $this->_getDefaultPGI();
                $pgi_form["pgi_start_date"] =substr($start_time ,0 ,7);
                $this->action_pgi_setting($pgi_form,$room_key,1);
            }
        }
        return true;
    }


    //オプション登録
    function action_room_option_add($room_key, $service_option_key, $option_start_time) {
        require_once("classes/dbi/ordered_service_option.dbi.php");
        $ordered_service_option = new OrderedServiceOptionTable($this->get_dsn());
        $this->logger->trace("start_time",__FILE__,__LINE__,$option_start_time);
        if (!$option_start_time) {
            $option_start_time = date("Y-m-d H:i:s");
        }

        if ($service_option_key == 23) {// teleconference
            require_once("classes/dbi/pgi_setting.dbi.php");
            $pgi_setting_table = new PGiSettingTable($this->get_dsn());

            $month_first_day = substr($option_start_time ,0 ,7);
            $pgi_setting = $pgi_setting_table->findEnablleAtYM($room_key, $month_first_day);
            if (count($pgi_setting) <= 0) {
                 $this->action_room_detail('', 'If wants to enable teleconference option, the PGi setting is needed from '.$month_first_day.'.');
                 exit;
            }
        }

        $today = date("Y-m-d 23:59:59");
        if (strtotime($option_start_time) >= strtotime($today))  {
            $status = "2";
        } else {
            $status = "1";
        }
        $data = array(
            "room_key" => $room_key,
            "service_option_key" => $service_option_key,
            "ordered_service_option_status" => $status,
            "ordered_service_option_starttime" => $option_start_time,
            "ordered_service_option_registtime" => date("Y-m-d 00:00:00"),
            "ordered_service_option_deletetime" => "0000-00-00 00:00:00",
            );
        //オーディエンスオプションの際は部屋の人数変更
        if ($service_option_key == 7) {
            $where = "room_key = '".addslashes($room_key)."'".
                     " AND room_plan_status = 1";
            $room_plan_db = new N2MY_DB($this->get_dsn(), "room_plan");
            $room_plan = $room_plan_db->getRow($where);
            if ($room_plan["service_key"] == "45" || $room_plan["service_key"] == "46" || $room_plan["service_key"] == "47") {
                $this->logger2->error("not add audience");
            } else if (!$room_plan) {
                //プランが無かった場合は予約があるか確認
                $where = "room_key = '".addslashes($room_key)."'".
                     " AND room_plan_status = 2";
                $room_plan_reserve = $room_plan_db->getRow($where);
                if ($room_plan_reserve) {
                    $ordered_service_option->add($data);
                }
            } else {
                $ordered_service_option->add($data);
                $where_coount = "room_key = '".addslashes($room_key)."'".
                     " AND service_option_key = ".addslashes($service_option_key).
                     " AND ordered_service_option_status = 1";
                $audience_count = $ordered_service_option->numRows($where_coount);
                if ($audience_count > 0) {
                    $seat_data["max_seat"] = "9";
                    $audience_seat = $audience_count."0";
                    $this->logger2->info($audience_seat);
                } else {
                    $seat_data["max_seat"] = "10";
                    $audience_seat = "0";
                }
                $seat_data["max_audience_seat"] = $audience_seat;
                $where_room = "room_key = '".addslashes($room_key)."'";
                $room_db = new N2MY_DB($this->get_dsn(), "room");
                $room_data = $room_db->update($seat_data, $where_room);
            }
        } else {
            $ordered_service_option->add($data);
            if ($service_option_key == 23) {// teleconference
                $room_db       = new N2MY_DB($this->get_dsn(), "room");
                $teleconf_data = array('use_teleconf'=>1, 'use_pgi_dialin'=>1, 'use_pgi_dialin_free'=>1, 'use_pgi_dialout'=>1);
                $room_db->update($teleconf_data , "room_key = '".addslashes($room_key)."'");
            }
        }
        // スマートフォン連携時に電話連携がなければ自動でセットする
        if ($service_option_key == 21) {
            $where = "room_key = '".addslashes($room_key)."'".
                 " AND service_option_key = 19".
                 " AND ordered_service_option_status = 1";
            $count = $ordered_service_option->numRows($where);
            if ($count == 0) {
                $data2 = $data;
                $data2["service_option_key"] = 19;
                $ordered_service_option->add($data2);
            }
        }
        return true;
    }



    /**
     * 乱数ID発行
     */
    function create_id() {
        list($a, $b) = explode(" ", microtime());
        $a = (int)($a * 100000);
        $a = strrev(str_pad($a, 5, "0", STR_PAD_LEFT));
        $c = $a.$b;
        // 36
        //print (base_convert($c, 10, 36));
        // 62
        return $this->dec2any($c);
    }

    /**
     * 数値から62進数のID発行
     */
    function dec2any( $num, $base=57, $index=false ) {
        if (! $base ) {
            $base = strlen( $index );
        } else if (! $index ) {
            $index = substr(
                    '23456789ab' .
                    'cdefghijkm' .
                    'nopqrstuvw' .
                    'xyzABCDEFG' .
                    'HJKLMNPQRS' .
                    'TUVWXYZ' ,0 ,$base );
        }
        $out = "";
        for ( $t = floor( log10( $num ) / log10( $base ) ); $t >= 0; $t-- ) {
            $a = floor( $num / pow( $base, $t ) );
            $out = $out . substr( $index, $a, 1 );
            $num = $num - ( $a * pow( $base, $t ) );
        }
        return $out;
    }


/*
    private function registerConenferenceManager() {
      $ivesClient = new N2MY_IvesClient();
      /**
       * 確認項目
       * ・既に登録済か確認できるApiｎ有無
       * ・すでに登録されているuriを再度登録してもんだいないか
       *
      $ivesClient->registerConferenceMngrListener();
    }
*/
    private function createAdHocConference($room, $did, $vad, $mcu_host, $media_mixer_id) {
      try {
        require_once("classes/polycom/Polycom.class.php");
          $polycom = new PolycomClass($this->get_dsn());
          $polycom->setWsdlDomain($mcu_host);
          $polycom->createAdhocConferenceTemplate($did, $vad, $media_mixer_id);
      }
      catch (Exception $e) {
            $this->logger2->error($e);
            return false;
      }
    }
/*
    private function creteVideoOption($room, $did) {
        require_once("classes/polycom/Polycom.class.php");
        require_once("classes/dbi/video_conference.dbi.php");
        $polycom	= new PolycomClass($this->get_dsn());
        $ivesClient = new N2MY_IvesClient();
        $video		= new VideoConferenceTable($this->get_dsn());
        $where		= "room_key = '".addslashes($room["room_key"])."'".
            " AND is_active = 1";
        $videoInfo	= $video->getRow($where);
        // remove conference
        if ($videoInfo) {
      $conf = $ivesClient->callGetConference($videoInfo["conference_id"]);
      if ($conf->return) {
        $polycom->removeConference($videoInfo["conference_id"]);
      }
    }
    else {
      $conference = $polycom->checkExistDidInConferences($did);
      if ($conference) {
        $polycom->removeConference($conference->id);
      }
    }
        // create conference
        $confId = $polycom->createConference($did);
        //
        $video->removeAllByRoomKey($room["room_key"]);
    $data = array(
      "user_key"		=> $room["user_key"],
      "room_key"		=> $room["room_key"],
      "conference_id"	=> $confId,
      "did"			=> $did,
      "is_active"		=> 1,
      "createtime"	=> date("Y-m-d H:i:s"));
    $video->add($data);
  }
*/
    //}}}
      //{{{action_remove_conferen ce
    private function action_remove_conference($roomKey)
    {
        try {
            require_once("classes/polycom/Polycom.class.php");
            require_once("classes/dbi/video_conference.dbi.php");
            $dsn = $this->get_dsn();
            $ivesSettingTable	= new IvesSettingTable($dsn);
            $video				= new VideoConferenceTable($dsn);
            $mcu_db             = new McuServerTable($this->get_auth_dsn());

            $res = $ivesSettingTable->findByRoomKey($roomKey);
            if (PEAR::isError($res) || empty($res[0]["ives_did"])) {
                throw new Exception("select ives_setting room_key failed PEAR said : ".$roomKey);
            }

            $ivesSettingList = $res[0];
            $mcu_server_key = $ivesSettingList["mcu_server_key"];
            $did            = $ivesSettingList["ives_did"];
            $confId         = $ivesSettingList["ives_conference_id"];

            $mcu_host = $mcu_db->getActiveServerAddress($mcu_server_key);
            if(!$mcu_host) {
                $msg = "No MCU server available where key is '" . $mcu_server_key . "'.";
                $this->logger2->warn($msg);
                throw new Exception($msg);
            }
            $polycom = new PolycomClass($dsn);
            $polycom->setWsdlDomain($mcu_host);

            // remove adhocConference template.
            $polycom->removeAdhocConferenceTemplate($did);
            // remove IVes setting record.
            $where = "room_key = '".$roomKey."'";
            $data  = array(
                "is_deleted" => 1
            );
            $res = $ivesSettingTable->update($data, $where);

            if (PEAR::isError($res)) {
                throw new Exception("ives setting update room failed : ".$roomKey);
            }

            // remove
            $where = "room_key = '".$roomKey."'".
                        " AND did = '".$did."'".
                        " AND is_active = 1";
            $videoInfo = $video->getRow($where);

            // remove from video table.
            $video->removeAllByRoomKey($roomKey);
            //sip account 削除
            $this->action_delete_sip_account($ivesSettingList["sip_uid"]);

        } catch (SoapFault $e) {
            $this->logger->warn(__FUNCTION__."#soap fault",__FILE__,__LINE__);
            $this->action_room_detail('', '', '', 'Failed to delete a MCU Conference.');
            exit;
        } catch (Exception $e) {
            $this->logger->warn(__FUNCTION__."#exception",__FILE__,__LINE__);
            $this->action_room_detail('', '', '', 'Failed to delete a MCU Conference.');
            exit;
        }

        //操作ログ登録
        $operation_data = array (
            "staff_key"          => $_SESSION["_authsession"]["data"]["staff_key"],
            "action_name"        => __FUNCTION__,
            "table_name"         => 'ives_setting',
            "keyword"            => $roomKey,
            "info"               => serialize($data),
            "operation_datetime" => date("Y-m-d H:i:s"),
        );
        $this->add_operation_log($operation_data);
    }
    //}}}

    //{{{findMeeting
    private function findMeeting($meetingKey)
    {
        $this->logger2->info("-------findMeeting--------" . $meetingKey);

        $objMeeting = new DBI_Meeting($this->get_dsn());
        $where      = "meeting_ticket='".mysql_real_escape_string($meetingKey)."'";

        return $objMeeting->getRow($where);
    }

    //}}}
    //{{{ updateSetting
    private function updateSetting($ivesSettingKey, $data)
    {
        $setting  = new IvesSettingTable($this->get_dsn());
        $where = sprintf("ives_setting_key = '%s' and client_id = ''",
                         mysql_real_escape_string($ivesSettingKey));

        $res = $setting->update($data, $where);
        if (PEAR::isError($res)) {
            throw new Exception("update room failed PEAR said : ".$res->getUserInfo());
        }
    }

function _header() {
        return <<<EOM
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Room Relation Key Add</title>
<style TYPE="text/css">
<!--
body {
text-align: center;
font-size: 12px;
font-family: "Arial, sans-serif, ＭＳ Ｐゴシック", Osaka, "ヒラギノ角ゴ Pro W3" ;
margin: 10px 0px 0px 0px;
}
.title {
font-size: 16px;
font-weight: bold;
margin: 20px auto 0px auto;
width: 760px
}
#content {
text-align: center;
}
#content table {
border-collapse: collapse;
border-spacing: 0;
empty-cells: show;
border: 1px solid #999999;
margin: 20px auto;
width: 760px
}
#content th {
border: 1px solid #999999;
padding: 4px 5px;
background-color: #EFEFEF;
text-align: left;
}
#content td {
border: 1px solid #999999;
padding: 4px 10px;
text-align: left;
}
.errorSection {
color: #FF0000;
}
-->
</style>
</head>
<body>
<div id="content">
<h1>Room Add</h1>
EOM;
    }

    function _footer() {
       print "</body></html>";
    }

}
$main =& new AppRoomRelation();
$main->execute();
