#!/usr/bin/env python
# -*- coding: utf-8 -*-

import ConfigParser

from suds.client import Client

def soapRequest(wsdl, mngr):
	ret = 0

	try:
		client = Client(wsdl)
		ret = client.service.registerConferenceMngrListener(mngr);
	except:
		print "soapRequest Error!"
	#finally:
	return	ret

if __name__ == "__main__":
	inifile = ConfigParser.SafeConfigParser()
	try:
		inifile.read("/data/services/vcube_mcu/config.ini")
		wsdl = inifile.get("mcu", "wsdl")
		mngr = inifile.get("mcu", "mngr")
	except:
		print "設定ファイル取得失敗"

	print "isRegisterConferenceManager : " + str(soapRequest(wsdl, mngr));

