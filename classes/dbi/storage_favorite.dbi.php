<?php

require_once("classes/N2MY_DBI.class.php");

class StorageFavoriteTable extends N2MY_DB {

    var $table = 'storage_favorite_file';
    var $logger = null;

    /**
     * DB接続
     */
    function StorageFavoriteTable($dsn) {
        $this->init($dsn, "storage_favorite_file");
    }

    /**
     * 対象キーの共有資料を取得
     */
    function getStorageKey($where) {
        return $this->getOne($where, "");
    }

}