<?php

require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_DBI.class.php");
require_once("classes/N2MY_IndividualAccount.class.php");
require_once( "classes/dbi/member_usage_details.dbi.php" );
require_once("classes/dbi/user_service_option.dbi.php");

class AppMemberLog extends AppFrame
{
    function init()
    {
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
    }

    public function default_view()
    {
        $this->action_top();
    }
/*
    private function action_top2()
    {
        $request = $this->request->getAll();
        if( $request["year"] && $request["month"] ){
            $year	= $request["year"];
            $month	= $request["month"];
        } else {
            $lastdate = strtotime( "last Month" );
            $year = date( "Y", $lastdate );
            $month = date( "m", $lastdate );
        }
        $now = sprintf( "%04d-%02d", $year, $month );
        $firstDay = date( "Y-m-d H:i:s", mktime( 0, 0, 0, $month, 1, $year ) );
        $lastDay = date( "Y-m-d H:i:s", mktime( 23, 59, 59, $month + 1, 0, $year ) );
        $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        foreach( $server_list["SERVER_LIST"] as $key => $dsn ){
            $objMeetingdb	 = new N2MY_DB( $dsn );
            $objMGMdb		 = new N2MY_DB( $this->account_dsn );

            //メンバー課金ユーザー毎のユーザー情報（会社情報）と部屋情報、会議情報を取得
            $sql = "
SELECT
    u.user_id,
    u.user_key,
    u.max_rec_size,
    u.user_company_name,
    u.user_company_postnumber,
    u.user_company_address,
    u.user_company_phone,
    u.user_company_fax,
    u.user_starttime,
    u.user_registtime,
    u.user_updatetime,
    u.user_deletetime,
    u.service_key,
    u.user_plan_yearly,
    u.user_plan_yearly_starttime,
    u.user_plan_starttime,
    u.room_name,
    m.meeting_key,
    m.room_key,
    m.meeting_room_name,
    m.meeting_max_seat,
    m.meeting_size_used,
    m.meeting_log_owner,
    IF( DATE_FORMAT( m.meeting_start_datetime, '%Y-%m' ) = '" . $now . "' || m.meeting_start_datetime IS null, m.meeting_start_datetime, '" . $firstDay . "' ) AS meeting_start_datetime,
    IF( DATE_FORMAT( m.meeting_stop_datetime, '%Y-%m' ) = '" . $now . "' || m.meeting_stop_datetime IS null, m.meeting_stop_datetime, '" . $lastDay . "' ) AS meeting_stop_datetime, m.meeting_stop_datetime,
    m.meeting_extended_counter,
    m.has_recorded_minutes,
    m.has_recorded_video,
    m.is_active,
    m.is_reserved,
    m.create_datetime,
    m.update_datetime,
    m.use_flg,
    IF( DATE_FORMAT( m.actual_start_datetime, '%Y-%m' ) = '" . $now . "' || m.actual_start_datetime IS null, m.actual_start_datetime, '" . $firstDay . "' ) AS actual_start_datetime,
    IF( DATE_FORMAT( m.actual_end_datetime, '%Y-%m' ) = '" . $now . "' || m.actual_end_datetime IS null, m.actual_end_datetime, '" . $lastDay . "' ) AS actual_end_datetime,
    m.meeting_use_minute
FROM
(
    SELECT
        u.*, r.room_key, r.room_name
    FROM
    (
        SELECT
            u.*, up.service_key, up.user_plan_yearly, up.user_plan_yearly_starttime, up.user_plan_starttime
        FROM
            user_plan up
        LEFT JOIN
            user u
        ON
            up.user_key = u.user_key
        WHERE
            up.user_plan_status = 1 AND
            DATE_FORMAT( up.user_plan_starttime, '%Y-%m' ) <= '" . $year. "-" . $month . "'
    ) u
    LEFT JOIN
        room r
    ON
        u.user_key = r.user_key
    WHERE
        u.account_model = 'member' AND
        u.user_status=1 AND
        ( u.user_delete_status = 0 OR
        ( u.user_delete_status = 2 AND DATE_FORMAT( u.user_deletetime, '%Y-%m' ) >= '". $year. "-" . $month ."'"." ) )
) u
LEFT JOIN
    meeting m
ON
    u.room_key = m.room_key
WHERE
    m.use_flg=1 AND
    DATE_FORMAT( m.actual_end_datetime, '%Y-%m' ) = '". $year. "-" . $month ."' AND
    m.meeting_use_minute >= 1
ORDER BY
    u.user_key, m.meeting_key";
            $rs_userList = $objMeetingdb->_conn->query( $sql );

            while( $ret_userInfo = $rs_userList->fetchRow( DB_FETCHMODE_ASSOC ) ){
                $planSql = sprintf( "
SELECT
    service_key, service_name
FROM
    service
WHERE
    service_key='%s'", $ret_userInfo["service_key"] );
                $rs_planInfo = $objMGMdb->_conn->query( $planSql );
                $planInfo = $rs_planInfo->fetchRow( DB_FETCHMODE_ASSOC );
                $ret_userInfo["service_name"] = $planInfo["service_name"];

                //導入サポートの取得
                $objUserServiceOption		= new UserServiceOptionTable( $dsn );
                $where = "user_key='" . $ret_userInfo["user_key"] . "' AND user_service_option_status=1 AND service_option_key = 14 AND DATE_FORMAT( user_service_option_starttime, '%Y-%m' ) = '" . $info["year"]. "-" . $info["month"] . "'";
                $ret_userInfo["support"] = 	$objUserServiceOption->numRows( $where );

                //シェアリング、容量追加オプションの取得
                $optionList = 	$objUserServiceOption->getRowsAssoc( sprintf( "user_key='%s' AND user_service_option_status=1 AND service_option_key != 14", $ret_userInfo["user_key"] ) );
                $desktop_share = 0;
                $hdd_extension = 0;
                for( $i = 0; $i < count( $optionList ); $i++ ){
                    switch( $optionList[$i]["service_option_key"] ){
                        case "3":
                               $desktop_share = 1;
                            break;

                        case "4":
                            $hdd_extension += 1; //spellどうすっか
                            break;
                    }
                }
                $ret_userInfo["desktop_share"] = $desktop_share;
                $ret_userInfo["hdd_extension"] = $hdd_extension;

                $sql = "
SELECT
    p.*,
log.num
FROM
    (
    SELECT
        p.participant_key,
        p.participant_name,
        p.participant_station,
        if( DATE_FORMAT( p.uptime_start, '%Y-%m' ) = '" . $now . "' || p.uptime_start IS null, p.uptime_start, '" . $firstDay . "' ) as uptime_start ,
        if( DATE_FORMAT( p.uptime_end, '%Y-%m' ) = '" . $now . "' || p.uptime_end IS null, p.uptime_end, '" . $lastDay . "' ) as uptime_end ,
        m.member_id,
        m.member_key,
        m.member_name,
        m.member_name_kana,
        m.room_key
    FROM
        participant p
    LEFT JOIN
        member m
    ON
        p.member_key = m.member_key
    WHERE
        meeting_key = '" . $ret_userInfo["meeting_key"] . "'
    ) p
LEFT JOIN
    (
    SELECT
        COUNT( * ) as num, participant_key
    FROM
        meeting_member_use_log
    WHERE
        DATE_FORMAT( create_datetime, '%Y-%m' ) = '" . $now . "'
    GROUP BY
        participant_key
    ) log
ON
    p.participant_key = log.participant_key
ORDER BY
    p.member_key";

                $rs_memberList = $objMeetingdb->_conn->query( $sql );
                $memberList = array();
                while( $ret_memberInfo = $rs_memberList->fetchRow( DB_FETCHMODE_ASSOC ) ){
                    $ret_userInfo["participant_key"] = $ret_memberInfo["participant_key"];
                    $ret_userInfo["participant_name"] = $ret_memberInfo["participant_name"];
                    $ret_userInfo["participant_station"] = $ret_memberInfo["participant_station"];
                    $ret_userInfo["uptime_start"] = $ret_memberInfo["uptime_start"];
                    $ret_userInfo["uptime_end"] = $ret_memberInfo["uptime_end"];
                    $ret_userInfo["member_id"] = $ret_memberInfo["member_id"];
                    $ret_userInfo["member_key"] = $ret_memberInfo["member_key"];
                    $ret_userInfo["member_name"] = $ret_memberInfo["member_name"];
                    $ret_userInfo["member_name_kana"] = $ret_memberInfo["member_name_kana"];
                    $ret_userInfo["num"] = $ret_memberInfo["num"];
                    var_dump($ret_userInfo); echo"<br>";

                }
            }
        }
    }

*/
    private function action_top()
    {
        $request = $this->request->getAll();
        if( $request["year"] && $request["month"] && $request["day"] ){
            $date["year"]   = $request["year"];
            $date["month"]  = $request["month"];
            $date["day"]  = $request["day"];
        } else {
            $lastdate = strtotime( "last Day" );
            $date["year"] = date( "Y", $lastdate );
            $date["month"] = date( "m", $lastdate );
            $date["day"] = date( "d", $lastdate );
        }
        print "<pre>";
        print_r($date);
        $date["now"] = sprintf( "%04d-%02d-%02d", $date["year"], $date["month"], $date["day"] );
        $server_list = parse_ini_file( sprintf( "%sconfig/server_list.ini", N2MY_APP_DIR ), true);
        foreach( $server_list["SERVER_LIST"] as $key => $dsn ){
            $objN2MYIndividual      = new N2MY_IndividualAccount( $dsn, $this->account_dsn );
            $objMGMdb               = new N2MY_DB( $this->account_dsn );
            $objMeetingdb           = new N2MY_DB( $dsn );
            $objMemberUsageDetail   = new DBI_MemberUsageDetails( $dsn );
            $userList           = $objN2MYIndividual->getUserList( $date );
            $participantList    = $objN2MYIndividual->getParticipantList( $date );
            $result = array();
            for( $i = 0; $i < count( $userList ); $i++ ){
                for( $j = 0; $j < count( $participantList ); $j++ ){
                    if( $userList[$i]["user_key"] == $participantList[$j]["user_key"] ){
                        $result[] = array_merge( $userList[$i], $participantList[$j] );
                    }
                }
            }
            $result_count = array(
                'add' => 0,
                'update' => 0
                );
            // 成形
            foreach( $result as $key => $info ){
                $data = array(
                    "user_key"                      => $info["user_key"],
                    "user_id"                       => $info["user_id"],
                    "user_company_name"             => $info["user_company_name"],
                    "user_company_postnumber"       => $info["user_company_postnumber"],
                    "user_company_address"          => $info["user_company_address"],
                    "user_company_phone"            => $info["user_company_phone"],
                    "user_company_fax"              => $info["user_company_fax"],
                    "user_starttime"                => $info["user_starttime"],
                    "user_deletetime"               => $info["user_deletetime"],
                    "user_plan_yearly"              => $info["user_plan_yearly"],
                    "user_plan_yearly_starttime"    => $info["user_plan_yearly_starttime"],
                    "room_key"                      => $info["room_key"],
                    "meeting_max_seat"              => $info["meeting_max_seat"],
                    "service_name"                  => $info["service_name"],
                    "intro"                         => $info["intro"],
                    "desktop_share"                 => $info["desktop_share"],
                    "hdd_extension"                 => $info["hdd_extension"],
                    "meeting_key"                   => $info["meeting_key"],
                    "meeting_room_name"             => $info["meeting_room_name"],
                    "actual_start_datetime"         => $info["actual_start_datetime"],
                    "actual_end_datetime"           => $info["actual_end_datetime"],
                    "participant_key"               => $info["participant_key"],
                    "participant_station"           => $info["participant_station"],
                    "member_key"                    => $info["member_key"],
                    "member_id"                     => $info["member_id"],
                    "member_name"                   => $info["member_name"],
                    "member_name_kana"              => $info["member_name_kana"],
                    "member_email"                  => $info["member_email"],
                    "uptime_start"                  => $info["uptime_start"],
                    "uptime_end"                    => $info["uptime_end"],
                    "use_count"                     => $info["use_count"],
                );
                // 作成日が同じ場合は追加しない（現在は集計日のカラムの項目がないため問題有り）
                $where = "user_key = ".$info["user_key"].
                    " AND meeting_key = ".$info["meeting_key"].
                    " AND member_key = ".$info["member_key"].
                    " AND participant_key = ".$info["participant_key"].
                    " AND create_datetime LIKE '".date( "Y-m-d" )."%'";
                $num = $objMemberUsageDetail->numRows($where);
                if ($num > 0) {
                    $result_count['update']++;
//                    $objMemberUsageDetail->update($data, $where);
                } else {
                    $result_count['add']++;
                    $data["create_datetime"] = date( "Y-m-d H:i:s" );
                    $objMemberUsageDetail->add($data);
                }
            }
        }
        print_r($result_count);
    }
}

$main =& new AppMemberLog();
$main->execute();
