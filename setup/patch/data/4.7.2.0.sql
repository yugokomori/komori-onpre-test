ALTER TABLE  `room` ADD  `invited_limit_time` INT NOT NULL DEFAULT  '0' COMMENT  '時間制限を適用する場合は分をいれる' AFTER  `disable_rec_flg`;
ALTER TABLE  `user` ADD  `external_member_invitation_flg` TINYINT NOT NULL DEFAULT  '0' COMMENT  '別ユーザーに紐付くメンバーの招待（メンバー課金用）' AFTER  `vcubeid_add_use_flg`;
ALTER TABLE  `member` CHANGE  `lang`  `lang` VARCHAR( 10 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '言語';
ALTER TABLE  `reservation_user` CHANGE  `r_user_lang`  `r_user_lang` VARCHAR( 10 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;