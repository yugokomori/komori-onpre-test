//dom.event.addEventListener(window, 'load', ecoInit );
var eco = new Object();
function ecoInit()
{
    eco = new EcoClass();
};

var FormClass = function(){};
FormClass.prototype = {
    createElement: function( name ){ var element = document.createElement( name ); return element; },
    remove :function( idName ){ while( idName.hasChildNodes() ){ idName.removeChild( idName.lastChild ); }; },
    excute: function()
    {
        var login_form = document.getElementById( "login_form" );
        login_form.submit();
    }
};

var cookieData = false;
var sendFlg = false;
var searchFlg = false;
var resultStations;

var EcoClass = Class.create();
EcoClass.prototype =
{
        FormClass: new FormClass(),
        url: "/services/eco/",

        //駅名検索
        search: function( Boolean )
        {
            var station = document.getElementById( "eco-place" ).value;
            sendFlg = Boolean;
            
//            if (searchFlg == false ||
//               (this.sendFlg && ! station)) {
//           	this.FormClass.excute();
//                //sendFlg = Boolean;
//                return;
//            }
            
            if(sendFlg && !station)  {
           	this.FormClass.excute();
                //sendFlg = Boolean;
                return;
            }
            
            if( ! station||(cookieData == true &&  searchFlg == false)) {
                alert( "駅名を入力してください" );
                return false;
            }
            
            searchFlg = false;	
            var myAjax = new Ajax.Request(
            this.url,
            {
                method : 'post',
                postBody : $H({
                    action_get_stationlist: '',
                    station_name: station
                    }).toQueryString(),
                onSuccess: this.searchResult
            }
            );},

        //検索結果
        searchResult: function( obj )
        {
            if( obj.responseText ){
                eval("var result = " + obj.responseText);
                resultStations = result;
                EcoClass.prototype.setResultStation();
            } else {
                EcoClass.prototype.fault();
            }
        },

        fault: function()
        {
            alert( "該当する駅名がありません。" );
            sendFlg = false;
        },

        setResultStation: function()
        {
            var result = document.getElementById( "result" );
            this.FormClass.remove( result );
            if( resultStations.length > 1 ){
                this.setSelectForm();
                sendFlg = false;
            } else if( resultStations.length == 1) {
                this.setStationName( resultStations[0].value );
            } else {
                this.fault();
                return false;
            }
        },

        setStationName: function( station_name )
        {
            var multiLine = document.getElementById( "multi_line" );
            multiLine.style.display = "none";

            var result = document.getElementById( "result" );
            var input = this.FormClass.createElement( "input" );
            input.type = "hidden";
            input.name = "user_station";
            input.value = station_name;
            result.appendChild( input );

            if( sendFlg ){
                this.FormClass.excute();
                return;
             } else {
                this.controlResultField( true );
                result.appendChild( document.createTextNode( station_name ) );
             }
            this.setInit( false );
        },

        setSelectForm: function()
        {
            this.setInit( false );
            this.controlResultField( true );
            var multiLine = document.getElementById( "multi_line" );
            multiLine.style.display = "block";

            var result = document.getElementById( "result" );
            var select = this.FormClass.createElement( "select" );
                select.name = "user_station";

            var size = ( 3 < resultStations.length ) ? 3 : resultStations.length;
                select.size = size;

                //ie6対策のため.1 秒後に再度表示させている
                select.style.visibility = "hidden";
                setTimeout( function() {
                    select.style.visibility = "visible";
                }, 100 );

            for( var i = 0; i < resultStations.length; i++ ){
                var option = this.FormClass.createElement( "option" );
                    option.value = resultStations[i].value;
                    option.appendChild( document.createTextNode( resultStations[i].value ) );
                    if( 0 == i ) option.selected = 'selected';
                        select.appendChild( option );
            }
            result.appendChild( select );

        },

        //
        setInit: function( Boolean ){ cookieData = Boolean },

        // 駅入力フォームへ set event
        setReturnEvent: function(){
            var ecoStationForm = document.getElementById("eco-place");
//            dom.event.addEventListener(ecoStationForm, 'focus', function(){
//                searchFlg = true;
//            });
            dom.event.addEventListener(ecoStationForm, 'keydown', function(e) {
                //Mozilla(Firefox, NN) and Opera
                keycode = (e != null) ? e.which : event.keyCode;
                //return
                switch( keycode ){
                    case 13:
                        if (e != null) {
                            // イベントの上位伝播を防止
                            e.preventDefault();
                            e.stopPropagation();
                            // Internet Explorer
                        } else {
                            event.returnValue = false;
                            event.cancelBubble = true;
                        }
                        if (searchFlg == true)
                        	EcoClass.prototype.search();
                        break;

                    default:
                    	searchFlg = true;//
                        break;
                }
            });
        },

        controlResultField: function( Boolean )
        {
            var station = document.getElementById( "station-col" );
            station.style.display = Boolean ? "block" : "none";
        },

        initialize: function()
        {
            var station_name = defaultStation;
            if( station_name ) {
                this.controlResultField( true );
                this.setStationName( station_name );
            } else {
                this.setInit( true );
                this.controlResultField( false );
            }
            this.setReturnEvent();
        }
};

//var eco = new Object();
