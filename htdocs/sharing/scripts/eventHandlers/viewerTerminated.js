if ( reason == 0 || reason == 1 || reason == 2001 ) {
	eventListener.onSharingEnd();
}

else if ( reason == 2002 ) {
	
	eventListener.onBeforeSharingStart();
	setTimeout( "rConnectSuccess()", 2000 );
}

else {
	
	alert(
		Constants.getTerminatedReason( reason ) + "\n" +
		"Code : " + reason
	);
	
	eventListener.onSharingError();
}
