<?php 

abstract class WSDLParams {
	public function __construct($class, $obj) {
		foreach ($obj as $key => $value) {
			if (property_exists(get_class($class), $key)) {
				$class->$key = $value; 
			}       
		}
	}

	public function getParams($class) {
		$arr = array();
		foreach ($class as $key => $value) {
			$arr[$key] = $value;
		}
		return $arr;
	}
}