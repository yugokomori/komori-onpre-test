<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("classes/N2MY_DBI.class.php");

/*
 * ビジネスロジックを含まない純粋なClipテーブルのDBI
 * */

class ClipTable extends N2MY_DB {

    var $table = "clip";
    var $logger = null;
    protected $primary_key = null;

    public function __construct( $dsn ) 
    {
        $this->init($dsn, $this->table );
    }
    public function add($data)
    {
        $data["createtime"] = date("Y-m-d H:i:s");
        $res = parent::add($data);
        if (PEAR::isError($res)) {
            throw new Exception("db errror where:$where , data:".print_r($data,true));
        }
    }
    public function update($data, $where)
    {
        $data["updatetime"] = date("Y-m-d H:i:s");
        $res = parent::update($data, $where);
        if (PEAR::isError($res)) {
            throw new Exception("db errror where:$where , data:".print_r($data,true));
        }
    }
    //{{{ updateupdateByClipKey
    public function updateByClipKey($data, $clip_key)
    {
        if (!$clip_key) {
            throw "clip_key required";
        }

        $where = "clip_key = '".mysql_real_escape_string($clip_key)."'";
        $this->update($data, $where);
    
    }
    //}}}
    //{{{ countByUserKey
    public function countByUserKey($user_key, $and_where = null) 
    {
        if (!$user_key) {
            throw "user_key is empty";
        }
        $where = sprintf("user_key ='%s' AND is_deleted = 0 ", mysql_real_escape_string($user_key));
        if ($and_where) {
            $where = $where .' AND '.$and_where;
        }
        $res = $this->numRows($where);
        if ($res === false) {
            throw new Exception('sql execute error where :'.$where);
        }

        return $res;
    }
    //{{{ findByUserKey
    public function findByUserKey($user_key, $sort = array(), $limit = null, $offset = 0, $columns = "*", $and_where = null) 
    {
        if (!$user_key) {
            throw new Exception("user_key is empty");
        }
        $where  = sprintf("user_key ='%s' AND is_deleted = 0 ", mysql_real_escape_string($user_key));
        if ($and_where) {
            $where = $where .' AND '.$and_where;
        }

        $db_res = $this->select($where, $sort, $limit, $offset, $columns);
        if (!$db_res || PEAR::isError($db_res)) {
            throw new Exception("db errror where:".$db_res->getQuery());
        }

        $res = array();
        while ($row = $db_res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $res[] = $row;
        }
        return $res;
    }
    ////{{{ findConvertedByUserKey
    public function findConvertedByUserKey($user_key, $sort = array(), $limit = null, $offset = 0, $columns = "*") 
    {
        return $this->findByUserKey($user_key, $sort, $limit, $offset, $columns, 'clip_status = 2');
    }
    //{{{ countConvertedByUserKey
    public function countConvertedByUserKey($user_key)
    {
        return $this->countByUserKey($user_key, 'clip_status = 2');
    }
    public function updateByUsersClipKey($data, $clip_key, $user_key)
    {
        if (!$user_key || !$clip_key) {
            throw new Exception("user_key: $user_key  or clip_key : $clip_key  is empty ");
        }
        $where = "user_key = '".mysql_real_escape_string($user_key)."'"
               ." AND clip_key = '".mysql_real_escape_string($clip_key)."'";
        $res =  $this->update($data, $where);
        if (PEAR::isError($res)) {
            throw new Exception(__FILE__." ".__LINE__." db errror where:$where , data:".print_r($data,true));
        }
    }
    public function deleteByUsersClipKey($clip_key, $user_key)
    {
        $this->updateByUsersClipKey(array('is_deleted'=>1), $clip_key, $user_key);
    }
    //{{{ findIncDeleteByUsersClipKeys
    //} 削除済みのクリップも含める
    //*/
    public function findIncDeletedByUsersClipKeys($clip_keys, $user_key, $sort = array(), $limit = null,
                                        $offset = 0, $columns = "*")
    {
        return $this->findbyUsersClipKeys($clip_keys, $user_key, $sort, $limit, $offset, $columns, true);
 
    }
    //{{{ findByUsersClipKeys
    public function findbyUsersClipKeys($clip_keys, $user_key, $sort = array(), $limit = null,
                                        $offset = 0, $columns = "*", $include_delete = false)
    {
        if (!$user_key) {
            throw new Exception("user_key: $user_key is empty ");
        }
        if (count($clip_keys) <= 0){
            return array();
        }
        foreach ($clip_keys as $k => $clip_key) {
            $clip_keys[$k]  = mysql_real_escape_string($clip_key);
        }
        $where  = sprintf("clip_key in ('%s') AND user_key='%s'", 
                          implode($clip_keys,"','"), mysql_real_escape_string($user_key)); 
        if (!$include_delete) {
            $where .= " AND is_deleted = 0";
        }
        $db_res = $this->select($where ,$sort, $limit, $offset, $columns);
        if (!$db_res || PEAR::isError($db_res)) {
            throw new Exception(__FILE__.' '.__LINE__." where : $where");
        }

        $res = array();
        while ($row = $db_res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $res[] = $row;
        }
        return $res;
    }
    //{{{ activeUsersClipKeysIn
    /*
     *  clip_keysの内、is_deleted=0のclip_keyを返す
     * */
    public function activeUsersClipKeysIn($clip_keys, $user_key)
    {
        if (count($clip_keys) <= 0) {
            return array();
        }

        $res  = $this->findByUsersClipKeys($clip_keys, $user_key, $sort = array(), $limit = count($clip_keys), 
                                           $offset = 0, $column = 'clip_key');
        if (count($res) <=0) {
            return array();
        }
        $res_clip_keys = array();
        foreach ($clip_keys as $clip_key)  {
            foreach ($res as $r) {
                if ($clip_key == $r['clip_key']) {
                    $res_clip_keys[] = $clip_key;
                }
            }
        }
        return $res_clip_keys;
    }
    //}}}
}
