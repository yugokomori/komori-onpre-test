-- マルチカメラ
ALTER TABLE `service` ADD `multicamera` INT NOT NULL COMMENT 'マルチカメラ' AFTER `whiteboard`;

INSERT INTO `service_option` (`service_option_key` ,`country_key` ,`service_option_name` ,`service_option_price` ,`service_option_init_price` ,`service_option_running_price`) VALUES
('18', '1', 'multicamera', '0', '0', '0');

INSERT INTO `options` (`option_key` ,`option_name` ,`create_datetime` ,`update_datetime`) VALUES
(16 , 'multicamera', '0000-00-00 00:00:00', NULL);

INSERT INTO `participant_type` (`participant_type_key` ,`participant_type_name` ,`create_datetime` ,`update_datetime`) VALUES
('8', 'multicamera', '0000-00-00 00:00:00', NULL),
('9', 'multicamera_audience', '0000-00-00 00:00:00', NULL);

-- 電話連携
ALTER TABLE `service` ADD `telephone` INT NOT NULL COMMENT '電話連携' AFTER `multicamera`;

INSERT INTO `service_option` (`service_option_key` ,`country_key` ,`service_option_name` ,`service_option_price` ,`service_option_init_price` ,`service_option_running_price`) VALUES
('19', '1', 'telephone', '0', '0', '0');

INSERT INTO `options` (`option_key` ,`option_name` ,`create_datetime` ,`update_datetime`) VALUES
(17 , 'telephone', '0000-00-00 00:00:00', NULL);

-- 録画GW
ALTER TABLE `service` ADD `record_gw` INT NOT NULL COMMENT '録画GW' AFTER `telephone`;

INSERT INTO `service_option` (`service_option_key` ,`country_key` ,`service_option_name` ,`service_option_price` ,`service_option_init_price` ,`service_option_running_price`) VALUES
('20', '1', 'record_gw', '0', '0', '0');

INSERT INTO `options` (`option_key` ,`option_name` ,`create_datetime` ,`update_datetime`) VALUES
(18 , 'record_gw', '0000-00-00 00:00:00', NULL);

-- FMS再起動時間設定を空で指定できるようにする
ALTER TABLE `fms_server` CHANGE `maintenance_time` `maintenance_time` VARCHAR( 8 ) NULL DEFAULT NULL COMMENT 'リブート時間';
