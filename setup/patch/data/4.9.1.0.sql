ALTER TABLE  `meeting_invitation` ADD  `contact_name` VARCHAR( 64 ) NULL DEFAULT NULL AFTER  `audience_session_id`;
ALTER TABLE  `user` ADD  `is_convert_wb_to_pdf` TINYINT( 1 ) NOT NULL DEFAULT  '1' COMMENT  'WBのPDF化デフォルトフラグ' AFTER  `is_minutes_delete`;
ALTER TABLE  `room` ADD  `is_convert_wb_to_pdf` TINYINT( 1 ) NOT NULL DEFAULT  '1' COMMENT  'WBのPDF化デフォルトフラグ' AFTER  `disable_rec_flg`;
ALTER TABLE  `reservation` ADD  `is_convert_wb_to_pdf` TINYINT( 1 ) NOT NULL DEFAULT  '1' COMMENT  'WBのPDF化デフォルトフラグ' AFTER  `is_auto_rec_flg`;
ALTER TABLE  `meeting_sequence` ADD  `last_play_datetime` DATETIME NULL DEFAULT NULL COMMENT  '最終再生日時' AFTER  `update_datetime`;
ALTER TABLE  `user` ADD  `support_info_type` VARCHAR( 64 ) NULL DEFAULT NULL COMMENT  'サポートタイプ(Free：free)' AFTER  `payment_terms`;