#! /bin/sh

HOSTNAME="`hostname`"
LOGFILE="/var/log/${HOSTNAME}.log"
SCRIPT_NAME="`basename $0`"
SHOSTNAME=${1}
PROCESS_TYPE=${2}
RC=0

LOG()
{
    STATUS=$1
    MSG=$2
    echo [`date '+%Y/%m/%d %T'`] [${STATUS}] "${HOSTNAME}" "${SCRIPT_NAME}" "${MSG}" >> ${LOGFILE}
}

#/usr/bin/curl "http://webmeeting.arrow.mew.co.jp/vcube/guest/api/mcu/server_notice.php?server=${SHOSTNAME}&type=${PROCESS_TYPE}"
/usr/bin/curl "http://t-webmeeting.arrow.mew.co.jp/vcube/guest/api/mcu/server_notice.php?server=${SHOSTNAME}&type=${PROCESS_TYPE}"
if [ $? -ne 0 ]; then
    LOG "err" "\"curl(server_notice.php) failed.\""
    RC=1
fi

exit ${RC}
