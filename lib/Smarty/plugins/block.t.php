<?php

if (!function_exists("_")) {
    // gettextのエミュレーション
    function _($a) {
        return $a;
    }
}

/*
 * Created on 2009/09/03
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
function smarty_block_t($params, $content, &$smarty) {
    static $logger = null;
    if (!$logger) {
        $logger = EZLogger2::getInstance();
    }
    $key = substr($content, 0, strpos($content, ","));
    if (!$key) {
        $source = substr($content, strpos($content, ",") + 4, -2);
        smarty_add_word($source);
        //$logger->debug(array($params, $contents, $source, $smarty->_plugins['block']));
        return $source;
    } else {
        $source = smarty_gettext($key, $smarty->_tpl_vars["__frame"]["lang"]);
        if ( !$source ) {
            $source = substr($content, strpos($content, ",") + 4, -2);
        }
        $text = "";
        $script = '$text = \''.$source.'\';';
        if(eval($script) === false) {
            $logger->warn(array($key, $content, $script, $text, $params));
            return $content;
        }
        if ($params) {
            //$logger->debug(array($key, $content, $script, $text, $params));
            $text = vsprintf($text, $params);
            if ($smarty->_tpl_vars["__frame"]["n2my"]["edit_template"]) {
                $text .= '<a target="template" href="/multilang/?action_get_template=&define_no='.$key.'&lang='.$smarty->_tpl_vars["__frame"]["lang"].'#'.$key.'">[編集]</a>';
            }
        }
    }
    //$logger->debug($text,"smarty block content");
    return $text;
}

function smarty_gettext($id, $lang) {
    static $_lang = null;
	static $data = null;
    static $data2 = null;
    if($_lang == null || $_lang != $lang) {
    	$_lang = $lang;
    	$data = null;
    }
    if (!isset($data)) {
        eval('$data = '.file_get_contents(N2MY_APP_DIR."config/lang/".$lang."/html.php").';');
        eval('$data2 = '.file_get_contents(N2MY_APP_DIR."config/lang/".$lang."/xml.php").';');
        $data = $data + $data2;
    }
//    print $data[$id];
    return $data[$id];
}

function smarty_add_word() {
    static $logger = null;
    static $ini = null;
    static $conn = null;
    if (isset($ini)) {
        $ini = parse_ini_file(N2MY_CONFIG_FILE, true);
        $logger = EZLogger2::getInstance();
        $logger->info($ini);
        $lang_dsn = $ini["GLOBAL"]["lang_dsn"];
    }
    if ($conn) {
        // 追加登録
    }
}
?>