<?php
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_Account.class.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/mgm/dbi/mcu_server.dbi.php");
require_once("classes/mgm/dbi/media_mixer.dbi.php");

set_time_limit(180);

class AppMcuNotice extends AppFrame {

    function init() {
    }

    function auth() {
    }

    function default_view(){
        $server = $this->request->get("server");
        $type = $this->request->get("type");
        $this->session->set("lang", "ja");
        $this->logger2->info(array($server,$type), "mcu server alert");
        $obj_mcuServer = new McuServerTable(N2MY_MDB_DSN);
        $obj_mediaMixer = new MediaMixerTable(N2MY_MDB_DSN);

        if (!$server) {
            $this->logger2->info(array("parameter error" ,$server, $type));
            print "parameter error";
            exit;
        }
        $where_now_mcu = "server_address LIKE '%".mysql_escape_string($server)."%'";
        $now_mcu_server = $obj_mcuServer->getRow($where_now_mcu);
        if (!$now_mcu_server) {
            $this->logger2->info(array("mcu server notfound" ,$server, $type));
            print "mcu server notfound";
            exit;
        } else if ($now_mcu_server["system_down_flg"]) {
            $this->logger2->info(array("already server down" ,$server, $type));
            exit;
        }

        //現在はmixerのみが落ちた場合でもコントローラーを切り替える
        switch ($type) {
            case "mcu":
                $alert_type = "mcu";
                break;
            case "mixer":
                $alert_type = "mcu";
                break;
            case "mcu,mixer":
                $alert_type = "mcu";
                break;
        }

        //mixerを停止
         $where_now_mixer = "media_ip = '".mysql_escape_string($now_mcu_server["ip"])."'";
         $data = array("is_available" => 0);
         $obj_mediaMixer->update($data, $where_now_mixer);

         //システムダウンステータスに変更
         $data = array("is_available" => 0,
             "system_down_flg" => 1);
         $obj_mcuServer->update($data, $where_now_mcu);

         //mcu_nortificationにデータ登録
         require_once 'classes/dbi/mcu_notification.dbi.php';
         $obj_McuNotification = new McuNotificationTable(N2MY_MDB_DSN);
         $add_mcuNotification_data = array(
           "server_key" => $now_mcu_server["server_key"],
           "server_address" => $now_mcu_server["server_address"],
           "server_down_flg" => 1,
           "notification_type" => 1,
           //"reservation_ids" => "1d03f53ca2850e3d79853c1ef17862e4,c601cd3617ab9b56a86745c563694392,8594a05860e89895464a2afa153d2ccb,17b9e1ae4ef0000266ccbd4d10942bc4",
           "server_down_datetime" => date('Y-m-d H:i:s'),
             );
         $obj_McuNotification->add($add_mcuNotification_data);

      //障害アラート送信
         $to = N2MY_MCU_NOTIFICATION_TO;
         $from = N2MY_MCU_NOTIFICATION_FROM;
         if ($to && $from) {
           $subject = $this->get_message("MCU_NOTIFICATION", "subject"). " " .$now_mcu_server["server_address"];
           $this->logger2->info($this->get_message("MCU_NOTIFICATION", "subject"));
           $this->template->assign("server_address", $now_mcu_server["server_address"]);
           $this->template->assign("ip", $now_mcu_server["ip"]);
           $this->template->assign("type", $type);
           $this->template->assign("send_date", date('Y-m-d H:i:s'));
           $body = $this->fetch("common/mail/mcu_notification.t.txt");
           $this->sendReport($from, $to, $subject, $body);
           $this->logger2->info(array($server,$type), "mcu server alert end");
         }

        //コントローラーでの利用か確認。コントローラーだった場合はmcuを切り替える。
        $this->logger2->info($now_mcu_server);
        if ($now_mcu_server["is_available"] && $alert_type == "mcu") {
            //停止しているサーバを取得
          $available_media_where = "is_available = 1";
          $available_media_servers = $obj_mediaMixer->getRowsAssoc($available_media_where);
          $this->logger2->debug($available_media_servers);
          $available_media_server_ip = "";
          foreach ($available_media_servers as $available_media_server) {
            $available_media_server_ip[] =  "'".$available_media_server["media_ip"]."'";
          }
          $where = "is_available = 0 AND system_down_flg = 0";
          $this->logger2->debug($available_media_server_ip);
            if($available_media_server_ip) {
              $where .= ' AND ip IN ('.implode(",",$available_media_server_ip).')';
              $this->logger2->info($where);
              $sort = array(
                      "server_key" => "asc");
              $mcu_servers = $obj_mcuServer->getRowsAssoc($where,$sort);
              $this->logger2->info($mcu_servers);
            } else {
              $mcu_servers = "";
            }

            if ($mcu_servers){

                foreach ($mcu_servers as $mcu_server) {
                    //別mcuを有効に変更
                    $cmd = sprintf("ssh " .N2MY_MCU_SCP_USER . '@%s "%s"' , $mcu_server["server_address"], $this->config->get("MCU_NOTIFICATION", "reload_cmd"));
                    exec($cmd, $arr, $res);
                    if($res !== 0){
                        $this->logger2->info(array($res,$arr,$cmd));
                    } else {
                        $where = "server_key = ".$mcu_server["server_key"];
                        $data = array("is_available" => 1);
                        $obj_mcuServer->update($data, $where);
                        break;
                    }
                }
            }
        }

        //APサーバに通知
        $ap_servers = explode(',', $this->config->get("MCU_NOTIFICATION", "ap_server_list"));
        $ap_server_symfony_path = $this->config->get("MCU_NOTIFICATION", "ap_server_symfony_path");
        $ap_server_symfony_task = $this->config->get("MCU_NOTIFICATION", "ap_server_symfony_task");

        $alert_status = 0;
        //$alert_status = 1; //for test
        if ($ap_server_symfony_task && $ap_server_symfony_path && $ap_servers) {
            foreach ($ap_servers as $ap_server) {
                $cmd = sprintf("ssh " .N2MY_AP_SCP_USER . '@%s %s' , $ap_server, '"'.$ap_server_symfony_path.'"'.' "'.$ap_server_symfony_task.'"');
                exec($cmd, $arr, $res);
                if($res !== 0){
                    $this->logger2->info(array($res,$arr,$cmd));
                } else {
                    $this->logger2->info(array($res,$arr,$cmd));
                    $alert_status = 1;
                    break;
                }
            }
        } else {
            $this->logger->warn(array($ap_servers, $ap_server_symfony_path, $ap_server_symfony_task),"notfound ap server config");
        }

        if ($alert_status) {
          require_once ("classes/N2MY_Reservation.class.php");
          $objReservation = new N2MY_Reservation($this->get_dsn(), $this->session->get("dsn_key"));
          require_once ("classes/dbi/reservation.dbi.php");
          $obj_Reservation = new ReservationTable($this->get_dsn());
          //現在の会議をチェック
          $now_reservations = array();
          $where_now_mixer = "media_ip = '".mysql_escape_string($now_mcu_server["ip"])."'";
          $now_mediaMixer = $obj_mediaMixer->getRow($where_now_mixer);
          //$this->logger2->info($now_mediaMixer)
          $where_now_reservation = "reservation_status = 1 AND media_mixer_key = '".mysql_escape_string($now_mediaMixer["media_mixer_key"])."'";
          $where_now_reservation .= "AND reservation_starttime < '".date('Y-m-d H:i:s')."'";
          $where_now_reservation .= "AND reservation_extend_endtime > '".date('Y-m-d H:i:s')."'";
          $_now_reservations = $obj_Reservation->getRowsAssoc($where_now_reservation);
          //$this->logger2->info($where_now_reservation);
          //$this->logger2->info($_now_reservations);
          foreach($_now_reservations as $key => $reservation_data) {
            if($reservation_data) {
              $reservation_data = $objReservation->getDetail($reservation_data["reservation_session"]);
              require_once 'classes/dbi/member.dbi.php';
              $obj_Member = new MemberTable($this->get_dsn());
              $where = "member_status = 0 AND member_key = '".addslashes($reservation_data["info"]["member_key"])."'";
              $member_info = $obj_Member->getRow($where);
              $reservation_info["member_info"] = $member_info;
              $reservation_info["info"] = $reservation_data["info"];
              //extend参加人数
              require_once("classes/dbi/reservation_extend.dbi.php");
              $obj_reservationExtend = new ReservationExtendTable($this->get_dsn());
              $reservation_extends = $obj_reservationExtend->getRowsAssoc("reservation_session_id = '".$reservation_info["info"]["reservation_session"]."'");
              //$this->logger2->info($reservation_extend);
              foreach ($reservation_extends as $reservation_extend) {
                if($reservation_extend["extend_seat"]) {
                  $reservation_info["info"]["max_seat"] += $reservation_extend["extend_seat"];
                }
              }

              $now_reservations[] = $reservation_info;

              //mcu_down_flgを立つ
              $data = array();
              $data["mcu_down_flg"] = 1;
              $where = "reservation_key = '".addslashes($reservation_data["info"]["reservation_key"])."'";
              $obj_Reservation->update($data, $where);
            }
          }

          //notificationに入ってreservation_idsの確認
          $future_reservations = array();
          $last_mcu_notification = $obj_McuNotification->getRow("","*",array("server_down_datetime" => "desc"));
          //$this->logger2->info($last_mcu_notification);
          $reservation_sessions = split(",", $last_mcu_notification["reservation_ids"]);
          $this->logger2->info($reservation_sessions);

          foreach($reservation_sessions as $reservation_session) {
              $reservation_data = $objReservation->getDetail($reservation_session);
              if($reservation_data && $reservation_data["info"]["status"] != "end") {
                require_once 'classes/dbi/member.dbi.php';
                $obj_Member = new MemberTable($this->get_dsn());
                $where = "member_status = 0 AND member_key = '".addslashes($reservation_data["info"]["member_key"])."'";
                $member_info = $obj_Member->getRow($where);
                $reservation_info["member_info"] = $member_info;
                $reservation_info["info"] = $reservation_data["info"];

                $future_reservations[] = $reservation_info;

                //mcu_down_flgを立つ
                $data = array();
                $data["mcu_down_flg"] = 1;
                $where = "reservation_key = '".addslashes($reservation_data["info"]["reservation_key"])."'";
                $obj_Reservation->update($data, $where);
              }

          }

          //メール送信
          require_once ("lib/EZLib/EZMail/EZSmtp.class.php");
          $mail = new EZSmtp(null, $lang_cd, "UTF-8");
          $mail->setSubject("ＶＡ 会議システム　予約再割り振り実施のお知らせ");
          $mail->setFrom(N2MY_RETRY_RESERVATION_FROM);
          $mail->setReturnPath(NOREPLY_ADDRESS);
          $mail->setTo(N2MY_RETRY_RESERVATION_TO);
          $template = new EZTemplate($this->config->getAll('SMARTY_DIR'));
          $template->assign('reservations', $future_reservations);
          $template->assign('now_reservations', $now_reservations);
          $template->assign('has_mcu_server_down', 1);
          $body = $template->fetch('common/mail/send_reservation.t.txt');
          $this->logger2->info($body);
          $mail->setBody($body);
          $mail->send();
        } else {
          //エラーログ出す
          $this->logger->warn(array($ap_servers, $ap_server_symfony_path, $ap_server_symfony_task),"no response from ap server");
        }
        $this->logger2->info(array($server,$type), "mcu server alert end");
    }
}

$main = new AppMcuNotice();
$main->execute();
