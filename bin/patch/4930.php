<?php
ini_set("memory_limit","512M");
require_once("set_env.php");
require_once ("classes/AppFrame.class.php");
require_once("classes/dbi/room.dbi.php");
require_once("classes/dbi/member.dbi.php");
require_once("classes/dbi/outbound.dbi.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/N2MY_Account.class.php");
require_once("classes/mgm/dbi/relation.dbi.php");

class Patch_4930 extends AppFrame {

    var $dsn = null;
    var $obj_user = null;
    var $obj_room = null;
    var $obj_member = null;
    var $obj_outbound = null;
    var $obj_N2MY_Account = null;
    var $obj_relation = null;

    function __destruct() {
        //unlink(N2MY_APP_DIR."/tmp/sess_".session_id());
    }

    function init() {
        $this->dsn = $this->get_dsn();
        $this->obj_user = new UserTable($this->dsn);
        $this->obj_room     = new RoomTable($this->dsn);
        $this->obj_member     = new MemberTable($this->dsn);
        $this->obj_outbound     = new OutboundTable($this->dsn);
        $this->obj_N2MY_Account = new N2MY_Account($this->dsn);
        $this->obj_relation = new MgmRelationTable(N2MY_MDB_DSN);
    }
    function default_view() {
        //outbound_number_id登録
        $where = "use_sales = 1 AND user_delete_status != 2";
        $user_list = $this->obj_user->getRowsAssoc($where);
        $this->logger2->info($user_list);
        foreach($user_list as $user){
            $outbound_where = "user_key = ".$user["user_key"];
	        $outbound_count = $this->obj_outbound->numRows($outbound_where);
	        $this->logger2->info($outbound_count);
	        if ($outbound_count == 0 || !$outbound_count) {
	            // 登録
	            $outbound_data = array(
	                "user_key"   => $user["user_key"],
	                "status"     => 1,
	                "create_datetime" => date("Y-m-d H:i:s"),
	                "update_datetime" => date("Y-m-d H:i:s"),
	                );
	            $this->obj_outbound->add($outbound_data);
	        }
	        //memberのoutbound_idを確認
            $member_where = "user_key = ".$user["user_key"].
                            " AND (outbound_id != '' OR outbound_id != NULL)";
	        $member_list = $this->obj_member->getRowsAssoc($member_where, null, null, 0, "member_key, member_id");
	        if ($member_list) {
	            $outbound_where = "user_key = ".$user["user_key"];
	            $outbound_info = $this->obj_outbound->getRow($outbound_where);
	            $outbound_key = sprintf("%03d",$outbound_info["outbound_key"]);
	            $sales_member_count = 0;
	            foreach ($member_list as $member) {
	                $sales_member_count = $sales_member_count + 1;
	                $outbound_sort = $sales_member_count;
		            $outbound_sort = sprintf("%03d",$outbound_sort);
		            $outbound_number_id = $outbound_key.$outbound_sort;
		            $member_update_data = array("outbound_number_id" => $outbound_number_id);
		            $where_member = "member_key = ".$member["member_key"];
		            $this->obj_member->update($member_update_data, $where_member);
		            $relation_data = array(
			            "relation_key" => $outbound_number_id,
			            "relation_type" => "outbound_number",
			            "user_key" => $member["member_id"],
			            "status" => "1",
			            "create_datetime" => date("Y-m-d H:i:s"),
			            );
			        $this->obj_relation->add($relation_data);
	            }
	        }
        }

    }

    private function _log($string){
        $config = parse_ini_file( sprintf( "%sconfig/config.ini", N2MY_APP_DIR ), true);
        $log_dir = $config["LOGGER"]["log_dir"];
        $log_prefix = "4930_patch";
        $log_file = sprintf("%s/%s_%s.log", $log_dir, $log_prefix, date('Ymd', time()));
        $fp = fopen($log_file, "a");
        if($fp){
            fwrite($fp, $string."\n");
        }
        fclose($fp);
    }

}
$main = new Patch_4930();
$main->execute();