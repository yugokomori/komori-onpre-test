jQuery.noConflict();
var j$ = jQuery;
var isMSIE = /*@cc_on!@*/false;
var appVersion = window.navigator.appVersion.toLowerCase();
var is_ie = 0;
var select_folder_id = 0;
var select_folder_member_key = 0;
var is_private_folder = 1;
var open_folder;
if (appVersion.indexOf("msie 6.") != -1) {
	is_ie = '6';
  } else if (appVersion.indexOf("msie 7.") != -1) {
	  is_ie = '7';
  } else if (appVersion.indexOf("msie 8.") != -1) {
	  is_ie = '8';
  } else if (appVersion.indexOf("msie 9.") != -1) {
	  is_ie = '9';
  }


var engine = document.documentMode;
/*
 * TODO
 */
function deleteFile(userKey, memberKey) {
    var storageIds = {};
    var territorys = {};
    var owners     = {};
    var sortKey    = null;
    var exit       = false;
    j$("input:checkbox:checked").each(function(){
    	sortKey = j$(this).val();
        storageIds[sortKey] = j$("#storage_id" + sortKey).val();
    });

    j$.getJSON(
        "/services/storage/index.php?action_delete=",
        {
            "storage_ids" : storageIds,
            "offset"      : parseInt(j$("input:checkbox").length),
            "ts"           : new Date().getTime()
        },
        function (data, status) {
            var error = null;
            if (!data || data.error) {
                return alert(_(8160, "ファイル削除に失敗しました。"  + data.error));
            }
            setList();
            alert(_(8161, "ファイル削除しました。"));
            changeAllCheckbox(true);
        }
    );
}

function selectFile(){
    j$("input:checkbox:checked").each(function(){
    	storage_id = j$(this).val();
        storage_name = j$("#storage_name"+storage_id).children("a").html();
        //alert(storage_id +" " + storage_name +" " + storage_owner_mid);
        if (opener.window.add_storage_row(storage_id, storage_name)==false) {
            alert("error");
        }
    });
    window.close();
}




function uploadFile() {
    j$(function($){
    	if(is_ie == 6 || is_ie == 7){
	        var link = document.createElement("a");
	        link.setAttribute("href", "?action_upload=&offset=" + parseInt($("input:checkbox").length) + "&ts=" + new Date().getTime());
	        window.open(link, 'mywindow2', 'width=440, height=260, menubar=no, toolbar=no, scrollbars=yes');
    	}
    	else{
	        var link = document.createElement("a");
	        link.setAttribute("href", "?action_upload=&offset=" + parseInt($("input:checkbox").length) + "&ts=" + new Date().getTime());
	        link.setAttribute("rel", "shadowbox;width=440;height=260");
	        Shadowbox.open(link);
    	}
    });
}

function downloadFile() {
    var storageId = "";
    j$("input:checkbox:checked").each(function(){
        var sortKey = j$(this).val();
        storageId += "storage" + sortKey + "=" + j$("#storage_id" + sortKey + ", ").val() + "&";
    });
    location.href = "/services/whiteboardStorage/index.php?action_download=&" + storageId;
}

// TODO
function setFavorite(storageId, sortKey, checked) {
    if (checked) {
        checked = 1;
    } else {
        checked = 0;
    }
    j$.getJSON(
        "/services/storage/index.php?action_favorite=",
        {
            "storage_id" : j$("input[name=storage_id" + sortKey + "]").val(),
            "checked"    : checked,
            "ts"           : new Date().getTime()
        },
        function (data, status) {
            if (data && "db" == data.error) {
                return alert(_(8165, "お気に入りの追加に失敗しました。"));
            }
            favoriteStatus = false;
            var favoriteImage = "off";

            if (!checked) {
                favoriteStatus = true;
                favoriteImage = "on";
            }
            j$( '#storage_favorite' + sortKey).empty().append(
                "<input type='hidden' id=favorite_status" + sortKey + " name=storage_favorite" + sortKey +" value=\"" + favoriteStatus + "\">" +
                "<img src='/shared/images/star_" + favoriteImage + ".gif' onclick='setFavorite("
                    + storageId +" ," + "\"" + sortKey + "\"" + ", " + favoriteStatus
                    + ");' width='16' height='16' alt='' />"
            );
            setFavoriteStatus();
        }
    );
}

function setFavoriteStatus() {
    var result = false;
    if (1 <= j$("input:checked").length) {
        var storageFavorite = false;
        var favoriteAddStatus = false;
        var messege = _(8166, "お気に入りに追加" );
        var sortKey = null;

        j$("input:checked").each(function(){
            sortKey = j$(this).val();
            if (sortKey) {
                storageFavorite = j$("#favorite_status" + sortKey).val();
                if (!storageFavorite) {
                    favoriteAddStatus = true;
                }
            }
        });
        j$('#favorite').empty().append(
            "<a onClick='setSomeFavorite("+ false + ");'>"
                + messege + "</a>");
        result = true;
    }
    return result;
}

// TODO
function setSomeFavorite(checked) {
    var storageIds = {};
    var sortKey
    j$("input:checkbox:checked").each(function(){
    	sortKey = j$(this).val();
        storageIds[sortKey] = j$("#storage_id" + sortKey).val();
    });

    j$.getJSON(
        "/services/storage/index.php?action_some_favorite=",
        {
            "storage_ids" : storageIds,
            "checked"     : checked,
            "ts"           : new Date().getTime()
        },
        function (data, status) {
            if(data && "db" == data.error){
                return alert(_(8165, "お気に入りの追加に失敗しました。"));
            }
            var sortKey = null;
            j$("input:checkbox:checked").each(function(){
                var favoriteStatus = false;
                var favoriteImage = "off";

                if (!checked) {
                    favoriteStatus = true;
                    favoriteImage = "on";
                }
                sortKey = j$(this).val();
                j$( '#storage_favorite' + sortKey).empty().append(
                    "<input type='hidden' id=favorite_status" + sortKey + " name=storage_favorite" + sortKey +" value=\"" + favoriteStatus + "\">" +
                    "<img src='/shared/images/star_" + favoriteImage + ".gif' "
                        + "onclick='setFavorite("
                        + j$("#storage_id" + sortKey).val() +" ," + "\""
                        + sortKey + "\"" + ", "
                        + favoriteStatus + ");' width='16' height='16' alt='' />"
                );
            });
            alert(_(8167, "お気に入りの追加しました。"));
            changeAllCheckbox(true);
            j$("#more-btn").parent().find("ul.more-subnav").slideUp('slow');
            j$('#toolbutton').hide();
        }
    );
}


// TODO 小森修正中
function searchList(searchType, searchArea) {
    j$.getJSON(
        "/services/storage/index.php?action_searchList=",
        {
            "searchType"   : searchType,
            "sort_key"     : j$("input[name=sort_key]").val(),
            "sort_type"    : j$("input[name=sort_type]").val(),
            "ts"           : new Date().getTime()
        },
        function (data, status) {
        	j$("input[name=search_type]").val(searchType);
            if(!data || "db" == data.error){
                return alert(_(8168, "一覧の絞込みに失敗しました。"));
            }
            var sortKey = null;
            j$('#listbody').empty();
            for(i = 0; i < data.list.length; i++){
                sortKey = 1;
                j$('#listbody').append(
                    outputListRow(sortKey, getAccount(), data.list[i], false)
                );
            }
            j$('#common_storage_size').empty().append(data.size.use_size + " / " + data.size.max_size +" MB ");
            resetClass();
            if(searchArea == "nav"){
                j$('#'+searchType+'_dsp').addClass('active_type');
            }
            //changeNextRowButton(data.next_row);
            changeCheckbox(getAccount().memberKey);

            if (isMSIE) {
            	setTimeout("setbox()", 1000);
            } else {
            	setbox();
            }

        }
    );
}

function setbox(){
    Shadowbox.clearCache();
    Shadowbox.setup();
}

function change_navlist(){

}

function defaultFolder(node, folderName, adminStatus) {
    var event = "";
    if (!adminStatus) {
        event = "onClick='getFolderList(\"" + node + "\", \"" + folderName + "\", true);'";
    }
    var folder  = "<span class='' id='folder_name_" + node + "'" + event + "><object>" + folderName + "</object></span>";
    if (adminStatus) {
        addFolderRow(node, '', 'close_arrow', folderName, true, '', folder, 'on', false, true);
    } else {
        addFolderRow(node, '', 'close_arrow', folderName, false, '', folder, 'on', false, true);
    }
}

function adminFolder() {
    var allinputs = j$("input:checkbox:checked");
    var storageId = "";

    allinputs.each(function(){
        var sortKey = j$(this).val();
        storageId += "&storage_id_" + sortKey + "=" + j$("#storage_id" + sortKey + ", ").val();
    });
    var offset = "&offset=" +  j$("input:checkbox").length;
    var link = document.createElement("a");
    link.setAttribute("href", "?action_adminFolder=" + storageId + offset);
    link.setAttribute("rel", "shadowbox;width=600;height=400");
    Shadowbox.open(link);
}

function getFolderList(node, folderName, rootStatus) {
    var position = "";
    if (rootStatus) {
        position = "root";
    }
    j$.getJSON(
        "/services/whiteboardStorage/index.php?action_searchFolderList=",
        {
            "node"     : node,
            "position" : position
        },
        function (data, status) {
            if(!data || "db" == data.error){
                return alert(_(10020, "一覧の絞り込みに失敗しました。"));
            }
            var sortKey = null;
            j$('#listbody').empty();

            if (data.list) {
                for(i = 0; i < data.list.length; i++){
                    sortKey = data.up_row + 1 + "_"
                        + (parseInt(data.rowset.offset) + i + 1);
                    j$('#listbody').append(
                        outputListRow(sortKey, getAccount(), data.list[i], false)
                    );
                }
            }
            changeNextRowButton(data.next_row);
        }
    );
}

function modifyFolderName(node, folderName, adminStatus, addStatus){
    var oldFolderName = "";
    if (!addStatus) {
        oldFolderName = folderName;
    }

    j$("#folder_name_" + node).empty().append(
        "<input type='text' onKeyPress='FolderNameOnKeyPressBlur(event, " + node + ", " + adminStatus + ")' onBlur='FolderNameOnKeyPressBlur(\"\", " + node + ", " + adminStatus + ")' id='folder_change_name_" + node + "' name='folder_change_name_" + node + "' value='" + folderName + "'>" +
        "<input type='hidden' id='folder_before_name_" + node + "' name='folder_before_name_" + node + "' value='" + oldFolderName + "'>"
    );

    j$("#folder_id_" + node).attr("checked", "checked");
    j$('#folder_change_name_' + node).focus();
}

function FolderNameOnKeyPressBlur(event, node, adminStatus) {
    var folderModifyName = j$("input[name=folder_change_name_" + node + "]");
    if (event) {
        if (13 == event.keyCode) {
            if (!folderModifyName.val()) {
                return folderModifyName.focus();
            }
            modifyFolderNameComplete(node, adminStatus);
        }
        return;
    }
    if (!folderModifyName.val()) {
        return folderModifyName.focus();
    }
    modifyFolderNameComplete(node, adminStatus);
}

function modifyFolderNameComplete(node, adminStatus){
    var folderModifyName = j$("input[name=folder_change_name_" + node + "]").val();
    var folderBeforeName = j$("input[name=folder_before_name_" + node + "]").val();

    if (folderBeforeName == folderModifyName) {
        j$("#folder_name_" + node).empty()
            .append("<object onClick='modifyFolderName(\"" + node + "\", \"" + folderBeforeName + "\", " + adminStatus + ");'>" + folderBeforeName + "</object>");
        return;
    }
    j$.getJSON(
        "/services/whiteboardStorage/index.php?action_modifyFolderName=",
        {
            "node"   : node,
            "folder_name" : folderModifyName
        },
        function (data, status) {
            if (data && "db" == data.error) {
                return alert(_(10016, "フォルダ名を変更できません。"));
            }
            var arrow = "";
            if ("on" == j$("#" + node + "_open_status").val()) {
                arrow = 'close_arrow2';
            } else {
                arrow = 'open_arrow2';
            }
            j$("#arrow_" + node).empty()
                .append("<span class='" + arrow + "' onClick='getFolder(\"" + node + "\", \"" + folderModifyName + "\", " + adminStatus + ");'>&nbsp;</span>");

            j$("#folder_name_" + node).empty();
            if (adminStatus) {
                j$("#folder_name_" + node).append("<object onClick='modifyFolderName(\"" + node + "\", \"" + folderModifyName + "\", " + adminStatus + ");'>" + folderModifyName + "</object>");
            } else {
                j$("#folder_name_" + node).append(folderModifyName);
            }
        }
    );
}

function modifyFolderDoc() {
    var storageIds          = j$("input[name=storage_ids]").val().split(",");
    var addFolderIds        = j$("input[name=folder_ids]").val().split(",");
    var deleteFolderIds     = j$("input[name=del_folder_ids]").val().split(",");
    var exit                = true;
    var checkbox            = j$(":image");

    j$.getJSON(
        "/services/whiteboardStorage/index.php?action_modifyFolderDoc=",
        {
            "add_folder_ids" : addFolderIds,
            "del_folder_ids" : deleteFolderIds,
            "storage_ids"    : storageIds
        },
        function (data, status) {
            if (!data) {
                return alert(_(10008, "資料の登録に失敗しました。"));
            }
            var error = data.error;
            if (error) {
                for(i = 0; i < error.length; i++){
                    if ("db" == error[i].message){
                        alert(_(10012, "%sでDBエラーが発生しました。", error[i].name));
                    }
                }
            }
            if (data.list.add) {
                var add = data.list.add;
                for(i = 0; i < add.length; i++){
                    alert(_(10009, "%sに%sを登録しました。", add[i].folder_name, add[i].storage_name));
                }
            }
            if (data.list.del) {
                var del = data.list.del;
                for(i = 0; i < del.length; i++){
                    alert(_(10010, "%sから%sを削除しました。", del[i].folder_name, del[i].storage_name));
                }
            }
            parent.Shadowbox.close();
        }
    );
}


function deleteFolder_ander(node, admnStatus){
    var folderId = node;
    var offset   = j$("input[name=offset]").val();

    j$.getJSON(
        "/services/whiteboardStorage/index.php?action_deleteFolder=",
        {
            "folder_id"      : folderId,
            "offset"         : offset
        },
        function (data, status) {
            if (!data) {
                return alert(_(10013, "資料の削除に失敗しました。"));
            }
            var error = data.error;
            for(i = 0; i < error.length; i++){
                if ("db" == error[i].message){
                    alert(_(10014, "%sの削除に失敗しました。", error[i].name));
                }
            }

            j$('#' + folderId).empty();

            var newFolderIds = "";
            var oldFolderIds = j$("input[name=folder_ids]").val();
            if (oldFolderIds) {
                oldFolderIds = oldFolderIds.split(",");

                if (0 < oldFolderIds.length) {
                    for (var i = 0; i < oldFolderIds.length; i++) {
                        var keyExist = false;
                        var oldFolderId = oldFolderIds[i];
                        if (!oldFolderId) {
                            continue;
                        }
                        if (oldFolderId == folderId) {
                            keyExist = true;
                        }
                        if (!keyExist) {
                            newFolderIds += oldFolderIds[i] + ",";
                        }
                    }
                }
            j$("input[name=folder_ids]").val(newFolderIds);
            }

            redrawListCharge(data, admnStatus);
            changeNextRowButton(data.next_row);
        }
    );
}

function changeFolderCheckbox(node, checkStatus){
    var setStatus = false;
    var newDeleteFolderIds = "";

    if ("on2" == checkStatus) {
        var oldDeleteFolderIds = j$("input[name=del_folder_ids]").val().split(",");
        for (var i = 0; i < oldDeleteFolderIds.length; i++) {
            var oldDeleteFolderId = oldDeleteFolderIds[i];
            if (!oldDeleteFolderId) {
                continue;
            }
            if (node != oldDeleteFolderId) {
                newDeleteFolderIds += oldDeleteFolderId + ",";
                continue;
            }
            setStatus = true;
        }
        j$("input[name=del_folder_ids]").val(newDeleteFolderIds);
    }

    var defaultFolderIds = j$("input[name=default_folder_ids]").val().split(",");
    if ("on2" != checkStatus) {
        var newFolderIds = "";
        var oldFolderIds = j$("input[name=folder_ids]").val().split(",");
        var newDeleteFolderIds = "";
        var oldDeleteFolderIds = j$("input[name=del_folder_ids]").val().split(",");
        var addStatus = false;
        if (0 < oldFolderIds.length) {
            setStatus = false;
            for (var i = 0; i < oldFolderIds.length; i++) {
                var oldFolderId = oldFolderIds[i];
                if (!oldFolderId) {
                    continue;
                }
                if (parseInt(node) != oldFolderId) {
                    newFolderIds += oldFolderId + ",";
                    continue;
                }
                setStatus = true;
            }
            if (!setStatus) {
                newFolderIds += node;
                addStatus = true;
            }
        } else {
            newFolderIds += node;
            addStatus = true
        }
        j$("input[name=folder_ids]").val(newFolderIds);
    }

    if (0 < oldDeleteFolderIds.length) {
        for (var i = 0; i < oldDeleteFolderIds.length; i++) {
            var oldDeleteFolderId = oldDeleteFolderIds[i];
            if (!oldDeleteFolderId) {
                continue;
            }
                newDeleteFolderIds += oldDeleteFolderId + ",";
        }
    }
    if (!addStatus) {
        for (var i = 0; i < defaultFolderIds.length; i++) {
            if (parseInt(node) == defaultFolderIds[i]) {
                if (0 < oldDeleteFolderIds.length) {
                    var compareStatus = false;
                    for (var j = 0; j < oldDeleteFolderIds.length; j++) {
                        if (node == oldDeleteFolderIds[j]) {
                            compareStatus = true;
                            break;
                        }
                    }
                    if (!compareStatus) {
                        newDeleteFolderIds += node + ",";
                        break;
                    }
                    compareStatus = false;
                }
            }
        }
    }
    j$("input[name=del_folder_ids]").val(newDeleteFolderIds);

    var checkImage = "";
    if ("on2" == checkStatus || "on" == checkStatus) {
        checkImage = "/shared/images/checkbox.gif";
        checkStatus = "off";
    } else {
        checkImage = "/shared/images/checked.gif";
        checkStatus = "on";
    }
    j$("#check_" + node).empty().append(
            "<input type='image' src='" + checkImage + "' value='" + node + "' id='folder_id_" + node + "' onClick='changeFolderCheckbox(\"" + node + "\", \"" + checkStatus + "\")'>" +
            "<input type='hidden' id='check_status_" + node + "' value=" + checkStatus + ">"
    );
    changeFolderFileModifyButton();
}

function getAdminDefaultFolder(rootNode) {
    var storageIds = j$("input[name=storage_ids]").val();
    if ("" == storageIds) {
        return;
    }
    j$.getJSON(
        "/services/whiteboardStorage/index.php?action_getAdminFolder=",
        {
            "storage_ids"   : storageIds.split(",")
        },
        function (data, status) {
            var error = null;
            if (!data || data.error) {
                return alert(_(10015, "フォルダの取得に失敗しました。"));
            }
            var lists = data.lists;
            if (0 == lists.length) {
                return;
            }
            var oldFolderId;
            j$("input[name=default_folder_ids]").val(data.folder_ids);

            var folderIds = j$("input[name=default_folder_ids]").val().split(",");
            j$("input[name=storage_ids]").val(storageIds)

            for (var i = 0; i < lists.length; i++) {
                var list             = lists[i];
                var parentFolderId   = list.parent_folder_id;
                var parentFolderName = list.parent_folder_name;
                var childFolderId    = list.children_folder_id;
                var childFolderName  = list.children_folder_name;
                var checkbox = "";

                if (oldFolderId != parentFolderId) {
                    var modyfyName = "";
                    var event      = "";
                    var rootStatus = false;
                    if(rootNode == parentFolderId) {
                        var arrow  = "open_arrow";
                        var folder = "";
                        rootStatus = true;
                    } else {
                        var arrow  = "open_arrow2";
                        var folder = "class='folder1'";
                        checkbox   = getFolderCheckBox(parentFolderId, folderIds);
                        modyfyName = "onClick='modifyFolderName(\"" + parentFolderId + "\", \"" + parentFolderName + "\", true);'";
                    }
                    folder = "<span " + folder + " id='folder_name_" + parentFolderId + "'><object onClick='modifyFolderName(\"" + parentFolderId + "\", \"" + parentFolderName + "\", true);'>" + parentFolderName + "</object></span>";

                    j$('#' + parentFolderId).empty();
                    addFolderRow(parentFolderId, '', arrow, parentFolderName, true, checkbox, folder, 'off', false , rootStatus);

                }
                checkbox = getFolderCheckBox(childFolderId, folderIds);

                var folder = "<span class='folder1' id='folder_name_" + childFolderId + "'><object onClick='modifyFolderName(\"" + childFolderId + "\", \"" + childFolderName + "\", true);'>" + childFolderName + "</object></span>";

                addFolderRow(parentFolderId, childFolderId, 'close_arrow2', childFolderName, true, checkbox, folder, 'on', true, false);

                oldFolderId = parentFolderId;
                var nextList = lists[i+1];
                if (!nextList || nextList.parent_folder_id != parentFolderId) {
                    j$('#' + parentFolderId).append("</ul");
                }
            }
            changeFolderFileModifyButton();
        }
    );
}

function getFolder(node, folderName, adminStatus, rootStatus) {
    var folderIds;
    if (j$("input[name=folder_ids]").val()) {
        folderIds = j$("input[name=folder_ids]").val().split(",");
    }
    var openStatus = j$("#" + node + "_open_status").val();
    if ("on" == openStatus) {
        j$.getJSON(
            "/services/whiteboardStorage/index.php?action_getFolder=",
            {
                "node" : node
            },
            function (data, status) {
                if(!data || "db" == data.error){
                    return alert(_(10015, "フォルダの取得に失敗しました。"));
                }
                var arrowBox    = 'open_arrow';
                var arrowFolder = 'open_arrow2';
                openStatus      = "off";
                var param = {
                    'node'          : node,
                    'folderIds'     : folderIds,
                    'folderName'    : folderName,
                    'arrowBox'      : 'open_arrow',
                    'arrowFolder'   : 'open_arrow2',
                    'openStatus'    : 'off',
                    'adminStatus'   : adminStatus,
                    'rootStatus'    : rootStatus
                }
                outputParentFolder(param)
                outputFolder(data, node, folderIds, adminStatus);
            }
        );
    } else {
        var param = {
                'node'          : node,
                'folderIds'     : folderIds,
                'folderName'    : folderName,
                'arrowBox'      : 'close_arrow',
                'arrowFolder'   : 'close_arrow2',
                'openStatus'    : 'on',
                'adminStatus'   : adminStatus,
                'rootStatus'    : rootStatus
            }
        outputParentFolder(param)
    }
}

function getFolderCheckBox(node, folderIds) {
    var result = false;
    var checkStatus = false;
    var defaultFolderIds = j$("input[name=default_folder_ids]").val().split(",");
    var deleteFolderIds  = j$("input[name=del_folder_ids]").val();
    var count = 0;
    var node = parseInt(node);
    var checkImage = "/shared/images/checkbox.gif";

    if (defaultFolderIds) {
        for (var i = 0; i < defaultFolderIds.length; i++) {
            if (defaultFolderIds[i] != node) {
                continue;
            }
            if ("" == deleteFolderIds) {
                count++;
                continue;
            }
            deleteFolderIds = deleteFolderIds.split(",");
            var delStatus = false;
            for (var j = 0; j < deleteFolderIds.length; j++) {
                if (deleteFolderIds[j] == node) {
                    delStatus = true;
                    break;
                }
            }
            if (!delStatus) {
                count++;
            }
        }
        if (0 < count) {
            checkStatus = "on2";
            if (1 < count) {
                checkImage = "/shared/images/checked2.gif";
            } else if (1 == count) {
                checkImage = "/shared/images/checked.gif";
            }
        }
    }
    count = 0;
    if (!checkStatus && folderIds) {
        for (var j = 0; j < folderIds.length; j++) {
            if (folderIds[j] == node) {
                checkImage = "/shared/images/checked.gif";
                checkStatus = "on";
                break;
            }
        }
    }
    result = "<span id='check_" + node + "'>" +
             "<input type='image' src='" + checkImage + "' value='" + node + "' id='folder_id_" + node + "' onClick='changeFolderCheckbox(" + node + ", \"" + checkStatus + "\")' >&nbsp;" +
             "<input type='hidden' id='check_status_" + node + "' value=" + checkStatus + "></span>";

    return result;
}

function addFolderRow(bossNode, childNode, arrow, folderName, adminStatus, checkbox, folder, openStatus, childStatus, rootStatus, addStatus) {
    if (childNode) {
        addNode = childNode;
    } else {
        addNode = bossNode;
    }
    var button = "<object onClick='addFolder(\"" + addNode + "\", " + adminStatus + ");'><span class='create-folder'>&nbsp;</span></object>";
    if (!rootStatus) {
        button += "<object onClick='deleteFolder(\"" + addNode + "\", " + adminStatus + ");'><span class='delete-folder'>&nbsp;</span></object>";
    }

    var openTag  = "";
    var closeTag = "";
    var parentNode    = "";
    if (childStatus) {
        openTag  = "<li id = " + childNode + ">";
        closeTag = "</li>";
    }
    if (0 == j$('#parent_' + bossNode).size()) {
        openTag  = "<ul id='parent_" + bossNode + "'>" + openTag;
        closeTag = closeTag + "</ul>";
    } else {
        if (childStatus || addStatus) {
            parentNode    = "parent_";
        }
    }

    j$('#' + parentNode + bossNode).append(
        openTag +
        "<div onmouseover='changeFolderModifyButton(\"" + addNode + "\", \"show\");' onmouseout='changeFolderModifyButton(\"" + addNode + "\", \"hide\");'>" +
        "<object id='arrow_" + addNode + "'>" +
        "<span class='" + arrow + "' onClick='getFolder(\"" + addNode + "\", \"" + folderName + "\", " + adminStatus + ", " + rootStatus + ");'>&nbsp;</span>" +
        "</object>" + checkbox + folder +
        "<input type='hidden' id='" + addNode + "_open_status' value='" + openStatus + "'>" +
        "<object class='arrow-block' id = 'select_" + addNode + "' style='display: none;'>" + button + "</object>" +
        "</div>" +
        closeTag);
}

function changeFolderModifyButton(node, status){
    if ("show" == status) {
        if (!j$('#folder_change_name_' + node).val()) {
            j$('#select_' + node).show();
        }
    } else {
        j$('#select_' + node).hide();
    }
}

function changeFolderFileModifyButton() {
    var addFolderIds    = j$("input[name=folder_ids]").val();
    var deleteFolderIds = j$("input[name=del_folder_ids]").val();

    if ("" != addFolderIds || "" != deleteFolderIds) {
        var status = "class='button button25 b-poplayer' onClick='confirmAddDoc();'";
    } else {
        var status = "class='button button25 b-poplayer_off'";
    }
    j$('#update').empty().append(
        "<a " + status + "><span class='button-inner'><span class='btnin'>資料を登録</span></span></a>"
    );

}

function outputParentFolder(param) {
    var modyfyName  = "";
    var checkbox    = "";
    var folderId    = "";
    var rootStatus  = false;
    var folderName  = param.folderName;
    var node        = param.node;
    var adminStatus = param.adminStatus;
    var rootStatus  = param.rootStatus;

    if(rootStatus) {
        var arrow  = param.arrowBox;
        folderId = "";
        rootStatus = true;
    } else {
        var arrow  = param.arrowFolder;
        folderId   = "folder1";
        modyfyName = "onClick='modifyFolderName(\"" + node + "\", \"" + folderName + "\", " + adminStatus + ");'";
        if (adminStatus && ("" != j$("input[name=storage_ids]").val())) {
            checkbox = getFolderCheckBox(node, param.folderIds);
        }
    }
    var folder = "";
    if (adminStatus) {
        folder = "<span class='" + folderId + "' id='folder_name_" + node + "'><object " + modyfyName + ">" + folderName + "</object></span>";
    } else {
        folder = "<span class='" + folderId + "' onClick='getFolderList(\"" + node + "\", \"" + folderName + "\", " + rootStatus + ");'>" + folderName + "</span>";
    }

    j$('#' + node).empty();
    addFolderRow(node, '', arrow, folderName, adminStatus, checkbox, folder, param.openStatus, false, rootStatus);
    changeFolderFileModifyButton();
}

function outputFolder(lists, node, folderIds, adminStatus) {
    j$('#' + node).append("<ul class='close_folder>");

    var checkbox    = "";
    var getNodeEvent = "";
    var frameStatus  = false;

    for(var i = 0; i < lists.length; i++){
        var list = lists[i];
        if (!list.folder_id) {
            continue;
        }
        if (!frameStatus) {
            j$('#' + node).append("<ul id='parent_" + node + "'>");
        }
        var folderId     = list.folder_id;
        var folderName   = list.folder_name;

        var list = "";
        var checkbox = "";
        if (adminStatus && ("" != j$("input[name=storage_ids]").val())) {
            checkbox = getFolderCheckBox(folderId, folderIds);
        }
        if (!adminStatus) {
            list = "onClick='getFolderList(\"" + folderId + "\", \"" + folderName + "\", false, false);'";
        }
        var nodeEvent = "<object onClick='addFolder(\"" + folderId + "\", " + adminStatus + ");'><span class='create-folder'>&nbsp;</span></a></div>" +
                        "<object onClick='deleteFolder(\"" + folderId + "\", " + adminStatus + ");'><span class='delete-folder'>&nbsp;</span></a></div>";
        var folder  = "<span class='folder1' id='folder_name_" + folderId + "'" + list + ">";
        if (adminStatus) {
            folder += "<object onClick='modifyFolderName(\"" + folderId + "\", \"" + folderName + "\", " + adminStatus + ");'>";
            folder += folderName + "</object>";
        } else {
            folder += folderName;
        }
        folder     += "</span>";

        addFolderRow(node, folderId, 'close_arrow2', folderName, adminStatus, checkbox, folder, 'on', true);
    }
    if (!frameStatus) {
        j$('#' + node).append("</ul>");
        frameStatus = true;
    }
}

function addListRow() {
    j$.getJSON(
        "/services/storage/index.php?action_addListRow=",
        {
            "offset"   : parseInt(j$("input:checkbox").length),
            "ts"           : new Date().getTime()
        },
        function (data, status) {
            if (!data || "db" == data.error){
                return alert(_(10022, "一覧の読み込みに失敗しました。"));
            }
            var sortKey = null;
            var account = getAccount();

            if (data.list) {
                for (i = 0; i < data.list.length; i++){
                    sortKey = data.up_row + 1 + "_"
                        + (parseInt(data.rowset.offset) + i + 1);
                    j$('#listbody').append(outputListRow(sortKey, account, data.list[i], false));
                }
            }
            j$('#add_row_status').append(
                "<input type='hidden' id='add_status' name='add_status' value = true>"
            );
            var charges = data.charges;

            j$('#common_storage_size').empty().append(
                    charges.commonStorageSize + " MB / " +
                    charges.commonStorageMaxSize +" MB "
            );
            if (account.memberKey) {
                j$('#private_storage_size').empty().append(
                        charges.privateStorageSize + " MB / " +
                        charges.privateStorageMaxSize +" MB "
                );
            }
            changeNextRowButton(data.next_row);
            Shadowbox.setup();
        }
    );
}

function showPersonalWhite(storage_key){
    var _url =  '/services/storage/play.php?storage_key='+storage_key;
    jQuery.facebox({ajax : _url});
    jQuery(document).bind('afterClose.facebox',
        function() {
          jQuery('#facebox .content').empty();
        })
}

// TODO 小森編集中
function outputListRow(sortKey, account, list, addRow) {
	sortKey = list.storage_file_key;
	list.user_file_name = list.user_file_name.escapeHTML();
    if (!addRow) {
        var cell = "<tr id=doc" + sortKey + "><td class='doneMtg'>";
    } else {
        var cell = "<td class='doneMtg'>";
    }
    var checkbox = "";
    var memberKey = account["memberKey"];
    if(list.status == 0 || list.status == 1 || (j$("input[name=select_file]").val() && list.status != 2) || (j$("input[name=select_file]").val() && 'personal_white' == list.category)){
    	checkbox = "disabled";
    }else{

        cell += "<input type='checkbox' id=check" + sortKey + " name=check" + sortKey
          + " value=" + sortKey + " onClick='changeCheckbox(" + memberKey + ");'" + checkbox + ">";

        cell += "<input type='hidden' id=storage_id" + sortKey + " name=storage_id" + sortKey
          + " value=" + list.storage_file_key + ">";
        //共有・個人
        cell += "<input type='hidden' name='owner_member_key' value=" + list.member_key + ">";
    }
    cell += "</td><td class='doneMtg'>";

    cell += "<div id=\"storage_favorite"+ sortKey + "\">";
    cell += "<input type='hidden' id='favorite_status" + sortKey
          + "' name=storage_favorite" + sortKey +" value='" + list.favorite + "'>";

    if (list.favorite) {
        cell += "<img src='/shared/images/star_on.gif' "
              + "onclick='setFavorite("+ list.storage_file_key +" ," + "\"" + sortKey + "\"" + ", true);'"
              + "width='16' height='16' alt='' />";
    } else {
        cell += "<img src='/shared/images/star_off.gif' ";

        if ("0" == list.status || "1" == list.status || "2" == list.status) {
            cell += "onclick='setFavorite("+ list.storage_file_key +" ," + "\"" + sortKey + "\"" + ", false);'";
        }
        cell += "width='16' height='16' alt='' />";
    }
    cell += "</div>";
    cell += "</td>";

    cell += "<td class='doneMtg'><div id=storage_name"+ sortKey + ">";
    cell += "<input type='hidden' id=document_name" + sortKey + " value=\"" + list.user_file_name + "\">";

    if (2 == list.status) {
    	if('document' == list.category || 'image' == list.category){
	        if ('vector' == list.format && 'document' == list.category) {
	            var type = "width=640;height=480;player=swf";
	        } else {
	            var type = "player=img";
	        }

	        cell += "<a href=\"" + "/s/"+ list.storage_file_key + "/1/" + list.member_key + "/\""
	              + "src=\"/s/"+ list.storage_file_key + "/1/" + list.member_key + "/\""
	              + "alt=\"\" title=\"" + list.user_file_name + "(1 / " + list.document_index + ")\""
	              + "border=0 src=\"/s/ "+ list.storage_file_key +" /1/\" "
	              + "rel=" + "\"" + "shadowbox[docs" + sortKey + "];" + type + "\" id=\"file_name"+list.storage_file_key+"\">" + list.user_file_name + "</a>";

	        for (cnt = 1; cnt < list.document_index; cnt++) {
	            cell += "<a href=\"/s/" + list.storage_file_key + "/" + (cnt + 1) + "/" + list.member_key + "/\""
	                  + "src=\"/s/"+ list.storage_file_key + "/" + (cnt + 1) + "/" + list.member_key + "/\" "
	                  + "alt=\"\" title=\"" + list.user_file_name + "(" + (cnt + 1) + " / " + list.document_index + ")\""
	                  + "border=0 rel=" + "\"" + "shadowbox[docs" + sortKey + "]; " + type + "\"></a>";
	        }
        }
    	else if ('video' == list.category){
            if (j$("input[name=select_file]").val() ) {
                cell += "<a href='javascript:void(0);' onClick='playClip(\"" + list.clip_key + "\");return false;'>"+ list.user_file_name +"</a>";
            }else{
                cell +="<a href=\"/services/storage/index.php?action_showDetail&clip_key=" + list.clip_key + "&storage_file_key="+list.storage_file_key+"\" id=\"file_name"+list.storage_file_key+"\">" +list.user_file_name+ "</a>";
            }
    	}else if('personal_white' == list.category){
    		var type = "width=640;height=480";
/*
    		cell += "<a href=\"/services/storage/play.php?storage_key=" + list.storage_file_key + "\""
	            + "src=\"/services/storage/play.php/storage_key=" + list.storage_file_key + "\""
	            + "alt=\"\" title=\"" + list.user_file_name + "(1 / " + list.document_index + ")\""
	            + "border=0 src=\"/services/storage/play.php/storage_key=" + list.storage_file_key + "\""
	            + "rel=" + "\"" + "shadowbox[docs" + sortKey + "];" + type + "\" id=\"file_name"+list.storage_file_key+"\">" + list.user_file_name + "</a>";
*/
	        cell += "<a href=\"javascript:void(0)\" onclick=\"showPersonalWhite("+list.storage_file_key+");\""
	        + "rel=\"facebox[];width=120;height=120\" id=\"file_name"+list.storage_file_key+"\" >" + list.user_file_name + "</a>";
    		//cell += "<a href=\"/services/storage/play.php\">" +list.user_file_name+ "</a>";
    	}
    } else {
       cell += "<span id = \"file_name"+list.storage_file_key+"\">"+list.user_file_name + "</span>";
    }
    cell +=  "&nbsp;&nbsp;";
    switch (list.status) {
        case "0":
            cell += _(8169, "変換待ち");
            break;
        case "1":
            cell += _(8170, "変換中");
            break;
        case "2":
            break;
        case "3":
            cell += _(8171, "削除済");
            break;
        default:
            cell += "<span class='attention'>"+_(8172, 'エラー')+"</span>";
            break;
    }
    cell += "</div></td>";


    cell += "<td class='doneMtg'>" + list.extension + "</td>";
    cell += "<td class='doneMtg'>" + list.file_size + "</td>";

    if (j$("input[name=select_file]").val() == undefined) {
    	cell += "<td class='doneMtg'>";
     	cell += "<div id=storage_datetime"+ list.storage_file_key + ">" + list.update_datetime + "</div>";
    }
    cell += "<input type='hidden' id=storage_owner"+ sortKey + " name=storage_owner"+ sortKey + " value=" + list.owner + "></td>";


    if (0 != memberKey && j$("input[name=select_file]").val() == undefined) {

        if (0 != list.member_key) {
            var teritory = _(8173, "個人");
        } else {
            var teritory = _(8174, "共有");
        }
        cell += "<td class='doneMtg'>" + teritory;
        if ('0' != memberKey && memberKey == list.owner) {
            cell += " <img src='/shared/images/icon_owner.gif' width='12' height='16' alt='自分' class='owner' />";
        }
        cell += "</td><input type='hidden' id=storage_territory" + sortKey + " name=storage_territory" + sortKey +" value=" + list.member_key + ">";
    }

    if (!addRow) {
        cell += "</tr>";
    }
    return cell;
}

function getAccount() {
    var account = {
        "userKey" : j$("input[name=user_key]").val(),
        "memberKey" : j$("input[name=member_key]").val()
    };

    return account;
}

function redrawListCharge(data, admnStatus) {
    if (admnStatus) {
        var listNode = parent.document.getElementById('listbody');
    } else {
        var listNode = document.getElementById('listbody');
    }
    while (listNode.firstChild) {
        listNode.removeChild(listNode.firstChild);
    }
    var account = getAccount();
    sortKey = null;
    if (data.list) {
        for (i = 0; i < data.list.length; i++) {
            sortKey = data.up_row + 1 + "_"
                + (parseInt(data.rowset.offset) + (data.list.length - i));

            var result = outputListRow(sortKey, account, data.list[i], true);

            if (admnStatus) {
                var createNode = parent.document.createElement('tr');
            } else {
                var createNode = document.createElement('tr');
            }

            createNode.id        = sortKey;
            createNode.innerHTML = result;
            listNode.insertBefore(createNode, listNode.firstChild);
        }
    }

    var charges = data.charges;
    j$('#common_storage_size').empty().append(
        charges.commonStorageSize    + " MB / " +
        charges.commonStorageMaxSize +" MB ");
    if (account.memberKey) {
        j$('#private_storage_size').empty().append(
            charges.privateStorageSize    + " MB / " +
            charges.privateStorageMaxSize +" MB ");
    }

    if (admnStatus) {
        parent.Shadowbox.setup();
    } else {
        Shadowbox.setup();
    }
}

// チェックボックス変更時イベント TODO 小森編集中
function changeCheckbox(memberKey) {


	changeCheckboxModifyMoveButton( memberKey);
	changeCheckboxModifyCopyButton( memberKey);


    var checkStatus = "";
    var altStatus   = _(8189, "チェック");
    if (j$("input:checkbox").length == j$("input:checked").length) {
        checkStatus = "checked";
    } else if (1 <= j$("input:checked").length) {
        checkStatus = "checked2";
    } else {
        checkStatus = "checkbox";
    }
    j$('#all_check').empty()
        .append("<img src='/shared/images/" + checkStatus + ".gif' alt='" + altStatus + "' />");

    if (setFavoriteStatus()) {
        if(2 <= j$("input:checked").length){
        	j$('#name').hide();
        }else{
        	j$('#name').show();
        }
        return j$('#toolbutton').show();
    }

    j$('#toolbutton').hide();
}

// チェックボックス変更時のファイルコピーボタン表示 TODO
function changeCheckboxModifyCopyButton(memberKey) {
    var layerStatus  = "";
    var event  = "'";
    if (memberKey) {
        layerStatus = "";
    }
    var image        = "";
    var message = "";
    event  = "onclick='copyFileForm()'";
    image   = "";
    message = _(8178, "コピー");

    j$('#copy').empty();
    j$('#copy').append(
        "<a class='button button25 "+ layerStatus + "' " + event + ";>" +
        "<span class='button-inner'>" +
        "<span id='no-icon' class='btnin icon " + image + "'>" + message + "</span></span>" +
        "</a>"
    );
}

// チェックボックス変更時のファイル移動ボタン表示 TODO
function changeCheckboxModifyMoveButton(memberKey) {
    var layerStatus  = "";
    var event  = "'";
    if (memberKey) {
        layerStatus = "";
    }
    var image        = "";
    var message = "";

    event  = "onclick='moveFileForm()'";
    image   = "";
    message = _(8179, "移動");

    j$('#move').empty();
    j$('#move').empty().append(
        "<a class='button button25 "+ layerStatus + "' " + event + ";>" +
        "<span class='button-inner'>" +
        "<span id='no-icon' class='btnin icon " + image + "'>" + message + "</span></span>" +
        "</a>"
    );
}

function changeAllCheckbox(clearStatus) {
    if (0 == j$("input:checkbox").length) {
        return;
    }
    j$('#all_check').empty();
    if (clearStatus || (j$("input:checkbox:checked").val() && 0 < j$("input:checkbox:checked").val().length)) {
        j$(":checked").each(function(){
            this.checked = false;
        });

        j$('#all_check').append("<img src='/shared/images/checkbox.gif' alt='チェック' />");
        j$('#toolbutton').hide();
        return;
    }

    j$("input:checkbox:not(:checked)").each(function(){
        this.checked = true;
    });

    j$('#all_check').append("<img src='/shared/images/checked.gif' alt='チェック' />");
    j$('#toolbutton').show();
}

function changeNextRowButton(nextRow) {
    if (!nextRow) {
        return j$('#nextrow').hide();
    }
    j$('#nextrow').show();
}

// ファイル名変更画面 TODO
function modifyName() {
    var storageId;
    var territory;
    var exit = false;
    j$("input:checkbox:checked").each(function(){
        if (exit) {
            return;
        }
        if (1 < length) {
            exit = true;
            return alert(_(8180, "チェックボックスが複数選択されています。"));
        }
        sortKey = j$(this).val() + ", ";
        storageId = j$("#storage_id" + sortKey).val();
        territory = j$("#storage_territory" + sortKey).val();
    });
    if (exit) {
        return;
    }
    if(is_ie == 6 || is_ie == 7){
        var link = document.createElement("a");
	    link.setAttribute("href", "?action_modifyName=&storage_key=" + storageId
		        + "&sort_key=" + sortKey + "&territory=" + territory
		        + "&offset=" + parseInt(j$("input:checkbox").length -1));
        window.open(link, 'mywindow2', 'width=440, height=260, menubar=no, toolbar=no, scrollbars=yes');
    }else{
        var link = document.createElement("a");
	    link.setAttribute("href", "?action_modifyName=&storage_key=" + storageId
	        + "&sort_key=" + sortKey + "&territory=" + territory
	        + "&offset=" + parseInt(j$("input:checkbox").length -1));
	    link.setAttribute("rel", "shadowbox;width=440;height=200");
	    Shadowbox.open(link);
    }
}

// ファイル名変更TODO
function modifyNameComplate() {
    var sortKey =  j$("input[name=sort_key]").val();
    var storageKey =  j$("input[name=storage_key]").val();
    var storageName =  j$("input[name=storage_name]").val();
    var offset = j$("input[name=offset]").val();
    var account = {
            "userKey" : j$("input[name=user_key]").val(),
            "memberKey" : j$("input[name=member_key]").val()
        }
    var territory = j$("input[name=territory]").val();

    j$.getJSON(
        "/services/storage/index.php?action_modifyNameComplete=",
        {
            "storage_name" : storageName,
            "storage_key"  : storageKey,
            "territory"    : territory,
            "offset"       : offset,
            "ts"           : new Date().getTime()
        },
        function (data, status) {
            if(data.error){
            	alert(_(8181, "編集に失敗しました。"));
                if(is_ie == 6 || is_ie == 7){
                	window.close();
                	return;
                }
                return ;
            }else{
            	alert(_(8182, "変更しました"));
            }
            if(is_ie == 6 || is_ie == 7){
            	window.close();
            	window.opener.location.reload();
            	return;
            }
            parent.j$("#file_name" +storageKey ).empty().text(data.user_file_name);
            parent.j$("#storage_datetime" +storageKey ).empty().html(data.update_datetime);
            parent.Shadowbox.close();
        }
    );
}

// キーワード検索 TODO
function searchKeyword() {
    j$.getJSON(
        "/services/storage/index.php?action_searchList=",
        {
            "searchKeyword"   : j$("input[name=input-docs-search]").val(),
            "sort_key"     : j$("input[name=sort_key]").val(),
            "sort_type"     : j$("input[name=sort_type]").val(),
            "ts"           : new Date().getTime()
        },
        function (data, status) {
            if(!data || "db" == data.error){
                return alert(_(8168, "一覧の絞込みに失敗しました。"));
            }
            j$("input[name=search_type]").val('keyword');
            var sortKey = null;
            j$('#listbody').empty();
            for(i = 0; i < data.list.length; i++){
                sortKey = 1;
                j$('#listbody').append(
                    outputListRow(sortKey, getAccount(), data.list[i], false)
                );
            }
            j$('#common_storage_size').empty().append(data.size.use_size + " / " + data.size.max_size +" MB ");
            //changeNextRowButton(data.next_row);
            changeCheckbox(getAccount().memberKey);
            resetClass();
            if (isMSIE) {
            	setTimeout("setbox()", 1000);
            } else {
            	setbox();
            }
        }
    );
}

// ソート TODO 修正中
function sortListRows(event,sortKey,sortType)
{
    //IE対応
    var target = (event.target) ? event.target : event.srcElement;
    var sortButton = j$(target);
    j$("input[name=sort_key]").val(sortKey);
    j$("input[name=sort_type]").val(sortType);
    //class swtich
    j$(".asc_on").toggleClass("asc_on asc");
    j$(".desc_on").toggleClass("desc_on desc");
    if(sortButton.hasClass("asc")){
        sortButton.toggleClass("asc_on asc");
    } else if (sortButton.hasClass("desc")) {
        sortButton.toggleClass("desc_on desc");
    }
    setList();

}

// 一覧を再表示させる TODO
function setList(){
	var search_type = j$("input[name=search_type]").val();
	if(search_type == "folder"){
        var user_key = $j("input[name=user_key]").val();
        var member_key = $j("input[name=member_key]").val();
        if(j$("input[name=private]").val() == 1){
        	getFileListinFolder(j$("input[name=folder]").val(), user_key, member_key );
        }else{
        	getFileListinFolder(j$("input[name=folder]").val(), user_key, '0' );
        }
	}else if(search_type == "keyword"){
		searchKeyword();
	}else{
		searchList(search_type, "nav");
	}
}

// TODO 修正中
function uploadComplate() {
    var sortKey  =  $j("input[name=sort_key]").val();
    var document = $j("input[type=file]").val();
    var format   = $j("input[name=format]:checked").val();
    var offset   = $j("input[name=offset]").val() + 1;
    var account = {
        "userKey" : $j("input[name=user_key]").val(),
        "memberKey" : $j("input[name=member_key]").val()
    }
    $j('#upload_file').upload('/services/storage/index.php?action_uploadComplete=',
        {
            "format": format,
            "offset": offset,
            "ts"           : new Date().getTime()
        },
        function(data) {
        if("size over" == data.error){
        	alert(_(8183, "ストレージの容量がオーバーしました。"));
        	parent.Shadowbox.close();
        	if(is_ie == 6 || is_ie == 7){
        		window.close();
        	}
            return ;
        } else if ("format error" == data.error){
        	alert(_(8184, "対応していないファイルです。"));
        	parent.Shadowbox.close();
        	if(is_ie == 6 || is_ie == 7){
        		window.close();
        	}
        	return ;
        } else if("size error" == data.error){
        	alert(_(8185, "ファイルサイズがオーバーしています。"));
        	parent.Shadowbox.close();
        	if(is_ie == 6 || is_ie == 7){
        		window.close();
        	}
        	return;
        } else if(data.error) {
        	alert(_(8186, "アップロードに失敗しました。"));
        	parent.Shadowbox.close();
        	if(is_ie == 6 || is_ie == 7){
        		window.close();
        	}
            return ;
        }
        if (null != data) {
        	var lists = data.list;
        	var size = data.size;

            var account = getAccount();
            parent.j$('#listbody').empty();
            var sortKey = 1;
            if(data.account.member_key != 0){
            	parent.getFileListinFolder(0, data.account.user_key, data.account.member_key );
            }else{
            	parent.getFileListinFolder("00", data.account.user_key, data.account.member_key );
            }
            parent.j$('#common_storage_size').empty().append(size.use_size + " / " + size.max_size +" MB ");
            alert(_(8187, "アップロードしました。"));
            if(is_ie == 6 || is_ie == 7){
            	/*
            	window.opener.j$('#common_storage_size').empty().append(size.use_size + " / " + size.max_size +" MB ");
            	window.opener.j$('#listbody').empty();
            	var sortKey = 1;
                for(i = 0; i < lists.length; i++){
                	window.opener.j$('#listbody').append(outputListRow(sortKey, account, lists[i], false));
                }
                */
                window.opener.location.reload();
                window.close();
            }

        }
        parent.Shadowbox.clearCache();
        parent.Shadowbox.setup();
        parent.Shadowbox.close();
        },'json'
    );
}

function openLogPlayer(storageId, storageFileExtension)
{
    if ("swf" == storageFileExtension) {
        alert("swf");
        // 議事録再生
        openLastLogPlayer(storageId);
    }
}

function openLastLogPlayer(id) {
        var url = location.protocol;
        url = url + '//' + location.host;
        url = url + '/services/core/meeting_log/playStorageLog.php?action_lastlogplayer&id=' + id;
        MYWINDOW = window.open( url, 'VrmsLastLogPlayerWindow', 'width=593, height=600, resizable=yes, location=no, menubar=no, scrollbars=no, status=no, toolbar=no' );
        MYWINDOW.moveTo( (screen.availWidth-593) / 2, (screen.availHeight-600)/2 );
}

// TODO フォルダ内ファイル取得
function getFileListinFolder(folder_key,user_key,member_key){
	select_folder_id = folder_key;
	select_folder_member_key = member_key;
    j$.getJSON(
            "/services/storage/index.php?action_get_file_in_folder=",
            {
                "folder_key" : folder_key,
                "user_key"  : user_key,
                "member_key"    : member_key,
                "sort_key"     : j$("input[name=sort_key]").val(),
                "sort_type"     : j$("input[name=sort_type]").val(),
                "ts"           : new Date().getTime()
            },
            function (data, status) {
                if(data.error){
                    return alert(_(8168, "一覧の絞込みに失敗しました。"));
                }
                if (null != data) {
                	j$("input[name=search_type]").val("folder");
                	j$("input[name=folder]").val(folder_key);
                	//console.log(data);
                    //alert(data.list[1].file_name);
                    var account = getAccount();
                    j$('#listbody').empty();
                    var sortKey = 1;
                    for(var c = 0; c < data.folders.length; c++){
                    	j$('#listbody').append(outputFolderListRow(account, data.folders[c], false));
                    }
                    for(var i = 0; i < data.list.length; i++){
                        j$('#listbody').append(outputListRow(sortKey, account, data.list[i], false));
                    }
                    j$('#common_storage_size').empty().append(data.size.use_size + " / " + data.size.max_size +" MB ");
                    resetClass();
                    if(member_key == 0){
                        j$("input[name=private]").val("0");
                        j$('#common_dsp' + folder_key).addClass('active_type');
                    }else{
                        j$("input[name=private]").val("1");
                        j$('#private_dsp' + folder_key).addClass('active_type');
                    }
                    select_folder_id = folder_key;
                }
                changeCheckbox(getAccount().memberKey);
                if (isMSIE) {
                	setTimeout("setbox()", 1000);
                } else {
                	setbox();
                }
				if(treeObj != undefined){
                    treeObj.showHideNode(false , "node"+folder_key);
				}
            }
        );
}

function inputClear(){
	j$('input-docs-search').val("");
}

function resetClass(){
	j$("#all_dsp").removeClass("active_type");
	j$("#favorite_dsp").removeClass("active_type");
	j$("#owner_dsp").removeClass("active_type");
	j$("#private_dsp").removeClass("active_type");
	j$("#common_dsp").removeClass("active_type");
	j$(".active_type").removeClass("active_type");
}



// TODO ----4930

function addFolderComplate(parent_folder , member_key , folder_name , user_key){
    j$.getJSON(
            "/services/storage/index.php?action_addCompleteFolder=",
            {
                "parent_folder"  : parent_folder,
                "member_key"     : member_key,
                "folder_name"    : folder_name,
                "user_key"       : user_key,
                "ts"           : new Date().getTime()
            },
            function (data, status) {
                if(data.error == -1){
                    alert(_(8662, "4階層まで作成可能です"));
                    parent.Shadowbox.clearCache();
                    parent.Shadowbox.setup();
                    parent.Shadowbox.close();
                    if(is_ie == 6 || is_ie == 7){
                    	window.close();
                    }
                    return;
                }else if (data.error == -2){
                    alert(_(8663, "50個まで作成可能です"));
                    parent.Shadowbox.clearCache();
                    parent.Shadowbox.setup();
                    parent.Shadowbox.close();
                    if(is_ie == 6 || is_ie == 7){
                    	window.close();
                    }
                    return;
                }else if (data.error){
                    alert(_(8664, "フォルダーの作成に失敗しました:" + data.error));
                    parent.Shadowbox.clearCache();
                    parent.Shadowbox.setup();
                    parent.Shadowbox.close();
                    if(is_ie == 6 || is_ie == 7){
                    	window.close();
                    }
                    return ;
                }
                if(is_ie == 6 || is_ie == 7){
                	alert(_(8665, "フォルダーが作成されました"));
                	window.close();
                	window.opener.location.reload();
                	return;
                }
            	var search_type = parent.j$("input[name=search_type]").val();
            	if(search_type == "folder"){
            		parent.setFloderTree(true , parent.j$("input[name=folder]").val());
            	}else{
            		parent.setFloderTree(true);
            	}
            	if(parent.j$("input[name=folder]").val() == data.parent_storage_folder.storage_folder_key){
            		parent.getFileListinFolder(data.parent_storage_folder.storage_folder_key, data.parent_storage_folder.user_key, data.parent_storage_folder.member_key );
            	}
            	alert(_(8665, "フォルダーが作成されました"));
                parent.Shadowbox.clearCache();
                parent.Shadowbox.setup();
                parent.Shadowbox.close();
            }
        );
}


//TODO 4930
function addFolderform(parent_folder , is_private){
    j$(function($){
    	// IE対策
    	if(is_ie == 6 || is_ie == 7){
	        var link = document.createElement("a");
	        link.setAttribute("href", "?action_addFolder=&parent_folder=0" + parent_folder + "&is_private=" + is_private + "&ts=" + new Date().getTime());
	        window.open(link, 'mywindow2', 'width=440, height=260, menubar=no, toolbar=no, scrollbars=yes');
    	}
    	else{
	        var link = document.createElement("a");
	        link.setAttribute("href", "?action_addFolder=&parent_folder=0" + parent_folder + "&is_private=" + is_private + "&ts=" + new Date().getTime());
	        link.setAttribute("rel", "shadowbox;width=440;height=260");
	        Shadowbox.open(link);
    	}
    });
}


function viewFolderMenu(){
	 $j('.folder_menu').each(function(){
		    var folder_key = $j(this).attr('folder');
	    	var text_html = '<ul class="tree-menu"><li><a href="javascript:void(0)" onClick="addFolderform(\''+ $j(this).attr('folder')+ '\' , \''+ $j(this).attr('member')+ '\')">' + _(8681, "新規作成") + '</a></li></ul>';
		    if(folder_key != 0){
		    	text_html = '<ul class="tree-menu"><li><a href="javascript:void(0)" onClick="addFolderform(\''+ $j(this).attr('folder')+ '\', \''+ $j(this).attr('member')+ '\')">'+_(8681, "新規作成")+'</a></li><li><a href="javascript:void(0)" onClick="updataNameFolderform(\''+ $j(this).attr('folder')+ '\')">' + _(8682, "名前の変更") +'</a></li><li><a href="javascript:void(0)" onClick="moveFolderForm(\''+ $j(this).attr('folder')+ '\')">' + _(8179, "移動") +'</a></li><li><a href="javascript:void(0)" onClick="confirmDeleteFolder(\''+ $j(this).attr('folder')+ '\')">' + _(8683, "削除") +'</a></li></ul>';
		    }
		    var tip_obj = j$(this).qtip(
				      {
				         content: {
				            text: text_html
				         },
				         position: {
				            corner: {
				               target: 'rightMiddle',
				               tooltip: 'leftMiddle'
				            },
				            adjust: {
				               screen: false
				            }
				         },
				         show: {
				            when: 'click',
				            solo: false
				         },
				         hide: { when:"mouseout", fixed:true },
		                 style: {
				            tip: false,
				            border: {
				               width: 0,
				               radius: 0
				            },
				            name: 'main'
				         }
				      });
		    var tip_api = tip_obj.qtip("api");
	 });
}

//TODO 4930
function setFloderTree(openFlag , active_folder){
	if(active_folder === undefined) active_folder = null;
    j$.getJSON(
            "/services/storage/index.php?action_getFolderTree=",
            {
                "ts"           : new Date().getTime()
            },
            function (data, status) {
                if(data.error){
                    return alert(_(8666, "フォルダー一覧取得に失敗しました"));
                }
//console.log(data);
                var account = getAccount();
                j$('#dhtmlgoodies_tree2').empty();
                var html = "";
                var i = 0;
                if(data.private_folder_tree != 1){
                    private_folder_length = data.private_folder_tree.length
                    html = html + '<li id="node0" noDrag="true" noSiblings="true" noDelete="true" noRename="true"><div><a href="#"></a><a href="javascript:void(0)"><span onClick="getFileListinFolder(\'0\', \''+account.userKey+'\', \''+account.memberKey+'\' );"></span>';
                	if(j$("input[name=select_file]").val() == undefined){
                		html = html +'<span id="folder_menu0" class="folder_menu" folder="0" member=\''+account.memberKey+'\'><img src="/shared/images/btn_desc.gif"></span>';
                	}

                    html = html + '<span id="private_dsp0" class="folder" onClick="getFileListinFolder(\'0\', \''+account.userKey+'\', \''+account.memberKey+'\' );">' + _(8667, "個人ストレージ") + '</span></a></div><ul>';
                    for(i = 0; i < private_folder_length; i++){
	                	html = html + outputFolderRow(data.private_folder_tree[i],"private_dsp");
	                }
                    html = html + "</ul>";
                    html = html + "</li>";
                }
                if(data.shared_folder_tree != 1){
                	shared_folder_length = data.shared_folder_tree.length
                	html = html + '<li id="node00" noDrag="true" noSiblings="true" noDelete="true" noRename="true"><div><a href="#"></a><a href="javascript:void(0)"><span onClick="getFileListinFolder(\'00\', \''+account.userKey+'\', \'0\' );"></span>';
                	if(j$("input[name=select_file]").val() == undefined){
                		html = html +'<span id="folder_menu00" class="folder_menu" folder="0" member="0"><img src="/shared/images/btn_desc.gif"></span>';
                	}
                	html = html + '<span id="common_dsp00" class="folder" onClick="getFileListinFolder(\'00\', \''+account.userKey+'\', \'0\' );">' + _(8668, "共有ストレージ") + '</span></a></div><ul>'
                	for(i = 0; i < shared_folder_length; i++){
                		html = html + outputFolderRow(data.shared_folder_tree[i] ,"common_dsp" );
	                }
                	html = html + "</ul>"
	                html = html + "</li>"
                }

                j$('#dhtmlgoodies_tree2').html(html);
                j$('#private_dsp' +active_folder).addClass('active_type');
                j$('#common_dsp' +active_folder).addClass('active_type');
            	treeObj = new JSDragDropTree();
            	treeObj.setTreeId('dhtmlgoodies_tree2');
            	treeObj.setImageFolder('../../shared/images/storage_folder/')
            	if(!openFlag){
            		treeObj.Set_Cookie('dhtmlgoodies_expandedNodes',"",-50);
            	}
            	treeObj.initTree(openFlag);
            	if(!openFlag){
            		treeObj.showHideNode(false , "node0")
            		treeObj.showHideNode(false , "node00")
            	}
            	viewFolderMenu();
            	//treeObj.showHideNode(false , "node-1")
            }
        );
}

//TODO 4930
function outputFolderRow(folder , folder_type){
	var row = "";
	row  = row + '<li id="node'+folder.storage_folder_key +'" noDrag="true" noSiblings="true" noDelete="true" noRename="true"><div><a href="#"></a><a href="javascript:void(0)"><span onClick="getFileListinFolder(\''+folder.storage_folder_key +'\', \''+folder.user_key +'\', \''+folder.member_key +'\' );"></span>';
	if(j$("input[name=select_file]").val() == undefined){
		row = row +'<span class="folder_menu" folder=\''+folder.storage_folder_key+'\' member=\''+folder.member_key+'\'><img src="/shared/images/btn_desc.gif"></span>';
	}
	row = row + '<span id="' + folder_type + folder.storage_folder_key + '" class="folder" onClick="getFileListinFolder(\''+folder.storage_folder_key +'\', \''+folder.user_key +'\', \''+folder.member_key +'\' );">' + folder.folder_name + '</span></a></div>';
	if(folder.kids){
		var folder_length = folder.kids.length;
		row = row + "<ul>"
		for(var z = 0; z < folder_length; z++){
			row = row + outputFolderRow(folder.kids[z] , folder_type);
		}
		row = row + "</ul>"
	}
	return row;
}

//TODO 4930
function setSelectFloderTree(){
    j$.getJSON(
            "/services/storage/index.php?action_getFolderTree=",
            {
                "ts"           : new Date().getTime()
            },
            function (data, status) {
                if(data.error){
                    return alert(_(8666, "フォルダ一覧取得に失敗しました。"));
                }
                var account = getAccount();
                j$('#dhtmlgoodies_tree2').empty();
                html = "";
                if(data.private_folder_tree != 1){
                    private_folder_length = data.private_folder_tree.length
                    html = html + '<li id="node0" noDrag="true" noSiblings="true" noDelete="true" noRename="true"><div><a href="#"></a><a href="javascript:void(0)"><span id="private_dsp0" class="folder" onClick="selectFolder(\'0\', \''+account.userKey+'\', \''+account.memberKey+'\' );">' + _(8667, "個人ストレージ") + '</span></a></div>';
                    html = html + "<ul>"
                    for(i = 0; i < private_folder_length; i++){
	                	html = html + outputSelectFolderRow(data.private_folder_tree[i],"private_dsp");
	                }
                    html = html + "</ul>"
                    html = html + "</li>"
                }
                if(data.shared_folder_tree != 1){
                	shared_folder_length = data.shared_folder_tree.length
                	html = html + '<li id="node00" noDrag="true" noSiblings="true" noDelete="true" noRename="true"><div><a href="#"></a><a href="javascript:void(0)"><span id="common_dsp0" class="folder" onClick="selectFolder(\'0\', \''+account.userKey+'\', \'0\' );">' + _(8668, "共有ストレージ") + '</span></a></div>';
                	html = html + "<ul>"
                	for(i = 0; i < shared_folder_length; i++){
                		html = html + outputSelectFolderRow(data.shared_folder_tree[i] ,"common_dsp" );
	                }
                	html = html + "</ul>"
	                html = html + "</li>"
                }

                j$('#dhtmlgoodies_tree2').html(html);
            	treeObj = new JSDragDropTree();
            	treeObj.setTreeId('dhtmlgoodies_tree2');
            	treeObj.setImageFolder('../../shared/images/storage_folder/')
            	treeObj.initTree();
            }
        );
}

//TODO 4930
function outputSelectFolderRow(folder , folder_type){
	var row = "";
	row  = row + '<li id="node'+folder.storage_folder_key +'" noDrag="true" noSiblings="true" noDelete="true" noRename="true"><div><a href="#"></a><a href="javascript:void(0)"><span id="' + folder_type + folder.storage_folder_key + '" class="folder" onClick="selectFolder(\''+folder.storage_folder_key +'\', \''+folder.user_key +'\', \''+folder.member_key +'\' );">' + folder.folder_name + '</span></a></div>';
//console.log(folder.folder_name);
	if(folder.kids){
		var folder_length = folder.kids.length;
		row = row + "<ul>"
		for(var z = 0; z < folder_length; z++){
			row = row + outputSelectFolderRow(folder.kids[z],folder_type);
		}
		row = row + "</ul>"
	}
	return row;
}

//TODO 4930 別ウィンドウフォルダ選択用
function selectFolder(select_folder_key , folder_user_key , folder_member_key){
	select_folder = select_folder_key;
	select_folder_user_key = folder_user_key;
	select_folder_member_key = folder_member_key;
	j$(".active_type").removeClass("active_type");
    if(folder_member_key == 0){
        j$('#common_dsp' + select_folder_key).addClass('active_type');
    }else{
        j$('#private_dsp' + select_folder_key).addClass('active_type');
    }
}

//TODO 4930
function copyFolderForm(folder_key){

	if(folder_key == null){
		return alert(_(8162, "対象のフォルダが見つかりません。"));
	}
    var link = document.createElement("a");
    link.setAttribute("href", "?action_copyFolderForm=&folder_key=" +folder_key+ "&ts=" + new Date().getTime());
    link.setAttribute("rel", "shadowbox;width=440;height=260");
    Shadowbox.open(link);
}

function copyFolder(toMemberKey , toFolder , folder_key){
    j$.getJSON(
            "/services/storage/index.php?action_copyFolder=",
            {
                "folder_key"  : folder_key,
                "offset"      : parseInt(j$("input:checkbox").length),
                "toMemberKey" : toMemberKey,
                "toFolder"    : toFolder,
                "ts"           : new Date().getTime()
            },
            function (data, status) {
                var error = null;
                if (!data || data.error) {
                    return alert(_(111111, "フォルダコピーに失敗しました。")+ data.error);
                }

                parent.setFloderTree(true);
                alert(_(111111, "フォルダコピーに成功しました。"));
                parent.changeAllCheckbox(true);
                parent.Shadowbox.clearCache();
                parent.Shadowbox.setup();
                parent.Shadowbox.close();
                /*
                for (i = 0; i < data.copy_error.length; i++) {
                    error = data.copy_error[i];
                    name = error.name;
                    message = error.message;
                    if("duplicate" == message){
                        alert(_(10005, "コピー先ディレクトリに%sがある為、コピーできません。", name));
                    } else if("size over" == message){
                        alert(_(10002, "%sでストレージの容量がオーバーしました。", name));
                    } else if("old file notfound" == message){
                        alert(_(10003, "%sのコピー元ファイルが見つかりません。", name));
                    } else if("old dir notfound" == message){
                        alert(_(10004, "%sのコピー元ディレクトリが見つかりません。", name));
                    } else if("new file notfound" == message || "new dir notfound" == message) {
                        alert(_(10007, "%sのコピーに失敗しました。", name));
                    }
                }
                if (rowCount == data.copy_error.length) {
                    return
                }
                */

            }
        );
}

//TODO 4930
function moveFolderForm(folder_key){

	if(folder_key == null){
		return alert(_(8162, "対象のフォルダが見つかりません。"));
	}
	if(is_ie == 6 || is_ie == 7){
        var link = document.createElement("a");
	    link.setAttribute("href", "?action_moveFolderForm=&folder_key=" +folder_key+ "&ts=" + new Date().getTime());
        window.open(link, 'mywindow2', 'width=440, height=260, menubar=no, toolbar=no, scrollbars=yes');
	}else{
	    var link = document.createElement("a");
	    link.setAttribute("href", "?action_moveFolderForm=&folder_key=" +folder_key+ "&ts=" + new Date().getTime());
	    link.setAttribute("rel", "shadowbox;width=440;height=260");
	    Shadowbox.open(link);
    }
}

function moveFolder(toMemberKey , toFolder , folder_key){
//console.log(toFolder);
    j$.getJSON(
            "/services/storage/index.php?action_moveFolder=",
            {
                "folder_key"  : folder_key,
                "offset"      : parseInt(j$("input:checkbox").length),
                "toMemberKey" : toMemberKey,
                "toFolder"    : toFolder,
                "ts"           : new Date().getTime()
            },
            function (data, status) {
                var error = null;
                if (data.error == -1) {
                	alert(_(8669, "フォルダーの移動に失敗しました"));
                    if(is_ie == 6 || is_ie == 7){
                    	window.close();
                    }
                	parent.Shadowbox.close();
                    return;
                }else if(data.error == -2){
                	alert(_(8662, "4階層まで作成可能です"));
                    if(is_ie == 6 || is_ie == 7){
                    	window.close();
                    }
                	parent.Shadowbox.close();
                	return ;
                }else if(data.error == -3){
                	alert(_(8672, "移動先が不正です"));
                    if(is_ie == 6 || is_ie == 7){
                    	window.close();
                    }
                	parent.Shadowbox.close();
                	return
                }


                if(is_ie == 6 || is_ie == 7){
                	alert(_(8673, "フォルダーを移動しました"));
                	window.close();
                	window.opener.location.reload();
                	return;
                }
            	var search_type = parent.j$("input[name=search_type]").val();
            	if(search_type == "folder"){
            		parent.setFloderTree(true , parent.j$("input[name=folder]").val());
            	}else{
            		parent.setFloderTree(true);
            	}
            	if(parent.j$("input[name=folder]").val() == data.folder_info.parent_storage_folder_key){
            		parent.getFileListinFolder(parent.j$("input[name=folder]").val(), data.folder_info.user_key, data.folder_info.member_key );
            	}else if(parent.j$("input[name=folder]").val() == data.to_folder_info.storage_folder_key){
               		parent.getFileListinFolder(parent.j$("input[name=folder]").val(), data.to_folder_info.user_key, data.to_folder_info.member_key );
            	}
            	alert(_(8673, "フォルダーを移動しました"));
                parent.Shadowbox.clearCache();
                parent.Shadowbox.setup();
                parent.Shadowbox.close();
            }
        );
}


// TODO 4930
function moveFileForm(){
    var storageIds = {};
    var sortKey    = null;
    var setStatus  = false;
    var rowCount   = 0;
    j$("input:checkbox:checked").each(function(){
        sortKey = j$(this).val();
        storageIds[sortKey] = j$("#storage_id" + sortKey).val();
        setStatus = true;
//console.log(j$("#storage_id" + sortKey).val());
        rowCount++;
    });
    if (!setStatus) {
        return alert(_(8162, "対象のファイルが見つかりません。"));
    }
    if(is_ie == 6 || is_ie == 7){
        var link = document.createElement("a");
        link.setAttribute("href", "?action_moveFileForm=&ts=" + new Date().getTime());
        window.open(link, 'mywindow2', 'width=440, height=260, menubar=no, toolbar=no, scrollbars=yes');
    }else{
	    var link = document.createElement("a");
	    link.setAttribute("href", "?action_moveFileForm=&ts=" + new Date().getTime());
	    link.setAttribute("rel", "shadowbox;width=440;height=260");
	    Shadowbox.open(link);
    }
}

/**
 * ファイル移動 TODO
 *
 */
function moveFile(toMemberKey, toFolder , storageIds) {
    var sortKey    = null;
    var setStatus  = false;
    var rowCount   = 0;

    j$.getJSON(
            "/services/storage/index.php?action_move=",
            {
                "storage_ids" : storageIds,
                "offset"      : parseInt(j$("input:checkbox").length),
                "toMemberKey" : toMemberKey,
                "toFolder"    : toFolder,
                "ts"           : new Date().getTime()
            },
            function (data, status) {
                var error = null;
                if (!data || data.error) {
                	alert(_(8163, "ファイル移動に失敗しました。")+ data.error);
                    if(is_ie == 6 || is_ie == 7){
                    	window.close();
                    }
                	parent.Shadowbox.close();
                    return ;
                }

                parent.setList();
                alert(_(8164, "ファイル移動に成功しました。"));
                if(is_ie == 6 || is_ie == 7){
                	window.close();
                	window.opener.location.reload();
                	return;
                }
                parent.Shadowbox.clearCache();
                parent.Shadowbox.setup();
                parent.Shadowbox.close();
                /*
                for (i = 0; i < data.copy_error.length; i++) {
                    error = data.copy_error[i];
                    name = error.name;
                    message = error.message;
                    if("duplicate" == message){
                        alert(_(10005, "コピー先ディレクトリに%sがある為、コピーできません。", name));
                    } else if("size over" == message){
                        alert(_(10002, "%sでストレージの容量がオーバーしました。", name));
                    } else if("old file notfound" == message){
                        alert(_(10003, "%sのコピー元ファイルが見つかりません。", name));
                    } else if("old dir notfound" == message){
                        alert(_(10004, "%sのコピー元ディレクトリが見つかりません。", name));
                    } else if("new file notfound" == message || "new dir notfound" == message) {
                        alert(_(10007, "%sのコピーに失敗しました。", name));
                    }
                }
                if (rowCount == data.copy_error.length) {
                    return
                }
                */

            }
        );
}

//TODO 4930
function copyFileForm(){
    var storageIds = {};
    var sortKey    = null;
    var setStatus  = false;
    var rowCount   = 0;
    j$("input:checkbox:checked").each(function(){
        sortKey = j$(this).val();
        storageIds[sortKey] = j$("#storage_id" + sortKey).val();
        setStatus = true;
        rowCount++;
    });
    if (!setStatus) {
        return alert(_(8162, "対象のファイルが見つかりません。"));
    }
    if(is_ie == 6 || is_ie == 7){
        var link = document.createElement("a");
        link.setAttribute("href", "?action_copyFileForm=&&ts=" + new Date().getTime());
        window.open(link, 'mywindow2', 'width=440, height=260, menubar=no, toolbar=no, scrollbars=yes');
    }else{
	    var link = document.createElement("a");
	    link.setAttribute("href", "?action_copyFileForm=&&ts=" + new Date().getTime());
	    link.setAttribute("rel", "shadowbox;width=440;height=260");
	    Shadowbox.open(link);
    }
}

/**
 * ファイルコピー TODO
 * @returns
 */
function copyFile(toMemberKey, toFolder, storageIds) {


    j$.getJSON(
        "/services/storage/index.php?action_copy=",
        {
            "storage_ids" : storageIds,
            "offset"      : parseInt(j$("input:checkbox").length),
            "toMemberKey" : toMemberKey,
            "toFolder"    : toFolder,
            "ts"           : new Date().getTime()
        },
        function (data, status) {
            var error = null;
            if (!data || data.error) {
            	if(data.error == "size over"){
            		alert(_(8183, "ストレージの容量がオーバーしました。"));
                    if(is_ie == 6 || is_ie == 7){
                    	window.close();
                    	return;
                    }
            		parent.Shadowbox.close();
            		return
            	}
            	alert(_(8573, "ファイルのコピーに失敗しました。"  + data.error));
                if(is_ie == 6 || is_ie == 7){
                	window.close();
                	return;
                }
            	parent.Shadowbox.close();
                return;
            }

            parent.setList();
            alert(_(8572, "ファイルのコピーに成功しました。"));
            if(is_ie == 6 || is_ie == 7){
            	window.close();
            	window.opener.location.reload();
            	return;
            }
            parent.changeAllCheckbox(true);
            parent.Shadowbox.clearCache();
            parent.Shadowbox.setup();
            parent.Shadowbox.close();
/*
            for (i = 0; i < data.copy_error.length; i++) {
                error = data.copy_error[i];
                name = error.name;
                message = error.message;
                if("duplicate" == message){
                    alert(_(10005, "コピー先ディレクトリに%sがある為、コピーできません。", name));
                } else if("size over" == message){
                    alert(_(10002, "%sでストレージの容量がオーバーしました。", name));
                } else if("old file notfound" == message){
                    alert(_(10003, "%sのコピー元ファイルが見つかりません。", name));
                } else if("old dir notfound" == message){
                    alert(_(10004, "%sのコピー元ディレクトリが見つかりません。", name));
                } else if("new file notfound" == message || "new dir notfound" == message) {
                    alert(_(10007, "%sのコピーに失敗しました。", name));
                }
            }
            if (rowCount == data.copy_error.length) {
                return
            }
*/
            changeAllCheckbox(true);
        }
    );
}

function updataNameFolderform(folder_key){
    j$(function($){
    	// IE対策だが問題なさそう
    	if(is_ie == 6 || is_ie == 7){
	        var link = document.createElement("a");
	        link.setAttribute("href", "?action_addFolder=&folder_key=" + folder_key + "&ts=" + new Date().getTime());
	        window.open(link, 'mywindow2', 'width=440, height=260, menubar=no, toolbar=no, scrollbars=yes');
    	}
    	else{
	        var link = document.createElement("a");
	        link.setAttribute("href", "?action_addFolder=&folder_key=" + folder_key + "&ts=" + new Date().getTime());
	        link.setAttribute("rel", "shadowbox;width=440;height=260");
	        Shadowbox.open(link);
    	}
    });
}

function deleteFolder(folder_key){
    j$.getJSON(
            "/services/storage/index.php?action_deleteFolder=",
            {
                "folder_key"  : folder_key,
                "ts"           : new Date().getTime()
            },
            function (data, status) {
            	var account = getAccount();
                if(data.error){
                    alert(_(8674, "削除に失敗しました"));
                    return
                }
                setFloderTree(true);
            	var search_type = j$("input[name=search_type]").val();
            	if(search_type != "folder"){
            		return alert(_(8676, "削除しました"));
            	}

                if(j$("input[name=folder]").val() == folder_key){
                	getFileListinFolder(data.parent_storage_folder.storage_folder_key , data.parent_storage_folder.user_key , data.parent_storage_folder.member_key);
                }else{
                    if(j$("input[name=private]").val() == 1){
                    	getFileListinFolder(j$("input[name=folder]").val(), account.userKey, account.memberKey );
                    }else{
                    	getFileListinFolder(j$("input[name=folder]").val(), account.userKey, '0' );
                    }
                }
                alert(_(8676, "削除しました"));
            }
        );
}

function updataNameFolderComplate(folder_key , folder_name, member_key){
    j$.getJSON(
            "/services/storage/index.php?action_updataNameCompleteFolder=",
            {
                "folder_key"  : folder_key,
                "folder_name"    : folder_name,
                "ts"           : new Date().getTime()
            },
            function (data, status) {
                if(data.error){
                    alert(_(8677, "名前の変更に失敗しました"));
                    if(is_ie == 6 || is_ie == 7){
                    	window.close();
                    	return;
                    }
                    parent.Shadowbox.clearCache();
                    parent.Shadowbox.setup();
                    parent.Shadowbox.close();
                    return
                }
                if(is_ie == 6 || is_ie == 7){
                	alert(_(8679, "変更しました。"));
                	window.close();
                	window.opener.location.reload();
                	return;
                }
            	var search_type = parent.j$("input[name=search_type]").val();
            	if(search_type == "folder"){
            		parent.setFloderTree(true , parent.j$("input[name=folder]").val());
            	}else{
            		parent.setFloderTree(true);
            	}
            	if(parent.j$("input[name=folder]").val() == data.folder_info.parent_storage_folder_key){
            		parent.getFileListinFolder(parent.j$("input[name=folder]").val(), data.folder_info.user_key, data.folder_info.member_key );
            	}
                alert(_(8679, "変更しました。"));
                parent.Shadowbox.clearCache();
                parent.Shadowbox.setup();
                parent.Shadowbox.close();

            }
        );
}

function outputFolderListRow(account, folder_info, addRow){
	cell = '<tr><td>-</td><td>-</td><td><span  onclick="getFileListinFolder(\''+folder_info.storage_folder_key+'\', \''+folder_info.user_key+'\', \''+folder_info.member_key+'\' );"><img src="../../shared/images/storage_folder/dhtmlgoodies_folder.gif"> '+ folder_info.folder_name +'</span></td><td>folder</td><td></td>';

	if(j$("input[name=select_file]").val() == undefined){
		cell = cell + '<td>'+ folder_info.update_datetime +'</td>';
	}

	var memberKey = account["memberKey"];
	if (0 != memberKey && j$("input[name=select_file]").val() == undefined) {
		cell = cell + "<td></td>";
	}
	cell = cell + "</tr>";
	return cell;
}

