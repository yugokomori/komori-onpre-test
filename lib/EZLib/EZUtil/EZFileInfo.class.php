<?php
class EZ_FileInfo
{
    var $finfo = null;

    /**
     * コンストラクタ
     *
     * mime-magicデータベースをpear内に内包してしまいました。
     * Linuxとかでも同じデータベースでOK？
     */
    function __construct() {
        $mime_db = dirname(__FILE__).'/magic';
        $this->finfo = new finfo(FILEINFO_MIME, $mime_db);
        if (!$this->finfo) {
            trigger_error("mime-magic データベースのオープンに失敗しました", E_USER_ERROR);
        }
    }

    /**
     * 指定ファイルのMIME_TYPEを取得します
     */
    function get_type($file_path) {
        if (!file_exists($file_path)) {
            return false;
        }
        return $this->finfo->file($file_path);
    }

    function get_extension($file_path) {
        if (!file_exists($file_path)) {
            return false;
        }
        $mime_type = $this->finfo->file($file_path);
        switch ($mime_type) {
            // BMP
            case "image/bmp":
                return "bmp";
            // GIF
            case "image/gif":
                return "gif";
            // PNG
            case "image/png":
                return "png";
            // JPEG
            case "image/jpeg":
                return "jpg";
            // JPEG
            case "image/tiff":
                return "tif";
            // それ以外
            default :
                return false;
       }
    }

    /**
     * 指定ファイルをMIME_TYPEのヘッダ付きで出力します
     */
    function output($file_path) {
        if (!file_exists($file_path)) {
            return false;
        }
        $mime_type = $this->finfo->file($file_path);
        header("Content-Type: ".$mime_type.";");
        print file_get_contents($file_path);
    }
}
