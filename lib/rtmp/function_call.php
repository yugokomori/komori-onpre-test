<?php
############################################################################
#    Copyright (C) 2003/2004 by yannick connan                             #
#    yannick@dazzlebox.com                                                 #
#                                                                          #
#    This program is free software; you can redistribute it and#or modify  #
#    it under the terms of the GNU Library General Public License as       #
#    published by the Free Software Foundation; either version 2 of the    #
#    License, or (at your option) any later version.                       #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU Library General Public     #
#    License along with this program; if not, write to the                 #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################

require 'amf_marshal.php';

class FunctionCall {
    var $method     = NULL;
    var $request_id = NULL;
    var $args       = NULL;
    var $first_arg  = NULL;
        
    function FunctionCall($method,$request_id,$args) {
        $this->method = $method;
        $this->request_id = $request_id;
        $this->args = $args;
        $this->first_arg = NULL;
        var_dump(array($method,$request_id,$args));
    }
    
    function _amf_load ($t_data) {
        /*
        list ($method, $request_id, $first_arg, $args) = Marshal.load_array($t_data);
        */
    }
    
    function serialize() {
        global $Marshal;
        return $Marshal->dump_array(array($this->method,$this->request_id,$this->first_arg), $this->args);
    }
    
    function to_s() {
        $t_str = "method : ".$this->method."\n";
        $t_str .= "request_id : ".$this->request_id."\n";
        $t_str .= "arguments : \n".join($this->args,"\n")."\n";
    }
    
    // なんか暴挙なような気もするが･･･。
    function getVar($varname) {
        if (isset($this->$varname)) {
            return $this->$varname;
        }
    }
    
    function setVar($varname, $value) {
        if (isset($this->$varname)) {
            $this->$varname = $value;
        }
    }

}