<?php
require_once("classes/AppFrame.class.php");
require_once("classes/dbi/room.dbi.php");
require_once("classes/dbi/user.dbi.php");
require_once("classes/mgm/MGM_Auth.class.php");
require_once("lib/EZLib/EZUtil/EZLanguage.class.php");

class AppInboundIndex extends AppFrame
{
    var $room = null;
    var $_name_space = null;

    function default_view()
    {
        $this->action_show_top();
    }

    function init()
    {
        $this->_name_space = md5(__FILE__);
    }

    function action_show_top()
    {
        $inbound_id = $this->request->get("inbound_id");
        if ($inbound_id) {
            $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn());
            if ( ! $server_info = $obj_MGMClass->getRelationDsn( $inbound_id, "inbound_id" ) ){
                $this->display('user/404_error.t.html');
                return false;
            }
            require_once("classes/dbi/inbound.dbi.php");
            $obj_Inbound = new InboundTable( $this->get_dsn() );
            $where = "inbound_id = '".addslashes($inbound_id)."'";
            $inbound_info = $obj_Inbound->getRow($where);
            $this->logger->debug("inbound_info",__FILE__,__LINE__,$inbound_info);
            //DESIGN
            $inbound_design_info = $obj_Inbound->get_iframe_size($inbound_info["design_key"]);
            //部屋取得
            $obj_Room = new RoomTable( $this->get_dsn() );
            $where_room = "user_key = ".$inbound_info["user_key"].
                          " AND inbound_id = '".$inbound_id."'".
                          " AND inbound_flg = 1".
                          " AND room_status = 1";
            $room_count = $obj_Room->numRows($where_room);
            $this->template->assign("room_count", $room_count);
            $obj_User = new UserTable( $this->get_dsn() );
            $where_user = "user_key = ".$inbound_info["user_key"];
            $user_info = $obj_User->getRow($where_user, "user_id");
            $this->session->set("server_info", $server_info );
            $this->template->assign("serverDsnKey", $server_info["host_name"]);
            $this->template->assign('server', $this->config->get("N2MY","fms_server"));
            $this->template->assign("inbound_info", $inbound_info);
            $this->template->assign("user_id", $user_info["user_id"]);
            // テンプレートファイルのカスタマイズ
            $user_key = str_pad($inbound_info["user_key"], 3, "0", STR_PAD_LEFT);
            $design_key = str_pad($inbound_info["design_key"], 3, "0", STR_PAD_LEFT);
            //$lang = $inbound_info["lang"];
            $this->request->set("inbound_lang", $this->_lang);
            //$lang = EZLanguage::getLangCd($lang);
            $this->logger->debug("lang",__FILE__,__LINE__,$this->_lang);
            $this->template->assign("inbound_lang", $this->_lang);
            $this->template->assign("inbound_design_info", $inbound_design_info);
            //カスタマイズの場合は100
            if ("custom" == $inbound_info["design_type"]) {
                $template_file = $this->_check_template($this->_lang, $user_key);
            } else if ("original" == $inbound_info["design_type"]) {
                $image_info = unserialize($inbound_info["addition"]);
                $image_info["absence_image"]["dir"] .= "?".strtotime($inbound_info["update_datetime"]);
                $image_info["enter_image"]["dir"] .= "?".strtotime($inbound_info["update_datetime"]);
                $image_info["busy_image"]["dir"] .= "?".strtotime($inbound_info["update_datetime"]);
                $image_info["online_image"]["dir"] .= "?".strtotime($inbound_info["update_datetime"]);
                $this->logger->debug("image_info",__FILE__,__LINE__,$image_info);
                $this->template->assign("image_info", $image_info);
            }
            $template_file = "/inbound/inbound_index.t.html";
            $this->logger->debug("template_last",__FILE__,__LINE__,$template_file);

            $this->display($template_file);
        } else {
            $this->logger->error(__FUNCTION__."#inbound_key error", __FILE__, __LINE__, $inbound_id);
            $this->display('user/404_error.t.html');
            return false;
            exit;
        }
    }

    function get_random_roomkey($user_key, $inbound_id)
    {
        require_once ("classes/core/Core_Meeting.class.php");
        require_once("classes/dbi/user.dbi.php");
        $obj_User = new UserTable( $this->get_dsn() );
        $where_user = "user_key = ".$user_key.
                      " AND user_status = 1".
                      " AND user_delete_status = 0";
        $user_info = $obj_User->getRow($where_user);
        $obj_CoreMeeting = new Core_Meeting( $this->get_dsn() );
        $room_status = $obj_CoreMeeting->getMeetingStatusInbound( $user_info["user_id"], $inbound_id);
        require_once("classes/dbi/room.dbi.php");
        $obj_Room = new RoomTable($this->get_dsn());
        $where_room = "user_key = ".$user_info["user_key"].
                      " AND room_status = 1".
                      " AND reenter_flg = 1";
        $reenter_flg_rooms = $obj_Room->getRowsAssoc($where_room, array("room_key" => "asc"), null, null, "room_key", "room_key");
        $this->logger->debug("room_status",__FILE__,__LINE__,$reenter_flg_rooms);

        if ($room_status) {
            foreach($room_status as $room) {
                if ("1" == $room["status"] && "1" == $room["pcount"]) {
                    if (array_key_exists($room["room_key"], $reenter_flg_rooms)) {
                        $this->logger2->debug($room["room_key"]);
                        return $room["room_key"];
                    } else {
                        $room_list[] = $room["room_key"];
                    }
                }
            }
            return $room_list[0];
        }
        return false;
    }

    /**
     * 会議開始
     */
    function action_meeting_start() {
        $user_key = $this->request->get("user_id");
        $inbound_id = $this->request->get("inbound_id") ? $this->request->get("inbound_id") : $this->session->get("inbound_id");
        require_once("classes/dbi/inbound.dbi.php");
        $obj_Inbound = new InboundTable( $this->get_dsn() );
        $where = "inbound_id = '".addslashes($inbound_id)."'";
        $inbound_info = $obj_Inbound->getRow($where);
        if (!$inbound_info) {
            return false ;
        }
        $this->logger->info("user_key",__FILE__,__LINE__,$inbound_id);
        $room_key = $this->get_random_roomkey($inbound_info["user_key"], $inbound_id);
        $this->logger2->info($room_key);
        if (!$room_key) {
            $this->request->set("inbound_lang", $this->session->get("lang",$this->_name_space));
            $this->display('error.t.html');
            exit;
        }

        $narrow = $this->request->get("is_narrow");
        $type = "customer";   //$this->request->get("type");
        $country_key = $this->session->get("country_key");
        $login_type = "customer";
        $this->session->set("login_type", $login_type);
        $meeting_key = $this->request->get("meeting_key");

        require_once("classes/N2MY_Meeting.class.php");
        $obj_N2MY_Meeting = new N2MY_Meeting($this->get_dsn());
        // 最後に実行されたMeetingKeyを取得
        $obj_Room = new RoomTable( $this->get_dsn() );
        $where = "room_key = '".addslashes($room_key)."'";
        $room_info = $obj_Room->getRow($where);
        $room_info = $obj_Room->getRow("room_key = '".addslashes($room_key)."'");
        if (!$room_info) {
            $message = array(
                "title" => $this->get_message("MEETING_START", "using_title"),
                "text" => $this->get_message("MEETING_START", "using_text"),
                "back_url" => "javascript:window.close();",
                "back_url_label" => $this->get_message("MEETING_START", "using_back_url_label")
                );
            $this->logger2->debug($message);
            $this->template->assign("message", $message);
            return $this->display('outbound/error.t.html');
        }
        $obj_User = new UserTable($this->get_dsn());
        $user_info = $obj_User->getRow("user_key = ".$room_info["user_key"]);
        $this->session->set("user_info", $user_info);

        //地域情報
        $country_key = $this->request->get("country");
        if ($country_key == "auto") {
          $result_country_key = $this->_get_country_key($country_key);
          $country_key = $result_country_key;
        }

        if (!$country_key) {
          $country_key = $this->session->get("country_key");
        }

        // 地域が特定できない場合は利用可能な地域をデフォルトとする
        $country_list = $this->get_country_list();

        if (!array_key_exists($country_key, $country_list)) {
          $_country_data = array_shift($country_list);
          $country_key = $_country_data["country_key"];
        }
        $this->session->set("country_key", $country_key);
        // 会議キー
        $meeting_key = $this->request->get("meeting_key");
        // 会議情報取得
        $meeting_info = $obj_N2MY_Meeting->getMeeting($room_key, $login_type, $meeting_key);
        if ($meeting_info["reservation_session"] || !$meeting_info) {
            $reservationInfo = $this->obj_Reservation->getRow(sprintf("reservation_session='%s'", $meeting_info["reservation_session"]));
        }
        if (!$meeting_info) {
            $message = array(
                "title" => $this->get_message("MEETING_START", "using_title"),
                "text" => $this->get_message("MEETING_START", "using_text"),
                "back_url" => "javascript:window.close();",
                "back_url_label" => $this->get_message("MEETING_START", "using_back_url_label")
                );
            $this->logger2->debug($message);
            $this->template->assign("message", $message);
            return $this->display('inbound/error.t.html');
        }
        $meeting_key = $meeting_info["meeting_key"];

        // オプション指定
        $options = array(
            "meeting_name" => $meeting_info["meeting_name"],
            "user_key"     => $user_info["user_key"],
            "start_time"   => $meeting_info["start_time"],
            "end_time"     => $meeting_info["end_time"],
            "country_id"   => $country_key,
            "password"     => $meeting_info["meeting_password"]
        );
        // 現在の部屋のオプションで会議更新
        $core_session = $obj_N2MY_Meeting->createMeeting($room_key, $meeting_key, $options);
        // ユーザごとのオプションを取得
        $type        = "customer";
        // マルチカメラが無効の場合はtypeをnormalに変更

        // ホワイトボードアップロードメールアドレス
        $mail_upload_address = ($this->config->get("N2MY", "mail_wb_host")) ? $room_key."@".$this->config->get("N2MY", "mail_wb_host") : "";

        if ($inbound_info["customer_info_flg"] == 2) {
            $name = $inbound_info["customer_name"];
        } else {
            $name = "";
        }
        // モバイル判別
        if (strpos($_SERVER['HTTP_USER_AGENT'], "Android") || strpos($_SERVER['HTTP_USER_AGENT'],"iPhone") || strpos($_SERVER['HTTP_USER_AGENT'],"iPod") || strpos($_SERVER['HTTP_USER_AGENT'],"iPad")) {
            $base_url = parse_url(N2MY_BASE_URL);
            $referer = $_SERVER["HTTP_REFERER"];
            $url    = $this->config->get("N2MY","mobile_protcol_sales", 'vcube-sales')."://info?inbound_id=".$inbound_id."&entrypoint=".$base_url["host"]."&callbackurl=".urlencode($referer)."&name=".$name;
            $this->logger2->info($url);
            header("Location: ".$url);
            exit;
        }

        /*typeを増やした場合の影響範囲がはかり知れないので元にもどします。。*/
        if ($this->session->get("invitedGuest")) $login_type = "invite";
        $user_options = array(
            "user_key"  => $user_info["user_key"],
            "name"      => $name,
            "participant_email"         => $_COOKIE["personal_email_secure"],
            "participant_station"       => $_COOKIE["personal_station"],
            "narrow"    => $this->request->get("is_narrow"),
            "lang"      => $this->session->get("lang"),
            "skin_type" => $this->request->get("skin_type", ""),
            "mode"      => "",
            "role"      => $login_type,
            "mail_upload_address"       => $mail_upload_address,
            "account_model"       => $user_info["account_model"],
            );
        // 会議入室
        $meetingDetail = $obj_N2MY_Meeting->startMeeting($core_session, $type, $user_options);
        if ( !$meetingDetail ) {
            $message["title"] = MEETING_START_ERROR_TITLE;
            $message["body"] = MEETING_START_ERROR_BODY;
            return $this->render_valid($message);
        }
        $log_data = array(
            "user_key"               => $meetingDetail["user_key"],
            "room_key"               => $meetingDetail["room_key"],
            "meeting_key"            => $meetingDetail["meeting_key"],
            "participant_key"        => $meetingDetail["participant_key"],
            "participant_session_id" => $meetingDetail["participant_session_id"],
            "http_referer"           => $_SERVER["HTTP_REFERER"],
            "remote_addr"            => $_SERVER["REMOTE_ADDR"],
            "http_user_agent"        => $_SERVER["HTTP_USER_AGENT"],
            "inbound_id"             => $inbound_id,
            "event"                  => "onClick",
            "create_datetime"        => date("Y-m-d H:i:s"),
        );
        require_once('classes/dbi/inbound_log.dbi.php');
        $obj_InboundLog = new InboundLogTable( $this->get_dsn() );
        $obj_InboundLog->add($log_data);

        //リダイレクト先の core/htdocs/sercie/*　の処理を直接書いています。
        $this->session->set( "room_key", $room_key );
        $this->session->set( "type", $meetingDetail["participant_type_name"] );
        $this->session->set( "meeting_key", $meetingDetail["meeting_key"] );
        $this->session->set( "meeting_type", $type );
        $this->session->set( "participant_key", $meetingDetail["participant_key"] );
        $this->session->set( "participant_name", $name );
        $this->session->set( "mail_upload_address", $mail_upload_address );
        $this->session->set( "fl_ver", "as3" );
        $this->session->set( "display_size", "16to9" );
        $this->session->set( "meeting_version", $user_info["meeting_version"] );
        $this->session->set( "reload_type", 'normal');
        // 参加者情報
        $this->session->set( "user_agent_ticket", uniqid());
        // 入室時のログ
        $this->logger2->debug(array(
            "room_key" => $room_key,
            "type" => $meetingDetail["participant_type_name"],
            "meeting_key" => $meetingDetail["meeting_key"],
            "meeting_type" => $type,
            "participant_key" => $meetingDetail["participant_key"],
            "participant_name" => "",
            ));
        // 会議入室
        $redirect_url = N2MY_BASE_URL."inbound/?action_meeting_display";
        if (!$room_info["room_password"]) {
            header( "Location: ".$redirect_url);
        } else {
            $this->session->set("redirect_url", $redirect_url, $this->_name_space);
            // パスワード確認
            $params = array(
                "action_room_auth_login"    => "",
                "room_key" => $room_key,
                "ns"              => $this->_name_space
                );
            $url = $this->get_redirect_url("inbound", $params);
            $this->logger->info(__FUNCTION__."#end", __FILE__, __LINE__,$url);
            header("Location: ".$url);
        }
        $this->logger->debug(__FUNCTION__,__FILE__,__LINE__,array($room_key, $meeting_info));
        return true;
    }

    function action_meeting_display()
    {
        $this->render_meeting_view();
    }

    private function render_meeting_view()
    {
        $type = $this->session->get( "type" );
        $this->template->assign('charset', "UTF-8" );

        // 会議情報取得
        $user_info = $this->session->get("user_info");
        $meeting_key = $this->session->get("meeting_key");
        require_once( "classes/dbi/meeting.dbi.php" );
        $obj_Meetng = new MeetingTable($this->get_dsn());
        $where = "meeting_key = '".addslashes($meeting_key)."'" .
                " AND user_key = '".$user_info["user_key"]."'" .
                " AND is_active = 1" .
                " AND is_deleted = 0";
        $meeting_info = $obj_Meetng->getRow($where);
        $this->template->assign('meeting_info' , $meeting_info );
        $obj_Room = new RoomTable($this->get_dsn());
        $room_info = $obj_Room->getRow("room_key = '".addslashes($meeting_info['room_key'])."'");
        $this->template->assign("room_info" , $room_info);

        //Skinsの選択を追加
        $skinName = $user_info["user_logo"] ? "SkinsFuji" : "Skins";
        $this->template->assign('skinName' , $skinName );
        $this->template->assign('locale' , $this->get_language() );

        $meeting_version = $this->session->get("meeting_version");
        if ($meeting_version >= "4.7.0.0") {
           $version = "4.7.0.0";
        } else {
           $version = "4.6.5.0";
        }
        $this->template->assign('meeting_version' , $version );

        $content_delivery_base_url = $this->get_content_delivery_base_url();
        $this->template->assign("content_delivery_base_url", $content_delivery_base_url);

        // eco canceler
        $certificate = substr(md5(uniqid(rand(), true)), 1, 16);
        $oneTime = array(
                    "certificate"    =>    $certificate,
                    "createtime"    =>    time());
        $this->session->set("oneTime", $oneTime);
        $this->template->assign('certificateId', $certificate );
        $this->template->assign('apiUrl', '/services/api.php?action_issue_certificate');

        // UserAgent更新
        $this->template->assign('user_agent_ticket' , $this->session->get("user_agent_ticket") );
        // ディスプレイサイズ指定
        $this->template->assign('display_size', htmlspecialchars($this->session->get("display_size"), ENT_QUOTES));
        if ($this->session->get("fl_ver") == "as3") {
            $this->template->assign('meeting_type' , htmlspecialchars($this->session->get("meeting_type"),ENT_QUOTES) );
            $this->template->assign('participant_name' , $this->session->get("participant_name") );
            // V-Cube Meetingのバージョンが違う場合、キャッシュしたデータを使わせない。
            $this->template->assign('cachebuster' , $this->get_meeting_version());
            $this->display( sprintf( 'core/%s/meeting_as3.t.html', $type ) );
        } else {
            $this->display( sprintf( 'core/%s/meeting_base.t.html', $type ) );
        }
    }

    function action_room_auth_login ($message = "") {
        $this->logger->trace(__FUNCTION__."#start",__FILE__,__LINE__);
        $this->template->assign('message', $message);
        $this->template->assign('ns', $this->request->get("ns"));
        $room_key = $this->request->get("room_key");
        $this->template->assign('room_key', $room_key);
        $this->display('outbound/auth.t.html');
    }

    private function render_valid()
    {
        $this->display( 'outbound/error.t.html' );
    }

    private function get_content_delivery_base_url() {
        $protocol = '';
        $content_delivery_base_url = '';
        $country_key = $this->session->get("country_key");

        if($this->config->get("CONTENT_DELIVERY", "is_enabled", false)) {
            $country_key = $this->session->get("country_key");
            // 中国からの入室の場合、データの参照元をCDNからではなくBravからに設定する。
            if($country_key == "cn") {
                $content_delivery_host = $this->config->get("CONTENT_DELIVERY_HOST", "cn", "");
                if(strlen($content_delivery_host)) {
                    $protocol = isset($_SERVER["HTTPS"]) ? 'https://' : 'http://';
                }
                $content_delivery_base_url = $protocol . $content_delivery_host;
                $meeting_file_dir = $this->config->get("CONTENT_DELIVERY", "meeting_file_dir", "");
                if(strlen($meeting_file_dir)) {
                    $content_delivery_base_url .= '/' . $meeting_file_dir;
                }
            }
        }
        return $content_delivery_base_url;
    }

    private function get_meeting_version() {
        $version_file = N2MY_APP_DIR."version.dat";
        if ($fp = fopen($version_file, "r")) {
            $version = trim(fgets($fp));
        }
        return $version;
    }

    //カスタマイズテンプレートファイルの有無を判別
    function _check_template($lang, $user_key, $type)
    {
        $template_dir = $this->template->template_dir."/".$lang."/";
        $template_file = "/inbound/".$user_key."/index.t.html";
        error_log($template_file);
        if (!file_exists($template_dir.$template_file)) {
            return false;
        }
        return $template_file;
    }
}

$main = new AppInboundIndex();
$main->execute();
