<?php
require_once("classes/AppFrame.class.php");
require_once("lib/pear/Auth.php");

define("BASE_DIR", realpath(dirname(__FILE__)."/../../"));

class FileUploader extends AppFrame {
    var $data = null;

    function init() {
        $this->data = parse_ini_file("up.ini", true);
    }

    // 社員以外使えないようにしておく
    function auth() {
        /*
        $auth_detail = "*";
        $this->_auth = new Auth("DB", array(
            "dsn" => $this->get_dsn(),
            "table" => "user",
            "cryptType" => "",
            "db_fields" => $auth_detail,
            "usernamecol" => "user_id",
            "passwordcol" => "user_password"
            ));
        //$this->_auth->setAdvancedSecurity();
        if (!$this->_auth->getAuth()) {
            $this->_auth->start();
            if ($this->_auth->checkAuth()) {
                $this->logger->info(__FUNCTION__."#admin_login",__FILE__,__LINE__,$_SESSION);
            } else {
                exit();
            }
        }
        */
    }

    function default_view() {
        $this->render_form();
    }

    function render_form() {
        $this->html_header();
        $files = $this->data["FILES"];
        foreach($files as $key => $file) {
            $filename = BASE_DIR.$file;
            //print_r($file_status);
            if (file_exists($filename)) {
                $file_status = stat($filename);
                print '<a href="?action_get=&key='.$key.'" target="_blank">'.$file.'</a>';
                if (!is_writable($filename)) {
                	print " [ 書き込み権限無し ]";
                }
				print '<br/>';
                print "作成日時：". date("Y/m/d H:i:s", $file_status["atime"])."<br />";
                print "更新日時：". date("Y/m/d H:i:s", $file_status["mtime"])."<br />";
                print $file_status["size"]." byte<br />";
            } else {
                print $file.'<br/>';
            }
            print '<form action="up.php" method="post" enctype="multipart/form-data">';
            print '<input type="hidden" name="key" value="'.$key.'">';
            print '<input type="file" name="file" value=""><br/>';
            print '<input type="submit" name="action_up"><br/>';
            print '</form>';
            // バックアップファイル
            $backup_list = glob($filename.".*");
            if ($backup_list) {
	            print '<form action="up.php" method="post">';
	            foreach($backup_list as $backup_file) {
	            	print '<input type="radio" name="backup['.$key.']" id="'.md5($backup_file).'" value="'.basename($backup_file).'" />';
	            	print '<label for="'.md5($backup_file).'">'.basename($backup_file).'</label>';
	            	print ' [ <a href="up.php?action_get=&key='.$key.'&d='.basename($backup_file).'">表示</a> ] ';
	            	print ' [ <a href="up.php?action_delete=&key='.$key.'&d='.basename($backup_file).'">削除</a> ] <br />';
	            }
	            print '<input type="submit" name="action_restore" value="復元"><br/>';
	            print '</form>';
            }
            print '<hr>';
        }
        $this->html_footer();
    }

    function html_header() {
        print '<html>';
        print '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        print '<body>';
        print '<h1>マニュアルとかファイル更新ツール Ver 0.2.0 (2008/10/10)</h1>';
    }

    function html_footer() {
        print '</body>';
        print '</html>';
    }

    function action_get() {
        $files = $this->data["FILES"];
        $key = $this->request->get("key");
        $date = $this->request->get("d");
        if ($date) {
	        $filename = BASE_DIR.dirname($files[$key])."/".$date;
        } else {
	        $filename = BASE_DIR.$files[$key];
        }
        if (file_exists($filename)) {
            $file_parts = pathinfo($filename);
            switch ($file_parts["extension"]) {
                case "exe":
                    header("Content-Type: application/octet-stream");
                    header("Content-Disposition: attachment; filename=".$file_parts["basename"]);
                    break;
                default:
                    header("Content-Type: application/octet-stream");
                    header("Content-Disposition: attachment; filename=".$file_parts["basename"]);
            }
            print file_get_contents($filename);
        } else {
            print "file not found";
        }
    }

    function action_up() {
        $files = $this->data["FILES"];
        $key = $this->request->get("key");
        if (!$filename = BASE_DIR.$files[$key]) {
        	die("不正なIDを検出しました");
        }
        $file_parts = pathinfo($filename);
        $file = $_FILES["file"];
        $upload_file_parts = pathinfo($file["name"]);
        $extension = $upload_file_parts["extension"];
        $this->logger->info(__FUNCTION__,__FILE__,__LINE__,array($files[$key], $_FILES));
        if ($file["error"] != 0) {
            die ("アップロードに失敗しました。");
        } elseif ($file["size"] == 0) {
            die ("アップロードに失敗しました。");
        } elseif ($file_parts["extension"] != $extension) {
            die ("拡張子が間違っています。");
        } else {
        	chmod(dirname($filename), 0777);
        	if (file_exists($filename)) {
	            rename($filename, $filename.".".date("Ymd_His"));
        	}
            move_uploaded_file($file["tmp_name"], $filename);
        	chmod($filename, 0777);
            header("Location: up.php");
        }
    }
    
    function action_restore() {
        $files = $this->data["FILES"];
        $info = $this->request->get("backup");
        list($key, $file) = each($info);
        if (!$filename = BASE_DIR.$files[$key]) {
        	die("不正なIDを検出しました");
        }
        $file = BASE_DIR.dirname($files[$key])."/".$file;
        if (!file_exists($filename)) {
        	die("現在公開中のファイルがない。");
        } elseif (!file_exists($file)) {
        	die("復元ファイルが存在しない。");
        } else {
	        rename($filename, $filename.".".date("Ymd_His"));
	        copy($file, $filename);
	    	chmod($filename, 0777);
        }
        header("Location: up.php");
    }
}
$main = new FileUploader();
$main->execute();
?>