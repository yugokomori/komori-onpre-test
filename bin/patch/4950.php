<?php
ini_set("memory_limit","512M");
require_once("set_env.php");
require_once ("classes/AppFrame.class.php");
require_once("classes/dbi/meeting_action_count_log.dbi.php");
require_once("classes/dbi/meeting_function_count_log.dbi.php");

class Patch_4950 extends AppFrame {

    var $dsn = null;

    function __destruct() {
        //unlink(N2MY_APP_DIR."/tmp/sess_".session_id());
    }

    function init() {
        $this->dsn = $this->get_dsn();
    }
    function default_view() {
        $obj_MeetingActionCountLog = new MeetingActionCountLogTable($this->dsn);
        echo "starting data extraction...".date("Y-m-d H:i:s");
        //echo "\n".memory_get_usage();
        $where = "meeting_action_count_key > 138326";
    	$old_data = $obj_MeetingActionCountLog->getRowsAssoc($where);
    	//$this->logger2->info($old_data);
    	echo "\ndata extraction finished...\n\nstarting data convertation...".date("Y-m-d H:i:s");
    	$new_data = array();
    	foreach($old_data as $data) {
    		$meeting_sequence_key = $data["meeting_sequence_key"];
    		$new_data[$meeting_sequence_key]["meeting_key"] =  $data["meeting_key"];
    		$new_data[$meeting_sequence_key]["meeting_sequence_key"] =  $data["meeting_sequence_key"];
    		$new_data[$meeting_sequence_key]["create_datetime"] =  $data["create_datetime"];
    		$action_name = $data["action_type"];
    		$new_data[$meeting_sequence_key][$action_name] =  $data["count"];
    	}
    	//$this->logger2->info($new_data);
        echo "\ndata convertation finished...\n\nstarting data import...".date("Y-m-d H:i:s");
        $obj_MeetingFunctionCountLog = new MeetingFunctionCountLogTable($this->dsn);
        foreach($new_data as $data) {
            $obj_MeetingFunctionCountLog->add($data);
        }
        echo "\ndata import...finished...".date("Y-m-d H:i:s");
        //echo "\n".memory_get_usage();
    }

    private function _log($string){
        $config = parse_ini_file( sprintf( "%sconfig/config.ini", N2MY_APP_DIR ), true);
        $log_dir = $config["LOGGER"]["log_dir"];
        $log_prefix = "4930_patch";
        $log_file = sprintf("%s/%s_%s.log", $log_dir, $log_prefix, date('Ymd', time()));
        $fp = fopen($log_file, "a");
        if($fp){
            fwrite($fp, $string."\n");
        }
        fclose($fp);
    }

}
$main = new Patch_4950();
$main->execute();