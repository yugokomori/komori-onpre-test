<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/N2MY_DBI.class.php");

define("DOCUMENT_STATUS_WAIT",    0);
define("DOCUMENT_STATUS_DO",      1);
define("DOCUMENT_STATUS_SUCCESS", 2);
define("DOCUMENT_STATUS_DELETE",  3);
define("DOCUMENT_STATUS_ERROR",  -1);
define("DOCUMENT_STATUS_PAGE_ERROR",  -3);

class DBI_Document extends N2MY_DB
{
    public $table = "document";
    protected $primary_key = "document_key";

    function __construct( $dsn ) {
        $this->init( $dsn, $this->table );
    }

    /**
     * ドキュメント情報一覧を取得する
     *
     * @access public
     * @param  string $meeting_key 会議プライマリKEY
     * @return array
     */
    function getList($meeting_key)
    {
        //check parameters
        if (!$meeting_key) {
            $this->logger->warn(__FUNCTION__.'invalid argument',__FILE__,__LINE__,array(
                'meeting_key' => $meeting_key,
                ));
            return array();
        }

        //prepare data
        $d   = array();
        $d[] = $meeting_key;

        //create sql
        $sql  = " select";
        $sql .= "   document_key      , ";
        $sql .= "   meeting_key       , ";
        $sql .= "   document_id       , ";
        $sql .= "   document_category , ";
        $sql .= "   document_extension, ";
        $sql .= "   document_status   , ";
        $sql .= "   document_index    , ";
        $sql .= "   document_sort     , ";
        $sql .= "   create_datetime   , ";
        $sql .= "   update_datetime     ";
        $sql .= " from ";
        $sql .=     $this->_tbl;
        $sql .= " where";
        $sql .= "   meeting_key     = ? ";
        $sql .= " and ";
        $sql .= "   document_status = 1 ";
        $sql .= " order by ";
        $sql .= "   document_sort asc ";

        //prepare
        $sth = $this->_conn->prepare($sql);
        if (PEAR::isError($sth)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$sth);
            //return $sth;
            return array();
        }

        //run query
        $result =& $this->_conn->execute($sth, $d);
        if (PEAR::isError($result)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$result);
            //return $result;
            return array();
        }

        //fetchRow
        $ra = array();
        while ($r =& $result->fetchRow(DB_FETCHMODE_ASSOC)) {
            if (PEAR::isError($r)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$r);
                //return $r;
                return array();
            }

            $ra[] = $r;
        }

        return $ra;
    }

// }}}
// {{{ -- getMeetingKey()

    /**
     * meeting_key を取得する
     *
     * @access public
     * @param  string  $document_id ドキュメントID
     * @return integer
     */
    function getMeetingKey($document_id)
    {
        //check parameters
        if (!$document_id) {
            $this->logger->warn(__FUNCTION__.'invalid argument',__FILE__,__LINE__,array(
                'document_id' => $document_id,
                ));
            return false;
        }

        //prepare data
        $d   = array();
        $d[] = $document_id;

        //create sql
        $sql  = " select";
        $sql .= "   meeting_key ";
        $sql .= " from ";
        $sql .=     $this->_tbl;
        $sql .= " where";
        $sql .= "   document_id = ?";
        $sql .= " limit 1 ;";

        //prepare
        $sth = $this->_conn->prepare($sql);
        if (PEAR::isError($sth)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$sth);
            //return $sth;
            return false;
        }

        //run query
        $result =& $this->_conn->execute($sth, $d);
        if (PEAR::isError($result)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$result);
            //return $result;
            return false;
        }

        //fetchRow
        $r =& $result->fetchRow(DB_FETCHMODE_ASSOC);
        if (PEAR::isError($r)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$r);
            //return $r;
            return false;
        }

        return $r['meeting_key'];
    }

// }}}
// {{{ -- getStatus()

    /**
     * ドキュメントステータス情報を取得する
     *
     * @access public
     * @param  string  $document_id ドキュメントID
     * @return integer
     */
    function getStatus($document_id)
    {
        //check parameters
        if (!$document_id) {
            $this->logger->warn(__FUNCTION__.'invalid argument',__FILE__,__LINE__,array(
                'document_id' => $document_id,
                ));
            return false;
        }

        //prepare data
        $d   = array();
        $d[] = $document_id;

        //create sql
        $sql  = " select";
        $sql .= "   document_status ";
        $sql .= " from ";
        $sql .=     $this->_tbl;
        $sql .= " where";
        $sql .= "   document_id = ?";
        $sql .= " limit 1 ;";

        //prepare
        $sth = $this->_conn->prepare($sql);
        if (PEAR::isError($sth)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$sth);
            //return $sth;
            return false;
        }

        //run query
        $result =& $this->_conn->execute($sth, $d);
        if (PEAR::isError($result)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$result);
            //return $result;
            return false;
        }

        //fetchRow
        $r =& $result->fetchRow(DB_FETCHMODE_ASSOC);
        if (PEAR::isError($r)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$r);
            //return $r;
            return false;
        }

        return $r['document_status'];
    }

// }}}
// {{{ -- removeDocuments()

    /**
     * 指定された会議KEYのドキュメントを格納ディレクトリごと削除する
     *
     * @access public
     * @param  string  $dir ディレクトリパス
     * @return boolean
     */
    function removeDocuments($dir)
    {
        $result = $this->_rmdir($dir);

        return $result;
    }

// }}}

// ------------------------------------------------------------------------
}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
