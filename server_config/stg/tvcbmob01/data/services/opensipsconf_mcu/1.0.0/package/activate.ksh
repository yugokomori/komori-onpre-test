#!/bin/ksh
# DATE: 12/06/2012
# Author : Thomas Carvello
#
# Permet de cr�er le lien entre le fichier de conf d'opensips (/data/services/opensipsconf_mcu/cfg/opensips.cfg et /etc/opensips/opensips.cfg

function activate
{
   rm -f  /data/services/opensipsconf_mcu/$arg/visioConfig.php
   if [ -r /data/services/opensipsconf_mcu/$arg/opensips.cfg ]
        then
	if [ -r /etc/opensips/opensips.cfg ]; then
            echo "Sauvegarde du fichier opensips.cfg"
            cp -f  /etc/opensips/opensips.cfg /etc/opensips/opensips.cfg.sav
            rm -f  /etc/opensips/opensips.cfg
	fi
        echo "Activation du fichier de conf opensips $SERVICENAME  "
        ln -s /data/services/opensipsconf_mcu/$arg/opensips.cfg /etc/opensips/opensips.cfg
   else
        echo "Il n'y a pas de fichier cfg pour cette version, $SERVICENAME ."
   fi
}

##### Main...
arg=$1
SERVICENAME=`readlink /data/services/opensipsconf_mcu/$arg`


case $1 in
        "pre-prod")
                echo "Pas de mise en preproduction pour les fichiers de conf d'opensips";;
        "prod")
                echo "Mise en production des specificites opensips,  version $SERVICENAME."
                activate;;

        *)
                echo "usage: activate.ksh [options]"
                echo "options :"
                echo " prod :    Active cette version du script avec le script specifique pour cette machine";;
esac
