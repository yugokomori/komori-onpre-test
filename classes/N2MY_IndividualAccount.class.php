<?php
require_once( "classes/N2MY_DBI.class.php" );
require_once( "lib/EZLib/EZCore/EZLogger2.class.php" );

class N2MY_IndividualAccount
{
    private $logger = "";

    var $auth_dsn = "";
    private $dsn = null;
    private $userKey = "";

    private $billList = array(
                    "user_id"=>"ユーザーID",
                    "user_key"=>"ユーザーキー",
//                    "max_rec_size"=>"max_rec_size",
                    "user_company_name"=>"会社名",
                    "user_company_postnumber"=>"会社郵便番号",
                    "user_company_address"=>"会社住所",
                    "user_company_phone"=>"会社電話番号",
                    "user_company_fax"=>"会社FAX番号",
                    "user_starttime"=>"ユーザー利用開始日",
//                    "user_registtime"=>"ユーザー登録日",
//                    "user_updatetime"=>"ユーザー更新日",
                    "user_deletetime"=>"ユーザー削除日",
//                    "service_key"=>"service_key",
                    "user_plan_yearly"=>"年契約",
                    "user_plan_yearly_starttime"=>"年間契約開始日",
//                    "user_plan_starttime"=>"プラン開始日",
//                    "room_name"=>"部屋名",
                    "meeting_max_seat"=>"最大参加可能人数",
//                    "meeting_size_used"=>"meeting_size_used",
//                    "meeting_log_owner"=>"meeting_log_owner",
//                    "meeting_start_datetime"=>"会議開始時間",
//                    "meeting_stop_datetime"=>"開始終了時間",
//                    "meeting_extended_counter"=>"meeting_extended_counter",
//                    "has_recorded_minutes"=>"has_recorded_minutes",
//                    "has_recorded_video"=>"has_recorded_video",
//                    "is_active"=>"is_active",
//                    "is_reserved"=>"is_reserved",
//                    "create_datetime"=>"create_datetime",
//                    "update_datetime"=>"update_datetime",
//                    "use_flg"=>"use_flg",
                    "service_name"=>"プラン名",
                    "intro"=>"導入プラン",
                    "desktop_share"=>"シェアリングオプション",
                    "hdd_extension"=>"追加容量",
                    "meeting_key"=>"会議キー",
                    "meeting_room_name"=>"会議名",
                    "actual_start_datetime"=>"会議実開始時間",
                    "actual_end_datetime"=>"会議実終了時間",
                    "member_id"=>"member_id",
                    "member_name"=>"メンバー名",
                    "member_name_kana"=>"メンバー名カナ",
                    "uptime_start"=>"入室時間",
                    "uptime_end"=>"退出時間",
//                    "meeting_use_minute"=>"利用時間",
//                    "participant_name"=>"会議参加者名",
                    "use_count"=>"利用時間",
                    );

    /**
     * コンストラクタ
     */
    function __construct( $dsn, $auth_dsn = null )
    {
        $this->logger =& EZlogger2::getInstance();
        $this->dsn = $dsn;
        $this->auth_dsn = $auth_dsn;

        $this->objMeetingdb	 = new N2MY_DB( $dsn );
        $this->objMGMdb		 = new N2MY_DB( $auth_dsn );
    }

    public function getPlan( $userKey )
    {
        $this->objMeetingDB = new N2MY_DB( $this->dsn, "user_plan" );
        $condition = sprintf( "user_key='%s' AND user_plan_status=1 AND user_plan_starttime<='%s'", $userKey, date( "Y-m-d" ) );
        return $this->objMeetingDB->getRow( $condition );
    }

    public function getOptionList( $userKey )
    {
        $this->objMeetingDB = new N2MY_DB( $this->dsn, "user_service_option" );
        $condition = sprintf( "user_key='%s' AND user_service_option_starttime<='%s' AND user_service_option_status=1", $userKey, date( "Y-m-d" ) );
        $optionList = $this->objMeetingDB->getRowsAssoc( $condition );

        $option = array();
        for( $i = 0; $i < count( $optionList ); $i++ ){
            switch( $optionList[$i]["service_option_key"] ){
                case "3":
                    $option["desktop_share"] = true;
                    break;

                case "4":
                    $option["hdd_extention"] += 1;
                    break;

            }
        }
        return $option;
    }
    /**
     * メンバー課金ユーザー毎のユーザー情報（会社情報）と部屋情報、会議情報を取得
     *
     */
    public function getIndividualAccountUserList( $year, $month )
    {
            $sql = "
SELECT
    u.user_id,
    u.user_key,
    u.max_rec_size,
    u.user_company_name,
    u.user_company_postnumber,
    u.user_company_address,
    u.user_company_phone,
    u.user_company_fax,
    u.user_starttime,
    u.user_registtime,
    u.user_updatetime,
    u.user_deletetime,
    u.service_key,
    u.user_plan_yearly,
    u.user_plan_yearly_starttime,
    u.user_plan_starttime,
    u.room_name,
    m.meeting_key,
    m.room_key,
    m.meeting_room_name,
    m.meeting_max_seat,
    m.meeting_size_used,
    m.meeting_log_owner,
    m.meeting_start_datetime,
    m.meeting_stop_datetime,
    m.meeting_extended_counter,
    m.has_recorded_minutes,
    m.has_recorded_video,
    m.is_active,
    m.is_reserved,
    m.create_datetime,
    m.update_datetime,
    m.use_flg,
    m.actual_start_datetime,
    m.actual_end_datetime,
    m.meeting_use_minute
FROM
(
    SELECT
        u.*, r.room_key, r.room_name
    FROM
    (
        SELECT
            u.*, up.service_key, up.user_plan_yearly, up.user_plan_yearly_starttime, up.user_plan_starttime
        FROM
            user_plan up
        LEFT JOIN
            user u
        ON
            up.user_key = u.user_key
        WHERE
            up.user_plan_status = 1 AND
            DATE_FORMAT( up.user_plan_starttime, '%Y-%m' ) <= '" . $year. "-" . $month . "'
    ) u
    LEFT JOIN
        room r
    ON
        u.user_key = r.user_key
    WHERE
        u.account_model = 'member' AND
        u.user_status=1 AND
        ( u.user_delete_status = 0 OR
        ( u.user_delete_status = 2 AND DATE_FORMAT( u.user_deletetime, '%Y-%m' ) >= '". $year . "-" . $month ."'"." ) )
) u
LEFT JOIN
    meeting m
ON
    u.room_key = m.room_key
WHERE
    m.use_flg=1 AND
    DATE_FORMAT( m.actual_end_datetime, '%Y-%m' ) = '". $year . "-" . $month ."' AND
    m.meeting_use_minute >= 1
ORDER BY
    u.user_key, m.meeting_key";

        $rs_userList = $this->objMeetingdb->_conn->query( $sql );
        $list = array();
        while( $ret_userInfo = $rs_userList->fetchRow( DB_FETCHMODE_ASSOC ) ){
            $list[] = $ret_userInfo;
        }
        return $list;
    }


    public function getUserList( $date )
    {
        $dsnArray = parse_url( $this->dsn );
        $meetingDbName = str_replace( "/", "", $dsnArray["path"] );
        $authArray = parse_url( $this->auth_dsn );
        $mgmDbName = str_replace( "/", "", $authArray["path"] );

        $sql = "
SELECT
    u.*,
    s.service_key,
    s.user_plan_yearly,
    s.user_plan_yearly_starttime,
    s.user_plan_starttime
FROM
    (
    SELECT
        *
    FROM
        " . $meetingDbName. ".user
    WHERE
        account_model = 'member' AND
        ( user_delete_status = 0 OR
        ( user_delete_status = 2 AND user_deletetime >= '" . $date["now"] . " 00:00:00' ) )
    ) u
LEFT JOIN
    (
    SELECT
        service_key,
        user_key,
        user_plan_yearly,
        user_plan_yearly_starttime,
        user_plan_starttime
    FROM
        user_plan
    WHERE
        user_plan_status=1
    ) s
ON
    u.user_key = s.user_key
ORDER BY
    u.user_key";

        $rs_userList = $this->objMeetingdb->_conn->query( $sql );
        $list = array();
        while( $ret_userInfo = $rs_userList->fetchRow( DB_FETCHMODE_ASSOC ) ){
            //service取得
            $serviceSql = sprintf( "SELECT service_name FROM service WHERE service_key='%s' LIMIT 1", $ret_userInfo["service_key"] );
            $rs_service = $this->objMGMdb->_conn->query( $serviceSql );
            $service = $rs_service->fetchRow( DB_FETCHMODE_ASSOC );
            $ret_userInfo["service_name"] = $service["service_name"];
            //sharing, hdd_extension
            $optionSql="
SELECT
    *, count(*) num
FROM
    user_service_option uso
WHERE
    uso.user_key=" . $ret_userInfo["user_key"] . " AND
    ( ( uso.service_option_key=3 OR uso.service_option_key=4 ) OR
    ( uso.service_option_key=14 AND DATE_FORMAT( uso.user_service_option_starttime, '%Y-%m-%d' ) = '" . $date["now"] . "' ) ) AND
    uso.user_service_option_status = 1
GROUP BY
    uso.service_option_key";

            $rs_optionList = $this->objMeetingdb->_conn->query( $optionSql );
            while( $ret_optionInfo = $rs_optionList->fetchRow( DB_FETCHMODE_ASSOC ) ){
                switch( $ret_optionInfo["service_option_key"] )
                {
                    case "3":	//sharing
                        $ret_userInfo["desktop_share"] = 1;
                        break;

                    case "4":	// hdd_extension
                        $ret_userInfo["hdd_extension"] = $ret_optionInfo["num"];
                        break;

                    case "14":	//intro
                        $ret_userInfo["intro"] = $ret_optionInfo["num"];
                        break;
                }
            }
            $list[] = $ret_userInfo;
        }
        return $list;
    }

    public function getParticipantList( $date )
    {
        $sql = "
 SELECT
  `mtg`.`meeting_key`
 ,`mtg`.`fms_path`
 ,`mtg`.`layout_key`
 ,`mtg`.`server_key`
 ,`mtg`.`sharing_server_key`
 ,`mtg`.`user_key`
 ,`mtg`.`room_key`
 ,`mtg`.`meeting_ticket`
 ,`mtg`.`meeting_session_id`
 ,`mtg`.`pin_cd`
 ,`mtg`.`meeting_country`
 ,`mtg`.`meeting_room_name`
 ,`mtg`.`meeting_name`
 ,`mtg`.`meeting_tag`
 ,`mtg`.`meeting_max_audience`
 ,`mtg`.`meeting_max_whiteboard`
 ,`mtg`.`meeting_max_extendable`
 ,`mtg`.`meeting_max_seat`
 ,`mtg`.`meeting_size_recordable`
 ,`mtg`.`meeting_size_uploadable`
 ,`mtg`.`meeting_size_used`
 ,`mtg`.`meeting_log_owner`
 ,`mtg`.`meeting_log_password`
 ,`mtg`.`meeting_start_datetime`
 ,`mtg`.`meeting_stop_datetime`
 ,`mtg`.`meeting_extended_counter`
 ,`mtg`.`has_recorded_minutes`
 ,`mtg`.`has_recorded_video`
 ,`mtg`.`is_active`
 ,`mtg`.`is_extended`
 ,`mtg`.`is_locked`
 ,`mtg`.`is_blocked`
 ,`mtg`.`is_reserved`
 ,`mtg`.`intra_fms`
 ,`mtg`.`is_publish`
 ,`mtg`.`version`
 ,`mtg`.`create_datetime`
 ,`mtg`.`update_datetime`
 ,`mtg`.`room_addition`
 ,`mtg`.`is_deleted`
 ,`mtg`.`use_flg`
 ,`mtg`.`actual_start_datetime`
 ,`mtg`.`actual_end_datetime`
 ,`mtg`.`meeting_use_minute`
 ,`p`.`meeting_key`
 ,`p`.`member_key`
 ,`p`.`use_count`
 ,`p`.`member_id`
 ,`p`.`member_email`
 ,`p`.`member_name`
 ,`p`.`participant_key`
 ,`p`.`participant_station`
 ,`p`.`uptime_start`
 ,`p`.`uptime_end`
 FROM
 (
    SELECT
        `mmulgrp`.`meeting_key`, `mmulgrp`.`member_key`, `mmulgrp`.`use_count`,
        `m`.`member_id`, `m`.`member_email`, `m`.`member_name`,
        `ptp`.`participant_key`, `ptp`.`participant_station`, `ptp`.`uptime_start`, `ptp`.`uptime_end`
    FROM
    (
        SELECT
            `mmul`.`participant_key`, `mmul`.`meeting_key`, `mmul`.`member_key`, count( `mmul`.`log_no` ) AS `use_count`
        FROM
            `meeting_member_use_log` AS `mmul`
        WHERE
            DATE_FORMAT( `mmul`.`create_datetime`, '%Y-%m-%d') = '" . $date["now"] . "'
        GROUP BY
            `mmul`.`participant_key`, `mmul`.`meeting_key`, `mmul`.`member_key`
    )  as `mmulgrp`
    LEFT JOIN
        `participant` as `ptp`
    ON
      ( `mmulgrp`.`member_key` = `ptp`.`member_key`
    AND `mmulgrp`.`participant_key` = `ptp`.`participant_key` )
    LEFT JOIN
        `member` AS `m`
    ON
      ( `mmulgrp`.`member_key` = `m`.`member_key` )
 ) AS `p`
 LEFT JOIN
    `meeting` AS `mtg`
 ON
    `p`.`meeting_key` = `mtg`.`meeting_key`
 ORDER BY
    `mtg`.`meeting_key`, `mtg`.`actual_start_datetime` ";

        $rs_userList = $this->objMeetingdb->_conn->query( $sql );
        $list = array();
        while( $ret_userInfo = $rs_userList->fetchRow( DB_FETCHMODE_ASSOC ) ){
            $list[] = $ret_userInfo;
        }
        return $list;
    }

    public function getHeader()
    {
        return $this->billList;
    }
}
