<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("classes/N2MY_DBI.class.php");

class ConferenceRecordTable extends N2MY_DB {

    var $table = "conference_record";
    var $logger = null;
    protected $primary_key = 'conference_record_key';

    public function __construct( $dsn )
    {
        $this->init($dsn, $this->table );
    }
}
