#!/usr/local/bin/perl

################################################################################
# Rotation Program for Backuped Files
#
################################################################################


# $hostName is used for backup file name
my $hostName = `/bin/hostname`;
chomp($hostName);


# where backup file is placed
my $backupDirectory = '/home/backup/local';

# shell commands
my $mv = '/bin/mv';
my $rm = '/bin/rm';
my $mkdir = '/bin/mkdir';

# how many versions of backuped files are needed
my $max = 7;

my $cmd;


#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# DON'T EDIT FOLLOWING CODE
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#--- remove the directory which version is $max
my $dir = "${backupDirectory}/${max}";
if (-d $dir) {
    $cmd = "$rm -rf $dir";
#    print "$cmd\n";
    system($cmd) == 0
        or die "system $cmd failed: $?\n";
}

#--- rename directories (1->2, 2->3,..., 6->7)
for (my $i = $max; $i > 1;) {
    my $toDir = "${backupDirectory}/${i}";
    $i--;
    $dir = "${backupDirectory}/${i}";
    if (-d $dir) {
        $cmd = "$mv $dir $toDir";
#       print "$cmd\n";
        system($cmd) == 0
            or die "system $cmd failed: $?\n";
    }
}

#--- move new files to directory '1'
$dir = "${backupDirectory}/1";
$cmd = "$mkdir $dir && $mv ${backupDirectory}/${hostName}_* $dir";
#print "$cmd\n";
system($cmd) == 0
    or die "system $cmd failed: $?\n";

#--- chown the whole directory
#$cmd = "chown -R backup.backup $backupDirectory";
#print "$cmd\n";
system($cmd) == 0
    or die "system $cmd failed: $?\n";
