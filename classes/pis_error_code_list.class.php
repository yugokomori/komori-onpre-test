#!/usr/local/bin/php
<?php
set_time_limit(0);
ini_set("memory_limit","2048M");

class AppErrorCodeListClass {

    var $dsn = null;
    /**
     * コンストラクタ
     */
    function AppErrorCodeListClass($dsn){
        $this->logger  = & EZLogger::getInstance();
        $this->logger2 = & EZLogger2::getInstance();
    }

    /**
     * エラーログ出力用
     * @param  [type] $value    エラーコード情報
     * @param  [type] $exit_flg 処理強制終了判定
     * @param  [type] $table_error_messages  各テーブルのエラー発生時
     */
    function error_log ($value,$exit_flg=null,$table_error_messages=null){

        switch($value){

				// -------------------------------------------------
				// 001 CSV作成処理系 START
				// -------------------------------------------------
			case ERROR_FTP_WIDS_KIHON :
				$error_code = "VCUBE_001_001"; // FTP取得失敗
				break;
			case ERROR_FTP_WIDS_ZAISEKI :
				$error_code = "VCUBE_001_002"; // FTP取得失敗
				break;
			case ERROR_FTP_WIDS_HENKAN :
				$error_code = "VCUBE_001_003"; // FTP取得失敗
				break;
			case ERROR_FTP_WIDS_GID_KIHON :
				$error_code = "VCUBE_001_004"; // FTP取得失敗
				break;
			case ERROR_FTP_WIDS_GID_SYOZOKU :
				$error_code = "VCUBE_001_005"; // FTP取得失敗
				break;
			case ERROR_KIHON_FILE_NOT_FOUND :
				$error_code = "VCUBE_001_006"; // ファイル読込み失敗
				break;
			case ERROR_ZAISEKI_FILE_NOT_FOUND :
				$error_code = "VCUBE_001_007"; // ファイル読込み失敗
				break;
			case ERROR_HENKAN_FILE_NOT_FOUND :
				$error_code = "VCUBE_001_008"; // ファイル読込み失敗
				break;
			case ERROR_GID_KIHON_FILE_NOT_FOUND :
				$error_code = "VCUBE_001_009"; // ファイル読込み失敗
				break;
			case ERROR_GID_SYOZOKU_FILE_NOT_FOUND :
				$error_code = "VCUBE_001_010"; // ファイル読込み失敗
				break;
			case ERROR_CREATE_TID_LIST :
				$error_code = "VCUBE_001_011"; // TID_LIST作成失敗
				break;
			case ERROR_CREATE_GID_LIST :
				$error_code = "VCUBE_001_012"; // GID_LIST作成失敗
				break;
			case ERROR_TID_LIST_DOSE_NOT_EXIT :
				$error_code = "VCUBE_001_013"; // ファイル読込み失敗
				break;
			case ERROR_GID_LIST_DOSE_NOT_EXIT :
				$error_code = "VCUBE_001_014"; // ファイル読込み失敗
				break;
			case ERROR_CREATE_USER_LIST :
				$error_code = "VCUBE_001_015"; // USER_LIST作成失敗
				break;
			case ERROR_IDE_NOT_FOUND :
				$error_code = "VCUBE_001_016"; // ID無しデータ読込み
				break;
        	// -------------------------------------------------
        	// 001 CSV作成処理系 END
        	// -------------------------------------------------

            // -------------------------------------------------
            // 002 ID取込処理系 START
            // -------------------------------------------------

            case ERROR_USER_LIST_FLG:
                $error_code =  "VCUBE_002_001";   //USER_LIST作成成功確認
                break;
            case ERROR_USER_LIST:
                $error_code =  "VCUBE_002_002";   //USER_LIST存在確認
                break;
            case ERROR_WHITE_CODE:
                $error_code =  "VCUBE_002_003";   //ホワイトコード　DB取得失敗
                break;
            case ERROR_WHITE_ID:
                $error_code =  "VCUBE_002_004";   //ホワイトID　DB取得失敗
                break;
            case ERROR_MONTHLY_TABLE:
                $error_code =  "VCUBE_002_005";   //月次処理　DB取得失敗
                break;
            case ERROR_MONTHLY:
                $error_code =  "VCUBE_002_006";   //月次処理　DB書き込み時の障害
                break;
            case ERROR_WHITE_UPDATE:
                $error_code =  "VCUBE_002_007";   //ホワイト系　DB書き込み時の障害
                $value = $table_error_messages;
                break;
            case ERROR_BLACK_ID:
                $error_code =  "VCUBE_002_008";   //ブラックID　DB取得失敗
                break;
            case ERROR_BLACK_UPDATE:
                $error_code =  "VCUBE_002_009";   //ブラック系　DB書き込み時の障害
                break;
			case ERROR_MUITIPLE_START_UP :
				$error_code = "VCUBE_002_010"; // 多重起動
				break;
			case ERROR_PROCESS_KILL :
				$error_code = "VCUBE_002_011"; // プロセスkill
				break;
                // -------------------------------------------------
                // 002 ID取込処理系 END
                // -------------------------------------------------
            default:
                // 例外エラー
                $value = "VCUBE_unknown error";
                break;
        }

        $this->logger2->error($error_code . " [ ".$value ." ]");
        if($exit_flg){
            // エラーコードつきの終了
            exit($error_code);
        }
    }
    /*
     * エラーログに出力するため使用しない
    *
    // エラーメール送信
    public function sendReport($mail_from, $mail_to, $mail_subject, $mail_body) {
    require_once("lib/EZLib/EZMail/EZSmtp.class.php");
    // 入力チェック
    if (!$mail_from || !$mail_to || !$mail_subject || !$mail_body) {
    return false;
    }
    require_once "lib/EZLib/EZUtil/EZLanguage.class.php";
    $lang = EZLanguage::getLangCd("ja");
    $obj_SendMail = new EZSmtp(null, $lang, "UTF-8");
    $obj_SendMail->setFrom($mail_from);
    $obj_SendMail->setReturnPath($mail_from);
    $obj_SendMail->setTo($mail_to);
    $obj_SendMail->setSubject($mail_subject);
    $obj_SendMail->setBody($mail_body);


    // メール送信
    $result = $obj_SendMail->send();
    $this->logger2->info(array($mail_from, $mail_to, $mail_subject, $mail_body));
    }
    */

}

