<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
ini_set("memory_limit","2000M");
set_time_limit(0);
require_once ('classes/AppFrame.class.php');
require_once ('classes/mgm/dbi/user.dbi.php');
require_once("classes/N2MY_Account.class.php");
require_once ('classes/dbi/member.dbi.php');
require_once ('classes/dbi/member_group.dbi.php');
require_once ('classes/dbi/member_status.dbi.php');
require_once ('classes/dbi/user.dbi.php');
require_once ('config/config.inc.php');
require_once ('lib/EZLib/EZUtil/EZEncrypt.class.php');
require_once( "lib/EZLib/EZCore/EZLogger2.class.php" );
require_once ('lib/EZLib/EZUtil/EZCsv.class.php');
require_once ('classes/dbi/blacklist.dbi.php');
require_once ('classes/dbi/whitelist.dbi.php');


class AppMember extends AppFrame
{
    var $logger = null;
    var $member = null;
    var $blacklist = null;
    var $whitelist = null;
    var $account_dsn = null;

    function init() {
        $this->account_dsn = $this->config->get("GLOBAL", "auth_dsn");
        $this->member      = new MemberTable($this->get_dsn());
        $this->blacklist   = new blacklistTable($this->get_dsn());
        $this->whitelist   = new whitelistTable($this->get_dsn());  
    }
                
    public function action_member_id() {
        $user_info = $this->session->get("user_info");
        $user_key = $user_info["user_key"];

        echo ($_POST["table"]) ;
        $table_name = $_POST["table"];
        
        // 対象となるTABLEチェック
        switch($table_name) {
           case "pismeeting_data.whitelist":
               $col_name  = "member_id";
               $table = "whitelist";
               break;  
           case "pismeeting_data.blacklist":
               $col_name  = "member_id";
               $table = "blacklist";
               break;  
           default:
               echo "例外発生";
               exit;
            }
        

    // before ファイル読み込み
        $request["upload_file"] = $_FILES["import_file_b"];
        $data = pathinfo($request["upload_file"]["name"]);

        //CSVファイルチェック
        if (empty($data['filename'])) {
          echo " 変更前のCSVファイルに不備があります。";
          exit;
        } elseif($data['extension'] != "csv") {
          echo " 変更前のCSVファイルに不備がありますす、形式を確認してください。";
          exit;
        }

        //CSVファイル格納
        $filename = $this->get_work_dir()."/".tmpfile();
        move_uploaded_file($request["upload_file"]["tmp_name"], $filename);
        $request["upload_file"]["tmp_name"] = $filename;
        $output_encoding = $this->get_output_encoding();
        $csv = new EZCsv(true,$output_encoding,"UTF-8");
        $csv->open($filename, "r");
        $rows_b = array();
        while($row = $csv->getNext(true)) {
//            $rows_b[] = $row[$col_name];
            $rows_b[] = $row["member_id"];
        }
        $csv->close();
        unlink($filename);

    // after ファイル読みこみ

        $request["upload_file"] = $_FILES["import_file_a"];
        $data = pathinfo($request["upload_file"]["name"]);
        //CSVファイルチェック
        if (empty($data['filename'])) {
          echo " 変更後のCSVファイルに不備があります。";
          exit;
        } elseif($data['extension'] != "csv") {
          echo " 変更後のCSVファイルに不備があります、形式を確認してください。";
          exit;
        }

        //CSVファイル格納
        $filename = $this->get_work_dir()."/".tmpfile();
        move_uploaded_file($request["upload_file"]["tmp_name"], $filename);
        $request["upload_file"]["tmp_name"] = $filename;
        $output_encoding = $this->get_output_encoding();
        $csv = new EZCsv(true,$output_encoding,"UTF-8");
        $csv->open($filename, "r");
        $rows_a = array();
        while($row = $csv->getNext(true)) {
//            $rows_a[] = $row[$col_name];
            $rows_a[] = $row["member_id"];
        }
        $csv->close();
        unlink($filename);

        // 変更前後の行数チェック
        $count_a = count($rows_a);
        $count_b = count($rows_b);
                
        if($count_a !== $count_b){
            echo "CSVファイルの行数に差異があります。";
            exit;
        }
        
        // 結果出力用
        $check =( "    Before     =>  After \n"); 
        echo nl2br($check);
        echo " <hr width=100% size=4 color=#000066> " ;

        $count = 0 ;        
        $ok_count = 0 ;        
        $ng_count = 0 ;        
        foreach($rows_b as $member_row) {


// csv が2つなくてもできるパターン
//
//            // 存在チェック
//            $sql_len = "SELECT count(*) FROM " . $table_name . " WHERE " . $col_name . "='". $rows_b[$count]."'";
//            $member_id_len = $this->$table->_conn->getOne($sql_len);
//
//            // 重複はないため0or1で判断
//            if ($member_id_len !== "0") {
//                $sql_sel = "SELECT LPAD(member_id,7,'0') FROM " . $table_name . " WHERE" . $col_name . "='". $member_row["member_id"]."'";        
//                $member_id_lpad = $this->memberTable->_conn->getOne($sql_sel);
//                
//                if ($member_id_lpad) {
//                   
//                      $sql_up = "UPDATE " . $table_name . " SET" . $col_name . "='". $member_id_lpad ."' WHERE" . $col_name . "='". $member_row["member_id"]."'"; 
//                      $result = $this->memberTable->_conn->query($sql_up);
//                }
//            }else{
//              // nullだった場合
//              echo " <font color='blue'>   Skip   ".$member_id_lpad ;
//              echo " <font color='blue'>   csvに対象のテーブルなし        :   " ;
//                }
//          }
//
    
    // データの更新1
    // member と userは同時に行う
    ///////////////
        if ($table_name === "pismeeting_account.user") {

           // account スキーマの処理
           $update_data = array("user_id"  => $rows_a[$count]);
           $preCheck_account =  $obj_MgmUserTable->getRow(sprintf("user_id='%s'", $rows_b[$count] ));

           if ($preCheck_account){
               $result =  $obj_MgmUserTable->update($update_data,  sprintf("user_id='%s'", $rows_b[$count] ));
                if (DB::isError($result)) {
                   $check =($check. " <font color='red'> ★ : account result :   ");
                   $check =($check." " .$rows_b[$count]. " => " . $rows_a[$count] . "\n");
                   $check_log =($check_log. " ★ : account result :   ");
                   $check_log =($check_log." " .$rows_b[$count]. " => " . $rows_a[$count] . "\n");
               } else {
                   $check =($check.  "<font color='black'> ok : account result :  ");
                   $check = $check_log =($check." " .$rows_b[$count]. " => " . $rows_a[$count] . "\n");
                   $check_log =($check_log.  " ok : account result :  ");
                   $check_log =($check_log." " .$rows_b[$count]. " => " . $rows_a[$count] . "\n");
               }
           }else{
               $check =($check. " <font color='red'> SKIP : account result :   ");
               $check =($check." " .$rows_b[$count]. " : not data \n"	);
               $check_log =($check_log. " SKIP : account result :   ");
               $check_log =($check_log." " .$rows_b[$count]. " : not data \n"	);
           }
           
        }

    // データの更新2
    // data スキーマの処理
    ///////////////
        $update_data = array("member_id"  => $rows_a[$count]);

        $sql = "SELECT count(*) FROM " . $table . " WHERE " . "member_id" . "='". $rows_b[$count]."'";
        $preCheck_data = $this->$table->_conn->getOne($sql);
        
        if ($preCheck_data){
            $result =  $this->$table->update($update_data,  sprintf("member_id='%s'", $rows_b[$count] ));                    
            // check
            if (DB::isError($result)) {
                $check =($check.  " <font color='red'> ★ : data result :");
                $check =($check." " .$rows_b[$count]. " => " . $rows_a[$count] . "\n");
                $check_log =($check_log.  " ★ : data result :");
                $check_log =($check_log." " .$rows_b[$count]. " => " . $rows_a[$count] . "\n");
            } else {
                $check =($check.  "<font color='black'> ok : data result :");
                $check =($check." " .$rows_b[$count]. " => " . $rows_a[$count] . "\n");
                $check_log =($check_log.  " ok : data result :");
                $check_log =($check_log." " .$rows_b[$count]. " => " . $rows_a[$count] . "\n");
            }
        }else{
            $check =($check.  " <font color='red'> SKIP : data result :");
            $check =($check." " .$rows_b[$count]. " :  not data \n");
            $check_log =($check_log.  " SKIP : data result :");
            $check_log =($check_log." " .$rows_b[$count]. " :  not data \n");
        }
        $count = $count + 1;
    }
    
    // logファイル出力
    $config = parse_ini_file( sprintf( "%sconfig/config.ini", N2MY_APP_DIR ), true);
    $log_dir = $config["LOGGER"]["log_dir"];
    $this->_log ( $check_log , $log_dir , $table);
    
    // 画面表示
    echo nl2br($check);
    
    }
    
    private function _log($string , $result_log_dir , $table){
        $log_prefix = $table."Table_log";
        $log_file = sprintf("%s/%s_%s.log", $result_log_dir, $log_prefix, date('Ymd', time()));
        $fp = fopen($log_file, "a");

        if($fp){
            fwrite($fp, $string."\n");
        }
        fclose($fp);
    }
    
}

$main =& new AppMember();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>

