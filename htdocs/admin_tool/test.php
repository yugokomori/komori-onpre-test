<?php
/*
 * Created on 2008/11/18
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

require_once("classes/AppFrame.class.php");
require_once('classes/mgm/dbi/FmsServer.dbi.php');

class AppFmsService extends AppFrame {

    function init() {
        // 開発者のみテスト可能
        if($_SERVER['REMOTE_ADDR'] != '192.168.1.188') {
            die('Who are you?');
        }
    }

    /**
     * 状態変更
     */
    function default_view() {
        # 参照DBのみ分散
//        $this->connectDB();
//        # 容量取得
//        $this->getMeetingSize();
//        # FMSの重み付けランダムセレクト
//        $this->getRandomSelect();
          # MCrypt動作確認
          $this->mcrypt_check();
    }

    function mcrypt_check() {
        $key = N2MY_ENCRYPT_KEY;
        $iv = N2MY_ENCRYPT_IV;
        $id = '281327,log_video,ja,284255';
        require_once("lib/EZLib/EZUtil/EZEncrypt.class.php");
        $crypt_id = EZEncrypt::encrypt($key, $iv, $id);
        print $crypt_id.'<br />';
        print EZEncrypt::decrypt($key, $iv, $crypt_id).'<br />';
//        print_r($_GET);
//        print '['.$_GET['id'].']<br />';
//        print $_SERVER["QUERY_STRING"];
        $params = split('&', $_SERVER["QUERY_STRING"]);
        foreach($params as $param) {
            $key = substr($param, 0, strpos('='));
            $id = substr($param, strpos('='));
        }
        print EZEncrypt::decrypt($key, $iv, str_replace(' ', '+', $id)).'<br />';
        phpinfo();
    }

    function connectDB() {
        require_once 'lib/EZLib/EZDB/EZDB.class.php';
        // 接続
        $db = new EZDB();
        $db->init();
        // 登録処理
        // 参照処理
    }

    function getMeetingSize() {
        require_once 'classes/core/dbi/Meeting.dbi.php';
        require_once 'classes/core/Core_Meeting.class.php';
        $obj_Meeting = new DBI_Meeting($this->get_dsn());
        $where = 'is_active = 0'
                .' AND is_deleted = 0'
                .' AND use_flg = 1'
                .' AND has_recorded_minutes = 1'
                .' AND has_recorded_video = 1'
                .' AND meeting_key = 506'
                ;
        $meeting_list = $obj_Meeting->getRowsAssoc($where, null, 1, 0, 'meeting_key,has_recorded_minutes,has_recorded_video,meeting_size_used');
        print '<pre>';
        $obj_CORE_Meeting = new Core_Meeting($this->get_dsn());
        foreach ($meeting_list as $meeting_info) {
            $getSize = $obj_CORE_Meeting->getMeetingSize($meeting_info['meeting_key']);
            print_r($meeting_info);
            print $getSize;
        }
    }

    /**
     * FMSの重み付けランダムセレクト
     */
    function getRandomSelect() {
        var_dump((null < '4.6.5.10'));
        $fms = $this->request->get("fms");
        $status = $this->request->get("status");
        $obj_FmsServer = new DBI_FmsServer( N2MY_MDB_DSN );
        $country = 'us';
        $ssl = 0;
        print '<pre>';
        $ret = array();
        for($i = 0; $i < 100; $i++) {
            if ($key = $this->getFms()) {
//            if ($key = $obj_FmsServer->getKeyByPriority($country, $ssl)) {
                $ret[$key]++;
            }
        }
        ksort($ret);
        print_r($ret);
        print array_sum($ret);
    }

    function getFms() {
        $list = array(
            '1'     => 0,
            '2'     => 0,
            '3'     => 1,
            '4'     => 1,
            '5'     => 1,
            '6'     => 0,
            '7'     => 1,
            '8'     => 1,
            '9'     => 0,
            '10'    => 0,
        );
        $cnt = 1;
        // FMSがなくなるまで
        while($list) {
            // ランダムな選択
            $sum = 0;
            foreach($list as $val) {
                $sum += $val;
            }
            $rand = rand(1,$sum);
            $i = 0;
            foreach($list as $key => $val) {
                if (($i <= $rand) && ($rand <= $i + $val)) {
                    break;
                }
                $i += $val;
            }
            // 接続可能なFMS
            if ($this->isActive($key)) {
                return $key;
            // 接続できない
            } else {
                // 永久ループ対策
                if ($cnt > 1000) {
                    return false;
                }
                // 指定から外す
                unset($list[$key]);
                $cnt++;
            }
        }
    }

    function isActive($key) {
        return in_array($key, array(1,2,3,4,5,6,8));
    }
}

$main = new AppFmsService();
$main->execute();
?>
