<?php
require_once("classes/core/Core_Meeting.class.php");
require_once("lib/EZLib/EZCore/EZLogger2.class.php");
//require_once("lib/EZLib/EZXML/EZXML.class.php");
require_once("lib/EZLib/EZUtil/EZUrl.class.php");
require_once("classes/core/dbi/Meeting.dbi.php");
require_once("classes/core/dbi/Participant.dbi.php");
require_once("classes/core/dbi/MeetingSequence.dbi.php");
require_once("classes/N2MY_Document.class.php");
require_once("classes/dbi/reservation.dbi.php");

class N2MY_MeetingLog {

    var $provider_id = "";
    var $provider_pw = "";
    var $logger = null;

    private $Core_Meeting;
    private $obj_Meeting;

    /**
     * コンストラクタ
     *
     * @param string $base_url COREのBaseURL
     * @param string $provider_id プロバイダID
     * @param string $provider_pw プロバイダパスワード
     */
    function __construct ($dsn) {
        $this->logger = & EZLogger2::getInstance();
        $this->dsn = $dsn;
        $this->Core_Meeting = new Core_Meeting( $this->dsn );
        $this->obj_Meeting = new DBI_Meeting( $this->dsn );
        $this->obj_Participant = new DBI_Participant( $this->dsn );
        $this->obj_MeetingSequence = new DBI_MeetingSequence( $this->dsn );
        $this->obj_Reservation     = new ReservationTable($this->dsn);
    }

    // PIS利用ログ用、参加会議&自分で予約した会議を取得し、一つのリストにする
    function getMyMeetingList($param){
        // 参加会議取得
        if (isset($param["start_time"]) || isset($param["end_time"])) {
            $start_limit = $param["start_time"] ? mysql_real_escape_string($param["start_time"]) : "0000-00-00 00:00:00";
            $end_limit   = $param["end_time"] ? mysql_real_escape_string($param["end_time"]) : "9999-12-31 23:59:59";
            $where = "(" .
            "( uptime_start between '$start_limit' AND '$end_limit' )" .
            " OR ( uptime_end between '$start_limit' AND '$end_limit' )" .
            " OR ( '$start_limit' BETWEEN uptime_start AND uptime_end )" .
            " OR ( '$end_limit' BETWEEN uptime_start AND uptime_end ) ) AND ";
            $where_reservation = "(" .
            "( reservation_starttime between '$start_limit' AND '$end_limit' )" .
            " OR ( reservation_extend_endtime between '$start_limit' AND '$end_limit' )" .
            " OR ( '$start_limit' BETWEEN reservation_starttime AND reservation_extend_endtime )" .
            " OR ( '$end_limit' BETWEEN reservation_starttime AND reservation_extend_endtime ) ) AND ";
        }
        $where = $where . " member_key = '" . $param["member_key"] . "' ";
        $participant_list  = $this->obj_Participant->getRowsAssoc($where);
        $z = 0;
        $reservation_list = array();
        foreach ($participant_list as $participant){
            $meeting_info = $this->obj_Meeting->getRow("meeting_key = " . $participant["meeting_key"] , "meeting_name, meeting_ticket , meeting_key , room_key , meeting_session_id, meeting_max_seat");
            $reservation_info = $this->obj_Reservation->getRow("meeting_key = '" . $meeting_info["meeting_ticket"]. "' ");

            if(!in_array($reservation_info["reservation_key"] , $reservation_list)){
                // 後で検索用の配列作成
                $meeting_list[$z]["meeting"] = $meeting_info;
                $reservation_list[] = $reservation_info["reservation_key"];
                $meeting_list[$z]["reservation"] = $reservation_info;
                $z++;
            }

        }
        // 予約会議取得
        $where_reservation = $where_reservation  . " NOW() > reservation_extend_endtime AND reservation_status = 1 AND member_key = '" . $param["member_key"] . "' ";
        $my_reservation_list = $this->obj_Reservation->getRowsAssoc($where_reservation);

        // 予約だけして参加してない会議を取得
        if(my_reservation_list){
            foreach($my_reservation_list as $my_reservation){
                if(!in_array($my_reservation["reservation_key"] , $reservation_list)){
                    $meeting_info = $this->obj_Meeting->getRow("meeting_ticket = '" . $my_reservation["meeting_key"]."'" , "meeting_name, meeting_ticket , meeting_key , room_key , meeting_session_id, meeting_max_seat");
                    $meeting_array["meeting"] = $meeting_info;
                    $meeting_array["reservation"] = $my_reservation;
                    $meeting_list[] = $meeting_array;
                }
            }
        }

        // 並び替え
        foreach ($meeting_list as $key => $value){
            $key_id[$key] = $value["reservation"]["reservation_starttime"];
        }
        array_multisort( $key_id , SORT_ASC , $meeting_list);
        return $meeting_list;
    }

    private  $payment_type_organizer = 1;
    private  $payment_type_participant = 2;

    private  $type_organizer = 1;
    private  $type_participant = 2;

    private  $type_PC = 0;
    private  $type_Mobile = 1;
    private  $type_TV = 2;


    /**
    * getMyMeetingListで取得した情報を渡す
    * $meeting_info[meeting]
    * $meeting_info[reservation]
    */
    function getLog($member_info , $meeting_info){
        require_once("classes/dbi/member.dbi.php");
        $log_data = array();
        // 自分の会議が確認
        if($meeting_info["reservation"]["member_key"] != $member_info["member_key"]){
            //他の人の会議で主催者負担だった場合は履歴なし
            if($meeting_info["reservation"]["payment_type"] == 1){
                return false;
            }else if($meeting_info["reservation"]["payment_type"] == 2){
                // 開催時間の計算
                $secounds  = strtotime($meeting_info["reservation"]["reservation_extend_endtime"]) - strtotime($meeting_info["reservation"]["reservation_starttime"]);
                $minute = $secounds / 60;
                $where = "use_count != 0 AND member_key = " .$member_info["member_key"]. " AND meeting_key = " . $meeting_info["meeting"]["meeting_key"];
                // 参加情報取得
                $my_participant_list  = $this->obj_Participant->getRowsAssoc($where);
                $my_participant_list = $this->_sameMemberInMeeting($my_participant_list);
                $obj_member = new MemberTable($this->dsn);
                $organizer_info = $obj_member->getRow("member_key = " . $meeting_info["reservation"]["member_key"]);
                if($meeting_info["reservation"]["reservation_extend_flg"]){
                    require_once("classes/dbi/reservation_extend.dbi.php");
                    // 予約拡張取得
                    $obj_reservation_extend = new ReservationExtendTable($this->dsn);
                    $where = "reservation_session_id = '" .mysql_real_escape_string($meeting_info["reservation"]["reservation_session"]) . "'";
                    $sort = array(
                            "extend_endtime" => "DESC"
                    );
                    $extend_infos = $obj_reservation_extend->getRowsAssoc($where , $sort );
                    // 終了時間、人数を拡張
                    $meeting_info["reservation"]["reservation_endtime"] = ($extend_infos[0]["extend_endtime"]);
                    $extend_time = 0;
                    foreach($extend_infos as $extend_info){
                        $extend_time = $extend_time + ($extend_info["extend_seat"] * (strtotime($meeting_info["reservation"]["reservation_endtime"]) - strtotime($extend_info["extend_starttime"])) / 60);
                        // 拡張された場合のビデオ会議
                        if($extend_info["extend_videoconference"]){
                            $use_videoconference = 1;
                            $videoconference_start_time = $extend_info["extend_starttime"];
                        }
                        $extend_seat = $extend_seat + $extend_info["extend_seat"];
                    }
                }
                foreach($my_participant_list as $my_participant){

                    // 参加者の利用時間が会議時間より長かったら会議時間に合わせる
                    if($my_participant["use_count"] > $minute){
                        $my_participant["use_count"] = $minute;
                    }
                    $log_data[] = $this->_setLogDate($meeting_info["meeting"]["meeting_name"] , $organizer_info["member_name"] ,
                                       strtotime($meeting_info["reservation"]["reservation_starttime"])  , strtotime($meeting_info["reservation"]["reservation_extend_endtime"]),
                                       $meeting_info["meeting"]["meeting_max_seat"] + $extend_seat, // 接続台数
                                       $this->payment_type_participant , $this->type_participant , $member_info["member_name"] , $member_info["member_name"],$my_participant["use_count"] , $my_participant["is_mobile"],
                                       $meeting_info["meeting"]["meeting_max_seat"] + $extend_seat,null , $meeting_info["reservation"]["mcu_down_flg"]);
                }

                return $log_data;
            }
        }

        return $this->_getMyMeetingLog($member_info , $meeting_info);
    }

    function getAdminMeetingLog($param){
        if (isset($param["start_time"]) || isset($param["end_time"])) {
            $start_limit = $param["start_time"] ? mysql_real_escape_string($param["start_time"]) : "0000-00-00 00:00:00";
            $end_limit   = $param["end_time"] ? mysql_real_escape_string($param["end_time"]) : "9999-12-31 23:59:59";
            $where_reservation = "(" .
            "( reservation_starttime between '$start_limit' AND '$end_limit' )" .
            " OR ( reservation_extend_endtime between '$start_limit' AND '$end_limit' )" .
            " OR ( '$start_limit' BETWEEN reservation_starttime AND reservation_extend_endtime )" .
            " OR ( '$end_limit' BETWEEN reservation_starttime AND reservation_extend_endtime ) ) AND ";
        }

        $where_reservation = $where_reservation . " NOW() > reservation_extend_endtime AND  reservation_status = 1 AND user_key = " . $param["user_key"];
        $reservation_list = $this->obj_Reservation->getRowsAssoc($where_reservation);
        $log_data = array();
        foreach($reservation_list as $reservation){
            $meeting = $this->obj_Meeting->getRow("meeting_ticket = '" . $reservation["meeting_key"]."'" , "meeting_name, meeting_ticket , meeting_key , room_key , meeting_session_id, meeting_max_seat");
            $meeting_array["meeting"] = $meeting;
            $meeting_array["reservation"] = $reservation;
            $obj_member = new MemberTable($this->dsn);
            if($reservation["member_key"]){
                $organizer_info = $obj_member->getRow("member_key = " . $reservation["member_key"]);
                $log_data = array_merge($log_data , $this->_getMyMeetingLog($organizer_info , $meeting_array , true));
            }
        }

        return $log_data;
    }

    /**
    * getMyMeetingListで取得した情報を渡す
    * $meeting_info[meeting]
    * $meeting_info[reservation]
    */

    private function _getMyMeetingLog($member_info , $meeting_info , $is_admin = false){
        $admin_add_log = null;
        $log_data = array();
        // ビデオ会議レコード用
        $use_videoconference = $meeting_info["reservation"]["use_videoconference"];
        $videoconference_start_time = $meeting_info["reservation"]["reservation_starttime"];
        require_once("classes/dbi/member.dbi.php");
        $extend_seat = 0;
        if($meeting_info["reservation"]["reservation_extend_flg"]){
            require_once("classes/dbi/reservation_extend.dbi.php");
            // 予約拡張取得
            $obj_reservation_extend = new ReservationExtendTable($this->dsn);
            $where = "reservation_session_id = '" .mysql_real_escape_string($meeting_info["reservation"]["reservation_session"]) . "'";
            $sort = array(
                "extend_endtime" => "DESC"
            );
            $extend_infos = $obj_reservation_extend->getRowsAssoc($where , $sort );
            // 終了時間、人数を拡張
            $meeting_info["reservation"]["reservation_endtime"] = ($extend_infos[0]["extend_endtime"]);
            $extend_time = 0;
            foreach($extend_infos as $extend_info){
                $extend_time = $extend_time + ($extend_info["extend_seat"] * (strtotime($meeting_info["reservation"]["reservation_endtime"]) - strtotime($extend_info["extend_starttime"])) / 60);
                // 拡張された場合のビデオ会議
                if($extend_info["extend_videoconference"]){
                    $use_videoconference = 1;
                    $videoconference_start_time = $extend_info["extend_starttime"];
                }
                $extend_seat = $extend_seat + $extend_info["extend_seat"];
            }
        }
        // 終了時間は延長も考慮
        $videoconference_end_time = $meeting_info["reservation"]["reservation_endtime"];
        // 5/7前の仕様、念のため書いておく
        if($meeting_info["reservation"]["payment_type"] == null){
            // 開催時間の計算
            $secounds  = strtotime($meeting_info["reservation"]["reservation_endtime"]) - strtotime($meeting_info["reservation"]["reservation_starttime"]);
            $use_minute = ($secounds * $meeting_info["meeting"]['meeting_max_seat'] / 60) + $extend_time;
            $connected_count = $extend_port + $meeting_info["meeting"]['meeting_max_seat'];
            if($is_admin){
                $admin_add_log = array(
                    "participant_member_id" => $member_info["member_id"],
                    "payment_member_id"=> $member_info["member_id"],
                    "payment_member_accounting_unit_code"=> $member_info["accounting_unit_code"],
                    "payment_member_expense_department_code"=> $member_info["expense_department_code"],
                );
            }
            //全会議時間取得
            $log_data[] = $this->_setLogDate($meeting_info["meeting"]["meeting_name"] , $member_info["member_name"] ,
                          strtotime($meeting_info["reservation"]["reservation_starttime"]) , strtotime($meeting_info["reservation"]["reservation_endtime"]),
                          $meeting_info["meeting"]["meeting_max_seat"] + $extend_seat, // 接続台数
                          $this->payment_type_organizer , $this->type_organizer , $member_info["member_name"] , $member_info["member_name"] , $use_minute , 0 ,
                          $meeting_info["meeting"]["meeting_max_seat"] + $extend_seat,$admin_add_log , $meeting_info["reservation"]["mcu_down_flg"]);
        // 主催者負担 利用者負担
        }else if($meeting_info["reservation"]["payment_type"] == 1|| $meeting_info["reservation"]["payment_type"] == 2){
            // 会議参加者情報取得(テレビ会議以外）
            $participant_list  = $this->obj_Participant->getRowsAssoc("use_count != 0 AND participant_protocol != 'sip' AND meeting_key = " . $meeting_info["meeting"]["meeting_key"]);

            //$log_data[] = $participant_list;
            // 端末毎に累計にする
            $participant_list = $this->_sameMemberInMeeting($participant_list);

//$this->logger->info($check_participant_list);
//$participant_list = $check_participant_list;
            $participant_count_total = 0;
            // 開催時間の計算
            $secounds  = strtotime($meeting_info["reservation"]["reservation_endtime"]) - strtotime($meeting_info["reservation"]["reservation_starttime"]);
            $minute = $secounds / 60;
            $use_time = $secounds * $meeting_info["meeting"]['meeting_max_seat'] / 60;
            foreach($participant_list as $participant){
                // 参加情報取得
                if($participant["member_key"]){
                    $obj_member = new MemberTable($this->dsn);
                    $participant_member_info = $obj_member->getRow("member_key = " . $participant["member_key"]);
                    $participant_name = $participant_member_info["member_name"];
                    $payment_name = $member_info["member_name"];
                    // 利用者負担だったら
                    if($meeting_info["reservation"]["payment_type"] == 2){
                        $payment_name  = $participant_member_info["member_name"];
                    }
                    if($is_admin){
                        $admin_add_log = array(
                            "participant_member_id" => $participant_member_info["member_id"],
                            "payment_member_id"=> $member_info["member_id"],
                            "payment_member_accounting_unit_code"=> $member_info["accounting_unit_code"],
                            "payment_member_expense_department_code"=> $member_info["expense_department_code"],
                        );
                        // 利用者負担だったら
                        if($meeting_info["reservation"]["payment_type"] == 2){
                            $admin_add_log = array(
                                "participant_member_id" => $participant_member_info["member_id"],
                                "payment_member_id"=> $participant_member_info["member_id"],
                                "payment_member_accounting_unit_code"=> $participant_member_info["accounting_unit_code"],
                                "payment_member_expense_department_code"=> $participant_member_info["expense_department_code"],
                            );
                        }
                    }
                }else{
                    $participant_name = "";
                    $payment_name = $member_info["member_name"];
                    if($is_admin){
                        $admin_add_log = array(
                            "participant_member_id" => "",
                            "payment_member_id"=> $member_info["member_id"],
                            "payment_member_accounting_unit_code"=> $member_info["accounting_unit_code"],
                            "payment_member_expense_department_code"=> $member_info["expense_department_code"],
                        );
                    }
                }
                // 参加者の参加時間が会議時間より長かったら会議時間に合わせる
                if($participant["use_count"] > $minute){
                    $participant["use_count"] = $minute;
                }

                // 端末区分 PC(0) or モバイル(1) or テレビ会議(2）のチェック
                if($participant["is_mobile"] ==  $this->type_PC){
                    $is_mobile_check = $this->type_PC;
                }elseif($participant["is_mobile"] == $this->type_Mobile && $participant["participant_protocol"] !== "sip"){
                    $is_mobile_check = $this->type_Mobile;
                }else{
                    $is_mobile_check = $this->type_TV;
                }

                $log_data[] = $this->_setLogDate($meeting_info["meeting"]["meeting_name"] , $member_info["member_name"] ,
                         strtotime($meeting_info["reservation"]["reservation_starttime"]) , strtotime($meeting_info["reservation"]["reservation_endtime"]),
                         $meeting_info["meeting"]["meeting_max_seat"] + $extend_seat, // 接続台数
                         $meeting_info["reservation"]["payment_type"] , $this->type_participant , $participant_name , $payment_name,$participant["use_count"] , $is_mobile_check,
                         $meeting_info["meeting"]["meeting_max_seat"] + $extend_seat,$admin_add_log , $meeting_info["reservation"]["mcu_down_flg"]);

                $participant_count_total = $participant_count_total + $participant["use_count"];
                }

            if($is_admin){
                $admin_add_log = array(
                    "participant_member_id" => $member_info["member_id"],
                    "payment_member_id"=> $member_info["member_id"],
                    "payment_member_accounting_unit_code"=> $member_info["accounting_unit_code"],
                    "payment_member_expense_department_code"=> $member_info["expense_department_code"],
                );
            }
            $log_data[] = $this->_setLogDate($meeting_info["meeting"]["meeting_name"] , $member_info["member_name"] ,
              strtotime($meeting_info["reservation"]["reservation_starttime"]) , strtotime($meeting_info["reservation"]["reservation_endtime"]),
              $meeting_info["meeting"]["meeting_max_seat"] + $extend_seat, // 接続台数
              $meeting_info["reservation"]["payment_type"] , $this->type_organizer , "" ,$member_info["member_name"],($use_time - $participant_count_total + $extend_time) , $this->type_PC,
              $meeting_info["meeting"]["meeting_max_seat"] + $extend_seat,$admin_add_log , $meeting_info["reservation"]["mcu_down_flg"]);
        }

        // ビデオ会議分のレコードを追加
        if($use_videoconference){
            $videoconference_time = (strtotime($videoconference_end_time) - strtotime($videoconference_start_time))/60;
            $log_data[] = $this->_setLogDate($meeting_info["meeting"]["meeting_name"] , $member_info["member_name"] ,
                    strtotime($meeting_info["reservation"]["reservation_starttime"]) , strtotime($meeting_info["reservation"]["reservation_endtime"]),
                    $meeting_info["meeting"]["meeting_max_seat"] + $extend_seat, // 接続台数
                    $meeting_info["reservation"]["payment_type"] , $this->type_participant , "" ,$member_info["member_name"],($videoconference_time) , $this->type_TV,
                    $meeting_info["meeting"]["meeting_max_seat"] + $extend_seat,$admin_add_log , $meeting_info["reservation"]["mcu_down_flg"]);
        }
        return $log_data;
    }

    // 参加者リストから同じmember、同じモバイルフラグの場合累計にする
    private function _sameMemberInMeeting($participant_list){
        $participant_list_count = count($participant_list);
        $check_participant_list = array();
        for($i= 0; $i < $participant_list_count; $i++){
            $list_add_flag = true;

            if($participant_list[$i]["member_key"]){
                $z = 0;
                $cp_count = count($check_participant_list);

                for($z= 0; $z < $cp_count; $z++){
                    if($check_participant_list[$z]["member_key"] == $participant_list[$i]["member_key"] && $check_participant_list[$z]["is_mobile"] == $participant_list[$i]["is_mobile"]){
                        $check_participant_list[$z]["use_count"] = $check_participant_list[$z]["use_count"] + $participant_list[$i]["use_count"];
                        $list_add_flag = false;
                        break;
                    }
                }
            }
            if($list_add_flag ){
                $check_participant_list[] = $participant_list[$i];
            }
        }
        return $check_participant_list;
    }

    private function _setLogDate ($meeting_name , $organizer_name , $meeting_start_date, $meeting_end_date , $connected_count , $payment_type , $type , $participant_name ,$payment_name , $time , $terminal_type, $max_port , $admin_add_log=null , $mcu_down_flg = 0){
            $log_data["meeting_name"] = $meeting_name;
            $log_data["organizer_name"] = $organizer_name;
            $log_data["meeting_start_date"] = $meeting_start_date;
            $log_data["meeting_end_date"] = $meeting_end_date;
            $log_data["connected_count"] = $connected_count;
            $log_data["payment_type"] = $payment_type;
            $log_data["type"] = $type;
            $log_data["participant_name"] = $participant_name;
            $log_data["payment_name"] = $payment_name;
            // 開催時間の計算
            $secounds  = strtotime($meeting_info["reservation"]["reservation_endtime"]) - strtotime($meeting_info["reservation"]["reservation_starttime"]);
            if($time < 0){
                $time = 0;
            }
            $log_data["time"] = $time;
            $log_data["terminal_type"] = $terminal_type;
            $log_data["mcu_down_flg"] = $mcu_down_flg;
            $log_data["max_port"] = $max_port;
            if($admin_add_log){
                $log_data = array_merge($log_data , $admin_add_log);
            }
            return $log_data;
    }

    /**
     * 検索条件に一致するログ一覧を取得
     * @param array $param 検索条件の連想配列
     * @param boolean $del_flg 削除フラグ
     */
     function getList($param, $del_flg = false) {
        $param = array(
            "title"             => $param["title"],
            "participant"       => $param["participant"],
            "user_key"          => $param["user_key"],
            "room_key"          => $param["room_key"],
            "member_key"        => $param["member_key"],
            "meeting_tag"       => $param["meeting_tag"],
            "sort_key"          => isset($param["sort_key"]) ? $param["sort_key"] : "",
            "sort_type"         => isset($param["sort_type"]) ? $param["sort_type"] : "",
            "limit"             => $param["limit"],
            "offset"            => $param["offset"],
            "start_datetime"    => $param["start_datetime"],
            "end_datetime"      => $param["end_datetime"],
        );

        return $del_flg ? $this->Core_Meeting->getMeetingInfoListAll( $param ) :
                                $this->Core_Meeting->getMeetingInfoList( $param );
     }

     function getCount($param, $del_flg = false) {
        $param = array(
            "title"             => $param["title"],
            "participant"       => $param["participant"],
            "room_key"          => $param["room_key"],
            "member_key"        => $param["member_key"],
            "meeting_tag"       => $param["meeting_tag"],
            "sort"              => isset($param["sort"]) ? $param["sort"] : "",
            "sort_type"         => isset($param["sort_type"]) ? $param["sort_type"] : "",
            "limit"             => $param["limit"],
            "offset"            => $param["offset"],
            "start_datetime"    => $param["start_datetime"],
            "end_datetime"      => $param["end_datetime"]
        );
        if ($del_flg) {
            $param["delete"] = 1;
        }
        return $this->Core_Meeting->getCount( $param );
     }

    /**
     * タイトル変更
     * core/htdocs/api/meeting/MeetingRename.php
     */
    function editTitle( $room_key, $meeting_key, $title )
    {
        $where = "room_key = '".addslashes( $room_key )."'" .
                " AND meeting_session_id = '".addslashes( $meeting_key )."'" .
                " AND is_active = 0".
                " AND is_deleted = 0";
        $meeting_key = $this->obj_Meeting->getOne( $where, "meeting_key" );
        if ($meeting_key) {
            $data = array( "meeting_name" => $title );
            $where = sprintf( "meeting_key='%s'", $meeting_key );
            $this->obj_Meeting->update( $data, $where );
        }
    }

    /**
     * パスワード設定
     */
    function setPassword($room_key, $meeting_key, $password, $owner = "")
    {
        $where = "room_key = '".addslashes( $room_key )."'" .
                " AND meeting_session_id = '".addslashes( $meeting_key )."'" .
                " AND is_active = 0".
                " AND is_deleted = 0";
        $meeting_key = $this->obj_Meeting->getOne($where, "meeting_key");

        if ( $meeting_key ) {
            $data = array(
                        "meeting_log_owner" => $owner,
                        "meeting_log_password" => $password
                    );
            $where = sprintf( "meeting_key='%s'", $meeting_key );
            $this->obj_Meeting->update( $data, $where );
        }
    }

    /**
     * パスワード解除
     */
    function unsetPassword($room_key, $meeting_key)
    {
        $where = "room_key = '".addslashes( $room_key )."'" .
                " AND meeting_session_id = '".addslashes( $meeting_key )."'" .
                " AND is_active = 0".
                " AND is_deleted = 0";
        $meeting_key = $this->obj_Meeting->getOne($where, "meeting_key");

        if ( $meeting_key ) {
            $data = array("meeting_log_password" => "");
            $where = sprintf( "meeting_key='%s'", $meeting_key );
            $this->obj_Meeting->update( $data, $where );
        }
    }

    //パスワード認証
    function checkPassword($room_key, $meeting_key, $password, $owner = "")
    {
        $where = "room_key = '".addslashes( $room_key )."'" .
                " AND meeting_session_id = '".addslashes( $meeting_key )."'" .
                " AND is_active = 0".
                " AND is_deleted = 0";
        $meeting_data = $this->obj_Meeting->getRow($where, "meeting_key, meeting_log_password");
        if (DB::isError($meeting_data) || empty($meeting_data) ) {
            $this->logger->error("meeting data not found : meeting_key:".$meeting_key);
            return false;
        }
        if ( $meeting_data["meeting_log_password"] == $password) {
            return true;
        } else {
            $this->logger->info("meeting password not match : meeting_key:".$meeting_key);
            return false;
        }
    }

    /**
     * 保護設定
     * core/htdocs/api/meeting_log/LogLock.php
     */
    function setProtect($room_key, $meeting_session_id)
    {
        $where = "room_key = '".addslashes( $room_key )."'" .
                " AND meeting_session_id = '".addslashes( $meeting_session_id )."'" .
                " AND is_active = 0".
                " AND is_deleted = 0".
                " AND is_locked = 0";
        $meeting_key = $this->obj_Meeting->getOne($where, "meeting_key");
        if ($meeting_key) {
            $data = array( "is_locked" => 1 );
            $where = sprintf( "meeting_key='%s'", $meeting_key );
            $this->obj_Meeting->update( $data, $where );
        }
    }

    /**
     * 保護解除
     */
    function unsetProtect($room_key, $meeting_session_id)
    {
        $where = "room_key = '".addslashes( $room_key )."'" .
                " AND meeting_session_id = '".addslashes( $meeting_session_id )."'" .
                " AND is_active = 0".
                " AND is_deleted = 0".
                " AND is_locked = 1";
        $meeting_key = $this->obj_Meeting->getOne($where, "meeting_key");
        if ($meeting_key) {
            $data = array( "is_locked" => 0 );
            $where = sprintf( "meeting_key='%s'", $meeting_key );
            $this->obj_Meeting->update( $data, $where );
        }
    }

    public function controlPublish($room_key, $meeting_session_id, $publish=0)
    {
        $where = "room_key = '".addslashes( $room_key )."'" .
                " AND meeting_session_id = '".addslashes( $meeting_session_id )."'" .
                " AND is_active = 0".
                " AND is_deleted = 0".
                " AND is_publish = '".addslashes( $publish )."'";
        $meeting_key = $this->obj_Meeting->getOne($where, "meeting_key");
        if ($meeting_key) {
            $is_publish = $publish ? 0 : 1;
            $data = array( "is_publish" => $is_publish );
            $where = sprintf( "meeting_key='%s'", $meeting_key );
            $this->obj_Meeting->update( $data, $where );
        }
    }

    /**
     * 会議の削除
     * api/meeting_log/LogDelete.php
     */
    function delete( $room_key, $meeting_session_id )
    {
        $where = "room_key = '".addslashes( $room_key )."'" .
                " AND meeting_session_id = '".addslashes( $meeting_session_id )."'" .
//                " AND meeting_key = '".addslashes( $meeting_key )."'" .
                " AND is_active = 0".
                " AND is_deleted = 0".
                " AND is_locked = 0";
        $meetingInfo = $this->obj_Meeting->getRow( $where );
        if ($meetingInfo) {
            $data = array(
                            "is_deleted"=> 1,
                            "is_active" => 0
                            );
            $where = sprintf( "meeting_key='%s'", $meetingInfo["meeting_key"] );
            $result = $this->obj_Meeting->update( $data, $where );
            // 映像の削除
            $this->deleteVideo( $meetingInfo["meeting_session_id"] );
            // 議事録の削除
            $this->deleteMinutes( $meetingInfo["meeting_session_id"] );
            // ファイルキャビネットの削除
            $this->Core_Meeting->deleteCabinet( $meetingInfo["meeting_key"] );
            // 動画生成データの削除
            $where = sprintf( "meeting_key='%s'", $meetingInfo["meeting_key"] );
            $sequenceList = $this->obj_MeetingSequence->getRowsAssoc( $where );
            foreach ($sequenceList as $sequence_info ) {
                if ($sequence_info["record_status"] == "complete" && $sequence_info["record_filepath"]) {
                    if (file_exists(N2MY_MOVIES_DIR.$sequence_info["record_filepath"])) {
                        $this->logger->info(N2MY_MOVIES_DIR.$sequence_info["record_filepath"], "delete movie");
                        unlink(N2MY_MOVIES_DIR.$sequence_info["record_filepath"]);
                    }
                }
            }
        }
    }

    /**
     * 映像の削除
     */
    public function deleteVideo( $meeting_session_id )
    {
        $flag = true;
        $meetingInfo = $this->obj_Meeting->getRow( sprintf( "meeting_session_id='%s'", $meeting_session_id  ) );
        if( ! $meetingInfo || $meetingInfo["is_locked"] ) return;

        $where = sprintf( "meeting_key='%s' AND has_recorded_video=1", $meetingInfo["meeting_key"] );
        $sequenceList = $this->obj_MeetingSequence->getRowsAssoc( $where );
        for( $i = 0; $i < count( $sequenceList ); $i++ ){
            $this->Core_Meeting->deleteVideo( $sequenceList[$i]["meeting_sequence_key"] );
        }
        // 会議容量
        $meeting_sequence_data = $this->obj_MeetingSequence->getStatus( $meetingInfo["meeting_key"] );
        // 資料容量
        if (($meeting_sequence_data['has_recorded_minutes'] == 0) && ($meeting_sequence_data['has_recorded_video'] == 0)) {
            // 通常
            $objDocument = new N2MY_Document($this->dsn);
            $where = "meeting_key = ". $meetingInfo["meeting_key"];
            $document_list = $objDocument->getList($where);
            $this->logger->info(__FUNCTION__,__FILE__,__LINE__,$document_list);
            foreach($document_list as $document) {
                // 物理削除
                if(!$objDocument->delete($document)) {
                    $flag = false;
                }
            }
            $document_size = 0;
        } else {
            $document_size = $this->Core_Meeting->getDocumentSize($meetingInfo["meeting_key"]);
        }
        // キャビネット容量
        $cabinet_size = 0;
        $this->logger->info(array(
            "meeting_size_used" => $meeting_sequence_data["meeting_size_used"],
            "document_size" => $document_size,
            "cabinet_size" => $cabinet_size
        ));
        $meetingData = array(
            "has_recorded_video" => $meeting_sequence_data['has_recorded_video'],
            "meeting_size_used" => $meeting_sequence_data["meeting_size_used"] + $document_size + $cabinet_size,
            "update_datetime" => date( "Y-m-d H:i:s" )
        );
        $this->logger->info($meetingData);
        $this->obj_Meeting->update( $meetingData, $where );
        return $flag;
    }

    /**
     * 議事録の削除
     */
    public function deleteMinutes( $meeting_session_id )
    {
        $flag = true;
        $meetingInfo = $this->obj_Meeting->getRow( sprintf( "meeting_session_id='%s'", $meeting_session_id ) );
        if( ! $meetingInfo || $meetingInfo["is_locked"] ) {
            $this->logger->info($meeting_session_id,"meeting is locked");
            return;
        }
        $where = sprintf( "meeting_key='%s' AND has_recorded_minutes=1", $meetingInfo["meeting_key"] );
        $sequenceList = $this->obj_MeetingSequence->getRowsAssoc( $where );
        for( $i = 0; $i < count( $sequenceList ); $i++ ){
            $this->Core_Meeting->deleteMinute( $sequenceList[$i]["meeting_sequence_key"] );
        }
        // 会議容量
        $meeting_sequence_data = $this->obj_MeetingSequence->getStatus( $meetingInfo["meeting_key"] );
        // 資料容量
        if (($meeting_sequence_data['has_recorded_minutes'] == 0) && ($meeting_sequence_data['has_recorded_video'] == 0)) {
            // 通常
            $objDocument = new N2MY_Document($this->dsn);
            $where = "meeting_key = ". $meetingInfo["meeting_key"];
            $document_list = $objDocument->getList($where);
            $this->logger->info(__FUNCTION__,__FILE__,__LINE__,$document_list);
            foreach($document_list as $document) {
                // 物理削除
                if(!$objDocument->delete($document)) {
                    $flag = false;
                }
            }
            $document_size = 0;
        } else {
            $document_size = $this->Core_Meeting->getDocumentSize($meetingInfo["meeting_key"]);
        }
        // キャビネット容量
        $cabinet_size = 0;
        $this->logger->info(array(
            "meeting_size_used" => $meeting_sequence_data["meeting_size_used"],
            "document_size" => $document_size,
            "cabinet_size" => $cabinet_size
        ));
        $meetingData = array(
            "has_recorded_minutes"  => $meeting_sequence_data['has_recorded_minutes'],
            "meeting_size_used"     => $meeting_sequence_data["meeting_size_used"] + $document_size + $cabinet_size,
            "update_datetime"       => date( "Y-m-d H:i:s" )
        );
        $this->obj_Meeting->update( $meetingData, $where );
        return $flag;
    }

    /**
     * 共有メモ削除
     * @param $meeting_key
     * @param $meeting_sequence_key
     * @param $shared_file_key
     */
    public function deleteSharedFile($meeting_key , $meeting_sequence_key , $shared_file_key){
        require_once("classes/dbi/shared_file.dbi.php");
        $objSharedFile = new sharedFileTable($this->dsn);
        $data = array(
                "status" => 0,
                "update_datetime" => date('Y-m-d H:i:s'),
        );
        $where = sprintf("shared_file_key='%s' AND meeting_key='%s' AND meeting_sequence_key='%s'",  $shared_file_key , $meeting_key , $meeting_sequence_key);
        $objSharedFile->update($data, $where);

        //ミーティングシーケンスのメモ数確認
        $where = sprintf("status = 1 AND meeting_sequence_key='%s'",  $meeting_sequence_key);
        $shard_file_count = $objSharedFile->numRows($where);
        // ミーティングシーケンスでメモの数が0だったらテーブル更新
        if($shard_file_count == 0){
            $data = array(
                    "has_shared_memo" => 0,
                    "update_datetime" => date('Y-m-d H:i:s'),
            );
            $where = sprintf("meeting_sequence_key='%s'",  $meeting_sequence_key);
            $this->obj_MeetingSequence->update($data, $where);
        }
        // ミーティングでのメモ数確認
        $where = sprintf("status = 1 AND meeting_key='%s'",  $meeting_key);
        $shard_file_count = $objSharedFile->numRows($where);
        if($shard_file_count == 0){
            $data = array(
                    "has_shared_memo" => 0,
                    "update_datetime" => date('Y-m-d H:i:s'),
            );
            $where = sprintf("meeting_key='%s'",  $meeting_key);
            $this->obj_Meeting->update($data, $where);
        }
    }

    /**
     * 指定した分サイズを引く
     * @param $meeting_sequence_key
     * @param int $size
     */
    public function shorten_meeting_size_used($meeting_sequence_key , $size){
        // サイズが数字じゃなかったらなにもしない
        if(!is_numeric($size)){
            return ;
        }

        $where = sprintf("meeting_sequence_key='%s'",  $meeting_sequence_key);
        $meeting_sequence_info = $this->obj_MeetingSequence->getRow($where);
        $new_size = $meeting_sequence_info["meeting_size_used"] - $size;
        // マイナス防止策
        if($new_size < 0){
            $new_size = 0;
        }
        $data = array(
                "meeting_size_used" => $new_size,
                "update_datetime" => date('Y-m-d H:i:s'),
                );
        $this->obj_MeetingSequence->update($data, $where);

    }

    /**
     * 会議の容量設定＆取得
     */
    public function getMeetingSize($meeting_session_id) {
        $meetingInfo = $this->obj_Meeting->getRow( sprintf( "meeting_session_id='%s'", $meeting_session_id ) );
        // 会議容量
        $meeting_sequence_data = $this->obj_MeetingSequence->getStatus( $meetingInfo["meeting_key"] );
        // 資料容量
        if (($meeting_sequence_data['has_recorded_minutes'] == 0) && ($meeting_sequence_data['has_recorded_video'] == 0)) {
            $document_size = 0;
        } else {
            $document_size = $this->Core_Meeting->getDocumentSize($meetingInfo["meeting_key"]);
        }
        // キャビネット容量
        $cabinet_size = 0;
        // 録画GW容量
        $record_gw_size = 0;
        $where = "meeting_key='".addslashes($meetingInfo["meeting_key"])."'";
        $sequenceList = $this->obj_MeetingSequence->getRowsAssoc( $where );
        foreach ($sequenceList as $key => $sequence_info) {
            if ($sequence_info["record_status"] == "complete") {
                if (file_exists(N2MY_MOVIES_DIR.$sequence_info["record_filepath"])) {
                    $record_gw_size += filesize(N2MY_MOVIES_DIR.$sequence_info["record_filepath"]);
                } else {
                    $this->logger->warn(N2MY_MOVIES_DIR.$sequence_info["record_filepath"], "file not exists");
                }
            }
        }
        $this->logger->info(array(
            "meeting_size_used" => $meeting_sequence_data["meeting_size_used"],
            "document_size"     => $document_size,
            "cabinet_size"      => $cabinet_size,
            "record_gw_size"    => $record_gw_size,
        ));

        $meeting_size = $meeting_sequence_data["meeting_size_used"] + $document_size + $cabinet_size + $record_gw_size;
        $meetingData = array(
            "meeting_size_used" => $meeting_size,
            "update_datetime" => date( "Y-m-d H:i:s" )
        );
        $where = "meeting_key = ". $meetingInfo["meeting_key"];
        $this->obj_Meeting->update( $meetingData, $where );
        return $meeting_size;
    }
}
