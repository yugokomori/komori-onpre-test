<?php
require_once 'classes/AppFrame.class.php';
require_once 'classes/N2MY_Enquete.class.php';

class EnqueteSummary extends AppFrame {

    var $enqete_list = array();
    var $objEnquete = null;

    function init() {
        $this->objEnquete = new N2MY_Enquete($this->config->get('GLOBAL', 'enquete_dsn'));
    }

    function default_view() {
        $this->action_enquete_list();
    }

    function action_enquete_list() {
        $this->render_enquete_list();
    }

    function render_enquete_list() {
        $enquete_list = $this->objEnquete->getList();
        foreach ($enquete_list as $enquete) {
            print '<a href="?action_answer=&id='.basename($enquete).'">'.basename($enquete).'</a><br />';
        }
    }

    function action_answer() {
        $id = $this->request->get('id');
        $this->objEnquete->init($id);
        $model = $this->objEnquete->getModel();
        $list = $this->objEnquete->getAnswer();
        $this->template->assign('model', $model);
        $this->template->assign('rows', $list);
        $this->display('admin_tool/enquete.t.html');
    }
}

$main = new EnqueteSummary();
$main->execute();