<?php
require_once("classes/N2MY_Api.class.php");
class N2MY_MeetingAdminAuthAPI extends N2MY_Api
{
    /**
     * 初期化
     *
     * @param
     * @return
     */
    function init()
    {
        // SSL対応
        header('Pragma:');
    }

    function auth()
    {
        // ユーザ権限の認証は行う
        $this->checkAuthorization();
    }

    /**
     * ログイン処理
     */
    function action_login()
    {
        $admin_pw = $this->request->get("admin_pw");
        $user_info = $this->session->get('user_info');
        require_once 'lib/EZLib/EZUtil/EZEncrypt.class.php';
        $user_admin_password = EZEncrypt::decrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $user_info["user_admin_password"]);
        if ($user_admin_password != $admin_pw) {
            return $this->display_error(1, "Login failed / Invalid auth token");
        }
        $this->session->set('admin_login', 1);
        $this->add_operation_log("admin_login");
        return $this->output();
    }

    /**
     * ログアウト処理
     */
    function action_logout()
    {
        $this->checkAuthorizationAdmin();
        $this->session->remove('admin_login');
        $this->add_operation_log("admin_logout");
        return $this->output();
    }

    /**
     * 管理者パスワード変更
     */
    function action_change_password() {
        $this->checkAuthorizationAdmin();
        $new_passwd = $this->request->get("admin_pw");
        $rules = array(
            "admin_pw"     => array(
                "required" => true,
                "range" => array(8, 128), // 6～10文字
                ),
        );
        $data = array(
            "admin_pw"     => $new_passwd,
        );
        $err_obj = $this->error_check($data, $rules);
        if (!preg_match('/^[!-\[\]-~]{8,128}$/', $new_passwd)) {
            $err_obj->set_error("pw", USER_ERROR_PASS_INVALID_01);
        } elseif (!preg_match('/[[:alpha:]]+/', $new_passwd) || preg_match('/^[[:alpha:]]+$/', $new_passwd)) {
            $err_obj->set_error("pw", USER_ERROR_PASS_INVALID_02);
        }
        if (EZValidator::isError($err_obj)) {
            return $this->display_error(100, "PARAMETER_ERROR", $this->get_error_info($err_obj));
        }
        $user_info = $this->session->get("user_info");
        require_once("classes/dbi/user.dbi.php");
        $objUser = new UserTable($this->get_dsn());
        $result = $objUser->changeAdminPasswd($user_info["user_key"], $new_passwd);
        $user_info = $this->session->get('user_info');
        $user_info["user_admin_password"] = EZEncrypt::encrypt(VCUBE_ENCRYPT_KEY, VCUBE_ENCRYPT_IV, $new_passwd);
        $this->session->set('user_info',$user_info);
        $this->logger->debug($result);
        $this->add_operation_log("change_admin_password");
        return $this->output();
    }
}
$main = new N2MY_MeetingAdminAuthAPI();
$main->execute();