<?php
/*
 * Created on 2006/09/28
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

// 実行時間を無期限
require_once("classes/mgm/dbi/meeting_date_log.dbi.php");
require_once("classes/dbi/room.dbi.php");
require_once("classes/AppFrame.class.php");

class AppBatch extends AppFrame
{

    var $date_log = null;

    function init()
    {
        $this->date_log = new MeetingDateLogTable($this->get_dsn(), $this->base_url, $this->provider_id, $this->provider_pw);
        $this->room = new RoomTable($this->get_dsn());
    }
    function default_view()
    {
        $this->action_day_summary();
    }

    /**
     * 利用状況集計
     */
    function action_day_summary() {
        // デフォルト2日前のデータを取得
        $date = $this->request->get("date", date("Y-m-d", time() - (3600 * 24 * 2)));
        $room_key = $this->get_room_key();
        // 部屋
        $this->logger->info(__FUNCTION__, __FILE__, __LINE__,$date);
        if ($room_key) {
            $ret = $this->_summary($date);
        } else {
            $ret = $this->_summary($date);
        }
    }

    /**
     * 日別集計
     */
    function _summary($date, $room_key = null)
    {
        $ret = $this->date_log->day_update($date, $room_key);
        return $ret;
    }

}

$main = new AppBatch();
$main->execute();

?>
