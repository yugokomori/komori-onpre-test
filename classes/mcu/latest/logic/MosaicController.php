<?php 
require_once(dirname(__FILE__)."/../logic/MCUConferenceController.php");
require_once(dirname(__FILE__)."/../params/SetMosaicSlotParams.php");

class MosaicController extends MCUConferenceController {
	
	private function setRecordByConfId($confId) {
		$this->conferenceProxy->setActiveConferenceByConfIdThrowable($confId);
			
		// meeting record.
		$this->meetingProxy->setActiveMeetingRecordByMeetingKeyThrowable($this->conferenceProxy->activeConferenceRecord["meeting_key"]);
		
		// room record
		$this->meetingProxy->setRoomRecordByRoomKeyThrowable($this->meetingProxy->activeMeetingRecord["room_key"]);
		
		// ives_setting record
		$this->meetingProxy->setSettingRecordByRoomKey($this->meetingProxy->activeMeetingRecord["room_key"]);
		
		//
// 		$this->fmsController = new FMSNetConnectionController($this->meetingProxy->activeMeetingRecord);
	}
	
	/**
	 * <p>
	 * BFCP終了時のみ判断に利用しているのでFMS上のdocumentSharingStatus
	 * は取り急ぎ参照しない
	 * </p>
	 * @param string $confId
	 * @return boolean
	 */
	public function isActivationForDefaultMosaic($confId) {
		$this->setRecordByConfId($confId);
		$participatedCount = count($this->mcuControllerProxy->getParticipatedVideoParticipantListByConfId($confId));
		return ($this->meetingProxy->activeSpeaker && $participatedCount >= 3);
	}
	
	public function isActivationForFlashMosaic($confId) {
		$this->setRecordByConfId($confId);
		return ($this->meetingProxy->activeSpeaker);
	}
	
	public function isActivationForMobileMosaic($confId) {
		$this->setRecordByConfId($confId);
		$participatedCount = count($this->mcuControllerProxy->getParticipatedVideoParticipantListByConfId($confId));
		return (($this->meetingProxy->roomRecord["mobile_4x4_layout"] &&
			$participatedCount == 17) ||
			(!$this->meetingProxy->roomRecord["mobile_4x4_layout"] &&
			$participatedCount == 10));
	}
	
	public function getInitConferenceMosaicSlotParams($confId, $activeSpeaker=true) {
		return $this->getSetMosaicSlotParams($confId, 0, $activeSpeaker);
	}
	
	public function getFlashMosaicSlotParams($confId, $activeSpeaker=true) {
		return $this->getSetMosaicSlotParams($confId, 1, $activeSpeaker);
	}
	
	public function getMobileMosaicSlotParams($confId) {
		$obj = new SetMosaicSlotParams(array(
				"confId"=>$confId,
				"mosaicId"=>2,
				"num"=>0,
				"partId"=>0));
		return $obj->getParams();
	}
	
	private function getSetMosaicSlotParams($confId, $mosaicId, $activeSpeaker = true) {
		$obj = new SetMosaicSlotParams(array(
				"confId"=>$confId,
				"mosaicId"=>$mosaicId,
				"num"=>0,
				"partId"=>((0 != $mosaicId) && $activeSpeaker) ? -2 : 0));
		return $obj->getParams();
	}
	
	public function getDefaultMosaicSlotParamsToActivateVadSlot($confId) {
		$obj = new SetMosaicSlotParams(array(
				"confId"=>$confId,
				"mosaicId"=>0,
				"num"=>0,
				"partId"=>-2));
		return $obj->getParams();
	}

	public function getDefaultMosaicSlotParamsToDeActivateVadSlot($confId) {
		$obj = new SetMosaicSlotParams(array(
				"confId"=>$confId,
				"mosaicId"=>0,
				"num"=>0,
				"partId"=>0));
		return $obj->getParams();
	}
	
	public function getMobileMosaicSlotParamsToActivateVadSlot($confId) {
		$obj = new SetMosaicSlotParams(array(
				"confId"=>$confId,
				"mosaicId"=>2,
				"num"=>0,
				"partId"=>-2));
		return $obj->getParams();
	}
	
	public function getMobileMosaicSlotParamsToDeActivateVadSlot($confId) {
		$obj = new SetMosaicSlotParams(array(
				"confId"=>$confId,
				"mosaicId"=>2,
				"num"=>0,
				"partId"=>0));
		return $obj->getParams();
	}
	
	public function getSalesConferenceFlashMosaicSlotParams($confId, $partId) {
		$obj = new SetMosaicSlotParams(array(
				"confId"=>$confId,
				"mosaicId"=>2,
				"num"=>0,
				"partId"=>$partId));
		return $obj->getParams();
	}
	
	public function getSalesConferenceMosaicMosaicSlotParams($confId, $partId) {
		$obj = new SetMosaicSlotParams(array(
				"confId"=>$confId,
				"mosaicId"=>2,
				"num"=>1,
				"partId"=>$partId));
		return $obj->getParams();
	}
}
