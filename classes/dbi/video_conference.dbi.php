<?php

require_once("classes/N2MY_DBI.class.php");

class VideoConferenceTable extends N2MY_DB {

    var $table = "video_conference";
    var $logger = null;
    protected $primary_key = "video_conference_key";

    public function __construct( $dsn )
    {
        $this->init($dsn, $this->table);
    }
    
    public function removeAllByRoomKey($roomKey) {
        $where    = "room_key = '".addslashes($roomKey)."'".
        			" AND is_active = 1";
		$data = array("is_active" => 0);
		parent::update($data, $where);
    }
    
    public function findLatestByRoomKey($roomKey) {
    	$where  = "room_key = '".mysql_real_escape_string($roomKey)."' AND is_active = 1";
        $db_res = $this->select($where , array('video_conference_key' => 'desc'));
        if (!$db_res) {
            return false;
        }
        return $db_res->fetchRow(DB_FETCHMODE_ASSOC);
    }
    
    public function getActiveVideoConferenceByMeetingKey($meeting_key) {
    	$where  = "meeting_key = '".mysql_real_escape_string($meeting_key)."' AND is_active = 1";
    	$db_res = $this->select($where , array('video_conference_key' => 'desc'));
    	if (!$db_res) {
    		return false;
    	}
    	return $db_res->fetchRow(DB_FETCHMODE_ASSOC);
    }
    
    public function getActiveVideoConferenceByConfId($confId) {
    	$where  = "conference_id = '".mysql_real_escape_string($confId)."' AND is_active = 1";
    	$db_res = $this->select($where , array('video_conference_key' => 'desc'));
    	if (!$db_res) {
    		return false;
    	}
    	return $db_res->fetchRow(DB_FETCHMODE_ASSOC);    	
    }


    public function getActiveVideoConferenceByDid($did) {
    	$where  = "did = '".mysql_real_escape_string($did)."' AND is_active = 1";
    	$db_res = $this->select($where , array('video_conference_key' => 'desc'));
    	if (!$db_res) {
    		return false;
    	}
    	return $db_res->fetchRow(DB_FETCHMODE_ASSOC);
    }
}
