<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("classes/AppFrame.class.php");
require_once('classes/core/dbi/Meeting.dbi.php');
require_once('classes/core/dbi/Participant.dbi.php');

class API_Layout extends AppFrame
{
    function init() {
        $this->dsn = $this->get_dsn();
    }

    function default_view()
    {
        $this->render_default();
    }

    function render_default()
    {
        $this->logger->debug(__FUNCTION__." called.", __FILE__, __LINE__);

        $obj_Meeting  = new DBI_Meeting( $this->dsn );

        $obj_Participant  = new DBI_Participant( $this->dsn );

        $meeting_key     = $this->session->get( 'meeting_key' );
        $participant_key = $this->session->get( 'participant_key' );

        if ( ! $meeting_key || ! $participant_key ) {
            $this->logger->error("missing parameters.", __FILE__, __LINE__);
            return;
        }

        $m  = $obj_Meeting->getRow( sprintf( "meeting_key='%s' AND is_deleted = 0", $meeting_key ) );
        $p  = $obj_Participant->getDetail( sprintf( "participant_key='%s'", $participant_key ) );

        $this->template->assign("meeting_max_audience", $m['meeting_max_audience']);

        $this->display( sprintf( "core/api/meeting_config/meeting/resources/layout_for_n2my_%s.t.xml", $p['participant_type_name'] ) );
    }
}

$main =& new API_Layout();
$main->execute();

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
