<?php
class PGiPhoneNumber
{
    static $local_main_location_id = 'JPNTKY';
    static $free_main_location_id  = 'JPNJPN';

    // Arrayから国内(Type='Local')のみ抽出
    static function extractLocalMain($pgi_phone_numbers)
    {
        if (!is_array($pgi_phone_numbers) || count($pgi_phone_numbers) <= 0) {
            return  null;
        }
        $config  = EZConfig::getInstance(N2MY_CONFIG_FILE)->getAll("PGI");
        foreach ($pgi_phone_numbers as $phone_number) {
            if ($phone_number['LocationID'] == self::$local_main_location_id
             && ($phone_number['Type']  == 'Local' || $phone_number['Type']  == 'Toll')) {
                return $phone_number;
            }
        }
        return null;
    }
    static function extractFreeMain($pgi_phone_numbers)
    {
        if (!is_array($pgi_phone_numbers) || count($pgi_phone_numbers) <= 0) {
            return  null;
        }
        $config  = EZConfig::getInstance(N2MY_CONFIG_FILE)->getAll("PGI");
        foreach ($pgi_phone_numbers as $phone_number) {
            if ($phone_number['LocationID'] == self::$free_main_location_id
             && $phone_number['Type']  == 'National toll free') {
                return $phone_number;
            }
        }
        return null;
    }
    static function extracts($pgi_phone_numbers, $condition = array()) {
        if (!is_array($pgi_phone_numbers) || count($pgi_phone_numbers) <= 0) {
            return  $pgi_phone_numbers;
        }
        $res = array();
        foreach ($pgi_phone_numbers as $k => $phone_number) {
            if (@$condition['Location']) {
                if (!preg_match('/^'.$condition['Location'].'/', $phone_number['Location'])) {
                    continue;
                }
            }
            if (@$condition['Types']) {
                $types =  $condition['Types'];
                if (in_array('Free', $condition['Types'])) {
                    $types =  array_merge($types, array('Toll free','International toll free','International Toll Free', 'National toll free', 'National mobile toll free'));
                }
                if (!in_array($phone_number['Type'] , $types )) {
                    continue;
                }
            }
            $res[$k] = $phone_number;
        }
        return $res;

    }
    // ArrayのLocationを言語マッピングに対応したものに変換
    static function findLocationConfigByID($locationID)
    {
        if (!$locationID) {
            $locationID = "USAUSA";
        }
        foreach (self::getLocationConfig() as $config) {
            if ((string)$config->id == $locationID){
                return $config;
            }
        }
    }
    // ArrayのLocationからResion情報を取得
    static function findRegionConfigByRegion($region)
    {
        if (!$region) {
            $region = "0";
        }
        foreach (self::getRegionConfig() as $config) {
            if ((string)$config->id == $region){
                return $config;
            }
        }
    }
    // LocationのCountryIDからCountry情報を取得
    static function findCountryConfigById($id)
    {
        if (!$id) {
            $id = "OTHER";
        }
        foreach (self::getCountryConfig() as $config) {
            if ((string)$config->id == $id){
                return $config;
            }
        }
    }
    static function findTypeConfigByKey($key)
    {
        if (!$key) {
            throw('key is Empry');
        }
        foreach (self::getTypeConfig() as $config) {
            if ((string)$config->key == $key){
                return $config;
            }
        }
    }
        static function getTypeConfig()
    {
        $config = simplexml_load_file(N2MY_APP_DIR.'/config/pgi/phone_type.xml');
        if (!$config) {
            throw('type file not found');
        }
        return $config;
    }
    static function getLocationConfig()
    {
        $config = simplexml_load_file(N2MY_APP_DIR.'/config/pgi/location.xml');
        if (!$config) {
            throw('multilang file not found');
        }
        return $config;
    }
    static function getRegionConfig()
    {
        $config = simplexml_load_file(N2MY_APP_DIR.'/config/pgi/region.xml');
        if (!$config) {
            throw('multilang file not found');
        }
        return $config;
    }
    static function getCountryConfig()
    {
        $config = simplexml_load_file(N2MY_APP_DIR.'/config/pgi/country.xml');
        if (!$config) {
            throw('multilang file not found');
        }
        return $config;
    }
    // ArrayのLocationを言語マッピングに対応したものに変換
    static function toMultilangLocationName($pgi_phone_numbers, $lang)
    {
        if (!is_array($pgi_phone_numbers) || count($pgi_phone_numbers) <= 0) {
            return  $pgi_phone_numbers;
        }

        $location_config = self::getLocationConfig();
        foreach ($pgi_phone_numbers as $k => $phone_number) {
            foreach ($location_config->location as $location) {
                if ($location->id == $phone_number['LocationID']) {
                    if ($location->name->{$lang}){
                        $location = (string)$location->name->{$lang};
                    } else {
                        $location = (string)$location->name->en_US;
                    }
                    $pgi_phone_numbers[$k]['Location'] = $location;
                }
            }
        }
        return $pgi_phone_numbers;
    }
    // ArrayのLocationを言語マッピングに対応したものに変換
    static function toMultilangTypeName($pgi_phone_numbers, $lang)
    {
        if (!is_array($pgi_phone_numbers) || count($pgi_phone_numbers) <= 0) {
            return  $pgi_phone_numbers;
        }

        $config = self::getTypeConfig();
        foreach ($pgi_phone_numbers as $k => $phone_number) {
            foreach ($config->phoneType as $phoneType) {
                if ($phoneType->key == $phone_number['Type']) {
                    if ($phoneType->name->{$lang}){
                        $type = (string)$phoneType->name->{$lang};
                    } else {
                        $type = (string)$phoneType->name->en_US;
                    }
                    $pgi_phone_numbers[$k]['Type'] = $type;
                }
            }
        }
        return $pgi_phone_numbers;
    }
}
