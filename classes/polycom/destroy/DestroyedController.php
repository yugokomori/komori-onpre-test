<?php

require_once(dirname(__FILE__)."/../../../classes/polycom/Polycom.class.php");
require_once(dirname(__FILE__)."/../../../classes/polycom/destroy/client/DestroyedFlashClient.php");
require_once(dirname(__FILE__)."/../../../classes/polycom/destroy/client/DestroyedPolycomClient.php");
require_once(dirname(__FILE__)."/../../../classes/polycom/destroy/meeting/DestroyedFlashAndPolycomMeeting.php");
require_once(dirname(__FILE__)."/../../../classes/polycom/destroy/meeting/DestroyedPolycomOnlyMeeting.php");

require_once(dirname(__FILE__)."/../../../lib/EZLib/EZCore/EZConfig.class.php");
require_once(dirname(__FILE__)."/../../../lib/EZLib/EZCore/EZLogger2.class.php");

class DestroyedController {
	public static function create($request) {
		$config = EZConfig::getInstance(N2MY_CONFIG_FILE);
		$logger = EZLogger2::getInstance($config);
		$logger->info($request);
		$polycomClass	= new PolycomClass(DestroyedController::getDsn());
		$participant = $polycomClass->getTargetPolycomParticipant($request->confId, $request->partId);
		if ($participant) {
			$targetMeeting = $polycomClass->getMeetingByMeetingKey($participant["meeting_key"]);
			$flashList = $polycomClass->getFlashParticipantListFromVideoConferenceByConfId($request->confId);
			$obj = (count($flashList) > 0)? new DestroyedFlashAndPolycomMeeting($targetMeeting): new DestroyedPolycomOnlyMeeting($targetMeeting);
			$logger->info("obj type: ".get_class($obj));
			return new DestroyedPolycomClient($request, $obj);
		}
		else {
			$targetMeeting = $polycomClass->getMeetingByConfId($request->confId);
			$flashList = $polycomClass->getFlashParticipantListFromVideoConferenceByConfId($request->confId);
			$obj = (count($flashList) > 0)? new DestroyedFlashAndPolycomMeeting($targetMeeting): new DestroyedPolycomOnlyMeeting($targetMeeting);
			return new DestroyedFlashClient($request, $obj);
		}
	}
	
	private function getDsn() {
		$server_list = parse_ini_file(dirname(__FILE__)."/../../../config/server_list.ini");
		return $server_list[N2MY_DEFAULT_DB];
	}
}
