<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
require_once("classes/N2MY_DBI.class.php");
require_once("classes/core/dbi/Meeting.dbi.php");

class DBI_Participant extends N2MY_DB
{
    public $table = "participant";
    public $logger = null;
    public $rules = array();
    public $dsn = null;
    public $obj_MGM_DB = null;
    protected $primary_key = "participant_key";

    /**
     * コンストラクタ
     */
    public function __construct( $dsn ) {
      $this->dsn = $dsn;
        $this->init( $dsn, $this->table );
        $this->obj_MGM_DB = new N2MY_DB(N2MY_MDB_DSN);
    }

    /**
     * 参加者更新
     */
    public function updateList($meeting_key, $participant_list) {
        $where = "meeting_key = " . $meeting_key;

        // 一時的に全員退出
        $data = array(
            "is_active"       => 0,
            "update_datetime" => date("Y-m-d H:i:s")
            );
        $ret = $this->update($data, $where);
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
            return false;
        }
        foreach ($participant_list as $idx => $val) {
            $participant_key = $val["id"];
            $where = "meeting_key = " . $meeting_key.
                " AND (participant_key = '".addslashes($participant_key)."' OR participant_session_id = '".addslashes($participant_key)."')";
            // 名前を入力した瞬間を参加とみなす
            $info = $this->getRow( $where );
            $edit_particpant = array(
                "is_active"             => 1,
                "participant_name"      => $val['name'],
                "participant_port"      => $val['port'],
                "update_datetime"       => date("Y-m-d H:i:s"),
                "uptime_end"            => date("Y-m-d H:i:s")
                );
            if ($val['protocol']) {
                $edit_particpant["participant_protocol"] = $val['protocol'];
            }
            if (DB::isError($info)) {
                $this->logger2->error($info->getUserInfo());
                return false;
            }
            // 存在すれば更新
            if ($info) {
                // 開始時間
                if(!$info["uptime_start"]) $edit_particpant["uptime_start"] = date("Y-m-d H:i:s");
                // 参加者情報更新
                $this->logger2->info(array($edit_particpant, $where));
                $this->update($edit_particpant, $where);
            // h323など存在しない
            } else {
                $edit_particpant['participant_session_id']  = $val['id'];
                $edit_particpant['meeting_key']             = $meeting_key;
                $edit_particpant['participant_mode_name']   = $val['type'];
                $edit_particpant['participant_type_name']   = $val['type'];
                $edit_particpant['participant_role_name']   = 'default';
                $edit_particpant['participant_locale']      = 'ja';
                $edit_particpant['is_narrowband']           = 0;
                $edit_particpant['uptime_start']            = date("Y-m-d H:i:s");
                $edit_particpant['use_count']               = 0;
                $edit_particpant['create_datetime']         = date('Y-m-d H:i:s');
                $this->logger2->info($edit_particpant);
                $this->add($edit_particpant);
            }
            if (DB::isError($ret)) {
                $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
                return false;
            }
        }
        return true;
    }

    /**
     * 参加者更新
     */
    public function updateListWithoutPolycom($meeting_key, $participant_list) {
      $where = "meeting_key = " . $meeting_key .
      " AND `participant_protocol` IS NOT NULL AND `participant_protocol` <> 'sip'";

      // 一時的に「ポリコム以外」全員退出
      $data = array(
          "is_active"       => 0,
          "update_datetime" => date("Y-m-d H:i:s")
      );
      $ret = $this->update($data, $where);
      if (DB::isError($ret)) {
        $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
        return false;
      }
      foreach ($participant_list as $idx => $val) {
        $participant_key = $val["id"];
        $where = "meeting_key = " . $meeting_key.
        " AND (participant_key = '".addslashes($participant_key)."' OR participant_session_id = '".addslashes($participant_key)."')";
        // 名前を入力した瞬間を参加とみなす
        $info = $this->getRow( $where );
        $edit_particpant = array(
            "is_active"             => 1,
            "participant_name"      => $val['name'],
            "participant_port"      => $val['port'],
            "update_datetime"       => date("Y-m-d H:i:s"),
            "uptime_end"            => date("Y-m-d H:i:s")
        );
        if ($val['protocol']) {
          $edit_particpant["participant_protocol"] = $val['protocol'];
        }
        if (DB::isError($info)) {
          $this->logger2->error($info->getUserInfo());
          return false;
        }
        // 存在すれば更新
        if ($info) {
          // 開始時間
          if(!$info["uptime_start"]) $edit_particpant["uptime_start"] = date("Y-m-d H:i:s");
          // 参加者情報更新
          $this->logger2->info(array($edit_particpant, $where));
          $this->update($edit_particpant, $where);
          // h323など存在しない
        } else {
          $edit_particpant['participant_session_id']  = $val['id'];
          $edit_particpant['meeting_key']             = $meeting_key;
          $edit_particpant['participant_mode_name']   = $val['type'];
          $edit_particpant['participant_type_name']   = $val['type'];
          $edit_particpant['participant_role_name']   = 'default';
          $edit_particpant['participant_locale']      = 'ja';
          $edit_particpant['is_narrowband']           = 0;
          $edit_particpant['uptime_start']            = date("Y-m-d H:i:s");
          $edit_particpant['use_count']               = 0;
          $edit_particpant['create_datetime']         = date('Y-m-d H:i:s');
          $this->logger2->info($edit_particpant);
          $this->add($edit_particpant);
        }
        if (DB::isError($ret)) {
          $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
          return false;
        }
      }
      return true;
    }

    public function updateCount( $info )
    {
        $sql = "UPDATE ".$this->table." SET" .
            " use_count = use_count + 1" .
            ",uptime_end = '".addslashes($info["uptime_end"])."'" .
            ",update_datetime = '".date("Y-m-d H:i:s")."'" .
            " WHERE participant_key = ".$info["participant_key"];
        $ret = $this->_conn->query($sql);
        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$ret->getUserInfo());
            return false;
        }
        return true;
    }

    public function getCount( $meeting_key )
    {
        if (!$meeting_key) {
            return 0;
        }

        $where = sprintf( "is_active=1 AND (participant_type_key=1 OR participant_type_key=2 OR participant_type_key=6 OR participant_type_key=8 OR participant_type_key=14 OR participant_type_key=16 OR participant_type_key=17) AND meeting_key='%s'", $meeting_key );
        $participant_info = $this->getRowsAssoc( $where, null, null, 0, "participant_key");
        return count( $participant_info );
    }

    function getDetail( $where )
    {
        $data = $this->getRow( $where );

        $data['participant_mode_name'] = $this->getMode( sprintf( "participant_mode_key='%s'", $data['participant_mode_key'] ), "participant_mode_name" );
        $data['participant_type_name'] = $this->getType( sprintf( "participant_type_key='%s'", $data['participant_type_key'] ), "participant_type_name" );
        $data['participant_role_name'] = $this->getRole( sprintf( "participant_role_key='%s'", $data['participant_role_key'] ), "participant_role_name" );

        return $data;
    }

    function getList($meeting_keys, $is_online = true, $participant_key = false)
    {
        //set is_active
        $is_active = 1;
        if (!$is_online) {
            $is_active = 0;
        }

        //check parameter type
        $meeting_key = $meeting_keys;
        $is_single   = 1;
        if (is_array($meeting_keys)) {
            $meeting_key = implode(", ", $meeting_keys);
            $is_single   = 0;
        }

        $where = $is_single ? sprintf( "meeting_key='%s'", $meeting_key ) : sprintf( "meeting_key in ( %s )", $meeting_key );

        if ($is_active) {
            $where .= " AND is_active = 1";
        }
        $participantlist = $this->getRowsAssoc( $where, array("participant_key" => "asc"));
        $return = array();
        foreach( $participantlist as $participant ){
            $participant['participant_mode_name'] = $this->getMode( sprintf( "participant_mode_key='%s'", $participant['participant_mode_key'] ), "participant_mode_name" );
            $participant['participant_type_name'] = $this->getType( sprintf( "participant_type_key='%s'", $participant['participant_type_key'] ), "participant_type_name" );
            $participant['participant_role_name'] = $this->getRole( sprintf( "participant_role_key='%s'", $participant['participant_role_key'] ), "participant_role_name" );

            if ($participant_key) {
                $return[$participant['participant_key']] = $participant;
            } else {
                $return[] = $participant;
            }
        }
        return $return;
    }

    function getConferenceList($ivesDId, $ivesConferenceId)
    {
        $where = sprintf( "ives_did='%s' AND ives_conference_id='%s' AND is_active = 1", $ivesDId, $ivesConferenceId );

        $participantlist = $this->getRowsAssoc( $where, array("participant_key" => "asc"));
        $return = array();
        foreach( $participantlist as $participant ){
            $return[] = $participant;
        }
        return $return;
    }

    function addPolycomUser($data) {
        $data["participant_mode_key"] = 1;
        $data["participant_type_key"] = 1;
        $data["participant_role_key"] = 2;
        $data["create_datetime"] = date("Y-m-d H:i:s");
        $data["update_datetime"] = date("Y-m-d H:i:s");
        $obj_Meeting = new DBI_Meeting( $this->dsn );
        $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_key='%s'", addslashes($data["meeting_key"]) ) );
        $data["user_key"] = $meeting_info["user_key"];
        $participantKey = parent::add($data);

        $result = $this->getRow( sprintf( "participant_key=%d", addslashes($participantKey) ) );
    return $result;
    }

    public function add( $data )
    {
        if (!$data['participant_mode_key'] = $this->getMode( sprintf( "participant_mode_name='%s'", $data['participant_mode_name'] ), "participant_mode_key" )) {
            $data['participant_mode_key'] = $this->getMode( sprintf( "participant_mode_name='%s'", 'unknown' ), "participant_mode_key" );
        }
        if (!$data['participant_type_key'] = $this->getType( sprintf( "participant_type_name='%s'", $data['participant_type_name'] ), "participant_type_key" )) {
            $data['participant_type_key'] = $this->getType( sprintf( "participant_type_name='%s'", 'unknown' ), "participant_type_key" );
        }
        if (!$data['participant_role_key'] = $this->getRole( sprintf( "participant_role_name='%s'", $data['participant_role_name'] ), "participant_role_key" )) {
            $data['participant_role_key'] = $this->getRole( sprintf( "participant_role_name='%s'", 'unknown' ), "participant_role_key" );
        }
        if (!$data['participant_mode_key'] || !$data['participant_type_key'] || !$data['participant_role_key']) {
            $this->logger2->warn($data, 'Error get key');
        }

        //PGi連携用にPin_cdを発行する
        $sql = sprintf( "SELECT max(teleconference_pin_cd) FROM %s where  meeting_key = '%s'", $this->table, $data["meeting_key"] );
        $ret = $this->_conn->query($sql);
        $pin_cd = $ret->fetchRow(DB_FETCHMODE_ASSOC);
        $pin_cd =  $pin_cd["max(teleconference_pin_cd)"] + 1;

//        $data['participant_session_id'] = md5(uniqid(rand(), 1));
        $d = array(
            "meeting_key"            => $data["meeting_key"],
            "participant_session_id" => $data["participant_session_id"],
            "storage_login_id"       => $data["storage_login_id"],
            "member_key"             => $data["member_key"],
            "participant_id"         => $data["participant_id"],
            "participant_name"       => $data["participant_name"],
            "participant_email"      => $data["participant_email"],
            "participant_station"    => $data["participant_station"],
            "participant_locale"     => $data["participant_locale"],
            "participant_country"    => $data["participant_country"],
            "participant_mode_key"   => $data["participant_mode_key"],
            "participant_type_key"   => $data["participant_type_key"],
            "participant_role_key"   => $data["participant_role_key"],
            "participant_skin_type"  => $data["participant_skin_type"],
            "uptime_start"           => $data["uptime_start"],
            "teleconference_pin_cd"  => $pin_cd,
            "is_narrowband"          => $data["is_narrowband"],
            "is_mobile"              => $data["is_mobile"] ? 1 : 0,
            "is_active"              => $data["is_active"] ? 1 : 0,
            "create_datetime"        => date( "Y-m-d H:i:s" )
        );
        $obj_Meeting = new DBI_Meeting( $this->dsn );
        $meeting_info = $obj_Meeting->getRow( sprintf( "meeting_key='%s'", addslashes($data["meeting_key"]) ) );
        $d["user_key"] = $meeting_info["user_key"];
        parent::add( $d );

        $sql = sprintf( "SELECT max(participant_key) FROM %s where  participant_session_id = '%s'", $this->table, $data["participant_session_id"] );
        $ret = $this->_conn->query($sql);
        $result = $ret->fetchRow(DB_FETCHMODE_ASSOC);
        return $result["max(participant_key)"];
    }

    private function getMode( $where, $column="*" )
    {
        $sql = sprintf( "SELECT %s FROM participant_mode WHERE %s", $column, $where );
        $ret = $this->obj_MGM_DB->_conn->query($sql);
        $result = $ret->fetchRow(DB_FETCHMODE_ASSOC);
        return $result[$column];
    }

    public function getModeList() {
        static $mode_list;
        if (isset($mode_list)) return $mode_list;
        $sql = "SELECT participant_mode_key, participant_mode_name FROM participant_mode";
        $ret = $this->obj_MGM_DB->_conn->query($sql);
        $mode_list = array();
        if (DB::isError($ret)) {
            $this->logger2->error($ret->getUserInfo());
            return $mode_list;
        }
        while($row = $ret->fetchRow(DB_FETCHMODE_ASSOC)) {
            $mode_list[$row["participant_mode_key"]] = $row["participant_mode_name"];
        }
        return $mode_list;
    }

    private function getType( $where, $column="*" )
    {
        $sql = sprintf( "SELECT %s FROM participant_type WHERE %s", $column, $where );
        $ret = $this->obj_MGM_DB->_conn->query($sql);
        $result = $ret->fetchRow(DB_FETCHMODE_ASSOC);
        return $result[$column];
    }

    public function getTypeList() {
        static $type_list;
        if (isset($type_list)) return $type_list;
        $sql = "SELECT participant_type_key, participant_type_name FROM participant_type";
        $ret = $this->obj_MGM_DB->_conn->query($sql);
        $type_list = array();
        if (DB::isError($ret)) {
            $this->logger2->error($ret->getUserInfo());
            return $type_list;
        }
        while($row = $ret->fetchRow(DB_FETCHMODE_ASSOC)) {
            $type_list[$row["participant_type_key"]] = $row["participant_type_name"];
        }
        return $type_list;
    }

    private function getRole( $where, $column="*" )
    {
        $sql = sprintf( "SELECT %s FROM participant_role WHERE %s", $column, $where );
        $ret = $this->obj_MGM_DB->_conn->query($sql);
        $result = $ret->fetchRow(DB_FETCHMODE_ASSOC);
        return $result[$column];
    }

    public function getRoleList() {
        static $role_list;
        if (isset($role_list)) return $role_list;
        $sql = "SELECT participant_role_key, participant_role_name FROM participant_role";
        $ret = $this->obj_MGM_DB->_conn->query($sql);
        $role_list = array();
        if (DB::isError($ret)) {
            $this->logger2->error($ret->getUserInfo());
            return $role_list;
        }
        while($row = $ret->fetchRow(DB_FETCHMODE_ASSOC)) {
            $role_list[$row["participant_role_key"]] = $row["participant_role_name"];
        }
        return $role_list;
    }

    public function getActiveParticipantListByMeetingKey($meetingKey) {
        $where = "meeting_key = '".$meetingKey."'".
                 " AND is_active = 1";
    	return $this->getRowsAssoc($where);
    }

    public function getAllActiveParticipantsByUserKey($userKey)
    {
        $where = sprintf( "user_key='%s' AND is_active = 1", $userKey);

        $participantlist = $this->getRowsAssoc( $where, array("user_key" => "asc"));
        $return = array();
        foreach( $participantlist as $participant ){
            $return[] = $participant;
        }
        return $return;
    }
    /**
     *
     * @param int $meetingKey
     * @return participant record.
     */
    public function getActiveParticipantByParticipantKey($participantKey) {
        $where = "participant_key = '".$participantKey."'".
                 " AND is_active = 1";
    	return $this->getRow($where);
    }
}
/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
