UPDATE `questionnaire` SET `is_active`='0' WHERE `questionnaire_id`='1';
INSERT INTO `questionnaire` (`questionnaire_id`, `type`, `is_active`, `is_deleted`, `created_datetime`, `updated_datetime`, `deleted_datetime`) VALUES
(2, 'meeting_end', 1, 0, NOW( ), '0000-00-00 00:00:00', '0000-00-00 00:00:00');

INSERT INTO `question` (`question_id`,`questionnaire_id`, `lang1`, `lang2`, `lang3`, `lang4`, `lang5`, `type`, `sort`, `created_datetime`, `updated_datetime`, `deleted_datetime`) VALUES
(16, 2, '今回のWeb会議利用に満足されていますか？', 'How satisfied are you with this Web Conferencing service?', '您对本次网络视频会议的使用满意吗？', NULL, NULL, 1, 1, NOW( ), '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 2, '映像について（複数選択可）', 'About Video (multiple selections)', '关于图像（可以多选）', NULL, NULL, 2, 2, NOW( ), '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 2, '音声について（複数選択可）', 'About Audio (multiple selections)', '关于语音（可以多选）', NULL, NULL, 2, 3, NOW( ), '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 2, 'その他問題があったと感じた点・ご意見・ご要望があればお願いいたします（自由記述）', 'Please give us your comments and suggestions (Free description)', '请您填写其他的意见或建议。（自由阐述）', NULL, NULL, 5, 4, NOW( ), '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 2,  'お名前',  'Name',  '姓名',  NULL,  NULL,  '5',  '5', NOW( ) ,  '0000-00-00 00:00:00',  '0000-00-00 00:00:00'),
(21, 2,  'フリガナ',  'Name',  '假名',  NULL,  NULL,  '5',  '6', NOW( ) ,  '0000-00-00 00:00:00',  '0000-00-00 00:00:00'),
(22, 2,  '会社名・所属先',  'Company',  '公司名・所属部门',  NULL,  NULL,  '5',  '7', NOW( ) ,  '0000-00-00 00:00:00',  '0000-00-00 00:00:00'),
(23, 2,  'メールアドレス',  'Email Address',  '邮箱地址',  NULL,  NULL,  '5',  '8', NOW( ) ,  '0000-00-00 00:00:00',  '0000-00-00 00:00:00'),
(24, 2,  '電話番号',  'Phone Number',  '电话号码',  NULL,  NULL,  '5',  '9', NOW( ) ,  '0000-00-00 00:00:00',  '0000-00-00 00:00:00');


INSERT INTO `question_branch` (`question_branch_id`, `question_id`, `lang1`, `lang2`, `lang3`, `lang4`, `lang5`, `sort`, `created_datetime`, `updated_datetime`, `deleted_datetime`) VALUES
(46, 16, '満足', 'Satisfied', '满意', '', '', 1, NOW( ), '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 16, '普通', 'So so', '一般', '', '', 2, NOW( ), '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 16, '不満', 'Dissatisfied', '不满意', '', '', 3, NOW( ), '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 17, '相手の映像が表示されない', 'Cannot see others'' video', '未显示对方图像', '', '', 1, NOW( ), '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 17, '自分の映像が表示されない', 'Cannot see your own video', '未显示我方图像', '', '', 2, NOW( ), '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 17, '映像の品質が悪い', 'Video quality is not good', '图像质量差', '', '', 3, NOW( ), '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 18, '自分の音声が取得できない', 'Cannot hear your own voice', '无法接通我方语音', '', '', 1, NOW( ), '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 18, '相手の音声が聞こえない', 'Cannot hear others'' voice', '无法听到对方语音', '', '', 2, NOW( ), '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 18, '音が頻繁に途切れる', 'Lose sound very often', '经常听不到声音', '', '', 3, NOW( ), '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 18, '音声が遅延する', 'Cause delay', '语音延迟', '', '', 4, NOW( ), '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 18, '音声がエコーする（やまびこのように聞こえる）', 'Cause echo', '语音回响（听起来有回音）', '', '', 5, NOW( ), '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 18, '音声がハウリングする（キーンという耳障りな大きな音がする）', 'Cause howling noise', '语音啸响（声音尖锐刺耳、大声作响）', '', '', 6, NOW( ), '0000-00-00 00:00:00', '0000-00-00 00:00:00');
