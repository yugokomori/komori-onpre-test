#!/bin/sh

_log() {
	msg=$1
	exectime=`date +"[%Y-%m-%d %H:%M:%S]"`
	echo "${exectime} ${msg}" >> /home/mtg-user/.cron.d/cron.log
}

http_request() {
	format=$1
	url=`printf $format $fms $status`
	#echo $url
	curl $url
}

change_fms_status() {
	# Meeting Ver.4
	#------
	# ASP 
	http_request "http://meeting01.nice2meet.us/admin_tool/fms_service.php?fms=%s&status=%s"
	# USA
	http_request "http://meeting.vcube.com/admin_tool/fms_service.php?fms=%s&status=%s"
	# kokuyo
	http_request "http://meeting.meetima.jp/admin_tool/fms_status.php?fms=%s&status=%s" 
	# lms.vismee.jp
	#http_request "http://lms.vismee.jp/admin_tool/fms_service.php?fms=%s&status=%s"
	# ricoh
	http_request "http://meeting.webkaigi.tv/admin_tool/fms_status.php?fms=%s&status=%s"

	# Meeting Ver.3
	#------
	# Meeting3
	http_request "http://www.nice2meet.us/admin_tool/fms_service.php?fms=%s&status=%s"

	# Sales Ver.4
	#------
	# ASP
	http_request "http://vstaff:vCube8sD@sales.nice2meet.us/admin_tool/fms_status.php?fms=%s&status=%s"
	# KOKUYO
	http_request "http://sales-support.meetima.jp/admin_tool/fms_status.php?fms=%s&status=%s"
	
}

# 停止状態
fms_stop() {
	change_fms_status
	_log "${fms} status stop OK"

}

# 開始処理
fms_start() {
	# fms 再起動
	fms_alive
	if [ $? = 1 ]
	then
		_log "${fms} restart OK"
	else
		_log "${fms} restart NG!!"
	fi
	# web側のステータスを更新
	change_fms_status
	echo "${fms} status start OK"
}

fms_restart() {
	fms_stop
	fms_start
}

fms_alive() {
        for a in $(seq 1 10)
        do
		# fms再起動
		_log "ssh -t -t ${user}@${fms} ${reboot_shell}"
		ssh -t -t ${user}@${fms} ${reboot_shell}
		sleep 10
		# 状態確認
                alive=`/usr/local/bin/ruby -r \
			/home/project/meeting.nice2meet.us/lib/ruby/rtmp.rb -e \
			"h = RTMPHack.new; h.connection_uri = 'rtmp://${fms}/meeting'; h.connection_args = [0x900]; h.method_name = 'aliveCheck'; h.execute;"`
                if [ $alive = "alive" ]
                then
                        return 1
                fi
		_log "${fms} restart error"
		sleep 5
        done
        return 0
        exit
}


user=$1
fms=$2
status=$3
reboot_shell=$4

if [ $status = "stop" ]
then
	fms_stop
elif [ $status = "start" ]
then
	fms_start
elif [ $status = "restart" ]
then
	fms_restart
else
	_log "parameter error"
fi
