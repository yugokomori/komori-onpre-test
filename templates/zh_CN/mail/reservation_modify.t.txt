﻿※本邮件为发信专用邮件，无法回信。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
来自V-CUBE Meeting的通知
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
欢迎使用网络视频会议系统。
以下会议的变更内容已通过邮件发送给参加者。

■会议名称
{$info.reservation_name}

■召开时间
{$info.reservation_starttime} ～ {$info.reservation_endtime} (GMT {if $info.reservation_place >= 0}+{/if}{$info.reservation_place})

■主持人
{$info.organizer.name} 

■{if $info.reservation_pw && $info.reservation_pw_type == 2}会议记录{/if}密码
{if $info.reservation_pw}{$info.reservation_pw}{else}无{/if}

------------------------------
会议的详细信息
------------------------------
{if $info.pin_cd}
■V-CUBE Mobile使用的验证码
{$info.pin_cd}
※通过iPad/Android启动V-CUBE Mobile的应用程序，
在「用验证码登陆」中输入验证码，也可以参加网络视频会议。
{/if}
{if ($temporarySipNumberAddress||$temporarySipAddress||$temporaryH323NumberAddress||$temporaryH323Address) && (!$info.reservation_pw || ($info.reservation_pw && $info.reservation_pw_type == 2))}

■会议系统专用受邀者地址
・SIP
{if $temporarySipNumberAddress}
{$temporarySipNumberAddress}
{/if}
{if $temporarySipAddress}
{$temporarySipAddress}
{/if}
 ・H.323
{if $temporaryH323NumberAddress}
{$temporaryH323NumberAddress}
{/if}
{if $temporaryH323Address}
{$temporaryH323Address}
{/if}
※从Polycom等电视会议终端连接V-CUBE Meeting时，请使用该地址进入会议室。
{/if}
{if $teleconf_info && $teleconf_info.tc_type != ''}

■电话会议号码和密码
{if $teleconf_info.tc_type == 'pgi'}
如果你没有密码，请稍等片刻，或按＃。
{if $teleconf_info.use_pgi_dialin || $teleconf_info.use_pgi_dialin_free}
-- 访问点（拨入）号码:
{$teleconf_info.main_phone_number.Number|japan_tel}  ({$teleconf_info.main_phone_number.Location})
--  其它访问点一览:
{if $info.guest_url_format ==1}
{$teleconf_info.phone_number_url}
{else}
<{$teleconf_info.phone_number_url}>
{/if}
{/if}
-- 密码
{$teleconf_info.pgi_m_pass_code|wordwrap:3:" ":true}
{/if}
{/if}

■会议的参加者已向以下参加者发送预约会议变更的通知邮件。

{if $guests}{foreach from=$guests item=guest}
- {$guest.name} {if $guest.type == 1} (听众){elseif $guest.type == 2} (资料用户){/if}
　{$guest.email}
　邀请URL: {if $info.guest_url_format ==1}{$base_url}guest/r/{$guest.r_user_session}&c={$guest.country_key}{else}<{$base_url}guest/r/{$guest.r_user_session}&c={$guest.country_key}>{/if} 
{/foreach}{/if}

- 发给参加者的消息
{$info.mail_body}

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
■V-CUBE Meeting
{if $info.guest_url_format ==1}{$base_url}{else}<{$base_url}>{/if} 
■开发・运营:
V-cube株式会社 {if $info.guest_url_format ==1}http://www.vcube.com/ {else}<http://www.vcube.com/>{/if} 
〒153-0051 东京都目黑区上目黑2-1-1 中目黑GT Tower 20F
■Customer Support Center(Japanese)
Tel: 0570-002192(Open 24/7)
E-mail: {if $info.guest_url_format ==1} vsupport@vcube.co.jp {else} <vsupport@vcube.co.jp>{/if} 
■Customer Support Center(English)
Tel:
Tokyo +81-3-4560-1287
Malaysia +60-3-7724-9693
Singapore +65-3158-2832
China +86-4006-618-2360
E-mail: {if $info.guest_url_format ==1} vcube_support@vcube.com {else} <vcube_support@vcube.com>{/if} 
Office hours: Weekdays 9:00-18:00(GMT+8)
Closed on weekends and Malaysian holidays.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
