@echo off
rem プロセスリストからDocゲートウェイの稼動状況を確認

tasklist|grep VrmsDocumentConverter >result.txt
for %%F IN (result.txt) do if %%~zF EQU 0 ( 

goto run

) ELSE goto check


:run
rm result.txt
start VrmsDocumentConverter.exe
exit

:check
rm result.txt
echo 現在別のユーザログインでドキュメント変換ゲートウェイのプロセスが実行しています。
set end=
set /p end="プロセスの自動起動を行いません(Enterで終了)" 
exit
