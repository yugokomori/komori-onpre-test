<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("config/config.inc.php");
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_Clip.class.php");
require_once("classes/ValidateClip.class.php");
require_once("classes/dbi/clip.dbi.php");
require_once("classes/dbi/meeting_clip.dbi.php");
require_once("classes/dbi/meeting.dbi.php");
require_once("classes/ClipFormat.class.php");
require_once("classes/core/dbi/Storage.dbi.php");
require_once("classes/dbi/storage_file.dbi.php");


class AppClip extends AppFrame {

    /**
     * ログクラス
     * @access public
     */
    var    $logger     = null;
    var    $meetingLog = null;
    static $MAX_LIST   = 12;

    function init() {
        $this->obj_N2MY_Clip = new N2MY_Clip( $this->get_dsn() );
        $this->_name_space = md5(__FILE__);
    }

    /**
     * ログイン認証
     */
    function auth() {
        if (isset($_REQUEST['action_callback'])) {
            return;
        }
        $this->checkAuth();
        $user_info = $this->session->get("user_info");
        if ($user_info['account_model'] == "free") {
            $this->logger->warn(__FUNCTION__."#option disable",__FILE__,__LINE__);
            header("Location: ".'/');
            exit;
        }
    }

    /**
     * デフォルト処理
     */
    function default_view() {
        $user_info   = $this->session->get( "user_info" );
        $member_info = $this->session->get( "member_info" );
        $this->action_list();
    }

    /**
     * クリップ詳細表示
     */
    public function action_showDetail()
    {
        $clip_info             = $this->_getClipInfo();
        $clip_info["duration"] = gmdate('H:i:s', $clip_info["duration"]);
        $meeting_list          = $this->_getMeetingClipList( $clip_info["clip_key"] );
        $vido_config           = $this->config->getAll( "VIDEO" );

        $this->template->assign("skin",        $vido_config["base_skin"]);
        $this->template->assign("skin_width",  $vido_config["base_skin_width"]);
        $this->template->assign("skin_height", $vido_config["base_skin_height"]);
        $this->template->assign("client_id",   $vido_config["client_id"]);
        $this->template->assign("clip_key",    $clip_info["clip_key"]);
        $this->template->assign( "clip_info",     $clip_info );
        $this->template->assign( "meeting_list",  $meeting_list );

        $this->display( "user/clip/detail.t.html" );

    }

    /**
     * クリップ編集実行
     */
    public function action_edit_done()
    {

        $clip_info = $this->_getClipInfo();
        $user_info = $this->session->get( "user_info" );

        $request = $this->request->getAll();

        $validater = new ValidateClip( $request );
        $validater->forEdit( $request );

        if( $validater->hasError() ){
            $error = $validater->getErrors();
            $message = "";
            foreach( $error as $v){
                $message .= '<li>'.$v.'</li>';
            }
            $this->template->assign( "message", $message );

        }else{
            $data["title"]       = $request["title"];
            $data["description"] = $request["description"];
            $data["updatetime"]  = date( "Y-m-d H:i:s" );

            $wheres[] = sprintf( "clip_key = '%s'", mysql_real_escape_string( $request["clip_key"] ) );
            $wheres[] = sprintf( "user_key = '%s'", mysql_real_escape_string( $user_info["user_key"] ) );
            $wheres[] = sprintf( "is_deleted = '%s'", 0 );
            $where = implode( " AND ", $wheres );

            $this->clip_obj = new ClipTable( $this->get_dsn() );
            $rs = $this->clip_obj->update( $data, $where );
            if( DB::isError($rs) ) {
                $this->logger2->error("clip edit error");
                $this->errorToExit(CLIP_ERROR_EDIT);
            }
            $this->template->assign( "message", CLIP_INFO_EDIT );

        }

        $this->action_showDetail();
    }

    /**
     * クリップ削除確認
     */
    public function action_delete_confirm()
    {
        $clip_info             = $this->_getClipInfo();
        $clip_info["duration"] = gmdate('H:i:s', $clip_info["duration"]);
        $meeting_list          = $this->_getMeetingClipList( $clip_info["clip_key"] );
        $vido_config           = $this->config->getAll( "VIDEO" );
        $thumbnail_src         = ClipFormat::thumbnailUrl( $clip_info['clip_key'], $clip_info['storage_no'] );

        $this->template->assign( "clip_info", $clip_info );
        $this->template->assign( "clip_src",      $clip_src );
        $this->template->assign( "thumbnail_src", $thumbnail_src );
        $this->template->assign( "meeting_list",  $meeting_list );

        $this->display( "user/clip/delete_confirm.t.html" );

    }
    /**
     * クリップ削除実行
     */
    public function action_delete_done()
    {
        $clip_info             = $this->_getClipInfo();
        $clip_info["duration"] = gmdate('H:i:s', $clip_info["duration"]);

        $user_info = $this->session->get( "user_info" );
        try {
            $this->obj_N2MY_Clip->delete( $clip_info["clip_key"], $user_info['user_key']);
            $this->template->assign( "message", CLIP_SUCCESS_DELETE );
        } catch (Exception $e) {
            $this->template->assign( "message", CLIP_ERROR_DELETE );
            $this->logger2->error( $e->getMessage() );
        }
        $this->template->assign( "clip_info", $clip_info );
        $this->display( "user/clip/delete_done.t.html" );
    }
    /**
     * クリップ詳細情報取得
     */
    private function _getClipInfo()
    {

        $request = $this->request->getAll();
        if( empty( $request["clip_key"] ) ){
            $this->logger2->error("clip key lost");
            $this->errorToExit(CLIP_ERROR_KEY_LOST);
        }

        $user_info = $this->session->get( "user_info" );
        $this->clip_obj = new ClipTable( $this->get_dsn() );

        $wheres[] = sprintf( "clip_key   = '%s'", mysql_real_escape_string( $request["clip_key"] ) );
        $wheres[] = sprintf( "user_key   = '%s'", mysql_real_escape_string( $user_info["user_key"] ) );
        $wheres[] = sprintf( "is_deleted = '%s'", 0 );

        $rs = $this->clip_obj->getRow( implode( " AND ", $wheres ), "*", array(), 1 );
        if (DB::isError($rs) || empty($rs) ) {
            $this->template->assign( "message", CLIP_ERROR_NOT_FOUND );
            $this->display( "user/clip/error.t.html" );
            $this->logger2->error("clip not found");
            exit;
        }

        return $rs;

    }

    /**
     * クリップ利用中ミーティング取得
     */
    private function _getMeetingClipList( $clip_key )
    {

        //ミーティングクリップからミーティング記録を取得
        $this->meeting_clip_obj = new MeetingClipTable( $this->get_dsn() );
        $wheres[] = sprintf( "clip_key   = '%s'", mysql_real_escape_string( $clip_key ) );
        $wheres[] = sprintf( "is_deleted = '%s'", 0 );

        $rs = $this->meeting_clip_obj->select( implode( " AND ", $wheres ) );
        if( DB::isError($rs) || $rs === false ){
            $this->logger2->error("db error:clip_key=".$clip_key);
            return;
        }

        if( empty($rs) )  return;

        while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            $meeting_keys[] = $row["meeting_key"];
        }


        //ミーティング情報を取得
        $user_info = $this->session->get( "user_info" );
        $this->meeting_obj = new MeetingTable( $this->get_dsn() );

        $wheres = array();
        $wheres[] = sprintf( "user_key = '%s'", $user_info["user_key"] );
        $wheres[] = sprintf( "meeting_key in ('%s')", implode( "','", $meeting_keys ) );

        $rs = $this->meeting_obj->select( implode( " AND ", $wheres ) );
        if( DB::isError($rs) || $rs === false ){
            $this->logger2->error("db error:meeting_key=" . implode( " AND ", $wheres ) );
            return;
        }

        while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            $ret[] = $row;
        }

        return $ret;

    }

    /**
     *
     */
    public function action_upload() {

        if (!$this->check_submit_key($this->_name_space)) {
            $this->logger->warn(__FUNCTION__."#duplicate",__FILE__,__LINE__);
            return $this->action_list();
        }

        $user_info = $this->session->get( "user_info" );
        if (!$user_info) {
            $this->logger2->error('user_info is empty');
            $this->displayErrorToExit(CLIP_ERROR_UPLOAD);
        }

        $file_path  = @$_FILES['clip_file']['tmp_name'];
        $video_conf = $this->config->getAll("VIDEO");
        $n2my_clip  = new N2MY_Clip($this->get_dsn());

        // for debug
        $params = array('title'       => @$_POST['clip_title']       ? $_POST['clip_title']       : $_FILES['clip_file']['name'],
                        'description' => @$_POST['clip_description'] ? $_POST['clip_description'] : '',
                        'user_key'    => $user_info['user_key']);
        $errors = array();

        $validate = new ValidateClip();
        $validate->forCreate($params, $file_path);
        if ($validate->hasError()) {
            $errors = $validate->getErrors();
        }
        $clip_upload_limit = $user_info['clip_share_storage_size'] * 1000 * 1000;// to byte
        if ($clip_upload_limit <= $n2my_clip->sizeOfConvertedByUser($user_info['user_key'])) {
            $errors[] = CLIP_ERROR_USER_LIMIT;
        }

        if (0 < count($errors)) {
            return $this->action_list($errors);
        }

        try {
            $n2my_clip->add($params, $file_path);
        } catch (Exception $e) {
            $this->logger2->error($e->getMessage());
            $this->errorToExit(CLIP_ERROR_UPLOAD);
        }
        $this->session->set("uploaded_clip", true);
        header("Location: ".'/services/clip/?action_list');
    }

    /**
     * 一覧
     */
    public function action_list($messages = array()) {

        $user_info   = $this->session->get("user_info");
        $member_info = $this->session->get("member_info");
        $clip_table  = new ClipTable($this->get_dsn());

        $page  = (int)@$this->request->get("page");
        $page  = $page ? $page : 1;
        try {
            $total     = $clip_table->countByUserKey($user_info['user_key']);
            $clip_list = $clip_table->findByUserKey($user_info['user_key'], $sort   = array('createtime'=>'desc'),
                                                                            $limit  = self::$MAX_LIST,
                                                                            $offset = self::$MAX_LIST * ($page - 1),
                                                                            $columns = '*');
        } catch (Exception $e) {
            $this->logger2->error($e->getMessage());
            $this->errorToExit(CLIP_ERROR_LIST);
        }
        $n2my_clip  = new N2MY_Clip($this->get_dsn());
        try {
            $uploaded_clip_size = $n2my_clip->sizeOfConvertedByUser($user_info['user_key']);
        } catch (Exception $e) {
            $this->logger2->error('get size failed user_key:'.$user_info['user_key']);
            $this->errorToExit(CLIP_ERROR_KEY_LOST);
        }

        if (0 < count($clip_list)) {
            foreach ($clip_list as $k => $clip) {
                $clip_list[$k]['thumbnail_url'] = ClipFormat::thumbnailUrl($clip['clip_key'], $clip['storage_no']);
                $clip_list[$k]["duration"]      = gmdate('H:i:s', $clip["duration"]);
            }
        }
        if ($this->session->get("uploaded_clip")) {
            $this->session->set("uploaded_clip", false);
            $messages[] = CLIP_INFO_UPLOAD;
        }

        $video_config = $this->config->getAll("VIDEO");
        $this->template->assign(array("clip_list"         => $clip_list,
                                      "max_filesize"      => $video_config['max_filesize'],
                                      "messages"          => $messages,
                                      "pager"             => $this->setPager(self::$MAX_LIST, $page  , $total),
                                      "uploaded_clip_size"=> $uploaded_clip_size,
                                      "base_action"       => 'clip_list'));

        $this->set_submit_key($this->_name_space);

        $this->display("user/clip/top.t.html");
    }

    /**
     * プレビュー表示
     */
    public  function action_preview()
    {
        $request  = $this->request->getAll();
        $clip_key = @$request["clip_key"];
        if (!$clip_key) {
            $this->logger2->error("clip key lost");
            $this->errorToExit(CLIP_ERROR_KEY_LOST);
        }

        $video_config = $this->config->getAll("VIDEO");
        $clip_src = sprintf(N2MY_BASE_URL."/services/clip/getclip.php?clip_key=%s&id=%s", $clip_key, $video_config["client_id"]);

        $this->template->assign("clip_src", $clip_src);
        $this->display("user/clip/preview.t.html");
    }

    /**
     * ビデオからのコールバック
     */
    public  function action_callback()
    {
        if (!@$_REQUEST['result']) {
            $this->logger2->error('result is empty');
            exit;
        }

        $parsed_clip = $this->parseCallbackClip();
        if (!$parsed_clip) {
            $this->logger2->error('clip parse failed - post result:'.$_REQUEST['result']);
            exit;
        }

        $n2my_clip = new N2MY_Clip($this->get_dsn());
        try {
            $video_clip = $n2my_clip->feedFromVideo($parsed_clip['CLIPKEY']);
        } catch (Exception $e) {
            $this->logger2->error('video feed error ' .$e->getMessage());
            exit;
        }

        $params = array('duration' => $parsed_clip['DURATION']);

        if ($video_clip) {
            $params = array('flv_filesize' => $video_clip['FLV_FILESIZE'],
                            'clip_status'  => $video_clip['STATUS'],
                            'duration'     => $video_clip['DURATION']) ;
        } else {
            // コールバック時にビデオのデータを取得できなければエラーとする
            $params = array('clip_status' => N2MY_Clip::$STATUS['UNKNOWN_ERROR']) ;
        }

        $clip_table  = new ClipTable($this->get_dsn());

        try {
            $clip_table->updateByClipKey($params, $video_clip['CLIPKEY']);
            $_objStorage      = new DBI_StorageFile($this->get_dsn());
            $storage_data = array(
                    "status"          => $video_clip['STATUS'],
                    "file_size"       => $video_clip['FLV_FILESIZE'],
                    "update_datetime" => date("Y-m-d H:i:s"),
                    );
            $where = "clip_key = '".$video_clip['CLIPKEY']."'";
            $_objStorage->update($storage_data, $where);
        } catch(Exception $e) {
            $this->logger2->error('update fault');
            exit;
        }

        print 1;

    }

    /**
     *
     */
    private function parseCallbackClip()
    {
        require_once ("classes/XmlUtil.class.php");
        $XML = new XmlUtil();
        $XML->parse_into_struct($_REQUEST['result']);
        $clips =  $XML->getTagValues('CLIP:');
        return @$clips[0];
    }
    /**
     *
     */
    public function action_sort_list()
    {
        $search = $this->session->get("condition", $this->_name_space);
        $search["page"]         = 1;
        $search["sort_key"]     = $this->request->get("sort_key")  ? $this->request->get("sort_key")  : "reservation_starttime";
        $search["sort_type"]    = $this->request->get("sort_type") ? $this->request->get("sort_type") : "desc";
        $this->session->set("condition", $search, $this->_name_space);
        $this->render_top();
    }
    private function errorToExit($message)
    {
        $this->template->assign("message", $message);
        $this->display("user/clip/error.t.html");
        exit;
    }
}

$main = new AppClip();
$main->execute();
