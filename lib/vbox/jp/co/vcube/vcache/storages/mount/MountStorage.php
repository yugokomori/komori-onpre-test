<?php

/**
 * @author Yuki Asano <y-asano@vcube.co.jp>
 * @copyright Vcube, Inc. All Rights Reserved.
 * @version 0.1
 */

require_once( "jp/co/vcube/vcache/core/AbstractStorage.php" );
require_once( "jp/co/vcube/vcache/core/ResultArrayObject.php" );
require_once( "jp/co/vcube/util/ExecCommand.php" );
require_once( "jp/co/vcube/vcache/util/TransferOption.php" );

abstract class MountStorage extends AbstractStorage
{

  /**
   * @private $result ResultArrayObject - A result value from copyTo method.
   */
  private $result;

  /**
   * $this filepath copy to $target filepath.
   * @param $target Object - Storage instance that inherited AbstractStorage.
   * @throws Exception - Invalid transfer_mode.
   * @throws Exception - Invalid return value. Allow only inherited ResultArrayObject class.
   * @return void
   */
  final public function copyTo( $target )
  {
    if ( $this->isRemoteStorage() && $target->isMountStorage() ) {
      $result = $this->downloadTo( $target );

    } elseif ( $this->isMountStorage() && $target->isRemoteStorage() ) {
      $result = $this->uploadTo( $target );

    } elseif ( $this->isMountStorage() && $target->isMountStorage() ) {

      if ( !$this->transfer_option )
        $this->transfer_option = new TransferOption( $this->default_transfer_mode );

      $result = null;
      switch( $this->transfer_option->getTransferMode() ) {
        case AbstractStorage::TRANSFER_MODE_SCP:
          $result = $this->transferBySCP( $target );
          break;

        case AbstractStorage::TRANSFER_MODE_SFTP:
          $result = $this->transferBySFTP( $target );
          break;

        case AbstractStorage::TRANSFER_MODE_RSYNC:
          $result = $this->transferByRSYNC( $target );
          break;

        default:
          throw new Exception( "Invalid transfer_mode. - ".$this->transfer_mode );
      }

    } elseif ( $this->isRemoteStorage() && $target->isRemoteStorage() ) {
      throw new Exception( "RemoteStorage can not allowed with RemoteStorage." );
    }

    if ( !($result instanceof ResultArrayObject) && !is_subclass_of($result, 'ResultArrayObject') )
      throw new Exception( "Invalid return value. Allow only inherited ResultArrayObject class. - ". ($result ? get_class($result) : "void") );

    $this->result = $result;

  }

  /**
   * $this filepath copy to $target filepath.
   * @param $target Object - Storage instance that inherited AbstractStorage.
   * @throws Exception - Invalid transfer_mode.
   * @throws Exception - Invalid return value. Allow only inherited ResultArrayObject class.
   * @return void
   */
  final public function uploadTo( $target )
  {
    /* Do not remove below line when you override this method. */
    $this->validateUploadTo( $target );

    if ( !$this->transfer_option )
      $this->transfer_option = new TransferOption( $this->default_transfer_mode );

    $result = null;
    switch( $this->transfer_option->getTransferMode() ) {
      case AbstractStorage::TRANSFER_MODE_SCP:
        $result = $this->transferBySCP( $target );
        break;

      case AbstractStorage::TRANSFER_MODE_SFTP:
        $result = $this->transferBySFTP( $target );
        break;

      case AbstractStorage::TRANSFER_MODE_RSYNC:
        $result = $this->transferByRSYNC( $target );
        break;

      case AbstractStorage::TRANSFER_MODE_S3CMD:
        $result = $target->upload( $this );
        break;

      case AbstractStorage::TRANSFER_MODE_S3SYNC:
        $result = $target->upload( $this );
        break;

      default:
        throw new Exception( "Invalid transfer_mode. - ".$this->transfer_mode );
    }

    $this->result = $result;

    return $result;

  }

  /**
   * $this filepath copy to $target filepath.
   * @param $target Object - Storage instance that inherited AbstractStorage.
   * @throws Exception - Invalid transfer_mode.
   * @throws Exception - Invalid return value. Allow only inherited ResultArrayObject class.
   * @return void
   */
  final public function downloadTo( $target )
  {
    /* Do not remove below line when you override this method. */
    throw new Exception( "The LocalStorage objects To RemoteStorage objects  can not allowed." );
  }

  final public function upload( $target )
  {
    /* Do not remove below line when you override this method. */
    throw new Exception( "The LocalStorage objects To RemoteStorage objects  can not allowed." );
  }

  final public function download( $target )
  {
    $result = $target->downloadTo( $this );

    return $result;
  }

  /**
   * A result value from copyTo method.
   * @return ResultArrayObject - Return value must ResultArrayObject or inherited it.
   */
  public function getResultArrayObject()
  {
    return $this->result;
  }

  /**
   * A transfer method for SCP.
   * @param $target Object - Storage instance that inherited AbstractStorage.
   * @throws Exception - ExecCommand exceptions directly.
   * @return ResultArrayObject - Return value must ResultArrayObject or inherited it.
   */
  final private function transferBySCP( $target )
  {
    $target_config = $target->getStorageConfig();

    $cmd = new ExecCommand( $this->transfer_option->timeout, $this->transfer_option->expect_return );

    $cmd->setTrustedArgument( $this->transfer_option->first_argument );
    if ( $this->unescape )
      $cmd->setTrustedArgument( $this->filepath );
    else
      $cmd->setArgument( $this->filepath );

    $target_path = sprintf( "%s@%s:%s", $target_config->user, $target_config->host, $target->filepath );
    if ( $target->unescape )
      $cmd->setTrustedArgument( $target_path );
    else
      $cmd->setArgument( $target_path );

    $cmd->execute( "scp" );

    return new ResultArrayObject( $cmd->getResults() );
  }

  /**
   * A transfer method for SFTP.
   * @param $target Object - Storage instance that inherited AbstractStorage.
   * @return void
   */
  final private function transferBySFTP( $target )
  {
  }

  /**
   * A transfer method for RSYNC.
   * @param $target Object - Storage instance that inherited AbstractStorage.
   * @return $cmd ExecCommand instance.
   */
  final private function transferByRSYNC( $target )
  {
  }

  /**
   * $this filepath copy to $target filepath, before execute delete method for $this filepath.
   * @param $target Object - Storage instance that inherited AbstractStorage.
   * @return void
   */
  public function moveTo( $target )
  {

    $this->copyTo( $target );
    $ret = $this->getResultArrayObject();
    if ($ret['return'] !== 0)
      $this->delete();
    
  }

  /**
   * Create specified directory.
   * @throws Exception - Could not create a directory. The mkdir() method was failed.
   * @return void
   */
  public function makeDir()
  {
    if ( mkdir( $this->filepath ) == false )
      throw new Exception( "Could not create a directory. The mkdir() method was failed. - ". $this->filepath );
  }

  /**
   * Return the exists filelist array without dot-files(`.` and `..`).
   * @throws Exception - It is not a directory.
   * @return $list Array - A exists filelist array.
   */
  public function filelist()
  {
    if ( !is_dir($this->filepath) )
      throw new Exception( "It is not a direcotry. - ". $this->filepath );

    $list = scandir( $this->filepath );
    $targets = array_merge( array_keys( $list, "." ), array_keys( $list, ".." ) );

    foreach( $targets as $v ) {
      unset( $list[$v] );
    }

    return new ResultArrayObject( array_values( $list ) );
  }

  /**
   * Delete a file.
   * @throws Exception - The unlink() method was failed.
   * @return void
   */
  public function delete()
  {
    if ( @unlink( $this->filepath ) === false )
      throw new Exception( "The unlink() method was failed. - ". $this->filepath );
  }

  /**
   * Verify a file exists.
   * @return Boolean
   */
  public function exists()
  {
    return file_exists( $this->filepath );
  }
}
