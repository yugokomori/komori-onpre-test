<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */
// +----------------------------------------------------------------------+
// |                                                                      |
// | [            ]                                                       |
// |                                                                      |
// +----------------------------------------------------------------------+
// | PHP version 4                                                        |
// +----------------------------------------------------------------------+
// | Authors:oneda                                                        |
// |                                                                      |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006               all rights reserved.                |
// +----------------------------------------------------------------------+
//
require_once("classes/N2MY_DBI.class.php");

class MemberGroupTable extends N2MY_DB {

    protected $primary_key = "member_group_key";

    function MemberGroupTable($dsn) {
        $this->rules = array(

            "member_group_key"     => array(
                "required" => true),
            "user_key"     => array(
                "required" => true),
            "member_group_name"     => array(
                "required" => true)
        );
        $this->init($dsn, "member_group");
    }

    function getGroupName($key){

        $query = "select * from member_group "
                       . " where member_group_key={$key}";

        $ret = $this->_conn->getone($query);

        if (DB::isError($ret)) {
            $this->logger->error(__FUNCTION__."#query error", __FILE__, __LINE__, $query);
            return $ret;
        }
        return $ret;
    }

    /**
     * 連想配列形式で返す
     */
    function getAssoc($user_key) {
        $where = "user_key = ".$user_key;
        $rs = $this->select($where, null, null, null, "member_group_key,member_group_name");
        if (DB::isError($rs)) {
            $this->logger->error(__FUNCTION__,__FILE__,__LINE__,$rs->getUserInfo());
            return $rs;
        }
        while($row = $rs->fetchRow(DB_FETCHMODE_ASSOC)) {
            $list[$row["member_group_key"]] = $row["member_group_name"];
        }
        return $list;
    }

    public function add( $data )
    {
        $data["member_group_registtime"] = date( "Y-m-d H:i:s" );
        return parent::add( $data );
    }

}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */
?>
