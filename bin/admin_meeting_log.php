#!/usr/local/bin/php
<?php
require_once("set_env.php");
require_once("classes/AppFrame.class.php");
require_once("classes/N2MY_Reservation.class.php");
require_once("classes/dbi/reservation.dbi.php");
require_once("classes/N2MY_MeetingLog.class.php");
require_once ('lib/EZLib/EZUtil/EZCsv.class.php');

class AppAdminMeetingLog extends AppFrame {

    var $_mail_flg = 1;
    var $dsn = null;
    var $N2MY_Reservation = null;
    var $obj_Reservation = null;
    var $obj_Meeting_log = null;

    // 手動実行フラグ
    var $command_flag = false;

    function __destruct() {
        //unlink(N2MY_APP_DIR."/tmp/sess_".session_id());
    }

    function __construct($argv = false) {
        parent::__construct();
        $this->command_flag = $argv;
    }

    function init() {
        $this->dsn = $this->get_dsn();
        $this->N2MY_Reservation = new N2MY_Reservation($this->dsn);
        $this->obj_Reservation     = new ReservationTable($this->dsn);
        $this->obj_Meeting_log     = new N2MY_MeetingLog($this->dsn);
    }

    function default_view() {
        // 月始めに一回実行される想定
        // 対象ユーザーIDとファイル保存パスはconfig/config.inc.phpで定義
        $start_limit = date("Y-m-01 00:00:00",strtotime("-1 month"));
        $end_limit = date('Y-m-d 23:59:59', mktime(0, 0, 0, date('m'), 0, date('Y')));

//$start_limit = date("Y-m-01 00:00:00");
//$end_limit = date('Y-m-t 23:59:59');

        $param = array(
            "user_key" => ADMIN_USER_KEY,
            "start_time"=> $start_limit,
            "end_time"=> $end_limit,
        );
        $log_data = $this->obj_Meeting_log->getAdminMeetingLog($param);
        $csv_name = "admin_log".date("Ym" , strtotime("-1 month")) . ".csv";
//$csv_name = "admin_log".date("Ym") . ".csv";
        $csv_path = ADMIN_CSV_PATH . $csv_name;
        $csv = new EZCsv(true,"SJIS","UTF-8");
        $header = array(
            "meeting_name" => "会議名",
            "organizer_name" => "主催者",
            "meeting_date" => "開催日時",
            "connected_count" => "接続台数",
            "payment_type" => "会議形態",
            "type" => "区分",
            "participant_name" => "参加者",
            "participant_member_id" => "参加者ID",
            "payment_name" => "費用負担者",
            "payment_member_id" => "費用負担者ID",
            "payment_member_accounting_unit_code" => "経理単位コード",
            "payment_member_expense_department_code" => "費用部課コード",
            "time" => "利用時間(分)",
            "terminal_type" => "端末区分",
            "mcu_down_flg" => "MCU障害",
            );
        $csv->open($csv_path, "w");
        $csv->setHeader($header);
        $csv->write($header);
        foreach($log_data as $log){

            if($log["participant_name"] == ""){
                if($log["type"] == 2){
                    $log["participant_name"] = "招待者";
                }
            }
            if($log["type"] == 1){
                $log["type"] = "主催者";
            }else if ($log["type"] == 2){
                $log["type"] = "参加者";
            }

            if($log["payment_type"] == 1){
                $log["payment_type"] = "主催者負担";
            }else if ($log["payment_type"] == 2){
                $log["payment_type"] = "利用者負担";
            }
            if($log["terminal_type"] == 1){
                $log["terminal_type"] = "iPad";
            }elseif($log["terminal_type"] == 0){
                $log["terminal_type"] = "PC";
            }else{
                // テレビ会議は参加者と参加者IDのカラムを非表示
                $log["terminal_type"] = "テレビ会議";
                $log["participant_name"] = null;
                $log["participant_member_id"] = null;
            }

            $csv_data = array();
            $csv_data["meeting_name"] = $log["meeting_name"];
            $csv_data["organizer_name"] = $log["organizer_name"];
            $csv_data["meeting_date"] = date("Y-m-d H:i:s",$log["meeting_start_date"]) . ' - ' . date("Y-m-d H:i:s",$log["meeting_end_date"]);
            $csv_data["connected_count"] = $log["connected_count"];
            $csv_data["payment_type"] = $log["payment_type"];
            $csv_data["type"] = $log["type"];
            $csv_data["participant_name"] = $log["participant_name"];
            $csv_data["participant_member_id"] = $log["participant_member_id"];
            $csv_data["payment_name"] = $log["payment_name"];
            $csv_data["payment_member_id"] = $log["payment_member_id"];
            $csv_data["payment_member_accounting_unit_code"] = $log["payment_member_accounting_unit_code"];
            $csv_data["payment_member_expense_department_code"] = $log["payment_member_expense_department_code"];
            $csv_data["time"] = $log["time"];
            $csv_data["terminal_type"] = $log["terminal_type"];
            $csv_data["mcu_down_flg"] = $log["mcu_down_flg"];
//$this->logger2->info($csv_data);

            $csv->write($csv_data);
        }

//$this->logger2->info($csv_data);
        var_dump($csv_name);
    }


}
$main = new AppAdminMeetingLog($argv[1]);
$main->execute();
