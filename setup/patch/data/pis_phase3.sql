ALTER TABLE  reservation ADD  `use_videoconference` INT( 4 ) NOT NULL DEFAULT  '0' COMMENT 'テレビ会議使用可否0: 使用しない、1: 使用する' AFTER  `mcu_down_flg`;
ALTER TABLE  reservation ADD  `videoconference_room_num` varchar( 30 ) NULL DEFAULT NULL COMMENT 'テレビ会議室番号' AFTER  `use_videoconference`;
ALTER TABLE  reservation ADD  `videoconference_layout` varchar( 30 ) NULL DEFAULT NULL COMMENT 'テレビ会議映像レイアウト' AFTER  `videoconference_room_num`;

ALTER TABLE  reservation_extend ADD  `extend_videoconference` INT( 4 ) NOT NULL DEFAULT  '0' COMMENT 'テレビ会議使用可否0: 使用しない、1: 使用する' AFTER  `extend_seat`;
ALTER TABLE  reservation_extend ADD  `videoconference_room_num` varchar( 30 ) NULL DEFAULT NULL COMMENT 'テレビ会議室番号' AFTER  `extend_videoconference`;
ALTER TABLE  reservation_extend ADD  `videoconference_layout` varchar( 30 ) NULL DEFAULT NULL COMMENT 'テレビ会議映像レイアウト' AFTER  `videoconference_room_num`;

/* ホワイト会社コード */
CREATE TABLE IF NOT EXISTS `white_company_list` (
  `white_company_list_key` int(11) NOT NULL AUTO_INCREMENT,
  `user_key` int(11) NOT NULL DEFAULT '0',
  `company_id`  varchar( 20 ) NOT NULL COMMENT '会社コード',
  `company_name` varchar( 100 ) NOT NULL COMMENT '会社名',
  `accounting_unit_code` varchar(10) NOT NULL DEFAULT '' COMMENT '経理単位コード',
  `expense_department_code` varchar(10) NOT NULL DEFAULT '' COMMENT '費用部課コード',
  `memo` varchar( 200 ) NULL COMMENT 'メモ',
  `white_company_list_status` int(4) NOT NULL DEFAULT '0' COMMENT '-1:削除、0:有効',
  `create_datetime` datetime DEFAULT NULL COMMENT '作成日時',
  `update_datetime` datetime DEFAULT NULL COMMENT '更新日時',
  PRIMARY KEY (`white_company_list_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/* ブラックIDコード */
CREATE TABLE IF NOT EXISTS `blacklist` (
  `blacklist_key` int(11) NOT NULL AUTO_INCREMENT,
  `user_key` int(11) NOT NULL DEFAULT '0',
  `member_id`  varchar( 20 ) NOT NULL COMMENT 'メンバーID',
  `member_name` varchar( 100 ) NOT NULL COMMENT 'メンバー名',
  `memo` varchar( 200 ) NULL COMMENT 'メモ',
  `blacklist_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '-1:削除、0:有効',
  `create_datetime` datetime DEFAULT NULL COMMENT '作成日時',
  `update_datetime` datetime DEFAULT NULL COMMENT '更新日時',
  PRIMARY KEY (`blacklist_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/* ホワイトIDテーブルにメモ欄追加 */
ALTER TABLE  whitelist ADD  `memo` varchar( 200 ) NULL COMMENT 'メモ' AFTER  `expense_department_code`;

/* テレビ会議連携管理 */
CREATE TABLE IF NOT EXISTS `video_conference_ip_list` (
  `video_conference_ip_list_key` int(11) NOT NULL AUTO_INCREMENT,
  `user_key` int(11) NOT NULL DEFAULT '0',
  `room_num`  varchar( 100 ) NOT NULL COMMENT '会議室番号',
  `connect_ip` varchar( 100 ) NOT NULL COMMENT '接続先IP',
  `priority` int(11) NOT NULL COMMENT 'プライオリティ',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0:削除、1:有効',
  `create_datetime` datetime DEFAULT NULL COMMENT '作成日時',
  `update_datetime` datetime DEFAULT NULL COMMENT '更新日時',
  PRIMARY KEY (`video_conference_ip_list_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


/* 会社コード、名称追加 */
ALTER TABLE  member ADD `member_company_id` varchar( 10 ) NULL DEFAULT NULL COMMENT '会社コード'AFTER `member_pin_cd`;
ALTER TABLE  member ADD `member_company_name` varchar( 100 ) NULL DEFAULT NULL COMMENT '会社名'AFTER `member_pin_cd`;

/* index 追加*/
ALTER TABLE  `reservation` ADD INDEX (  `user_key` );
ALTER TABLE  `reservation_user` ADD INDEX (  `r_member_key` );
ALTER TABLE  `reservation_user` ADD INDEX (  `reservation_key` );

/* テレビ会議自動接続、切断*/
ALTER TABLE  meeting ADD `connecting_videoconference_flg` INT( 4 ) NOT NULL DEFAULT  '0' COMMENT 'テレビ会議接続中フラグ' AFTER `temporary_did`;
