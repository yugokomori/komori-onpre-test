--
-- テーブルの構造 `faq`
--

DROP TABLE IF EXISTS `faq`;
CREATE TABLE IF NOT EXISTS `faq` (
  `faq_key` int(11) NOT NULL AUTO_INCREMENT,
  `faq_category` int(11) NOT NULL DEFAULT '0',
  `faq_subcategory` int(11) NOT NULL DEFAULT '0',
  `faq_order` int(11) NOT NULL DEFAULT '0',
  `faq_question` text NOT NULL,
  `faq_answer` text NOT NULL,
  `faq_status` int(11) NOT NULL DEFAULT '0',
  `faq_lang` varchar(255) NOT NULL DEFAULT '0',
  `faq_type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1: すべてのサービス, 2: 通常, 3: メンバー課金モデル',
  PRIMARY KEY (`faq_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

--
-- テーブルの構造 `faq_data`
--

DROP TABLE IF EXISTS `faq_data`;
CREATE TABLE IF NOT EXISTS `faq_data` (
  `faq_key` int(11) NOT NULL,
  `lang` varchar(10) NOT NULL,
  `question` text,
  `answer` text,
  `create_datetime` datetime NOT NULL,
  `update_datetime` datetime NOT NULL,
  UNIQUE KEY `faq_key` (`faq_key`,`lang`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='FAQデータ';

--
-- テーブルの構造 `faq_list`
--

DROP TABLE IF EXISTS `faq_list`;
CREATE TABLE IF NOT EXISTS `faq_list` (
  `faq_key` int(11) NOT NULL AUTO_INCREMENT,
  `faq_category` int(11) NOT NULL DEFAULT '0',
  `faq_subcategory` int(11) NOT NULL DEFAULT '0',
  `faq_order` int(11) NOT NULL DEFAULT '0',
  `faq_question` text NOT NULL,
  `faq_answer` text NOT NULL,
  `faq_status` int(11) NOT NULL DEFAULT '0',
  `faq_lang` varchar(255) NOT NULL DEFAULT '0',
  `faq_type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1: すべてのサービス, 2: 通常, 3: メンバー課金モデル',
  PRIMARY KEY (`faq_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


ALTER TABLE `convert_file` ADD `document_format` VARCHAR( 64 ) NULL COMMENT '変換後のフォーマット' AFTER `document_id` ,
ADD `document_version` VARCHAR( 64 ) NULL COMMENT 'アニメーションのバージョン' AFTER `document_format`;

ALTER TABLE `user_agent` ADD `invoice_flg` INT NULL COMMENT '請求フラグ' AFTER `user_id`;

ALTER TABLE  `news` CHANGE  `news_lang`  `news_lang` VARCHAR( 11 ) NOT NULL DEFAULT  '1';
UPDATE `news` SET news_lang = 'ja_JP' WHERE news_lang = 1;
UPDATE `news` SET news_lang = 'en_US' WHERE news_lang = 2;
