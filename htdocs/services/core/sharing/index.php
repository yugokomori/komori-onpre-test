<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

//require_once('webapp/classes/AppFrameSession.class.php');
require_once("classes/AppFrame.class.php");

class Service_DesktopSharing extends AppFrame
{

     var $_enc = "EUC-JP";
     var $_is_controller = null;
     var $_lang = "ja";
     var $_key = null;
     var $_key_group = null;
     var $_protocol = null;
     var $_user_name = null;

    public function init()
    {
        $this->_name_space = md5(__FILE__);
    }

    function default_view()
    {
        //get parameters from session
//        $this->_lang = $this->session->getChild('meeting_info', 'language', null, "ja");

        //get parameters from request
        $this->_is_controller = $this->request->get('isControl', null);
        $this->_key           = $this->request->get('key'      , null);
        $this->_key_group     = $this->request->get('group_key', null);
        $this->_protocol      = $this->request->get('protocol' , null);
        $this->_user_name     = $this->request->get('user_name', null);

        //check conditions
        $condition = 1;
        $condition = (is_null($this->_key)       || $this->_key == "")       ? 0 : 1;
        $condition = (is_null($this->_key_group) || $this->_key_group == "") ? 0 : 1;

        //call renderer
        $renderer = null;
        if ($condition) {
            $renderer = $this->render_default();
        } else {
            $renderer = $this->render_error();
        }

        if (PEAR::isError($renderer)) {
            return $renderer;
        }
    }

    /**
     * [renderer] デスクトップシェアリングを表示する
     *
     * @access public
     * @param  object &$factory クラスファクトリーオブジェクト（参照渡し）
     * @return
     */
    function render_default()
    {
        //assign values
        $this->template->assign('key'      , $this->_key);
        $this->template->assign('group_key', $this->_key_group);
        $this->template->assign('protocol' , $this->_protocol);
        $this->template->assign('user_name', $this->_user_name);
        $this->template->assign("sess_id", session_id());

//        $this->template->setLanguage($this->session->get("lang"));

        // service info
//        $this->set_language($this->session->get("lang"));
        $frame["service_info"] = $this->get_message("SERVICE_INFO");
        $this->template->assign("__frame", $frame);

        //display xml_template
        if ($this->_is_controller) {
            //$this->logger->info(__FUNCTION__,__FILE__,__LINE__,array(session_id(), $_SESSION, $_REQUEST));
            $this->display("sharing/sharing_controller.t.html");
        } else {
            //$this->logger->info(__FUNCTION__,__FILE__,__LINE__,array(session_id(), $_SESSION, $_REQUEST));
            $this->display("sharing/sharing_client.t.html");
        }
    }

    function render_error()
    {
        $this->display("sharing/sharing_error.t.html");
    }

}
if ($_REQUEST["PHPSESSID"]) {
    session_id($_REQUEST["PHPSESSID"]);
}
$main =& new Service_DesktopSharing();
$main->execute();
