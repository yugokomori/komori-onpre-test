<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * [User_Clip_Post_Wait]
 *
 * @package
 * @author      Your Name <mail@example.com>
 * @access      public
 */


class User_Clip_Post_Waiting
{

    // {{{ execute
    /**
     * [[機能説明]]
     *
     * @access  public
     */
    function execute()
    {


        if (!isset($this->auth['job_mgr_no'])) {
            $this->log->info("'".__CLASS__."::".__FUNCTION__."' was executed.", __FILE__." +".__LINE__);
            return "error";
        }
        $this->_touchConvertAPI();

        return "waiting";
    }
    // }}}
    //{{{ _touchconvertapi()
    function _touchconvertapi()
    {

        $url = sprintf("%s/post/", CLIP_DESTINATION_HOST);

        $this->storage_no = rand(1,100);

        /*--- default ---*/
        $param = array('client_id'         => CLIENT_ID,
                       'group_id'          => $this->auth['job_mgr_no'],
                       'title'             => $this->title,
                       'body'              => $this->body,
                       'callback_url'      => CALLBACK_URL,
                       'storage'           => "/".$this->storage_no."/movie/",
                       'thumbnail_storage' => "/".$this->storage_no."/thumbnail/",
                       'play_url'          => sprintf("%s/%s/%s",VIRTUALHOST_URL,
                                                      VIRTUALHOST_HUSH,$this->storage_no),
                       'private'           => PRIVATE_DEFAULT,
                       'qtconv'            => QTCONV);

        $upload_file = @$_FILES['upload']['tmp_name'];

        if ($this->uploadflag == 2){
            $s = $this->Session->getParameter($this->_ns);
            $upload_file = $s['upload_path'];
        } elseif ($this->uploadflag == 3 && $this->file_name) {
            $upload_file = RECORD_TMP_PATH."/".$this->file_name.".flv";
        } elseif ($this->uploadflag == 1){
            if (defined('POST_L4_L3') && POST_L4_L3 == 'STORE'){
                $upload_file = sprintf("%s/%s", UPLOAD_TMP_PATH, sha1(uniqid(rand(), true)));
            }else{
                $upload_file = sprintf("%s/%s", TMP_SAVE_DIR, sha1(uniqid(rand(), true)));
            }
            move_uploaded_file($_FILES['upload']['tmp_name'], $upload_file);
            chmod($upload_file,0777);
        }

        $this->log->info("-------------------------".$upload_file);
        if (!file_exists($upload_file)){
            $this->Session->removeParameter($this->_ns);
            $this->log->error("'".__CLASS__."::".__FUNCTION__."' was executed.", __FILE__." +".__LINE__);
            return "error";
        }

        $this->clip_filesize = filesize($upload_file);
        if ($this->clip_filesize > UPLOAD_SIZE_LIMIT_PC) {
            $this->Session->removeParameter($this->_ns);
            $this->log->error("'".__CLASS__."::".__FUNCTION__."' was executed.", __FILE__." +".__LINE__);
            $this->error_msg1 = UPLOAD_SIZE_ERROR;
            unlink($upload_file);
            return "error";
        }

        if (defined('POST_L4_L3') && POST_L4_L3 == 'STORE'){
            $res = $this->VideoClip->post($url, L3_ACCESS_USER, L3_ACCESS_KEY, $param, $upload_file, false, true);
        }else{
            $res = $this->VideoClip->post($url, L3_ACCESS_USER, L3_ACCESS_KEY, $param, $upload_file);
        }

        //tmp内のファイル削除
        if ($this->uploadflag == 1 || $this->uploadflag == 2 || $this->uploadflag == 3){
            unlink($upload_file);
        }
        if (!$this->VideoClip->checkHTTPStatusCode()) {
            $this->Session->removeParameter($this->_ns);
            $this->log->error("'".__CLASS__."::".__FUNCTION__."' was executed.", __FILE__." +".__LINE__. " got code:".$this->VideoClip->getResponseCode());
            return "error";
        }
        $this->XML->parse_into_struct($res);
        $ret = $this->XML->getTagValues();

        if (!isset($ret[0]['CLIP_KEY'])) {
            $this->Session->removeParameter($this->_ns);
            $this->log->error("'".__CLASS__."::".__FUNCTION__."' was executed.", __FILE__." +".__LINE__." POST Response XML was none.");
            return "error";
        }

        $this->clip_key = $ret[0]['CLIP_KEY'];
        $s['clip_key']  = $this->clip_key;

        $this->Session->setParameter($this->_ns, $s);
        $this->Session->setParameter('User_Clip',array('clip_key'=>$s['clip_key']));
        return "waiting";

    }
    //}}}
    // {{{ _addClip
    /**
     * L4動画情報登録
     *
     * @access  public
     */
    function _addClip()
    {
        $log =& LogFactory::getLog();
        $log->debug("'".__CLASS__."::".__FUNCTION__."' was executed.", __FILE__." +".__LINE__);

        // --- set Parameter---
        $param = array('clip_key'           => $this->clip_key,
                       'clip_key_user_hash' => md5($this->auth['job_mgr_no'].$this->clip_key),
                       'job_mgr_no'         => $this->auth['job_mgr_no'],
                       'thumb_dir'          => $this->storage_no,
                       'clip_filesize'      => $this->clip_filesize,
                       'clip_status'        => 0,
                       'video_clip_status'  => 0,
                       'clip_title'         => $this->title,
                       'clip_body'          => $this->body,
                       'play_count'         => "0",
                       'upload_type'        => "1",
                       'mobile_play_count'  => "0",
                       'is_deleted'         => "0",
                       'create_datetime'    => date("Y-m-d H:i:s"));

        return true;
    }
    // }}}
}
?>
