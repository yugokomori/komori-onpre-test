<?php
require_once ('classes/AppFrame.class.php');
require_once ('classes/N2MY_Auth.class.php');
require_once ('classes/mgm/MGM_Auth.class.php');

class AppCenterAdminLogin extends AppFrame {

    private $obj_Auth = null;

    function init() {
    }

    function action_showTop($message = "") {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        // 言語設定
        $user_id    = $this->request->get("user_id");
        $lang          = $this->request->get("lang", $this->request->getCookie("lang", substr($this->get_language(), 0, 2)));
        $country       = $this->request->get("country", $this->request->getCookie("country"));
        $time_zone     = $this->request->get("time_zone", $this->request->getCookie("time_zone") ? $this->request->getCookie("time_zone") : "");
        // タイムゾーンのセキュリティ強化
        $timezone_list = $this->get_timezone_list();
        if (!array_key_exists($time_zone, $timezone_list)) {
            $timezone_keys = array_keys($timezone_list);
            $time_zone = $this->config->get("GLOBAL", "time_zone", 9);
        }
        // 地域コード取得
        $country_list = $this->get_country_list();
        $language_list = $this->get_language_list();
        // エラーメッセージ
        $this->template->assign("message", $message);
        // 設定
        $this->template->assign("lang", $lang);
        $this->template->assign("country", $country);
        $this->template->assign("time_zone", $time_zone);
        $this->template->assign("user_id", $user_id);
        $this->template->assign("centre_admin_flg", 1);
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__);
        $this->display('user/login/centre.t.html');
    }

    /**
     * ログイン処理
     */
    function action_showLogin() {
        $this->logger->trace(__FUNCTION__."#start", __FILE__, __LINE__);
        $this->session->removeAll();
        session_regenerate_id(true);
        $request = $this->request->getAll();
        // パラメタ
        $user_id      = $this->request->get("user_id");
        $user_pw      = $this->request->get("user_password");
        $lang         = $this->request->get("lang");
        $time_zone    = $this->request->get("time_zone");
        $_country_key = $this->request->get("country");
        //サーバー取得
        $obj_MGMClass = new MGM_AuthClass( $this->get_auth_dsn() );
        if ( ! $user_info = $obj_MGMClass->getUserInfoById( $user_id ) ){
            $message .= "<li>".LOGIN_ERROR_INVALIDUSER . "</li>";
            return $this->action_showTop($message);
        }
        if ( ! $server_info = $obj_MGMClass->getServerInfo( $user_info["server_key"] ) ){
            return $this->action_server_down();
        } else {
            $this->session->set( "server_info", $server_info );
        }

        // ログインチェック
        $obj_Auth = new N2MY_Auth( $this->get_dsn() );
        if ($login_info = $obj_Auth->check($user_id, $user_pw, null , null, "centre_admin_login")) {
            if ($login_info["user_info"]['account_model'] != 'centre') {
                $message .= "<li>".LOGIN_ERROR_INVALIDUSER . "</li>";
                return $this->action_showTop($message);
            }
            $this->session->set('login', '1');
            if ($custom = $this->config->get('SMARTY_DIR','custom')) {
                $login_info["user_info"]["custom"] = $custom;
            }
            $login_info["user_info"]["login_url"] = N2MY_BASE_URL;
            $this->session->set('center_admin_login', '1');
            $this->session->set('user_info', $login_info["user_info"]);
            $this->session->set('member_info', $login_info["member_info"]);
        } else {
            $message .= "<li>".LOGIN_ERROR_INVALIDUSER . "</li>";
            return $this->action_showTop($message);
        }

        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());
        /**
         * 接続可能なipか確認
         * とりあえずはオプション扱いではなく
         * 自分のkeyとひもづくiplistがあれば契約しているとみなし
         * 接続元のipと照らし合わせる
         */
         if (false === $obj_N2MY_Account->checkRemoteAddress($login_info["user_info"]["user_key"], $_SERVER["REMOTE_ADDR"])){
             $this->session->removeAll();
            $message["title"] = $this->get_message("WHITELIST", "title");
            $message["body"] = $this->get_message("WHITELIST", "body");
            $this->template->assign('message', $message);
            $this->display('common.t.html');
            exit;
         }

        // 地域コードの不正な指定を防ぐ
        $country_list = $this->get_country_list();
        if (!array_key_exists($_country_key, $country_list)) {
            $country_keys = array_keys($country_list);
            $_country_key = $country_keys[0];
            $this->logger->warn(__FUNCTION__, __FILE__, __LINE__,$_country_key);
        }
        foreach ($country_list as $country_key => $country_row) {
            if ($_country_key == $country_key) {
                $this->session->set('country_key', $country_row["country_key"]);
            }
        }
        $timezone_list = $this->get_timezone_list();
        if (!array_key_exists($time_zone, $timezone_list)) {
            $timezone_keys = array_keys($timezone_list);
            $time_zone = $this->config->get("GLOBAL", "time_zone", 9);
            $this->logger->warn(__FUNCTION__, __FILE__, __LINE__,$time_zone);
        }
        // クッキー
        $limit = time() + 365 * 24 * 3600;
        setcookie("lang", $lang,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("country", $_country_key,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        setcookie("time_zone", $time_zone,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);

        // セッション
        $this->session->set("lang", $lang);
        $this->session->set('time_zone', $time_zone);
        require_once("classes/N2MY_Account.class.php");
        $obj_N2MY_Account = new N2MY_Account($this->get_dsn());

        //メンバー課金利用の場合ユーザーキーからプラン及びオプションをもってくる
        if( $login_info["user_info"]["account_model"] == "member" || $login_info["user_info"]["account_model"] == "centre" ){
            require_once("classes/N2MY_IndividualAccount.class.php");
            $objIndividualAccount = new N2MY_IndividualAccount( $this->get_dsn(), $this->get_auth_dsn() );
            $login_info["user_info"]["user_key"];
            $planInfo = $objIndividualAccount->getPlan( $login_info["user_info"]["user_key"] );
            $this->session->set( 'plan_info', $planInfo );
        }
        //　メインメニューに遷移
        $this->logger2->info($user_id, "center_admin_login");
        $this->logger->debug(__FUNCTION__, __FILE__, __LINE__, array(
            "server_info" => $server_info,
            "login_info" => $login_info));
        $url = $this->get_redirect_url("services/admin/index.php");
        $this->logger->trace(__FUNCTION__."#end", __FILE__, __LINE__,$url);

        $rooms = $this->get_room_list();
        header("Location: ".$url);
        //操作ログ追加
        $this->add_operation_log("login");
        exit;
    }

    function default_view() {
        $this->logger->trace(__FUNCTION__ . "#start", __FILE__, __LINE__);
        $this->action_showTop();
        $this->logger->trace(__FUNCTION__ . "#end", __FILE__, __LINE__);
    }

    private function action_server_down()
    {
        $this->render_fault();
    }

    private function render_fault()
    {
        echo "server down";
    }

    function action_set_env() {
        // 有効期限
        $limit_time = time() + 365 * 24 * 3600;
        // 地域コード
        $_country_key = $this->request->get("country_key");
        $request = $this->request->getAll();
        if ($_country_key) {
            $country_list = $this->get_country_list();
            // 改ざんされた場合の対応
            if (!array_key_exists($_country_key, $country_list)) {
                $country_keys = array_keys($country_list);
                $_country_key = $country_keys[0];
                $this->logger->warn(__FUNCTION__, __FILE__, __LINE__,$_country_key);
            }
            foreach ($country_list as $country_key => $country_row) {
                if ($_country_key == $country_key) {
                    setcookie("country", $_country_key,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
                }
            }
        }
        // 言語コード
        $lang = $this->request->get("lang");
        if ($lang) {
            setcookie("lang", $lang,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
            $this->session->set("lang", $lang);
        }
        // タイムゾーン
        $time_zone = $this->request->get("time_zone");
        if (is_numeric($time_zone)) {
            setcookie("time_zone", $time_zone,  N2MY_COOKIE_LIFETIME, N2MY_COOKIE_PATH,N2MY_COOKIE_DOMAIN,N2MY_COOKIE_SECURE,N2MY_COOKIE_HTTPONRY);
        }
        header("Location: index.php");
    }

}

$main = new AppCenterAdminLogin();
$main->execute();
?>
