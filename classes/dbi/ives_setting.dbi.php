<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 foldmethod=marker: */

require_once("classes/N2MY_DBI.class.php");

/*
 * ビジネスロジックを含まない純粋なIvesSettingテーブルのDBI
 * */

class IvesSettingTable extends N2MY_DB {

    var $table = "ives_setting";
    var $logger = null;
    protected $primary_key = 'ives_setting_key';

    public function __construct( $dsn )
    {
        $this->init($dsn, $this->table );
    }
    public function add($data)
    {
        $data["registtime"] = date("Y-m-d H:i:s");
        $pgi_setting_key = parent::add($data);
        if (PEAR::isError($pgi_setting_key)) {
            throw new Exception("db errror where:$where , data:".print_r($data,true)." mysql said".mysql_error());
        }


        return $pgi_setting_key;
    }
    public function update($data, $where)
    {
        $data["updatetime"] = date("Y-m-d H:i:s");
        $res = parent::update($data, $where);
        if (PEAR::isError($res)) {
            throw new Exception("db errror where:$where , data:".print_r($data,true).' mysql_error:'.mysql_error());
        }
    }
    //}}}
    public function findByKey($ives_setting_key)
    {
        if (!$ives_setting_key) {
            throw new Exception("ives_setting_key is empty ");
        }

        $where  = "ives_setting_key = '".mysql_real_escape_string($ives_setting_key)."'"
                  ." AND is_deleted = 0";

        $db_res = $this->select($where , array(),1);
        if ($db_res === false || PEAR::isError($db_res)) {
            throw new Exception(__FILE__.' '.__LINE__." where : $where");
        }

        if (!$db_res) {
            return array();
        }

        return $db_res->fetchRow(DB_FETCHMODE_ASSOC);
    }
    public function findLatestByRoomKey($roomKey) {
        if (!$roomKey) {
            throw new Exception("room_key is empty ");
        }
        $where  = "room_key = '".mysql_real_escape_string($roomKey)."' AND is_deleted != 1";
        $db_res = $this->select($where , array('ives_setting_key' => 'desc'));
        if (!$db_res) {
            return false;
        }
        return $db_res->fetchRow(DB_FETCHMODE_ASSOC);
        /*
        $res = array();
        while ($row  =$db_res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $res[] = $row;
        }
        return $res;
        */	
    }
    
    public function findLatestByUserKey($userKey) {
    	$where  = "user_key = '".mysql_real_escape_string($userKey)."' AND is_deleted = 0";
        $db_res = $this->select($where , array('ives_setting_key' => 'desc'));
        if (!$db_res) {
            return false;
        }
        return $db_res->fetchRow(DB_FETCHMODE_ASSOC);
    }
    
    public function findByRoomKey($room_key)
    {
        if (!$room_key) {
            throw new Exception("room_key: is empty ");
        }

        $where  = "room_key = '".mysql_real_escape_string($room_key)."'"
                 ." AND is_deleted = 0";

        $db_res = $this->select($where , array('startdate' => 'desc'));

        if (!$db_res) {
            return false;
        }
        $res = array();
        while ($row  =$db_res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $res[] = $row;
        }
        return $res;
    }
    public function findByDid($did)
    {
        if (!$did) {
            throw new Exception("did: is empty ");
        }

        $where  = "ives_did = '".mysql_real_escape_string($did)."'"
                 ." AND is_deleted = 0";

        $db_res = $this->select($where , array('startdate' => 'desc'));

        if (!$db_res) {
            return false;
        }
        $res = array();
        while ($row  =$db_res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $res[] = $row;
        }
        return $res;
    }
}
