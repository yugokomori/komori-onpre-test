
jQuery.noConflict();
var j$ = jQuery;

/* ================================================== */

/*  共通  */

/* ================================================== */


// リンクの点線を消す
j$(document).ready(function(){j$("a").bind("focus",function(){if(this.blur)this.blur();});});

//checker

function str_escape(str) {

    if (!str) return "";
    return str
        .replace(/&/g   ,"&amp;")
        .replace(/</g   ,"&lt;")
        .replace(/>/g   ,"&gt;")
        .replace(/\x22/g,"&quot;")
        .replace(/\x27/g,"&#39;")
    ;
    return str;
}

function window_close_logout() {
    if (confirm(_('x1',"アプリケーションを終了しますか？")) == true) {
        var myAjax = new Ajax.Request (
            "/services/logout/index.php",
            {
                method: 'get'
            });
        window.close();
    }
}

function get_flash_version() {
    // swfobject
    var playerVersion = swfobject.getFlashPlayerVersion();
    return playerVersion.major + "." + playerVersion.minor + "." + playerVersion.release;
}

function _() {
    if (!arguments || arguments.length < 1 || !RegExp) return;
    var id = arguments[0];
    var msg = gLangList[id];
    // 本番適用時には外します
//    if (arguments[1] != gOriginalLangList[id]) {
//        alert("オリジナルのメッセージIDと一致していません。\n"+
//                "DBの文言が更新されていないか、IDが間違っている可能性があります。\n" +
//                "再度確認してください\n\n" +
//                "ID: "+id+"\n" +
//                arguments[1]+"\n" +
//                gOriginalLangList[id]);
//    }
    if (msg) {
        str = msg;
    } else {
        if (gLangCustomList[id]) {
            str = gLangCustomList[id];
        } else {
            str = arguments[1];
        }
    }

    arguments[0] = null;
    arguments[1] = null;
    arguments    = $A(arguments).compact();
    if (arguments.length == 1 && typeof arguments[0] == 'object') {
        arguments = $A(arguments[0]);
    }
    if (arguments.length == 0) {
        return str;
    }
    return sprintf(str,arguments);
}

function addEvent(element, name, func) {
    if (element) {
        element.addEventListener ?
        element.addEventListener(name, func, false) :
        element.attachEvent('on' + name, func);
    }
}

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- V1.1.3: Sandeep V. Tamhankar (stamhankar@hotmail.com) -->
<!-- Original:  Sandeep V. Tamhankar (stamhankar@hotmail.com) -->
<!-- Changes:
/* 1.1.4: Fixed a bug where upper ASCII characters (i.e. accented letters
international characters) were allowed.

1.1.3: Added the restriction to only accept addresses ending in two
letters (interpreted to be a country code) or one of the known
TLDs (com, net, org, edu, int, mil, gov, arpa), including the
new ones (biz, aero, name, coop, info, pro, museum).  One can
easily update the list (if ICANN adds even more TLDs in the
future) by updating the knownDomsPat variable near the
top of the function.  Also, I added a variable at the top
of the function that determines whether or not TLDs should be
checked at all.  This is good if you are using this function
internally (i.e. intranet site) where hostnames don't have to
conform to W3C standards and thus internal organization e-mail
addresses don't have to either.
Changed some of the logic so that the function will work properly
with Netscape 6.

1.1.2: Fixed a bug where trailing . in e-mail address was passing
(the bug is actually in the weak regexp engine of the browser; I
simplified the regexps to make it work).

1.1.1: Removed restriction that countries must be preceded by a domain,
so abc@host.uk is now legal.  However, there's still the
restriction that an address must end in a two or three letter
word.

1.1: Rewrote most of the function to conform more closely to RFC 822.

1.0: Original  */
function validEmail(emailStr) {
 var checkTLD=0;
 var knownDomsPat=/^(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum)$/;
 var emailPat=/^(.+)@(.+)$/;
 var specialChars="\\(\\)><@,;:\\\\\\\"\\.\\[\\]";
 var validChars="\[^\\s" + specialChars + "\]";
 var quotedUser="(\"[^\"]*\")";
 var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
 var atom=validChars + '+';
 var word="(" + atom + "|" + quotedUser + ")";
 var userPat=new RegExp("^" + word + "(\\." + word + ")*$");
 var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$");
 var matchArray=emailStr.match(emailPat);
 if (matchArray==null) {
//  alert("Email address seems incorrect (check @ and .'s)");
  return false;
 }
 var user=matchArray[1];
 var domain=matchArray[2];
 for (i=0; i<user.length; i++) {
  if (user.charCodeAt(i)>127) {
//   alert("Ths username contains invalid characters.");
   return false;
  }
 }
 for (i=0; i<domain.length; i++) {
  if (domain.charCodeAt(i)>127) {
//   alert("Ths domain name contains invalid characters.");
   return false;
  }
 }
 if (user.match(userPat)==null) {
//  alert("The username doesn't seem to be valid.");
  return false;
 }
 var IPArray=domain.match(ipDomainPat);
 if (IPArray!=null) {
  for (var i=1;i<=4;i++) {
   if (IPArray[i]>255) {
//    alert("Destination IP address is invalid!");
    return false;
   }
  }
  return true;
 }
 var atomPat=new RegExp("^" + atom + "$");
 var domArr=domain.split(".");
 var len=domArr.length;
 for (i=0;i<len;i++) {
  if (domArr[i].search(atomPat)==-1) {
//   alert("The domain name does not seem to be valid.");
   return false;
  }
 }
 if (checkTLD && domArr[domArr.length-1].length!=2 &&
  domArr[domArr.length-1].search(knownDomsPat)==-1) {
//  alert("The address must end in a well-known domain or two letter " + "country.");
  return false;
 }

 if (len<2) {
//  alert("This address is missing a hostname!");
  return false;
 }
 return true;
}

