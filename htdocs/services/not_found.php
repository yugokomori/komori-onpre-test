<?php
require_once("classes/AppFrame.class.php");
class AppPageNotFound extends AppFrame {

    function init() {
    }

    function auth() {
    }

    function default_view(){
        $this->display("user/404_error.t.html");
    }

}
$main = new AppPageNotFound();
$main->execute();
?>
