<?php
require_once("classes/N2MY_DBI.class.php");

class UserFmsServerTable extends N2MY_DB
{

    var $table = 'user_fms_server';
    protected $primary_key = "fms_key";

    function __construct( $dsn ) {
        $this->init($dsn, $this->table );
    }
}

