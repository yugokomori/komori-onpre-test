ALTER TABLE  `ives_setting` ADD  `ignore_video_conference_address` TINYINT( 1 ) NOT NULL DEFAULT  '0' AFTER  `use_active_speaker`;
ALTER TABLE  `video_conference_participant` ADD  `participant_ip` VARCHAR( 20 ) NOT NULL AFTER  `participant_name`;
ALTER TABLE  `video_conference` ADD  `document_sharing_status` TINYINT( 1 ) NOT NULL AFTER  `did`;
ALTER TABLE  `video_conference_participant` ADD  `participant_state` INT( 2 ) NOT NULL AFTER  `participant_type` ,
ADD INDEX ( participant_state );