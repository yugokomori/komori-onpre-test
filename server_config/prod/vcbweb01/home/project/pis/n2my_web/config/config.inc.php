<?php
#---------
# 環境設定
#---------
# アプリケーションルートディレクトリ
define('N2MY_APP_DIR',      '/home/project/pis/n2my_web/');
# バーチャル毎に設定を切り替えたい場合のみ、.htaccessなどで指定してください
define('N2MY_CONFIG_FILE',   isset($_SERVER['CONFIG_FILE']) ? $_SERVER['CONFIG_FILE'] : N2MY_APP_DIR."config/config.ini");
# video conference設定ファイル
define('N2MY_VC_CONFIG_FILE',   N2MY_APP_DIR."config/mcu_config.ini");
define('N2MY_MCU_SERVER_XML',   N2MY_APP_DIR."config/mcu.xml");
# PGi Hybrid設定ファイル
define('N2MY_PGI_CONFIG_FILE',   N2MY_APP_DIR."config/pgi_config.ini");
# ドキュメントルートパス
define('N2MY_DOCUMENT_ROOT', N2MY_APP_DIR.'htdocs/');
# 資料変換時のディレクトリ
define('N2MY_DOC_DIR',       N2MY_DOCUMENT_ROOT.'docs/');
# ファイルキャビネットディレクトリ
define('N2MY_FILES_DIR',     N2MY_DOCUMENT_ROOT.'files/');
# 共有メモのディレクトリ
define('N2MY_SHARE_DIR',     N2MY_APP_DIR.'shared/');
# 録画GW生成先ディレクトリ
define('N2MY_MOVIES_DIR',    N2MY_APP_DIR.'webapp/movies/');
# ベースURL
//define('N2MY_BASE_URL',      'http://tvcbweb01.mew.co.jp/vcube/');
//define('N2MY_BASE_URL', 'http://testfep.arrow.mew.co.jp/vcube/');
define('N2MY_BASE_URL', 'https://webmeeting.arrow.mew.co.jp/vcube/');
//define('N2MY_BASE_URL', 'http://'.$_SERVER['SERVER_NAME'].'/vcube/');
# サーバ内から実行するURL
define('N2MY_LOCAL_URL',     'http://vcbweb01.mew.co.jp/vcube/');

//define('N2MY_BASE_ADMIN_URL',      'http://tvcbweb01.mew.co.jp/vcadmin/');
define('N2MY_BASE_ADMIN_URL', 'http://'.$_SERVER['SERVER_NAME'].'/vcadmin/');

//define('N2MY_BASE_FEP_URL',      'http://testfep.arrow.mew.co.jp/vcube/');
define('N2MY_BASE_FEP_URL',      'https://webmeeting.arrow.mew.co.jp/vcube/');

//define('N2MY_BASE_GUEST_URL',      'http://testfep.arrow.mew.co.jp/vcube/guest/');
define('N2MY_BASE_GUEST_URL',      'https://webmeeting.arrow.mew.co.jp/vcube/guest/');

//define('N2MY_LIVE_URL',      'http://testfep.arrow.mew.co.jp/');
define('N2MY_LIVE_URL',      'https://webmeeting.arrow.mew.co.jp/');

//define('N2MY_RESERVATION_WEB_URL',      'http://testfep.arrow.mew.co.jp/vcuberes/');
define('N2MY_RESERVATION_WEB_URL',      'https://webmeeting.arrow.mew.co.jp/vcuberes/');
# PHPディレクトリ
define('N2MY_PHP_DIR',       '/usr/local/php-5.4.15/');
# Rubyディレクトリ
define('N2MY_RUBY_DIR',      '/usr/bin/');
# ImageMagickディレクトリ
define('N2MY_IMGMGC_DIR',    '/usr/local/bin/');
# デフォルトDB
define('N2MY_DEFAULT_DB',    'pis_data');
# 可逆暗号化
define('N2MY_ENCRYPT_KEY',   'N2MY');
# 可逆暗号化 IV(８文字)
define('N2MY_ENCRYPT_IV',    'N2MYN2MY');
# 可逆暗号化2
define('VCUBE_ENCRYPT_KEY',   'N2MY');
# 可逆暗号化2 IV(８文字)
define('VCUBE_ENCRYPT_IV',    'N2MYN2MY');
# GIPS共通鍵
define('N2MY_IDENTITY_KEY',  '{N2MY_IDENTITY_KEY}');
# fmsとのSCPコマンドを行うユーザー名
define('N2MY_SCP_USER',  'n2my');
# mcuとのSCPコマンドを行うユーザー名
define('N2MY_MCU_SCP_USER',  'n2my');
# AP server とのSCPコマンドを行うユーザー名
define('N2MY_AP_SCP_USER',  'n2my');

# 管理者用利用ログを取得するuser_key
define('ADMIN_USER_KEY', 3);
# 管理者用利用ログのパス
define('ADMIN_CSV_PATH', '/mnt/vcbweb01/admin_meeting_log/');
# 統合ID処理時、メンバーに設定するパスワード
define('MEMBER_PASSWORD', "Gt0vsS1hVdOsoNfI01yG");
# 統合ID基本リストのパス
define('WIDS_KIHON_PATH', N2MY_APP_DIR . 'bin/id_list/WIDS_VCUBE_KIHON');
# 統合ID原籍リストのパス
define('WIDS_ZAISEKI_PATH', N2MY_APP_DIR . 'bin/id_list/WIDS_VCUBE_ZAISEKI');
# ID変換リストのパス
define('WIDS_HENKAN_PATH', N2MY_APP_DIR . 'bin/id_list/WIDS_VCUBE_HENKAN');
# ID基本情報リストのパス
define('WIDS_GID_KIHON_PATH', N2MY_APP_DIR . 'bin/id_list/WIDS_VCUBE_GID_KIHON');
# 所属情報リストのパス
define('WIDS_GID_SYOZOKU_PATH', N2MY_APP_DIR . 'bin/id_list/WIDS_VCUBE_GID_SYOZOKU');
# TID_LIST
define('TID_LIST_PATH', N2MY_APP_DIR . 'bin/id_list/TID_LIST');
# GID_LIST
define('GID_LIST_PATH', N2MY_APP_DIR . 'bin/id_list/GID_LIST');
# USER_LIST
define('USER_LIST_PATH', N2MY_APP_DIR . 'bin/id_list/USER_LIST');
# USER_LIST_FLG_FILE
define('USER_LIST_FLG_PATH', N2MY_APP_DIR . 'bin/id_list/USER_LIST_FLG_FILE');


# FTPサーバ利用フラグ
define('FTP_USEFLG','1');

# FTPサーバ情報 KIHON (IP address , Port , ID , PW , Filepath)
define('FTP_SERVER_KIHON','widsftp01.arrow.mew.co.jp');
define('FTP_SERVER_PORT_KIHON','21');
define('FTP_SERVER_ID_KIHON','pubvcube');
define('FTP_SERVER_PW_KIHON','EbUcvpUb');
define('FTP_SERVER_WIDS_KIHON_PATH','/WIDS_VCUBE_KIHON.csv');

# FTPサーバ情報 ZAISEKI (IP address , Port , ID , PW , Filepath)
define('FTP_SERVER_ZAISEKI','widsftp01.arrow.mew.co.jp');
define('FTP_SERVER_PORT_ZAISEKI','21');
define('FTP_SERVER_ID_ZAISEKI','pubvcube');
define('FTP_SERVER_PW_ZAISEKI','EbUcvpUb');
define('FTP_SERVER_WIDS_ZAISEKI_PATH','/WIDS_VCUBE_ZAISEKI.csv');

# FTPサーバ情報 HENKAN (IP address , Port , ID , PW , Filepath)
define('FTP_SERVER_HENKAN','widsftp01.arrow.mew.co.jp');
define('FTP_SERVER_PORT_HENKAN','21');
define('FTP_SERVER_ID_HENKAN','pubvcube');
define('FTP_SERVER_PW_HENKAN','EbUcvpUb');
define('FTP_SERVER_WIDS_HENKAN_PATH','/WIDS_VCUBE_CONV.csv');

# FTPサーバ情報 GID_KIHON (IP address , Port , ID , PW , Filepath)
define('FTP_SERVER_GID_KIHON','ftppaids01.is.jp.panasonic.com');
define('FTP_SERVER_PORT_GID_KIHON','21');
define('FTP_SERVER_ID_GID_KIHON','ftppaids_vcb');
define('FTP_SERVER_PW_GID_KIHON','ftppaids_vcb');
define('FTP_SERVER_WIDS_GID_KIHON_PATH','/PAIDS_VCUBE_KIHON.csv');

# FTPサーバ情報 GID_SYOZOKU (IP address , Port , ID , PW , Filepath)
define('FTP_SERVER_GID_SYOZOKU','ftppaids01.is.jp.panasonic.com');
define('FTP_SERVER_PORT_GID_SYOZOKU','21');
define('FTP_SERVER_ID_GID_SYOZOKU','ftppaids_vcb');
define('FTP_SERVER_PW_GID_SYOZOKU','ftppaids_vcb');
define('FTP_SERVER_WIDS_GID_SYOZOKU_PATH','/PAIDS_VCUBE_SYOZOKU.csv');



# 会議削除期限（日数）
define('MEETINGLOG_EXPIRE_DAY',  '365');

#---------
# メールアドレス
#---------
# メールアドレス設定
ini_set("SMTP", 'smtpchk.mew.co.jp');
# NO REPLY
define('NOREPLY_ADDRESS',      'tvteam@ml.is-c.jp.panasonic.com');
# メール送信元
define('N2MY_MAIL_FROM',      'tvteam@ml.is-c.jp.panasonic.com');
# アカウント発行情報の送信先
define('N2MY_ACCOUNT_TO',     '');
# アカウント発行情報の送信元
define('N2MY_ACCOUNT_FROM',   '');
# サービス管理者問い合わせ先
define('ADMIN_INQUIRY_TO',    '');
# サービス管理者問い合わせ元
define('ADMIN_INQUIRY_FROM',  '');
# ユーザページお問い合わせ先
define('USER_ASK_TO',         '');
# ユーザページお問い合わせ元
define('USER_ASK_FROM',       '');
# パスワードリセットお問い合わせ先（管理者メールアドレスが登録されていない場合）
define('PW_RESET_TO',         '');
# パスワードリセットお問い合わせ元
define('PW_RESET_FROM',       '');
# 予約時の送信元
define('RESERVATION_FROM',    'tvteam@ml.is-c.jp.panasonic.com');
# システムの警告メール送信元
define('N2MY_ALERT_TO',       '');
# システムの警告メール送信先
define('N2MY_ALERT_FROM',     '');
# 会議記録移動エラーメール送信先
define('N2MY_LOG_ALERT_TO',   '');
# 会議記録移動エラーメール送信元
define('N2MY_LOG_ALERT_FROM', '');
# システムログがエラー以上の場合に警告メールを送信
define('N2MY_ERROR_FROM',     '');
# システムログがエラー以上の場合に警告メールを送信
define('N2MY_ERROR_TO',       '');
# Cron実行メールを送信
define('N2MY_CRON_FROM',     '');
# Cron実行メールを送信
define('N2MY_CRON_TO',       '');
# 予約再割り振り失敗アラート送信元
define('N2MY_RETRY_RESERVATION_FROM', 'tvteam@ml.is-c.jp.panasonic.com');
# 予約再割り振り失敗アラート送信先
define('N2MY_RETRY_RESERVATION_TO', 'tvteam@ml.is-c.jp.panasonic.com,webmt-apply@ml.is-c.jp.panasonic.com');
# MCUアラートFROM
define('N2MY_MCU_NOTIFICATION_FROM', 'tvteam@ml.is-c.jp.panasonic.com');
# MCUアラートTO
define('N2MY_MCU_NOTIFICATION_TO', 'tvteam@ml.is-c.jp.panasonic.com,webmt-apply@ml.is-c.jp.panasonic.com');
# DOCアラートFROM
define('N2MY_DOC_ALERT_FROM',    'tvteam@ml.is-c.jp.panasonic.com');
# DOC アラートTO
define('N2MY_DOC_ALERT_TO',    'tvteam@ml.is-c.jp.panasonic.com,webmt-apply@ml.is-c.jp.panasonic.com');

#---------
# アプリケーション設定
#---------
# ユーザー用メインメニュー会議室表示件数
define('MAINMENU_MAX_VIEW', '5');
# 会議予約一覧表示件数
define('RESERVATION_MAX_VIEW', '5');
# 録画容量(Mbyte)
define('RECORD_SIZE', '500');
# サーバー側で使用したCO2（1人/分）
define('ECO_CLIENT_CO2', 2.053628);
# ログレベルのエラー検出時のメール送信フラグ
define("N2MY_ERROR_NOTIFICATION", '0');
# ストレージ容量 1ユーザー
define('STORAGE_USER_SIZE' , 500);
# ストレージ容量 1部屋
define('STORAGE_ROOM_SIZE' , 500);
# ストレージ容量 オプション
define('STORAGE_OPTION_SIZE' , 1000);
# 登録可能バナー横幅
define('BANNER_WIDTH_MAX', 500);
# 登録可能バナー縦幅
define('BANNAR_HEIGHT_MAX', 300);


#---------
# php.ini
#---------
ini_set("display_errors",  '0');
ini_set("log_errors",      '1');
ini_set("error_reporting", '2039');
ini_set("error_log",       N2MY_APP_DIR.'logs/php_error.log');

#---------
# session
#---------
ini_set("session.save_path", N2MY_APP_DIR.'tmp');
ini_set("session.gc_maxlifetime", '604800');
ini_set("session.cache_limiter", 'none');
ini_set("session.save_handler", 'files');

#---------
# API利用時のセッション名 (.htaccessのSetEnvで変更可能)
#---------
if (!defined('N2MY_SESSION')) {
    define('N2MY_SESSION', 'n2my_session');
}

class N2MY_SESSION_DB {

    var $sess_db = null;

    function sess_open() {
        require_once('lib/EZLib/EZCore/EZConfig.class.php');
        require_once('lib/EZLib/EZDB/EZDB.class.php');
        $config = EZConfig::getInstance(N2MY_CONFIG_FILE);
        EZLogger::getInstance($config);
        EZLogger2::getInstance($config);
        $auth_dsn = $config->get("GLOBAL", "auth_dsn");
        $this->sess_db = new EZDB();
        $this->sess_db->init($auth_dsn, "php_session");
    }

    function sess_close() {
    }

    function sess_read($id) {
        $where = "id = '".mysql_real_escape_string($id)."'";
        $data = $this->sess_db->getOne($where, "data");
        return $data;
    }

    function sess_write($id, $data) {
        $access = time();
        $data = array(
            "id" => $id,
            "access" => $access,
            "data" => $data,
        );
        return $this->sess_db->replace($data);
    }

    function sess_destroy($id) {
        $where = "id = '".mysql_real_escape_string($id)."'";
        return $this->sess_db->remove($where);
    }

    function sess_clean($max) {
        $old = time() - $max;
        $where = "access = '".mysql_real_escape_string($old)."'";
        return $this->sess_db->remove($where);
    }
}

if (ini_get("session.save_handler") == "user") {

    $_n2my_session = new N2MY_SESSION_DB();
    session_set_save_handler(
        array($_n2my_session, 'sess_open'),
        array($_n2my_session, 'sess_close'),
        array($_n2my_session, 'sess_read'),
        array($_n2my_session, 'sess_write'),
        array($_n2my_session, 'sess_destroy'),
        array($_n2my_session, 'sess_clean')
        );
}
