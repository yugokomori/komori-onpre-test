CREATE TABLE `news_cabinet` (
`news_cabinet_key` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ファイルキー',
`news_key` int(11) NOT NULL COMMENT  'インフォメーションキー',
`session_id` varchar(64) NOT NULL COMMENT  'セッションキー',
`tmp_name`  varchar(255) DEFAULT NULL COMMENT  'テンポラリネーム',
`type` varchar(255) NOT NULL COMMENT  'タイプ',
`size` int(10) NOT NULL DEFAULT '0' COMMENT 'ファイルサイズ',
`extension` varchar(255) NOT NULL COMMENT  '拡張子',
`file_name` varchar(255) NOT NULL COMMENT  'ファイル名',
`file_path` varchar(255) NOT NULL COMMENT  'ファイルパス',
`create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
PRIMARY KEY (`news_cabinet_key`),
KEY `user_key` (`news_key`)
)ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `class_list` (
`class_list_key` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '区分キー',
`id_classification_key` int(11) NOT NULL  COMMENT 'ID側で管理しているキー',
`name`  varchar(64) NOT NULL COMMENT  '区分名',
`status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'ステータス 0:無効、1:有効',
`add_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'IDを追加する区分フラグ 0:無効、1:有効',
`create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
`update_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
PRIMARY KEY (`class_list_key`)
)ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

insert into class_list(id_classification_key,name,status,add_flag) values (1,'三者協定',1,1);
insert into class_list(id_classification_key,name,status,add_flag) values (2,'連結会社',1,0);
insert into class_list(id_classification_key,name,status,add_flag) values (3,'住建販社',1,0);
insert into class_list(id_classification_key,name,status,add_flag) values (4,'電材販社',1,0);
insert into class_list(id_classification_key,name,status,add_flag) values (5,'代理店',1,0);
insert into class_list(id_classification_key,name,status,add_flag) values (6,'工務店',1,0);
insert into class_list(id_classification_key,name,status,add_flag) values (7,'協力会社',1,0);
insert into class_list(id_classification_key,name,status,add_flag) values (0,'その他',1,0);

INSERT INTO news (news_lang,news_date,news_title,news_contents,news_show) VALUES ('en_US','2015-10-22','','aaaa',1) ;
INSERT INTO news (news_lang,news_date,news_title,news_contents,news_show) VALUES ('ja_JP','2015-10-22','','aaaa',1) ;